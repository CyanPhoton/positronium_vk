use std::mem::MaybeUninit;
use std::ops::{Deref, DerefMut};
use std::path::Path;
use std::ptr::NonNull;
use defer_alloc::DeferAllocArc;

use crate::*;

pub(crate) type GeneralLoader<H> = extern "C" fn(handle: H, name: UnsizedCStr) -> VoidFunction;

pub(crate) fn load_fn<H, F: Copy>(loader: GeneralLoader<H>, handle: H, name: UnsizedCStr) -> Option<F> {
    let ptr = loader(handle, name);
    if unsafe { std::mem::transmute::<_, *const u8>(ptr) } != std::ptr::null() {
        unsafe { Some(*core::mem::transmute::<_, &F>(&ptr)) }
    } else {
        None
    }
}

pub struct Handle;

impl Handle {
    pub fn null<H>() -> H where H: IsHandle {
        H::null()
    }
}

pub unsafe trait IsHandle: Sized {
    fn null() -> Self {
        unsafe { core::mem::zeroed() }
    }
}

pub unsafe trait IsHandleRef<'handle>: Sized {
    fn null() -> Self {
        unsafe { core::mem::zeroed() }
    }
}

pub struct LibraryData {
    pub(crate) dropper: DeferAllocArc<LibraryDropper>,
}

pub struct LibraryDropper {
    #[allow(dead_code)] // Used for keep alive
    library: Option<libloading::Library>
}

#[cfg(all(feature="automatic_destroy_on_drop", feature="nullable_functional_handles"))]
impl LibraryDropper {
    pub fn null() -> Self {
        LibraryDropper {
            library: None
        }
    }
}

impl LibraryData {
    pub fn new(library: libloading::Library) -> LibraryData {
        LibraryData {
            dropper: DeferAllocArc::new(LibraryDropper {
                library: Some(library)
            })
        }
    }

    #[cfg(feature="nullable_functional_handles")]
    pub fn null() -> LibraryData {
        LibraryData {
            dropper: DeferAllocArc::new(LibraryDropper::null()),
        }
    }
}

impl Loader {
    pub fn new_auto() -> core::result::Result<Loader, libloading::Error> {
        #[cfg(windows)]
            let library_path = std::path::Path::new("vulkan-1.dll");
        #[cfg(all(unix, not(target_os = "android"), not(target_os = "macos")))]
            let library_path = std::path::Path::new("libvulkan.so.1");
        #[cfg(target_os = "macos")]
            let library_path = std::path::Path::new("libvulkan.dylib");

        Self::from_lib_path(library_path)
    }

    pub fn from_lib_path(library_path: &Path) -> core::result::Result<Loader, libloading::Error> {
        let lib = unsafe { libloading::Library::new(library_path) }?;

        let get_instance_proc_addr = unsafe { core::mem::transmute(lib.get::<GetInstanceProcAddr>(b"vkGetInstanceProcAddr\0")?.into_raw().into_raw()) };

        let library_data = LibraryData::new(lib);

        Ok(Self::from_proc_addr_func(&library_data, get_instance_proc_addr))
    }

    pub fn from_proc_addr_func(library_data: &LibraryData, get_instance_proc_addr: GetInstanceProcAddr) -> Loader {
        Loader {
            data: LoaderData::new(library_data, get_instance_proc_addr)
        }
    }
}

pub enum AllocMode {
    Implementation,
    InheritParent,
    Custom(RawAllocationCallbacks)
}

impl AllocMode {
    pub fn apply(self, parent: Option<RawAllocationCallbacks>) -> Option<RawAllocationCallbacks> {
        match self {
            AllocMode::Implementation => None,
            AllocMode::InheritParent => parent,
            AllocMode::Custom(a) => Some(a)
        }
    }
}

pub fn nonnull_maybe_uninit<T, M: GeneralMaybeUninit<T>>(uninit: &mut M) -> NonNull<T> {
    // This is sound since uninit.as_mut_ptr() always return non-null
    unsafe { NonNull::new_unchecked(uninit.get_mut_ptr()) }
}

// Multi mut slice share

pub struct MutHandleMultiShareBase<'handle, H: IsHandleRef<'handle>, N: Sized> {
    pub(crate) handle: &'handle mut H,
    pub(crate) multi_slices: N
}

pub struct MutHandleMultiShareEmptyBase<'handle, H: IsHandleRef<'handle>> {
    pub(crate) handle: &'handle mut H,
}

impl <'handle, H: IsHandleRef<'handle>, N: Sized> MutHandleMultiShareBase<'handle, H, N> {
    pub fn chain_mut_share<'slice, I, A: Fn(&mut I) -> &mut H>(self, slice: &'slice mut [I], accessor: A) -> MutHandleMultiShareBase<'handle, H, (N, MutHandleMultiShare<'handle, 'slice, H, I, A>)> {
        for v in slice.iter_mut() {
            // Safety: We are copying for example a `DescriptorSetMutHandle` which is not normally safe
            // since it gives multiple mutable "references", though in this case we are controlling it in
            // a way that should be safe for Vulkan's externally synchronized parameter lists
            *accessor(v) = unsafe { core::ptr::read(self.handle) };
        }
        MutHandleMultiShareBase {
            handle: self.handle,
            multi_slices: (self.multi_slices, MutHandleMultiShare {
                _handle: Default::default(),
                slice,
                accessor
            })
        }
    }

    pub fn finish<U: FnMut(N)>(self, mut user: U) {
        user(self.multi_slices);
    }
}

impl <'handle, H: IsHandleRef<'handle>> MutHandleMultiShareEmptyBase<'handle, H> {
    pub fn chain_mut_share<'slice, I, A: Fn(&mut I) -> &mut H>(self, slice: &'slice mut [I], accessor: A) -> MutHandleMultiShareBase<'handle, H, MutHandleMultiShare<'handle, 'slice, H, I, A>> {
        for v in slice.iter_mut() {
            // Safety: We are copying for example a `DescriptorSetMutHandle` which is not normally safe
            // since it gives multiple mutable "references", though in this case we are controlling it in
            // a way that should be safe for Vulkan's externally synchronized parameter lists
            *accessor(v) = unsafe { core::ptr::read(self.handle) };
        }
        MutHandleMultiShareBase {
            handle: self.handle,
            multi_slices: MutHandleMultiShare {
                _handle: Default::default(),
                slice,
                accessor
            }
        }
    }
}

pub struct MutHandleMultiShare<'handle, 'slice, H: IsHandleRef<'handle>, I, A: Fn(&mut I) -> &mut H> {
    pub(crate) _handle: core::marker::PhantomData<&'handle H>,
    pub(crate) slice: &'slice mut [I],
    pub(crate) accessor: A,
}

impl<'handle, 'slice, H: IsHandleRef<'handle>, I, A: Fn(&mut I) -> &mut H> MutHandleMultiShare<'handle, 'slice, H, I, A> {
    pub fn as_slice(&self) -> &[I] {
        self.deref()
    }

    pub fn as_mut_slice(&mut self) -> &mut [I] {
        self.deref_mut()
    }
}

impl<'handle, 'slice, H: IsHandleRef<'handle>, I, A: Fn(&mut I) -> &mut H> Drop for MutHandleMultiShare<'handle, 'slice, H, I, A> {
    fn drop(&mut self) {
        for v in self.slice.iter_mut() {
            *(self.accessor)(v) = H::null();
        }
    }
}

impl<'handle, 'slice, H: IsHandleRef<'handle>, I, A: Fn(&mut I) -> &mut H> Deref for MutHandleMultiShare<'handle, 'slice, H, I, A> {
    type Target = [I];

    fn deref(&self) -> &[I] {
        self.slice
    }
}

impl<'handle, 'slice, H: IsHandleRef<'handle>, I, A: Fn(&mut I) -> &mut H> DerefMut for MutHandleMultiShare<'handle, 'slice, H, I, A> {
    fn deref_mut(&mut self) -> &mut [I] {
        self.slice
    }
}

pub enum EnumerateOptions<'index, 'slice, I, T> {
    Length(&'index mut I),
    Slice(&'index mut I, &'slice mut [T])
}

impl<'index, 'slice, I, T> EnumerateOptions<'index, 'slice, I, T>  {
    pub fn first_mut<'s>(&'s mut self) -> Option<&'slice mut T> {
        match self {
            EnumerateOptions::Length(_) => None,
            EnumerateOptions::Slice(_, slice) => unsafe { core::mem::transmute(slice.first_mut()) } // Hack lifetimes
        }
    }

    pub fn get_slice_len(&self) -> usize {
        match self {
            EnumerateOptions::Length(_) => 0,
            EnumerateOptions::Slice(_, slice) => slice.len()
        }
    }

    pub fn set_len(&mut self, len: I) {
        match self {
            EnumerateOptions::Length(l) => **l = len,
            EnumerateOptions::Slice(l, _) => **l = len,
        }
    }
}

pub enum DoubleEnumerateOptions<'index, 'slice_1, 'slice_2, I, T1, T2> {
    Length(&'index mut I),
    Slice(&'index mut I, &'slice_1 mut [T1], &'slice_2 mut [T2])
}

impl<'index, 'slice_1, 'slice_2, I, T1, T2> DoubleEnumerateOptions<'index, 'slice_1, 'slice_2, I, T1, T2> {
    pub fn assert_sizes(&self) {
        if let DoubleEnumerateOptions::Slice(_, a, b) = self {
            assert_eq!(a.len(), b.len(), "Slices must be of the same length");
        }
    }

    pub fn first_mut_1<'s>(&'s mut self) -> Option<&'slice_1 mut T1> {
        match self {
            DoubleEnumerateOptions::Length(_) => None,
            DoubleEnumerateOptions::Slice(_, slice, _) => unsafe { core::mem::transmute(slice.first_mut()) } // Hack lifetimes
        }
    }

    pub fn first_mut_2<'s>(&'s mut self) -> Option<&'slice_2 mut T2> {
        match self {
            DoubleEnumerateOptions::Length(_) => None,
            DoubleEnumerateOptions::Slice(_, _, slice) => unsafe { core::mem::transmute(slice.first_mut()) } // Hack lifetimes
        }
    }

    pub fn get_slice_len(&self) -> usize {
        self.assert_sizes();

        match self {
            DoubleEnumerateOptions::Length(_) => 0,
            DoubleEnumerateOptions::Slice(_, slice, _) => slice.len()
        }
    }

    pub fn set_len(&mut self, len: I) {
        match self {
            DoubleEnumerateOptions::Length(l) => **l = len,
            DoubleEnumerateOptions::Slice(l, _, _) => **l = len,
        }
    }
}

// Specialisations

pub struct MappedMemory {
    data: &'static mut [u8],
    #[cfg(feature="automatic_destroy_on_drop")]
    dropper: MappedMemoryDropper
}

impl MappedMemory {
    pub(crate) fn new(data: &'static mut [u8], device_memory: &mut DeviceMemory) -> Self {
        MappedMemory {
            data,
            #[cfg(feature="automatic_destroy_on_drop")]
            dropper: MappedMemoryDropper {
                device: device_memory.data.device,
                device_memory: device_memory.as_mut_handle().handle,
                unmap_memory: device_memory.data.unmap_memory,
                device_memory_dropper: device_memory.data.dropper.clone()
            }
        }
    }

    pub unsafe fn from_raw(data: &'static mut [u8], device_memory: &mut DeviceMemory) -> Self {
        MappedMemory::new(data, device_memory)
    }

    pub fn as_slice(&self) -> &[u8] {
        self.deref()
    }

    pub fn as_mut_slice(&mut self) -> &mut [u8] {
        self.deref_mut()
    }
}

impl Deref for MappedMemory {
    type Target = [u8];

    fn deref(&self) -> &[u8] {
        self.data
    }
}

impl DerefMut for MappedMemory {
    fn deref_mut(&mut self) -> &mut [u8] {
        self.data
    }
}

#[cfg(feature="automatic_destroy_on_drop")]
struct MappedMemoryDropper {
    device: DeviceRaw,
    device_memory: DeviceMemoryMutRaw,
    unmap_memory: UnmapMemory,
    device_memory_dropper: DeferAllocArc<DeviceMemoryDropper>
}

#[cfg(feature="automatic_destroy_on_drop")]
impl Drop for MappedMemoryDropper {
    fn drop(&mut self) {
        (self.unmap_memory)(self.device, self.device_memory)
    }
}

impl CommandPool {
    #[cfg(feature = "automatic_destroy_on_drop")]
    fn fill_first_cmd_dropper(first: &mut Option<DeferAllocArc<CommandBufferDropper>>, value: CommandBufferData) {
        *first = Some(value.dropper)
    }

    pub fn free_command_buffers<I>(&mut self, command_buffers: I) where I: IntoIterator<Item=CommandBuffer> {
        #[cfg(feature = "automatic_destroy_on_drop")]
        let mut first_dropper: Option<DeferAllocArc<CommandBufferDropper>> = None; // Note: Keeps alive the dropper so that parent's don't get dropped early

        let mut first_cmd: Option<(DeviceRaw, CommandPoolRaw)> = None;
        let mut command_buffer_handles = command_buffers.into_iter()
            .map(|mut cmd: CommandBuffer| {
                let h = cmd.as_mut_handle().handle;
                if let Some(first_cmd) = &first_cmd {
                    assert_eq!(first_cmd.1, cmd.data.command_pool, "Command buffers must all belong to the same command pool");
                } else {
                    first_cmd = Some((cmd.data.device, cmd.data.command_pool));
                    #[cfg(feature = "automatic_destroy_on_drop")]
                    Self::fill_first_cmd_dropper(&mut first_dropper, cmd.data);
                }
                h
            })
            .collect::<Vec<_>>();

        let count = command_buffer_handles.len() as u32;
        if let Some((cmd, (device, _))) = command_buffer_handles.first_mut().zip(first_cmd) {
            (self.data.free_command_buffers)(device, self.as_mut_handle().handle, count, Some(core::ptr::NonNull::from(cmd)));
        }
    }
}

impl DescriptorPool {
    #[cfg(feature = "automatic_destroy_on_drop")]
    fn fill_first_set_dropper(first: &mut Option<DeferAllocArc<DescriptorSetDropper>>, value: DescriptorSetData) {
        *first = Some(value.dropper)
    }

    pub fn free_descriptor_sets<I>(&mut self, descriptor_sets: I) -> core::result::Result<SuccessCode, ErrorCode> where I: IntoIterator<Item=DescriptorSet> {
        #[cfg(feature = "automatic_destroy_on_drop")]
            let mut first_dropper: Option<DeferAllocArc<DescriptorSetDropper>> = None; // Note: Keeps alive the dropper so that parent's don't get dropped early

        let mut first_set: Option<(DeviceRaw, DescriptorPoolRaw)> = None;
        let mut descriptor_set_handles = descriptor_sets.into_iter()
            .map(|mut set: DescriptorSet| {
                let h = set.as_mut_handle().handle;
                if let Some(first_set) = &first_set {
                    assert_eq!(first_set.1, set.data.descriptor_pool, "Descriptor sets must all belong to the same descriptor pool");
                } else {
                    first_set = Some((set.data.device, set.data.descriptor_pool));
                    #[cfg(feature = "automatic_destroy_on_drop")]
                        Self::fill_first_set_dropper(&mut first_dropper, set.data);
                }
                h
            })
            .collect::<Vec<_>>();

        let count = descriptor_set_handles.len() as u32;
        if let Some((set, (device, _))) = descriptor_set_handles.first_mut().zip(first_set) {
            let result = (self.data.free_descriptor_sets)(device, self.as_mut_handle().handle, count, Some(core::ptr::NonNull::from(set)));

            if result == RawResult::eSuccess {
                Ok(result.into_success().normalise())
            } else {
                Err(result.into_error().normalise())
            }
        } else {
            Ok(SuccessCode::eSuccess)
        }
    }
}

pub struct DeviceFaultInfoEXT<V: GenericVecGen> {
    pub description: ConstSizeCStr<MAX_DESCRIPTION_SIZE>,
    pub address_infos: V::V<DeviceFaultAddressInfoEXT>,
    pub vendor_infos: V::V<DeviceFaultVendorInfoEXT>,
    pub vendor_binary_data: V::V<u8>
}

pub struct RawDeviceFaultInfoEXT<'f_p_next> {
    s_type: SType<Self>,
    p_next: Option<NextMutPtr<'f_p_next>>,
    description: ConstSizeCStr<MAX_DESCRIPTION_SIZE>,
    address_infos: *mut RawDeviceFaultAddressInfoEXT,
    vendor_infos: *mut RawDeviceFaultVendorInfoEXT,
    vendor_binary_data: *mut u8
}

impl StructureTyped for RawDeviceFaultInfoEXT<'_> {}

impl Default for SType<RawDeviceFaultInfoEXT<'_>> {
    fn default() -> Self {
        SType {
            s_type: RawStructureType::eDeviceFaultInfoExt,
            _phantom: Default::default()
        }
    }
}

impl OutStructure for RawDeviceFaultInfoEXT<'_> {}

pub struct RawDeviceFaultCountsEXT<'f_p_next> {
    s_type: SType<Self>,
    p_next: Option<NextMutPtr<'f_p_next>>,
    address_info_count: u32,
    vendor_info_count: u32,
    vendor_binary_size: DeviceSize
}

impl StructureTyped for RawDeviceFaultCountsEXT<'_> {}

impl Default for SType<RawDeviceFaultCountsEXT<'_>> {
    fn default() -> Self {
        SType {
            s_type: RawStructureType::eDeviceFaultCountsExt,
            _phantom: Default::default()
        }
    }
}

impl OutStructure for RawDeviceFaultCountsEXT<'_> {}

impl Device {
    /// SUCCESS_CODES = [[SuccessCode::eSuccess], [SuccessCode::eIncomplete]];
    ///
    /// ERROR_CODES = [[ErrorCode::eErrorOutOfHostMemory]];
    pub fn get_device_fault_info_ext<V: GenericVecGen>(&self) -> core::result::Result<(DeviceFaultInfoEXT<V>, SuccessCode), ErrorCode> {
        // let mut properties = V0::new_with();
        // loop {
        //     let mut property_count = 0;
        //     let result = (self.data.enumerate_instance_extension_properties)(layer_name, core::ptr::NonNull::from(&mut property_count), None);
        //     if result.0 < 0 {
        //         return Err(result.into_error().normalise())
        //     }
        //
        //     properties.resize_with(property_count as usize, || ExtensionProperties::uninit());
        //     let result = (self.data.enumerate_instance_extension_properties)(layer_name, core::ptr::NonNull::from(&mut property_count), properties.first_mut().map(|p| nonnull_maybe_uninit(p)));
        //
        //     if result.0 >= 0 && result != RawResult::eIncomplete {
        //         properties.truncate(property_count as usize);
        //
        //         let properties = properties.into_iter().map(|v| unsafe { v.assume_init() }.normalise()).collect();
        //         return Ok((properties, result.into_success().normalise()))
        //     } else if result.0 < 0 {
        //         return Err(result.into_error().normalise())
        //     }
        // }

        let mut address_infos = V::new_with::<MaybeUninit<RawDeviceFaultAddressInfoEXT>>();
        let mut vendor_infos = V::new_with::<MaybeUninit<RawDeviceFaultVendorInfoEXT>>();
        let mut vendor_binary_data = V::new_with::<u8>();

        loop {
            let mut counts = RawDeviceFaultCountsEXT {
                s_type: Default::default(),
                p_next: None,
                address_info_count: 0,
                vendor_info_count: 0,
                vendor_binary_size: 0,
            };

            let result = (self.data.get_device_fault_info_ext)(self.handle, NonNull::from(&mut counts), None);
            if result.0 < 0 {
                return Err(result.into_error().normalise());
            }

            address_infos.resize_with(counts.address_info_count as usize, || MaybeUninit::uninit());
            vendor_infos.resize_with(counts.vendor_info_count as usize, || MaybeUninit::uninit());
            vendor_binary_data.resize_with(counts.vendor_binary_size as usize, || 0);

            let mut data = RawDeviceFaultInfoEXT {
                s_type: Default::default(),
                p_next: None,
                description: Default::default(),
                address_infos: address_infos.first_mut().map_or(std::ptr::null_mut(), |p| p.as_mut_ptr()),
                vendor_infos: vendor_infos.first_mut().map_or(std::ptr::null_mut(), |p| p.as_mut_ptr()),
                vendor_binary_data: vendor_binary_data.first_mut().map_or(std::ptr::null_mut(), |p| p as *mut _),
            };

            let result = (self.data.get_device_fault_info_ext)(self.handle, NonNull::from(&mut counts), Some(NonNull::from(&mut data)));

            if result.0 >= 0 && result != RawResult::eIncomplete {
                address_infos.truncate(counts.address_info_count as usize);
                vendor_infos.truncate(counts.vendor_info_count as usize);
                vendor_binary_data.truncate(counts.vendor_binary_size as usize);
                
                let address_infos = address_infos.into_iter().map(|v| unsafe { v.assume_init() }.normalise()).collect();
                let vendor_infos = vendor_infos.into_iter().map(|v| unsafe { v.assume_init() }.normalise()).collect();
                
                return Ok((DeviceFaultInfoEXT {
                    description: data.description,
                    address_infos,
                    vendor_infos,
                    vendor_binary_data,
                }, result.into_success().normalise()))
            } else if result.0 < 0 {
                return Err(result.into_error().normalise())
            }
        }

/*        let mut counts = RawDeviceFaultCountsEXT {
            s_type: Default::default(),
            p_next: None,
            address_info_count: 0,
            vendor_info_count: 0,
            vendor_binary_size: 0,
        };

        loop {
            let result = (self.data.get_device_fault_info_ext)(self.handle, NonNull::from(&mut counts), None);
            if result != crate::RawResult::eSuccess {
                return Err(result.into_error().normalise());
            }



            let result = (self.data.get_device_fault_info_ext)(self.handle, NonNull::from(&mut counts), (NonNull::from(&mut data)));
        }*/
    }
}