use std::ops::{Index, IndexMut};

#[cfg(feature = "smallvec")]
use smallvec::smallvec;

pub trait GenericVecGen {
    type V<T>: GenericVec<T>;
    fn new_with<T>() -> Self::V<T>;
    fn init_with<T>(value: T, count: usize) -> Self::V<T> where T: Clone;
}

pub trait GenericVec<T>: Index<usize, Output=T> + IndexMut<usize, Output=T> + IntoIterator<Item=T> + FromIterator<T> + GenericVecGen {
    fn new() -> Self;
    fn init(value: T, count: usize) -> Self where T: Clone;
    fn resize_with<F: FnMut() -> T>(&mut self, new_len: usize, f: F);
    fn truncate(&mut self, len: usize);
    fn first(&self) -> Option<&T>;
    fn first_mut(&mut self) -> Option<&mut T>;
}

pub struct VecType;

impl GenericVecGen for VecType {
    type V<T> = Vec<T>;

    fn new_with<T>() -> Self::V<T> {
        Vec::new()
    }

    fn init_with<T>(value: T, count: usize) -> Self::V<T> where T: Clone {
        vec![value; count]
    }
}

impl<U> GenericVecGen for Vec<U> {
    type V<T> = Vec<T>;

    fn new_with<T>() -> Self::V<T> {
        Vec::new()
    }

    fn init_with<T>(value: T, count: usize) -> Self::V<T> where T: Clone {
        vec![value; count]
    }
}

impl<T> GenericVec<T> for Vec<T> {
    fn new() -> Self {
        Vec::new()
    }

    fn init(value: T, count: usize) -> Self where T: Clone {
        vec![value; count]
    }

    fn resize_with<F: FnMut() -> T>(&mut self, new_len: usize, f: F) {
        self.resize_with(new_len, f)
    }

    fn truncate(&mut self, len: usize) {
        self.truncate(len)
    }

    fn first(&self) -> Option<&T> {
        self.as_slice().first()
    }

    fn first_mut(&mut self) -> Option<&mut T> {
        self.as_mut_slice().first_mut()
    }
}

#[cfg(feature = "smallvec")]
pub struct SmallVecType<const N: usize>;

#[cfg(feature = "smallvec")]
impl<const N: usize> GenericVecGen for SmallVecType<N> {
    type V<T> = smallvec::SmallVec<[T; N]>;

    fn new_with<T>() -> Self::V<T> {
        smallvec::SmallVec::new()
    }

    fn init_with<T>(value: T, count: usize) -> Self::V<T> where T: Clone {
        smallvec![value; count]
    }
}

#[cfg(feature = "smallvec")]
impl<U, const N: usize> GenericVecGen for smallvec::SmallVec<[U; N]> {
    type V<T> = smallvec::SmallVec<[T; N]>;

    fn new_with<T>() -> Self::V<T> {
        smallvec::SmallVec::new()
    }

    fn init_with<T>(value: T, count: usize) -> Self::V<T> where T: Clone {
        smallvec![value; count]
    }
}

#[cfg(feature = "smallvec")]
impl<T, const N: usize> GenericVec<T> for smallvec::SmallVec<[T; N]> {
    fn new() -> Self {
        smallvec::SmallVec::new()
    }

    fn init(value: T, count: usize) -> Self where T: Clone {
        smallvec![value; count]
    }

    fn resize_with<F: FnMut() -> T>(&mut self, new_len: usize, f: F) {
        self.resize_with(new_len, f)
    }

    fn truncate(&mut self, len: usize) {
        self.truncate(len)
    }

    fn first(&self) -> Option<&T> {
        self.as_slice().first()
    }

    fn first_mut(&mut self) -> Option<&mut T> {
        self.as_mut_slice().first_mut()
    }
}