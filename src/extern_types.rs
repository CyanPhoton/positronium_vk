pub use super::*;
// Extern types:
fieldless_debug_derive!(Display);
#[repr(C)]
#[derive(Copy, Clone, Default)]
pub struct Display { _private: [u8; 0] }

pub type VisualID = std::os::raw::c_ulong;
pub type Window = std::os::raw::c_ulong;
pub type RROutput = std::os::raw::c_ulong;
fieldless_debug_derive!(WlDisplay);
#[repr(C)]
#[derive(Copy, Clone, Default)]
pub struct WlDisplay { _private: [u8; 0] }

fieldless_debug_derive!(WlSurface);
#[repr(C)]
#[derive(Copy, Clone, Default)]
pub struct WlSurface { _private: [u8; 0] }

pub type HINSTANCE = *const std::os::raw::c_void;
pub type HWND = *const std::os::raw::c_void;
pub type HMONITOR = *const std::os::raw::c_void;
pub type HANDLE = *const std::os::raw::c_void;
fieldless_debug_derive!(SECURITY_ATTRIBUTES);
#[repr(C)]
#[derive(Copy, Clone, Default)]
pub struct SECURITY_ATTRIBUTES { _private: [u8; 0] }

pub type DWORD = std::os::raw::c_ulong;
pub type LPCWSTR = *const std::os::raw::c_void;
fieldless_debug_derive!(xcb_connection_t);
#[repr(C)]
#[derive(Copy, Clone, Default)]
pub struct xcb_connection_t { _private: [u8; 0] }

pub type xcb_visualid_t = u32;
pub type xcb_window_t = u32;
fieldless_debug_derive!(IDirectFB);
#[repr(C)]
#[derive(Copy, Clone, Default)]
pub struct IDirectFB { _private: [u8; 0] }

fieldless_debug_derive!(IDirectFBSurface);
#[repr(C)]
#[derive(Copy, Clone, Default)]
pub struct IDirectFBSurface { _private: [u8; 0] }

pub type zx_handle_t = u32;
fieldless_debug_derive!(GgpStreamDescriptor);
#[repr(C)]
#[derive(Copy, Clone, Default)]
pub struct GgpStreamDescriptor { _private: [u8; 0] }

fieldless_debug_derive!(GgpFrameToken);
#[repr(C)]
#[derive(Copy, Clone, Default)]
pub struct GgpFrameToken { _private: [u8; 0] }

fieldless_debug_derive!(screen_context);
#[repr(C)]
#[derive(Copy, Clone, Default)]
pub struct screen_context { _private: [u8; 0] }

fieldless_debug_derive!(screen_window);
#[repr(C)]
#[derive(Copy, Clone, Default)]
pub struct screen_window { _private: [u8; 0] }

