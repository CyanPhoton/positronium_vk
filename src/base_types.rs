pub use super::*;
// Base types:
fieldless_debug_derive!(ANativeWindow);
#[repr(C)]
pub struct ANativeWindow { #[doc(hidden)] _private: [u8; 0] }

fieldless_debug_derive!(AHardwareBuffer);
#[repr(C)]
pub struct AHardwareBuffer { #[doc(hidden)] _private: [u8; 0] }

fieldless_debug_derive!(CAMetalLayer);
#[repr(C)]
pub struct CAMetalLayer { #[doc(hidden)] _private: [u8; 0] }

pub type MTLDevice_id = *mut std::ffi::c_void;

pub type MTLCommandQueue_id = *mut std::ffi::c_void;

pub type MTLBuffer_id = *mut std::ffi::c_void;

pub type MTLTexture_id = *mut std::ffi::c_void;

pub type MTLSharedEvent_id = *mut std::ffi::c_void;

pub type IOSurfaceRef = *mut std::ffi::c_void;

pub type SampleMask = u32;

pub type Flags = u32;

pub type Flags64 = u64;

pub type DeviceSize = u64;

pub type DeviceAddress = u64;

pub type RemoteAddressNV = u8;

