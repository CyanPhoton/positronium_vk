use crate::{RawErrorCode, RawResult, RawSuccessCode};

impl RawResult {
    pub(crate) fn into_success(self) -> RawSuccessCode {
        RawSuccessCode(self.0)
    }

    pub(crate) fn into_error(self) -> RawErrorCode {
        RawErrorCode(self.0)
    }
}