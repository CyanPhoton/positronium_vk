pub use super::*;
// Function types/pointers:
pub type InternalAllocationNotification<D> = extern "C" fn(user_data: Option<&'static D>, size: usize, allocation_type: RawInternalAllocationType, allocation_scope: RawSystemAllocationScope);
pub type InternalFreeNotification<D> = extern "C" fn(user_data: Option<&'static D>, size: usize, allocation_type: RawInternalAllocationType, allocation_scope: RawSystemAllocationScope);
pub type ReallocationFunction<D> = extern "C" fn(user_data: Option<&'static D>, original: *mut [u8; 0], size: usize, alignment: usize, allocation_scope: RawSystemAllocationScope) -> *mut [u8; 0];
pub type AllocationFunction<D> = extern "C" fn(user_data: Option<&'static D>, size: usize, alignment: usize, allocation_scope: RawSystemAllocationScope) -> *mut [u8; 0];
pub type FreeFunction<D> = extern "C" fn(user_data: Option<&'static D>, memory: *mut [u8; 0]);
pub type VoidFunction = extern "C" fn();
pub type DebugReportCallbackExt<D> = extern "C" fn(flags: DebugReportFlagsEXT, object_type: RawDebugReportObjectTypeEXT, object: u64, location: usize, message_code: i32, layer_prefix: Option<UnsizedCStr>, message: Option<UnsizedCStr>, user_data: Option<&'static D>) -> Bool32;
pub type DebugUtilsMessengerCallbackExt<D> = extern "C" fn(message_severity: RawDebugUtilsMessageSeverityFlagBitsEXT, message_types: DebugUtilsMessageTypeFlagsEXT, callback_data: Option<&RawDebugUtilsMessengerCallbackDataEXT>, user_data: Option<&'static D>) -> Bool32;
pub type DeviceMemoryReportCallbackExt<D> = extern "C" fn(callback_data: Option<&RawDeviceMemoryReportCallbackDataEXT>, user_data: Option<&'static D>);
pub type GetInstanceProcAddrLunarg = extern "C" fn(instance: InstanceRaw, name: UnsizedCStr) -> VoidFunction;

pub(crate) type AcquireDrmDisplayExt = extern "C" fn(physical_device: PhysicalDeviceRaw, drm_fd: i32, display: DisplayKHRRaw) -> RawResult;
pub(crate) type AcquireFullScreenExclusiveModeExt = extern "C" fn(device: DeviceRaw, swapchain: SwapchainKHRRaw) -> RawResult;
pub(crate) type AcquireNextImage2Khr = extern "C" fn(device: DeviceRaw, acquire_info: core::ptr::NonNull<RawAcquireNextImageInfoKHR>, image_index: core::ptr::NonNull<u32>) -> RawResult;
pub(crate) type AcquireNextImageKhr = extern "C" fn(device: DeviceRaw, swapchain: SwapchainKHRMutRaw, timeout: u64, semaphore: SemaphoreMutRaw, fence: FenceMutRaw, image_index: core::ptr::NonNull<u32>) -> RawResult;
pub(crate) type AcquirePerformanceConfigurationIntel = extern "C" fn(device: DeviceRaw, acquire_info: &RawPerformanceConfigurationAcquireInfoINTEL, configuration: core::ptr::NonNull<PerformanceConfigurationINTELRaw>) -> RawResult;
pub(crate) type AcquireProfilingLockKhr = extern "C" fn(device: DeviceRaw, info: &RawAcquireProfilingLockInfoKHR) -> RawResult;
pub(crate) type AcquireWinrtDisplayNv = extern "C" fn(physical_device: PhysicalDeviceRaw, display: DisplayKHRRaw) -> RawResult;
pub(crate) type AcquireXlibDisplayExt = extern "C" fn(physical_device: PhysicalDeviceRaw, dpy: core::ptr::NonNull<Display>, display: DisplayKHRRaw) -> RawResult;
pub(crate) type AllocateCommandBuffers = extern "C" fn(device: DeviceRaw, allocate_info: core::ptr::NonNull<RawCommandBufferAllocateInfo>, command_buffers: core::ptr::NonNull<CommandBufferRaw>) -> RawResult;
pub(crate) type AllocateDescriptorSets = extern "C" fn(device: DeviceRaw, allocate_info: core::ptr::NonNull<RawDescriptorSetAllocateInfo>, descriptor_sets: core::ptr::NonNull<DescriptorSetRaw>) -> RawResult;
pub(crate) type AllocateMemory = extern "C" fn(device: DeviceRaw, allocate_info: &RawMemoryAllocateInfo, allocator: Option<&RawAllocationCallbacks>, memory: core::ptr::NonNull<DeviceMemoryRaw>) -> RawResult;
pub(crate) type BeginCommandBuffer = extern "C" fn(command_buffer: CommandBufferMutRaw, begin_info: &RawCommandBufferBeginInfo) -> RawResult;
pub(crate) type BindAccelerationStructureMemoryNv = extern "C" fn(device: DeviceRaw, bind_info_count: u32, bind_infos: &RawBindAccelerationStructureMemoryInfoNV) -> RawResult;
pub(crate) type BindBufferMemory = extern "C" fn(device: DeviceRaw, buffer: BufferMutRaw, memory: DeviceMemoryRaw, memory_offset: DeviceSize) -> RawResult;
pub(crate) type BindBufferMemory2 = extern "C" fn(device: DeviceRaw, bind_info_count: u32, bind_infos: &RawBindBufferMemoryInfo) -> RawResult;
pub(crate) type BindImageMemory = extern "C" fn(device: DeviceRaw, image: ImageMutRaw, memory: DeviceMemoryRaw, memory_offset: DeviceSize) -> RawResult;
pub(crate) type BindImageMemory2 = extern "C" fn(device: DeviceRaw, bind_info_count: u32, bind_infos: &RawBindImageMemoryInfo) -> RawResult;
pub(crate) type BindOpticalFlowSessionImageNv = extern "C" fn(device: DeviceRaw, session: OpticalFlowSessionNVRaw, binding_point: RawOpticalFlowSessionBindingPointNV, view: ImageViewRaw, layout: RawImageLayout) -> RawResult;
pub(crate) type BindVideoSessionMemoryKhr = extern "C" fn(device: DeviceRaw, video_session: VideoSessionKHRMutRaw, bind_session_memory_info_count: u32, bind_session_memory_infos: &RawBindVideoSessionMemoryInfoKHR) -> RawResult;
pub(crate) type BuildAccelerationStructuresKhr = extern "C" fn(device: DeviceRaw, deferred_operation: DeferredOperationKHRRaw, info_count: u32, infos: &RawAccelerationStructureBuildGeometryInfoKHR, build_range_infos: &&RawAccelerationStructureBuildRangeInfoKHR) -> RawResult;
pub(crate) type BuildMicromapsExt = extern "C" fn(device: DeviceRaw, deferred_operation: DeferredOperationKHRRaw, info_count: u32, infos: &RawMicromapBuildInfoEXT) -> RawResult;
pub(crate) type CmdBeginConditionalRenderingExt = extern "C" fn(command_buffer: CommandBufferMutRaw, conditional_rendering_begin: &RawConditionalRenderingBeginInfoEXT);
pub(crate) type CmdBeginDebugUtilsLabelExt = extern "C" fn(command_buffer: CommandBufferMutRaw, label_info: &RawDebugUtilsLabelEXT);
pub(crate) type CmdBeginQuery = extern "C" fn(command_buffer: CommandBufferMutRaw, query_pool: QueryPoolRaw, query: u32, flags: QueryControlFlags);
pub(crate) type CmdBeginQueryIndexedExt = extern "C" fn(command_buffer: CommandBufferMutRaw, query_pool: QueryPoolRaw, query: u32, flags: QueryControlFlags, index: u32);
pub(crate) type CmdBeginRenderPass = extern "C" fn(command_buffer: CommandBufferMutRaw, render_pass_begin: &RawRenderPassBeginInfo, contents: RawSubpassContents);
pub(crate) type CmdBeginRenderPass2 = extern "C" fn(command_buffer: CommandBufferMutRaw, render_pass_begin: &RawRenderPassBeginInfo, subpass_begin_info: &RawSubpassBeginInfo);
pub(crate) type CmdBeginRendering = extern "C" fn(command_buffer: CommandBufferMutRaw, rendering_info: &RawRenderingInfo);
pub(crate) type CmdBeginTransformFeedbackExt = extern "C" fn(command_buffer: CommandBufferMutRaw, first_counter_buffer: u32, counter_buffer_count: u32, counter_buffers: Option<&BufferRaw>, counter_buffer_offsets: Option<&DeviceSize>);
pub(crate) type CmdBeginVideoCodingKhr = extern "C" fn(command_buffer: CommandBufferMutRaw, begin_info: &RawVideoBeginCodingInfoKHR);
pub(crate) type CmdBindDescriptorBufferEmbeddedSamplersExt = extern "C" fn(command_buffer: CommandBufferMutRaw, pipeline_bind_point: RawPipelineBindPoint, layout: PipelineLayoutRaw, set: u32);
pub(crate) type CmdBindDescriptorBuffersExt = extern "C" fn(command_buffer: CommandBufferMutRaw, buffer_count: u32, binding_infos: &RawDescriptorBufferBindingInfoEXT);
pub(crate) type CmdBindDescriptorSets = extern "C" fn(command_buffer: CommandBufferMutRaw, pipeline_bind_point: RawPipelineBindPoint, layout: PipelineLayoutRaw, first_set: u32, descriptor_set_count: u32, descriptor_sets: &DescriptorSetRaw, dynamic_offset_count: u32, dynamic_offsets: Option<&u32>);
pub(crate) type CmdBindIndexBuffer = extern "C" fn(command_buffer: CommandBufferMutRaw, buffer: BufferRaw, offset: DeviceSize, index_type: RawIndexType);
pub(crate) type CmdBindInvocationMaskHuawei = extern "C" fn(command_buffer: CommandBufferMutRaw, image_view: ImageViewRaw, image_layout: RawImageLayout);
pub(crate) type CmdBindPipeline = extern "C" fn(command_buffer: CommandBufferMutRaw, pipeline_bind_point: RawPipelineBindPoint, pipeline: PipelineRaw);
pub(crate) type CmdBindPipelineShaderGroupNv = extern "C" fn(command_buffer: CommandBufferMutRaw, pipeline_bind_point: RawPipelineBindPoint, pipeline: PipelineRaw, group_index: u32);
pub(crate) type CmdBindShadingRateImageNv = extern "C" fn(command_buffer: CommandBufferMutRaw, image_view: ImageViewRaw, image_layout: RawImageLayout);
pub(crate) type CmdBindTransformFeedbackBuffersExt = extern "C" fn(command_buffer: CommandBufferMutRaw, first_binding: u32, binding_count: u32, buffers: &BufferRaw, offsets: &DeviceSize, sizes: Option<&DeviceSize>);
pub(crate) type CmdBindVertexBuffers = extern "C" fn(command_buffer: CommandBufferMutRaw, first_binding: u32, binding_count: u32, buffers: &BufferRaw, offsets: &DeviceSize);
pub(crate) type CmdBindVertexBuffers2 = extern "C" fn(command_buffer: CommandBufferMutRaw, first_binding: u32, binding_count: u32, buffers: &BufferRaw, offsets: &DeviceSize, sizes: Option<&DeviceSize>, strides: Option<&DeviceSize>);
pub(crate) type CmdBlitImage = extern "C" fn(command_buffer: CommandBufferMutRaw, src_image: ImageRaw, src_image_layout: RawImageLayout, dst_image: ImageRaw, dst_image_layout: RawImageLayout, region_count: u32, regions: &RawImageBlit, filter: RawFilter);
pub(crate) type CmdBlitImage2 = extern "C" fn(command_buffer: CommandBufferMutRaw, blit_image_info: &RawBlitImageInfo2);
pub(crate) type CmdBuildAccelerationStructureNv = extern "C" fn(command_buffer: CommandBufferMutRaw, info: &RawAccelerationStructureInfoNV, instance_data: BufferRaw, instance_offset: DeviceSize, update: Bool32, dst: AccelerationStructureNVRaw, src: AccelerationStructureNVRaw, scratch: BufferRaw, scratch_offset: DeviceSize);
pub(crate) type CmdBuildAccelerationStructuresIndirectKhr = extern "C" fn(command_buffer: CommandBufferMutRaw, info_count: u32, infos: &RawAccelerationStructureBuildGeometryInfoKHR, indirect_device_addresses: &DeviceAddress, indirect_strides: &u32, max_primitive_counts: &&u32);
pub(crate) type CmdBuildAccelerationStructuresKhr = extern "C" fn(command_buffer: CommandBufferMutRaw, info_count: u32, infos: &RawAccelerationStructureBuildGeometryInfoKHR, build_range_infos: &&RawAccelerationStructureBuildRangeInfoKHR);
pub(crate) type CmdBuildMicromapsExt = extern "C" fn(command_buffer: CommandBufferMutRaw, info_count: u32, infos: &RawMicromapBuildInfoEXT);
pub(crate) type CmdClearAttachments = extern "C" fn(command_buffer: CommandBufferMutRaw, attachment_count: u32, attachments: &RawClearAttachment, rect_count: u32, rects: &RawClearRect);
pub(crate) type CmdClearColourImage = extern "C" fn(command_buffer: CommandBufferMutRaw, image: ImageRaw, image_layout: RawImageLayout, colour: Option<&RawClearColourValue>, range_count: u32, ranges: &RawImageSubresourceRange);
pub(crate) type CmdClearDepthStencilImage = extern "C" fn(command_buffer: CommandBufferMutRaw, image: ImageRaw, image_layout: RawImageLayout, depth_stencil: &RawClearDepthStencilValue, range_count: u32, ranges: &RawImageSubresourceRange);
pub(crate) type CmdControlVideoCodingKhr = extern "C" fn(command_buffer: CommandBufferMutRaw, coding_control_info: &RawVideoCodingControlInfoKHR);
pub(crate) type CmdCopyAccelerationStructureKhr = extern "C" fn(command_buffer: CommandBufferMutRaw, info: &RawCopyAccelerationStructureInfoKHR);
pub(crate) type CmdCopyAccelerationStructureNv = extern "C" fn(command_buffer: CommandBufferMutRaw, dst: AccelerationStructureNVRaw, src: AccelerationStructureNVRaw, mode: RawCopyAccelerationStructureModeKHR);
pub(crate) type CmdCopyAccelerationStructureToMemoryKhr = extern "C" fn(command_buffer: CommandBufferMutRaw, info: &RawCopyAccelerationStructureToMemoryInfoKHR);
pub(crate) type CmdCopyBuffer = extern "C" fn(command_buffer: CommandBufferMutRaw, src_buffer: BufferRaw, dst_buffer: BufferRaw, region_count: u32, regions: &RawBufferCopy);
pub(crate) type CmdCopyBuffer2 = extern "C" fn(command_buffer: CommandBufferMutRaw, copy_buffer_info: &RawCopyBufferInfo2);
pub(crate) type CmdCopyBufferToImage = extern "C" fn(command_buffer: CommandBufferMutRaw, src_buffer: BufferRaw, dst_image: ImageRaw, dst_image_layout: RawImageLayout, region_count: u32, regions: &RawBufferImageCopy);
pub(crate) type CmdCopyBufferToImage2 = extern "C" fn(command_buffer: CommandBufferMutRaw, copy_buffer_to_image_info: &RawCopyBufferToImageInfo2);
pub(crate) type CmdCopyImage = extern "C" fn(command_buffer: CommandBufferMutRaw, src_image: ImageRaw, src_image_layout: RawImageLayout, dst_image: ImageRaw, dst_image_layout: RawImageLayout, region_count: u32, regions: &RawImageCopy);
pub(crate) type CmdCopyImage2 = extern "C" fn(command_buffer: CommandBufferMutRaw, copy_image_info: &RawCopyImageInfo2);
pub(crate) type CmdCopyImageToBuffer = extern "C" fn(command_buffer: CommandBufferMutRaw, src_image: ImageRaw, src_image_layout: RawImageLayout, dst_buffer: BufferRaw, region_count: u32, regions: &RawBufferImageCopy);
pub(crate) type CmdCopyImageToBuffer2 = extern "C" fn(command_buffer: CommandBufferMutRaw, copy_image_to_buffer_info: &RawCopyImageToBufferInfo2);
pub(crate) type CmdCopyMemoryIndirectNv = extern "C" fn(command_buffer: CommandBufferMutRaw, copy_buffer_address: DeviceAddress, copy_count: u32, stride: u32);
pub(crate) type CmdCopyMemoryToAccelerationStructureKhr = extern "C" fn(command_buffer: CommandBufferMutRaw, info: &RawCopyMemoryToAccelerationStructureInfoKHR);
pub(crate) type CmdCopyMemoryToImageIndirectNv = extern "C" fn(command_buffer: CommandBufferMutRaw, copy_buffer_address: DeviceAddress, copy_count: u32, stride: u32, dst_image: ImageRaw, dst_image_layout: RawImageLayout, image_subresources: &RawImageSubresourceLayers);
pub(crate) type CmdCopyMemoryToMicromapExt = extern "C" fn(command_buffer: CommandBufferMutRaw, info: &RawCopyMemoryToMicromapInfoEXT);
pub(crate) type CmdCopyMicromapExt = extern "C" fn(command_buffer: CommandBufferMutRaw, info: &RawCopyMicromapInfoEXT);
pub(crate) type CmdCopyMicromapToMemoryExt = extern "C" fn(command_buffer: CommandBufferMutRaw, info: &RawCopyMicromapToMemoryInfoEXT);
pub(crate) type CmdCopyQueryPoolResults = extern "C" fn(command_buffer: CommandBufferMutRaw, query_pool: QueryPoolRaw, first_query: u32, query_count: u32, dst_buffer: BufferRaw, dst_offset: DeviceSize, stride: DeviceSize, flags: QueryResultFlags);
pub(crate) type CmdCuLaunchKernelNvx = extern "C" fn(command_buffer: CommandBufferRaw, launch_info: &RawCuLaunchInfoNVX);
pub(crate) type CmdDebugMarkerBeginExt = extern "C" fn(command_buffer: CommandBufferMutRaw, marker_info: &RawDebugMarkerMarkerInfoEXT);
pub(crate) type CmdDebugMarkerEndExt = extern "C" fn(command_buffer: CommandBufferMutRaw);
pub(crate) type CmdDebugMarkerInsertExt = extern "C" fn(command_buffer: CommandBufferMutRaw, marker_info: &RawDebugMarkerMarkerInfoEXT);
pub(crate) type CmdDecodeVideoKhr = extern "C" fn(command_buffer: CommandBufferMutRaw, decode_info: &RawVideoDecodeInfoKHR);
pub(crate) type CmdDecompressMemoryIndirectCountNv = extern "C" fn(command_buffer: CommandBufferMutRaw, indirect_commands_address: DeviceAddress, indirect_commands_count_address: DeviceAddress, stride: u32);
pub(crate) type CmdDecompressMemoryNv = extern "C" fn(command_buffer: CommandBufferMutRaw, decompress_region_count: u32, decompress_memory_regions: &RawDecompressMemoryRegionNV);
pub(crate) type CmdDispatch = extern "C" fn(command_buffer: CommandBufferMutRaw, group_count_x: u32, group_count_y: u32, group_count_z: u32);
pub(crate) type CmdDispatchBase = extern "C" fn(command_buffer: CommandBufferMutRaw, base_group_x: u32, base_group_y: u32, base_group_z: u32, group_count_x: u32, group_count_y: u32, group_count_z: u32);
pub(crate) type CmdDispatchIndirect = extern "C" fn(command_buffer: CommandBufferMutRaw, buffer: BufferRaw, offset: DeviceSize);
pub(crate) type CmdDraw = extern "C" fn(command_buffer: CommandBufferMutRaw, vertex_count: u32, instance_count: u32, first_vertex: u32, first_instance: u32);
pub(crate) type CmdDrawClusterHuawei = extern "C" fn(command_buffer: CommandBufferMutRaw, group_count_x: u32, group_count_y: u32, group_count_z: u32);
pub(crate) type CmdDrawClusterIndirectHuawei = extern "C" fn(command_buffer: CommandBufferMutRaw, buffer: BufferRaw, offset: DeviceSize);
pub(crate) type CmdDrawIndexed = extern "C" fn(command_buffer: CommandBufferMutRaw, index_count: u32, instance_count: u32, first_index: u32, vertex_offset: i32, first_instance: u32);
pub(crate) type CmdDrawIndexedIndirect = extern "C" fn(command_buffer: CommandBufferMutRaw, buffer: BufferRaw, offset: DeviceSize, draw_count: u32, stride: u32);
pub(crate) type CmdDrawIndexedIndirectCount = extern "C" fn(command_buffer: CommandBufferMutRaw, buffer: BufferRaw, offset: DeviceSize, count_buffer: BufferRaw, count_buffer_offset: DeviceSize, max_draw_count: u32, stride: u32);
pub(crate) type CmdDrawIndirect = extern "C" fn(command_buffer: CommandBufferMutRaw, buffer: BufferRaw, offset: DeviceSize, draw_count: u32, stride: u32);
pub(crate) type CmdDrawIndirectByteCountExt = extern "C" fn(command_buffer: CommandBufferMutRaw, instance_count: u32, first_instance: u32, counter_buffer: BufferRaw, counter_buffer_offset: DeviceSize, counter_offset: u32, vertex_stride: u32);
pub(crate) type CmdDrawIndirectCount = extern "C" fn(command_buffer: CommandBufferMutRaw, buffer: BufferRaw, offset: DeviceSize, count_buffer: BufferRaw, count_buffer_offset: DeviceSize, max_draw_count: u32, stride: u32);
pub(crate) type CmdDrawMeshTasksExt = extern "C" fn(command_buffer: CommandBufferMutRaw, group_count_x: u32, group_count_y: u32, group_count_z: u32);
pub(crate) type CmdDrawMeshTasksIndirectCountExt = extern "C" fn(command_buffer: CommandBufferMutRaw, buffer: BufferRaw, offset: DeviceSize, count_buffer: BufferRaw, count_buffer_offset: DeviceSize, max_draw_count: u32, stride: u32);
pub(crate) type CmdDrawMeshTasksIndirectCountNv = extern "C" fn(command_buffer: CommandBufferMutRaw, buffer: BufferRaw, offset: DeviceSize, count_buffer: BufferRaw, count_buffer_offset: DeviceSize, max_draw_count: u32, stride: u32);
pub(crate) type CmdDrawMeshTasksIndirectExt = extern "C" fn(command_buffer: CommandBufferMutRaw, buffer: BufferRaw, offset: DeviceSize, draw_count: u32, stride: u32);
pub(crate) type CmdDrawMeshTasksIndirectNv = extern "C" fn(command_buffer: CommandBufferMutRaw, buffer: BufferRaw, offset: DeviceSize, draw_count: u32, stride: u32);
pub(crate) type CmdDrawMeshTasksNv = extern "C" fn(command_buffer: CommandBufferMutRaw, task_count: u32, first_task: u32);
pub(crate) type CmdDrawMultiExt = extern "C" fn(command_buffer: CommandBufferMutRaw, draw_count: u32, vertex_info: Option<&RawMultiDrawInfoEXT>, instance_count: u32, first_instance: u32, stride: u32);
pub(crate) type CmdDrawMultiIndexedExt = extern "C" fn(command_buffer: CommandBufferMutRaw, draw_count: u32, index_info: Option<&RawMultiDrawIndexedInfoEXT>, instance_count: u32, first_instance: u32, stride: u32, vertex_offset: Option<&i32>);
pub(crate) type CmdEndConditionalRenderingExt = extern "C" fn(command_buffer: CommandBufferMutRaw);
pub(crate) type CmdEndDebugUtilsLabelExt = extern "C" fn(command_buffer: CommandBufferMutRaw);
pub(crate) type CmdEndQuery = extern "C" fn(command_buffer: CommandBufferMutRaw, query_pool: QueryPoolRaw, query: u32);
pub(crate) type CmdEndQueryIndexedExt = extern "C" fn(command_buffer: CommandBufferMutRaw, query_pool: QueryPoolRaw, query: u32, index: u32);
pub(crate) type CmdEndRenderPass = extern "C" fn(command_buffer: CommandBufferMutRaw);
pub(crate) type CmdEndRenderPass2 = extern "C" fn(command_buffer: CommandBufferMutRaw, subpass_end_info: &RawSubpassEndInfo);
pub(crate) type CmdEndRendering = extern "C" fn(command_buffer: CommandBufferMutRaw);
pub(crate) type CmdEndTransformFeedbackExt = extern "C" fn(command_buffer: CommandBufferMutRaw, first_counter_buffer: u32, counter_buffer_count: u32, counter_buffers: Option<&BufferRaw>, counter_buffer_offsets: Option<&DeviceSize>);
pub(crate) type CmdEndVideoCodingKhr = extern "C" fn(command_buffer: CommandBufferMutRaw, end_coding_info: &RawVideoEndCodingInfoKHR);
pub(crate) type CmdExecuteCommands = extern "C" fn(command_buffer: CommandBufferMutRaw, command_buffer_count: u32, command_buffers: &CommandBufferRaw);
pub(crate) type CmdExecuteGeneratedCommandsNv = extern "C" fn(command_buffer: CommandBufferMutRaw, is_preprocessed: Bool32, generated_commands_info: &RawGeneratedCommandsInfoNV);
pub(crate) type CmdFillBuffer = extern "C" fn(command_buffer: CommandBufferMutRaw, dst_buffer: BufferRaw, dst_offset: DeviceSize, size: DeviceSize, data: u32);
pub(crate) type CmdInsertDebugUtilsLabelExt = extern "C" fn(command_buffer: CommandBufferMutRaw, label_info: &RawDebugUtilsLabelEXT);
pub(crate) type CmdNextSubpass = extern "C" fn(command_buffer: CommandBufferMutRaw, contents: RawSubpassContents);
pub(crate) type CmdNextSubpass2 = extern "C" fn(command_buffer: CommandBufferMutRaw, subpass_begin_info: &RawSubpassBeginInfo, subpass_end_info: &RawSubpassEndInfo);
pub(crate) type CmdOpticalFlowExecuteNv = extern "C" fn(command_buffer: CommandBufferRaw, session: OpticalFlowSessionNVRaw, execute_info: &RawOpticalFlowExecuteInfoNV);
pub(crate) type CmdPipelineBarrier = extern "C" fn(command_buffer: CommandBufferMutRaw, src_stage_mask: PipelineStageFlags, dst_stage_mask: PipelineStageFlags, dependency_flags: DependencyFlags, memory_barrier_count: u32, memory_barriers: Option<&RawMemoryBarrier>, buffer_memory_barrier_count: u32, buffer_memory_barriers: Option<&RawBufferMemoryBarrier>, image_memory_barrier_count: u32, image_memory_barriers: Option<&RawImageMemoryBarrier>);
pub(crate) type CmdPipelineBarrier2 = extern "C" fn(command_buffer: CommandBufferMutRaw, dependency_info: &RawDependencyInfo);
pub(crate) type CmdPreprocessGeneratedCommandsNv = extern "C" fn(command_buffer: CommandBufferMutRaw, generated_commands_info: &RawGeneratedCommandsInfoNV);
pub(crate) type CmdPushConstants = extern "C" fn(command_buffer: CommandBufferMutRaw, layout: PipelineLayoutRaw, stage_flags: ShaderStageFlags, offset: u32, size: u32, values: &u8);
pub(crate) type CmdPushDescriptorSetKhr = extern "C" fn(command_buffer: CommandBufferMutRaw, pipeline_bind_point: RawPipelineBindPoint, layout: PipelineLayoutRaw, set: u32, descriptor_write_count: u32, descriptor_writes: &RawWriteDescriptorSet);
pub(crate) type CmdPushDescriptorSetWithTemplateKhr = extern "C" fn(command_buffer: CommandBufferMutRaw, descriptor_update_template: DescriptorUpdateTemplateRaw, layout: PipelineLayoutRaw, set: u32, data: Option<&u8>);
pub(crate) type CmdResetEvent = extern "C" fn(command_buffer: CommandBufferMutRaw, event: EventRaw, stage_mask: PipelineStageFlags);
pub(crate) type CmdResetEvent2 = extern "C" fn(command_buffer: CommandBufferMutRaw, event: EventRaw, stage_mask: PipelineStageFlags2);
pub(crate) type CmdResetQueryPool = extern "C" fn(command_buffer: CommandBufferMutRaw, query_pool: QueryPoolRaw, first_query: u32, query_count: u32);
pub(crate) type CmdResolveImage = extern "C" fn(command_buffer: CommandBufferMutRaw, src_image: ImageRaw, src_image_layout: RawImageLayout, dst_image: ImageRaw, dst_image_layout: RawImageLayout, region_count: u32, regions: &RawImageResolve);
pub(crate) type CmdResolveImage2 = extern "C" fn(command_buffer: CommandBufferMutRaw, resolve_image_info: &RawResolveImageInfo2);
pub(crate) type CmdSetAlphaToCoverageEnableExt = extern "C" fn(command_buffer: CommandBufferMutRaw, alpha_to_coverage_enable: Bool32);
pub(crate) type CmdSetAlphaToOneEnableExt = extern "C" fn(command_buffer: CommandBufferMutRaw, alpha_to_one_enable: Bool32);
pub(crate) type CmdSetBlendConstants = extern "C" fn(command_buffer: CommandBufferMutRaw, blend_constants: ConstSizePtr<f32, 4>);
pub(crate) type CmdSetCheckpointNv = extern "C" fn(command_buffer: CommandBufferMutRaw, checkpoint_marker: Option<&u8>);
pub(crate) type CmdSetCoarseSampleOrderNv = extern "C" fn(command_buffer: CommandBufferMutRaw, sample_order_type: RawCoarseSampleOrderTypeNV, custom_sample_order_count: u32, custom_sample_orders: Option<&RawCoarseSampleOrderCustomNV>);
pub(crate) type CmdSetColourBlendAdvancedExt = extern "C" fn(command_buffer: CommandBufferMutRaw, first_attachment: u32, attachment_count: u32, colour_blend_advanced: &RawColourBlendAdvancedEXT);
pub(crate) type CmdSetColourBlendEnableExt = extern "C" fn(command_buffer: CommandBufferMutRaw, first_attachment: u32, attachment_count: u32, colour_blend_enables: &Bool32);
pub(crate) type CmdSetColourBlendEquationExt = extern "C" fn(command_buffer: CommandBufferMutRaw, first_attachment: u32, attachment_count: u32, colour_blend_equations: &RawColourBlendEquationEXT);
pub(crate) type CmdSetColourWriteEnableExt = extern "C" fn(command_buffer: CommandBufferMutRaw, attachment_count: u32, colour_write_enables: &Bool32);
pub(crate) type CmdSetColourWriteMaskExt = extern "C" fn(command_buffer: CommandBufferMutRaw, first_attachment: u32, attachment_count: u32, colour_write_masks: &ColourComponentFlags);
pub(crate) type CmdSetConservativeRasterizationModeExt = extern "C" fn(command_buffer: CommandBufferMutRaw, conservative_rasterization_mode: RawConservativeRasterizationModeEXT);
pub(crate) type CmdSetCoverageModulationModeNv = extern "C" fn(command_buffer: CommandBufferMutRaw, coverage_modulation_mode: RawCoverageModulationModeNV);
pub(crate) type CmdSetCoverageModulationTableEnableNv = extern "C" fn(command_buffer: CommandBufferMutRaw, coverage_modulation_table_enable: Bool32);
pub(crate) type CmdSetCoverageModulationTableNv = extern "C" fn(command_buffer: CommandBufferMutRaw, coverage_modulation_table_count: u32, coverage_modulation_table: &f32);
pub(crate) type CmdSetCoverageReductionModeNv = extern "C" fn(command_buffer: CommandBufferMutRaw, coverage_reduction_mode: RawCoverageReductionModeNV);
pub(crate) type CmdSetCoverageToColourEnableNv = extern "C" fn(command_buffer: CommandBufferMutRaw, coverage_to_colour_enable: Bool32);
pub(crate) type CmdSetCoverageToColourLocationNv = extern "C" fn(command_buffer: CommandBufferMutRaw, coverage_to_colour_location: u32);
pub(crate) type CmdSetCullMode = extern "C" fn(command_buffer: CommandBufferMutRaw, cull_mode: CullModeFlags);
pub(crate) type CmdSetDepthBias = extern "C" fn(command_buffer: CommandBufferMutRaw, depth_bias_constant_factor: f32, depth_bias_clamp: f32, depth_bias_slope_factor: f32);
pub(crate) type CmdSetDepthBiasEnable = extern "C" fn(command_buffer: CommandBufferMutRaw, depth_bias_enable: Bool32);
pub(crate) type CmdSetDepthBounds = extern "C" fn(command_buffer: CommandBufferMutRaw, min_depth_bounds: f32, max_depth_bounds: f32);
pub(crate) type CmdSetDepthBoundsTestEnable = extern "C" fn(command_buffer: CommandBufferMutRaw, depth_bounds_test_enable: Bool32);
pub(crate) type CmdSetDepthClampEnableExt = extern "C" fn(command_buffer: CommandBufferMutRaw, depth_clamp_enable: Bool32);
pub(crate) type CmdSetDepthClipEnableExt = extern "C" fn(command_buffer: CommandBufferMutRaw, depth_clip_enable: Bool32);
pub(crate) type CmdSetDepthClipNegativeOneToOneExt = extern "C" fn(command_buffer: CommandBufferMutRaw, negative_one_to_one: Bool32);
pub(crate) type CmdSetDepthCompareOp = extern "C" fn(command_buffer: CommandBufferMutRaw, depth_compare_op: RawCompareOp);
pub(crate) type CmdSetDepthTestEnable = extern "C" fn(command_buffer: CommandBufferMutRaw, depth_test_enable: Bool32);
pub(crate) type CmdSetDepthWriteEnable = extern "C" fn(command_buffer: CommandBufferMutRaw, depth_write_enable: Bool32);
pub(crate) type CmdSetDescriptorBufferOffsetsExt = extern "C" fn(command_buffer: CommandBufferMutRaw, pipeline_bind_point: RawPipelineBindPoint, layout: PipelineLayoutRaw, first_set: u32, set_count: u32, buffer_indices: &u32, offsets: &DeviceSize);
pub(crate) type CmdSetDeviceMask = extern "C" fn(command_buffer: CommandBufferMutRaw, device_mask: u32);
pub(crate) type CmdSetDiscardRectangleExt = extern "C" fn(command_buffer: CommandBufferMutRaw, first_discard_rectangle: u32, discard_rectangle_count: u32, discard_rectangles: &RawRect2D);
pub(crate) type CmdSetDiscardRectangleEnableExt = extern "C" fn(command_buffer: CommandBufferMutRaw, discard_rectangle_enable: Bool32);
pub(crate) type CmdSetDiscardRectangleModeExt = extern "C" fn(command_buffer: CommandBufferMutRaw, discard_rectangle_mode: RawDiscardRectangleModeEXT);
pub(crate) type CmdSetEvent = extern "C" fn(command_buffer: CommandBufferMutRaw, event: EventRaw, stage_mask: PipelineStageFlags);
pub(crate) type CmdSetEvent2 = extern "C" fn(command_buffer: CommandBufferMutRaw, event: EventRaw, dependency_info: &RawDependencyInfo);
pub(crate) type CmdSetExclusiveScissorEnableNv = extern "C" fn(command_buffer: CommandBufferMutRaw, first_exclusive_scissor: u32, exclusive_scissor_count: u32, exclusive_scissor_enables: &Bool32);
pub(crate) type CmdSetExclusiveScissorNv = extern "C" fn(command_buffer: CommandBufferMutRaw, first_exclusive_scissor: u32, exclusive_scissor_count: u32, exclusive_scissors: &RawRect2D);
pub(crate) type CmdSetExtraPrimitiveOverestimationSizeExt = extern "C" fn(command_buffer: CommandBufferMutRaw, extra_primitive_overestimation_size: f32);
pub(crate) type CmdSetFragmentShadingRateEnumNv = extern "C" fn(command_buffer: CommandBufferMutRaw, shading_rate: RawFragmentShadingRateNV, combiner_ops: ConstSizePtr<RawFragmentShadingRateCombinerOpKHR, 2>);
pub(crate) type CmdSetFragmentShadingRateKhr = extern "C" fn(command_buffer: CommandBufferMutRaw, fragment_size: &RawExtent2D, combiner_ops: ConstSizePtr<RawFragmentShadingRateCombinerOpKHR, 2>);
pub(crate) type CmdSetFrontFace = extern "C" fn(command_buffer: CommandBufferMutRaw, front_face: RawFrontFace);
pub(crate) type CmdSetLineRasterizationModeExt = extern "C" fn(command_buffer: CommandBufferMutRaw, line_rasterization_mode: RawLineRasterizationModeEXT);
pub(crate) type CmdSetLineStippleExt = extern "C" fn(command_buffer: CommandBufferMutRaw, line_stipple_factor: u32, line_stipple_pattern: u16);
pub(crate) type CmdSetLineStippleEnableExt = extern "C" fn(command_buffer: CommandBufferMutRaw, stippled_line_enable: Bool32);
pub(crate) type CmdSetLineWidth = extern "C" fn(command_buffer: CommandBufferMutRaw, line_width: f32);
pub(crate) type CmdSetLogicOpExt = extern "C" fn(command_buffer: CommandBufferMutRaw, logic_op: RawLogicOp);
pub(crate) type CmdSetLogicOpEnableExt = extern "C" fn(command_buffer: CommandBufferMutRaw, logic_op_enable: Bool32);
pub(crate) type CmdSetPatchControlPointsExt = extern "C" fn(command_buffer: CommandBufferMutRaw, patch_control_points: u32);
pub(crate) type CmdSetPerformanceMarkerIntel = extern "C" fn(command_buffer: CommandBufferMutRaw, marker_info: &RawPerformanceMarkerInfoINTEL) -> RawResult;
pub(crate) type CmdSetPerformanceOverrideIntel = extern "C" fn(command_buffer: CommandBufferMutRaw, override_info: &RawPerformanceOverrideInfoINTEL) -> RawResult;
pub(crate) type CmdSetPerformanceStreamMarkerIntel = extern "C" fn(command_buffer: CommandBufferMutRaw, marker_info: &RawPerformanceStreamMarkerInfoINTEL) -> RawResult;
pub(crate) type CmdSetPolygonModeExt = extern "C" fn(command_buffer: CommandBufferMutRaw, polygon_mode: RawPolygonMode);
pub(crate) type CmdSetPrimitiveRestartEnable = extern "C" fn(command_buffer: CommandBufferMutRaw, primitive_restart_enable: Bool32);
pub(crate) type CmdSetPrimitiveTopology = extern "C" fn(command_buffer: CommandBufferMutRaw, primitive_topology: RawPrimitiveTopology);
pub(crate) type CmdSetProvokingVertexModeExt = extern "C" fn(command_buffer: CommandBufferMutRaw, provoking_vertex_mode: RawProvokingVertexModeEXT);
pub(crate) type CmdSetRasterizationSamplesExt = extern "C" fn(command_buffer: CommandBufferMutRaw, rasterization_samples: RawSampleCountFlagBits);
pub(crate) type CmdSetRasterizationStreamExt = extern "C" fn(command_buffer: CommandBufferMutRaw, rasterization_stream: u32);
pub(crate) type CmdSetRasterizerDiscardEnable = extern "C" fn(command_buffer: CommandBufferMutRaw, rasterizer_discard_enable: Bool32);
pub(crate) type CmdSetRayTracingPipelineStackSizeKhr = extern "C" fn(command_buffer: CommandBufferMutRaw, pipeline_stack_size: u32);
pub(crate) type CmdSetRepresentativeFragmentTestEnableNv = extern "C" fn(command_buffer: CommandBufferMutRaw, representative_fragment_test_enable: Bool32);
pub(crate) type CmdSetSampleLocationsExt = extern "C" fn(command_buffer: CommandBufferMutRaw, sample_locations_info: &RawSampleLocationsInfoEXT);
pub(crate) type CmdSetSampleLocationsEnableExt = extern "C" fn(command_buffer: CommandBufferMutRaw, sample_locations_enable: Bool32);
pub(crate) type CmdSetSampleMaskExt = extern "C" fn(command_buffer: CommandBufferMutRaw, samples: RawSampleCountFlagBits, sample_mask: &SampleMask);
pub(crate) type CmdSetScissor = extern "C" fn(command_buffer: CommandBufferMutRaw, first_scissor: u32, scissor_count: u32, scissors: &RawRect2D);
pub(crate) type CmdSetScissorWithCount = extern "C" fn(command_buffer: CommandBufferMutRaw, scissor_count: u32, scissors: &RawRect2D);
pub(crate) type CmdSetShadingRateImageEnableNv = extern "C" fn(command_buffer: CommandBufferMutRaw, shading_rate_image_enable: Bool32);
pub(crate) type CmdSetStencilCompareMask = extern "C" fn(command_buffer: CommandBufferMutRaw, face_mask: StencilFaceFlags, compare_mask: u32);
pub(crate) type CmdSetStencilOp = extern "C" fn(command_buffer: CommandBufferMutRaw, face_mask: StencilFaceFlags, fail_op: RawStencilOp, pass_op: RawStencilOp, depth_fail_op: RawStencilOp, compare_op: RawCompareOp);
pub(crate) type CmdSetStencilReference = extern "C" fn(command_buffer: CommandBufferMutRaw, face_mask: StencilFaceFlags, reference: u32);
pub(crate) type CmdSetStencilTestEnable = extern "C" fn(command_buffer: CommandBufferMutRaw, stencil_test_enable: Bool32);
pub(crate) type CmdSetStencilWriteMask = extern "C" fn(command_buffer: CommandBufferMutRaw, face_mask: StencilFaceFlags, write_mask: u32);
pub(crate) type CmdSetTessellationDomainOriginExt = extern "C" fn(command_buffer: CommandBufferMutRaw, domain_origin: RawTessellationDomainOrigin);
pub(crate) type CmdSetVertexInputExt = extern "C" fn(command_buffer: CommandBufferMutRaw, vertex_binding_description_count: u32, vertex_binding_descriptions: Option<&RawVertexInputBindingDescription2EXT>, vertex_attribute_description_count: u32, vertex_attribute_descriptions: Option<&RawVertexInputAttributeDescription2EXT>);
pub(crate) type CmdSetViewport = extern "C" fn(command_buffer: CommandBufferMutRaw, first_viewport: u32, viewport_count: u32, viewports: &RawViewport);
pub(crate) type CmdSetViewportShadingRatePaletteNv = extern "C" fn(command_buffer: CommandBufferMutRaw, first_viewport: u32, viewport_count: u32, shading_rate_palettes: &RawShadingRatePaletteNV);
pub(crate) type CmdSetViewportSwizzleNv = extern "C" fn(command_buffer: CommandBufferMutRaw, first_viewport: u32, viewport_count: u32, viewport_swizzles: &RawViewportSwizzleNV);
pub(crate) type CmdSetViewportWScalingEnableNv = extern "C" fn(command_buffer: CommandBufferMutRaw, viewport_w_scaling_enable: Bool32);
pub(crate) type CmdSetViewportWScalingNv = extern "C" fn(command_buffer: CommandBufferMutRaw, first_viewport: u32, viewport_count: u32, viewport_w_scalings: &RawViewportWScalingNV);
pub(crate) type CmdSetViewportWithCount = extern "C" fn(command_buffer: CommandBufferMutRaw, viewport_count: u32, viewports: &RawViewport);
pub(crate) type CmdSubpassShadingHuawei = extern "C" fn(command_buffer: CommandBufferMutRaw);
pub(crate) type CmdTraceRaysIndirect2Khr = extern "C" fn(command_buffer: CommandBufferMutRaw, indirect_device_address: DeviceAddress);
pub(crate) type CmdTraceRaysIndirectKhr = extern "C" fn(command_buffer: CommandBufferMutRaw, raygen_shader_binding_table: &RawStridedDeviceAddressRegionKHR, miss_shader_binding_table: &RawStridedDeviceAddressRegionKHR, hit_shader_binding_table: &RawStridedDeviceAddressRegionKHR, callable_shader_binding_table: &RawStridedDeviceAddressRegionKHR, indirect_device_address: DeviceAddress);
pub(crate) type CmdTraceRaysKhr = extern "C" fn(command_buffer: CommandBufferMutRaw, raygen_shader_binding_table: &RawStridedDeviceAddressRegionKHR, miss_shader_binding_table: &RawStridedDeviceAddressRegionKHR, hit_shader_binding_table: &RawStridedDeviceAddressRegionKHR, callable_shader_binding_table: &RawStridedDeviceAddressRegionKHR, width: u32, height: u32, depth: u32);
pub(crate) type CmdTraceRaysNv = extern "C" fn(command_buffer: CommandBufferMutRaw, raygen_shader_binding_table_buffer: BufferRaw, raygen_shader_binding_offset: DeviceSize, miss_shader_binding_table_buffer: BufferRaw, miss_shader_binding_offset: DeviceSize, miss_shader_binding_stride: DeviceSize, hit_shader_binding_table_buffer: BufferRaw, hit_shader_binding_offset: DeviceSize, hit_shader_binding_stride: DeviceSize, callable_shader_binding_table_buffer: BufferRaw, callable_shader_binding_offset: DeviceSize, callable_shader_binding_stride: DeviceSize, width: u32, height: u32, depth: u32);
pub(crate) type CmdUpdateBuffer = extern "C" fn(command_buffer: CommandBufferMutRaw, dst_buffer: BufferRaw, dst_offset: DeviceSize, data_size: DeviceSize, data: &u8);
pub(crate) type CmdWaitEvents = extern "C" fn(command_buffer: CommandBufferMutRaw, event_count: u32, events: &EventRaw, src_stage_mask: PipelineStageFlags, dst_stage_mask: PipelineStageFlags, memory_barrier_count: u32, memory_barriers: Option<&RawMemoryBarrier>, buffer_memory_barrier_count: u32, buffer_memory_barriers: Option<&RawBufferMemoryBarrier>, image_memory_barrier_count: u32, image_memory_barriers: Option<&RawImageMemoryBarrier>);
pub(crate) type CmdWaitEvents2 = extern "C" fn(command_buffer: CommandBufferMutRaw, event_count: u32, events: &EventRaw, dependency_infos: &RawDependencyInfo);
pub(crate) type CmdWriteAccelerationStructuresPropertiesKhr = extern "C" fn(command_buffer: CommandBufferMutRaw, acceleration_structure_count: u32, acceleration_structures: &AccelerationStructureKHRRaw, query_type: RawQueryType, query_pool: QueryPoolRaw, first_query: u32);
pub(crate) type CmdWriteAccelerationStructuresPropertiesNv = extern "C" fn(command_buffer: CommandBufferMutRaw, acceleration_structure_count: u32, acceleration_structures: &AccelerationStructureNVRaw, query_type: RawQueryType, query_pool: QueryPoolRaw, first_query: u32);
pub(crate) type CmdWriteBufferMarker2Amd = extern "C" fn(command_buffer: CommandBufferMutRaw, stage: PipelineStageFlags2, dst_buffer: BufferRaw, dst_offset: DeviceSize, marker: u32);
pub(crate) type CmdWriteBufferMarkerAmd = extern "C" fn(command_buffer: CommandBufferMutRaw, pipeline_stage: RawPipelineStageFlagBits, dst_buffer: BufferRaw, dst_offset: DeviceSize, marker: u32);
pub(crate) type CmdWriteMicromapsPropertiesExt = extern "C" fn(command_buffer: CommandBufferMutRaw, micromap_count: u32, micromaps: &MicromapEXTRaw, query_type: RawQueryType, query_pool: QueryPoolRaw, first_query: u32);
pub(crate) type CmdWriteTimestamp = extern "C" fn(command_buffer: CommandBufferMutRaw, pipeline_stage: RawPipelineStageFlagBits, query_pool: QueryPoolRaw, query: u32);
pub(crate) type CmdWriteTimestamp2 = extern "C" fn(command_buffer: CommandBufferMutRaw, stage: PipelineStageFlags2, query_pool: QueryPoolRaw, query: u32);
pub(crate) type CompileDeferredNv = extern "C" fn(device: DeviceRaw, pipeline: PipelineRaw, shader: u32) -> RawResult;
pub(crate) type CopyAccelerationStructureKhr = extern "C" fn(device: DeviceRaw, deferred_operation: DeferredOperationKHRRaw, info: &RawCopyAccelerationStructureInfoKHR) -> RawResult;
pub(crate) type CopyAccelerationStructureToMemoryKhr = extern "C" fn(device: DeviceRaw, deferred_operation: DeferredOperationKHRRaw, info: &RawCopyAccelerationStructureToMemoryInfoKHR) -> RawResult;
pub(crate) type CopyMemoryToAccelerationStructureKhr = extern "C" fn(device: DeviceRaw, deferred_operation: DeferredOperationKHRRaw, info: &RawCopyMemoryToAccelerationStructureInfoKHR) -> RawResult;
pub(crate) type CopyMemoryToMicromapExt = extern "C" fn(device: DeviceRaw, deferred_operation: DeferredOperationKHRRaw, info: &RawCopyMemoryToMicromapInfoEXT) -> RawResult;
pub(crate) type CopyMicromapExt = extern "C" fn(device: DeviceRaw, deferred_operation: DeferredOperationKHRRaw, info: &RawCopyMicromapInfoEXT) -> RawResult;
pub(crate) type CopyMicromapToMemoryExt = extern "C" fn(device: DeviceRaw, deferred_operation: DeferredOperationKHRRaw, info: &RawCopyMicromapToMemoryInfoEXT) -> RawResult;
pub(crate) type CreateAccelerationStructureKhr = extern "C" fn(device: DeviceRaw, create_info: &RawAccelerationStructureCreateInfoKHR, allocator: Option<&RawAllocationCallbacks>, acceleration_structure: core::ptr::NonNull<AccelerationStructureKHRRaw>) -> RawResult;
pub(crate) type CreateAccelerationStructureNv = extern "C" fn(device: DeviceRaw, create_info: &RawAccelerationStructureCreateInfoNV, allocator: Option<&RawAllocationCallbacks>, acceleration_structure: core::ptr::NonNull<AccelerationStructureNVRaw>) -> RawResult;
pub(crate) type CreateAndroidSurfaceKhr = extern "C" fn(instance: InstanceRaw, create_info: &RawAndroidSurfaceCreateInfoKHR, allocator: Option<&RawAllocationCallbacks>, surface: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult;
pub(crate) type CreateBuffer = extern "C" fn(device: DeviceRaw, create_info: &RawBufferCreateInfo, allocator: Option<&RawAllocationCallbacks>, buffer: core::ptr::NonNull<BufferRaw>) -> RawResult;
pub(crate) type CreateBufferCollectionFuchsia = extern "C" fn(device: DeviceRaw, create_info: &RawBufferCollectionCreateInfoFUCHSIA, allocator: Option<&RawAllocationCallbacks>, collection: core::ptr::NonNull<BufferCollectionFUCHSIARaw>) -> RawResult;
pub(crate) type CreateBufferView = extern "C" fn(device: DeviceRaw, create_info: &RawBufferViewCreateInfo, allocator: Option<&RawAllocationCallbacks>, view: core::ptr::NonNull<BufferViewRaw>) -> RawResult;
pub(crate) type CreateCommandPool = extern "C" fn(device: DeviceRaw, create_info: &RawCommandPoolCreateInfo, allocator: Option<&RawAllocationCallbacks>, command_pool: core::ptr::NonNull<CommandPoolRaw>) -> RawResult;
pub(crate) type CreateComputePipelines = extern "C" fn(device: DeviceRaw, pipeline_cache: PipelineCacheRaw, create_info_count: u32, create_infos: &RawComputePipelineCreateInfo, allocator: Option<&RawAllocationCallbacks>, pipelines: core::ptr::NonNull<PipelineRaw>) -> RawResult;
pub(crate) type CreateCuFunctionNvx = extern "C" fn(device: DeviceRaw, create_info: &RawCuFunctionCreateInfoNVX, allocator: Option<&RawAllocationCallbacks>, function: core::ptr::NonNull<CuFunctionNVXRaw>) -> RawResult;
pub(crate) type CreateCuModuleNvx = extern "C" fn(device: DeviceRaw, create_info: &RawCuModuleCreateInfoNVX, allocator: Option<&RawAllocationCallbacks>, module: core::ptr::NonNull<CuModuleNVXRaw>) -> RawResult;
pub(crate) type CreateDebugReportCallbackExt = extern "C" fn(instance: InstanceRaw, create_info: &RawDebugReportCallbackCreateInfoEXT, allocator: Option<&RawAllocationCallbacks>, callback: core::ptr::NonNull<DebugReportCallbackEXTRaw>) -> RawResult;
pub(crate) type CreateDebugUtilsMessengerExt = extern "C" fn(instance: InstanceRaw, create_info: &RawDebugUtilsMessengerCreateInfoEXT, allocator: Option<&RawAllocationCallbacks>, messenger: core::ptr::NonNull<DebugUtilsMessengerEXTRaw>) -> RawResult;
pub(crate) type CreateDeferredOperationKhr = extern "C" fn(device: DeviceRaw, allocator: Option<&RawAllocationCallbacks>, deferred_operation: core::ptr::NonNull<DeferredOperationKHRRaw>) -> RawResult;
pub(crate) type CreateDescriptorPool = extern "C" fn(device: DeviceRaw, create_info: &RawDescriptorPoolCreateInfo, allocator: Option<&RawAllocationCallbacks>, descriptor_pool: core::ptr::NonNull<DescriptorPoolRaw>) -> RawResult;
pub(crate) type CreateDescriptorSetLayout = extern "C" fn(device: DeviceRaw, create_info: &RawDescriptorSetLayoutCreateInfo, allocator: Option<&RawAllocationCallbacks>, set_layout: core::ptr::NonNull<DescriptorSetLayoutRaw>) -> RawResult;
pub(crate) type CreateDescriptorUpdateTemplate = extern "C" fn(device: DeviceRaw, create_info: &RawDescriptorUpdateTemplateCreateInfo, allocator: Option<&RawAllocationCallbacks>, descriptor_update_template: core::ptr::NonNull<DescriptorUpdateTemplateRaw>) -> RawResult;
pub(crate) type CreateDevice = extern "C" fn(physical_device: PhysicalDeviceRaw, create_info: &RawDeviceCreateInfo, allocator: Option<&RawAllocationCallbacks>, device: core::ptr::NonNull<DeviceRaw>) -> RawResult;
pub(crate) type CreateDirectFbSurfaceExt = extern "C" fn(instance: InstanceRaw, create_info: &RawDirectFBSurfaceCreateInfoEXT, allocator: Option<&RawAllocationCallbacks>, surface: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult;
pub(crate) type CreateDisplayModeKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, display: DisplayKHRMutRaw, create_info: &RawDisplayModeCreateInfoKHR, allocator: Option<&RawAllocationCallbacks>, mode: core::ptr::NonNull<DisplayModeKHRRaw>) -> RawResult;
pub(crate) type CreateDisplayPlaneSurfaceKhr = extern "C" fn(instance: InstanceRaw, create_info: &RawDisplaySurfaceCreateInfoKHR, allocator: Option<&RawAllocationCallbacks>, surface: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult;
pub(crate) type CreateEvent = extern "C" fn(device: DeviceRaw, create_info: &RawEventCreateInfo, allocator: Option<&RawAllocationCallbacks>, event: core::ptr::NonNull<EventRaw>) -> RawResult;
pub(crate) type CreateFence = extern "C" fn(device: DeviceRaw, create_info: &RawFenceCreateInfo, allocator: Option<&RawAllocationCallbacks>, fence: core::ptr::NonNull<FenceRaw>) -> RawResult;
pub(crate) type CreateFramebuffer = extern "C" fn(device: DeviceRaw, create_info: &RawFramebufferCreateInfo, allocator: Option<&RawAllocationCallbacks>, framebuffer: core::ptr::NonNull<FramebufferRaw>) -> RawResult;
pub(crate) type CreateGraphicsPipelines = extern "C" fn(device: DeviceRaw, pipeline_cache: PipelineCacheRaw, create_info_count: u32, create_infos: &RawGraphicsPipelineCreateInfo, allocator: Option<&RawAllocationCallbacks>, pipelines: core::ptr::NonNull<PipelineRaw>) -> RawResult;
pub(crate) type CreateHeadlessSurfaceExt = extern "C" fn(instance: InstanceRaw, create_info: &RawHeadlessSurfaceCreateInfoEXT, allocator: Option<&RawAllocationCallbacks>, surface: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult;
pub(crate) type CreateIosSurfaceMvk = extern "C" fn(instance: InstanceRaw, create_info: &RawIOSSurfaceCreateInfoMVK, allocator: Option<&RawAllocationCallbacks>, surface: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult;
pub(crate) type CreateImage = extern "C" fn(device: DeviceRaw, create_info: &RawImageCreateInfo, allocator: Option<&RawAllocationCallbacks>, image: core::ptr::NonNull<ImageRaw>) -> RawResult;
pub(crate) type CreateImagePipeSurfaceFuchsia = extern "C" fn(instance: InstanceRaw, create_info: &RawImagePipeSurfaceCreateInfoFUCHSIA, allocator: Option<&RawAllocationCallbacks>, surface: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult;
pub(crate) type CreateImageView = extern "C" fn(device: DeviceRaw, create_info: &RawImageViewCreateInfo, allocator: Option<&RawAllocationCallbacks>, view: core::ptr::NonNull<ImageViewRaw>) -> RawResult;
pub(crate) type CreateIndirectCommandsLayoutNv = extern "C" fn(device: DeviceRaw, create_info: &RawIndirectCommandsLayoutCreateInfoNV, allocator: Option<&RawAllocationCallbacks>, indirect_commands_layout: core::ptr::NonNull<IndirectCommandsLayoutNVRaw>) -> RawResult;
pub(crate) type CreateInstance = extern "C" fn(create_info: &RawInstanceCreateInfo, allocator: Option<&RawAllocationCallbacks>, instance: core::ptr::NonNull<InstanceRaw>) -> RawResult;
pub(crate) type CreateMacOsSurfaceMvk = extern "C" fn(instance: InstanceRaw, create_info: &RawMacOSSurfaceCreateInfoMVK, allocator: Option<&RawAllocationCallbacks>, surface: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult;
pub(crate) type CreateMetalSurfaceExt = extern "C" fn(instance: InstanceRaw, create_info: &RawMetalSurfaceCreateInfoEXT, allocator: Option<&RawAllocationCallbacks>, surface: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult;
pub(crate) type CreateMicromapExt = extern "C" fn(device: DeviceRaw, create_info: &RawMicromapCreateInfoEXT, allocator: Option<&RawAllocationCallbacks>, micromap: core::ptr::NonNull<MicromapEXTRaw>) -> RawResult;
pub(crate) type CreateOpticalFlowSessionNv = extern "C" fn(device: DeviceRaw, create_info: &RawOpticalFlowSessionCreateInfoNV, allocator: Option<&RawAllocationCallbacks>, session: core::ptr::NonNull<OpticalFlowSessionNVRaw>) -> RawResult;
pub(crate) type CreatePipelineCache = extern "C" fn(device: DeviceRaw, create_info: &RawPipelineCacheCreateInfo, allocator: Option<&RawAllocationCallbacks>, pipeline_cache: core::ptr::NonNull<PipelineCacheRaw>) -> RawResult;
pub(crate) type CreatePipelineLayout = extern "C" fn(device: DeviceRaw, create_info: &RawPipelineLayoutCreateInfo, allocator: Option<&RawAllocationCallbacks>, pipeline_layout: core::ptr::NonNull<PipelineLayoutRaw>) -> RawResult;
pub(crate) type CreatePrivateDataSlot = extern "C" fn(device: DeviceRaw, create_info: &RawPrivateDataSlotCreateInfo, allocator: Option<&RawAllocationCallbacks>, private_data_slot: core::ptr::NonNull<PrivateDataSlotRaw>) -> RawResult;
pub(crate) type CreateQueryPool = extern "C" fn(device: DeviceRaw, create_info: &RawQueryPoolCreateInfo, allocator: Option<&RawAllocationCallbacks>, query_pool: core::ptr::NonNull<QueryPoolRaw>) -> RawResult;
pub(crate) type CreateRayTracingPipelinesKhr = extern "C" fn(device: DeviceRaw, deferred_operation: DeferredOperationKHRRaw, pipeline_cache: PipelineCacheRaw, create_info_count: u32, create_infos: &RawRayTracingPipelineCreateInfoKHR, allocator: Option<&RawAllocationCallbacks>, pipelines: core::ptr::NonNull<PipelineRaw>) -> RawResult;
pub(crate) type CreateRayTracingPipelinesNv = extern "C" fn(device: DeviceRaw, pipeline_cache: PipelineCacheRaw, create_info_count: u32, create_infos: &RawRayTracingPipelineCreateInfoNV, allocator: Option<&RawAllocationCallbacks>, pipelines: core::ptr::NonNull<PipelineRaw>) -> RawResult;
pub(crate) type CreateRenderPass = extern "C" fn(device: DeviceRaw, create_info: &RawRenderPassCreateInfo, allocator: Option<&RawAllocationCallbacks>, render_pass: core::ptr::NonNull<RenderPassRaw>) -> RawResult;
pub(crate) type CreateRenderPass2 = extern "C" fn(device: DeviceRaw, create_info: &RawRenderPassCreateInfo2, allocator: Option<&RawAllocationCallbacks>, render_pass: core::ptr::NonNull<RenderPassRaw>) -> RawResult;
pub(crate) type CreateSampler = extern "C" fn(device: DeviceRaw, create_info: &RawSamplerCreateInfo, allocator: Option<&RawAllocationCallbacks>, sampler: core::ptr::NonNull<SamplerRaw>) -> RawResult;
pub(crate) type CreateSamplerYcbcrConversion = extern "C" fn(device: DeviceRaw, create_info: &RawSamplerYcbcrConversionCreateInfo, allocator: Option<&RawAllocationCallbacks>, ycbcr_conversion: core::ptr::NonNull<SamplerYcbcrConversionRaw>) -> RawResult;
pub(crate) type CreateScreenSurfaceQnx = extern "C" fn(instance: InstanceRaw, create_info: &RawScreenSurfaceCreateInfoQNX, allocator: Option<&RawAllocationCallbacks>, surface: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult;
pub(crate) type CreateSemaphore = extern "C" fn(device: DeviceRaw, create_info: &RawSemaphoreCreateInfo, allocator: Option<&RawAllocationCallbacks>, semaphore: core::ptr::NonNull<SemaphoreRaw>) -> RawResult;
pub(crate) type CreateShaderModule = extern "C" fn(device: DeviceRaw, create_info: &RawShaderModuleCreateInfo, allocator: Option<&RawAllocationCallbacks>, shader_module: core::ptr::NonNull<ShaderModuleRaw>) -> RawResult;
pub(crate) type CreateSharedSwapchainsKhr = extern "C" fn(device: DeviceRaw, swapchain_count: u32, create_infos: core::ptr::NonNull<RawSwapchainCreateInfoKHR>, allocator: Option<&RawAllocationCallbacks>, swapchains: core::ptr::NonNull<SwapchainKHRRaw>) -> RawResult;
pub(crate) type CreateStreamDescriptorSurfaceGgp = extern "C" fn(instance: InstanceRaw, create_info: &RawStreamDescriptorSurfaceCreateInfoGGP, allocator: Option<&RawAllocationCallbacks>, surface: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult;
pub(crate) type CreateSwapchainKhr = extern "C" fn(device: DeviceRaw, create_info: core::ptr::NonNull<RawSwapchainCreateInfoKHR>, allocator: Option<&RawAllocationCallbacks>, swapchain: core::ptr::NonNull<SwapchainKHRRaw>) -> RawResult;
pub(crate) type CreateValidationCacheExt = extern "C" fn(device: DeviceRaw, create_info: &RawValidationCacheCreateInfoEXT, allocator: Option<&RawAllocationCallbacks>, validation_cache: core::ptr::NonNull<ValidationCacheEXTRaw>) -> RawResult;
pub(crate) type CreateViSurfaceNn = extern "C" fn(instance: InstanceRaw, create_info: &RawViSurfaceCreateInfoNN, allocator: Option<&RawAllocationCallbacks>, surface: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult;
pub(crate) type CreateVideoSessionKhr = extern "C" fn(device: DeviceRaw, create_info: &RawVideoSessionCreateInfoKHR, allocator: Option<&RawAllocationCallbacks>, video_session: core::ptr::NonNull<VideoSessionKHRRaw>) -> RawResult;
pub(crate) type CreateVideoSessionParametersKhr = extern "C" fn(device: DeviceRaw, create_info: &RawVideoSessionParametersCreateInfoKHR, allocator: Option<&RawAllocationCallbacks>, video_session_parameters: core::ptr::NonNull<VideoSessionParametersKHRRaw>) -> RawResult;
pub(crate) type CreateWaylandSurfaceKhr = extern "C" fn(instance: InstanceRaw, create_info: &RawWaylandSurfaceCreateInfoKHR, allocator: Option<&RawAllocationCallbacks>, surface: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult;
pub(crate) type CreateWin32SurfaceKhr = extern "C" fn(instance: InstanceRaw, create_info: &RawWin32SurfaceCreateInfoKHR, allocator: Option<&RawAllocationCallbacks>, surface: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult;
pub(crate) type CreateXcbSurfaceKhr = extern "C" fn(instance: InstanceRaw, create_info: &RawXcbSurfaceCreateInfoKHR, allocator: Option<&RawAllocationCallbacks>, surface: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult;
pub(crate) type CreateXlibSurfaceKhr = extern "C" fn(instance: InstanceRaw, create_info: &RawXlibSurfaceCreateInfoKHR, allocator: Option<&RawAllocationCallbacks>, surface: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult;
pub(crate) type DebugMarkerSetObjectNameExt = extern "C" fn(device: DeviceRaw, name_info: core::ptr::NonNull<RawDebugMarkerObjectNameInfoEXT>) -> RawResult;
pub(crate) type DebugMarkerSetObjectTagExt = extern "C" fn(device: DeviceRaw, tag_info: core::ptr::NonNull<RawDebugMarkerObjectTagInfoEXT>) -> RawResult;
pub(crate) type DebugReportMessageExt = extern "C" fn(instance: InstanceRaw, flags: DebugReportFlagsEXT, object_type: RawDebugReportObjectTypeEXT, object: u64, location: usize, message_code: i32, layer_prefix: UnsizedCStr, message: UnsizedCStr);
pub(crate) type DeferredOperationJoinKhr = extern "C" fn(device: DeviceRaw, operation: DeferredOperationKHRRaw) -> RawResult;
pub(crate) type DestroyAccelerationStructureKhr = extern "C" fn(device: DeviceRaw, acceleration_structure: AccelerationStructureKHRMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyAccelerationStructureNv = extern "C" fn(device: DeviceRaw, acceleration_structure: AccelerationStructureNVMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyBuffer = extern "C" fn(device: DeviceRaw, buffer: BufferMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyBufferCollectionFuchsia = extern "C" fn(device: DeviceRaw, collection: BufferCollectionFUCHSIARaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyBufferView = extern "C" fn(device: DeviceRaw, buffer_view: BufferViewMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyCommandPool = extern "C" fn(device: DeviceRaw, command_pool: CommandPoolMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyCuFunctionNvx = extern "C" fn(device: DeviceRaw, function: CuFunctionNVXRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyCuModuleNvx = extern "C" fn(device: DeviceRaw, module: CuModuleNVXRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyDebugReportCallbackExt = extern "C" fn(instance: InstanceRaw, callback: DebugReportCallbackEXTMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyDebugUtilsMessengerExt = extern "C" fn(instance: InstanceRaw, messenger: DebugUtilsMessengerEXTMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyDeferredOperationKhr = extern "C" fn(device: DeviceRaw, operation: DeferredOperationKHRMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyDescriptorPool = extern "C" fn(device: DeviceRaw, descriptor_pool: DescriptorPoolMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyDescriptorSetLayout = extern "C" fn(device: DeviceRaw, descriptor_set_layout: DescriptorSetLayoutMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyDescriptorUpdateTemplate = extern "C" fn(device: DeviceRaw, descriptor_update_template: DescriptorUpdateTemplateMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyDevice = extern "C" fn(device: DeviceMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyEvent = extern "C" fn(device: DeviceRaw, event: EventMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyFence = extern "C" fn(device: DeviceRaw, fence: FenceMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyFramebuffer = extern "C" fn(device: DeviceRaw, framebuffer: FramebufferMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyImage = extern "C" fn(device: DeviceRaw, image: ImageMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyImageView = extern "C" fn(device: DeviceRaw, image_view: ImageViewMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyIndirectCommandsLayoutNv = extern "C" fn(device: DeviceRaw, indirect_commands_layout: IndirectCommandsLayoutNVMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyInstance = extern "C" fn(instance: InstanceMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyMicromapExt = extern "C" fn(device: DeviceRaw, micromap: MicromapEXTMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyOpticalFlowSessionNv = extern "C" fn(device: DeviceRaw, session: OpticalFlowSessionNVRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyPipeline = extern "C" fn(device: DeviceRaw, pipeline: PipelineMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyPipelineCache = extern "C" fn(device: DeviceRaw, pipeline_cache: PipelineCacheMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyPipelineLayout = extern "C" fn(device: DeviceRaw, pipeline_layout: PipelineLayoutMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyPrivateDataSlot = extern "C" fn(device: DeviceRaw, private_data_slot: PrivateDataSlotMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyQueryPool = extern "C" fn(device: DeviceRaw, query_pool: QueryPoolMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyRenderPass = extern "C" fn(device: DeviceRaw, render_pass: RenderPassMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroySampler = extern "C" fn(device: DeviceRaw, sampler: SamplerMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroySamplerYcbcrConversion = extern "C" fn(device: DeviceRaw, ycbcr_conversion: SamplerYcbcrConversionMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroySemaphore = extern "C" fn(device: DeviceRaw, semaphore: SemaphoreMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyShaderModule = extern "C" fn(device: DeviceRaw, shader_module: ShaderModuleMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroySurfaceKhr = extern "C" fn(instance: InstanceRaw, surface: SurfaceKHRMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroySwapchainKhr = extern "C" fn(device: DeviceRaw, swapchain: SwapchainKHRMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyValidationCacheExt = extern "C" fn(device: DeviceRaw, validation_cache: ValidationCacheEXTMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyVideoSessionKhr = extern "C" fn(device: DeviceRaw, video_session: VideoSessionKHRMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DestroyVideoSessionParametersKhr = extern "C" fn(device: DeviceRaw, video_session_parameters: VideoSessionParametersKHRMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type DeviceWaitIdle = extern "C" fn(device: DeviceRaw) -> RawResult;
pub(crate) type DisplayPowerControlExt = extern "C" fn(device: DeviceRaw, display: DisplayKHRRaw, display_power_info: &RawDisplayPowerInfoEXT) -> RawResult;
pub(crate) type EndCommandBuffer = extern "C" fn(command_buffer: CommandBufferMutRaw) -> RawResult;
pub(crate) type EnumerateDeviceExtensionProperties = extern "C" fn(physical_device: PhysicalDeviceRaw, layer_name: Option<UnsizedCStr>, property_count: core::ptr::NonNull<u32>, properties: Option<core::ptr::NonNull<RawExtensionProperties>>) -> RawResult;
pub(crate) type EnumerateDeviceLayerProperties = extern "C" fn(physical_device: PhysicalDeviceRaw, property_count: core::ptr::NonNull<u32>, properties: Option<core::ptr::NonNull<RawLayerProperties>>) -> RawResult;
pub(crate) type EnumerateInstanceExtensionProperties = extern "C" fn(layer_name: Option<UnsizedCStr>, property_count: core::ptr::NonNull<u32>, properties: Option<core::ptr::NonNull<RawExtensionProperties>>) -> RawResult;
pub(crate) type EnumerateInstanceLayerProperties = extern "C" fn(property_count: core::ptr::NonNull<u32>, properties: Option<core::ptr::NonNull<RawLayerProperties>>) -> RawResult;
pub(crate) type EnumerateInstanceVersion = extern "C" fn(api_version: core::ptr::NonNull<Version>) -> RawResult;
pub(crate) type EnumeratePhysicalDeviceGroups = extern "C" fn(instance: InstanceRaw, physical_device_group_count: core::ptr::NonNull<u32>, physical_device_group_properties: Option<core::ptr::NonNull<RawPhysicalDeviceGroupProperties>>) -> RawResult;
pub(crate) type EnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, queue_family_index: u32, counter_count: core::ptr::NonNull<u32>, counters: Option<core::ptr::NonNull<RawPerformanceCounterKHR>>, counter_descriptions: Option<core::ptr::NonNull<RawPerformanceCounterDescriptionKHR>>) -> RawResult;
pub(crate) type EnumeratePhysicalDevices = extern "C" fn(instance: InstanceRaw, physical_device_count: core::ptr::NonNull<u32>, physical_devices: Option<core::ptr::NonNull<PhysicalDeviceRaw>>) -> RawResult;
pub(crate) type ExportMetalObjectsExt = extern "C" fn(device: DeviceRaw, metal_objects_info: core::ptr::NonNull<RawExportMetalObjectsInfoEXT>);
pub(crate) type FlushMappedMemoryRanges = extern "C" fn(device: DeviceRaw, memory_range_count: u32, memory_ranges: &RawMappedMemoryRange) -> RawResult;
pub(crate) type FreeCommandBuffers = extern "C" fn(device: DeviceRaw, command_pool: CommandPoolMutRaw, command_buffer_count: u32, command_buffers: Option<core::ptr::NonNull<CommandBufferMutRaw>>);
pub(crate) type FreeDescriptorSets = extern "C" fn(device: DeviceRaw, descriptor_pool: DescriptorPoolMutRaw, descriptor_set_count: u32, descriptor_sets: Option<core::ptr::NonNull<DescriptorSetMutRaw>>) -> RawResult;
pub(crate) type FreeMemory = extern "C" fn(device: DeviceRaw, memory: DeviceMemoryMutRaw, allocator: Option<&RawAllocationCallbacks>);
pub(crate) type GetAccelerationStructureBuildSizesKhr = extern "C" fn(device: DeviceRaw, build_type: RawAccelerationStructureBuildTypeKHR, build_info: &RawAccelerationStructureBuildGeometryInfoKHR, max_primitive_counts: Option<&u32>, size_info: core::ptr::NonNull<RawAccelerationStructureBuildSizesInfoKHR>);
pub(crate) type GetAccelerationStructureDeviceAddressKhr = extern "C" fn(device: DeviceRaw, info: &RawAccelerationStructureDeviceAddressInfoKHR) -> DeviceAddress;
pub(crate) type GetAccelerationStructureHandleNv = extern "C" fn(device: DeviceRaw, acceleration_structure: AccelerationStructureNVRaw, data_size: usize, data: core::ptr::NonNull<u8>) -> RawResult;
pub(crate) type GetAccelerationStructureMemoryRequirementsNv = extern "C" fn(device: DeviceRaw, info: &RawAccelerationStructureMemoryRequirementsInfoNV, memory_requirements: core::ptr::NonNull<RawMemoryRequirements2>);
pub(crate) type GetAccelerationStructureOpaqueCaptureDescriptorDataExt = extern "C" fn(device: DeviceRaw, info: &RawAccelerationStructureCaptureDescriptorDataInfoEXT, data: core::ptr::NonNull<u8>) -> RawResult;
pub(crate) type GetAndroidHardwareBufferPropertiesAndroid = extern "C" fn(device: DeviceRaw, buffer: &AHardwareBuffer, properties: core::ptr::NonNull<RawAndroidHardwareBufferPropertiesANDROID>) -> RawResult;
pub(crate) type GetBufferCollectionPropertiesFuchsia = extern "C" fn(device: DeviceRaw, collection: BufferCollectionFUCHSIARaw, properties: core::ptr::NonNull<RawBufferCollectionPropertiesFUCHSIA>) -> RawResult;
pub(crate) type GetBufferDeviceAddress = extern "C" fn(device: DeviceRaw, info: &RawBufferDeviceAddressInfo) -> DeviceAddress;
pub(crate) type GetBufferMemoryRequirements = extern "C" fn(device: DeviceRaw, buffer: BufferRaw, memory_requirements: core::ptr::NonNull<RawMemoryRequirements>);
pub(crate) type GetBufferMemoryRequirements2 = extern "C" fn(device: DeviceRaw, info: &RawBufferMemoryRequirementsInfo2, memory_requirements: core::ptr::NonNull<RawMemoryRequirements2>);
pub(crate) type GetBufferOpaqueCaptureAddress = extern "C" fn(device: DeviceRaw, info: &RawBufferDeviceAddressInfo) -> u64;
pub(crate) type GetBufferOpaqueCaptureDescriptorDataExt = extern "C" fn(device: DeviceRaw, info: &RawBufferCaptureDescriptorDataInfoEXT, data: core::ptr::NonNull<u8>) -> RawResult;
pub(crate) type GetCalibratedTimestampsExt = extern "C" fn(device: DeviceRaw, timestamp_count: u32, timestamp_infos: &RawCalibratedTimestampInfoEXT, timestamps: core::ptr::NonNull<u64>, max_deviation: core::ptr::NonNull<u64>) -> RawResult;
pub(crate) type GetDeferredOperationMaxConcurrencyKhr = extern "C" fn(device: DeviceRaw, operation: DeferredOperationKHRRaw) -> u32;
pub(crate) type GetDeferredOperationResultKhr = extern "C" fn(device: DeviceRaw, operation: DeferredOperationKHRRaw) -> RawResult;
pub(crate) type GetDescriptorExt = extern "C" fn(device: DeviceRaw, descriptor_info: &RawDescriptorGetInfoEXT, data_size: usize, descriptor: core::ptr::NonNull<u8>);
pub(crate) type GetDescriptorSetHostMappingValve = extern "C" fn(device: DeviceRaw, descriptor_set: DescriptorSetRaw, data: core::ptr::NonNull<*mut u8>);
pub(crate) type GetDescriptorSetLayoutBindingOffsetExt = extern "C" fn(device: DeviceRaw, layout: DescriptorSetLayoutRaw, binding: u32, offset: core::ptr::NonNull<DeviceSize>);
pub(crate) type GetDescriptorSetLayoutHostMappingInfoValve = extern "C" fn(device: DeviceRaw, binding_reference: &RawDescriptorSetBindingReferenceVALVE, host_mapping: core::ptr::NonNull<RawDescriptorSetLayoutHostMappingInfoVALVE>);
pub(crate) type GetDescriptorSetLayoutSizeExt = extern "C" fn(device: DeviceRaw, layout: DescriptorSetLayoutRaw, layout_size_in_bytes: core::ptr::NonNull<DeviceSize>);
pub(crate) type GetDescriptorSetLayoutSupport = extern "C" fn(device: DeviceRaw, create_info: &RawDescriptorSetLayoutCreateInfo, support: core::ptr::NonNull<RawDescriptorSetLayoutSupport>);
pub(crate) type GetDeviceAccelerationStructureCompatibilityKhr = extern "C" fn(device: DeviceRaw, version_info: &RawAccelerationStructureVersionInfoKHR, compatibility: core::ptr::NonNull<RawAccelerationStructureCompatibilityKHR>);
pub(crate) type GetDeviceBufferMemoryRequirements = extern "C" fn(device: DeviceRaw, info: &RawDeviceBufferMemoryRequirements, memory_requirements: core::ptr::NonNull<RawMemoryRequirements2>);
pub(crate) type GetDeviceFaultInfoExt = extern "C" fn(device: DeviceRaw, fault_counts: core::ptr::NonNull<RawDeviceFaultCountsEXT>, fault_info: Option<core::ptr::NonNull<RawDeviceFaultInfoEXT>>) -> RawResult;
pub(crate) type GetDeviceGroupPeerMemoryFeatures = extern "C" fn(device: DeviceRaw, heap_index: u32, local_device_index: u32, remote_device_index: u32, peer_memory_features: core::ptr::NonNull<PeerMemoryFeatureFlags>);
pub(crate) type GetDeviceGroupPresentCapabilitiesKhr = extern "C" fn(device: DeviceRaw, device_group_present_capabilities: core::ptr::NonNull<RawDeviceGroupPresentCapabilitiesKHR>) -> RawResult;
pub(crate) type GetDeviceGroupSurfacePresentModes2Ext = extern "C" fn(device: DeviceRaw, surface_info: &RawPhysicalDeviceSurfaceInfo2KHR, modes: core::ptr::NonNull<DeviceGroupPresentModeFlagsKHR>) -> RawResult;
pub(crate) type GetDeviceGroupSurfacePresentModesKhr = extern "C" fn(device: DeviceRaw, surface: SurfaceKHRMutRaw, modes: core::ptr::NonNull<DeviceGroupPresentModeFlagsKHR>) -> RawResult;
pub(crate) type GetDeviceImageMemoryRequirements = extern "C" fn(device: DeviceRaw, info: &RawDeviceImageMemoryRequirements, memory_requirements: core::ptr::NonNull<RawMemoryRequirements2>);
pub(crate) type GetDeviceImageSparseMemoryRequirements = extern "C" fn(device: DeviceRaw, info: &RawDeviceImageMemoryRequirements, sparse_memory_requirement_count: core::ptr::NonNull<u32>, sparse_memory_requirements: Option<core::ptr::NonNull<RawSparseImageMemoryRequirements2>>);
pub(crate) type GetDeviceMemoryCommitment = extern "C" fn(device: DeviceRaw, memory: DeviceMemoryRaw, committed_memory_in_bytes: core::ptr::NonNull<DeviceSize>);
pub(crate) type GetDeviceMemoryOpaqueCaptureAddress = extern "C" fn(device: DeviceRaw, info: &RawDeviceMemoryOpaqueCaptureAddressInfo) -> u64;
pub(crate) type GetDeviceMicromapCompatibilityExt = extern "C" fn(device: DeviceRaw, version_info: &RawMicromapVersionInfoEXT, compatibility: core::ptr::NonNull<RawAccelerationStructureCompatibilityKHR>);
pub(crate) type GetDeviceProcAddr = extern "C" fn(device: DeviceRaw, name: UnsizedCStr) -> VoidFunction;
pub(crate) type GetDeviceQueue = extern "C" fn(device: DeviceRaw, queue_family_index: u32, queue_index: u32, queue: core::ptr::NonNull<QueueRaw>);
pub(crate) type GetDeviceQueue2 = extern "C" fn(device: DeviceRaw, queue_info: &RawDeviceQueueInfo2, queue: core::ptr::NonNull<QueueRaw>);
pub(crate) type GetDeviceSubpassShadingMaxWorkgroupSizeHuawei = extern "C" fn(device: DeviceRaw, renderpass: RenderPassRaw, max_workgroup_size: core::ptr::NonNull<RawExtent2D>) -> RawResult;
pub(crate) type GetDisplayModeProperties2Khr = extern "C" fn(physical_device: PhysicalDeviceRaw, display: DisplayKHRRaw, property_count: core::ptr::NonNull<u32>, properties: Option<core::ptr::NonNull<RawDisplayModeProperties2KHR>>) -> RawResult;
pub(crate) type GetDisplayModePropertiesKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, display: DisplayKHRRaw, property_count: core::ptr::NonNull<u32>, properties: Option<core::ptr::NonNull<RawDisplayModePropertiesKHR>>) -> RawResult;
pub(crate) type GetDisplayPlaneCapabilities2Khr = extern "C" fn(physical_device: PhysicalDeviceRaw, display_plane_info: core::ptr::NonNull<RawDisplayPlaneInfo2KHR>, capabilities: core::ptr::NonNull<RawDisplayPlaneCapabilities2KHR>) -> RawResult;
pub(crate) type GetDisplayPlaneCapabilitiesKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, mode: DisplayModeKHRMutRaw, plane_index: u32, capabilities: core::ptr::NonNull<RawDisplayPlaneCapabilitiesKHR>) -> RawResult;
pub(crate) type GetDisplayPlaneSupportedDisplaysKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, plane_index: u32, display_count: core::ptr::NonNull<u32>, displays: Option<core::ptr::NonNull<DisplayKHRRaw>>) -> RawResult;
pub(crate) type GetDrmDisplayExt = extern "C" fn(physical_device: PhysicalDeviceRaw, drm_fd: i32, connector_id: u32, display: core::ptr::NonNull<DisplayKHRRaw>) -> RawResult;
pub(crate) type GetDynamicRenderingTilePropertiesQcom = extern "C" fn(device: DeviceRaw, rendering_info: &RawRenderingInfo, properties: core::ptr::NonNull<RawTilePropertiesQCOM>) -> RawResult;
pub(crate) type GetEventStatus = extern "C" fn(device: DeviceRaw, event: EventRaw) -> RawResult;
pub(crate) type GetFenceFdKhr = extern "C" fn(device: DeviceRaw, get_fd_info: &RawFenceGetFdInfoKHR, fd: core::ptr::NonNull<::std::os::raw::c_int>) -> RawResult;
pub(crate) type GetFenceStatus = extern "C" fn(device: DeviceRaw, fence: FenceRaw) -> RawResult;
pub(crate) type GetFenceWin32HandleKhr = extern "C" fn(device: DeviceRaw, get_win32_handle_info: &RawFenceGetWin32HandleInfoKHR, handle: core::ptr::NonNull<HANDLE>) -> RawResult;
pub(crate) type GetFramebufferTilePropertiesQcom = extern "C" fn(device: DeviceRaw, framebuffer: FramebufferRaw, properties_count: core::ptr::NonNull<u32>, properties: Option<core::ptr::NonNull<RawTilePropertiesQCOM>>) -> RawResult;
pub(crate) type GetGeneratedCommandsMemoryRequirementsNv = extern "C" fn(device: DeviceRaw, info: &RawGeneratedCommandsMemoryRequirementsInfoNV, memory_requirements: core::ptr::NonNull<RawMemoryRequirements2>);
pub(crate) type GetImageDrmFormatModifierPropertiesExt = extern "C" fn(device: DeviceRaw, image: ImageRaw, properties: core::ptr::NonNull<RawImageDrmFormatModifierPropertiesEXT>) -> RawResult;
pub(crate) type GetImageMemoryRequirements = extern "C" fn(device: DeviceRaw, image: ImageRaw, memory_requirements: core::ptr::NonNull<RawMemoryRequirements>);
pub(crate) type GetImageMemoryRequirements2 = extern "C" fn(device: DeviceRaw, info: &RawImageMemoryRequirementsInfo2, memory_requirements: core::ptr::NonNull<RawMemoryRequirements2>);
pub(crate) type GetImageOpaqueCaptureDescriptorDataExt = extern "C" fn(device: DeviceRaw, info: &RawImageCaptureDescriptorDataInfoEXT, data: core::ptr::NonNull<u8>) -> RawResult;
pub(crate) type GetImageSparseMemoryRequirements = extern "C" fn(device: DeviceRaw, image: ImageRaw, sparse_memory_requirement_count: core::ptr::NonNull<u32>, sparse_memory_requirements: Option<core::ptr::NonNull<RawSparseImageMemoryRequirements>>);
pub(crate) type GetImageSparseMemoryRequirements2 = extern "C" fn(device: DeviceRaw, info: &RawImageSparseMemoryRequirementsInfo2, sparse_memory_requirement_count: core::ptr::NonNull<u32>, sparse_memory_requirements: Option<core::ptr::NonNull<RawSparseImageMemoryRequirements2>>);
pub(crate) type GetImageSubresourceLayout = extern "C" fn(device: DeviceRaw, image: ImageRaw, subresource: &RawImageSubresource, layout: core::ptr::NonNull<RawSubresourceLayout>);
pub(crate) type GetImageSubresourceLayout2Ext = extern "C" fn(device: DeviceRaw, image: ImageRaw, subresource: &RawImageSubresource2EXT, layout: core::ptr::NonNull<RawSubresourceLayout2EXT>);
pub(crate) type GetImageViewAddressNvx = extern "C" fn(device: DeviceRaw, image_view: ImageViewRaw, properties: core::ptr::NonNull<RawImageViewAddressPropertiesNVX>) -> RawResult;
pub(crate) type GetImageViewHandleNvx = extern "C" fn(device: DeviceRaw, info: &RawImageViewHandleInfoNVX) -> u32;
pub(crate) type GetImageViewOpaqueCaptureDescriptorDataExt = extern "C" fn(device: DeviceRaw, info: &RawImageViewCaptureDescriptorDataInfoEXT, data: core::ptr::NonNull<u8>) -> RawResult;
pub(crate) type GetInstanceProcAddr = extern "C" fn(instance: InstanceRaw, name: UnsizedCStr) -> VoidFunction;
pub(crate) type GetMemoryAndroidHardwareBufferAndroid = extern "C" fn(device: DeviceRaw, info: &RawMemoryGetAndroidHardwareBufferInfoANDROID, buffer: core::ptr::NonNull<*mut AHardwareBuffer>) -> RawResult;
pub(crate) type GetMemoryFdKhr = extern "C" fn(device: DeviceRaw, get_fd_info: &RawMemoryGetFdInfoKHR, fd: core::ptr::NonNull<::std::os::raw::c_int>) -> RawResult;
pub(crate) type GetMemoryFdPropertiesKhr = extern "C" fn(device: DeviceRaw, handle_type: RawExternalMemoryHandleTypeFlagBits, fd: ::std::os::raw::c_int, memory_fd_properties: core::ptr::NonNull<RawMemoryFdPropertiesKHR>) -> RawResult;
pub(crate) type GetMemoryHostPointerPropertiesExt = extern "C" fn(device: DeviceRaw, handle_type: RawExternalMemoryHandleTypeFlagBits, host_pointer: &u8, memory_host_pointer_properties: core::ptr::NonNull<RawMemoryHostPointerPropertiesEXT>) -> RawResult;
pub(crate) type GetMemoryRemoteAddressNv = extern "C" fn(device: DeviceRaw, memory_get_remote_address_info: &RawMemoryGetRemoteAddressInfoNV, address: core::ptr::NonNull<RemoteAddressNV>) -> RawResult;
pub(crate) type GetMemoryWin32HandleKhr = extern "C" fn(device: DeviceRaw, get_win32_handle_info: &RawMemoryGetWin32HandleInfoKHR, handle: core::ptr::NonNull<HANDLE>) -> RawResult;
pub(crate) type GetMemoryWin32HandleNv = extern "C" fn(device: DeviceRaw, memory: DeviceMemoryRaw, handle_type: ExternalMemoryHandleTypeFlagsNV, handle: core::ptr::NonNull<HANDLE>) -> RawResult;
pub(crate) type GetMemoryWin32HandlePropertiesKhr = extern "C" fn(device: DeviceRaw, handle_type: RawExternalMemoryHandleTypeFlagBits, handle: HANDLE, memory_win32_handle_properties: core::ptr::NonNull<RawMemoryWin32HandlePropertiesKHR>) -> RawResult;
pub(crate) type GetMemoryZirconHandleFuchsia = extern "C" fn(device: DeviceRaw, get_zircon_handle_info: &RawMemoryGetZirconHandleInfoFUCHSIA, zircon_handle: core::ptr::NonNull<zx_handle_t>) -> RawResult;
pub(crate) type GetMemoryZirconHandlePropertiesFuchsia = extern "C" fn(device: DeviceRaw, handle_type: RawExternalMemoryHandleTypeFlagBits, zircon_handle: zx_handle_t, memory_zircon_handle_properties: core::ptr::NonNull<RawMemoryZirconHandlePropertiesFUCHSIA>) -> RawResult;
pub(crate) type GetMicromapBuildSizesExt = extern "C" fn(device: DeviceRaw, build_type: RawAccelerationStructureBuildTypeKHR, build_info: &RawMicromapBuildInfoEXT, size_info: core::ptr::NonNull<RawMicromapBuildSizesInfoEXT>);
pub(crate) type GetPastPresentationTimingGoogle = extern "C" fn(device: DeviceRaw, swapchain: SwapchainKHRMutRaw, presentation_timing_count: core::ptr::NonNull<u32>, presentation_timings: Option<core::ptr::NonNull<RawPastPresentationTimingGOOGLE>>) -> RawResult;
pub(crate) type GetPerformanceParameterIntel = extern "C" fn(device: DeviceRaw, parameter: RawPerformanceParameterTypeINTEL, value: core::ptr::NonNull<RawPerformanceValueINTEL>) -> RawResult;
pub(crate) type GetPhysicalDeviceCalibrateableTimeDomainsExt = extern "C" fn(physical_device: PhysicalDeviceRaw, time_domain_count: core::ptr::NonNull<u32>, time_domains: Option<core::ptr::NonNull<RawTimeDomainEXT>>) -> RawResult;
pub(crate) type GetPhysicalDeviceCooperativeMatrixPropertiesNv = extern "C" fn(physical_device: PhysicalDeviceRaw, property_count: core::ptr::NonNull<u32>, properties: Option<core::ptr::NonNull<RawCooperativeMatrixPropertiesNV>>) -> RawResult;
pub(crate) type GetPhysicalDeviceDirectFbPresentationSupportExt = extern "C" fn(physical_device: PhysicalDeviceRaw, queue_family_index: u32, dfb: core::ptr::NonNull<IDirectFB>) -> Bool32;
pub(crate) type GetPhysicalDeviceDisplayPlaneProperties2Khr = extern "C" fn(physical_device: PhysicalDeviceRaw, property_count: core::ptr::NonNull<u32>, properties: Option<core::ptr::NonNull<RawDisplayPlaneProperties2KHR>>) -> RawResult;
pub(crate) type GetPhysicalDeviceDisplayPlanePropertiesKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, property_count: core::ptr::NonNull<u32>, properties: Option<core::ptr::NonNull<RawDisplayPlanePropertiesKHR>>) -> RawResult;
pub(crate) type GetPhysicalDeviceDisplayProperties2Khr = extern "C" fn(physical_device: PhysicalDeviceRaw, property_count: core::ptr::NonNull<u32>, properties: Option<core::ptr::NonNull<RawDisplayProperties2KHR>>) -> RawResult;
pub(crate) type GetPhysicalDeviceDisplayPropertiesKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, property_count: core::ptr::NonNull<u32>, properties: Option<core::ptr::NonNull<RawDisplayPropertiesKHR>>) -> RawResult;
pub(crate) type GetPhysicalDeviceExternalBufferProperties = extern "C" fn(physical_device: PhysicalDeviceRaw, external_buffer_info: &RawPhysicalDeviceExternalBufferInfo, external_buffer_properties: core::ptr::NonNull<RawExternalBufferProperties>);
pub(crate) type GetPhysicalDeviceExternalFenceProperties = extern "C" fn(physical_device: PhysicalDeviceRaw, external_fence_info: &RawPhysicalDeviceExternalFenceInfo, external_fence_properties: core::ptr::NonNull<RawExternalFenceProperties>);
pub(crate) type GetPhysicalDeviceExternalImageFormatPropertiesNv = extern "C" fn(physical_device: PhysicalDeviceRaw, format: RawFormat, e_type: RawImageType, tiling: RawImageTiling, usage: ImageUsageFlags, flags: ImageCreateFlags, external_handle_type: ExternalMemoryHandleTypeFlagsNV, external_image_format_properties: core::ptr::NonNull<RawExternalImageFormatPropertiesNV>) -> RawResult;
pub(crate) type GetPhysicalDeviceExternalSemaphoreProperties = extern "C" fn(physical_device: PhysicalDeviceRaw, external_semaphore_info: &RawPhysicalDeviceExternalSemaphoreInfo, external_semaphore_properties: core::ptr::NonNull<RawExternalSemaphoreProperties>);
pub(crate) type GetPhysicalDeviceFeatures = extern "C" fn(physical_device: PhysicalDeviceRaw, features: core::ptr::NonNull<RawPhysicalDeviceFeatures>);
pub(crate) type GetPhysicalDeviceFeatures2 = extern "C" fn(physical_device: PhysicalDeviceRaw, features: core::ptr::NonNull<RawPhysicalDeviceFeatures2>);
pub(crate) type GetPhysicalDeviceFormatProperties = extern "C" fn(physical_device: PhysicalDeviceRaw, format: RawFormat, format_properties: core::ptr::NonNull<RawFormatProperties>);
pub(crate) type GetPhysicalDeviceFormatProperties2 = extern "C" fn(physical_device: PhysicalDeviceRaw, format: RawFormat, format_properties: core::ptr::NonNull<RawFormatProperties2>);
pub(crate) type GetPhysicalDeviceFragmentShadingRatesKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, fragment_shading_rate_count: core::ptr::NonNull<u32>, fragment_shading_rates: Option<core::ptr::NonNull<RawPhysicalDeviceFragmentShadingRateKHR>>) -> RawResult;
pub(crate) type GetPhysicalDeviceImageFormatProperties = extern "C" fn(physical_device: PhysicalDeviceRaw, format: RawFormat, e_type: RawImageType, tiling: RawImageTiling, usage: ImageUsageFlags, flags: ImageCreateFlags, image_format_properties: core::ptr::NonNull<RawImageFormatProperties>) -> RawResult;
pub(crate) type GetPhysicalDeviceImageFormatProperties2 = extern "C" fn(physical_device: PhysicalDeviceRaw, image_format_info: &RawPhysicalDeviceImageFormatInfo2, image_format_properties: core::ptr::NonNull<RawImageFormatProperties2>) -> RawResult;
pub(crate) type GetPhysicalDeviceMemoryProperties = extern "C" fn(physical_device: PhysicalDeviceRaw, memory_properties: core::ptr::NonNull<RawPhysicalDeviceMemoryProperties>);
pub(crate) type GetPhysicalDeviceMemoryProperties2 = extern "C" fn(physical_device: PhysicalDeviceRaw, memory_properties: core::ptr::NonNull<RawPhysicalDeviceMemoryProperties2>);
pub(crate) type GetPhysicalDeviceMultisamplePropertiesExt = extern "C" fn(physical_device: PhysicalDeviceRaw, samples: RawSampleCountFlagBits, multisample_properties: core::ptr::NonNull<RawMultisamplePropertiesEXT>);
pub(crate) type GetPhysicalDeviceOpticalFlowImageFormatsNv = extern "C" fn(physical_device: PhysicalDeviceRaw, optical_flow_image_format_info: &RawOpticalFlowImageFormatInfoNV, format_count: core::ptr::NonNull<u32>, image_format_properties: Option<core::ptr::NonNull<RawOpticalFlowImageFormatPropertiesNV>>) -> RawResult;
pub(crate) type GetPhysicalDevicePresentRectanglesKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, surface: SurfaceKHRMutRaw, rect_count: core::ptr::NonNull<u32>, rects: Option<core::ptr::NonNull<RawRect2D>>) -> RawResult;
pub(crate) type GetPhysicalDeviceProperties = extern "C" fn(physical_device: PhysicalDeviceRaw, properties: core::ptr::NonNull<RawPhysicalDeviceProperties>);
pub(crate) type GetPhysicalDeviceProperties2 = extern "C" fn(physical_device: PhysicalDeviceRaw, properties: core::ptr::NonNull<RawPhysicalDeviceProperties2>);
pub(crate) type GetPhysicalDeviceQueueFamilyPerformanceQueryPassesKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, performance_query_create_info: &RawQueryPoolPerformanceCreateInfoKHR, num_passes: core::ptr::NonNull<u32>);
pub(crate) type GetPhysicalDeviceQueueFamilyProperties = extern "C" fn(physical_device: PhysicalDeviceRaw, queue_family_property_count: core::ptr::NonNull<u32>, queue_family_properties: Option<core::ptr::NonNull<RawQueueFamilyProperties>>);
pub(crate) type GetPhysicalDeviceQueueFamilyProperties2 = extern "C" fn(physical_device: PhysicalDeviceRaw, queue_family_property_count: core::ptr::NonNull<u32>, queue_family_properties: Option<core::ptr::NonNull<RawQueueFamilyProperties2>>);
pub(crate) type GetPhysicalDeviceScreenPresentationSupportQnx = extern "C" fn(physical_device: PhysicalDeviceRaw, queue_family_index: u32, window: core::ptr::NonNull<screen_window>) -> Bool32;
pub(crate) type GetPhysicalDeviceSparseImageFormatProperties = extern "C" fn(physical_device: PhysicalDeviceRaw, format: RawFormat, e_type: RawImageType, samples: RawSampleCountFlagBits, usage: ImageUsageFlags, tiling: RawImageTiling, property_count: core::ptr::NonNull<u32>, properties: Option<core::ptr::NonNull<RawSparseImageFormatProperties>>);
pub(crate) type GetPhysicalDeviceSparseImageFormatProperties2 = extern "C" fn(physical_device: PhysicalDeviceRaw, format_info: &RawPhysicalDeviceSparseImageFormatInfo2, property_count: core::ptr::NonNull<u32>, properties: Option<core::ptr::NonNull<RawSparseImageFormatProperties2>>);
pub(crate) type GetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNv = extern "C" fn(physical_device: PhysicalDeviceRaw, combination_count: core::ptr::NonNull<u32>, combinations: Option<core::ptr::NonNull<RawFramebufferMixedSamplesCombinationNV>>) -> RawResult;
pub(crate) type GetPhysicalDeviceSurfaceCapabilities2Ext = extern "C" fn(physical_device: PhysicalDeviceRaw, surface: SurfaceKHRRaw, surface_capabilities: core::ptr::NonNull<RawSurfaceCapabilities2EXT>) -> RawResult;
pub(crate) type GetPhysicalDeviceSurfaceCapabilities2Khr = extern "C" fn(physical_device: PhysicalDeviceRaw, surface_info: &RawPhysicalDeviceSurfaceInfo2KHR, surface_capabilities: core::ptr::NonNull<RawSurfaceCapabilities2KHR>) -> RawResult;
pub(crate) type GetPhysicalDeviceSurfaceCapabilitiesKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, surface: SurfaceKHRRaw, surface_capabilities: core::ptr::NonNull<RawSurfaceCapabilitiesKHR>) -> RawResult;
pub(crate) type GetPhysicalDeviceSurfaceFormats2Khr = extern "C" fn(physical_device: PhysicalDeviceRaw, surface_info: &RawPhysicalDeviceSurfaceInfo2KHR, surface_format_count: core::ptr::NonNull<u32>, surface_formats: Option<core::ptr::NonNull<RawSurfaceFormat2KHR>>) -> RawResult;
pub(crate) type GetPhysicalDeviceSurfaceFormatsKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, surface: SurfaceKHRRaw, surface_format_count: core::ptr::NonNull<u32>, surface_formats: Option<core::ptr::NonNull<RawSurfaceFormatKHR>>) -> RawResult;
pub(crate) type GetPhysicalDeviceSurfacePresentModes2Ext = extern "C" fn(physical_device: PhysicalDeviceRaw, surface_info: &RawPhysicalDeviceSurfaceInfo2KHR, present_mode_count: core::ptr::NonNull<u32>, present_modes: Option<core::ptr::NonNull<RawPresentModeKHR>>) -> RawResult;
pub(crate) type GetPhysicalDeviceSurfacePresentModesKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, surface: SurfaceKHRRaw, present_mode_count: core::ptr::NonNull<u32>, present_modes: Option<core::ptr::NonNull<RawPresentModeKHR>>) -> RawResult;
pub(crate) type GetPhysicalDeviceSurfaceSupportKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, queue_family_index: u32, surface: SurfaceKHRRaw, supported: core::ptr::NonNull<Bool32>) -> RawResult;
pub(crate) type GetPhysicalDeviceToolProperties = extern "C" fn(physical_device: PhysicalDeviceRaw, tool_count: core::ptr::NonNull<u32>, tool_properties: Option<core::ptr::NonNull<RawPhysicalDeviceToolProperties>>) -> RawResult;
pub(crate) type GetPhysicalDeviceVideoCapabilitiesKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, video_profile: &RawVideoProfileInfoKHR, capabilities: core::ptr::NonNull<RawVideoCapabilitiesKHR>) -> RawResult;
pub(crate) type GetPhysicalDeviceVideoFormatPropertiesKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, video_format_info: &RawPhysicalDeviceVideoFormatInfoKHR, video_format_property_count: core::ptr::NonNull<u32>, video_format_properties: Option<core::ptr::NonNull<RawVideoFormatPropertiesKHR>>) -> RawResult;
pub(crate) type GetPhysicalDeviceWaylandPresentationSupportKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, queue_family_index: u32, display: core::ptr::NonNull<WlDisplay>) -> Bool32;
pub(crate) type GetPhysicalDeviceWin32PresentationSupportKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, queue_family_index: u32) -> Bool32;
pub(crate) type GetPhysicalDeviceXcbPresentationSupportKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, queue_family_index: u32, connection: core::ptr::NonNull<xcb_connection_t>, visual_id: xcb_visualid_t) -> Bool32;
pub(crate) type GetPhysicalDeviceXlibPresentationSupportKhr = extern "C" fn(physical_device: PhysicalDeviceRaw, queue_family_index: u32, dpy: core::ptr::NonNull<Display>, visual_id: VisualID) -> Bool32;
pub(crate) type GetPipelineCacheData = extern "C" fn(device: DeviceRaw, pipeline_cache: PipelineCacheRaw, data_size: core::ptr::NonNull<usize>, data: Option<core::ptr::NonNull<u8>>) -> RawResult;
pub(crate) type GetPipelineExecutableInternalRepresentationsKhr = extern "C" fn(device: DeviceRaw, executable_info: &RawPipelineExecutableInfoKHR, internal_representation_count: core::ptr::NonNull<u32>, internal_representations: Option<core::ptr::NonNull<RawPipelineExecutableInternalRepresentationKHR>>) -> RawResult;
pub(crate) type GetPipelineExecutablePropertiesKhr = extern "C" fn(device: DeviceRaw, pipeline_info: &RawPipelineInfoKHR, executable_count: core::ptr::NonNull<u32>, properties: Option<core::ptr::NonNull<RawPipelineExecutablePropertiesKHR>>) -> RawResult;
pub(crate) type GetPipelineExecutableStatisticsKhr = extern "C" fn(device: DeviceRaw, executable_info: &RawPipelineExecutableInfoKHR, statistic_count: core::ptr::NonNull<u32>, statistics: Option<core::ptr::NonNull<RawPipelineExecutableStatisticKHR>>) -> RawResult;
pub(crate) type GetPipelinePropertiesExt = extern "C" fn(device: DeviceRaw, pipeline_info: &RawPipelineInfoKHR, pipeline_properties: core::ptr::NonNull<RawPipelinePropertiesIdentifierEXT>) -> RawResult;
pub(crate) type GetPrivateData = extern "C" fn(device: DeviceRaw, object_type: RawObjectType, object_handle: u64, private_data_slot: PrivateDataSlotRaw, data: core::ptr::NonNull<u64>);
pub(crate) type GetQueryPoolResults = extern "C" fn(device: DeviceRaw, query_pool: QueryPoolRaw, first_query: u32, query_count: u32, data_size: usize, data: core::ptr::NonNull<u8>, stride: DeviceSize, flags: QueryResultFlags) -> RawResult;
pub(crate) type GetQueueCheckpointData2Nv = extern "C" fn(queue: QueueRaw, checkpoint_data_count: core::ptr::NonNull<u32>, checkpoint_data: Option<core::ptr::NonNull<RawCheckpointData2NV>>);
pub(crate) type GetQueueCheckpointDataNv = extern "C" fn(queue: QueueRaw, checkpoint_data_count: core::ptr::NonNull<u32>, checkpoint_data: Option<core::ptr::NonNull<RawCheckpointDataNV>>);
pub(crate) type GetRandROutputDisplayExt = extern "C" fn(physical_device: PhysicalDeviceRaw, dpy: core::ptr::NonNull<Display>, rr_output: RROutput, display: core::ptr::NonNull<DisplayKHRRaw>) -> RawResult;
pub(crate) type GetRayTracingCaptureReplayShaderGroupHandlesKhr = extern "C" fn(device: DeviceRaw, pipeline: PipelineRaw, first_group: u32, group_count: u32, data_size: usize, data: core::ptr::NonNull<u8>) -> RawResult;
pub(crate) type GetRayTracingShaderGroupHandlesKhr = extern "C" fn(device: DeviceRaw, pipeline: PipelineRaw, first_group: u32, group_count: u32, data_size: usize, data: core::ptr::NonNull<u8>) -> RawResult;
pub(crate) type GetRayTracingShaderGroupStackSizeKhr = extern "C" fn(device: DeviceRaw, pipeline: PipelineRaw, group: u32, group_shader: RawShaderGroupShaderKHR) -> DeviceSize;
pub(crate) type GetRefreshCycleDurationGoogle = extern "C" fn(device: DeviceRaw, swapchain: SwapchainKHRMutRaw, display_timing_properties: core::ptr::NonNull<RawRefreshCycleDurationGOOGLE>) -> RawResult;
pub(crate) type GetRenderAreaGranularity = extern "C" fn(device: DeviceRaw, render_pass: RenderPassRaw, granularity: core::ptr::NonNull<RawExtent2D>);
pub(crate) type GetSamplerOpaqueCaptureDescriptorDataExt = extern "C" fn(device: DeviceRaw, info: &RawSamplerCaptureDescriptorDataInfoEXT, data: core::ptr::NonNull<u8>) -> RawResult;
pub(crate) type GetSemaphoreCounterValue = extern "C" fn(device: DeviceRaw, semaphore: SemaphoreRaw, value: core::ptr::NonNull<u64>) -> RawResult;
pub(crate) type GetSemaphoreFdKhr = extern "C" fn(device: DeviceRaw, get_fd_info: &RawSemaphoreGetFdInfoKHR, fd: core::ptr::NonNull<::std::os::raw::c_int>) -> RawResult;
pub(crate) type GetSemaphoreWin32HandleKhr = extern "C" fn(device: DeviceRaw, get_win32_handle_info: &RawSemaphoreGetWin32HandleInfoKHR, handle: core::ptr::NonNull<HANDLE>) -> RawResult;
pub(crate) type GetSemaphoreZirconHandleFuchsia = extern "C" fn(device: DeviceRaw, get_zircon_handle_info: &RawSemaphoreGetZirconHandleInfoFUCHSIA, zircon_handle: core::ptr::NonNull<zx_handle_t>) -> RawResult;
pub(crate) type GetShaderInfoAmd = extern "C" fn(device: DeviceRaw, pipeline: PipelineRaw, shader_stage: RawShaderStageFlagBits, info_type: RawShaderInfoTypeAMD, info_size: core::ptr::NonNull<usize>, info: Option<core::ptr::NonNull<u8>>) -> RawResult;
pub(crate) type GetShaderModuleCreateInfoIdentifierExt = extern "C" fn(device: DeviceRaw, create_info: &RawShaderModuleCreateInfo, identifier: core::ptr::NonNull<RawShaderModuleIdentifierEXT>);
pub(crate) type GetShaderModuleIdentifierExt = extern "C" fn(device: DeviceRaw, shader_module: ShaderModuleRaw, identifier: core::ptr::NonNull<RawShaderModuleIdentifierEXT>);
pub(crate) type GetSwapchainCounterExt = extern "C" fn(device: DeviceRaw, swapchain: SwapchainKHRRaw, counter: RawSurfaceCounterFlagBitsEXT, counter_value: core::ptr::NonNull<u64>) -> RawResult;
pub(crate) type GetSwapchainImagesKhr = extern "C" fn(device: DeviceRaw, swapchain: SwapchainKHRRaw, swapchain_image_count: core::ptr::NonNull<u32>, swapchain_images: Option<core::ptr::NonNull<ImageRaw>>) -> RawResult;
pub(crate) type GetSwapchainStatusKhr = extern "C" fn(device: DeviceRaw, swapchain: SwapchainKHRMutRaw) -> RawResult;
pub(crate) type GetValidationCacheDataExt = extern "C" fn(device: DeviceRaw, validation_cache: ValidationCacheEXTRaw, data_size: core::ptr::NonNull<usize>, data: Option<core::ptr::NonNull<u8>>) -> RawResult;
pub(crate) type GetVideoSessionMemoryRequirementsKhr = extern "C" fn(device: DeviceRaw, video_session: VideoSessionKHRRaw, memory_requirements_count: core::ptr::NonNull<u32>, memory_requirements: Option<core::ptr::NonNull<RawVideoSessionMemoryRequirementsKHR>>) -> RawResult;
pub(crate) type GetWinrtDisplayNv = extern "C" fn(physical_device: PhysicalDeviceRaw, device_relative_id: u32, display: core::ptr::NonNull<DisplayKHRRaw>) -> RawResult;
pub(crate) type ImportFenceFdKhr = extern "C" fn(device: DeviceRaw, import_fence_fd_info: core::ptr::NonNull<RawImportFenceFdInfoKHR>) -> RawResult;
pub(crate) type ImportFenceWin32HandleKhr = extern "C" fn(device: DeviceRaw, import_fence_win32_handle_info: core::ptr::NonNull<RawImportFenceWin32HandleInfoKHR>) -> RawResult;
pub(crate) type ImportSemaphoreFdKhr = extern "C" fn(device: DeviceRaw, import_semaphore_fd_info: core::ptr::NonNull<RawImportSemaphoreFdInfoKHR>) -> RawResult;
pub(crate) type ImportSemaphoreWin32HandleKhr = extern "C" fn(device: DeviceRaw, import_semaphore_win32_handle_info: core::ptr::NonNull<RawImportSemaphoreWin32HandleInfoKHR>) -> RawResult;
pub(crate) type ImportSemaphoreZirconHandleFuchsia = extern "C" fn(device: DeviceRaw, import_semaphore_zircon_handle_info: core::ptr::NonNull<RawImportSemaphoreZirconHandleInfoFUCHSIA>) -> RawResult;
pub(crate) type InitializePerformanceApiIntel = extern "C" fn(device: DeviceRaw, initialize_info: &RawInitializePerformanceApiInfoINTEL) -> RawResult;
pub(crate) type InvalidateMappedMemoryRanges = extern "C" fn(device: DeviceRaw, memory_range_count: u32, memory_ranges: &RawMappedMemoryRange) -> RawResult;
pub(crate) type MapMemory = extern "C" fn(device: DeviceRaw, memory: DeviceMemoryMutRaw, offset: DeviceSize, size: DeviceSize, flags: MemoryMapFlags, data: core::ptr::NonNull<Option<core::ptr::NonNull<u8>>>) -> RawResult;
pub(crate) type MergePipelineCaches = extern "C" fn(device: DeviceRaw, dst_cache: PipelineCacheMutRaw, src_cache_count: u32, src_caches: &PipelineCacheRaw) -> RawResult;
pub(crate) type MergeValidationCachesExt = extern "C" fn(device: DeviceRaw, dst_cache: ValidationCacheEXTMutRaw, src_cache_count: u32, src_caches: &ValidationCacheEXTRaw) -> RawResult;
pub(crate) type QueueBeginDebugUtilsLabelExt = extern "C" fn(queue: QueueRaw, label_info: &RawDebugUtilsLabelEXT);
pub(crate) type QueueBindSparse = extern "C" fn(queue: QueueMutRaw, bind_info_count: u32, bind_info: Option<&RawBindSparseInfo>, fence: FenceMutRaw) -> RawResult;
pub(crate) type QueueEndDebugUtilsLabelExt = extern "C" fn(queue: QueueRaw);
pub(crate) type QueueInsertDebugUtilsLabelExt = extern "C" fn(queue: QueueRaw, label_info: &RawDebugUtilsLabelEXT);
pub(crate) type QueuePresentKhr = extern "C" fn(queue: QueueMutRaw, present_info: core::ptr::NonNull<RawPresentInfoKHR>) -> RawResult;
pub(crate) type QueueSetPerformanceConfigurationIntel = extern "C" fn(queue: QueueRaw, configuration: PerformanceConfigurationINTELRaw) -> RawResult;
pub(crate) type QueueSubmit = extern "C" fn(queue: QueueMutRaw, submit_count: u32, submits: Option<&RawSubmitInfo>, fence: FenceMutRaw) -> RawResult;
pub(crate) type QueueSubmit2 = extern "C" fn(queue: QueueMutRaw, submit_count: u32, submits: Option<&RawSubmitInfo2>, fence: FenceMutRaw) -> RawResult;
pub(crate) type QueueWaitIdle = extern "C" fn(queue: QueueMutRaw) -> RawResult;
pub(crate) type RegisterDeviceEventExt = extern "C" fn(device: DeviceRaw, device_event_info: &RawDeviceEventInfoEXT, allocator: Option<&RawAllocationCallbacks>, fence: core::ptr::NonNull<FenceRaw>) -> RawResult;
pub(crate) type RegisterDisplayEventExt = extern "C" fn(device: DeviceRaw, display: DisplayKHRRaw, display_event_info: &RawDisplayEventInfoEXT, allocator: Option<&RawAllocationCallbacks>, fence: core::ptr::NonNull<FenceRaw>) -> RawResult;
pub(crate) type ReleaseDisplayExt = extern "C" fn(physical_device: PhysicalDeviceRaw, display: DisplayKHRRaw) -> RawResult;
pub(crate) type ReleaseFullScreenExclusiveModeExt = extern "C" fn(device: DeviceRaw, swapchain: SwapchainKHRRaw) -> RawResult;
pub(crate) type ReleasePerformanceConfigurationIntel = extern "C" fn(device: DeviceRaw, configuration: PerformanceConfigurationINTELMutRaw) -> RawResult;
pub(crate) type ReleaseProfilingLockKhr = extern "C" fn(device: DeviceRaw);
pub(crate) type ReleaseSwapchainImagesExt = extern "C" fn(device: DeviceRaw, release_info: core::ptr::NonNull<RawReleaseSwapchainImagesInfoEXT>) -> RawResult;
pub(crate) type ResetCommandBuffer = extern "C" fn(command_buffer: CommandBufferMutRaw, flags: CommandBufferResetFlags) -> RawResult;
pub(crate) type ResetCommandPool = extern "C" fn(device: DeviceRaw, command_pool: CommandPoolMutRaw, flags: CommandPoolResetFlags) -> RawResult;
pub(crate) type ResetDescriptorPool = extern "C" fn(device: DeviceRaw, descriptor_pool: DescriptorPoolMutRaw, flags: DescriptorPoolResetFlags) -> RawResult;
pub(crate) type ResetEvent = extern "C" fn(device: DeviceRaw, event: EventMutRaw) -> RawResult;
pub(crate) type ResetFences = extern "C" fn(device: DeviceRaw, fence_count: u32, fences: core::ptr::NonNull<FenceMutRaw>) -> RawResult;
pub(crate) type ResetQueryPool = extern "C" fn(device: DeviceRaw, query_pool: QueryPoolRaw, first_query: u32, query_count: u32);
pub(crate) type SetBufferCollectionBufferConstraintsFuchsia = extern "C" fn(device: DeviceRaw, collection: BufferCollectionFUCHSIARaw, buffer_constraints_info: &RawBufferConstraintsInfoFUCHSIA) -> RawResult;
pub(crate) type SetBufferCollectionImageConstraintsFuchsia = extern "C" fn(device: DeviceRaw, collection: BufferCollectionFUCHSIARaw, image_constraints_info: &RawImageConstraintsInfoFUCHSIA) -> RawResult;
pub(crate) type SetDebugUtilsObjectNameExt = extern "C" fn(device: DeviceRaw, name_info: core::ptr::NonNull<RawDebugUtilsObjectNameInfoEXT>) -> RawResult;
pub(crate) type SetDebugUtilsObjectTagExt = extern "C" fn(device: DeviceRaw, tag_info: core::ptr::NonNull<RawDebugUtilsObjectTagInfoEXT>) -> RawResult;
pub(crate) type SetDeviceMemoryPriorityExt = extern "C" fn(device: DeviceRaw, memory: DeviceMemoryRaw, priority: f32);
pub(crate) type SetEvent = extern "C" fn(device: DeviceRaw, event: EventMutRaw) -> RawResult;
pub(crate) type SetHdrMetadataExt = extern "C" fn(device: DeviceRaw, swapchain_count: u32, swapchains: &SwapchainKHRRaw, metadata: &RawHdrMetadataEXT);
pub(crate) type SetLocalDimmingAmd = extern "C" fn(device: DeviceRaw, swap_chain: SwapchainKHRRaw, local_dimming_enable: Bool32);
pub(crate) type SetPrivateData = extern "C" fn(device: DeviceRaw, object_type: RawObjectType, object_handle: u64, private_data_slot: PrivateDataSlotRaw, data: u64) -> RawResult;
pub(crate) type SignalSemaphore = extern "C" fn(device: DeviceRaw, signal_info: &RawSemaphoreSignalInfo) -> RawResult;
pub(crate) type SubmitDebugUtilsMessageExt = extern "C" fn(instance: InstanceRaw, message_severity: RawDebugUtilsMessageSeverityFlagBitsEXT, message_types: DebugUtilsMessageTypeFlagsEXT, callback_data: &RawDebugUtilsMessengerCallbackDataEXT);
pub(crate) type TrimCommandPool = extern "C" fn(device: DeviceRaw, command_pool: CommandPoolMutRaw, flags: CommandPoolTrimFlags);
pub(crate) type UninitializePerformanceApiIntel = extern "C" fn(device: DeviceRaw);
pub(crate) type UnmapMemory = extern "C" fn(device: DeviceRaw, memory: DeviceMemoryMutRaw);
pub(crate) type UpdateDescriptorSetWithTemplate = extern "C" fn(device: DeviceRaw, descriptor_set: DescriptorSetRaw, descriptor_update_template: DescriptorUpdateTemplateRaw, data: Option<&u8>);
pub(crate) type UpdateDescriptorSets = extern "C" fn(device: DeviceRaw, descriptor_write_count: u32, descriptor_writes: Option<&RawWriteDescriptorSet>, descriptor_copy_count: u32, descriptor_copies: Option<&RawCopyDescriptorSet>);
pub(crate) type UpdateVideoSessionParametersKhr = extern "C" fn(device: DeviceRaw, video_session_parameters: VideoSessionParametersKHRRaw, update_info: &RawVideoSessionParametersUpdateInfoKHR) -> RawResult;
pub(crate) type WaitForFences = extern "C" fn(device: DeviceRaw, fence_count: u32, fences: &FenceRaw, wait_all: Bool32, timeout: u64) -> RawResult;
pub(crate) type WaitForPresentKhr = extern "C" fn(device: DeviceRaw, swapchain: SwapchainKHRMutRaw, present_id: u64, timeout: u64) -> RawResult;
pub(crate) type WaitSemaphores = extern "C" fn(device: DeviceRaw, wait_info: &RawSemaphoreWaitInfo, timeout: u64) -> RawResult;
pub(crate) type WriteAccelerationStructuresPropertiesKhr = extern "C" fn(device: DeviceRaw, acceleration_structure_count: u32, acceleration_structures: &AccelerationStructureKHRRaw, query_type: RawQueryType, data_size: usize, data: core::ptr::NonNull<u8>, stride: usize) -> RawResult;
pub(crate) type WriteMicromapsPropertiesExt = extern "C" fn(device: DeviceRaw, micromap_count: u32, micromaps: &MicromapEXTRaw, query_type: RawQueryType, data_size: usize, data: core::ptr::NonNull<u8>, stride: usize) -> RawResult;

pub(crate) mod invalid_functions {
    use super::*;
    pub extern "C" fn acquire_drm_display_ext (_: PhysicalDeviceRaw, _: i32, _: DisplayKHRRaw) -> RawResult { panic!("vkAcquireDrmDisplayEXT was not loaded successfully") }
    pub extern "C" fn acquire_full_screen_exclusive_mode_ext (_: DeviceRaw, _: SwapchainKHRRaw) -> RawResult { panic!("vkAcquireFullScreenExclusiveModeEXT was not loaded successfully") }
    pub extern "C" fn acquire_next_image2_khr (_: DeviceRaw, _: core::ptr::NonNull<RawAcquireNextImageInfoKHR>, _: core::ptr::NonNull<u32>) -> RawResult { panic!("vkAcquireNextImage2KHR was not loaded successfully") }
    pub extern "C" fn acquire_next_image_khr (_: DeviceRaw, _: SwapchainKHRMutRaw, _: u64, _: SemaphoreMutRaw, _: FenceMutRaw, _: core::ptr::NonNull<u32>) -> RawResult { panic!("vkAcquireNextImageKHR was not loaded successfully") }
    pub extern "C" fn acquire_performance_configuration_intel (_: DeviceRaw, _: &RawPerformanceConfigurationAcquireInfoINTEL, _: core::ptr::NonNull<PerformanceConfigurationINTELRaw>) -> RawResult { panic!("vkAcquirePerformanceConfigurationINTEL was not loaded successfully") }
    pub extern "C" fn acquire_profiling_lock_khr (_: DeviceRaw, _: &RawAcquireProfilingLockInfoKHR) -> RawResult { panic!("vkAcquireProfilingLockKHR was not loaded successfully") }
    pub extern "C" fn acquire_winrt_display_nv (_: PhysicalDeviceRaw, _: DisplayKHRRaw) -> RawResult { panic!("vkAcquireWinrtDisplayNV was not loaded successfully") }
    pub extern "C" fn acquire_xlib_display_ext (_: PhysicalDeviceRaw, _: core::ptr::NonNull<Display>, _: DisplayKHRRaw) -> RawResult { panic!("vkAcquireXlibDisplayEXT was not loaded successfully") }
    pub extern "C" fn allocate_command_buffers (_: DeviceRaw, _: core::ptr::NonNull<RawCommandBufferAllocateInfo>, _: core::ptr::NonNull<CommandBufferRaw>) -> RawResult { panic!("vkAllocateCommandBuffers was not loaded successfully") }
    pub extern "C" fn allocate_descriptor_sets (_: DeviceRaw, _: core::ptr::NonNull<RawDescriptorSetAllocateInfo>, _: core::ptr::NonNull<DescriptorSetRaw>) -> RawResult { panic!("vkAllocateDescriptorSets was not loaded successfully") }
    pub extern "C" fn allocate_memory (_: DeviceRaw, _: &RawMemoryAllocateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DeviceMemoryRaw>) -> RawResult { panic!("vkAllocateMemory was not loaded successfully") }
    pub extern "C" fn begin_command_buffer (_: CommandBufferMutRaw, _: &RawCommandBufferBeginInfo) -> RawResult { panic!("vkBeginCommandBuffer was not loaded successfully") }
    pub extern "C" fn bind_acceleration_structure_memory_nv (_: DeviceRaw, _: u32, _: &RawBindAccelerationStructureMemoryInfoNV) -> RawResult { panic!("vkBindAccelerationStructureMemoryNV was not loaded successfully") }
    pub extern "C" fn bind_buffer_memory (_: DeviceRaw, _: BufferMutRaw, _: DeviceMemoryRaw, _: DeviceSize) -> RawResult { panic!("vkBindBufferMemory was not loaded successfully") }
    pub extern "C" fn bind_buffer_memory2 (_: DeviceRaw, _: u32, _: &RawBindBufferMemoryInfo) -> RawResult { panic!("vkBindBufferMemory2 was not loaded successfully") }
    pub extern "C" fn bind_image_memory (_: DeviceRaw, _: ImageMutRaw, _: DeviceMemoryRaw, _: DeviceSize) -> RawResult { panic!("vkBindImageMemory was not loaded successfully") }
    pub extern "C" fn bind_image_memory2 (_: DeviceRaw, _: u32, _: &RawBindImageMemoryInfo) -> RawResult { panic!("vkBindImageMemory2 was not loaded successfully") }
    pub extern "C" fn bind_optical_flow_session_image_nv (_: DeviceRaw, _: OpticalFlowSessionNVRaw, _: RawOpticalFlowSessionBindingPointNV, _: ImageViewRaw, _: RawImageLayout) -> RawResult { panic!("vkBindOpticalFlowSessionImageNV was not loaded successfully") }
    pub extern "C" fn bind_video_session_memory_khr (_: DeviceRaw, _: VideoSessionKHRMutRaw, _: u32, _: &RawBindVideoSessionMemoryInfoKHR) -> RawResult { panic!("vkBindVideoSessionMemoryKHR was not loaded successfully") }
    pub extern "C" fn build_acceleration_structures_khr (_: DeviceRaw, _: DeferredOperationKHRRaw, _: u32, _: &RawAccelerationStructureBuildGeometryInfoKHR, _: &&RawAccelerationStructureBuildRangeInfoKHR) -> RawResult { panic!("vkBuildAccelerationStructuresKHR was not loaded successfully") }
    pub extern "C" fn build_micromaps_ext (_: DeviceRaw, _: DeferredOperationKHRRaw, _: u32, _: &RawMicromapBuildInfoEXT) -> RawResult { panic!("vkBuildMicromapsEXT was not loaded successfully") }
    pub extern "C" fn cmd_begin_conditional_rendering_ext (_: CommandBufferMutRaw, _: &RawConditionalRenderingBeginInfoEXT) { panic!("vkCmdBeginConditionalRenderingEXT was not loaded successfully") }
    pub extern "C" fn cmd_begin_debug_utils_label_ext (_: CommandBufferMutRaw, _: &RawDebugUtilsLabelEXT) { panic!("vkCmdBeginDebugUtilsLabelEXT was not loaded successfully") }
    pub extern "C" fn cmd_begin_query (_: CommandBufferMutRaw, _: QueryPoolRaw, _: u32, _: QueryControlFlags) { panic!("vkCmdBeginQuery was not loaded successfully") }
    pub extern "C" fn cmd_begin_query_indexed_ext (_: CommandBufferMutRaw, _: QueryPoolRaw, _: u32, _: QueryControlFlags, _: u32) { panic!("vkCmdBeginQueryIndexedEXT was not loaded successfully") }
    pub extern "C" fn cmd_begin_render_pass (_: CommandBufferMutRaw, _: &RawRenderPassBeginInfo, _: RawSubpassContents) { panic!("vkCmdBeginRenderPass was not loaded successfully") }
    pub extern "C" fn cmd_begin_render_pass2 (_: CommandBufferMutRaw, _: &RawRenderPassBeginInfo, _: &RawSubpassBeginInfo) { panic!("vkCmdBeginRenderPass2 was not loaded successfully") }
    pub extern "C" fn cmd_begin_rendering (_: CommandBufferMutRaw, _: &RawRenderingInfo) { panic!("vkCmdBeginRendering was not loaded successfully") }
    pub extern "C" fn cmd_begin_transform_feedback_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: Option<&BufferRaw>, _: Option<&DeviceSize>) { panic!("vkCmdBeginTransformFeedbackEXT was not loaded successfully") }
    pub extern "C" fn cmd_begin_video_coding_khr (_: CommandBufferMutRaw, _: &RawVideoBeginCodingInfoKHR) { panic!("vkCmdBeginVideoCodingKHR was not loaded successfully") }
    pub extern "C" fn cmd_bind_descriptor_buffer_embedded_samplers_ext (_: CommandBufferMutRaw, _: RawPipelineBindPoint, _: PipelineLayoutRaw, _: u32) { panic!("vkCmdBindDescriptorBufferEmbeddedSamplersEXT was not loaded successfully") }
    pub extern "C" fn cmd_bind_descriptor_buffers_ext (_: CommandBufferMutRaw, _: u32, _: &RawDescriptorBufferBindingInfoEXT) { panic!("vkCmdBindDescriptorBuffersEXT was not loaded successfully") }
    pub extern "C" fn cmd_bind_descriptor_sets (_: CommandBufferMutRaw, _: RawPipelineBindPoint, _: PipelineLayoutRaw, _: u32, _: u32, _: &DescriptorSetRaw, _: u32, _: Option<&u32>) { panic!("vkCmdBindDescriptorSets was not loaded successfully") }
    pub extern "C" fn cmd_bind_index_buffer (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: RawIndexType) { panic!("vkCmdBindIndexBuffer was not loaded successfully") }
    pub extern "C" fn cmd_bind_invocation_mask_huawei (_: CommandBufferMutRaw, _: ImageViewRaw, _: RawImageLayout) { panic!("vkCmdBindInvocationMaskHUAWEI was not loaded successfully") }
    pub extern "C" fn cmd_bind_pipeline (_: CommandBufferMutRaw, _: RawPipelineBindPoint, _: PipelineRaw) { panic!("vkCmdBindPipeline was not loaded successfully") }
    pub extern "C" fn cmd_bind_pipeline_shader_group_nv (_: CommandBufferMutRaw, _: RawPipelineBindPoint, _: PipelineRaw, _: u32) { panic!("vkCmdBindPipelineShaderGroupNV was not loaded successfully") }
    pub extern "C" fn cmd_bind_shading_rate_image_nv (_: CommandBufferMutRaw, _: ImageViewRaw, _: RawImageLayout) { panic!("vkCmdBindShadingRateImageNV was not loaded successfully") }
    pub extern "C" fn cmd_bind_transform_feedback_buffers_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: &BufferRaw, _: &DeviceSize, _: Option<&DeviceSize>) { panic!("vkCmdBindTransformFeedbackBuffersEXT was not loaded successfully") }
    pub extern "C" fn cmd_bind_vertex_buffers (_: CommandBufferMutRaw, _: u32, _: u32, _: &BufferRaw, _: &DeviceSize) { panic!("vkCmdBindVertexBuffers was not loaded successfully") }
    pub extern "C" fn cmd_bind_vertex_buffers2 (_: CommandBufferMutRaw, _: u32, _: u32, _: &BufferRaw, _: &DeviceSize, _: Option<&DeviceSize>, _: Option<&DeviceSize>) { panic!("vkCmdBindVertexBuffers2 was not loaded successfully") }
    pub extern "C" fn cmd_blit_image (_: CommandBufferMutRaw, _: ImageRaw, _: RawImageLayout, _: ImageRaw, _: RawImageLayout, _: u32, _: &RawImageBlit, _: RawFilter) { panic!("vkCmdBlitImage was not loaded successfully") }
    pub extern "C" fn cmd_blit_image2 (_: CommandBufferMutRaw, _: &RawBlitImageInfo2) { panic!("vkCmdBlitImage2 was not loaded successfully") }
    pub extern "C" fn cmd_build_acceleration_structure_nv (_: CommandBufferMutRaw, _: &RawAccelerationStructureInfoNV, _: BufferRaw, _: DeviceSize, _: Bool32, _: AccelerationStructureNVRaw, _: AccelerationStructureNVRaw, _: BufferRaw, _: DeviceSize) { panic!("vkCmdBuildAccelerationStructureNV was not loaded successfully") }
    pub extern "C" fn cmd_build_acceleration_structures_indirect_khr (_: CommandBufferMutRaw, _: u32, _: &RawAccelerationStructureBuildGeometryInfoKHR, _: &DeviceAddress, _: &u32, _: &&u32) { panic!("vkCmdBuildAccelerationStructuresIndirectKHR was not loaded successfully") }
    pub extern "C" fn cmd_build_acceleration_structures_khr (_: CommandBufferMutRaw, _: u32, _: &RawAccelerationStructureBuildGeometryInfoKHR, _: &&RawAccelerationStructureBuildRangeInfoKHR) { panic!("vkCmdBuildAccelerationStructuresKHR was not loaded successfully") }
    pub extern "C" fn cmd_build_micromaps_ext (_: CommandBufferMutRaw, _: u32, _: &RawMicromapBuildInfoEXT) { panic!("vkCmdBuildMicromapsEXT was not loaded successfully") }
    pub extern "C" fn cmd_clear_attachments (_: CommandBufferMutRaw, _: u32, _: &RawClearAttachment, _: u32, _: &RawClearRect) { panic!("vkCmdClearAttachments was not loaded successfully") }
    pub extern "C" fn cmd_clear_colour_image (_: CommandBufferMutRaw, _: ImageRaw, _: RawImageLayout, _: Option<&RawClearColourValue>, _: u32, _: &RawImageSubresourceRange) { panic!("vkCmdClearColorImage was not loaded successfully") }
    pub extern "C" fn cmd_clear_depth_stencil_image (_: CommandBufferMutRaw, _: ImageRaw, _: RawImageLayout, _: &RawClearDepthStencilValue, _: u32, _: &RawImageSubresourceRange) { panic!("vkCmdClearDepthStencilImage was not loaded successfully") }
    pub extern "C" fn cmd_control_video_coding_khr (_: CommandBufferMutRaw, _: &RawVideoCodingControlInfoKHR) { panic!("vkCmdControlVideoCodingKHR was not loaded successfully") }
    pub extern "C" fn cmd_copy_acceleration_structure_khr (_: CommandBufferMutRaw, _: &RawCopyAccelerationStructureInfoKHR) { panic!("vkCmdCopyAccelerationStructureKHR was not loaded successfully") }
    pub extern "C" fn cmd_copy_acceleration_structure_nv (_: CommandBufferMutRaw, _: AccelerationStructureNVRaw, _: AccelerationStructureNVRaw, _: RawCopyAccelerationStructureModeKHR) { panic!("vkCmdCopyAccelerationStructureNV was not loaded successfully") }
    pub extern "C" fn cmd_copy_acceleration_structure_to_memory_khr (_: CommandBufferMutRaw, _: &RawCopyAccelerationStructureToMemoryInfoKHR) { panic!("vkCmdCopyAccelerationStructureToMemoryKHR was not loaded successfully") }
    pub extern "C" fn cmd_copy_buffer (_: CommandBufferMutRaw, _: BufferRaw, _: BufferRaw, _: u32, _: &RawBufferCopy) { panic!("vkCmdCopyBuffer was not loaded successfully") }
    pub extern "C" fn cmd_copy_buffer2 (_: CommandBufferMutRaw, _: &RawCopyBufferInfo2) { panic!("vkCmdCopyBuffer2 was not loaded successfully") }
    pub extern "C" fn cmd_copy_buffer_to_image (_: CommandBufferMutRaw, _: BufferRaw, _: ImageRaw, _: RawImageLayout, _: u32, _: &RawBufferImageCopy) { panic!("vkCmdCopyBufferToImage was not loaded successfully") }
    pub extern "C" fn cmd_copy_buffer_to_image2 (_: CommandBufferMutRaw, _: &RawCopyBufferToImageInfo2) { panic!("vkCmdCopyBufferToImage2 was not loaded successfully") }
    pub extern "C" fn cmd_copy_image (_: CommandBufferMutRaw, _: ImageRaw, _: RawImageLayout, _: ImageRaw, _: RawImageLayout, _: u32, _: &RawImageCopy) { panic!("vkCmdCopyImage was not loaded successfully") }
    pub extern "C" fn cmd_copy_image2 (_: CommandBufferMutRaw, _: &RawCopyImageInfo2) { panic!("vkCmdCopyImage2 was not loaded successfully") }
    pub extern "C" fn cmd_copy_image_to_buffer (_: CommandBufferMutRaw, _: ImageRaw, _: RawImageLayout, _: BufferRaw, _: u32, _: &RawBufferImageCopy) { panic!("vkCmdCopyImageToBuffer was not loaded successfully") }
    pub extern "C" fn cmd_copy_image_to_buffer2 (_: CommandBufferMutRaw, _: &RawCopyImageToBufferInfo2) { panic!("vkCmdCopyImageToBuffer2 was not loaded successfully") }
    pub extern "C" fn cmd_copy_memory_indirect_nv (_: CommandBufferMutRaw, _: DeviceAddress, _: u32, _: u32) { panic!("vkCmdCopyMemoryIndirectNV was not loaded successfully") }
    pub extern "C" fn cmd_copy_memory_to_acceleration_structure_khr (_: CommandBufferMutRaw, _: &RawCopyMemoryToAccelerationStructureInfoKHR) { panic!("vkCmdCopyMemoryToAccelerationStructureKHR was not loaded successfully") }
    pub extern "C" fn cmd_copy_memory_to_image_indirect_nv (_: CommandBufferMutRaw, _: DeviceAddress, _: u32, _: u32, _: ImageRaw, _: RawImageLayout, _: &RawImageSubresourceLayers) { panic!("vkCmdCopyMemoryToImageIndirectNV was not loaded successfully") }
    pub extern "C" fn cmd_copy_memory_to_micromap_ext (_: CommandBufferMutRaw, _: &RawCopyMemoryToMicromapInfoEXT) { panic!("vkCmdCopyMemoryToMicromapEXT was not loaded successfully") }
    pub extern "C" fn cmd_copy_micromap_ext (_: CommandBufferMutRaw, _: &RawCopyMicromapInfoEXT) { panic!("vkCmdCopyMicromapEXT was not loaded successfully") }
    pub extern "C" fn cmd_copy_micromap_to_memory_ext (_: CommandBufferMutRaw, _: &RawCopyMicromapToMemoryInfoEXT) { panic!("vkCmdCopyMicromapToMemoryEXT was not loaded successfully") }
    pub extern "C" fn cmd_copy_query_pool_results (_: CommandBufferMutRaw, _: QueryPoolRaw, _: u32, _: u32, _: BufferRaw, _: DeviceSize, _: DeviceSize, _: QueryResultFlags) { panic!("vkCmdCopyQueryPoolResults was not loaded successfully") }
    pub extern "C" fn cmd_cu_launch_kernel_nvx (_: CommandBufferRaw, _: &RawCuLaunchInfoNVX) { panic!("vkCmdCuLaunchKernelNVX was not loaded successfully") }
    pub extern "C" fn cmd_debug_marker_begin_ext (_: CommandBufferMutRaw, _: &RawDebugMarkerMarkerInfoEXT) { panic!("vkCmdDebugMarkerBeginEXT was not loaded successfully") }
    pub extern "C" fn cmd_debug_marker_end_ext (_: CommandBufferMutRaw) { panic!("vkCmdDebugMarkerEndEXT was not loaded successfully") }
    pub extern "C" fn cmd_debug_marker_insert_ext (_: CommandBufferMutRaw, _: &RawDebugMarkerMarkerInfoEXT) { panic!("vkCmdDebugMarkerInsertEXT was not loaded successfully") }
    pub extern "C" fn cmd_decode_video_khr (_: CommandBufferMutRaw, _: &RawVideoDecodeInfoKHR) { panic!("vkCmdDecodeVideoKHR was not loaded successfully") }
    pub extern "C" fn cmd_decompress_memory_indirect_count_nv (_: CommandBufferMutRaw, _: DeviceAddress, _: DeviceAddress, _: u32) { panic!("vkCmdDecompressMemoryIndirectCountNV was not loaded successfully") }
    pub extern "C" fn cmd_decompress_memory_nv (_: CommandBufferMutRaw, _: u32, _: &RawDecompressMemoryRegionNV) { panic!("vkCmdDecompressMemoryNV was not loaded successfully") }
    pub extern "C" fn cmd_dispatch (_: CommandBufferMutRaw, _: u32, _: u32, _: u32) { panic!("vkCmdDispatch was not loaded successfully") }
    pub extern "C" fn cmd_dispatch_base (_: CommandBufferMutRaw, _: u32, _: u32, _: u32, _: u32, _: u32, _: u32) { panic!("vkCmdDispatchBase was not loaded successfully") }
    pub extern "C" fn cmd_dispatch_indirect (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize) { panic!("vkCmdDispatchIndirect was not loaded successfully") }
    pub extern "C" fn cmd_draw (_: CommandBufferMutRaw, _: u32, _: u32, _: u32, _: u32) { panic!("vkCmdDraw was not loaded successfully") }
    pub extern "C" fn cmd_draw_cluster_huawei (_: CommandBufferMutRaw, _: u32, _: u32, _: u32) { panic!("vkCmdDrawClusterHUAWEI was not loaded successfully") }
    pub extern "C" fn cmd_draw_cluster_indirect_huawei (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize) { panic!("vkCmdDrawClusterIndirectHUAWEI was not loaded successfully") }
    pub extern "C" fn cmd_draw_indexed (_: CommandBufferMutRaw, _: u32, _: u32, _: u32, _: i32, _: u32) { panic!("vkCmdDrawIndexed was not loaded successfully") }
    pub extern "C" fn cmd_draw_indexed_indirect (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("vkCmdDrawIndexedIndirect was not loaded successfully") }
    pub extern "C" fn cmd_draw_indexed_indirect_count (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("vkCmdDrawIndexedIndirectCount was not loaded successfully") }
    pub extern "C" fn cmd_draw_indirect (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("vkCmdDrawIndirect was not loaded successfully") }
    pub extern "C" fn cmd_draw_indirect_byte_count_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("vkCmdDrawIndirectByteCountEXT was not loaded successfully") }
    pub extern "C" fn cmd_draw_indirect_count (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("vkCmdDrawIndirectCount was not loaded successfully") }
    pub extern "C" fn cmd_draw_mesh_tasks_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: u32) { panic!("vkCmdDrawMeshTasksEXT was not loaded successfully") }
    pub extern "C" fn cmd_draw_mesh_tasks_indirect_count_ext (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("vkCmdDrawMeshTasksIndirectCountEXT was not loaded successfully") }
    pub extern "C" fn cmd_draw_mesh_tasks_indirect_count_nv (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("vkCmdDrawMeshTasksIndirectCountNV was not loaded successfully") }
    pub extern "C" fn cmd_draw_mesh_tasks_indirect_ext (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("vkCmdDrawMeshTasksIndirectEXT was not loaded successfully") }
    pub extern "C" fn cmd_draw_mesh_tasks_indirect_nv (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("vkCmdDrawMeshTasksIndirectNV was not loaded successfully") }
    pub extern "C" fn cmd_draw_mesh_tasks_nv (_: CommandBufferMutRaw, _: u32, _: u32) { panic!("vkCmdDrawMeshTasksNV was not loaded successfully") }
    pub extern "C" fn cmd_draw_multi_ext (_: CommandBufferMutRaw, _: u32, _: Option<&RawMultiDrawInfoEXT>, _: u32, _: u32, _: u32) { panic!("vkCmdDrawMultiEXT was not loaded successfully") }
    pub extern "C" fn cmd_draw_multi_indexed_ext (_: CommandBufferMutRaw, _: u32, _: Option<&RawMultiDrawIndexedInfoEXT>, _: u32, _: u32, _: u32, _: Option<&i32>) { panic!("vkCmdDrawMultiIndexedEXT was not loaded successfully") }
    pub extern "C" fn cmd_end_conditional_rendering_ext (_: CommandBufferMutRaw) { panic!("vkCmdEndConditionalRenderingEXT was not loaded successfully") }
    pub extern "C" fn cmd_end_debug_utils_label_ext (_: CommandBufferMutRaw) { panic!("vkCmdEndDebugUtilsLabelEXT was not loaded successfully") }
    pub extern "C" fn cmd_end_query (_: CommandBufferMutRaw, _: QueryPoolRaw, _: u32) { panic!("vkCmdEndQuery was not loaded successfully") }
    pub extern "C" fn cmd_end_query_indexed_ext (_: CommandBufferMutRaw, _: QueryPoolRaw, _: u32, _: u32) { panic!("vkCmdEndQueryIndexedEXT was not loaded successfully") }
    pub extern "C" fn cmd_end_render_pass (_: CommandBufferMutRaw) { panic!("vkCmdEndRenderPass was not loaded successfully") }
    pub extern "C" fn cmd_end_render_pass2 (_: CommandBufferMutRaw, _: &RawSubpassEndInfo) { panic!("vkCmdEndRenderPass2 was not loaded successfully") }
    pub extern "C" fn cmd_end_rendering (_: CommandBufferMutRaw) { panic!("vkCmdEndRendering was not loaded successfully") }
    pub extern "C" fn cmd_end_transform_feedback_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: Option<&BufferRaw>, _: Option<&DeviceSize>) { panic!("vkCmdEndTransformFeedbackEXT was not loaded successfully") }
    pub extern "C" fn cmd_end_video_coding_khr (_: CommandBufferMutRaw, _: &RawVideoEndCodingInfoKHR) { panic!("vkCmdEndVideoCodingKHR was not loaded successfully") }
    pub extern "C" fn cmd_execute_commands (_: CommandBufferMutRaw, _: u32, _: &CommandBufferRaw) { panic!("vkCmdExecuteCommands was not loaded successfully") }
    pub extern "C" fn cmd_execute_generated_commands_nv (_: CommandBufferMutRaw, _: Bool32, _: &RawGeneratedCommandsInfoNV) { panic!("vkCmdExecuteGeneratedCommandsNV was not loaded successfully") }
    pub extern "C" fn cmd_fill_buffer (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: DeviceSize, _: u32) { panic!("vkCmdFillBuffer was not loaded successfully") }
    pub extern "C" fn cmd_insert_debug_utils_label_ext (_: CommandBufferMutRaw, _: &RawDebugUtilsLabelEXT) { panic!("vkCmdInsertDebugUtilsLabelEXT was not loaded successfully") }
    pub extern "C" fn cmd_next_subpass (_: CommandBufferMutRaw, _: RawSubpassContents) { panic!("vkCmdNextSubpass was not loaded successfully") }
    pub extern "C" fn cmd_next_subpass2 (_: CommandBufferMutRaw, _: &RawSubpassBeginInfo, _: &RawSubpassEndInfo) { panic!("vkCmdNextSubpass2 was not loaded successfully") }
    pub extern "C" fn cmd_optical_flow_execute_nv (_: CommandBufferRaw, _: OpticalFlowSessionNVRaw, _: &RawOpticalFlowExecuteInfoNV) { panic!("vkCmdOpticalFlowExecuteNV was not loaded successfully") }
    pub extern "C" fn cmd_pipeline_barrier (_: CommandBufferMutRaw, _: PipelineStageFlags, _: PipelineStageFlags, _: DependencyFlags, _: u32, _: Option<&RawMemoryBarrier>, _: u32, _: Option<&RawBufferMemoryBarrier>, _: u32, _: Option<&RawImageMemoryBarrier>) { panic!("vkCmdPipelineBarrier was not loaded successfully") }
    pub extern "C" fn cmd_pipeline_barrier2 (_: CommandBufferMutRaw, _: &RawDependencyInfo) { panic!("vkCmdPipelineBarrier2 was not loaded successfully") }
    pub extern "C" fn cmd_preprocess_generated_commands_nv (_: CommandBufferMutRaw, _: &RawGeneratedCommandsInfoNV) { panic!("vkCmdPreprocessGeneratedCommandsNV was not loaded successfully") }
    pub extern "C" fn cmd_push_constants (_: CommandBufferMutRaw, _: PipelineLayoutRaw, _: ShaderStageFlags, _: u32, _: u32, _: &u8) { panic!("vkCmdPushConstants was not loaded successfully") }
    pub extern "C" fn cmd_push_descriptor_set_khr (_: CommandBufferMutRaw, _: RawPipelineBindPoint, _: PipelineLayoutRaw, _: u32, _: u32, _: &RawWriteDescriptorSet) { panic!("vkCmdPushDescriptorSetKHR was not loaded successfully") }
    pub extern "C" fn cmd_push_descriptor_set_with_template_khr (_: CommandBufferMutRaw, _: DescriptorUpdateTemplateRaw, _: PipelineLayoutRaw, _: u32, _: Option<&u8>) { panic!("vkCmdPushDescriptorSetWithTemplateKHR was not loaded successfully") }
    pub extern "C" fn cmd_reset_event (_: CommandBufferMutRaw, _: EventRaw, _: PipelineStageFlags) { panic!("vkCmdResetEvent was not loaded successfully") }
    pub extern "C" fn cmd_reset_event2 (_: CommandBufferMutRaw, _: EventRaw, _: PipelineStageFlags2) { panic!("vkCmdResetEvent2 was not loaded successfully") }
    pub extern "C" fn cmd_reset_query_pool (_: CommandBufferMutRaw, _: QueryPoolRaw, _: u32, _: u32) { panic!("vkCmdResetQueryPool was not loaded successfully") }
    pub extern "C" fn cmd_resolve_image (_: CommandBufferMutRaw, _: ImageRaw, _: RawImageLayout, _: ImageRaw, _: RawImageLayout, _: u32, _: &RawImageResolve) { panic!("vkCmdResolveImage was not loaded successfully") }
    pub extern "C" fn cmd_resolve_image2 (_: CommandBufferMutRaw, _: &RawResolveImageInfo2) { panic!("vkCmdResolveImage2 was not loaded successfully") }
    pub extern "C" fn cmd_set_alpha_to_coverage_enable_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetAlphaToCoverageEnableEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_alpha_to_one_enable_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetAlphaToOneEnableEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_blend_constants (_: CommandBufferMutRaw, _: ConstSizePtr<f32, 4>) { panic!("vkCmdSetBlendConstants was not loaded successfully") }
    pub extern "C" fn cmd_set_checkpoint_nv (_: CommandBufferMutRaw, _: Option<&u8>) { panic!("vkCmdSetCheckpointNV was not loaded successfully") }
    pub extern "C" fn cmd_set_coarse_sample_order_nv (_: CommandBufferMutRaw, _: RawCoarseSampleOrderTypeNV, _: u32, _: Option<&RawCoarseSampleOrderCustomNV>) { panic!("vkCmdSetCoarseSampleOrderNV was not loaded successfully") }
    pub extern "C" fn cmd_set_colour_blend_advanced_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawColourBlendAdvancedEXT) { panic!("vkCmdSetColorBlendAdvancedEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_colour_blend_enable_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: &Bool32) { panic!("vkCmdSetColorBlendEnableEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_colour_blend_equation_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawColourBlendEquationEXT) { panic!("vkCmdSetColorBlendEquationEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_colour_write_enable_ext (_: CommandBufferMutRaw, _: u32, _: &Bool32) { panic!("vkCmdSetColorWriteEnableEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_colour_write_mask_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: &ColourComponentFlags) { panic!("vkCmdSetColorWriteMaskEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_conservative_rasterization_mode_ext (_: CommandBufferMutRaw, _: RawConservativeRasterizationModeEXT) { panic!("vkCmdSetConservativeRasterizationModeEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_coverage_modulation_mode_nv (_: CommandBufferMutRaw, _: RawCoverageModulationModeNV) { panic!("vkCmdSetCoverageModulationModeNV was not loaded successfully") }
    pub extern "C" fn cmd_set_coverage_modulation_table_enable_nv (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetCoverageModulationTableEnableNV was not loaded successfully") }
    pub extern "C" fn cmd_set_coverage_modulation_table_nv (_: CommandBufferMutRaw, _: u32, _: &f32) { panic!("vkCmdSetCoverageModulationTableNV was not loaded successfully") }
    pub extern "C" fn cmd_set_coverage_reduction_mode_nv (_: CommandBufferMutRaw, _: RawCoverageReductionModeNV) { panic!("vkCmdSetCoverageReductionModeNV was not loaded successfully") }
    pub extern "C" fn cmd_set_coverage_to_colour_enable_nv (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetCoverageToColorEnableNV was not loaded successfully") }
    pub extern "C" fn cmd_set_coverage_to_colour_location_nv (_: CommandBufferMutRaw, _: u32) { panic!("vkCmdSetCoverageToColorLocationNV was not loaded successfully") }
    pub extern "C" fn cmd_set_cull_mode (_: CommandBufferMutRaw, _: CullModeFlags) { panic!("vkCmdSetCullMode was not loaded successfully") }
    pub extern "C" fn cmd_set_depth_bias (_: CommandBufferMutRaw, _: f32, _: f32, _: f32) { panic!("vkCmdSetDepthBias was not loaded successfully") }
    pub extern "C" fn cmd_set_depth_bias_enable (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetDepthBiasEnable was not loaded successfully") }
    pub extern "C" fn cmd_set_depth_bounds (_: CommandBufferMutRaw, _: f32, _: f32) { panic!("vkCmdSetDepthBounds was not loaded successfully") }
    pub extern "C" fn cmd_set_depth_bounds_test_enable (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetDepthBoundsTestEnable was not loaded successfully") }
    pub extern "C" fn cmd_set_depth_clamp_enable_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetDepthClampEnableEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_depth_clip_enable_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetDepthClipEnableEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_depth_clip_negative_one_to_one_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetDepthClipNegativeOneToOneEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_depth_compare_op (_: CommandBufferMutRaw, _: RawCompareOp) { panic!("vkCmdSetDepthCompareOp was not loaded successfully") }
    pub extern "C" fn cmd_set_depth_test_enable (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetDepthTestEnable was not loaded successfully") }
    pub extern "C" fn cmd_set_depth_write_enable (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetDepthWriteEnable was not loaded successfully") }
    pub extern "C" fn cmd_set_descriptor_buffer_offsets_ext (_: CommandBufferMutRaw, _: RawPipelineBindPoint, _: PipelineLayoutRaw, _: u32, _: u32, _: &u32, _: &DeviceSize) { panic!("vkCmdSetDescriptorBufferOffsetsEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_device_mask (_: CommandBufferMutRaw, _: u32) { panic!("vkCmdSetDeviceMask was not loaded successfully") }
    pub extern "C" fn cmd_set_discard_rectangle_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawRect2D) { panic!("vkCmdSetDiscardRectangleEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_discard_rectangle_enable_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetDiscardRectangleEnableEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_discard_rectangle_mode_ext (_: CommandBufferMutRaw, _: RawDiscardRectangleModeEXT) { panic!("vkCmdSetDiscardRectangleModeEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_event (_: CommandBufferMutRaw, _: EventRaw, _: PipelineStageFlags) { panic!("vkCmdSetEvent was not loaded successfully") }
    pub extern "C" fn cmd_set_event2 (_: CommandBufferMutRaw, _: EventRaw, _: &RawDependencyInfo) { panic!("vkCmdSetEvent2 was not loaded successfully") }
    pub extern "C" fn cmd_set_exclusive_scissor_enable_nv (_: CommandBufferMutRaw, _: u32, _: u32, _: &Bool32) { panic!("vkCmdSetExclusiveScissorEnableNV was not loaded successfully") }
    pub extern "C" fn cmd_set_exclusive_scissor_nv (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawRect2D) { panic!("vkCmdSetExclusiveScissorNV was not loaded successfully") }
    pub extern "C" fn cmd_set_extra_primitive_overestimation_size_ext (_: CommandBufferMutRaw, _: f32) { panic!("vkCmdSetExtraPrimitiveOverestimationSizeEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_fragment_shading_rate_enum_nv (_: CommandBufferMutRaw, _: RawFragmentShadingRateNV, _: ConstSizePtr<RawFragmentShadingRateCombinerOpKHR, 2>) { panic!("vkCmdSetFragmentShadingRateEnumNV was not loaded successfully") }
    pub extern "C" fn cmd_set_fragment_shading_rate_khr (_: CommandBufferMutRaw, _: &RawExtent2D, _: ConstSizePtr<RawFragmentShadingRateCombinerOpKHR, 2>) { panic!("vkCmdSetFragmentShadingRateKHR was not loaded successfully") }
    pub extern "C" fn cmd_set_front_face (_: CommandBufferMutRaw, _: RawFrontFace) { panic!("vkCmdSetFrontFace was not loaded successfully") }
    pub extern "C" fn cmd_set_line_rasterization_mode_ext (_: CommandBufferMutRaw, _: RawLineRasterizationModeEXT) { panic!("vkCmdSetLineRasterizationModeEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_line_stipple_ext (_: CommandBufferMutRaw, _: u32, _: u16) { panic!("vkCmdSetLineStippleEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_line_stipple_enable_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetLineStippleEnableEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_line_width (_: CommandBufferMutRaw, _: f32) { panic!("vkCmdSetLineWidth was not loaded successfully") }
    pub extern "C" fn cmd_set_logic_op_ext (_: CommandBufferMutRaw, _: RawLogicOp) { panic!("vkCmdSetLogicOpEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_logic_op_enable_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetLogicOpEnableEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_patch_control_points_ext (_: CommandBufferMutRaw, _: u32) { panic!("vkCmdSetPatchControlPointsEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_performance_marker_intel (_: CommandBufferMutRaw, _: &RawPerformanceMarkerInfoINTEL) -> RawResult { panic!("vkCmdSetPerformanceMarkerINTEL was not loaded successfully") }
    pub extern "C" fn cmd_set_performance_override_intel (_: CommandBufferMutRaw, _: &RawPerformanceOverrideInfoINTEL) -> RawResult { panic!("vkCmdSetPerformanceOverrideINTEL was not loaded successfully") }
    pub extern "C" fn cmd_set_performance_stream_marker_intel (_: CommandBufferMutRaw, _: &RawPerformanceStreamMarkerInfoINTEL) -> RawResult { panic!("vkCmdSetPerformanceStreamMarkerINTEL was not loaded successfully") }
    pub extern "C" fn cmd_set_polygon_mode_ext (_: CommandBufferMutRaw, _: RawPolygonMode) { panic!("vkCmdSetPolygonModeEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_primitive_restart_enable (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetPrimitiveRestartEnable was not loaded successfully") }
    pub extern "C" fn cmd_set_primitive_topology (_: CommandBufferMutRaw, _: RawPrimitiveTopology) { panic!("vkCmdSetPrimitiveTopology was not loaded successfully") }
    pub extern "C" fn cmd_set_provoking_vertex_mode_ext (_: CommandBufferMutRaw, _: RawProvokingVertexModeEXT) { panic!("vkCmdSetProvokingVertexModeEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_rasterization_samples_ext (_: CommandBufferMutRaw, _: RawSampleCountFlagBits) { panic!("vkCmdSetRasterizationSamplesEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_rasterization_stream_ext (_: CommandBufferMutRaw, _: u32) { panic!("vkCmdSetRasterizationStreamEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_rasterizer_discard_enable (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetRasterizerDiscardEnable was not loaded successfully") }
    pub extern "C" fn cmd_set_ray_tracing_pipeline_stack_size_khr (_: CommandBufferMutRaw, _: u32) { panic!("vkCmdSetRayTracingPipelineStackSizeKHR was not loaded successfully") }
    pub extern "C" fn cmd_set_representative_fragment_test_enable_nv (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetRepresentativeFragmentTestEnableNV was not loaded successfully") }
    pub extern "C" fn cmd_set_sample_locations_ext (_: CommandBufferMutRaw, _: &RawSampleLocationsInfoEXT) { panic!("vkCmdSetSampleLocationsEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_sample_locations_enable_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetSampleLocationsEnableEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_sample_mask_ext (_: CommandBufferMutRaw, _: RawSampleCountFlagBits, _: &SampleMask) { panic!("vkCmdSetSampleMaskEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_scissor (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawRect2D) { panic!("vkCmdSetScissor was not loaded successfully") }
    pub extern "C" fn cmd_set_scissor_with_count (_: CommandBufferMutRaw, _: u32, _: &RawRect2D) { panic!("vkCmdSetScissorWithCount was not loaded successfully") }
    pub extern "C" fn cmd_set_shading_rate_image_enable_nv (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetShadingRateImageEnableNV was not loaded successfully") }
    pub extern "C" fn cmd_set_stencil_compare_mask (_: CommandBufferMutRaw, _: StencilFaceFlags, _: u32) { panic!("vkCmdSetStencilCompareMask was not loaded successfully") }
    pub extern "C" fn cmd_set_stencil_op (_: CommandBufferMutRaw, _: StencilFaceFlags, _: RawStencilOp, _: RawStencilOp, _: RawStencilOp, _: RawCompareOp) { panic!("vkCmdSetStencilOp was not loaded successfully") }
    pub extern "C" fn cmd_set_stencil_reference (_: CommandBufferMutRaw, _: StencilFaceFlags, _: u32) { panic!("vkCmdSetStencilReference was not loaded successfully") }
    pub extern "C" fn cmd_set_stencil_test_enable (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetStencilTestEnable was not loaded successfully") }
    pub extern "C" fn cmd_set_stencil_write_mask (_: CommandBufferMutRaw, _: StencilFaceFlags, _: u32) { panic!("vkCmdSetStencilWriteMask was not loaded successfully") }
    pub extern "C" fn cmd_set_tessellation_domain_origin_ext (_: CommandBufferMutRaw, _: RawTessellationDomainOrigin) { panic!("vkCmdSetTessellationDomainOriginEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_vertex_input_ext (_: CommandBufferMutRaw, _: u32, _: Option<&RawVertexInputBindingDescription2EXT>, _: u32, _: Option<&RawVertexInputAttributeDescription2EXT>) { panic!("vkCmdSetVertexInputEXT was not loaded successfully") }
    pub extern "C" fn cmd_set_viewport (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawViewport) { panic!("vkCmdSetViewport was not loaded successfully") }
    pub extern "C" fn cmd_set_viewport_shading_rate_palette_nv (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawShadingRatePaletteNV) { panic!("vkCmdSetViewportShadingRatePaletteNV was not loaded successfully") }
    pub extern "C" fn cmd_set_viewport_swizzle_nv (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawViewportSwizzleNV) { panic!("vkCmdSetViewportSwizzleNV was not loaded successfully") }
    pub extern "C" fn cmd_set_viewport_w_scaling_enable_nv (_: CommandBufferMutRaw, _: Bool32) { panic!("vkCmdSetViewportWScalingEnableNV was not loaded successfully") }
    pub extern "C" fn cmd_set_viewport_w_scaling_nv (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawViewportWScalingNV) { panic!("vkCmdSetViewportWScalingNV was not loaded successfully") }
    pub extern "C" fn cmd_set_viewport_with_count (_: CommandBufferMutRaw, _: u32, _: &RawViewport) { panic!("vkCmdSetViewportWithCount was not loaded successfully") }
    pub extern "C" fn cmd_subpass_shading_huawei (_: CommandBufferMutRaw) { panic!("vkCmdSubpassShadingHUAWEI was not loaded successfully") }
    pub extern "C" fn cmd_trace_rays_indirect2_khr (_: CommandBufferMutRaw, _: DeviceAddress) { panic!("vkCmdTraceRaysIndirect2KHR was not loaded successfully") }
    pub extern "C" fn cmd_trace_rays_indirect_khr (_: CommandBufferMutRaw, _: &RawStridedDeviceAddressRegionKHR, _: &RawStridedDeviceAddressRegionKHR, _: &RawStridedDeviceAddressRegionKHR, _: &RawStridedDeviceAddressRegionKHR, _: DeviceAddress) { panic!("vkCmdTraceRaysIndirectKHR was not loaded successfully") }
    pub extern "C" fn cmd_trace_rays_khr (_: CommandBufferMutRaw, _: &RawStridedDeviceAddressRegionKHR, _: &RawStridedDeviceAddressRegionKHR, _: &RawStridedDeviceAddressRegionKHR, _: &RawStridedDeviceAddressRegionKHR, _: u32, _: u32, _: u32) { panic!("vkCmdTraceRaysKHR was not loaded successfully") }
    pub extern "C" fn cmd_trace_rays_nv (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: BufferRaw, _: DeviceSize, _: DeviceSize, _: BufferRaw, _: DeviceSize, _: DeviceSize, _: BufferRaw, _: DeviceSize, _: DeviceSize, _: u32, _: u32, _: u32) { panic!("vkCmdTraceRaysNV was not loaded successfully") }
    pub extern "C" fn cmd_update_buffer (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: DeviceSize, _: &u8) { panic!("vkCmdUpdateBuffer was not loaded successfully") }
    pub extern "C" fn cmd_wait_events (_: CommandBufferMutRaw, _: u32, _: &EventRaw, _: PipelineStageFlags, _: PipelineStageFlags, _: u32, _: Option<&RawMemoryBarrier>, _: u32, _: Option<&RawBufferMemoryBarrier>, _: u32, _: Option<&RawImageMemoryBarrier>) { panic!("vkCmdWaitEvents was not loaded successfully") }
    pub extern "C" fn cmd_wait_events2 (_: CommandBufferMutRaw, _: u32, _: &EventRaw, _: &RawDependencyInfo) { panic!("vkCmdWaitEvents2 was not loaded successfully") }
    pub extern "C" fn cmd_write_acceleration_structures_properties_khr (_: CommandBufferMutRaw, _: u32, _: &AccelerationStructureKHRRaw, _: RawQueryType, _: QueryPoolRaw, _: u32) { panic!("vkCmdWriteAccelerationStructuresPropertiesKHR was not loaded successfully") }
    pub extern "C" fn cmd_write_acceleration_structures_properties_nv (_: CommandBufferMutRaw, _: u32, _: &AccelerationStructureNVRaw, _: RawQueryType, _: QueryPoolRaw, _: u32) { panic!("vkCmdWriteAccelerationStructuresPropertiesNV was not loaded successfully") }
    pub extern "C" fn cmd_write_buffer_marker2_amd (_: CommandBufferMutRaw, _: PipelineStageFlags2, _: BufferRaw, _: DeviceSize, _: u32) { panic!("vkCmdWriteBufferMarker2AMD was not loaded successfully") }
    pub extern "C" fn cmd_write_buffer_marker_amd (_: CommandBufferMutRaw, _: RawPipelineStageFlagBits, _: BufferRaw, _: DeviceSize, _: u32) { panic!("vkCmdWriteBufferMarkerAMD was not loaded successfully") }
    pub extern "C" fn cmd_write_micromaps_properties_ext (_: CommandBufferMutRaw, _: u32, _: &MicromapEXTRaw, _: RawQueryType, _: QueryPoolRaw, _: u32) { panic!("vkCmdWriteMicromapsPropertiesEXT was not loaded successfully") }
    pub extern "C" fn cmd_write_timestamp (_: CommandBufferMutRaw, _: RawPipelineStageFlagBits, _: QueryPoolRaw, _: u32) { panic!("vkCmdWriteTimestamp was not loaded successfully") }
    pub extern "C" fn cmd_write_timestamp2 (_: CommandBufferMutRaw, _: PipelineStageFlags2, _: QueryPoolRaw, _: u32) { panic!("vkCmdWriteTimestamp2 was not loaded successfully") }
    pub extern "C" fn compile_deferred_nv (_: DeviceRaw, _: PipelineRaw, _: u32) -> RawResult { panic!("vkCompileDeferredNV was not loaded successfully") }
    pub extern "C" fn copy_acceleration_structure_khr (_: DeviceRaw, _: DeferredOperationKHRRaw, _: &RawCopyAccelerationStructureInfoKHR) -> RawResult { panic!("vkCopyAccelerationStructureKHR was not loaded successfully") }
    pub extern "C" fn copy_acceleration_structure_to_memory_khr (_: DeviceRaw, _: DeferredOperationKHRRaw, _: &RawCopyAccelerationStructureToMemoryInfoKHR) -> RawResult { panic!("vkCopyAccelerationStructureToMemoryKHR was not loaded successfully") }
    pub extern "C" fn copy_memory_to_acceleration_structure_khr (_: DeviceRaw, _: DeferredOperationKHRRaw, _: &RawCopyMemoryToAccelerationStructureInfoKHR) -> RawResult { panic!("vkCopyMemoryToAccelerationStructureKHR was not loaded successfully") }
    pub extern "C" fn copy_memory_to_micromap_ext (_: DeviceRaw, _: DeferredOperationKHRRaw, _: &RawCopyMemoryToMicromapInfoEXT) -> RawResult { panic!("vkCopyMemoryToMicromapEXT was not loaded successfully") }
    pub extern "C" fn copy_micromap_ext (_: DeviceRaw, _: DeferredOperationKHRRaw, _: &RawCopyMicromapInfoEXT) -> RawResult { panic!("vkCopyMicromapEXT was not loaded successfully") }
    pub extern "C" fn copy_micromap_to_memory_ext (_: DeviceRaw, _: DeferredOperationKHRRaw, _: &RawCopyMicromapToMemoryInfoEXT) -> RawResult { panic!("vkCopyMicromapToMemoryEXT was not loaded successfully") }
    pub extern "C" fn create_acceleration_structure_khr (_: DeviceRaw, _: &RawAccelerationStructureCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<AccelerationStructureKHRRaw>) -> RawResult { panic!("vkCreateAccelerationStructureKHR was not loaded successfully") }
    pub extern "C" fn create_acceleration_structure_nv (_: DeviceRaw, _: &RawAccelerationStructureCreateInfoNV, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<AccelerationStructureNVRaw>) -> RawResult { panic!("vkCreateAccelerationStructureNV was not loaded successfully") }
    pub extern "C" fn create_android_surface_khr (_: InstanceRaw, _: &RawAndroidSurfaceCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("vkCreateAndroidSurfaceKHR was not loaded successfully") }
    pub extern "C" fn create_buffer (_: DeviceRaw, _: &RawBufferCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<BufferRaw>) -> RawResult { panic!("vkCreateBuffer was not loaded successfully") }
    pub extern "C" fn create_buffer_collection_fuchsia (_: DeviceRaw, _: &RawBufferCollectionCreateInfoFUCHSIA, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<BufferCollectionFUCHSIARaw>) -> RawResult { panic!("vkCreateBufferCollectionFUCHSIA was not loaded successfully") }
    pub extern "C" fn create_buffer_view (_: DeviceRaw, _: &RawBufferViewCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<BufferViewRaw>) -> RawResult { panic!("vkCreateBufferView was not loaded successfully") }
    pub extern "C" fn create_command_pool (_: DeviceRaw, _: &RawCommandPoolCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<CommandPoolRaw>) -> RawResult { panic!("vkCreateCommandPool was not loaded successfully") }
    pub extern "C" fn create_compute_pipelines (_: DeviceRaw, _: PipelineCacheRaw, _: u32, _: &RawComputePipelineCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<PipelineRaw>) -> RawResult { panic!("vkCreateComputePipelines was not loaded successfully") }
    pub extern "C" fn create_cu_function_nvx (_: DeviceRaw, _: &RawCuFunctionCreateInfoNVX, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<CuFunctionNVXRaw>) -> RawResult { panic!("vkCreateCuFunctionNVX was not loaded successfully") }
    pub extern "C" fn create_cu_module_nvx (_: DeviceRaw, _: &RawCuModuleCreateInfoNVX, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<CuModuleNVXRaw>) -> RawResult { panic!("vkCreateCuModuleNVX was not loaded successfully") }
    pub extern "C" fn create_debug_report_callback_ext (_: InstanceRaw, _: &RawDebugReportCallbackCreateInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DebugReportCallbackEXTRaw>) -> RawResult { panic!("vkCreateDebugReportCallbackEXT was not loaded successfully") }
    pub extern "C" fn create_debug_utils_messenger_ext (_: InstanceRaw, _: &RawDebugUtilsMessengerCreateInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DebugUtilsMessengerEXTRaw>) -> RawResult { panic!("vkCreateDebugUtilsMessengerEXT was not loaded successfully") }
    pub extern "C" fn create_deferred_operation_khr (_: DeviceRaw, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DeferredOperationKHRRaw>) -> RawResult { panic!("vkCreateDeferredOperationKHR was not loaded successfully") }
    pub extern "C" fn create_descriptor_pool (_: DeviceRaw, _: &RawDescriptorPoolCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DescriptorPoolRaw>) -> RawResult { panic!("vkCreateDescriptorPool was not loaded successfully") }
    pub extern "C" fn create_descriptor_set_layout (_: DeviceRaw, _: &RawDescriptorSetLayoutCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DescriptorSetLayoutRaw>) -> RawResult { panic!("vkCreateDescriptorSetLayout was not loaded successfully") }
    pub extern "C" fn create_descriptor_update_template (_: DeviceRaw, _: &RawDescriptorUpdateTemplateCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DescriptorUpdateTemplateRaw>) -> RawResult { panic!("vkCreateDescriptorUpdateTemplate was not loaded successfully") }
    pub extern "C" fn create_device (_: PhysicalDeviceRaw, _: &RawDeviceCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DeviceRaw>) -> RawResult { panic!("vkCreateDevice was not loaded successfully") }
    pub extern "C" fn create_direct_fb_surface_ext (_: InstanceRaw, _: &RawDirectFBSurfaceCreateInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("vkCreateDirectFBSurfaceEXT was not loaded successfully") }
    pub extern "C" fn create_display_mode_khr (_: PhysicalDeviceRaw, _: DisplayKHRMutRaw, _: &RawDisplayModeCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DisplayModeKHRRaw>) -> RawResult { panic!("vkCreateDisplayModeKHR was not loaded successfully") }
    pub extern "C" fn create_display_plane_surface_khr (_: InstanceRaw, _: &RawDisplaySurfaceCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("vkCreateDisplayPlaneSurfaceKHR was not loaded successfully") }
    pub extern "C" fn create_event (_: DeviceRaw, _: &RawEventCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<EventRaw>) -> RawResult { panic!("vkCreateEvent was not loaded successfully") }
    pub extern "C" fn create_fence (_: DeviceRaw, _: &RawFenceCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<FenceRaw>) -> RawResult { panic!("vkCreateFence was not loaded successfully") }
    pub extern "C" fn create_framebuffer (_: DeviceRaw, _: &RawFramebufferCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<FramebufferRaw>) -> RawResult { panic!("vkCreateFramebuffer was not loaded successfully") }
    pub extern "C" fn create_graphics_pipelines (_: DeviceRaw, _: PipelineCacheRaw, _: u32, _: &RawGraphicsPipelineCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<PipelineRaw>) -> RawResult { panic!("vkCreateGraphicsPipelines was not loaded successfully") }
    pub extern "C" fn create_headless_surface_ext (_: InstanceRaw, _: &RawHeadlessSurfaceCreateInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("vkCreateHeadlessSurfaceEXT was not loaded successfully") }
    pub extern "C" fn create_ios_surface_mvk (_: InstanceRaw, _: &RawIOSSurfaceCreateInfoMVK, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("vkCreateIOSSurfaceMVK was not loaded successfully") }
    pub extern "C" fn create_image (_: DeviceRaw, _: &RawImageCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<ImageRaw>) -> RawResult { panic!("vkCreateImage was not loaded successfully") }
    pub extern "C" fn create_image_pipe_surface_fuchsia (_: InstanceRaw, _: &RawImagePipeSurfaceCreateInfoFUCHSIA, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("vkCreateImagePipeSurfaceFUCHSIA was not loaded successfully") }
    pub extern "C" fn create_image_view (_: DeviceRaw, _: &RawImageViewCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<ImageViewRaw>) -> RawResult { panic!("vkCreateImageView was not loaded successfully") }
    pub extern "C" fn create_indirect_commands_layout_nv (_: DeviceRaw, _: &RawIndirectCommandsLayoutCreateInfoNV, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<IndirectCommandsLayoutNVRaw>) -> RawResult { panic!("vkCreateIndirectCommandsLayoutNV was not loaded successfully") }
    pub extern "C" fn create_instance (_: &RawInstanceCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<InstanceRaw>) -> RawResult { panic!("vkCreateInstance was not loaded successfully") }
    pub extern "C" fn create_mac_os_surface_mvk (_: InstanceRaw, _: &RawMacOSSurfaceCreateInfoMVK, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("vkCreateMacOSSurfaceMVK was not loaded successfully") }
    pub extern "C" fn create_metal_surface_ext (_: InstanceRaw, _: &RawMetalSurfaceCreateInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("vkCreateMetalSurfaceEXT was not loaded successfully") }
    pub extern "C" fn create_micromap_ext (_: DeviceRaw, _: &RawMicromapCreateInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<MicromapEXTRaw>) -> RawResult { panic!("vkCreateMicromapEXT was not loaded successfully") }
    pub extern "C" fn create_optical_flow_session_nv (_: DeviceRaw, _: &RawOpticalFlowSessionCreateInfoNV, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<OpticalFlowSessionNVRaw>) -> RawResult { panic!("vkCreateOpticalFlowSessionNV was not loaded successfully") }
    pub extern "C" fn create_pipeline_cache (_: DeviceRaw, _: &RawPipelineCacheCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<PipelineCacheRaw>) -> RawResult { panic!("vkCreatePipelineCache was not loaded successfully") }
    pub extern "C" fn create_pipeline_layout (_: DeviceRaw, _: &RawPipelineLayoutCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<PipelineLayoutRaw>) -> RawResult { panic!("vkCreatePipelineLayout was not loaded successfully") }
    pub extern "C" fn create_private_data_slot (_: DeviceRaw, _: &RawPrivateDataSlotCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<PrivateDataSlotRaw>) -> RawResult { panic!("vkCreatePrivateDataSlot was not loaded successfully") }
    pub extern "C" fn create_query_pool (_: DeviceRaw, _: &RawQueryPoolCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<QueryPoolRaw>) -> RawResult { panic!("vkCreateQueryPool was not loaded successfully") }
    pub extern "C" fn create_ray_tracing_pipelines_khr (_: DeviceRaw, _: DeferredOperationKHRRaw, _: PipelineCacheRaw, _: u32, _: &RawRayTracingPipelineCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<PipelineRaw>) -> RawResult { panic!("vkCreateRayTracingPipelinesKHR was not loaded successfully") }
    pub extern "C" fn create_ray_tracing_pipelines_nv (_: DeviceRaw, _: PipelineCacheRaw, _: u32, _: &RawRayTracingPipelineCreateInfoNV, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<PipelineRaw>) -> RawResult { panic!("vkCreateRayTracingPipelinesNV was not loaded successfully") }
    pub extern "C" fn create_render_pass (_: DeviceRaw, _: &RawRenderPassCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<RenderPassRaw>) -> RawResult { panic!("vkCreateRenderPass was not loaded successfully") }
    pub extern "C" fn create_render_pass2 (_: DeviceRaw, _: &RawRenderPassCreateInfo2, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<RenderPassRaw>) -> RawResult { panic!("vkCreateRenderPass2 was not loaded successfully") }
    pub extern "C" fn create_sampler (_: DeviceRaw, _: &RawSamplerCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SamplerRaw>) -> RawResult { panic!("vkCreateSampler was not loaded successfully") }
    pub extern "C" fn create_sampler_ycbcr_conversion (_: DeviceRaw, _: &RawSamplerYcbcrConversionCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SamplerYcbcrConversionRaw>) -> RawResult { panic!("vkCreateSamplerYcbcrConversion was not loaded successfully") }
    pub extern "C" fn create_screen_surface_qnx (_: InstanceRaw, _: &RawScreenSurfaceCreateInfoQNX, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("vkCreateScreenSurfaceQNX was not loaded successfully") }
    pub extern "C" fn create_semaphore (_: DeviceRaw, _: &RawSemaphoreCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SemaphoreRaw>) -> RawResult { panic!("vkCreateSemaphore was not loaded successfully") }
    pub extern "C" fn create_shader_module (_: DeviceRaw, _: &RawShaderModuleCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<ShaderModuleRaw>) -> RawResult { panic!("vkCreateShaderModule was not loaded successfully") }
    pub extern "C" fn create_shared_swapchains_khr (_: DeviceRaw, _: u32, _: core::ptr::NonNull<RawSwapchainCreateInfoKHR>, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SwapchainKHRRaw>) -> RawResult { panic!("vkCreateSharedSwapchainsKHR was not loaded successfully") }
    pub extern "C" fn create_stream_descriptor_surface_ggp (_: InstanceRaw, _: &RawStreamDescriptorSurfaceCreateInfoGGP, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("vkCreateStreamDescriptorSurfaceGGP was not loaded successfully") }
    pub extern "C" fn create_swapchain_khr (_: DeviceRaw, _: core::ptr::NonNull<RawSwapchainCreateInfoKHR>, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SwapchainKHRRaw>) -> RawResult { panic!("vkCreateSwapchainKHR was not loaded successfully") }
    pub extern "C" fn create_validation_cache_ext (_: DeviceRaw, _: &RawValidationCacheCreateInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<ValidationCacheEXTRaw>) -> RawResult { panic!("vkCreateValidationCacheEXT was not loaded successfully") }
    pub extern "C" fn create_vi_surface_nn (_: InstanceRaw, _: &RawViSurfaceCreateInfoNN, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("vkCreateViSurfaceNN was not loaded successfully") }
    pub extern "C" fn create_video_session_khr (_: DeviceRaw, _: &RawVideoSessionCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<VideoSessionKHRRaw>) -> RawResult { panic!("vkCreateVideoSessionKHR was not loaded successfully") }
    pub extern "C" fn create_video_session_parameters_khr (_: DeviceRaw, _: &RawVideoSessionParametersCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<VideoSessionParametersKHRRaw>) -> RawResult { panic!("vkCreateVideoSessionParametersKHR was not loaded successfully") }
    pub extern "C" fn create_wayland_surface_khr (_: InstanceRaw, _: &RawWaylandSurfaceCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("vkCreateWaylandSurfaceKHR was not loaded successfully") }
    pub extern "C" fn create_win32_surface_khr (_: InstanceRaw, _: &RawWin32SurfaceCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("vkCreateWin32SurfaceKHR was not loaded successfully") }
    pub extern "C" fn create_xcb_surface_khr (_: InstanceRaw, _: &RawXcbSurfaceCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("vkCreateXcbSurfaceKHR was not loaded successfully") }
    pub extern "C" fn create_xlib_surface_khr (_: InstanceRaw, _: &RawXlibSurfaceCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("vkCreateXlibSurfaceKHR was not loaded successfully") }
    pub extern "C" fn debug_marker_set_object_name_ext (_: DeviceRaw, _: core::ptr::NonNull<RawDebugMarkerObjectNameInfoEXT>) -> RawResult { panic!("vkDebugMarkerSetObjectNameEXT was not loaded successfully") }
    pub extern "C" fn debug_marker_set_object_tag_ext (_: DeviceRaw, _: core::ptr::NonNull<RawDebugMarkerObjectTagInfoEXT>) -> RawResult { panic!("vkDebugMarkerSetObjectTagEXT was not loaded successfully") }
    pub extern "C" fn debug_report_message_ext (_: InstanceRaw, _: DebugReportFlagsEXT, _: RawDebugReportObjectTypeEXT, _: u64, _: usize, _: i32, _: UnsizedCStr, _: UnsizedCStr) { panic!("vkDebugReportMessageEXT was not loaded successfully") }
    pub extern "C" fn deferred_operation_join_khr (_: DeviceRaw, _: DeferredOperationKHRRaw) -> RawResult { panic!("vkDeferredOperationJoinKHR was not loaded successfully") }
    pub extern "C" fn destroy_acceleration_structure_khr (_: DeviceRaw, _: AccelerationStructureKHRMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyAccelerationStructureKHR was not loaded successfully") }
    pub extern "C" fn destroy_acceleration_structure_nv (_: DeviceRaw, _: AccelerationStructureNVMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyAccelerationStructureNV was not loaded successfully") }
    pub extern "C" fn destroy_buffer (_: DeviceRaw, _: BufferMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyBuffer was not loaded successfully") }
    pub extern "C" fn destroy_buffer_collection_fuchsia (_: DeviceRaw, _: BufferCollectionFUCHSIARaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyBufferCollectionFUCHSIA was not loaded successfully") }
    pub extern "C" fn destroy_buffer_view (_: DeviceRaw, _: BufferViewMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyBufferView was not loaded successfully") }
    pub extern "C" fn destroy_command_pool (_: DeviceRaw, _: CommandPoolMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyCommandPool was not loaded successfully") }
    pub extern "C" fn destroy_cu_function_nvx (_: DeviceRaw, _: CuFunctionNVXRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyCuFunctionNVX was not loaded successfully") }
    pub extern "C" fn destroy_cu_module_nvx (_: DeviceRaw, _: CuModuleNVXRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyCuModuleNVX was not loaded successfully") }
    pub extern "C" fn destroy_debug_report_callback_ext (_: InstanceRaw, _: DebugReportCallbackEXTMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyDebugReportCallbackEXT was not loaded successfully") }
    pub extern "C" fn destroy_debug_utils_messenger_ext (_: InstanceRaw, _: DebugUtilsMessengerEXTMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyDebugUtilsMessengerEXT was not loaded successfully") }
    pub extern "C" fn destroy_deferred_operation_khr (_: DeviceRaw, _: DeferredOperationKHRMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyDeferredOperationKHR was not loaded successfully") }
    pub extern "C" fn destroy_descriptor_pool (_: DeviceRaw, _: DescriptorPoolMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyDescriptorPool was not loaded successfully") }
    pub extern "C" fn destroy_descriptor_set_layout (_: DeviceRaw, _: DescriptorSetLayoutMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyDescriptorSetLayout was not loaded successfully") }
    pub extern "C" fn destroy_descriptor_update_template (_: DeviceRaw, _: DescriptorUpdateTemplateMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyDescriptorUpdateTemplate was not loaded successfully") }
    pub extern "C" fn destroy_device (_: DeviceMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyDevice was not loaded successfully") }
    pub extern "C" fn destroy_event (_: DeviceRaw, _: EventMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyEvent was not loaded successfully") }
    pub extern "C" fn destroy_fence (_: DeviceRaw, _: FenceMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyFence was not loaded successfully") }
    pub extern "C" fn destroy_framebuffer (_: DeviceRaw, _: FramebufferMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyFramebuffer was not loaded successfully") }
    pub extern "C" fn destroy_image (_: DeviceRaw, _: ImageMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyImage was not loaded successfully") }
    pub extern "C" fn destroy_image_view (_: DeviceRaw, _: ImageViewMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyImageView was not loaded successfully") }
    pub extern "C" fn destroy_indirect_commands_layout_nv (_: DeviceRaw, _: IndirectCommandsLayoutNVMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyIndirectCommandsLayoutNV was not loaded successfully") }
    pub extern "C" fn destroy_instance (_: InstanceMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyInstance was not loaded successfully") }
    pub extern "C" fn destroy_micromap_ext (_: DeviceRaw, _: MicromapEXTMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyMicromapEXT was not loaded successfully") }
    pub extern "C" fn destroy_optical_flow_session_nv (_: DeviceRaw, _: OpticalFlowSessionNVRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyOpticalFlowSessionNV was not loaded successfully") }
    pub extern "C" fn destroy_pipeline (_: DeviceRaw, _: PipelineMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyPipeline was not loaded successfully") }
    pub extern "C" fn destroy_pipeline_cache (_: DeviceRaw, _: PipelineCacheMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyPipelineCache was not loaded successfully") }
    pub extern "C" fn destroy_pipeline_layout (_: DeviceRaw, _: PipelineLayoutMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyPipelineLayout was not loaded successfully") }
    pub extern "C" fn destroy_private_data_slot (_: DeviceRaw, _: PrivateDataSlotMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyPrivateDataSlot was not loaded successfully") }
    pub extern "C" fn destroy_query_pool (_: DeviceRaw, _: QueryPoolMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyQueryPool was not loaded successfully") }
    pub extern "C" fn destroy_render_pass (_: DeviceRaw, _: RenderPassMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyRenderPass was not loaded successfully") }
    pub extern "C" fn destroy_sampler (_: DeviceRaw, _: SamplerMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroySampler was not loaded successfully") }
    pub extern "C" fn destroy_sampler_ycbcr_conversion (_: DeviceRaw, _: SamplerYcbcrConversionMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroySamplerYcbcrConversion was not loaded successfully") }
    pub extern "C" fn destroy_semaphore (_: DeviceRaw, _: SemaphoreMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroySemaphore was not loaded successfully") }
    pub extern "C" fn destroy_shader_module (_: DeviceRaw, _: ShaderModuleMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyShaderModule was not loaded successfully") }
    pub extern "C" fn destroy_surface_khr (_: InstanceRaw, _: SurfaceKHRMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroySurfaceKHR was not loaded successfully") }
    pub extern "C" fn destroy_swapchain_khr (_: DeviceRaw, _: SwapchainKHRMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroySwapchainKHR was not loaded successfully") }
    pub extern "C" fn destroy_validation_cache_ext (_: DeviceRaw, _: ValidationCacheEXTMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyValidationCacheEXT was not loaded successfully") }
    pub extern "C" fn destroy_video_session_khr (_: DeviceRaw, _: VideoSessionKHRMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyVideoSessionKHR was not loaded successfully") }
    pub extern "C" fn destroy_video_session_parameters_khr (_: DeviceRaw, _: VideoSessionParametersKHRMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkDestroyVideoSessionParametersKHR was not loaded successfully") }
    pub extern "C" fn device_wait_idle (_: DeviceRaw) -> RawResult { panic!("vkDeviceWaitIdle was not loaded successfully") }
    pub extern "C" fn display_power_control_ext (_: DeviceRaw, _: DisplayKHRRaw, _: &RawDisplayPowerInfoEXT) -> RawResult { panic!("vkDisplayPowerControlEXT was not loaded successfully") }
    pub extern "C" fn end_command_buffer (_: CommandBufferMutRaw) -> RawResult { panic!("vkEndCommandBuffer was not loaded successfully") }
    pub extern "C" fn enumerate_device_extension_properties (_: PhysicalDeviceRaw, _: Option<UnsizedCStr>, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawExtensionProperties>>) -> RawResult { panic!("vkEnumerateDeviceExtensionProperties was not loaded successfully") }
    pub extern "C" fn enumerate_device_layer_properties (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawLayerProperties>>) -> RawResult { panic!("vkEnumerateDeviceLayerProperties was not loaded successfully") }
    pub extern "C" fn enumerate_instance_extension_properties (_: Option<UnsizedCStr>, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawExtensionProperties>>) -> RawResult { panic!("vkEnumerateInstanceExtensionProperties was not loaded successfully") }
    pub extern "C" fn enumerate_instance_layer_properties (_: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawLayerProperties>>) -> RawResult { panic!("vkEnumerateInstanceLayerProperties was not loaded successfully") }
    pub extern "C" fn enumerate_instance_version (_: core::ptr::NonNull<Version>) -> RawResult { panic!("vkEnumerateInstanceVersion was not loaded successfully") }
    pub extern "C" fn enumerate_physical_device_groups (_: InstanceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPhysicalDeviceGroupProperties>>) -> RawResult { panic!("vkEnumeratePhysicalDeviceGroups was not loaded successfully") }
    pub extern "C" fn enumerate_physical_device_queue_family_performance_query_counters_khr (_: PhysicalDeviceRaw, _: u32, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPerformanceCounterKHR>>, _: Option<core::ptr::NonNull<RawPerformanceCounterDescriptionKHR>>) -> RawResult { panic!("vkEnumeratePhysicalDeviceQueueFamilyPerformanceQueryCountersKHR was not loaded successfully") }
    pub extern "C" fn enumerate_physical_devices (_: InstanceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<PhysicalDeviceRaw>>) -> RawResult { panic!("vkEnumeratePhysicalDevices was not loaded successfully") }
    pub extern "C" fn export_metal_objects_ext (_: DeviceRaw, _: core::ptr::NonNull<RawExportMetalObjectsInfoEXT>) { panic!("vkExportMetalObjectsEXT was not loaded successfully") }
    pub extern "C" fn flush_mapped_memory_ranges (_: DeviceRaw, _: u32, _: &RawMappedMemoryRange) -> RawResult { panic!("vkFlushMappedMemoryRanges was not loaded successfully") }
    pub extern "C" fn free_command_buffers (_: DeviceRaw, _: CommandPoolMutRaw, _: u32, _: Option<core::ptr::NonNull<CommandBufferMutRaw>>) { panic!("vkFreeCommandBuffers was not loaded successfully") }
    pub extern "C" fn free_descriptor_sets (_: DeviceRaw, _: DescriptorPoolMutRaw, _: u32, _: Option<core::ptr::NonNull<DescriptorSetMutRaw>>) -> RawResult { panic!("vkFreeDescriptorSets was not loaded successfully") }
    pub extern "C" fn free_memory (_: DeviceRaw, _: DeviceMemoryMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("vkFreeMemory was not loaded successfully") }
    pub extern "C" fn get_acceleration_structure_build_sizes_khr (_: DeviceRaw, _: RawAccelerationStructureBuildTypeKHR, _: &RawAccelerationStructureBuildGeometryInfoKHR, _: Option<&u32>, _: core::ptr::NonNull<RawAccelerationStructureBuildSizesInfoKHR>) { panic!("vkGetAccelerationStructureBuildSizesKHR was not loaded successfully") }
    pub extern "C" fn get_acceleration_structure_device_address_khr (_: DeviceRaw, _: &RawAccelerationStructureDeviceAddressInfoKHR) -> DeviceAddress { panic!("vkGetAccelerationStructureDeviceAddressKHR was not loaded successfully") }
    pub extern "C" fn get_acceleration_structure_handle_nv (_: DeviceRaw, _: AccelerationStructureNVRaw, _: usize, _: core::ptr::NonNull<u8>) -> RawResult { panic!("vkGetAccelerationStructureHandleNV was not loaded successfully") }
    pub extern "C" fn get_acceleration_structure_memory_requirements_nv (_: DeviceRaw, _: &RawAccelerationStructureMemoryRequirementsInfoNV, _: core::ptr::NonNull<RawMemoryRequirements2>) { panic!("vkGetAccelerationStructureMemoryRequirementsNV was not loaded successfully") }
    pub extern "C" fn get_acceleration_structure_opaque_capture_descriptor_data_ext (_: DeviceRaw, _: &RawAccelerationStructureCaptureDescriptorDataInfoEXT, _: core::ptr::NonNull<u8>) -> RawResult { panic!("vkGetAccelerationStructureOpaqueCaptureDescriptorDataEXT was not loaded successfully") }
    pub extern "C" fn get_android_hardware_buffer_properties_android (_: DeviceRaw, _: &AHardwareBuffer, _: core::ptr::NonNull<RawAndroidHardwareBufferPropertiesANDROID>) -> RawResult { panic!("vkGetAndroidHardwareBufferPropertiesANDROID was not loaded successfully") }
    pub extern "C" fn get_buffer_collection_properties_fuchsia (_: DeviceRaw, _: BufferCollectionFUCHSIARaw, _: core::ptr::NonNull<RawBufferCollectionPropertiesFUCHSIA>) -> RawResult { panic!("vkGetBufferCollectionPropertiesFUCHSIA was not loaded successfully") }
    pub extern "C" fn get_buffer_device_address (_: DeviceRaw, _: &RawBufferDeviceAddressInfo) -> DeviceAddress { panic!("vkGetBufferDeviceAddress was not loaded successfully") }
    pub extern "C" fn get_buffer_memory_requirements (_: DeviceRaw, _: BufferRaw, _: core::ptr::NonNull<RawMemoryRequirements>) { panic!("vkGetBufferMemoryRequirements was not loaded successfully") }
    pub extern "C" fn get_buffer_memory_requirements2 (_: DeviceRaw, _: &RawBufferMemoryRequirementsInfo2, _: core::ptr::NonNull<RawMemoryRequirements2>) { panic!("vkGetBufferMemoryRequirements2 was not loaded successfully") }
    pub extern "C" fn get_buffer_opaque_capture_address (_: DeviceRaw, _: &RawBufferDeviceAddressInfo) -> u64 { panic!("vkGetBufferOpaqueCaptureAddress was not loaded successfully") }
    pub extern "C" fn get_buffer_opaque_capture_descriptor_data_ext (_: DeviceRaw, _: &RawBufferCaptureDescriptorDataInfoEXT, _: core::ptr::NonNull<u8>) -> RawResult { panic!("vkGetBufferOpaqueCaptureDescriptorDataEXT was not loaded successfully") }
    pub extern "C" fn get_calibrated_timestamps_ext (_: DeviceRaw, _: u32, _: &RawCalibratedTimestampInfoEXT, _: core::ptr::NonNull<u64>, _: core::ptr::NonNull<u64>) -> RawResult { panic!("vkGetCalibratedTimestampsEXT was not loaded successfully") }
    pub extern "C" fn get_deferred_operation_max_concurrency_khr (_: DeviceRaw, _: DeferredOperationKHRRaw) -> u32 { panic!("vkGetDeferredOperationMaxConcurrencyKHR was not loaded successfully") }
    pub extern "C" fn get_deferred_operation_result_khr (_: DeviceRaw, _: DeferredOperationKHRRaw) -> RawResult { panic!("vkGetDeferredOperationResultKHR was not loaded successfully") }
    pub extern "C" fn get_descriptor_ext (_: DeviceRaw, _: &RawDescriptorGetInfoEXT, _: usize, _: core::ptr::NonNull<u8>) { panic!("vkGetDescriptorEXT was not loaded successfully") }
    pub extern "C" fn get_descriptor_set_host_mapping_valve (_: DeviceRaw, _: DescriptorSetRaw, _: core::ptr::NonNull<*mut u8>) { panic!("vkGetDescriptorSetHostMappingVALVE was not loaded successfully") }
    pub extern "C" fn get_descriptor_set_layout_binding_offset_ext (_: DeviceRaw, _: DescriptorSetLayoutRaw, _: u32, _: core::ptr::NonNull<DeviceSize>) { panic!("vkGetDescriptorSetLayoutBindingOffsetEXT was not loaded successfully") }
    pub extern "C" fn get_descriptor_set_layout_host_mapping_info_valve (_: DeviceRaw, _: &RawDescriptorSetBindingReferenceVALVE, _: core::ptr::NonNull<RawDescriptorSetLayoutHostMappingInfoVALVE>) { panic!("vkGetDescriptorSetLayoutHostMappingInfoVALVE was not loaded successfully") }
    pub extern "C" fn get_descriptor_set_layout_size_ext (_: DeviceRaw, _: DescriptorSetLayoutRaw, _: core::ptr::NonNull<DeviceSize>) { panic!("vkGetDescriptorSetLayoutSizeEXT was not loaded successfully") }
    pub extern "C" fn get_descriptor_set_layout_support (_: DeviceRaw, _: &RawDescriptorSetLayoutCreateInfo, _: core::ptr::NonNull<RawDescriptorSetLayoutSupport>) { panic!("vkGetDescriptorSetLayoutSupport was not loaded successfully") }
    pub extern "C" fn get_device_acceleration_structure_compatibility_khr (_: DeviceRaw, _: &RawAccelerationStructureVersionInfoKHR, _: core::ptr::NonNull<RawAccelerationStructureCompatibilityKHR>) { panic!("vkGetDeviceAccelerationStructureCompatibilityKHR was not loaded successfully") }
    pub extern "C" fn get_device_buffer_memory_requirements (_: DeviceRaw, _: &RawDeviceBufferMemoryRequirements, _: core::ptr::NonNull<RawMemoryRequirements2>) { panic!("vkGetDeviceBufferMemoryRequirements was not loaded successfully") }
    pub extern "C" fn get_device_fault_info_ext (_: DeviceRaw, _: core::ptr::NonNull<RawDeviceFaultCountsEXT>, _: Option<core::ptr::NonNull<RawDeviceFaultInfoEXT>>) -> RawResult { panic!("vkGetDeviceFaultInfoEXT was not loaded successfully") }
    pub extern "C" fn get_device_group_peer_memory_features (_: DeviceRaw, _: u32, _: u32, _: u32, _: core::ptr::NonNull<PeerMemoryFeatureFlags>) { panic!("vkGetDeviceGroupPeerMemoryFeatures was not loaded successfully") }
    pub extern "C" fn get_device_group_present_capabilities_khr (_: DeviceRaw, _: core::ptr::NonNull<RawDeviceGroupPresentCapabilitiesKHR>) -> RawResult { panic!("vkGetDeviceGroupPresentCapabilitiesKHR was not loaded successfully") }
    pub extern "C" fn get_device_group_surface_present_modes2_ext (_: DeviceRaw, _: &RawPhysicalDeviceSurfaceInfo2KHR, _: core::ptr::NonNull<DeviceGroupPresentModeFlagsKHR>) -> RawResult { panic!("vkGetDeviceGroupSurfacePresentModes2EXT was not loaded successfully") }
    pub extern "C" fn get_device_group_surface_present_modes_khr (_: DeviceRaw, _: SurfaceKHRMutRaw, _: core::ptr::NonNull<DeviceGroupPresentModeFlagsKHR>) -> RawResult { panic!("vkGetDeviceGroupSurfacePresentModesKHR was not loaded successfully") }
    pub extern "C" fn get_device_image_memory_requirements (_: DeviceRaw, _: &RawDeviceImageMemoryRequirements, _: core::ptr::NonNull<RawMemoryRequirements2>) { panic!("vkGetDeviceImageMemoryRequirements was not loaded successfully") }
    pub extern "C" fn get_device_image_sparse_memory_requirements (_: DeviceRaw, _: &RawDeviceImageMemoryRequirements, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawSparseImageMemoryRequirements2>>) { panic!("vkGetDeviceImageSparseMemoryRequirements was not loaded successfully") }
    pub extern "C" fn get_device_memory_commitment (_: DeviceRaw, _: DeviceMemoryRaw, _: core::ptr::NonNull<DeviceSize>) { panic!("vkGetDeviceMemoryCommitment was not loaded successfully") }
    pub extern "C" fn get_device_memory_opaque_capture_address (_: DeviceRaw, _: &RawDeviceMemoryOpaqueCaptureAddressInfo) -> u64 { panic!("vkGetDeviceMemoryOpaqueCaptureAddress was not loaded successfully") }
    pub extern "C" fn get_device_micromap_compatibility_ext (_: DeviceRaw, _: &RawMicromapVersionInfoEXT, _: core::ptr::NonNull<RawAccelerationStructureCompatibilityKHR>) { panic!("vkGetDeviceMicromapCompatibilityEXT was not loaded successfully") }
    pub extern "C" fn get_device_proc_addr (_: DeviceRaw, _: UnsizedCStr) -> VoidFunction { panic!("vkGetDeviceProcAddr was not loaded successfully") }
    pub extern "C" fn get_device_queue (_: DeviceRaw, _: u32, _: u32, _: core::ptr::NonNull<QueueRaw>) { panic!("vkGetDeviceQueue was not loaded successfully") }
    pub extern "C" fn get_device_queue2 (_: DeviceRaw, _: &RawDeviceQueueInfo2, _: core::ptr::NonNull<QueueRaw>) { panic!("vkGetDeviceQueue2 was not loaded successfully") }
    pub extern "C" fn get_device_subpass_shading_max_workgroup_size_huawei (_: DeviceRaw, _: RenderPassRaw, _: core::ptr::NonNull<RawExtent2D>) -> RawResult { panic!("vkGetDeviceSubpassShadingMaxWorkgroupSizeHUAWEI was not loaded successfully") }
    pub extern "C" fn get_display_mode_properties2_khr (_: PhysicalDeviceRaw, _: DisplayKHRRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawDisplayModeProperties2KHR>>) -> RawResult { panic!("vkGetDisplayModeProperties2KHR was not loaded successfully") }
    pub extern "C" fn get_display_mode_properties_khr (_: PhysicalDeviceRaw, _: DisplayKHRRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawDisplayModePropertiesKHR>>) -> RawResult { panic!("vkGetDisplayModePropertiesKHR was not loaded successfully") }
    pub extern "C" fn get_display_plane_capabilities2_khr (_: PhysicalDeviceRaw, _: core::ptr::NonNull<RawDisplayPlaneInfo2KHR>, _: core::ptr::NonNull<RawDisplayPlaneCapabilities2KHR>) -> RawResult { panic!("vkGetDisplayPlaneCapabilities2KHR was not loaded successfully") }
    pub extern "C" fn get_display_plane_capabilities_khr (_: PhysicalDeviceRaw, _: DisplayModeKHRMutRaw, _: u32, _: core::ptr::NonNull<RawDisplayPlaneCapabilitiesKHR>) -> RawResult { panic!("vkGetDisplayPlaneCapabilitiesKHR was not loaded successfully") }
    pub extern "C" fn get_display_plane_supported_displays_khr (_: PhysicalDeviceRaw, _: u32, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<DisplayKHRRaw>>) -> RawResult { panic!("vkGetDisplayPlaneSupportedDisplaysKHR was not loaded successfully") }
    pub extern "C" fn get_drm_display_ext (_: PhysicalDeviceRaw, _: i32, _: u32, _: core::ptr::NonNull<DisplayKHRRaw>) -> RawResult { panic!("vkGetDrmDisplayEXT was not loaded successfully") }
    pub extern "C" fn get_dynamic_rendering_tile_properties_qcom (_: DeviceRaw, _: &RawRenderingInfo, _: core::ptr::NonNull<RawTilePropertiesQCOM>) -> RawResult { panic!("vkGetDynamicRenderingTilePropertiesQCOM was not loaded successfully") }
    pub extern "C" fn get_event_status (_: DeviceRaw, _: EventRaw) -> RawResult { panic!("vkGetEventStatus was not loaded successfully") }
    pub extern "C" fn get_fence_fd_khr (_: DeviceRaw, _: &RawFenceGetFdInfoKHR, _: core::ptr::NonNull<::std::os::raw::c_int>) -> RawResult { panic!("vkGetFenceFdKHR was not loaded successfully") }
    pub extern "C" fn get_fence_status (_: DeviceRaw, _: FenceRaw) -> RawResult { panic!("vkGetFenceStatus was not loaded successfully") }
    pub extern "C" fn get_fence_win32_handle_khr (_: DeviceRaw, _: &RawFenceGetWin32HandleInfoKHR, _: core::ptr::NonNull<HANDLE>) -> RawResult { panic!("vkGetFenceWin32HandleKHR was not loaded successfully") }
    pub extern "C" fn get_framebuffer_tile_properties_qcom (_: DeviceRaw, _: FramebufferRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawTilePropertiesQCOM>>) -> RawResult { panic!("vkGetFramebufferTilePropertiesQCOM was not loaded successfully") }
    pub extern "C" fn get_generated_commands_memory_requirements_nv (_: DeviceRaw, _: &RawGeneratedCommandsMemoryRequirementsInfoNV, _: core::ptr::NonNull<RawMemoryRequirements2>) { panic!("vkGetGeneratedCommandsMemoryRequirementsNV was not loaded successfully") }
    pub extern "C" fn get_image_drm_format_modifier_properties_ext (_: DeviceRaw, _: ImageRaw, _: core::ptr::NonNull<RawImageDrmFormatModifierPropertiesEXT>) -> RawResult { panic!("vkGetImageDrmFormatModifierPropertiesEXT was not loaded successfully") }
    pub extern "C" fn get_image_memory_requirements (_: DeviceRaw, _: ImageRaw, _: core::ptr::NonNull<RawMemoryRequirements>) { panic!("vkGetImageMemoryRequirements was not loaded successfully") }
    pub extern "C" fn get_image_memory_requirements2 (_: DeviceRaw, _: &RawImageMemoryRequirementsInfo2, _: core::ptr::NonNull<RawMemoryRequirements2>) { panic!("vkGetImageMemoryRequirements2 was not loaded successfully") }
    pub extern "C" fn get_image_opaque_capture_descriptor_data_ext (_: DeviceRaw, _: &RawImageCaptureDescriptorDataInfoEXT, _: core::ptr::NonNull<u8>) -> RawResult { panic!("vkGetImageOpaqueCaptureDescriptorDataEXT was not loaded successfully") }
    pub extern "C" fn get_image_sparse_memory_requirements (_: DeviceRaw, _: ImageRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawSparseImageMemoryRequirements>>) { panic!("vkGetImageSparseMemoryRequirements was not loaded successfully") }
    pub extern "C" fn get_image_sparse_memory_requirements2 (_: DeviceRaw, _: &RawImageSparseMemoryRequirementsInfo2, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawSparseImageMemoryRequirements2>>) { panic!("vkGetImageSparseMemoryRequirements2 was not loaded successfully") }
    pub extern "C" fn get_image_subresource_layout (_: DeviceRaw, _: ImageRaw, _: &RawImageSubresource, _: core::ptr::NonNull<RawSubresourceLayout>) { panic!("vkGetImageSubresourceLayout was not loaded successfully") }
    pub extern "C" fn get_image_subresource_layout2_ext (_: DeviceRaw, _: ImageRaw, _: &RawImageSubresource2EXT, _: core::ptr::NonNull<RawSubresourceLayout2EXT>) { panic!("vkGetImageSubresourceLayout2EXT was not loaded successfully") }
    pub extern "C" fn get_image_view_address_nvx (_: DeviceRaw, _: ImageViewRaw, _: core::ptr::NonNull<RawImageViewAddressPropertiesNVX>) -> RawResult { panic!("vkGetImageViewAddressNVX was not loaded successfully") }
    pub extern "C" fn get_image_view_handle_nvx (_: DeviceRaw, _: &RawImageViewHandleInfoNVX) -> u32 { panic!("vkGetImageViewHandleNVX was not loaded successfully") }
    pub extern "C" fn get_image_view_opaque_capture_descriptor_data_ext (_: DeviceRaw, _: &RawImageViewCaptureDescriptorDataInfoEXT, _: core::ptr::NonNull<u8>) -> RawResult { panic!("vkGetImageViewOpaqueCaptureDescriptorDataEXT was not loaded successfully") }
    pub extern "C" fn get_instance_proc_addr (_: InstanceRaw, _: UnsizedCStr) -> VoidFunction { panic!("vkGetInstanceProcAddr was not loaded successfully") }
    pub extern "C" fn get_memory_android_hardware_buffer_android (_: DeviceRaw, _: &RawMemoryGetAndroidHardwareBufferInfoANDROID, _: core::ptr::NonNull<*mut AHardwareBuffer>) -> RawResult { panic!("vkGetMemoryAndroidHardwareBufferANDROID was not loaded successfully") }
    pub extern "C" fn get_memory_fd_khr (_: DeviceRaw, _: &RawMemoryGetFdInfoKHR, _: core::ptr::NonNull<::std::os::raw::c_int>) -> RawResult { panic!("vkGetMemoryFdKHR was not loaded successfully") }
    pub extern "C" fn get_memory_fd_properties_khr (_: DeviceRaw, _: RawExternalMemoryHandleTypeFlagBits, _: ::std::os::raw::c_int, _: core::ptr::NonNull<RawMemoryFdPropertiesKHR>) -> RawResult { panic!("vkGetMemoryFdPropertiesKHR was not loaded successfully") }
    pub extern "C" fn get_memory_host_pointer_properties_ext (_: DeviceRaw, _: RawExternalMemoryHandleTypeFlagBits, _: &u8, _: core::ptr::NonNull<RawMemoryHostPointerPropertiesEXT>) -> RawResult { panic!("vkGetMemoryHostPointerPropertiesEXT was not loaded successfully") }
    pub extern "C" fn get_memory_remote_address_nv (_: DeviceRaw, _: &RawMemoryGetRemoteAddressInfoNV, _: core::ptr::NonNull<RemoteAddressNV>) -> RawResult { panic!("vkGetMemoryRemoteAddressNV was not loaded successfully") }
    pub extern "C" fn get_memory_win32_handle_khr (_: DeviceRaw, _: &RawMemoryGetWin32HandleInfoKHR, _: core::ptr::NonNull<HANDLE>) -> RawResult { panic!("vkGetMemoryWin32HandleKHR was not loaded successfully") }
    pub extern "C" fn get_memory_win32_handle_nv (_: DeviceRaw, _: DeviceMemoryRaw, _: ExternalMemoryHandleTypeFlagsNV, _: core::ptr::NonNull<HANDLE>) -> RawResult { panic!("vkGetMemoryWin32HandleNV was not loaded successfully") }
    pub extern "C" fn get_memory_win32_handle_properties_khr (_: DeviceRaw, _: RawExternalMemoryHandleTypeFlagBits, _: HANDLE, _: core::ptr::NonNull<RawMemoryWin32HandlePropertiesKHR>) -> RawResult { panic!("vkGetMemoryWin32HandlePropertiesKHR was not loaded successfully") }
    pub extern "C" fn get_memory_zircon_handle_fuchsia (_: DeviceRaw, _: &RawMemoryGetZirconHandleInfoFUCHSIA, _: core::ptr::NonNull<zx_handle_t>) -> RawResult { panic!("vkGetMemoryZirconHandleFUCHSIA was not loaded successfully") }
    pub extern "C" fn get_memory_zircon_handle_properties_fuchsia (_: DeviceRaw, _: RawExternalMemoryHandleTypeFlagBits, _: zx_handle_t, _: core::ptr::NonNull<RawMemoryZirconHandlePropertiesFUCHSIA>) -> RawResult { panic!("vkGetMemoryZirconHandlePropertiesFUCHSIA was not loaded successfully") }
    pub extern "C" fn get_micromap_build_sizes_ext (_: DeviceRaw, _: RawAccelerationStructureBuildTypeKHR, _: &RawMicromapBuildInfoEXT, _: core::ptr::NonNull<RawMicromapBuildSizesInfoEXT>) { panic!("vkGetMicromapBuildSizesEXT was not loaded successfully") }
    pub extern "C" fn get_past_presentation_timing_google (_: DeviceRaw, _: SwapchainKHRMutRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPastPresentationTimingGOOGLE>>) -> RawResult { panic!("vkGetPastPresentationTimingGOOGLE was not loaded successfully") }
    pub extern "C" fn get_performance_parameter_intel (_: DeviceRaw, _: RawPerformanceParameterTypeINTEL, _: core::ptr::NonNull<RawPerformanceValueINTEL>) -> RawResult { panic!("vkGetPerformanceParameterINTEL was not loaded successfully") }
    pub extern "C" fn get_physical_device_calibrateable_time_domains_ext (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawTimeDomainEXT>>) -> RawResult { panic!("vkGetPhysicalDeviceCalibrateableTimeDomainsEXT was not loaded successfully") }
    pub extern "C" fn get_physical_device_cooperative_matrix_properties_nv (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawCooperativeMatrixPropertiesNV>>) -> RawResult { panic!("vkGetPhysicalDeviceCooperativeMatrixPropertiesNV was not loaded successfully") }
    pub extern "C" fn get_physical_device_direct_fb_presentation_support_ext (_: PhysicalDeviceRaw, _: u32, _: core::ptr::NonNull<IDirectFB>) -> Bool32 { panic!("vkGetPhysicalDeviceDirectFBPresentationSupportEXT was not loaded successfully") }
    pub extern "C" fn get_physical_device_display_plane_properties2_khr (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawDisplayPlaneProperties2KHR>>) -> RawResult { panic!("vkGetPhysicalDeviceDisplayPlaneProperties2KHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_display_plane_properties_khr (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawDisplayPlanePropertiesKHR>>) -> RawResult { panic!("vkGetPhysicalDeviceDisplayPlanePropertiesKHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_display_properties2_khr (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawDisplayProperties2KHR>>) -> RawResult { panic!("vkGetPhysicalDeviceDisplayProperties2KHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_display_properties_khr (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawDisplayPropertiesKHR>>) -> RawResult { panic!("vkGetPhysicalDeviceDisplayPropertiesKHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_external_buffer_properties (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceExternalBufferInfo, _: core::ptr::NonNull<RawExternalBufferProperties>) { panic!("vkGetPhysicalDeviceExternalBufferProperties was not loaded successfully") }
    pub extern "C" fn get_physical_device_external_fence_properties (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceExternalFenceInfo, _: core::ptr::NonNull<RawExternalFenceProperties>) { panic!("vkGetPhysicalDeviceExternalFenceProperties was not loaded successfully") }
    pub extern "C" fn get_physical_device_external_image_format_properties_nv (_: PhysicalDeviceRaw, _: RawFormat, _: RawImageType, _: RawImageTiling, _: ImageUsageFlags, _: ImageCreateFlags, _: ExternalMemoryHandleTypeFlagsNV, _: core::ptr::NonNull<RawExternalImageFormatPropertiesNV>) -> RawResult { panic!("vkGetPhysicalDeviceExternalImageFormatPropertiesNV was not loaded successfully") }
    pub extern "C" fn get_physical_device_external_semaphore_properties (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceExternalSemaphoreInfo, _: core::ptr::NonNull<RawExternalSemaphoreProperties>) { panic!("vkGetPhysicalDeviceExternalSemaphoreProperties was not loaded successfully") }
    pub extern "C" fn get_physical_device_features (_: PhysicalDeviceRaw, _: core::ptr::NonNull<RawPhysicalDeviceFeatures>) { panic!("vkGetPhysicalDeviceFeatures was not loaded successfully") }
    pub extern "C" fn get_physical_device_features2 (_: PhysicalDeviceRaw, _: core::ptr::NonNull<RawPhysicalDeviceFeatures2>) { panic!("vkGetPhysicalDeviceFeatures2 was not loaded successfully") }
    pub extern "C" fn get_physical_device_format_properties (_: PhysicalDeviceRaw, _: RawFormat, _: core::ptr::NonNull<RawFormatProperties>) { panic!("vkGetPhysicalDeviceFormatProperties was not loaded successfully") }
    pub extern "C" fn get_physical_device_format_properties2 (_: PhysicalDeviceRaw, _: RawFormat, _: core::ptr::NonNull<RawFormatProperties2>) { panic!("vkGetPhysicalDeviceFormatProperties2 was not loaded successfully") }
    pub extern "C" fn get_physical_device_fragment_shading_rates_khr (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPhysicalDeviceFragmentShadingRateKHR>>) -> RawResult { panic!("vkGetPhysicalDeviceFragmentShadingRatesKHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_image_format_properties (_: PhysicalDeviceRaw, _: RawFormat, _: RawImageType, _: RawImageTiling, _: ImageUsageFlags, _: ImageCreateFlags, _: core::ptr::NonNull<RawImageFormatProperties>) -> RawResult { panic!("vkGetPhysicalDeviceImageFormatProperties was not loaded successfully") }
    pub extern "C" fn get_physical_device_image_format_properties2 (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceImageFormatInfo2, _: core::ptr::NonNull<RawImageFormatProperties2>) -> RawResult { panic!("vkGetPhysicalDeviceImageFormatProperties2 was not loaded successfully") }
    pub extern "C" fn get_physical_device_memory_properties (_: PhysicalDeviceRaw, _: core::ptr::NonNull<RawPhysicalDeviceMemoryProperties>) { panic!("vkGetPhysicalDeviceMemoryProperties was not loaded successfully") }
    pub extern "C" fn get_physical_device_memory_properties2 (_: PhysicalDeviceRaw, _: core::ptr::NonNull<RawPhysicalDeviceMemoryProperties2>) { panic!("vkGetPhysicalDeviceMemoryProperties2 was not loaded successfully") }
    pub extern "C" fn get_physical_device_multisample_properties_ext (_: PhysicalDeviceRaw, _: RawSampleCountFlagBits, _: core::ptr::NonNull<RawMultisamplePropertiesEXT>) { panic!("vkGetPhysicalDeviceMultisamplePropertiesEXT was not loaded successfully") }
    pub extern "C" fn get_physical_device_optical_flow_image_formats_nv (_: PhysicalDeviceRaw, _: &RawOpticalFlowImageFormatInfoNV, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawOpticalFlowImageFormatPropertiesNV>>) -> RawResult { panic!("vkGetPhysicalDeviceOpticalFlowImageFormatsNV was not loaded successfully") }
    pub extern "C" fn get_physical_device_present_rectangles_khr (_: PhysicalDeviceRaw, _: SurfaceKHRMutRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawRect2D>>) -> RawResult { panic!("vkGetPhysicalDevicePresentRectanglesKHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_properties (_: PhysicalDeviceRaw, _: core::ptr::NonNull<RawPhysicalDeviceProperties>) { panic!("vkGetPhysicalDeviceProperties was not loaded successfully") }
    pub extern "C" fn get_physical_device_properties2 (_: PhysicalDeviceRaw, _: core::ptr::NonNull<RawPhysicalDeviceProperties2>) { panic!("vkGetPhysicalDeviceProperties2 was not loaded successfully") }
    pub extern "C" fn get_physical_device_queue_family_performance_query_passes_khr (_: PhysicalDeviceRaw, _: &RawQueryPoolPerformanceCreateInfoKHR, _: core::ptr::NonNull<u32>) { panic!("vkGetPhysicalDeviceQueueFamilyPerformanceQueryPassesKHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_queue_family_properties (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawQueueFamilyProperties>>) { panic!("vkGetPhysicalDeviceQueueFamilyProperties was not loaded successfully") }
    pub extern "C" fn get_physical_device_queue_family_properties2 (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawQueueFamilyProperties2>>) { panic!("vkGetPhysicalDeviceQueueFamilyProperties2 was not loaded successfully") }
    pub extern "C" fn get_physical_device_screen_presentation_support_qnx (_: PhysicalDeviceRaw, _: u32, _: core::ptr::NonNull<screen_window>) -> Bool32 { panic!("vkGetPhysicalDeviceScreenPresentationSupportQNX was not loaded successfully") }
    pub extern "C" fn get_physical_device_sparse_image_format_properties (_: PhysicalDeviceRaw, _: RawFormat, _: RawImageType, _: RawSampleCountFlagBits, _: ImageUsageFlags, _: RawImageTiling, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawSparseImageFormatProperties>>) { panic!("vkGetPhysicalDeviceSparseImageFormatProperties was not loaded successfully") }
    pub extern "C" fn get_physical_device_sparse_image_format_properties2 (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceSparseImageFormatInfo2, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawSparseImageFormatProperties2>>) { panic!("vkGetPhysicalDeviceSparseImageFormatProperties2 was not loaded successfully") }
    pub extern "C" fn get_physical_device_supported_framebuffer_mixed_samples_combinations_nv (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawFramebufferMixedSamplesCombinationNV>>) -> RawResult { panic!("vkGetPhysicalDeviceSupportedFramebufferMixedSamplesCombinationsNV was not loaded successfully") }
    pub extern "C" fn get_physical_device_surface_capabilities2_ext (_: PhysicalDeviceRaw, _: SurfaceKHRRaw, _: core::ptr::NonNull<RawSurfaceCapabilities2EXT>) -> RawResult { panic!("vkGetPhysicalDeviceSurfaceCapabilities2EXT was not loaded successfully") }
    pub extern "C" fn get_physical_device_surface_capabilities2_khr (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceSurfaceInfo2KHR, _: core::ptr::NonNull<RawSurfaceCapabilities2KHR>) -> RawResult { panic!("vkGetPhysicalDeviceSurfaceCapabilities2KHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_surface_capabilities_khr (_: PhysicalDeviceRaw, _: SurfaceKHRRaw, _: core::ptr::NonNull<RawSurfaceCapabilitiesKHR>) -> RawResult { panic!("vkGetPhysicalDeviceSurfaceCapabilitiesKHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_surface_formats2_khr (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceSurfaceInfo2KHR, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawSurfaceFormat2KHR>>) -> RawResult { panic!("vkGetPhysicalDeviceSurfaceFormats2KHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_surface_formats_khr (_: PhysicalDeviceRaw, _: SurfaceKHRRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawSurfaceFormatKHR>>) -> RawResult { panic!("vkGetPhysicalDeviceSurfaceFormatsKHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_surface_present_modes2_ext (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceSurfaceInfo2KHR, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPresentModeKHR>>) -> RawResult { panic!("vkGetPhysicalDeviceSurfacePresentModes2EXT was not loaded successfully") }
    pub extern "C" fn get_physical_device_surface_present_modes_khr (_: PhysicalDeviceRaw, _: SurfaceKHRRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPresentModeKHR>>) -> RawResult { panic!("vkGetPhysicalDeviceSurfacePresentModesKHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_surface_support_khr (_: PhysicalDeviceRaw, _: u32, _: SurfaceKHRRaw, _: core::ptr::NonNull<Bool32>) -> RawResult { panic!("vkGetPhysicalDeviceSurfaceSupportKHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_tool_properties (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPhysicalDeviceToolProperties>>) -> RawResult { panic!("vkGetPhysicalDeviceToolProperties was not loaded successfully") }
    pub extern "C" fn get_physical_device_video_capabilities_khr (_: PhysicalDeviceRaw, _: &RawVideoProfileInfoKHR, _: core::ptr::NonNull<RawVideoCapabilitiesKHR>) -> RawResult { panic!("vkGetPhysicalDeviceVideoCapabilitiesKHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_video_format_properties_khr (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceVideoFormatInfoKHR, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawVideoFormatPropertiesKHR>>) -> RawResult { panic!("vkGetPhysicalDeviceVideoFormatPropertiesKHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_wayland_presentation_support_khr (_: PhysicalDeviceRaw, _: u32, _: core::ptr::NonNull<WlDisplay>) -> Bool32 { panic!("vkGetPhysicalDeviceWaylandPresentationSupportKHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_win32_presentation_support_khr (_: PhysicalDeviceRaw, _: u32) -> Bool32 { panic!("vkGetPhysicalDeviceWin32PresentationSupportKHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_xcb_presentation_support_khr (_: PhysicalDeviceRaw, _: u32, _: core::ptr::NonNull<xcb_connection_t>, _: xcb_visualid_t) -> Bool32 { panic!("vkGetPhysicalDeviceXcbPresentationSupportKHR was not loaded successfully") }
    pub extern "C" fn get_physical_device_xlib_presentation_support_khr (_: PhysicalDeviceRaw, _: u32, _: core::ptr::NonNull<Display>, _: VisualID) -> Bool32 { panic!("vkGetPhysicalDeviceXlibPresentationSupportKHR was not loaded successfully") }
    pub extern "C" fn get_pipeline_cache_data (_: DeviceRaw, _: PipelineCacheRaw, _: core::ptr::NonNull<usize>, _: Option<core::ptr::NonNull<u8>>) -> RawResult { panic!("vkGetPipelineCacheData was not loaded successfully") }
    pub extern "C" fn get_pipeline_executable_internal_representations_khr (_: DeviceRaw, _: &RawPipelineExecutableInfoKHR, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPipelineExecutableInternalRepresentationKHR>>) -> RawResult { panic!("vkGetPipelineExecutableInternalRepresentationsKHR was not loaded successfully") }
    pub extern "C" fn get_pipeline_executable_properties_khr (_: DeviceRaw, _: &RawPipelineInfoKHR, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPipelineExecutablePropertiesKHR>>) -> RawResult { panic!("vkGetPipelineExecutablePropertiesKHR was not loaded successfully") }
    pub extern "C" fn get_pipeline_executable_statistics_khr (_: DeviceRaw, _: &RawPipelineExecutableInfoKHR, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPipelineExecutableStatisticKHR>>) -> RawResult { panic!("vkGetPipelineExecutableStatisticsKHR was not loaded successfully") }
    pub extern "C" fn get_pipeline_properties_ext (_: DeviceRaw, _: &RawPipelineInfoKHR, _: core::ptr::NonNull<RawPipelinePropertiesIdentifierEXT>) -> RawResult { panic!("vkGetPipelinePropertiesEXT was not loaded successfully") }
    pub extern "C" fn get_private_data (_: DeviceRaw, _: RawObjectType, _: u64, _: PrivateDataSlotRaw, _: core::ptr::NonNull<u64>) { panic!("vkGetPrivateData was not loaded successfully") }
    pub extern "C" fn get_query_pool_results (_: DeviceRaw, _: QueryPoolRaw, _: u32, _: u32, _: usize, _: core::ptr::NonNull<u8>, _: DeviceSize, _: QueryResultFlags) -> RawResult { panic!("vkGetQueryPoolResults was not loaded successfully") }
    pub extern "C" fn get_queue_checkpoint_data2_nv (_: QueueRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawCheckpointData2NV>>) { panic!("vkGetQueueCheckpointData2NV was not loaded successfully") }
    pub extern "C" fn get_queue_checkpoint_data_nv (_: QueueRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawCheckpointDataNV>>) { panic!("vkGetQueueCheckpointDataNV was not loaded successfully") }
    pub extern "C" fn get_rand_r_output_display_ext (_: PhysicalDeviceRaw, _: core::ptr::NonNull<Display>, _: RROutput, _: core::ptr::NonNull<DisplayKHRRaw>) -> RawResult { panic!("vkGetRandROutputDisplayEXT was not loaded successfully") }
    pub extern "C" fn get_ray_tracing_capture_replay_shader_group_handles_khr (_: DeviceRaw, _: PipelineRaw, _: u32, _: u32, _: usize, _: core::ptr::NonNull<u8>) -> RawResult { panic!("vkGetRayTracingCaptureReplayShaderGroupHandlesKHR was not loaded successfully") }
    pub extern "C" fn get_ray_tracing_shader_group_handles_khr (_: DeviceRaw, _: PipelineRaw, _: u32, _: u32, _: usize, _: core::ptr::NonNull<u8>) -> RawResult { panic!("vkGetRayTracingShaderGroupHandlesKHR was not loaded successfully") }
    pub extern "C" fn get_ray_tracing_shader_group_stack_size_khr (_: DeviceRaw, _: PipelineRaw, _: u32, _: RawShaderGroupShaderKHR) -> DeviceSize { panic!("vkGetRayTracingShaderGroupStackSizeKHR was not loaded successfully") }
    pub extern "C" fn get_refresh_cycle_duration_google (_: DeviceRaw, _: SwapchainKHRMutRaw, _: core::ptr::NonNull<RawRefreshCycleDurationGOOGLE>) -> RawResult { panic!("vkGetRefreshCycleDurationGOOGLE was not loaded successfully") }
    pub extern "C" fn get_render_area_granularity (_: DeviceRaw, _: RenderPassRaw, _: core::ptr::NonNull<RawExtent2D>) { panic!("vkGetRenderAreaGranularity was not loaded successfully") }
    pub extern "C" fn get_sampler_opaque_capture_descriptor_data_ext (_: DeviceRaw, _: &RawSamplerCaptureDescriptorDataInfoEXT, _: core::ptr::NonNull<u8>) -> RawResult { panic!("vkGetSamplerOpaqueCaptureDescriptorDataEXT was not loaded successfully") }
    pub extern "C" fn get_semaphore_counter_value (_: DeviceRaw, _: SemaphoreRaw, _: core::ptr::NonNull<u64>) -> RawResult { panic!("vkGetSemaphoreCounterValue was not loaded successfully") }
    pub extern "C" fn get_semaphore_fd_khr (_: DeviceRaw, _: &RawSemaphoreGetFdInfoKHR, _: core::ptr::NonNull<::std::os::raw::c_int>) -> RawResult { panic!("vkGetSemaphoreFdKHR was not loaded successfully") }
    pub extern "C" fn get_semaphore_win32_handle_khr (_: DeviceRaw, _: &RawSemaphoreGetWin32HandleInfoKHR, _: core::ptr::NonNull<HANDLE>) -> RawResult { panic!("vkGetSemaphoreWin32HandleKHR was not loaded successfully") }
    pub extern "C" fn get_semaphore_zircon_handle_fuchsia (_: DeviceRaw, _: &RawSemaphoreGetZirconHandleInfoFUCHSIA, _: core::ptr::NonNull<zx_handle_t>) -> RawResult { panic!("vkGetSemaphoreZirconHandleFUCHSIA was not loaded successfully") }
    pub extern "C" fn get_shader_info_amd (_: DeviceRaw, _: PipelineRaw, _: RawShaderStageFlagBits, _: RawShaderInfoTypeAMD, _: core::ptr::NonNull<usize>, _: Option<core::ptr::NonNull<u8>>) -> RawResult { panic!("vkGetShaderInfoAMD was not loaded successfully") }
    pub extern "C" fn get_shader_module_create_info_identifier_ext (_: DeviceRaw, _: &RawShaderModuleCreateInfo, _: core::ptr::NonNull<RawShaderModuleIdentifierEXT>) { panic!("vkGetShaderModuleCreateInfoIdentifierEXT was not loaded successfully") }
    pub extern "C" fn get_shader_module_identifier_ext (_: DeviceRaw, _: ShaderModuleRaw, _: core::ptr::NonNull<RawShaderModuleIdentifierEXT>) { panic!("vkGetShaderModuleIdentifierEXT was not loaded successfully") }
    pub extern "C" fn get_swapchain_counter_ext (_: DeviceRaw, _: SwapchainKHRRaw, _: RawSurfaceCounterFlagBitsEXT, _: core::ptr::NonNull<u64>) -> RawResult { panic!("vkGetSwapchainCounterEXT was not loaded successfully") }
    pub extern "C" fn get_swapchain_images_khr (_: DeviceRaw, _: SwapchainKHRRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<ImageRaw>>) -> RawResult { panic!("vkGetSwapchainImagesKHR was not loaded successfully") }
    pub extern "C" fn get_swapchain_status_khr (_: DeviceRaw, _: SwapchainKHRMutRaw) -> RawResult { panic!("vkGetSwapchainStatusKHR was not loaded successfully") }
    pub extern "C" fn get_validation_cache_data_ext (_: DeviceRaw, _: ValidationCacheEXTRaw, _: core::ptr::NonNull<usize>, _: Option<core::ptr::NonNull<u8>>) -> RawResult { panic!("vkGetValidationCacheDataEXT was not loaded successfully") }
    pub extern "C" fn get_video_session_memory_requirements_khr (_: DeviceRaw, _: VideoSessionKHRRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawVideoSessionMemoryRequirementsKHR>>) -> RawResult { panic!("vkGetVideoSessionMemoryRequirementsKHR was not loaded successfully") }
    pub extern "C" fn get_winrt_display_nv (_: PhysicalDeviceRaw, _: u32, _: core::ptr::NonNull<DisplayKHRRaw>) -> RawResult { panic!("vkGetWinrtDisplayNV was not loaded successfully") }
    pub extern "C" fn import_fence_fd_khr (_: DeviceRaw, _: core::ptr::NonNull<RawImportFenceFdInfoKHR>) -> RawResult { panic!("vkImportFenceFdKHR was not loaded successfully") }
    pub extern "C" fn import_fence_win32_handle_khr (_: DeviceRaw, _: core::ptr::NonNull<RawImportFenceWin32HandleInfoKHR>) -> RawResult { panic!("vkImportFenceWin32HandleKHR was not loaded successfully") }
    pub extern "C" fn import_semaphore_fd_khr (_: DeviceRaw, _: core::ptr::NonNull<RawImportSemaphoreFdInfoKHR>) -> RawResult { panic!("vkImportSemaphoreFdKHR was not loaded successfully") }
    pub extern "C" fn import_semaphore_win32_handle_khr (_: DeviceRaw, _: core::ptr::NonNull<RawImportSemaphoreWin32HandleInfoKHR>) -> RawResult { panic!("vkImportSemaphoreWin32HandleKHR was not loaded successfully") }
    pub extern "C" fn import_semaphore_zircon_handle_fuchsia (_: DeviceRaw, _: core::ptr::NonNull<RawImportSemaphoreZirconHandleInfoFUCHSIA>) -> RawResult { panic!("vkImportSemaphoreZirconHandleFUCHSIA was not loaded successfully") }
    pub extern "C" fn initialize_performance_api_intel (_: DeviceRaw, _: &RawInitializePerformanceApiInfoINTEL) -> RawResult { panic!("vkInitializePerformanceApiINTEL was not loaded successfully") }
    pub extern "C" fn invalidate_mapped_memory_ranges (_: DeviceRaw, _: u32, _: &RawMappedMemoryRange) -> RawResult { panic!("vkInvalidateMappedMemoryRanges was not loaded successfully") }
    pub extern "C" fn map_memory (_: DeviceRaw, _: DeviceMemoryMutRaw, _: DeviceSize, _: DeviceSize, _: MemoryMapFlags, _: core::ptr::NonNull<Option<core::ptr::NonNull<u8>>>) -> RawResult { panic!("vkMapMemory was not loaded successfully") }
    pub extern "C" fn merge_pipeline_caches (_: DeviceRaw, _: PipelineCacheMutRaw, _: u32, _: &PipelineCacheRaw) -> RawResult { panic!("vkMergePipelineCaches was not loaded successfully") }
    pub extern "C" fn merge_validation_caches_ext (_: DeviceRaw, _: ValidationCacheEXTMutRaw, _: u32, _: &ValidationCacheEXTRaw) -> RawResult { panic!("vkMergeValidationCachesEXT was not loaded successfully") }
    pub extern "C" fn queue_begin_debug_utils_label_ext (_: QueueRaw, _: &RawDebugUtilsLabelEXT) { panic!("vkQueueBeginDebugUtilsLabelEXT was not loaded successfully") }
    pub extern "C" fn queue_bind_sparse (_: QueueMutRaw, _: u32, _: Option<&RawBindSparseInfo>, _: FenceMutRaw) -> RawResult { panic!("vkQueueBindSparse was not loaded successfully") }
    pub extern "C" fn queue_end_debug_utils_label_ext (_: QueueRaw) { panic!("vkQueueEndDebugUtilsLabelEXT was not loaded successfully") }
    pub extern "C" fn queue_insert_debug_utils_label_ext (_: QueueRaw, _: &RawDebugUtilsLabelEXT) { panic!("vkQueueInsertDebugUtilsLabelEXT was not loaded successfully") }
    pub extern "C" fn queue_present_khr (_: QueueMutRaw, _: core::ptr::NonNull<RawPresentInfoKHR>) -> RawResult { panic!("vkQueuePresentKHR was not loaded successfully") }
    pub extern "C" fn queue_set_performance_configuration_intel (_: QueueRaw, _: PerformanceConfigurationINTELRaw) -> RawResult { panic!("vkQueueSetPerformanceConfigurationINTEL was not loaded successfully") }
    pub extern "C" fn queue_submit (_: QueueMutRaw, _: u32, _: Option<&RawSubmitInfo>, _: FenceMutRaw) -> RawResult { panic!("vkQueueSubmit was not loaded successfully") }
    pub extern "C" fn queue_submit2 (_: QueueMutRaw, _: u32, _: Option<&RawSubmitInfo2>, _: FenceMutRaw) -> RawResult { panic!("vkQueueSubmit2 was not loaded successfully") }
    pub extern "C" fn queue_wait_idle (_: QueueMutRaw) -> RawResult { panic!("vkQueueWaitIdle was not loaded successfully") }
    pub extern "C" fn register_device_event_ext (_: DeviceRaw, _: &RawDeviceEventInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<FenceRaw>) -> RawResult { panic!("vkRegisterDeviceEventEXT was not loaded successfully") }
    pub extern "C" fn register_display_event_ext (_: DeviceRaw, _: DisplayKHRRaw, _: &RawDisplayEventInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<FenceRaw>) -> RawResult { panic!("vkRegisterDisplayEventEXT was not loaded successfully") }
    pub extern "C" fn release_display_ext (_: PhysicalDeviceRaw, _: DisplayKHRRaw) -> RawResult { panic!("vkReleaseDisplayEXT was not loaded successfully") }
    pub extern "C" fn release_full_screen_exclusive_mode_ext (_: DeviceRaw, _: SwapchainKHRRaw) -> RawResult { panic!("vkReleaseFullScreenExclusiveModeEXT was not loaded successfully") }
    pub extern "C" fn release_performance_configuration_intel (_: DeviceRaw, _: PerformanceConfigurationINTELMutRaw) -> RawResult { panic!("vkReleasePerformanceConfigurationINTEL was not loaded successfully") }
    pub extern "C" fn release_profiling_lock_khr (_: DeviceRaw) { panic!("vkReleaseProfilingLockKHR was not loaded successfully") }
    pub extern "C" fn release_swapchain_images_ext (_: DeviceRaw, _: core::ptr::NonNull<RawReleaseSwapchainImagesInfoEXT>) -> RawResult { panic!("vkReleaseSwapchainImagesEXT was not loaded successfully") }
    pub extern "C" fn reset_command_buffer (_: CommandBufferMutRaw, _: CommandBufferResetFlags) -> RawResult { panic!("vkResetCommandBuffer was not loaded successfully") }
    pub extern "C" fn reset_command_pool (_: DeviceRaw, _: CommandPoolMutRaw, _: CommandPoolResetFlags) -> RawResult { panic!("vkResetCommandPool was not loaded successfully") }
    pub extern "C" fn reset_descriptor_pool (_: DeviceRaw, _: DescriptorPoolMutRaw, _: DescriptorPoolResetFlags) -> RawResult { panic!("vkResetDescriptorPool was not loaded successfully") }
    pub extern "C" fn reset_event (_: DeviceRaw, _: EventMutRaw) -> RawResult { panic!("vkResetEvent was not loaded successfully") }
    pub extern "C" fn reset_fences (_: DeviceRaw, _: u32, _: core::ptr::NonNull<FenceMutRaw>) -> RawResult { panic!("vkResetFences was not loaded successfully") }
    pub extern "C" fn reset_query_pool (_: DeviceRaw, _: QueryPoolRaw, _: u32, _: u32) { panic!("vkResetQueryPool was not loaded successfully") }
    pub extern "C" fn set_buffer_collection_buffer_constraints_fuchsia (_: DeviceRaw, _: BufferCollectionFUCHSIARaw, _: &RawBufferConstraintsInfoFUCHSIA) -> RawResult { panic!("vkSetBufferCollectionBufferConstraintsFUCHSIA was not loaded successfully") }
    pub extern "C" fn set_buffer_collection_image_constraints_fuchsia (_: DeviceRaw, _: BufferCollectionFUCHSIARaw, _: &RawImageConstraintsInfoFUCHSIA) -> RawResult { panic!("vkSetBufferCollectionImageConstraintsFUCHSIA was not loaded successfully") }
    pub extern "C" fn set_debug_utils_object_name_ext (_: DeviceRaw, _: core::ptr::NonNull<RawDebugUtilsObjectNameInfoEXT>) -> RawResult { panic!("vkSetDebugUtilsObjectNameEXT was not loaded successfully") }
    pub extern "C" fn set_debug_utils_object_tag_ext (_: DeviceRaw, _: core::ptr::NonNull<RawDebugUtilsObjectTagInfoEXT>) -> RawResult { panic!("vkSetDebugUtilsObjectTagEXT was not loaded successfully") }
    pub extern "C" fn set_device_memory_priority_ext (_: DeviceRaw, _: DeviceMemoryRaw, _: f32) { panic!("vkSetDeviceMemoryPriorityEXT was not loaded successfully") }
    pub extern "C" fn set_event (_: DeviceRaw, _: EventMutRaw) -> RawResult { panic!("vkSetEvent was not loaded successfully") }
    pub extern "C" fn set_hdr_metadata_ext (_: DeviceRaw, _: u32, _: &SwapchainKHRRaw, _: &RawHdrMetadataEXT) { panic!("vkSetHdrMetadataEXT was not loaded successfully") }
    pub extern "C" fn set_local_dimming_amd (_: DeviceRaw, _: SwapchainKHRRaw, _: Bool32) { panic!("vkSetLocalDimmingAMD was not loaded successfully") }
    pub extern "C" fn set_private_data (_: DeviceRaw, _: RawObjectType, _: u64, _: PrivateDataSlotRaw, _: u64) -> RawResult { panic!("vkSetPrivateData was not loaded successfully") }
    pub extern "C" fn signal_semaphore (_: DeviceRaw, _: &RawSemaphoreSignalInfo) -> RawResult { panic!("vkSignalSemaphore was not loaded successfully") }
    pub extern "C" fn submit_debug_utils_message_ext (_: InstanceRaw, _: RawDebugUtilsMessageSeverityFlagBitsEXT, _: DebugUtilsMessageTypeFlagsEXT, _: &RawDebugUtilsMessengerCallbackDataEXT) { panic!("vkSubmitDebugUtilsMessageEXT was not loaded successfully") }
    pub extern "C" fn trim_command_pool (_: DeviceRaw, _: CommandPoolMutRaw, _: CommandPoolTrimFlags) { panic!("vkTrimCommandPool was not loaded successfully") }
    pub extern "C" fn uninitialize_performance_api_intel (_: DeviceRaw) { panic!("vkUninitializePerformanceApiINTEL was not loaded successfully") }
    pub extern "C" fn unmap_memory (_: DeviceRaw, _: DeviceMemoryMutRaw) { panic!("vkUnmapMemory was not loaded successfully") }
    pub extern "C" fn update_descriptor_set_with_template (_: DeviceRaw, _: DescriptorSetRaw, _: DescriptorUpdateTemplateRaw, _: Option<&u8>) { panic!("vkUpdateDescriptorSetWithTemplate was not loaded successfully") }
    pub extern "C" fn update_descriptor_sets (_: DeviceRaw, _: u32, _: Option<&RawWriteDescriptorSet>, _: u32, _: Option<&RawCopyDescriptorSet>) { panic!("vkUpdateDescriptorSets was not loaded successfully") }
    pub extern "C" fn update_video_session_parameters_khr (_: DeviceRaw, _: VideoSessionParametersKHRRaw, _: &RawVideoSessionParametersUpdateInfoKHR) -> RawResult { panic!("vkUpdateVideoSessionParametersKHR was not loaded successfully") }
    pub extern "C" fn wait_for_fences (_: DeviceRaw, _: u32, _: &FenceRaw, _: Bool32, _: u64) -> RawResult { panic!("vkWaitForFences was not loaded successfully") }
    pub extern "C" fn wait_for_present_khr (_: DeviceRaw, _: SwapchainKHRMutRaw, _: u64, _: u64) -> RawResult { panic!("vkWaitForPresentKHR was not loaded successfully") }
    pub extern "C" fn wait_semaphores (_: DeviceRaw, _: &RawSemaphoreWaitInfo, _: u64) -> RawResult { panic!("vkWaitSemaphores was not loaded successfully") }
    pub extern "C" fn write_acceleration_structures_properties_khr (_: DeviceRaw, _: u32, _: &AccelerationStructureKHRRaw, _: RawQueryType, _: usize, _: core::ptr::NonNull<u8>, _: usize) -> RawResult { panic!("vkWriteAccelerationStructuresPropertiesKHR was not loaded successfully") }
    pub extern "C" fn write_micromaps_properties_ext (_: DeviceRaw, _: u32, _: &MicromapEXTRaw, _: RawQueryType, _: usize, _: core::ptr::NonNull<u8>, _: usize) -> RawResult { panic!("vkWriteMicromapsPropertiesEXT was not loaded successfully") }
}

pub(crate) mod null_functions {
    use super::*;
    pub extern "C" fn acquire_drm_display_ext (_: PhysicalDeviceRaw, _: i32, _: DisplayKHRRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn acquire_full_screen_exclusive_mode_ext (_: DeviceRaw, _: SwapchainKHRRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn acquire_next_image2_khr (_: DeviceRaw, _: core::ptr::NonNull<RawAcquireNextImageInfoKHR>, _: core::ptr::NonNull<u32>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn acquire_next_image_khr (_: DeviceRaw, _: SwapchainKHRMutRaw, _: u64, _: SemaphoreMutRaw, _: FenceMutRaw, _: core::ptr::NonNull<u32>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn acquire_performance_configuration_intel (_: DeviceRaw, _: &RawPerformanceConfigurationAcquireInfoINTEL, _: core::ptr::NonNull<PerformanceConfigurationINTELRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn acquire_profiling_lock_khr (_: DeviceRaw, _: &RawAcquireProfilingLockInfoKHR) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn acquire_winrt_display_nv (_: PhysicalDeviceRaw, _: DisplayKHRRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn acquire_xlib_display_ext (_: PhysicalDeviceRaw, _: core::ptr::NonNull<Display>, _: DisplayKHRRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn allocate_command_buffers (_: DeviceRaw, _: core::ptr::NonNull<RawCommandBufferAllocateInfo>, _: core::ptr::NonNull<CommandBufferRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn allocate_descriptor_sets (_: DeviceRaw, _: core::ptr::NonNull<RawDescriptorSetAllocateInfo>, _: core::ptr::NonNull<DescriptorSetRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn allocate_memory (_: DeviceRaw, _: &RawMemoryAllocateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DeviceMemoryRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn begin_command_buffer (_: CommandBufferMutRaw, _: &RawCommandBufferBeginInfo) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn bind_acceleration_structure_memory_nv (_: DeviceRaw, _: u32, _: &RawBindAccelerationStructureMemoryInfoNV) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn bind_buffer_memory (_: DeviceRaw, _: BufferMutRaw, _: DeviceMemoryRaw, _: DeviceSize) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn bind_buffer_memory2 (_: DeviceRaw, _: u32, _: &RawBindBufferMemoryInfo) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn bind_image_memory (_: DeviceRaw, _: ImageMutRaw, _: DeviceMemoryRaw, _: DeviceSize) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn bind_image_memory2 (_: DeviceRaw, _: u32, _: &RawBindImageMemoryInfo) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn bind_optical_flow_session_image_nv (_: DeviceRaw, _: OpticalFlowSessionNVRaw, _: RawOpticalFlowSessionBindingPointNV, _: ImageViewRaw, _: RawImageLayout) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn bind_video_session_memory_khr (_: DeviceRaw, _: VideoSessionKHRMutRaw, _: u32, _: &RawBindVideoSessionMemoryInfoKHR) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn build_acceleration_structures_khr (_: DeviceRaw, _: DeferredOperationKHRRaw, _: u32, _: &RawAccelerationStructureBuildGeometryInfoKHR, _: &&RawAccelerationStructureBuildRangeInfoKHR) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn build_micromaps_ext (_: DeviceRaw, _: DeferredOperationKHRRaw, _: u32, _: &RawMicromapBuildInfoEXT) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_begin_conditional_rendering_ext (_: CommandBufferMutRaw, _: &RawConditionalRenderingBeginInfoEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_begin_debug_utils_label_ext (_: CommandBufferMutRaw, _: &RawDebugUtilsLabelEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_begin_query (_: CommandBufferMutRaw, _: QueryPoolRaw, _: u32, _: QueryControlFlags) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_begin_query_indexed_ext (_: CommandBufferMutRaw, _: QueryPoolRaw, _: u32, _: QueryControlFlags, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_begin_render_pass (_: CommandBufferMutRaw, _: &RawRenderPassBeginInfo, _: RawSubpassContents) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_begin_render_pass2 (_: CommandBufferMutRaw, _: &RawRenderPassBeginInfo, _: &RawSubpassBeginInfo) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_begin_rendering (_: CommandBufferMutRaw, _: &RawRenderingInfo) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_begin_transform_feedback_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: Option<&BufferRaw>, _: Option<&DeviceSize>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_begin_video_coding_khr (_: CommandBufferMutRaw, _: &RawVideoBeginCodingInfoKHR) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_bind_descriptor_buffer_embedded_samplers_ext (_: CommandBufferMutRaw, _: RawPipelineBindPoint, _: PipelineLayoutRaw, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_bind_descriptor_buffers_ext (_: CommandBufferMutRaw, _: u32, _: &RawDescriptorBufferBindingInfoEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_bind_descriptor_sets (_: CommandBufferMutRaw, _: RawPipelineBindPoint, _: PipelineLayoutRaw, _: u32, _: u32, _: &DescriptorSetRaw, _: u32, _: Option<&u32>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_bind_index_buffer (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: RawIndexType) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_bind_invocation_mask_huawei (_: CommandBufferMutRaw, _: ImageViewRaw, _: RawImageLayout) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_bind_pipeline (_: CommandBufferMutRaw, _: RawPipelineBindPoint, _: PipelineRaw) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_bind_pipeline_shader_group_nv (_: CommandBufferMutRaw, _: RawPipelineBindPoint, _: PipelineRaw, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_bind_shading_rate_image_nv (_: CommandBufferMutRaw, _: ImageViewRaw, _: RawImageLayout) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_bind_transform_feedback_buffers_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: &BufferRaw, _: &DeviceSize, _: Option<&DeviceSize>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_bind_vertex_buffers (_: CommandBufferMutRaw, _: u32, _: u32, _: &BufferRaw, _: &DeviceSize) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_bind_vertex_buffers2 (_: CommandBufferMutRaw, _: u32, _: u32, _: &BufferRaw, _: &DeviceSize, _: Option<&DeviceSize>, _: Option<&DeviceSize>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_blit_image (_: CommandBufferMutRaw, _: ImageRaw, _: RawImageLayout, _: ImageRaw, _: RawImageLayout, _: u32, _: &RawImageBlit, _: RawFilter) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_blit_image2 (_: CommandBufferMutRaw, _: &RawBlitImageInfo2) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_build_acceleration_structure_nv (_: CommandBufferMutRaw, _: &RawAccelerationStructureInfoNV, _: BufferRaw, _: DeviceSize, _: Bool32, _: AccelerationStructureNVRaw, _: AccelerationStructureNVRaw, _: BufferRaw, _: DeviceSize) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_build_acceleration_structures_indirect_khr (_: CommandBufferMutRaw, _: u32, _: &RawAccelerationStructureBuildGeometryInfoKHR, _: &DeviceAddress, _: &u32, _: &&u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_build_acceleration_structures_khr (_: CommandBufferMutRaw, _: u32, _: &RawAccelerationStructureBuildGeometryInfoKHR, _: &&RawAccelerationStructureBuildRangeInfoKHR) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_build_micromaps_ext (_: CommandBufferMutRaw, _: u32, _: &RawMicromapBuildInfoEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_clear_attachments (_: CommandBufferMutRaw, _: u32, _: &RawClearAttachment, _: u32, _: &RawClearRect) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_clear_colour_image (_: CommandBufferMutRaw, _: ImageRaw, _: RawImageLayout, _: Option<&RawClearColourValue>, _: u32, _: &RawImageSubresourceRange) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_clear_depth_stencil_image (_: CommandBufferMutRaw, _: ImageRaw, _: RawImageLayout, _: &RawClearDepthStencilValue, _: u32, _: &RawImageSubresourceRange) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_control_video_coding_khr (_: CommandBufferMutRaw, _: &RawVideoCodingControlInfoKHR) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_acceleration_structure_khr (_: CommandBufferMutRaw, _: &RawCopyAccelerationStructureInfoKHR) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_acceleration_structure_nv (_: CommandBufferMutRaw, _: AccelerationStructureNVRaw, _: AccelerationStructureNVRaw, _: RawCopyAccelerationStructureModeKHR) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_acceleration_structure_to_memory_khr (_: CommandBufferMutRaw, _: &RawCopyAccelerationStructureToMemoryInfoKHR) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_buffer (_: CommandBufferMutRaw, _: BufferRaw, _: BufferRaw, _: u32, _: &RawBufferCopy) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_buffer2 (_: CommandBufferMutRaw, _: &RawCopyBufferInfo2) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_buffer_to_image (_: CommandBufferMutRaw, _: BufferRaw, _: ImageRaw, _: RawImageLayout, _: u32, _: &RawBufferImageCopy) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_buffer_to_image2 (_: CommandBufferMutRaw, _: &RawCopyBufferToImageInfo2) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_image (_: CommandBufferMutRaw, _: ImageRaw, _: RawImageLayout, _: ImageRaw, _: RawImageLayout, _: u32, _: &RawImageCopy) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_image2 (_: CommandBufferMutRaw, _: &RawCopyImageInfo2) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_image_to_buffer (_: CommandBufferMutRaw, _: ImageRaw, _: RawImageLayout, _: BufferRaw, _: u32, _: &RawBufferImageCopy) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_image_to_buffer2 (_: CommandBufferMutRaw, _: &RawCopyImageToBufferInfo2) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_memory_indirect_nv (_: CommandBufferMutRaw, _: DeviceAddress, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_memory_to_acceleration_structure_khr (_: CommandBufferMutRaw, _: &RawCopyMemoryToAccelerationStructureInfoKHR) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_memory_to_image_indirect_nv (_: CommandBufferMutRaw, _: DeviceAddress, _: u32, _: u32, _: ImageRaw, _: RawImageLayout, _: &RawImageSubresourceLayers) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_memory_to_micromap_ext (_: CommandBufferMutRaw, _: &RawCopyMemoryToMicromapInfoEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_micromap_ext (_: CommandBufferMutRaw, _: &RawCopyMicromapInfoEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_micromap_to_memory_ext (_: CommandBufferMutRaw, _: &RawCopyMicromapToMemoryInfoEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_copy_query_pool_results (_: CommandBufferMutRaw, _: QueryPoolRaw, _: u32, _: u32, _: BufferRaw, _: DeviceSize, _: DeviceSize, _: QueryResultFlags) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_cu_launch_kernel_nvx (_: CommandBufferRaw, _: &RawCuLaunchInfoNVX) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_debug_marker_begin_ext (_: CommandBufferMutRaw, _: &RawDebugMarkerMarkerInfoEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_debug_marker_end_ext (_: CommandBufferMutRaw) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_debug_marker_insert_ext (_: CommandBufferMutRaw, _: &RawDebugMarkerMarkerInfoEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_decode_video_khr (_: CommandBufferMutRaw, _: &RawVideoDecodeInfoKHR) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_decompress_memory_indirect_count_nv (_: CommandBufferMutRaw, _: DeviceAddress, _: DeviceAddress, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_decompress_memory_nv (_: CommandBufferMutRaw, _: u32, _: &RawDecompressMemoryRegionNV) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_dispatch (_: CommandBufferMutRaw, _: u32, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_dispatch_base (_: CommandBufferMutRaw, _: u32, _: u32, _: u32, _: u32, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_dispatch_indirect (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_draw (_: CommandBufferMutRaw, _: u32, _: u32, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_draw_cluster_huawei (_: CommandBufferMutRaw, _: u32, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_draw_cluster_indirect_huawei (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_draw_indexed (_: CommandBufferMutRaw, _: u32, _: u32, _: u32, _: i32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_draw_indexed_indirect (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_draw_indexed_indirect_count (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_draw_indirect (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_draw_indirect_byte_count_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_draw_indirect_count (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_draw_mesh_tasks_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_draw_mesh_tasks_indirect_count_ext (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_draw_mesh_tasks_indirect_count_nv (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_draw_mesh_tasks_indirect_ext (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_draw_mesh_tasks_indirect_nv (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_draw_mesh_tasks_nv (_: CommandBufferMutRaw, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_draw_multi_ext (_: CommandBufferMutRaw, _: u32, _: Option<&RawMultiDrawInfoEXT>, _: u32, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_draw_multi_indexed_ext (_: CommandBufferMutRaw, _: u32, _: Option<&RawMultiDrawIndexedInfoEXT>, _: u32, _: u32, _: u32, _: Option<&i32>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_end_conditional_rendering_ext (_: CommandBufferMutRaw) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_end_debug_utils_label_ext (_: CommandBufferMutRaw) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_end_query (_: CommandBufferMutRaw, _: QueryPoolRaw, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_end_query_indexed_ext (_: CommandBufferMutRaw, _: QueryPoolRaw, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_end_render_pass (_: CommandBufferMutRaw) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_end_render_pass2 (_: CommandBufferMutRaw, _: &RawSubpassEndInfo) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_end_rendering (_: CommandBufferMutRaw) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_end_transform_feedback_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: Option<&BufferRaw>, _: Option<&DeviceSize>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_end_video_coding_khr (_: CommandBufferMutRaw, _: &RawVideoEndCodingInfoKHR) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_execute_commands (_: CommandBufferMutRaw, _: u32, _: &CommandBufferRaw) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_execute_generated_commands_nv (_: CommandBufferMutRaw, _: Bool32, _: &RawGeneratedCommandsInfoNV) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_fill_buffer (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: DeviceSize, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_insert_debug_utils_label_ext (_: CommandBufferMutRaw, _: &RawDebugUtilsLabelEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_next_subpass (_: CommandBufferMutRaw, _: RawSubpassContents) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_next_subpass2 (_: CommandBufferMutRaw, _: &RawSubpassBeginInfo, _: &RawSubpassEndInfo) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_optical_flow_execute_nv (_: CommandBufferRaw, _: OpticalFlowSessionNVRaw, _: &RawOpticalFlowExecuteInfoNV) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_pipeline_barrier (_: CommandBufferMutRaw, _: PipelineStageFlags, _: PipelineStageFlags, _: DependencyFlags, _: u32, _: Option<&RawMemoryBarrier>, _: u32, _: Option<&RawBufferMemoryBarrier>, _: u32, _: Option<&RawImageMemoryBarrier>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_pipeline_barrier2 (_: CommandBufferMutRaw, _: &RawDependencyInfo) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_preprocess_generated_commands_nv (_: CommandBufferMutRaw, _: &RawGeneratedCommandsInfoNV) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_push_constants (_: CommandBufferMutRaw, _: PipelineLayoutRaw, _: ShaderStageFlags, _: u32, _: u32, _: &u8) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_push_descriptor_set_khr (_: CommandBufferMutRaw, _: RawPipelineBindPoint, _: PipelineLayoutRaw, _: u32, _: u32, _: &RawWriteDescriptorSet) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_push_descriptor_set_with_template_khr (_: CommandBufferMutRaw, _: DescriptorUpdateTemplateRaw, _: PipelineLayoutRaw, _: u32, _: Option<&u8>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_reset_event (_: CommandBufferMutRaw, _: EventRaw, _: PipelineStageFlags) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_reset_event2 (_: CommandBufferMutRaw, _: EventRaw, _: PipelineStageFlags2) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_reset_query_pool (_: CommandBufferMutRaw, _: QueryPoolRaw, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_resolve_image (_: CommandBufferMutRaw, _: ImageRaw, _: RawImageLayout, _: ImageRaw, _: RawImageLayout, _: u32, _: &RawImageResolve) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_resolve_image2 (_: CommandBufferMutRaw, _: &RawResolveImageInfo2) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_alpha_to_coverage_enable_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_alpha_to_one_enable_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_blend_constants (_: CommandBufferMutRaw, _: ConstSizePtr<f32, 4>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_checkpoint_nv (_: CommandBufferMutRaw, _: Option<&u8>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_coarse_sample_order_nv (_: CommandBufferMutRaw, _: RawCoarseSampleOrderTypeNV, _: u32, _: Option<&RawCoarseSampleOrderCustomNV>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_colour_blend_advanced_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawColourBlendAdvancedEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_colour_blend_enable_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: &Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_colour_blend_equation_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawColourBlendEquationEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_colour_write_enable_ext (_: CommandBufferMutRaw, _: u32, _: &Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_colour_write_mask_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: &ColourComponentFlags) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_conservative_rasterization_mode_ext (_: CommandBufferMutRaw, _: RawConservativeRasterizationModeEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_coverage_modulation_mode_nv (_: CommandBufferMutRaw, _: RawCoverageModulationModeNV) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_coverage_modulation_table_enable_nv (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_coverage_modulation_table_nv (_: CommandBufferMutRaw, _: u32, _: &f32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_coverage_reduction_mode_nv (_: CommandBufferMutRaw, _: RawCoverageReductionModeNV) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_coverage_to_colour_enable_nv (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_coverage_to_colour_location_nv (_: CommandBufferMutRaw, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_cull_mode (_: CommandBufferMutRaw, _: CullModeFlags) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_depth_bias (_: CommandBufferMutRaw, _: f32, _: f32, _: f32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_depth_bias_enable (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_depth_bounds (_: CommandBufferMutRaw, _: f32, _: f32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_depth_bounds_test_enable (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_depth_clamp_enable_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_depth_clip_enable_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_depth_clip_negative_one_to_one_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_depth_compare_op (_: CommandBufferMutRaw, _: RawCompareOp) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_depth_test_enable (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_depth_write_enable (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_descriptor_buffer_offsets_ext (_: CommandBufferMutRaw, _: RawPipelineBindPoint, _: PipelineLayoutRaw, _: u32, _: u32, _: &u32, _: &DeviceSize) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_device_mask (_: CommandBufferMutRaw, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_discard_rectangle_ext (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawRect2D) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_discard_rectangle_enable_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_discard_rectangle_mode_ext (_: CommandBufferMutRaw, _: RawDiscardRectangleModeEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_event (_: CommandBufferMutRaw, _: EventRaw, _: PipelineStageFlags) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_event2 (_: CommandBufferMutRaw, _: EventRaw, _: &RawDependencyInfo) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_exclusive_scissor_enable_nv (_: CommandBufferMutRaw, _: u32, _: u32, _: &Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_exclusive_scissor_nv (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawRect2D) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_extra_primitive_overestimation_size_ext (_: CommandBufferMutRaw, _: f32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_fragment_shading_rate_enum_nv (_: CommandBufferMutRaw, _: RawFragmentShadingRateNV, _: ConstSizePtr<RawFragmentShadingRateCombinerOpKHR, 2>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_fragment_shading_rate_khr (_: CommandBufferMutRaw, _: &RawExtent2D, _: ConstSizePtr<RawFragmentShadingRateCombinerOpKHR, 2>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_front_face (_: CommandBufferMutRaw, _: RawFrontFace) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_line_rasterization_mode_ext (_: CommandBufferMutRaw, _: RawLineRasterizationModeEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_line_stipple_ext (_: CommandBufferMutRaw, _: u32, _: u16) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_line_stipple_enable_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_line_width (_: CommandBufferMutRaw, _: f32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_logic_op_ext (_: CommandBufferMutRaw, _: RawLogicOp) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_logic_op_enable_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_patch_control_points_ext (_: CommandBufferMutRaw, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_performance_marker_intel (_: CommandBufferMutRaw, _: &RawPerformanceMarkerInfoINTEL) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_performance_override_intel (_: CommandBufferMutRaw, _: &RawPerformanceOverrideInfoINTEL) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_performance_stream_marker_intel (_: CommandBufferMutRaw, _: &RawPerformanceStreamMarkerInfoINTEL) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_polygon_mode_ext (_: CommandBufferMutRaw, _: RawPolygonMode) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_primitive_restart_enable (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_primitive_topology (_: CommandBufferMutRaw, _: RawPrimitiveTopology) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_provoking_vertex_mode_ext (_: CommandBufferMutRaw, _: RawProvokingVertexModeEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_rasterization_samples_ext (_: CommandBufferMutRaw, _: RawSampleCountFlagBits) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_rasterization_stream_ext (_: CommandBufferMutRaw, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_rasterizer_discard_enable (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_ray_tracing_pipeline_stack_size_khr (_: CommandBufferMutRaw, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_representative_fragment_test_enable_nv (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_sample_locations_ext (_: CommandBufferMutRaw, _: &RawSampleLocationsInfoEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_sample_locations_enable_ext (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_sample_mask_ext (_: CommandBufferMutRaw, _: RawSampleCountFlagBits, _: &SampleMask) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_scissor (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawRect2D) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_scissor_with_count (_: CommandBufferMutRaw, _: u32, _: &RawRect2D) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_shading_rate_image_enable_nv (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_stencil_compare_mask (_: CommandBufferMutRaw, _: StencilFaceFlags, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_stencil_op (_: CommandBufferMutRaw, _: StencilFaceFlags, _: RawStencilOp, _: RawStencilOp, _: RawStencilOp, _: RawCompareOp) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_stencil_reference (_: CommandBufferMutRaw, _: StencilFaceFlags, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_stencil_test_enable (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_stencil_write_mask (_: CommandBufferMutRaw, _: StencilFaceFlags, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_tessellation_domain_origin_ext (_: CommandBufferMutRaw, _: RawTessellationDomainOrigin) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_vertex_input_ext (_: CommandBufferMutRaw, _: u32, _: Option<&RawVertexInputBindingDescription2EXT>, _: u32, _: Option<&RawVertexInputAttributeDescription2EXT>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_viewport (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawViewport) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_viewport_shading_rate_palette_nv (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawShadingRatePaletteNV) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_viewport_swizzle_nv (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawViewportSwizzleNV) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_viewport_w_scaling_enable_nv (_: CommandBufferMutRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_viewport_w_scaling_nv (_: CommandBufferMutRaw, _: u32, _: u32, _: &RawViewportWScalingNV) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_set_viewport_with_count (_: CommandBufferMutRaw, _: u32, _: &RawViewport) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_subpass_shading_huawei (_: CommandBufferMutRaw) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_trace_rays_indirect2_khr (_: CommandBufferMutRaw, _: DeviceAddress) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_trace_rays_indirect_khr (_: CommandBufferMutRaw, _: &RawStridedDeviceAddressRegionKHR, _: &RawStridedDeviceAddressRegionKHR, _: &RawStridedDeviceAddressRegionKHR, _: &RawStridedDeviceAddressRegionKHR, _: DeviceAddress) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_trace_rays_khr (_: CommandBufferMutRaw, _: &RawStridedDeviceAddressRegionKHR, _: &RawStridedDeviceAddressRegionKHR, _: &RawStridedDeviceAddressRegionKHR, _: &RawStridedDeviceAddressRegionKHR, _: u32, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_trace_rays_nv (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: BufferRaw, _: DeviceSize, _: DeviceSize, _: BufferRaw, _: DeviceSize, _: DeviceSize, _: BufferRaw, _: DeviceSize, _: DeviceSize, _: u32, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_update_buffer (_: CommandBufferMutRaw, _: BufferRaw, _: DeviceSize, _: DeviceSize, _: &u8) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_wait_events (_: CommandBufferMutRaw, _: u32, _: &EventRaw, _: PipelineStageFlags, _: PipelineStageFlags, _: u32, _: Option<&RawMemoryBarrier>, _: u32, _: Option<&RawBufferMemoryBarrier>, _: u32, _: Option<&RawImageMemoryBarrier>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_wait_events2 (_: CommandBufferMutRaw, _: u32, _: &EventRaw, _: &RawDependencyInfo) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_write_acceleration_structures_properties_khr (_: CommandBufferMutRaw, _: u32, _: &AccelerationStructureKHRRaw, _: RawQueryType, _: QueryPoolRaw, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_write_acceleration_structures_properties_nv (_: CommandBufferMutRaw, _: u32, _: &AccelerationStructureNVRaw, _: RawQueryType, _: QueryPoolRaw, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_write_buffer_marker2_amd (_: CommandBufferMutRaw, _: PipelineStageFlags2, _: BufferRaw, _: DeviceSize, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_write_buffer_marker_amd (_: CommandBufferMutRaw, _: RawPipelineStageFlagBits, _: BufferRaw, _: DeviceSize, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_write_micromaps_properties_ext (_: CommandBufferMutRaw, _: u32, _: &MicromapEXTRaw, _: RawQueryType, _: QueryPoolRaw, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_write_timestamp (_: CommandBufferMutRaw, _: RawPipelineStageFlagBits, _: QueryPoolRaw, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn cmd_write_timestamp2 (_: CommandBufferMutRaw, _: PipelineStageFlags2, _: QueryPoolRaw, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn compile_deferred_nv (_: DeviceRaw, _: PipelineRaw, _: u32) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn copy_acceleration_structure_khr (_: DeviceRaw, _: DeferredOperationKHRRaw, _: &RawCopyAccelerationStructureInfoKHR) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn copy_acceleration_structure_to_memory_khr (_: DeviceRaw, _: DeferredOperationKHRRaw, _: &RawCopyAccelerationStructureToMemoryInfoKHR) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn copy_memory_to_acceleration_structure_khr (_: DeviceRaw, _: DeferredOperationKHRRaw, _: &RawCopyMemoryToAccelerationStructureInfoKHR) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn copy_memory_to_micromap_ext (_: DeviceRaw, _: DeferredOperationKHRRaw, _: &RawCopyMemoryToMicromapInfoEXT) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn copy_micromap_ext (_: DeviceRaw, _: DeferredOperationKHRRaw, _: &RawCopyMicromapInfoEXT) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn copy_micromap_to_memory_ext (_: DeviceRaw, _: DeferredOperationKHRRaw, _: &RawCopyMicromapToMemoryInfoEXT) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_acceleration_structure_khr (_: DeviceRaw, _: &RawAccelerationStructureCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<AccelerationStructureKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_acceleration_structure_nv (_: DeviceRaw, _: &RawAccelerationStructureCreateInfoNV, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<AccelerationStructureNVRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_android_surface_khr (_: InstanceRaw, _: &RawAndroidSurfaceCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_buffer (_: DeviceRaw, _: &RawBufferCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<BufferRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_buffer_collection_fuchsia (_: DeviceRaw, _: &RawBufferCollectionCreateInfoFUCHSIA, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<BufferCollectionFUCHSIARaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_buffer_view (_: DeviceRaw, _: &RawBufferViewCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<BufferViewRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_command_pool (_: DeviceRaw, _: &RawCommandPoolCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<CommandPoolRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_compute_pipelines (_: DeviceRaw, _: PipelineCacheRaw, _: u32, _: &RawComputePipelineCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<PipelineRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_cu_function_nvx (_: DeviceRaw, _: &RawCuFunctionCreateInfoNVX, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<CuFunctionNVXRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_cu_module_nvx (_: DeviceRaw, _: &RawCuModuleCreateInfoNVX, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<CuModuleNVXRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_debug_report_callback_ext (_: InstanceRaw, _: &RawDebugReportCallbackCreateInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DebugReportCallbackEXTRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_debug_utils_messenger_ext (_: InstanceRaw, _: &RawDebugUtilsMessengerCreateInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DebugUtilsMessengerEXTRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_deferred_operation_khr (_: DeviceRaw, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DeferredOperationKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_descriptor_pool (_: DeviceRaw, _: &RawDescriptorPoolCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DescriptorPoolRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_descriptor_set_layout (_: DeviceRaw, _: &RawDescriptorSetLayoutCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DescriptorSetLayoutRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_descriptor_update_template (_: DeviceRaw, _: &RawDescriptorUpdateTemplateCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DescriptorUpdateTemplateRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_device (_: PhysicalDeviceRaw, _: &RawDeviceCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DeviceRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_direct_fb_surface_ext (_: InstanceRaw, _: &RawDirectFBSurfaceCreateInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_display_mode_khr (_: PhysicalDeviceRaw, _: DisplayKHRMutRaw, _: &RawDisplayModeCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<DisplayModeKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_display_plane_surface_khr (_: InstanceRaw, _: &RawDisplaySurfaceCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_event (_: DeviceRaw, _: &RawEventCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<EventRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_fence (_: DeviceRaw, _: &RawFenceCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<FenceRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_framebuffer (_: DeviceRaw, _: &RawFramebufferCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<FramebufferRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_graphics_pipelines (_: DeviceRaw, _: PipelineCacheRaw, _: u32, _: &RawGraphicsPipelineCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<PipelineRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_headless_surface_ext (_: InstanceRaw, _: &RawHeadlessSurfaceCreateInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_ios_surface_mvk (_: InstanceRaw, _: &RawIOSSurfaceCreateInfoMVK, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_image (_: DeviceRaw, _: &RawImageCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<ImageRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_image_pipe_surface_fuchsia (_: InstanceRaw, _: &RawImagePipeSurfaceCreateInfoFUCHSIA, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_image_view (_: DeviceRaw, _: &RawImageViewCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<ImageViewRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_indirect_commands_layout_nv (_: DeviceRaw, _: &RawIndirectCommandsLayoutCreateInfoNV, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<IndirectCommandsLayoutNVRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_instance (_: &RawInstanceCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<InstanceRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_mac_os_surface_mvk (_: InstanceRaw, _: &RawMacOSSurfaceCreateInfoMVK, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_metal_surface_ext (_: InstanceRaw, _: &RawMetalSurfaceCreateInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_micromap_ext (_: DeviceRaw, _: &RawMicromapCreateInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<MicromapEXTRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_optical_flow_session_nv (_: DeviceRaw, _: &RawOpticalFlowSessionCreateInfoNV, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<OpticalFlowSessionNVRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_pipeline_cache (_: DeviceRaw, _: &RawPipelineCacheCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<PipelineCacheRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_pipeline_layout (_: DeviceRaw, _: &RawPipelineLayoutCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<PipelineLayoutRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_private_data_slot (_: DeviceRaw, _: &RawPrivateDataSlotCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<PrivateDataSlotRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_query_pool (_: DeviceRaw, _: &RawQueryPoolCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<QueryPoolRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_ray_tracing_pipelines_khr (_: DeviceRaw, _: DeferredOperationKHRRaw, _: PipelineCacheRaw, _: u32, _: &RawRayTracingPipelineCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<PipelineRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_ray_tracing_pipelines_nv (_: DeviceRaw, _: PipelineCacheRaw, _: u32, _: &RawRayTracingPipelineCreateInfoNV, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<PipelineRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_render_pass (_: DeviceRaw, _: &RawRenderPassCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<RenderPassRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_render_pass2 (_: DeviceRaw, _: &RawRenderPassCreateInfo2, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<RenderPassRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_sampler (_: DeviceRaw, _: &RawSamplerCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SamplerRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_sampler_ycbcr_conversion (_: DeviceRaw, _: &RawSamplerYcbcrConversionCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SamplerYcbcrConversionRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_screen_surface_qnx (_: InstanceRaw, _: &RawScreenSurfaceCreateInfoQNX, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_semaphore (_: DeviceRaw, _: &RawSemaphoreCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SemaphoreRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_shader_module (_: DeviceRaw, _: &RawShaderModuleCreateInfo, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<ShaderModuleRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_shared_swapchains_khr (_: DeviceRaw, _: u32, _: core::ptr::NonNull<RawSwapchainCreateInfoKHR>, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SwapchainKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_stream_descriptor_surface_ggp (_: InstanceRaw, _: &RawStreamDescriptorSurfaceCreateInfoGGP, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_swapchain_khr (_: DeviceRaw, _: core::ptr::NonNull<RawSwapchainCreateInfoKHR>, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SwapchainKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_validation_cache_ext (_: DeviceRaw, _: &RawValidationCacheCreateInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<ValidationCacheEXTRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_vi_surface_nn (_: InstanceRaw, _: &RawViSurfaceCreateInfoNN, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_video_session_khr (_: DeviceRaw, _: &RawVideoSessionCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<VideoSessionKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_video_session_parameters_khr (_: DeviceRaw, _: &RawVideoSessionParametersCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<VideoSessionParametersKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_wayland_surface_khr (_: InstanceRaw, _: &RawWaylandSurfaceCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_win32_surface_khr (_: InstanceRaw, _: &RawWin32SurfaceCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_xcb_surface_khr (_: InstanceRaw, _: &RawXcbSurfaceCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn create_xlib_surface_khr (_: InstanceRaw, _: &RawXlibSurfaceCreateInfoKHR, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<SurfaceKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn debug_marker_set_object_name_ext (_: DeviceRaw, _: core::ptr::NonNull<RawDebugMarkerObjectNameInfoEXT>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn debug_marker_set_object_tag_ext (_: DeviceRaw, _: core::ptr::NonNull<RawDebugMarkerObjectTagInfoEXT>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn debug_report_message_ext (_: InstanceRaw, _: DebugReportFlagsEXT, _: RawDebugReportObjectTypeEXT, _: u64, _: usize, _: i32, _: UnsizedCStr, _: UnsizedCStr) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn deferred_operation_join_khr (_: DeviceRaw, _: DeferredOperationKHRRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_acceleration_structure_khr (_: DeviceRaw, _: AccelerationStructureKHRMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_acceleration_structure_nv (_: DeviceRaw, _: AccelerationStructureNVMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_buffer (_: DeviceRaw, _: BufferMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_buffer_collection_fuchsia (_: DeviceRaw, _: BufferCollectionFUCHSIARaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_buffer_view (_: DeviceRaw, _: BufferViewMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_command_pool (_: DeviceRaw, _: CommandPoolMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_cu_function_nvx (_: DeviceRaw, _: CuFunctionNVXRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_cu_module_nvx (_: DeviceRaw, _: CuModuleNVXRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_debug_report_callback_ext (_: InstanceRaw, _: DebugReportCallbackEXTMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_debug_utils_messenger_ext (_: InstanceRaw, _: DebugUtilsMessengerEXTMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_deferred_operation_khr (_: DeviceRaw, _: DeferredOperationKHRMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_descriptor_pool (_: DeviceRaw, _: DescriptorPoolMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_descriptor_set_layout (_: DeviceRaw, _: DescriptorSetLayoutMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_descriptor_update_template (_: DeviceRaw, _: DescriptorUpdateTemplateMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_device (_: DeviceMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_event (_: DeviceRaw, _: EventMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_fence (_: DeviceRaw, _: FenceMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_framebuffer (_: DeviceRaw, _: FramebufferMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_image (_: DeviceRaw, _: ImageMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_image_view (_: DeviceRaw, _: ImageViewMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_indirect_commands_layout_nv (_: DeviceRaw, _: IndirectCommandsLayoutNVMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_instance (_: InstanceMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_micromap_ext (_: DeviceRaw, _: MicromapEXTMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_optical_flow_session_nv (_: DeviceRaw, _: OpticalFlowSessionNVRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_pipeline (_: DeviceRaw, _: PipelineMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_pipeline_cache (_: DeviceRaw, _: PipelineCacheMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_pipeline_layout (_: DeviceRaw, _: PipelineLayoutMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_private_data_slot (_: DeviceRaw, _: PrivateDataSlotMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_query_pool (_: DeviceRaw, _: QueryPoolMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_render_pass (_: DeviceRaw, _: RenderPassMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_sampler (_: DeviceRaw, _: SamplerMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_sampler_ycbcr_conversion (_: DeviceRaw, _: SamplerYcbcrConversionMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_semaphore (_: DeviceRaw, _: SemaphoreMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_shader_module (_: DeviceRaw, _: ShaderModuleMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_surface_khr (_: InstanceRaw, _: SurfaceKHRMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_swapchain_khr (_: DeviceRaw, _: SwapchainKHRMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_validation_cache_ext (_: DeviceRaw, _: ValidationCacheEXTMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_video_session_khr (_: DeviceRaw, _: VideoSessionKHRMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn destroy_video_session_parameters_khr (_: DeviceRaw, _: VideoSessionParametersKHRMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn device_wait_idle (_: DeviceRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn display_power_control_ext (_: DeviceRaw, _: DisplayKHRRaw, _: &RawDisplayPowerInfoEXT) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn end_command_buffer (_: CommandBufferMutRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn enumerate_device_extension_properties (_: PhysicalDeviceRaw, _: Option<UnsizedCStr>, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawExtensionProperties>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn enumerate_device_layer_properties (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawLayerProperties>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn enumerate_instance_extension_properties (_: Option<UnsizedCStr>, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawExtensionProperties>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn enumerate_instance_layer_properties (_: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawLayerProperties>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn enumerate_instance_version (_: core::ptr::NonNull<Version>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn enumerate_physical_device_groups (_: InstanceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPhysicalDeviceGroupProperties>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn enumerate_physical_device_queue_family_performance_query_counters_khr (_: PhysicalDeviceRaw, _: u32, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPerformanceCounterKHR>>, _: Option<core::ptr::NonNull<RawPerformanceCounterDescriptionKHR>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn enumerate_physical_devices (_: InstanceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<PhysicalDeviceRaw>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn export_metal_objects_ext (_: DeviceRaw, _: core::ptr::NonNull<RawExportMetalObjectsInfoEXT>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn flush_mapped_memory_ranges (_: DeviceRaw, _: u32, _: &RawMappedMemoryRange) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn free_command_buffers (_: DeviceRaw, _: CommandPoolMutRaw, _: u32, _: Option<core::ptr::NonNull<CommandBufferMutRaw>>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn free_descriptor_sets (_: DeviceRaw, _: DescriptorPoolMutRaw, _: u32, _: Option<core::ptr::NonNull<DescriptorSetMutRaw>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn free_memory (_: DeviceRaw, _: DeviceMemoryMutRaw, _: Option<&RawAllocationCallbacks>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_acceleration_structure_build_sizes_khr (_: DeviceRaw, _: RawAccelerationStructureBuildTypeKHR, _: &RawAccelerationStructureBuildGeometryInfoKHR, _: Option<&u32>, _: core::ptr::NonNull<RawAccelerationStructureBuildSizesInfoKHR>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_acceleration_structure_device_address_khr (_: DeviceRaw, _: &RawAccelerationStructureDeviceAddressInfoKHR) -> DeviceAddress { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_acceleration_structure_handle_nv (_: DeviceRaw, _: AccelerationStructureNVRaw, _: usize, _: core::ptr::NonNull<u8>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_acceleration_structure_memory_requirements_nv (_: DeviceRaw, _: &RawAccelerationStructureMemoryRequirementsInfoNV, _: core::ptr::NonNull<RawMemoryRequirements2>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_acceleration_structure_opaque_capture_descriptor_data_ext (_: DeviceRaw, _: &RawAccelerationStructureCaptureDescriptorDataInfoEXT, _: core::ptr::NonNull<u8>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_android_hardware_buffer_properties_android (_: DeviceRaw, _: &AHardwareBuffer, _: core::ptr::NonNull<RawAndroidHardwareBufferPropertiesANDROID>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_buffer_collection_properties_fuchsia (_: DeviceRaw, _: BufferCollectionFUCHSIARaw, _: core::ptr::NonNull<RawBufferCollectionPropertiesFUCHSIA>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_buffer_device_address (_: DeviceRaw, _: &RawBufferDeviceAddressInfo) -> DeviceAddress { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_buffer_memory_requirements (_: DeviceRaw, _: BufferRaw, _: core::ptr::NonNull<RawMemoryRequirements>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_buffer_memory_requirements2 (_: DeviceRaw, _: &RawBufferMemoryRequirementsInfo2, _: core::ptr::NonNull<RawMemoryRequirements2>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_buffer_opaque_capture_address (_: DeviceRaw, _: &RawBufferDeviceAddressInfo) -> u64 { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_buffer_opaque_capture_descriptor_data_ext (_: DeviceRaw, _: &RawBufferCaptureDescriptorDataInfoEXT, _: core::ptr::NonNull<u8>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_calibrated_timestamps_ext (_: DeviceRaw, _: u32, _: &RawCalibratedTimestampInfoEXT, _: core::ptr::NonNull<u64>, _: core::ptr::NonNull<u64>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_deferred_operation_max_concurrency_khr (_: DeviceRaw, _: DeferredOperationKHRRaw) -> u32 { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_deferred_operation_result_khr (_: DeviceRaw, _: DeferredOperationKHRRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_descriptor_ext (_: DeviceRaw, _: &RawDescriptorGetInfoEXT, _: usize, _: core::ptr::NonNull<u8>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_descriptor_set_host_mapping_valve (_: DeviceRaw, _: DescriptorSetRaw, _: core::ptr::NonNull<*mut u8>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_descriptor_set_layout_binding_offset_ext (_: DeviceRaw, _: DescriptorSetLayoutRaw, _: u32, _: core::ptr::NonNull<DeviceSize>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_descriptor_set_layout_host_mapping_info_valve (_: DeviceRaw, _: &RawDescriptorSetBindingReferenceVALVE, _: core::ptr::NonNull<RawDescriptorSetLayoutHostMappingInfoVALVE>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_descriptor_set_layout_size_ext (_: DeviceRaw, _: DescriptorSetLayoutRaw, _: core::ptr::NonNull<DeviceSize>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_descriptor_set_layout_support (_: DeviceRaw, _: &RawDescriptorSetLayoutCreateInfo, _: core::ptr::NonNull<RawDescriptorSetLayoutSupport>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_device_acceleration_structure_compatibility_khr (_: DeviceRaw, _: &RawAccelerationStructureVersionInfoKHR, _: core::ptr::NonNull<RawAccelerationStructureCompatibilityKHR>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_device_buffer_memory_requirements (_: DeviceRaw, _: &RawDeviceBufferMemoryRequirements, _: core::ptr::NonNull<RawMemoryRequirements2>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_device_fault_info_ext (_: DeviceRaw, _: core::ptr::NonNull<RawDeviceFaultCountsEXT>, _: Option<core::ptr::NonNull<RawDeviceFaultInfoEXT>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_device_group_peer_memory_features (_: DeviceRaw, _: u32, _: u32, _: u32, _: core::ptr::NonNull<PeerMemoryFeatureFlags>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_device_group_present_capabilities_khr (_: DeviceRaw, _: core::ptr::NonNull<RawDeviceGroupPresentCapabilitiesKHR>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_device_group_surface_present_modes2_ext (_: DeviceRaw, _: &RawPhysicalDeviceSurfaceInfo2KHR, _: core::ptr::NonNull<DeviceGroupPresentModeFlagsKHR>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_device_group_surface_present_modes_khr (_: DeviceRaw, _: SurfaceKHRMutRaw, _: core::ptr::NonNull<DeviceGroupPresentModeFlagsKHR>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_device_image_memory_requirements (_: DeviceRaw, _: &RawDeviceImageMemoryRequirements, _: core::ptr::NonNull<RawMemoryRequirements2>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_device_image_sparse_memory_requirements (_: DeviceRaw, _: &RawDeviceImageMemoryRequirements, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawSparseImageMemoryRequirements2>>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_device_memory_commitment (_: DeviceRaw, _: DeviceMemoryRaw, _: core::ptr::NonNull<DeviceSize>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_device_memory_opaque_capture_address (_: DeviceRaw, _: &RawDeviceMemoryOpaqueCaptureAddressInfo) -> u64 { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_device_micromap_compatibility_ext (_: DeviceRaw, _: &RawMicromapVersionInfoEXT, _: core::ptr::NonNull<RawAccelerationStructureCompatibilityKHR>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_device_proc_addr (_: DeviceRaw, _: UnsizedCStr) -> VoidFunction { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_device_queue (_: DeviceRaw, _: u32, _: u32, _: core::ptr::NonNull<QueueRaw>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_device_queue2 (_: DeviceRaw, _: &RawDeviceQueueInfo2, _: core::ptr::NonNull<QueueRaw>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_device_subpass_shading_max_workgroup_size_huawei (_: DeviceRaw, _: RenderPassRaw, _: core::ptr::NonNull<RawExtent2D>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_display_mode_properties2_khr (_: PhysicalDeviceRaw, _: DisplayKHRRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawDisplayModeProperties2KHR>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_display_mode_properties_khr (_: PhysicalDeviceRaw, _: DisplayKHRRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawDisplayModePropertiesKHR>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_display_plane_capabilities2_khr (_: PhysicalDeviceRaw, _: core::ptr::NonNull<RawDisplayPlaneInfo2KHR>, _: core::ptr::NonNull<RawDisplayPlaneCapabilities2KHR>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_display_plane_capabilities_khr (_: PhysicalDeviceRaw, _: DisplayModeKHRMutRaw, _: u32, _: core::ptr::NonNull<RawDisplayPlaneCapabilitiesKHR>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_display_plane_supported_displays_khr (_: PhysicalDeviceRaw, _: u32, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<DisplayKHRRaw>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_drm_display_ext (_: PhysicalDeviceRaw, _: i32, _: u32, _: core::ptr::NonNull<DisplayKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_dynamic_rendering_tile_properties_qcom (_: DeviceRaw, _: &RawRenderingInfo, _: core::ptr::NonNull<RawTilePropertiesQCOM>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_event_status (_: DeviceRaw, _: EventRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_fence_fd_khr (_: DeviceRaw, _: &RawFenceGetFdInfoKHR, _: core::ptr::NonNull<::std::os::raw::c_int>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_fence_status (_: DeviceRaw, _: FenceRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_fence_win32_handle_khr (_: DeviceRaw, _: &RawFenceGetWin32HandleInfoKHR, _: core::ptr::NonNull<HANDLE>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_framebuffer_tile_properties_qcom (_: DeviceRaw, _: FramebufferRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawTilePropertiesQCOM>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_generated_commands_memory_requirements_nv (_: DeviceRaw, _: &RawGeneratedCommandsMemoryRequirementsInfoNV, _: core::ptr::NonNull<RawMemoryRequirements2>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_image_drm_format_modifier_properties_ext (_: DeviceRaw, _: ImageRaw, _: core::ptr::NonNull<RawImageDrmFormatModifierPropertiesEXT>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_image_memory_requirements (_: DeviceRaw, _: ImageRaw, _: core::ptr::NonNull<RawMemoryRequirements>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_image_memory_requirements2 (_: DeviceRaw, _: &RawImageMemoryRequirementsInfo2, _: core::ptr::NonNull<RawMemoryRequirements2>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_image_opaque_capture_descriptor_data_ext (_: DeviceRaw, _: &RawImageCaptureDescriptorDataInfoEXT, _: core::ptr::NonNull<u8>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_image_sparse_memory_requirements (_: DeviceRaw, _: ImageRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawSparseImageMemoryRequirements>>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_image_sparse_memory_requirements2 (_: DeviceRaw, _: &RawImageSparseMemoryRequirementsInfo2, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawSparseImageMemoryRequirements2>>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_image_subresource_layout (_: DeviceRaw, _: ImageRaw, _: &RawImageSubresource, _: core::ptr::NonNull<RawSubresourceLayout>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_image_subresource_layout2_ext (_: DeviceRaw, _: ImageRaw, _: &RawImageSubresource2EXT, _: core::ptr::NonNull<RawSubresourceLayout2EXT>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_image_view_address_nvx (_: DeviceRaw, _: ImageViewRaw, _: core::ptr::NonNull<RawImageViewAddressPropertiesNVX>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_image_view_handle_nvx (_: DeviceRaw, _: &RawImageViewHandleInfoNVX) -> u32 { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_image_view_opaque_capture_descriptor_data_ext (_: DeviceRaw, _: &RawImageViewCaptureDescriptorDataInfoEXT, _: core::ptr::NonNull<u8>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_instance_proc_addr (_: InstanceRaw, _: UnsizedCStr) -> VoidFunction { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_memory_android_hardware_buffer_android (_: DeviceRaw, _: &RawMemoryGetAndroidHardwareBufferInfoANDROID, _: core::ptr::NonNull<*mut AHardwareBuffer>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_memory_fd_khr (_: DeviceRaw, _: &RawMemoryGetFdInfoKHR, _: core::ptr::NonNull<::std::os::raw::c_int>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_memory_fd_properties_khr (_: DeviceRaw, _: RawExternalMemoryHandleTypeFlagBits, _: ::std::os::raw::c_int, _: core::ptr::NonNull<RawMemoryFdPropertiesKHR>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_memory_host_pointer_properties_ext (_: DeviceRaw, _: RawExternalMemoryHandleTypeFlagBits, _: &u8, _: core::ptr::NonNull<RawMemoryHostPointerPropertiesEXT>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_memory_remote_address_nv (_: DeviceRaw, _: &RawMemoryGetRemoteAddressInfoNV, _: core::ptr::NonNull<RemoteAddressNV>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_memory_win32_handle_khr (_: DeviceRaw, _: &RawMemoryGetWin32HandleInfoKHR, _: core::ptr::NonNull<HANDLE>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_memory_win32_handle_nv (_: DeviceRaw, _: DeviceMemoryRaw, _: ExternalMemoryHandleTypeFlagsNV, _: core::ptr::NonNull<HANDLE>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_memory_win32_handle_properties_khr (_: DeviceRaw, _: RawExternalMemoryHandleTypeFlagBits, _: HANDLE, _: core::ptr::NonNull<RawMemoryWin32HandlePropertiesKHR>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_memory_zircon_handle_fuchsia (_: DeviceRaw, _: &RawMemoryGetZirconHandleInfoFUCHSIA, _: core::ptr::NonNull<zx_handle_t>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_memory_zircon_handle_properties_fuchsia (_: DeviceRaw, _: RawExternalMemoryHandleTypeFlagBits, _: zx_handle_t, _: core::ptr::NonNull<RawMemoryZirconHandlePropertiesFUCHSIA>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_micromap_build_sizes_ext (_: DeviceRaw, _: RawAccelerationStructureBuildTypeKHR, _: &RawMicromapBuildInfoEXT, _: core::ptr::NonNull<RawMicromapBuildSizesInfoEXT>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_past_presentation_timing_google (_: DeviceRaw, _: SwapchainKHRMutRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPastPresentationTimingGOOGLE>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_performance_parameter_intel (_: DeviceRaw, _: RawPerformanceParameterTypeINTEL, _: core::ptr::NonNull<RawPerformanceValueINTEL>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_calibrateable_time_domains_ext (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawTimeDomainEXT>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_cooperative_matrix_properties_nv (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawCooperativeMatrixPropertiesNV>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_direct_fb_presentation_support_ext (_: PhysicalDeviceRaw, _: u32, _: core::ptr::NonNull<IDirectFB>) -> Bool32 { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_display_plane_properties2_khr (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawDisplayPlaneProperties2KHR>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_display_plane_properties_khr (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawDisplayPlanePropertiesKHR>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_display_properties2_khr (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawDisplayProperties2KHR>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_display_properties_khr (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawDisplayPropertiesKHR>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_external_buffer_properties (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceExternalBufferInfo, _: core::ptr::NonNull<RawExternalBufferProperties>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_external_fence_properties (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceExternalFenceInfo, _: core::ptr::NonNull<RawExternalFenceProperties>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_external_image_format_properties_nv (_: PhysicalDeviceRaw, _: RawFormat, _: RawImageType, _: RawImageTiling, _: ImageUsageFlags, _: ImageCreateFlags, _: ExternalMemoryHandleTypeFlagsNV, _: core::ptr::NonNull<RawExternalImageFormatPropertiesNV>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_external_semaphore_properties (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceExternalSemaphoreInfo, _: core::ptr::NonNull<RawExternalSemaphoreProperties>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_features (_: PhysicalDeviceRaw, _: core::ptr::NonNull<RawPhysicalDeviceFeatures>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_features2 (_: PhysicalDeviceRaw, _: core::ptr::NonNull<RawPhysicalDeviceFeatures2>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_format_properties (_: PhysicalDeviceRaw, _: RawFormat, _: core::ptr::NonNull<RawFormatProperties>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_format_properties2 (_: PhysicalDeviceRaw, _: RawFormat, _: core::ptr::NonNull<RawFormatProperties2>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_fragment_shading_rates_khr (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPhysicalDeviceFragmentShadingRateKHR>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_image_format_properties (_: PhysicalDeviceRaw, _: RawFormat, _: RawImageType, _: RawImageTiling, _: ImageUsageFlags, _: ImageCreateFlags, _: core::ptr::NonNull<RawImageFormatProperties>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_image_format_properties2 (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceImageFormatInfo2, _: core::ptr::NonNull<RawImageFormatProperties2>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_memory_properties (_: PhysicalDeviceRaw, _: core::ptr::NonNull<RawPhysicalDeviceMemoryProperties>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_memory_properties2 (_: PhysicalDeviceRaw, _: core::ptr::NonNull<RawPhysicalDeviceMemoryProperties2>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_multisample_properties_ext (_: PhysicalDeviceRaw, _: RawSampleCountFlagBits, _: core::ptr::NonNull<RawMultisamplePropertiesEXT>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_optical_flow_image_formats_nv (_: PhysicalDeviceRaw, _: &RawOpticalFlowImageFormatInfoNV, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawOpticalFlowImageFormatPropertiesNV>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_present_rectangles_khr (_: PhysicalDeviceRaw, _: SurfaceKHRMutRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawRect2D>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_properties (_: PhysicalDeviceRaw, _: core::ptr::NonNull<RawPhysicalDeviceProperties>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_properties2 (_: PhysicalDeviceRaw, _: core::ptr::NonNull<RawPhysicalDeviceProperties2>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_queue_family_performance_query_passes_khr (_: PhysicalDeviceRaw, _: &RawQueryPoolPerformanceCreateInfoKHR, _: core::ptr::NonNull<u32>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_queue_family_properties (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawQueueFamilyProperties>>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_queue_family_properties2 (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawQueueFamilyProperties2>>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_screen_presentation_support_qnx (_: PhysicalDeviceRaw, _: u32, _: core::ptr::NonNull<screen_window>) -> Bool32 { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_sparse_image_format_properties (_: PhysicalDeviceRaw, _: RawFormat, _: RawImageType, _: RawSampleCountFlagBits, _: ImageUsageFlags, _: RawImageTiling, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawSparseImageFormatProperties>>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_sparse_image_format_properties2 (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceSparseImageFormatInfo2, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawSparseImageFormatProperties2>>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_supported_framebuffer_mixed_samples_combinations_nv (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawFramebufferMixedSamplesCombinationNV>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_surface_capabilities2_ext (_: PhysicalDeviceRaw, _: SurfaceKHRRaw, _: core::ptr::NonNull<RawSurfaceCapabilities2EXT>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_surface_capabilities2_khr (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceSurfaceInfo2KHR, _: core::ptr::NonNull<RawSurfaceCapabilities2KHR>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_surface_capabilities_khr (_: PhysicalDeviceRaw, _: SurfaceKHRRaw, _: core::ptr::NonNull<RawSurfaceCapabilitiesKHR>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_surface_formats2_khr (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceSurfaceInfo2KHR, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawSurfaceFormat2KHR>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_surface_formats_khr (_: PhysicalDeviceRaw, _: SurfaceKHRRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawSurfaceFormatKHR>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_surface_present_modes2_ext (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceSurfaceInfo2KHR, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPresentModeKHR>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_surface_present_modes_khr (_: PhysicalDeviceRaw, _: SurfaceKHRRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPresentModeKHR>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_surface_support_khr (_: PhysicalDeviceRaw, _: u32, _: SurfaceKHRRaw, _: core::ptr::NonNull<Bool32>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_tool_properties (_: PhysicalDeviceRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPhysicalDeviceToolProperties>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_video_capabilities_khr (_: PhysicalDeviceRaw, _: &RawVideoProfileInfoKHR, _: core::ptr::NonNull<RawVideoCapabilitiesKHR>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_video_format_properties_khr (_: PhysicalDeviceRaw, _: &RawPhysicalDeviceVideoFormatInfoKHR, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawVideoFormatPropertiesKHR>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_wayland_presentation_support_khr (_: PhysicalDeviceRaw, _: u32, _: core::ptr::NonNull<WlDisplay>) -> Bool32 { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_win32_presentation_support_khr (_: PhysicalDeviceRaw, _: u32) -> Bool32 { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_xcb_presentation_support_khr (_: PhysicalDeviceRaw, _: u32, _: core::ptr::NonNull<xcb_connection_t>, _: xcb_visualid_t) -> Bool32 { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_physical_device_xlib_presentation_support_khr (_: PhysicalDeviceRaw, _: u32, _: core::ptr::NonNull<Display>, _: VisualID) -> Bool32 { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_pipeline_cache_data (_: DeviceRaw, _: PipelineCacheRaw, _: core::ptr::NonNull<usize>, _: Option<core::ptr::NonNull<u8>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_pipeline_executable_internal_representations_khr (_: DeviceRaw, _: &RawPipelineExecutableInfoKHR, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPipelineExecutableInternalRepresentationKHR>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_pipeline_executable_properties_khr (_: DeviceRaw, _: &RawPipelineInfoKHR, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPipelineExecutablePropertiesKHR>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_pipeline_executable_statistics_khr (_: DeviceRaw, _: &RawPipelineExecutableInfoKHR, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawPipelineExecutableStatisticKHR>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_pipeline_properties_ext (_: DeviceRaw, _: &RawPipelineInfoKHR, _: core::ptr::NonNull<RawPipelinePropertiesIdentifierEXT>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_private_data (_: DeviceRaw, _: RawObjectType, _: u64, _: PrivateDataSlotRaw, _: core::ptr::NonNull<u64>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_query_pool_results (_: DeviceRaw, _: QueryPoolRaw, _: u32, _: u32, _: usize, _: core::ptr::NonNull<u8>, _: DeviceSize, _: QueryResultFlags) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_queue_checkpoint_data2_nv (_: QueueRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawCheckpointData2NV>>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_queue_checkpoint_data_nv (_: QueueRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawCheckpointDataNV>>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_rand_r_output_display_ext (_: PhysicalDeviceRaw, _: core::ptr::NonNull<Display>, _: RROutput, _: core::ptr::NonNull<DisplayKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_ray_tracing_capture_replay_shader_group_handles_khr (_: DeviceRaw, _: PipelineRaw, _: u32, _: u32, _: usize, _: core::ptr::NonNull<u8>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_ray_tracing_shader_group_handles_khr (_: DeviceRaw, _: PipelineRaw, _: u32, _: u32, _: usize, _: core::ptr::NonNull<u8>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_ray_tracing_shader_group_stack_size_khr (_: DeviceRaw, _: PipelineRaw, _: u32, _: RawShaderGroupShaderKHR) -> DeviceSize { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_refresh_cycle_duration_google (_: DeviceRaw, _: SwapchainKHRMutRaw, _: core::ptr::NonNull<RawRefreshCycleDurationGOOGLE>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_render_area_granularity (_: DeviceRaw, _: RenderPassRaw, _: core::ptr::NonNull<RawExtent2D>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_sampler_opaque_capture_descriptor_data_ext (_: DeviceRaw, _: &RawSamplerCaptureDescriptorDataInfoEXT, _: core::ptr::NonNull<u8>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_semaphore_counter_value (_: DeviceRaw, _: SemaphoreRaw, _: core::ptr::NonNull<u64>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_semaphore_fd_khr (_: DeviceRaw, _: &RawSemaphoreGetFdInfoKHR, _: core::ptr::NonNull<::std::os::raw::c_int>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_semaphore_win32_handle_khr (_: DeviceRaw, _: &RawSemaphoreGetWin32HandleInfoKHR, _: core::ptr::NonNull<HANDLE>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_semaphore_zircon_handle_fuchsia (_: DeviceRaw, _: &RawSemaphoreGetZirconHandleInfoFUCHSIA, _: core::ptr::NonNull<zx_handle_t>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_shader_info_amd (_: DeviceRaw, _: PipelineRaw, _: RawShaderStageFlagBits, _: RawShaderInfoTypeAMD, _: core::ptr::NonNull<usize>, _: Option<core::ptr::NonNull<u8>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_shader_module_create_info_identifier_ext (_: DeviceRaw, _: &RawShaderModuleCreateInfo, _: core::ptr::NonNull<RawShaderModuleIdentifierEXT>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_shader_module_identifier_ext (_: DeviceRaw, _: ShaderModuleRaw, _: core::ptr::NonNull<RawShaderModuleIdentifierEXT>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_swapchain_counter_ext (_: DeviceRaw, _: SwapchainKHRRaw, _: RawSurfaceCounterFlagBitsEXT, _: core::ptr::NonNull<u64>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_swapchain_images_khr (_: DeviceRaw, _: SwapchainKHRRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<ImageRaw>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_swapchain_status_khr (_: DeviceRaw, _: SwapchainKHRMutRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_validation_cache_data_ext (_: DeviceRaw, _: ValidationCacheEXTRaw, _: core::ptr::NonNull<usize>, _: Option<core::ptr::NonNull<u8>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_video_session_memory_requirements_khr (_: DeviceRaw, _: VideoSessionKHRRaw, _: core::ptr::NonNull<u32>, _: Option<core::ptr::NonNull<RawVideoSessionMemoryRequirementsKHR>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn get_winrt_display_nv (_: PhysicalDeviceRaw, _: u32, _: core::ptr::NonNull<DisplayKHRRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn import_fence_fd_khr (_: DeviceRaw, _: core::ptr::NonNull<RawImportFenceFdInfoKHR>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn import_fence_win32_handle_khr (_: DeviceRaw, _: core::ptr::NonNull<RawImportFenceWin32HandleInfoKHR>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn import_semaphore_fd_khr (_: DeviceRaw, _: core::ptr::NonNull<RawImportSemaphoreFdInfoKHR>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn import_semaphore_win32_handle_khr (_: DeviceRaw, _: core::ptr::NonNull<RawImportSemaphoreWin32HandleInfoKHR>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn import_semaphore_zircon_handle_fuchsia (_: DeviceRaw, _: core::ptr::NonNull<RawImportSemaphoreZirconHandleInfoFUCHSIA>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn initialize_performance_api_intel (_: DeviceRaw, _: &RawInitializePerformanceApiInfoINTEL) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn invalidate_mapped_memory_ranges (_: DeviceRaw, _: u32, _: &RawMappedMemoryRange) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn map_memory (_: DeviceRaw, _: DeviceMemoryMutRaw, _: DeviceSize, _: DeviceSize, _: MemoryMapFlags, _: core::ptr::NonNull<Option<core::ptr::NonNull<u8>>>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn merge_pipeline_caches (_: DeviceRaw, _: PipelineCacheMutRaw, _: u32, _: &PipelineCacheRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn merge_validation_caches_ext (_: DeviceRaw, _: ValidationCacheEXTMutRaw, _: u32, _: &ValidationCacheEXTRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn queue_begin_debug_utils_label_ext (_: QueueRaw, _: &RawDebugUtilsLabelEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn queue_bind_sparse (_: QueueMutRaw, _: u32, _: Option<&RawBindSparseInfo>, _: FenceMutRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn queue_end_debug_utils_label_ext (_: QueueRaw) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn queue_insert_debug_utils_label_ext (_: QueueRaw, _: &RawDebugUtilsLabelEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn queue_present_khr (_: QueueMutRaw, _: core::ptr::NonNull<RawPresentInfoKHR>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn queue_set_performance_configuration_intel (_: QueueRaw, _: PerformanceConfigurationINTELRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn queue_submit (_: QueueMutRaw, _: u32, _: Option<&RawSubmitInfo>, _: FenceMutRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn queue_submit2 (_: QueueMutRaw, _: u32, _: Option<&RawSubmitInfo2>, _: FenceMutRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn queue_wait_idle (_: QueueMutRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn register_device_event_ext (_: DeviceRaw, _: &RawDeviceEventInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<FenceRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn register_display_event_ext (_: DeviceRaw, _: DisplayKHRRaw, _: &RawDisplayEventInfoEXT, _: Option<&RawAllocationCallbacks>, _: core::ptr::NonNull<FenceRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn release_display_ext (_: PhysicalDeviceRaw, _: DisplayKHRRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn release_full_screen_exclusive_mode_ext (_: DeviceRaw, _: SwapchainKHRRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn release_performance_configuration_intel (_: DeviceRaw, _: PerformanceConfigurationINTELMutRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn release_profiling_lock_khr (_: DeviceRaw) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn release_swapchain_images_ext (_: DeviceRaw, _: core::ptr::NonNull<RawReleaseSwapchainImagesInfoEXT>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn reset_command_buffer (_: CommandBufferMutRaw, _: CommandBufferResetFlags) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn reset_command_pool (_: DeviceRaw, _: CommandPoolMutRaw, _: CommandPoolResetFlags) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn reset_descriptor_pool (_: DeviceRaw, _: DescriptorPoolMutRaw, _: DescriptorPoolResetFlags) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn reset_event (_: DeviceRaw, _: EventMutRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn reset_fences (_: DeviceRaw, _: u32, _: core::ptr::NonNull<FenceMutRaw>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn reset_query_pool (_: DeviceRaw, _: QueryPoolRaw, _: u32, _: u32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn set_buffer_collection_buffer_constraints_fuchsia (_: DeviceRaw, _: BufferCollectionFUCHSIARaw, _: &RawBufferConstraintsInfoFUCHSIA) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn set_buffer_collection_image_constraints_fuchsia (_: DeviceRaw, _: BufferCollectionFUCHSIARaw, _: &RawImageConstraintsInfoFUCHSIA) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn set_debug_utils_object_name_ext (_: DeviceRaw, _: core::ptr::NonNull<RawDebugUtilsObjectNameInfoEXT>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn set_debug_utils_object_tag_ext (_: DeviceRaw, _: core::ptr::NonNull<RawDebugUtilsObjectTagInfoEXT>) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn set_device_memory_priority_ext (_: DeviceRaw, _: DeviceMemoryRaw, _: f32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn set_event (_: DeviceRaw, _: EventMutRaw) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn set_hdr_metadata_ext (_: DeviceRaw, _: u32, _: &SwapchainKHRRaw, _: &RawHdrMetadataEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn set_local_dimming_amd (_: DeviceRaw, _: SwapchainKHRRaw, _: Bool32) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn set_private_data (_: DeviceRaw, _: RawObjectType, _: u64, _: PrivateDataSlotRaw, _: u64) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn signal_semaphore (_: DeviceRaw, _: &RawSemaphoreSignalInfo) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn submit_debug_utils_message_ext (_: InstanceRaw, _: RawDebugUtilsMessageSeverityFlagBitsEXT, _: DebugUtilsMessageTypeFlagsEXT, _: &RawDebugUtilsMessengerCallbackDataEXT) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn trim_command_pool (_: DeviceRaw, _: CommandPoolMutRaw, _: CommandPoolTrimFlags) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn uninitialize_performance_api_intel (_: DeviceRaw) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn unmap_memory (_: DeviceRaw, _: DeviceMemoryMutRaw) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn update_descriptor_set_with_template (_: DeviceRaw, _: DescriptorSetRaw, _: DescriptorUpdateTemplateRaw, _: Option<&u8>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn update_descriptor_sets (_: DeviceRaw, _: u32, _: Option<&RawWriteDescriptorSet>, _: u32, _: Option<&RawCopyDescriptorSet>) { panic!("Associated handle has not been initialised") }
    pub extern "C" fn update_video_session_parameters_khr (_: DeviceRaw, _: VideoSessionParametersKHRRaw, _: &RawVideoSessionParametersUpdateInfoKHR) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn wait_for_fences (_: DeviceRaw, _: u32, _: &FenceRaw, _: Bool32, _: u64) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn wait_for_present_khr (_: DeviceRaw, _: SwapchainKHRMutRaw, _: u64, _: u64) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn wait_semaphores (_: DeviceRaw, _: &RawSemaphoreWaitInfo, _: u64) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn write_acceleration_structures_properties_khr (_: DeviceRaw, _: u32, _: &AccelerationStructureKHRRaw, _: RawQueryType, _: usize, _: core::ptr::NonNull<u8>, _: usize) -> RawResult { panic!("Associated handle has not been initialised") }
    pub extern "C" fn write_micromaps_properties_ext (_: DeviceRaw, _: u32, _: &MicromapEXTRaw, _: RawQueryType, _: usize, _: core::ptr::NonNull<u8>, _: usize) -> RawResult { panic!("Associated handle has not been initialised") }
}

pub(crate) mod no_op_destructors {
    use super::*;
    pub extern "C" fn destroy_acceleration_structure_khr (_: DeviceRaw, _: AccelerationStructureKHRMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_acceleration_structure_nv (_: DeviceRaw, _: AccelerationStructureNVMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_buffer (_: DeviceRaw, _: BufferMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_buffer_collection_fuchsia (_: DeviceRaw, _: BufferCollectionFUCHSIARaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_buffer_view (_: DeviceRaw, _: BufferViewMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_command_pool (_: DeviceRaw, _: CommandPoolMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_cu_function_nvx (_: DeviceRaw, _: CuFunctionNVXRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_cu_module_nvx (_: DeviceRaw, _: CuModuleNVXRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_debug_report_callback_ext (_: InstanceRaw, _: DebugReportCallbackEXTMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_debug_utils_messenger_ext (_: InstanceRaw, _: DebugUtilsMessengerEXTMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_deferred_operation_khr (_: DeviceRaw, _: DeferredOperationKHRMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_descriptor_pool (_: DeviceRaw, _: DescriptorPoolMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_descriptor_set_layout (_: DeviceRaw, _: DescriptorSetLayoutMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_descriptor_update_template (_: DeviceRaw, _: DescriptorUpdateTemplateMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_device (_: DeviceMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_event (_: DeviceRaw, _: EventMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_fence (_: DeviceRaw, _: FenceMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_framebuffer (_: DeviceRaw, _: FramebufferMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_image (_: DeviceRaw, _: ImageMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_image_view (_: DeviceRaw, _: ImageViewMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_indirect_commands_layout_nv (_: DeviceRaw, _: IndirectCommandsLayoutNVMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_instance (_: InstanceMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_micromap_ext (_: DeviceRaw, _: MicromapEXTMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_optical_flow_session_nv (_: DeviceRaw, _: OpticalFlowSessionNVRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_pipeline (_: DeviceRaw, _: PipelineMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_pipeline_cache (_: DeviceRaw, _: PipelineCacheMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_pipeline_layout (_: DeviceRaw, _: PipelineLayoutMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_private_data_slot (_: DeviceRaw, _: PrivateDataSlotMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_query_pool (_: DeviceRaw, _: QueryPoolMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_render_pass (_: DeviceRaw, _: RenderPassMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_sampler (_: DeviceRaw, _: SamplerMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_sampler_ycbcr_conversion (_: DeviceRaw, _: SamplerYcbcrConversionMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_semaphore (_: DeviceRaw, _: SemaphoreMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_shader_module (_: DeviceRaw, _: ShaderModuleMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_surface_khr (_: InstanceRaw, _: SurfaceKHRMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_swapchain_khr (_: DeviceRaw, _: SwapchainKHRMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_validation_cache_ext (_: DeviceRaw, _: ValidationCacheEXTMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_video_session_khr (_: DeviceRaw, _: VideoSessionKHRMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn destroy_video_session_parameters_khr (_: DeviceRaw, _: VideoSessionParametersKHRMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
    pub extern "C" fn free_command_buffers (_: DeviceRaw, _: CommandPoolMutRaw, _: u32, _: Option<core::ptr::NonNull<CommandBufferMutRaw>>) { /* No Op */ }
    pub extern "C" fn free_descriptor_sets (_: DeviceRaw, _: DescriptorPoolMutRaw, _: u32, _: Option<core::ptr::NonNull<DescriptorSetMutRaw>>) -> RawResult { /* No Op */ RawResult::eSuccess }
    pub extern "C" fn free_memory (_: DeviceRaw, _: DeviceMemoryMutRaw, _: Option<&RawAllocationCallbacks>) { /* No Op */ }
}

