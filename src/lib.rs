#[macro_use]
mod macros;
mod version;
mod bool32;
mod bit_field_iter;
mod const_size_ptr;
mod const_size_cstr;
mod unsized_cstr;
mod struct_utility;
mod handle_utility;
#[cfg(feature = "windowing_utility")]
mod windowing_utility;

pub use version::*;
pub use bool32::*;
pub use bit_field_iter::*;
pub use const_size_ptr::*;
pub use const_size_cstr::*;
pub use unsized_cstr::*;
pub use struct_utility::*;
pub use handle_utility::*;
#[cfg(feature = "windowing_utility")]
pub use windowing_utility::*;
use defer_alloc::DeferAllocArc;

pub use unsized_cstr_derive::unsized_cstr;

pub mod base_types;
pub mod extern_types;
pub mod defines;
pub mod constants;
pub mod function_types;
pub mod enums;
pub mod bit_fields;
pub mod structs_unions;
pub mod handles;
pub mod command_utility;
pub mod enum_utility;
pub mod vector_utility;

pub use base_types::*;
pub use extern_types::*;
pub use defines::*;
pub use constants::*;
pub use function_types::*;
pub use enums::*;
pub use bit_fields::*;
pub use structs_unions::*;
pub use handles::*;
pub use command_utility::*;
pub use enum_utility::*;
pub use vector_utility::*;
