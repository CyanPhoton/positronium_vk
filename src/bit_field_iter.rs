use std::iter::FusedIterator;

pub struct BitFieldIterator<E, M, D> where E: 'static + Into<D> + Copy, M: Into<D> + Copy, D: Eq + core::ops::BitAnd<D, Output=D> + core::ops::Not<Output=D> + Copy + Default + core::fmt::Binary {
    bit_field: M,
    remainder: D,
    //data: core::marker::PhantomData<D>,
    options: core::slice::Iter<'static, E>,
    default_enum: Option<E>,
    finished: bool
}

impl<E, M, D> BitFieldIterator<E, M, D> where E: 'static + Into<D> + Copy, M: Into<D> + Copy, D: Eq + core::ops::BitAnd<D, Output=D> + core::ops::Not<Output=D> + Copy + Default + core::fmt::Binary {
    pub fn new(bit_field: M, options: core::slice::Iter<'static, E>, default_enum: Option<E>) -> Self {
        BitFieldIterator { bit_field, remainder: bit_field.into(), /*data: core::default::Default::default(),*/ options, default_enum, finished: false }
    }
}

pub struct BitFieldIteratorUnknownBitsError<D> where D: core::fmt::Binary {
    remainder: D
}

impl<D> BitFieldIteratorUnknownBitsError<D> where D: core::fmt::Binary {
    fn new(remainder: D) -> Self {
        BitFieldIteratorUnknownBitsError { remainder }
    }

    pub fn remainder(&self) -> &D {
        &self.remainder
    }
}

impl<D> core::fmt::Debug for BitFieldIteratorUnknownBitsError<D> where D: core::fmt::Binary {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        writeln!(f, "Bit field had remaining bits, suggesting invalid mask, or newly added bits, remainder: {:#b}", self.remainder)
    }
}

impl<D> core::fmt::Display for BitFieldIteratorUnknownBitsError<D> where D: core::fmt::Binary {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        writeln!(f, "Bit field had remaining bits, suggesting invalid mask, or newly added bits, remainder: {:#b}", self.remainder)
    }
}

impl<D> std::error::Error for BitFieldIteratorUnknownBitsError<D> where D: core::fmt::Binary {}

impl<E, M, D> Iterator for BitFieldIterator<E, M, D> where E: 'static + Into<D> + Copy, M: Into<D> + Copy, D: Eq + core::ops::BitAnd<D, Output=D> + core::ops::Not<Output=D> + Copy + Default + core::fmt::Binary {
    type Item = core::result::Result<E, BitFieldIteratorUnknownBitsError<D>>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.finished {
            return None;
        }

        let b: D = self.bit_field.into();

        if let Some(d) = self.default_enum.take() {
            if b == D::default() {
                self.finished = true;
                return Some(Ok(d));
            }
        }

        while let Some(next) = self.options.next() {
            let o: D = (*next).into();

            if o == (o & b) {

                self.remainder = self.remainder & (!o);

                return Some(Ok(*next));
            }
        }

        if self.remainder == D::default() {
            self.finished = true;
            None
        } else {
            self.finished = true;
            Some(Err(BitFieldIteratorUnknownBitsError::new(self.remainder)))
        }

    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        if self.finished {
            (0, Some(0))
        } else {
            (0, Some(self.options.len() + 2)) // +1 for D::default(), +1 for Err(remainder)
        }
    }
}

impl<E, M, D> FusedIterator for BitFieldIterator<E, M, D> where E: 'static + Into<D> + Copy, M: Into<D> + Copy, D: Eq + core::ops::BitAnd<D, Output=D> + core::ops::Not<Output=D> + Copy + Default + core::fmt::Binary {}