use std::fmt::Debug;
use std::mem::MaybeUninit;
use std::num::NonZeroUsize;
use std::ptr::null_mut;

use crate::{RawStructureType};

pub(crate) use st::*;

mod st {
    use std::ptr::null;
    use super::*;

    /// For structs that have SType
    pub trait StructureTyped {
        #[doc(hidden)]
        fn as_base_in(&mut self) -> &mut VkBaseInStructure {
            unsafe { &mut *(self as *mut _ as *mut VkBaseInStructure) }
        }

        #[doc(hidden)]
        fn push_next(&mut self, next: &mut VkBaseInStructure) {
            next.as_base_in().p_next = std::mem::replace(&mut self.as_base_in().p_next, next as *mut VkBaseInStructure as *const _);
        }

        #[doc(hidden)]
        fn recursive_clear_next(&mut self) {
            let s = self.as_base_in();
            if !s.p_next.is_null() {
                let n = unsafe { &mut *(s.p_next as *mut VkBaseInStructure) };
                n.recursive_clear_next();
            }
            s.p_next = null();
        }

        #[doc(hidden)]
        fn recursive_clear_next_mark_init(&mut self, uninit_count: usize) {
            let s = self.as_base_in();
            if uninit_count > 0 {
                TrackedUninitTypedStruct::mark_initialised(s as *mut _ as *mut MaybeUninit<VkBaseInStructure>)
            }
            if !s.p_next.is_null() {
                let n = unsafe { &mut *(s.p_next as *mut VkBaseInStructure) };
                n.recursive_clear_next_mark_init(uninit_count.saturating_sub(1));
            }
            s.p_next = null();
        }
    }

    /// For structs that don't have a sType and/or pNext field
    pub trait PlainStructure {}

    pub trait GeneralMaybeUninit<S> {
        fn get_mut_ptr(&mut self) -> *mut S;
    }
}

/// For structs that have NextMutPtr
pub trait OutStructure: StructureTyped {
    fn is_tracked_uninitialized(&self) -> bool { false }
    fn mark_initialised(&mut self) {}
}

/// For structs that have NextPtr
pub trait InStructure: StructureTyped {}

#[repr(transparent)]
#[derive(Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct SType<S> {
    pub(crate) s_type: RawStructureType,
    pub(crate) _phantom: std::marker::PhantomData<S>,
}

// Need to manually impl Clone & Copy since otherwise it predicates on S: Copy + Clone, and this
// isn't true, but also isn't needed since S is PhantomData
impl<S> Clone for SType<S> {
    fn clone(&self) -> Self {
        SType {
            s_type: self.s_type,
            _phantom: self._phantom,
        }
    }
}

impl<S> Copy for SType<S> {}

impl<S> Debug for SType<S> where Self: Default {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Self::default().s_type.fmt(f)
    }
}

impl<S> SType<S> {
    pub fn get_s_type(self) -> RawStructureType {
        self.s_type
    }
}

#[repr(C)]
pub struct TrackedUninitTypedStruct<S> where S: StructureTyped {
    initialised: bool,
    // A constant amount of padding that will guarantee that no extra needs to be added
    // since nothing should have have an alignment of more than 8 bytes.
    // Needed so that we can convert a pointer to [s], to one to [initialised] without
    // needing to know what S it was.
    _padding: [u8; 7],
    pub(crate) s: MaybeUninit<S>,
}

impl<S: StructureTyped> TrackedUninitTypedStruct<S>  {
    const OFFSET: isize = -8;

    pub(crate) fn mark_initialised(maybe_uninit: *mut MaybeUninit<S>) {
        let s = maybe_uninit as *mut u8;
        let s = unsafe { s.offset(Self::OFFSET) } as *mut TrackedUninitTypedStruct<S>;
        unsafe { (*s).initialised = true; }
    }
}

impl<S: OutStructure> StructureTyped for TrackedUninitTypedStruct<S> {
    fn as_base_in(&mut self) -> &mut VkBaseInStructure {
        unsafe { &mut *(&mut self.s as *mut _ as *mut VkBaseInStructure) }
    }
}

impl<S: OutStructure> OutStructure for TrackedUninitTypedStruct<S> {
    fn is_tracked_uninitialized(&self) -> bool { true }

    fn mark_initialised(&mut self) {
        self.initialised = true;
    }
}

impl<S> Debug for TrackedUninitTypedStruct<S> where S: StructureTyped + Debug {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.initialised {
            unsafe { Some(&*self.s.as_ptr()) }
        } else {
            None
        }.fmt(f)
    }
}

pub struct TrackedUninitPlainStruct<S> where S: PlainStructure {
    pub(crate) s: MaybeUninit<S>,
    initialised: bool,
}

impl<S> Debug for TrackedUninitPlainStruct<S> where S: PlainStructure + Debug {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.initialised {
            unsafe { Some(&*self.s.as_ptr()) }
        } else {
            None
        }.fmt(f)
    }
}

#[allow(dead_code)]
#[repr(C)]
pub struct VkBaseInStructure {
    pub(crate) s_type: RawStructureType,
    pub(crate) p_next: *const std::os::raw::c_void,
}

impl StructureTyped for VkBaseInStructure  {}
impl InStructure for VkBaseInStructure  {}

#[allow(dead_code)]
#[repr(C)]
pub struct VkBaseOutStructure {
    pub(crate) s_type: RawStructureType,
    pub(crate) p_next: *mut std::os::raw::c_void,
}

impl StructureTyped for VkBaseOutStructure  {}
impl OutStructure for VkBaseOutStructure  {}

impl<S> TrackedUninitTypedStruct<S> where S: StructureTyped, SType<S>: Default {
    pub fn new() -> Self {
        let mut s = MaybeUninit::<S>::zeroed();

        // By the vulkan spec, if it has an s_type it is in the layout of VkBaseOutStructure, and can be accessed as such
        unsafe {
            (s.as_mut_ptr() as *mut VkBaseOutStructure).write(VkBaseOutStructure {
                s_type: SType::<S>::default().get_s_type(),
                p_next: null_mut(),
            });
        }

        TrackedUninitTypedStruct {
            s,
            initialised: false,
            _padding: [0; 7]
        }
    }

    pub fn is_initialised(&self) -> bool {
        self.initialised
    }

    pub fn into_value(self) -> Option<S> {
        if self.initialised {
            Some(unsafe { self.s.assume_init() })
        } else {
            None
        }
    }

    pub unsafe fn into_value_unchecked(self) -> S {
        self.s.assume_init()
    }
}

impl<S> TrackedUninitPlainStruct<S> where S: PlainStructure {
    pub fn new() -> Self {
        let s = MaybeUninit::<S>::zeroed();

        TrackedUninitPlainStruct {
            s,
            initialised: false,
        }
    }

    pub fn is_initialised(&self) -> bool {
        self.initialised
    }

    pub fn into_value(self) -> Option<S> {
        if self.initialised {
            Some(unsafe { self.s.assume_init() })
        } else {
            None
        }
    }

    pub unsafe fn into_value_unchecked(self) -> S {
        self.s.assume_init()
    }
}

impl<S: StructureTyped> StructureTyped for MaybeUninit<S> {
    fn as_base_in(&mut self) -> &mut VkBaseInStructure {
        unsafe { &mut *(self.as_mut_ptr() as *mut VkBaseInStructure) }
    }
}

impl<S: Sized> GeneralMaybeUninit<S> for MaybeUninit<S> {
    fn get_mut_ptr(&mut self) -> *mut S {
        self.as_mut_ptr()
    }
}

impl<S: Sized + StructureTyped> GeneralMaybeUninit<S> for TrackedUninitTypedStruct<S> {
    fn get_mut_ptr(&mut self) -> *mut S {
        self.s.as_mut_ptr()
    }
}

impl<S: Sized + PlainStructure> GeneralMaybeUninit<S> for TrackedUninitPlainStruct<S> {
    fn get_mut_ptr(&mut self) -> *mut S {
        self.s.as_mut_ptr()
    }
}

// #[derive(Copy, Clone)]
#[repr(transparent)]
pub struct NextPtr<'p_next> {
    pub ptr: core::ptr::NonNull<u8>,
    _phantom: core::marker::PhantomData<&'p_next [u8; 0]>,
}

// Sound since this is immutable
unsafe impl<'p_next> Send for NextPtr<'p_next> {}
unsafe impl<'p_next> Sync for NextPtr<'p_next> {}

impl<'p_next> Drop for NextPtr<'p_next> {
    fn drop(&mut self) {
        unsafe { &mut *(self.ptr.as_ptr() as *mut VkBaseOutStructure) }.recursive_clear_next();
    }
}

impl Debug for NextPtr<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("NextPtr").finish()
    }
}

#[repr(transparent)]
// #[derive(Copy, Clone)]
pub struct NextMutPtr<'p_next> {
    pub ptr: core::ptr::NonNull<u8>,
    _phantom: core::marker::PhantomData<&'p_next mut [u8; 0]>,
}

impl<'p_next> Drop for NextMutPtr<'p_next> {
    fn drop(&mut self) {
        unsafe { &mut *(self.ptr.as_ptr() as *mut VkBaseOutStructure) }.recursive_clear_next();
    }
}

impl Debug for NextMutPtr<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("NextMutPtr").finish()
    }
}

// pub struct NextCleaner<S> where S: StructureTyped {
//     s: MaybeUninit<S>
// }
//
// impl<S: StructureTyped> NextCleaner<S> {
//     pub(crate) fn new(s: S) -> Self {
//         Self { s: MaybeUninit::new(s) }
//     }
//
//     pub fn into_inner(mut self) -> S {
//         let mut s = unsafe { self.s.assume_init_mut() };
//         let mut s = std::mem::replace(s, MaybeUninit::zeroed());
//         s.recursive_clear_next();
//         s
//     }
// }
//
// impl<S: StructureTyped> Deref for NextCleaner<S> {
//     type Target = S;
//
//     fn deref(&self) -> &S {
//         unsafe { self.s.assume_init_ref() }
//     }
// }
//
// impl<S: StructureTyped> DerefMut for NextCleaner<S> {
//     fn deref_mut(&mut self) -> &mut S {
//         unsafe { self.s.assume_init_mut() }
//     }
// }
//
// impl<S: StructureTyped> Drop for NextCleaner<S> {
//     fn drop(&mut self) {
//         let mut s = unsafe { self.s.assume_init_mut() };
//         let mut s = std::mem::replace(s, MaybeUninit::zeroed());
//         s.recursive_clear_next();
//     }
// }

#[derive(Debug, Copy, Clone)]
pub enum OptionalSlice<'ptr, T> {
    None,
    Length(NonZeroUsize),
    Slice(&'ptr [T])
}

impl<'ptr, T> Default for OptionalSlice<'ptr, T> {
    fn default() -> Self {
        OptionalSlice::None
    }
}

impl<'ptr, T> From<&'ptr [T]> for OptionalSlice<'ptr, T> {
    fn from(slice: &'ptr [T]) -> Self {
        OptionalSlice::Slice(slice)
    }
}

impl<T> From<usize> for OptionalSlice<'static, T> {
    fn from(len: usize) -> Self {
        OptionalSlice::from_len(len)
    }
}

impl<'ptr, T> OptionalSlice<'ptr, T> {
    pub fn from_len(len: usize) -> Self {
        match NonZeroUsize::new(len) {
            None => OptionalSlice::None,
            Some(len) => OptionalSlice::Length(len)
        }
    }

    pub fn len(&self) -> usize {
        if let OptionalSlice::Slice(slice) = self {
            slice.len()
        } else {
            0
        }
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn as_slice(&self) -> &'ptr [T] {
        if let OptionalSlice::Slice(slice) = self {
            slice
        } else {
            &[][..]
        }
    }

    pub fn first(&self) -> Option<&'ptr T> {
        self.as_slice().first()
    }
}

#[derive(Debug)]
pub enum OptionalSliceMut<'ptr, T> {
    None,
    Length(NonZeroUsize),
    Slice(&'ptr mut [T])
}

impl<'ptr, T> Default for OptionalSliceMut<'ptr, T> {
    fn default() -> Self {
        OptionalSliceMut::None
    }
}

impl<'ptr, T> From<&'ptr mut [T]> for OptionalSliceMut<'ptr, T> {
    fn from(slice: &'ptr mut [T]) -> Self {
        OptionalSliceMut::Slice(slice)
    }
}

impl<T> From<usize> for OptionalSliceMut<'static, T> {
    fn from(len: usize) -> Self {
        OptionalSliceMut::from_len(len)
    }
}

impl<'ptr, T> OptionalSliceMut<'ptr, T> {
    pub fn from_len(len: usize) -> Self {
        match NonZeroUsize::new(len) {
            None => OptionalSliceMut::None,
            Some(len) => OptionalSliceMut::Length(len)
        }
    }

    pub fn len(&self) -> usize {
        if let OptionalSliceMut::Slice(slice) = self {
            slice.len()
        } else {
            0
        }
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn as_slice(&self) -> &'ptr [T] {
        if let OptionalSliceMut::Slice(slice) = self {
            unsafe { core::mem::transmute(&slice[..]) } // Hack lifetimes
        } else {
            &[][..]
        }
    }

    pub fn as_slice_mut(&mut self) -> &'ptr mut [T] {
        if let OptionalSliceMut::Slice(slice) = self {
            unsafe { core::mem::transmute(&mut slice[..]) } // Hack lifetimes
        } else {
            &mut [][..]
        }
    }

    pub fn first(&self) -> Option<&'ptr T> {
        self.as_slice().first()
    }

    pub fn first_mut(&mut self) -> Option<&'ptr mut T> {
        self.as_slice_mut().first_mut()
    }
}

// pub trait AsNext<S>: StructureTyped where S: StructureTyped {
//     fn as_next(&self) -> NextPtr<S> {
//         NextPtr {
//             ptr: core::ptr::NonNull::from(self).cast(),
//             _phantom: Default::default(),
//         }
//     }
// }
//
// pub trait AsNextMut<S>: StructureTyped where S: StructureTyped {
//     fn as_next_mut(&mut self) -> NextMutPtr<S> {
//         NextMutPtr {
//             ptr: core::ptr::NonNull::from(self).cast(),
//             _phantom: Default::default(),
//         }
//     }
// }
