use std::fmt::Debug;
use std::marker::PhantomData;
use std::ptr::NonNull;

#[repr(transparent)]
#[derive(Copy, Clone)]
pub struct ConstSizePtr<'ptr, T, const N: usize> {
    first: NonNull<T>,
    _phantom: PhantomData<&'ptr T>,
}

// Sound since this is immutable
unsafe impl<'ptr, T, const N: usize> Send for ConstSizePtr<'ptr, T, N> {}
unsafe impl<'ptr, T, const N: usize> Sync for ConstSizePtr<'ptr, T, N> {}

impl<'ptr, T, const N: usize> ConstSizePtr<'ptr, T, N> {
    pub fn new(slice: &[T]) -> ConstSizePtr<T, N> {
        assert!(N >= 1);
        assert!(slice.len() >= N);

        ConstSizePtr {
            first: NonNull::from(slice.first().unwrap()),
            _phantom: Default::default(),
        }
    }

    pub unsafe fn from_first(first: &T) -> ConstSizePtr<T, N> {
        ConstSizePtr {
            first: NonNull::from(first),
            _phantom: Default::default(),
        }
    }

    pub fn as_slice<'a>(&'a self) -> &'ptr [T] {
        unsafe { core::slice::from_raw_parts(self.first.as_ptr(), N) }
    }
}

impl<'s, T, const N: usize> Debug for ConstSizePtr<'s, T, N> where T: Debug {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.as_slice())
    }
}

#[repr(transparent)]
pub struct ConstSizeMutPtr<'ptr, T, const N: usize> {
    first: NonNull<T>,
    _phantom: PhantomData<&'ptr mut T>,
}

impl<'ptr, T, const N: usize> ConstSizeMutPtr<'ptr, T, N> {
    pub fn new(slice: &mut [T]) -> ConstSizeMutPtr<T, N> {
        assert!(slice.len() >= N);
        assert!(N >= 1);

        ConstSizeMutPtr {
            first: NonNull::from(slice.first_mut().unwrap()),
            _phantom: Default::default(),
        }
    }

    pub unsafe fn from_first(first: &mut T) -> ConstSizeMutPtr<T, N> {
        ConstSizeMutPtr {
            first: NonNull::from(first),
            _phantom: Default::default(),
        }
    }

    pub fn as_slice<'a>(&'a self) -> &'ptr [T] {
        unsafe { core::slice::from_raw_parts(self.first.as_ptr(), N) }
    }

    pub fn as_mut_slice<'a>(&'a mut self) -> &'ptr [T] {
        unsafe { core::slice::from_raw_parts_mut(self.first.as_mut(), N) }
    }
}

impl<'s, T, const N: usize> Debug for ConstSizeMutPtr<'s, T, N> where T: Debug {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.as_slice())
    }
}