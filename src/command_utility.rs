pub use super::*;
// Command Utility
pub struct RecordingCommandBuffer<'cmd> {
    command_buffer: &'cmd mut CommandBuffer,
}

impl CommandBuffer {
    pub fn record_with<F: FnOnce(RecordingCommandBuffer) -> core::result::Result<(), ErrorCode>>(&mut self, command_pool: &mut CommandPool, begin_info: &RawCommandBufferBeginInfo, record: F) -> core::result::Result<(SuccessCode, SuccessCode), ErrorCode> {
        let begin_result = self.begin_command_buffer(command_pool, begin_info)?;

        let recording = RecordingCommandBuffer {
            command_buffer: self,
        };

        record(recording)?;

        let end_result = self.end_command_buffer(command_pool)?;

        Ok((begin_result, end_result))
    }
}

impl<'cmd> RecordingCommandBuffer<'cmd> {
    pub fn begin_conditional_rendering_ext(&mut self, conditional_rendering_begin: &RawConditionalRenderingBeginInfoEXT) {
        unsafe { self.command_buffer.cmd_begin_conditional_rendering_ext_unchecked(conditional_rendering_begin) }
    }

    pub fn begin_debug_utils_label_ext(&mut self, label_info: &RawDebugUtilsLabelEXT) {
        unsafe { self.command_buffer.cmd_begin_debug_utils_label_ext_unchecked(label_info) }
    }

    pub fn begin_query(&mut self, query_pool: &QueryPool, query: u32, flags: QueryControlFlags) {
        unsafe { self.command_buffer.cmd_begin_query_unchecked(query_pool, query, flags) }
    }

    pub fn begin_query_indexed_ext(&mut self, query_pool: &QueryPool, query: u32, flags: QueryControlFlags, index: u32) {
        unsafe { self.command_buffer.cmd_begin_query_indexed_ext_unchecked(query_pool, query, flags, index) }
    }

    pub fn begin_render_pass(&mut self, render_pass_begin: &RawRenderPassBeginInfo, contents: SubpassContents) {
        unsafe { self.command_buffer.cmd_begin_render_pass_unchecked(render_pass_begin, contents) }
    }

    pub fn begin_render_pass2(&mut self, render_pass_begin: &RawRenderPassBeginInfo, subpass_begin_info: &RawSubpassBeginInfo) {
        unsafe { self.command_buffer.cmd_begin_render_pass2_unchecked(render_pass_begin, subpass_begin_info) }
    }

    pub fn begin_rendering(&mut self, rendering_info: &RawRenderingInfo) {
        unsafe { self.command_buffer.cmd_begin_rendering_unchecked(rendering_info) }
    }

    pub fn begin_transform_feedback_ext(&mut self, first_counter_buffer: u32, counter_buffers: &[BufferHandle], counter_buffer_offsets: &[DeviceSize]) {
        unsafe { self.command_buffer.cmd_begin_transform_feedback_ext_unchecked(first_counter_buffer, counter_buffers, counter_buffer_offsets) }
    }

    pub fn begin_video_coding_khr(&mut self, begin_info: &RawVideoBeginCodingInfoKHR) {
        unsafe { self.command_buffer.cmd_begin_video_coding_khr_unchecked(begin_info) }
    }

    pub fn bind_descriptor_buffer_embedded_samplers_ext(&mut self, pipeline_bind_point: PipelineBindPoint, layout: &PipelineLayout, set: u32) {
        unsafe { self.command_buffer.cmd_bind_descriptor_buffer_embedded_samplers_ext_unchecked(pipeline_bind_point, layout, set) }
    }

    pub fn bind_descriptor_buffers_ext(&mut self, binding_infos: &[RawDescriptorBufferBindingInfoEXT]) {
        unsafe { self.command_buffer.cmd_bind_descriptor_buffers_ext_unchecked(binding_infos) }
    }

    pub fn bind_descriptor_sets(&mut self, pipeline_bind_point: PipelineBindPoint, layout: &PipelineLayout, first_set: u32, descriptor_sets: &[DescriptorSetHandle], dynamic_offsets: &[u32]) {
        unsafe { self.command_buffer.cmd_bind_descriptor_sets_unchecked(pipeline_bind_point, layout, first_set, descriptor_sets, dynamic_offsets) }
    }

    pub fn bind_index_buffer(&mut self, buffer: &Buffer, offset: DeviceSize, index_type: IndexType) {
        unsafe { self.command_buffer.cmd_bind_index_buffer_unchecked(buffer, offset, index_type) }
    }

    pub fn bind_invocation_mask_huawei(&mut self, image_view: Option<&ImageView>, image_layout: ImageLayout) {
        unsafe { self.command_buffer.cmd_bind_invocation_mask_huawei_unchecked(image_view, image_layout) }
    }

    pub fn bind_pipeline(&mut self, pipeline_bind_point: PipelineBindPoint, pipeline: &Pipeline) {
        unsafe { self.command_buffer.cmd_bind_pipeline_unchecked(pipeline_bind_point, pipeline) }
    }

    pub fn bind_pipeline_shader_group_nv(&mut self, pipeline_bind_point: PipelineBindPoint, pipeline: &Pipeline, group_index: u32) {
        unsafe { self.command_buffer.cmd_bind_pipeline_shader_group_nv_unchecked(pipeline_bind_point, pipeline, group_index) }
    }

    pub fn bind_shading_rate_image_nv(&mut self, image_view: Option<&ImageView>, image_layout: ImageLayout) {
        unsafe { self.command_buffer.cmd_bind_shading_rate_image_nv_unchecked(image_view, image_layout) }
    }

    pub fn bind_transform_feedback_buffers_ext(&mut self, first_binding: u32, buffers: &[BufferHandle], offsets: &[DeviceSize], sizes: &[DeviceSize]) {
        unsafe { self.command_buffer.cmd_bind_transform_feedback_buffers_ext_unchecked(first_binding, buffers, offsets, sizes) }
    }

    pub fn bind_vertex_buffers(&mut self, first_binding: u32, buffers: &[BufferHandle], offsets: &[DeviceSize]) {
        unsafe { self.command_buffer.cmd_bind_vertex_buffers_unchecked(first_binding, buffers, offsets) }
    }

    pub fn bind_vertex_buffers2(&mut self, first_binding: u32, buffers: &[BufferHandle], offsets: &[DeviceSize], sizes: &[DeviceSize], strides: &[DeviceSize]) {
        unsafe { self.command_buffer.cmd_bind_vertex_buffers2_unchecked(first_binding, buffers, offsets, sizes, strides) }
    }

    pub fn blit_image(&mut self, src_image: &Image, src_image_layout: ImageLayout, dst_image: &Image, dst_image_layout: ImageLayout, regions: &[RawImageBlit], filter: Filter) {
        unsafe { self.command_buffer.cmd_blit_image_unchecked(src_image, src_image_layout, dst_image, dst_image_layout, regions, filter) }
    }

    pub fn blit_image2(&mut self, blit_image_info: &RawBlitImageInfo2) {
        unsafe { self.command_buffer.cmd_blit_image2_unchecked(blit_image_info) }
    }

    pub fn build_acceleration_structure_nv(&mut self, info: &RawAccelerationStructureInfoNV, instance_data: Option<&Buffer>, instance_offset: DeviceSize, update: Bool32, dst: &AccelerationStructureNV, src: Option<&AccelerationStructureNV>, scratch: &Buffer, scratch_offset: DeviceSize) {
        unsafe { self.command_buffer.cmd_build_acceleration_structure_nv_unchecked(info, instance_data, instance_offset, update, dst, src, scratch, scratch_offset) }
    }

    pub fn build_acceleration_structures_indirect_khr<V: GenericVecGen>(&mut self, infos: &[RawAccelerationStructureBuildGeometryInfoKHR], indirect_device_addresses: &[DeviceAddress], indirect_strides: &[u32], max_primitive_counts: &[&[u32]]) {
        unsafe { self.command_buffer.cmd_build_acceleration_structures_indirect_khr_unchecked::<V>(infos, indirect_device_addresses, indirect_strides, max_primitive_counts) }
    }

    pub fn build_acceleration_structures_khr<V: GenericVecGen>(&mut self, infos: &[RawAccelerationStructureBuildGeometryInfoKHR], build_range_infos: &[&[RawAccelerationStructureBuildRangeInfoKHR]]) {
        unsafe { self.command_buffer.cmd_build_acceleration_structures_khr_unchecked::<V>(infos, build_range_infos) }
    }

    pub fn build_micromaps_ext(&mut self, infos: &[RawMicromapBuildInfoEXT]) {
        unsafe { self.command_buffer.cmd_build_micromaps_ext_unchecked(infos) }
    }

    pub fn clear_attachments(&mut self, attachments: &[RawClearAttachment], rects: &[RawClearRect]) {
        unsafe { self.command_buffer.cmd_clear_attachments_unchecked(attachments, rects) }
    }

    pub fn clear_colour_image(&mut self, image: &Image, image_layout: ImageLayout, colour: Option<&RawClearColourValue>, ranges: &[RawImageSubresourceRange]) {
        unsafe { self.command_buffer.cmd_clear_colour_image_unchecked(image, image_layout, colour, ranges) }
    }

    pub fn clear_depth_stencil_image(&mut self, image: &Image, image_layout: ImageLayout, depth_stencil: &RawClearDepthStencilValue, ranges: &[RawImageSubresourceRange]) {
        unsafe { self.command_buffer.cmd_clear_depth_stencil_image_unchecked(image, image_layout, depth_stencil, ranges) }
    }

    pub fn control_video_coding_khr(&mut self, coding_control_info: &RawVideoCodingControlInfoKHR) {
        unsafe { self.command_buffer.cmd_control_video_coding_khr_unchecked(coding_control_info) }
    }

    pub fn copy_acceleration_structure_khr(&mut self, info: &RawCopyAccelerationStructureInfoKHR) {
        unsafe { self.command_buffer.cmd_copy_acceleration_structure_khr_unchecked(info) }
    }

    pub fn copy_acceleration_structure_nv(&mut self, dst: &AccelerationStructureNV, src: &AccelerationStructureNV, mode: CopyAccelerationStructureModeKHR) {
        unsafe { self.command_buffer.cmd_copy_acceleration_structure_nv_unchecked(dst, src, mode) }
    }

    pub fn copy_acceleration_structure_to_memory_khr(&mut self, info: &RawCopyAccelerationStructureToMemoryInfoKHR) {
        unsafe { self.command_buffer.cmd_copy_acceleration_structure_to_memory_khr_unchecked(info) }
    }

    pub fn copy_buffer(&mut self, src_buffer: &Buffer, dst_buffer: &Buffer, regions: &[RawBufferCopy]) {
        unsafe { self.command_buffer.cmd_copy_buffer_unchecked(src_buffer, dst_buffer, regions) }
    }

    pub fn copy_buffer2(&mut self, copy_buffer_info: &RawCopyBufferInfo2) {
        unsafe { self.command_buffer.cmd_copy_buffer2_unchecked(copy_buffer_info) }
    }

    pub fn copy_buffer_to_image(&mut self, src_buffer: &Buffer, dst_image: &Image, dst_image_layout: ImageLayout, regions: &[RawBufferImageCopy]) {
        unsafe { self.command_buffer.cmd_copy_buffer_to_image_unchecked(src_buffer, dst_image, dst_image_layout, regions) }
    }

    pub fn copy_buffer_to_image2(&mut self, copy_buffer_to_image_info: &RawCopyBufferToImageInfo2) {
        unsafe { self.command_buffer.cmd_copy_buffer_to_image2_unchecked(copy_buffer_to_image_info) }
    }

    pub fn copy_image(&mut self, src_image: &Image, src_image_layout: ImageLayout, dst_image: &Image, dst_image_layout: ImageLayout, regions: &[RawImageCopy]) {
        unsafe { self.command_buffer.cmd_copy_image_unchecked(src_image, src_image_layout, dst_image, dst_image_layout, regions) }
    }

    pub fn copy_image2(&mut self, copy_image_info: &RawCopyImageInfo2) {
        unsafe { self.command_buffer.cmd_copy_image2_unchecked(copy_image_info) }
    }

    pub fn copy_image_to_buffer(&mut self, src_image: &Image, src_image_layout: ImageLayout, dst_buffer: &Buffer, regions: &[RawBufferImageCopy]) {
        unsafe { self.command_buffer.cmd_copy_image_to_buffer_unchecked(src_image, src_image_layout, dst_buffer, regions) }
    }

    pub fn copy_image_to_buffer2(&mut self, copy_image_to_buffer_info: &RawCopyImageToBufferInfo2) {
        unsafe { self.command_buffer.cmd_copy_image_to_buffer2_unchecked(copy_image_to_buffer_info) }
    }

    pub fn copy_memory_indirect_nv(&mut self, copy_buffer_address: DeviceAddress, copy_count: u32, stride: u32) {
        unsafe { self.command_buffer.cmd_copy_memory_indirect_nv_unchecked(copy_buffer_address, copy_count, stride) }
    }

    pub fn copy_memory_to_acceleration_structure_khr(&mut self, info: &RawCopyMemoryToAccelerationStructureInfoKHR) {
        unsafe { self.command_buffer.cmd_copy_memory_to_acceleration_structure_khr_unchecked(info) }
    }

    pub fn copy_memory_to_image_indirect_nv(&mut self, copy_buffer_address: DeviceAddress, stride: u32, dst_image: &Image, dst_image_layout: ImageLayout, image_subresources: &[RawImageSubresourceLayers]) {
        unsafe { self.command_buffer.cmd_copy_memory_to_image_indirect_nv_unchecked(copy_buffer_address, stride, dst_image, dst_image_layout, image_subresources) }
    }

    pub fn copy_memory_to_micromap_ext(&mut self, info: &RawCopyMemoryToMicromapInfoEXT) {
        unsafe { self.command_buffer.cmd_copy_memory_to_micromap_ext_unchecked(info) }
    }

    pub fn copy_micromap_ext(&mut self, info: &RawCopyMicromapInfoEXT) {
        unsafe { self.command_buffer.cmd_copy_micromap_ext_unchecked(info) }
    }

    pub fn copy_micromap_to_memory_ext(&mut self, info: &RawCopyMicromapToMemoryInfoEXT) {
        unsafe { self.command_buffer.cmd_copy_micromap_to_memory_ext_unchecked(info) }
    }

    pub fn copy_query_pool_results(&mut self, query_pool: &QueryPool, first_query: u32, query_count: u32, dst_buffer: &Buffer, dst_offset: DeviceSize, stride: DeviceSize, flags: QueryResultFlags) {
        unsafe { self.command_buffer.cmd_copy_query_pool_results_unchecked(query_pool, first_query, query_count, dst_buffer, dst_offset, stride, flags) }
    }

    pub fn cu_launch_kernel_nvx(&mut self, launch_info: &RawCuLaunchInfoNVX) {
        unsafe { self.command_buffer.cmd_cu_launch_kernel_nvx_unchecked(launch_info) }
    }

    pub fn debug_marker_begin_ext(&mut self, marker_info: &RawDebugMarkerMarkerInfoEXT) {
        unsafe { self.command_buffer.cmd_debug_marker_begin_ext_unchecked(marker_info) }
    }

    pub fn debug_marker_end_ext(&mut self) {
        unsafe { self.command_buffer.cmd_debug_marker_end_ext_unchecked() }
    }

    pub fn debug_marker_insert_ext(&mut self, marker_info: &RawDebugMarkerMarkerInfoEXT) {
        unsafe { self.command_buffer.cmd_debug_marker_insert_ext_unchecked(marker_info) }
    }

    pub fn decode_video_khr(&mut self, decode_info: &RawVideoDecodeInfoKHR) {
        unsafe { self.command_buffer.cmd_decode_video_khr_unchecked(decode_info) }
    }

    pub fn decompress_memory_indirect_count_nv(&mut self, indirect_commands_address: DeviceAddress, indirect_commands_count_address: DeviceAddress, stride: u32) {
        unsafe { self.command_buffer.cmd_decompress_memory_indirect_count_nv_unchecked(indirect_commands_address, indirect_commands_count_address, stride) }
    }

    pub fn decompress_memory_nv(&mut self, decompress_memory_regions: &[RawDecompressMemoryRegionNV]) {
        unsafe { self.command_buffer.cmd_decompress_memory_nv_unchecked(decompress_memory_regions) }
    }

    pub fn dispatch(&mut self, group_count_x: u32, group_count_y: u32, group_count_z: u32) {
        unsafe { self.command_buffer.cmd_dispatch_unchecked(group_count_x, group_count_y, group_count_z) }
    }

    pub fn dispatch_base(&mut self, base_group_x: u32, base_group_y: u32, base_group_z: u32, group_count_x: u32, group_count_y: u32, group_count_z: u32) {
        unsafe { self.command_buffer.cmd_dispatch_base_unchecked(base_group_x, base_group_y, base_group_z, group_count_x, group_count_y, group_count_z) }
    }

    pub fn dispatch_indirect(&mut self, buffer: &Buffer, offset: DeviceSize) {
        unsafe { self.command_buffer.cmd_dispatch_indirect_unchecked(buffer, offset) }
    }

    pub fn draw(&mut self, vertex_count: u32, instance_count: u32, first_vertex: u32, first_instance: u32) {
        unsafe { self.command_buffer.cmd_draw_unchecked(vertex_count, instance_count, first_vertex, first_instance) }
    }

    pub fn draw_cluster_huawei(&mut self, group_count_x: u32, group_count_y: u32, group_count_z: u32) {
        unsafe { self.command_buffer.cmd_draw_cluster_huawei_unchecked(group_count_x, group_count_y, group_count_z) }
    }

    pub fn draw_cluster_indirect_huawei(&mut self, buffer: &Buffer, offset: DeviceSize) {
        unsafe { self.command_buffer.cmd_draw_cluster_indirect_huawei_unchecked(buffer, offset) }
    }

    pub fn draw_indexed(&mut self, index_count: u32, instance_count: u32, first_index: u32, vertex_offset: i32, first_instance: u32) {
        unsafe { self.command_buffer.cmd_draw_indexed_unchecked(index_count, instance_count, first_index, vertex_offset, first_instance) }
    }

    pub fn draw_indexed_indirect(&mut self, buffer: &Buffer, offset: DeviceSize, draw_count: u32, stride: u32) {
        unsafe { self.command_buffer.cmd_draw_indexed_indirect_unchecked(buffer, offset, draw_count, stride) }
    }

    pub fn draw_indexed_indirect_count(&mut self, buffer: &Buffer, offset: DeviceSize, count_buffer: &Buffer, count_buffer_offset: DeviceSize, max_draw_count: u32, stride: u32) {
        unsafe { self.command_buffer.cmd_draw_indexed_indirect_count_unchecked(buffer, offset, count_buffer, count_buffer_offset, max_draw_count, stride) }
    }

    pub fn draw_indirect(&mut self, buffer: &Buffer, offset: DeviceSize, draw_count: u32, stride: u32) {
        unsafe { self.command_buffer.cmd_draw_indirect_unchecked(buffer, offset, draw_count, stride) }
    }

    pub fn draw_indirect_byte_count_ext(&mut self, instance_count: u32, first_instance: u32, counter_buffer: &Buffer, counter_buffer_offset: DeviceSize, counter_offset: u32, vertex_stride: u32) {
        unsafe { self.command_buffer.cmd_draw_indirect_byte_count_ext_unchecked(instance_count, first_instance, counter_buffer, counter_buffer_offset, counter_offset, vertex_stride) }
    }

    pub fn draw_indirect_count(&mut self, buffer: &Buffer, offset: DeviceSize, count_buffer: &Buffer, count_buffer_offset: DeviceSize, max_draw_count: u32, stride: u32) {
        unsafe { self.command_buffer.cmd_draw_indirect_count_unchecked(buffer, offset, count_buffer, count_buffer_offset, max_draw_count, stride) }
    }

    pub fn draw_mesh_tasks_ext(&mut self, group_count_x: u32, group_count_y: u32, group_count_z: u32) {
        unsafe { self.command_buffer.cmd_draw_mesh_tasks_ext_unchecked(group_count_x, group_count_y, group_count_z) }
    }

    pub fn draw_mesh_tasks_indirect_count_ext(&mut self, buffer: &Buffer, offset: DeviceSize, count_buffer: &Buffer, count_buffer_offset: DeviceSize, max_draw_count: u32, stride: u32) {
        unsafe { self.command_buffer.cmd_draw_mesh_tasks_indirect_count_ext_unchecked(buffer, offset, count_buffer, count_buffer_offset, max_draw_count, stride) }
    }

    pub fn draw_mesh_tasks_indirect_count_nv(&mut self, buffer: &Buffer, offset: DeviceSize, count_buffer: &Buffer, count_buffer_offset: DeviceSize, max_draw_count: u32, stride: u32) {
        unsafe { self.command_buffer.cmd_draw_mesh_tasks_indirect_count_nv_unchecked(buffer, offset, count_buffer, count_buffer_offset, max_draw_count, stride) }
    }

    pub fn draw_mesh_tasks_indirect_ext(&mut self, buffer: &Buffer, offset: DeviceSize, draw_count: u32, stride: u32) {
        unsafe { self.command_buffer.cmd_draw_mesh_tasks_indirect_ext_unchecked(buffer, offset, draw_count, stride) }
    }

    pub fn draw_mesh_tasks_indirect_nv(&mut self, buffer: &Buffer, offset: DeviceSize, draw_count: u32, stride: u32) {
        unsafe { self.command_buffer.cmd_draw_mesh_tasks_indirect_nv_unchecked(buffer, offset, draw_count, stride) }
    }

    pub fn draw_mesh_tasks_nv(&mut self, task_count: u32, first_task: u32) {
        unsafe { self.command_buffer.cmd_draw_mesh_tasks_nv_unchecked(task_count, first_task) }
    }

    pub fn draw_multi_ext(&mut self, vertex_info: &[RawMultiDrawInfoEXT], instance_count: u32, first_instance: u32, stride: u32) {
        unsafe { self.command_buffer.cmd_draw_multi_ext_unchecked(vertex_info, instance_count, first_instance, stride) }
    }

    pub fn draw_multi_indexed_ext(&mut self, index_info: &[RawMultiDrawIndexedInfoEXT], instance_count: u32, first_instance: u32, stride: u32, vertex_offset: Option<&i32>) {
        unsafe { self.command_buffer.cmd_draw_multi_indexed_ext_unchecked(index_info, instance_count, first_instance, stride, vertex_offset) }
    }

    pub fn end_conditional_rendering_ext(&mut self) {
        unsafe { self.command_buffer.cmd_end_conditional_rendering_ext_unchecked() }
    }

    pub fn end_debug_utils_label_ext(&mut self) {
        unsafe { self.command_buffer.cmd_end_debug_utils_label_ext_unchecked() }
    }

    pub fn end_query(&mut self, query_pool: &QueryPool, query: u32) {
        unsafe { self.command_buffer.cmd_end_query_unchecked(query_pool, query) }
    }

    pub fn end_query_indexed_ext(&mut self, query_pool: &QueryPool, query: u32, index: u32) {
        unsafe { self.command_buffer.cmd_end_query_indexed_ext_unchecked(query_pool, query, index) }
    }

    pub fn end_render_pass(&mut self) {
        unsafe { self.command_buffer.cmd_end_render_pass_unchecked() }
    }

    pub fn end_render_pass2(&mut self, subpass_end_info: &RawSubpassEndInfo) {
        unsafe { self.command_buffer.cmd_end_render_pass2_unchecked(subpass_end_info) }
    }

    pub fn end_rendering(&mut self) {
        unsafe { self.command_buffer.cmd_end_rendering_unchecked() }
    }

    pub fn end_transform_feedback_ext(&mut self, first_counter_buffer: u32, counter_buffers: &[BufferHandle], counter_buffer_offsets: &[DeviceSize]) {
        unsafe { self.command_buffer.cmd_end_transform_feedback_ext_unchecked(first_counter_buffer, counter_buffers, counter_buffer_offsets) }
    }

    pub fn end_video_coding_khr(&mut self, end_coding_info: &RawVideoEndCodingInfoKHR) {
        unsafe { self.command_buffer.cmd_end_video_coding_khr_unchecked(end_coding_info) }
    }

    pub fn execute_commands(&mut self, command_buffers: &[CommandBufferHandle]) {
        unsafe { self.command_buffer.cmd_execute_commands_unchecked(command_buffers) }
    }

    pub fn execute_generated_commands_nv(&mut self, is_preprocessed: Bool32, generated_commands_info: &RawGeneratedCommandsInfoNV) {
        unsafe { self.command_buffer.cmd_execute_generated_commands_nv_unchecked(is_preprocessed, generated_commands_info) }
    }

    pub fn fill_buffer(&mut self, dst_buffer: &Buffer, dst_offset: DeviceSize, size: DeviceSize, data: u32) {
        unsafe { self.command_buffer.cmd_fill_buffer_unchecked(dst_buffer, dst_offset, size, data) }
    }

    pub fn insert_debug_utils_label_ext(&mut self, label_info: &RawDebugUtilsLabelEXT) {
        unsafe { self.command_buffer.cmd_insert_debug_utils_label_ext_unchecked(label_info) }
    }

    pub fn next_subpass(&mut self, contents: SubpassContents) {
        unsafe { self.command_buffer.cmd_next_subpass_unchecked(contents) }
    }

    pub fn next_subpass2(&mut self, subpass_begin_info: &RawSubpassBeginInfo, subpass_end_info: &RawSubpassEndInfo) {
        unsafe { self.command_buffer.cmd_next_subpass2_unchecked(subpass_begin_info, subpass_end_info) }
    }

    pub fn optical_flow_execute_nv(&mut self, session: &OpticalFlowSessionNV, execute_info: &RawOpticalFlowExecuteInfoNV) {
        unsafe { self.command_buffer.cmd_optical_flow_execute_nv_unchecked(session, execute_info) }
    }

    pub fn pipeline_barrier(&mut self, src_stage_mask: PipelineStageFlags, dst_stage_mask: PipelineStageFlags, dependency_flags: DependencyFlags, memory_barriers: &[RawMemoryBarrier], buffer_memory_barriers: &[RawBufferMemoryBarrier], image_memory_barriers: &[RawImageMemoryBarrier]) {
        unsafe { self.command_buffer.cmd_pipeline_barrier_unchecked(src_stage_mask, dst_stage_mask, dependency_flags, memory_barriers, buffer_memory_barriers, image_memory_barriers) }
    }

    pub fn pipeline_barrier2(&mut self, dependency_info: &RawDependencyInfo) {
        unsafe { self.command_buffer.cmd_pipeline_barrier2_unchecked(dependency_info) }
    }

    pub fn preprocess_generated_commands_nv(&mut self, generated_commands_info: &RawGeneratedCommandsInfoNV) {
        unsafe { self.command_buffer.cmd_preprocess_generated_commands_nv_unchecked(generated_commands_info) }
    }

    pub fn push_constants(&mut self, layout: &PipelineLayout, stage_flags: ShaderStageFlags, offset: u32, values: &[u8]) {
        unsafe { self.command_buffer.cmd_push_constants_unchecked(layout, stage_flags, offset, values) }
    }

    pub fn push_descriptor_set_khr(&mut self, pipeline_bind_point: PipelineBindPoint, layout: &PipelineLayout, set: u32, descriptor_writes: &[RawWriteDescriptorSet]) {
        unsafe { self.command_buffer.cmd_push_descriptor_set_khr_unchecked(pipeline_bind_point, layout, set, descriptor_writes) }
    }

    pub fn push_descriptor_set_with_template_khr(&mut self, descriptor_update_template: &DescriptorUpdateTemplate, layout: &PipelineLayout, set: u32, data: Option<&u8>) {
        unsafe { self.command_buffer.cmd_push_descriptor_set_with_template_khr_unchecked(descriptor_update_template, layout, set, data) }
    }

    pub fn reset_event(&mut self, event: &Event, stage_mask: PipelineStageFlags) {
        unsafe { self.command_buffer.cmd_reset_event_unchecked(event, stage_mask) }
    }

    pub fn reset_event2(&mut self, event: &Event, stage_mask: PipelineStageFlags2) {
        unsafe { self.command_buffer.cmd_reset_event2_unchecked(event, stage_mask) }
    }

    pub fn reset_query_pool(&mut self, query_pool: &QueryPool, first_query: u32, query_count: u32) {
        unsafe { self.command_buffer.cmd_reset_query_pool_unchecked(query_pool, first_query, query_count) }
    }

    pub fn resolve_image(&mut self, src_image: &Image, src_image_layout: ImageLayout, dst_image: &Image, dst_image_layout: ImageLayout, regions: &[RawImageResolve]) {
        unsafe { self.command_buffer.cmd_resolve_image_unchecked(src_image, src_image_layout, dst_image, dst_image_layout, regions) }
    }

    pub fn resolve_image2(&mut self, resolve_image_info: &RawResolveImageInfo2) {
        unsafe { self.command_buffer.cmd_resolve_image2_unchecked(resolve_image_info) }
    }

    pub fn set_alpha_to_coverage_enable_ext(&mut self, alpha_to_coverage_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_alpha_to_coverage_enable_ext_unchecked(alpha_to_coverage_enable) }
    }

    pub fn set_alpha_to_one_enable_ext(&mut self, alpha_to_one_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_alpha_to_one_enable_ext_unchecked(alpha_to_one_enable) }
    }

    pub fn set_blend_constants(&mut self, blend_constants: ConstSizePtr<f32, 4>) {
        unsafe { self.command_buffer.cmd_set_blend_constants_unchecked(blend_constants) }
    }

    pub fn set_checkpoint_nv(&mut self, checkpoint_marker: Option<&u8>) {
        unsafe { self.command_buffer.cmd_set_checkpoint_nv_unchecked(checkpoint_marker) }
    }

    pub fn set_coarse_sample_order_nv(&mut self, sample_order_type: CoarseSampleOrderTypeNV, custom_sample_orders: &[RawCoarseSampleOrderCustomNV]) {
        unsafe { self.command_buffer.cmd_set_coarse_sample_order_nv_unchecked(sample_order_type, custom_sample_orders) }
    }

    pub fn set_colour_blend_advanced_ext(&mut self, first_attachment: u32, colour_blend_advanced: &[RawColourBlendAdvancedEXT]) {
        unsafe { self.command_buffer.cmd_set_colour_blend_advanced_ext_unchecked(first_attachment, colour_blend_advanced) }
    }

    pub fn set_colour_blend_enable_ext(&mut self, first_attachment: u32, colour_blend_enables: &[Bool32]) {
        unsafe { self.command_buffer.cmd_set_colour_blend_enable_ext_unchecked(first_attachment, colour_blend_enables) }
    }

    pub fn set_colour_blend_equation_ext(&mut self, first_attachment: u32, colour_blend_equations: &[RawColourBlendEquationEXT]) {
        unsafe { self.command_buffer.cmd_set_colour_blend_equation_ext_unchecked(first_attachment, colour_blend_equations) }
    }

    pub fn set_colour_write_enable_ext(&mut self, colour_write_enables: &[Bool32]) {
        unsafe { self.command_buffer.cmd_set_colour_write_enable_ext_unchecked(colour_write_enables) }
    }

    pub fn set_colour_write_mask_ext(&mut self, first_attachment: u32, colour_write_masks: &[ColourComponentFlags]) {
        unsafe { self.command_buffer.cmd_set_colour_write_mask_ext_unchecked(first_attachment, colour_write_masks) }
    }

    pub fn set_conservative_rasterization_mode_ext(&mut self, conservative_rasterization_mode: ConservativeRasterizationModeEXT) {
        unsafe { self.command_buffer.cmd_set_conservative_rasterization_mode_ext_unchecked(conservative_rasterization_mode) }
    }

    pub fn set_coverage_modulation_mode_nv(&mut self, coverage_modulation_mode: CoverageModulationModeNV) {
        unsafe { self.command_buffer.cmd_set_coverage_modulation_mode_nv_unchecked(coverage_modulation_mode) }
    }

    pub fn set_coverage_modulation_table_enable_nv(&mut self, coverage_modulation_table_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_coverage_modulation_table_enable_nv_unchecked(coverage_modulation_table_enable) }
    }

    pub fn set_coverage_modulation_table_nv(&mut self, coverage_modulation_table: &[f32]) {
        unsafe { self.command_buffer.cmd_set_coverage_modulation_table_nv_unchecked(coverage_modulation_table) }
    }

    pub fn set_coverage_reduction_mode_nv(&mut self, coverage_reduction_mode: CoverageReductionModeNV) {
        unsafe { self.command_buffer.cmd_set_coverage_reduction_mode_nv_unchecked(coverage_reduction_mode) }
    }

    pub fn set_coverage_to_colour_enable_nv(&mut self, coverage_to_colour_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_coverage_to_colour_enable_nv_unchecked(coverage_to_colour_enable) }
    }

    pub fn set_coverage_to_colour_location_nv(&mut self, coverage_to_colour_location: u32) {
        unsafe { self.command_buffer.cmd_set_coverage_to_colour_location_nv_unchecked(coverage_to_colour_location) }
    }

    pub fn set_cull_mode(&mut self, cull_mode: CullModeFlags) {
        unsafe { self.command_buffer.cmd_set_cull_mode_unchecked(cull_mode) }
    }

    pub fn set_depth_bias(&mut self, depth_bias_constant_factor: f32, depth_bias_clamp: f32, depth_bias_slope_factor: f32) {
        unsafe { self.command_buffer.cmd_set_depth_bias_unchecked(depth_bias_constant_factor, depth_bias_clamp, depth_bias_slope_factor) }
    }

    pub fn set_depth_bias_enable(&mut self, depth_bias_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_depth_bias_enable_unchecked(depth_bias_enable) }
    }

    pub fn set_depth_bounds(&mut self, min_depth_bounds: f32, max_depth_bounds: f32) {
        unsafe { self.command_buffer.cmd_set_depth_bounds_unchecked(min_depth_bounds, max_depth_bounds) }
    }

    pub fn set_depth_bounds_test_enable(&mut self, depth_bounds_test_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_depth_bounds_test_enable_unchecked(depth_bounds_test_enable) }
    }

    pub fn set_depth_clamp_enable_ext(&mut self, depth_clamp_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_depth_clamp_enable_ext_unchecked(depth_clamp_enable) }
    }

    pub fn set_depth_clip_enable_ext(&mut self, depth_clip_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_depth_clip_enable_ext_unchecked(depth_clip_enable) }
    }

    pub fn set_depth_clip_negative_one_to_one_ext(&mut self, negative_one_to_one: Bool32) {
        unsafe { self.command_buffer.cmd_set_depth_clip_negative_one_to_one_ext_unchecked(negative_one_to_one) }
    }

    pub fn set_depth_compare_op(&mut self, depth_compare_op: CompareOp) {
        unsafe { self.command_buffer.cmd_set_depth_compare_op_unchecked(depth_compare_op) }
    }

    pub fn set_depth_test_enable(&mut self, depth_test_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_depth_test_enable_unchecked(depth_test_enable) }
    }

    pub fn set_depth_write_enable(&mut self, depth_write_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_depth_write_enable_unchecked(depth_write_enable) }
    }

    pub fn set_descriptor_buffer_offsets_ext(&mut self, pipeline_bind_point: PipelineBindPoint, layout: &PipelineLayout, first_set: u32, buffer_indices: &[u32], offsets: &[DeviceSize]) {
        unsafe { self.command_buffer.cmd_set_descriptor_buffer_offsets_ext_unchecked(pipeline_bind_point, layout, first_set, buffer_indices, offsets) }
    }

    pub fn set_device_mask(&mut self, device_mask: u32) {
        unsafe { self.command_buffer.cmd_set_device_mask_unchecked(device_mask) }
    }

    pub fn set_discard_rectangle_ext(&mut self, first_discard_rectangle: u32, discard_rectangles: &[RawRect2D]) {
        unsafe { self.command_buffer.cmd_set_discard_rectangle_ext_unchecked(first_discard_rectangle, discard_rectangles) }
    }

    pub fn set_discard_rectangle_enable_ext(&mut self, discard_rectangle_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_discard_rectangle_enable_ext_unchecked(discard_rectangle_enable) }
    }

    pub fn set_discard_rectangle_mode_ext(&mut self, discard_rectangle_mode: DiscardRectangleModeEXT) {
        unsafe { self.command_buffer.cmd_set_discard_rectangle_mode_ext_unchecked(discard_rectangle_mode) }
    }

    pub fn set_event(&mut self, event: &Event, stage_mask: PipelineStageFlags) {
        unsafe { self.command_buffer.cmd_set_event_unchecked(event, stage_mask) }
    }

    pub fn set_event2(&mut self, event: &Event, dependency_info: &RawDependencyInfo) {
        unsafe { self.command_buffer.cmd_set_event2_unchecked(event, dependency_info) }
    }

    pub fn set_exclusive_scissor_enable_nv(&mut self, first_exclusive_scissor: u32, exclusive_scissor_enables: &[Bool32]) {
        unsafe { self.command_buffer.cmd_set_exclusive_scissor_enable_nv_unchecked(first_exclusive_scissor, exclusive_scissor_enables) }
    }

    pub fn set_exclusive_scissor_nv(&mut self, first_exclusive_scissor: u32, exclusive_scissors: &[RawRect2D]) {
        unsafe { self.command_buffer.cmd_set_exclusive_scissor_nv_unchecked(first_exclusive_scissor, exclusive_scissors) }
    }

    pub fn set_extra_primitive_overestimation_size_ext(&mut self, extra_primitive_overestimation_size: f32) {
        unsafe { self.command_buffer.cmd_set_extra_primitive_overestimation_size_ext_unchecked(extra_primitive_overestimation_size) }
    }

    pub fn set_fragment_shading_rate_enum_nv(&mut self, shading_rate: FragmentShadingRateNV, combiner_ops: ConstSizePtr<RawFragmentShadingRateCombinerOpKHR, 2>) {
        unsafe { self.command_buffer.cmd_set_fragment_shading_rate_enum_nv_unchecked(shading_rate, combiner_ops) }
    }

    pub fn set_fragment_shading_rate_khr(&mut self, fragment_size: &RawExtent2D, combiner_ops: ConstSizePtr<RawFragmentShadingRateCombinerOpKHR, 2>) {
        unsafe { self.command_buffer.cmd_set_fragment_shading_rate_khr_unchecked(fragment_size, combiner_ops) }
    }

    pub fn set_front_face(&mut self, front_face: FrontFace) {
        unsafe { self.command_buffer.cmd_set_front_face_unchecked(front_face) }
    }

    pub fn set_line_rasterization_mode_ext(&mut self, line_rasterization_mode: LineRasterizationModeEXT) {
        unsafe { self.command_buffer.cmd_set_line_rasterization_mode_ext_unchecked(line_rasterization_mode) }
    }

    pub fn set_line_stipple_ext(&mut self, line_stipple_factor: u32, line_stipple_pattern: u16) {
        unsafe { self.command_buffer.cmd_set_line_stipple_ext_unchecked(line_stipple_factor, line_stipple_pattern) }
    }

    pub fn set_line_stipple_enable_ext(&mut self, stippled_line_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_line_stipple_enable_ext_unchecked(stippled_line_enable) }
    }

    pub fn set_line_width(&mut self, line_width: f32) {
        unsafe { self.command_buffer.cmd_set_line_width_unchecked(line_width) }
    }

    pub fn set_logic_op_ext(&mut self, logic_op: LogicOp) {
        unsafe { self.command_buffer.cmd_set_logic_op_ext_unchecked(logic_op) }
    }

    pub fn set_logic_op_enable_ext(&mut self, logic_op_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_logic_op_enable_ext_unchecked(logic_op_enable) }
    }

    pub fn set_patch_control_points_ext(&mut self, patch_control_points: u32) {
        unsafe { self.command_buffer.cmd_set_patch_control_points_ext_unchecked(patch_control_points) }
    }

    /// SUCCESS_CODES = [[SuccessCode::eSuccess]];
    ///
    /// ERROR_CODES = [[ErrorCode::eErrorTooManyObjects], [ErrorCode::eErrorOutOfHostMemory]];
    pub fn set_performance_marker_intel(&mut self, marker_info: &RawPerformanceMarkerInfoINTEL) -> core::result::Result<SuccessCode, ErrorCode> {
        unsafe { self.command_buffer.cmd_set_performance_marker_intel_unchecked(marker_info) }
    }

    /// SUCCESS_CODES = [[SuccessCode::eSuccess]];
    ///
    /// ERROR_CODES = [[ErrorCode::eErrorTooManyObjects], [ErrorCode::eErrorOutOfHostMemory]];
    pub fn set_performance_override_intel(&mut self, override_info: &RawPerformanceOverrideInfoINTEL) -> core::result::Result<SuccessCode, ErrorCode> {
        unsafe { self.command_buffer.cmd_set_performance_override_intel_unchecked(override_info) }
    }

    /// SUCCESS_CODES = [[SuccessCode::eSuccess]];
    ///
    /// ERROR_CODES = [[ErrorCode::eErrorTooManyObjects], [ErrorCode::eErrorOutOfHostMemory]];
    pub fn set_performance_stream_marker_intel(&mut self, marker_info: &RawPerformanceStreamMarkerInfoINTEL) -> core::result::Result<SuccessCode, ErrorCode> {
        unsafe { self.command_buffer.cmd_set_performance_stream_marker_intel_unchecked(marker_info) }
    }

    pub fn set_polygon_mode_ext(&mut self, polygon_mode: PolygonMode) {
        unsafe { self.command_buffer.cmd_set_polygon_mode_ext_unchecked(polygon_mode) }
    }

    pub fn set_primitive_restart_enable(&mut self, primitive_restart_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_primitive_restart_enable_unchecked(primitive_restart_enable) }
    }

    pub fn set_primitive_topology(&mut self, primitive_topology: PrimitiveTopology) {
        unsafe { self.command_buffer.cmd_set_primitive_topology_unchecked(primitive_topology) }
    }

    pub fn set_provoking_vertex_mode_ext(&mut self, provoking_vertex_mode: ProvokingVertexModeEXT) {
        unsafe { self.command_buffer.cmd_set_provoking_vertex_mode_ext_unchecked(provoking_vertex_mode) }
    }

    pub fn set_rasterization_samples_ext(&mut self, rasterization_samples: SampleCountFlagBits) {
        unsafe { self.command_buffer.cmd_set_rasterization_samples_ext_unchecked(rasterization_samples) }
    }

    pub fn set_rasterization_stream_ext(&mut self, rasterization_stream: u32) {
        unsafe { self.command_buffer.cmd_set_rasterization_stream_ext_unchecked(rasterization_stream) }
    }

    pub fn set_rasterizer_discard_enable(&mut self, rasterizer_discard_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_rasterizer_discard_enable_unchecked(rasterizer_discard_enable) }
    }

    pub fn set_ray_tracing_pipeline_stack_size_khr(&mut self, pipeline_stack_size: u32) {
        unsafe { self.command_buffer.cmd_set_ray_tracing_pipeline_stack_size_khr_unchecked(pipeline_stack_size) }
    }

    pub fn set_representative_fragment_test_enable_nv(&mut self, representative_fragment_test_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_representative_fragment_test_enable_nv_unchecked(representative_fragment_test_enable) }
    }

    pub fn set_sample_locations_ext(&mut self, sample_locations_info: &RawSampleLocationsInfoEXT) {
        unsafe { self.command_buffer.cmd_set_sample_locations_ext_unchecked(sample_locations_info) }
    }

    pub fn set_sample_locations_enable_ext(&mut self, sample_locations_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_sample_locations_enable_ext_unchecked(sample_locations_enable) }
    }

    pub fn set_sample_mask_ext(&mut self, samples: SampleCountFlagBits, sample_mask: &[SampleMask]) {
        unsafe { self.command_buffer.cmd_set_sample_mask_ext_unchecked(samples, sample_mask) }
    }

    pub fn set_scissor(&mut self, first_scissor: u32, scissors: &[RawRect2D]) {
        unsafe { self.command_buffer.cmd_set_scissor_unchecked(first_scissor, scissors) }
    }

    pub fn set_scissor_with_count(&mut self, scissors: &[RawRect2D]) {
        unsafe { self.command_buffer.cmd_set_scissor_with_count_unchecked(scissors) }
    }

    pub fn set_shading_rate_image_enable_nv(&mut self, shading_rate_image_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_shading_rate_image_enable_nv_unchecked(shading_rate_image_enable) }
    }

    pub fn set_stencil_compare_mask(&mut self, face_mask: StencilFaceFlags, compare_mask: u32) {
        unsafe { self.command_buffer.cmd_set_stencil_compare_mask_unchecked(face_mask, compare_mask) }
    }

    pub fn set_stencil_op(&mut self, face_mask: StencilFaceFlags, fail_op: StencilOp, pass_op: StencilOp, depth_fail_op: StencilOp, compare_op: CompareOp) {
        unsafe { self.command_buffer.cmd_set_stencil_op_unchecked(face_mask, fail_op, pass_op, depth_fail_op, compare_op) }
    }

    pub fn set_stencil_reference(&mut self, face_mask: StencilFaceFlags, reference: u32) {
        unsafe { self.command_buffer.cmd_set_stencil_reference_unchecked(face_mask, reference) }
    }

    pub fn set_stencil_test_enable(&mut self, stencil_test_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_stencil_test_enable_unchecked(stencil_test_enable) }
    }

    pub fn set_stencil_write_mask(&mut self, face_mask: StencilFaceFlags, write_mask: u32) {
        unsafe { self.command_buffer.cmd_set_stencil_write_mask_unchecked(face_mask, write_mask) }
    }

    pub fn set_tessellation_domain_origin_ext(&mut self, domain_origin: TessellationDomainOrigin) {
        unsafe { self.command_buffer.cmd_set_tessellation_domain_origin_ext_unchecked(domain_origin) }
    }

    pub fn set_vertex_input_ext(&mut self, vertex_binding_descriptions: &[RawVertexInputBindingDescription2EXT], vertex_attribute_descriptions: &[RawVertexInputAttributeDescription2EXT]) {
        unsafe { self.command_buffer.cmd_set_vertex_input_ext_unchecked(vertex_binding_descriptions, vertex_attribute_descriptions) }
    }

    pub fn set_viewport(&mut self, first_viewport: u32, viewports: &[RawViewport]) {
        unsafe { self.command_buffer.cmd_set_viewport_unchecked(first_viewport, viewports) }
    }

    pub fn set_viewport_shading_rate_palette_nv(&mut self, first_viewport: u32, shading_rate_palettes: &[RawShadingRatePaletteNV]) {
        unsafe { self.command_buffer.cmd_set_viewport_shading_rate_palette_nv_unchecked(first_viewport, shading_rate_palettes) }
    }

    pub fn set_viewport_swizzle_nv(&mut self, first_viewport: u32, viewport_swizzles: &[RawViewportSwizzleNV]) {
        unsafe { self.command_buffer.cmd_set_viewport_swizzle_nv_unchecked(first_viewport, viewport_swizzles) }
    }

    pub fn set_viewport_w_scaling_enable_nv(&mut self, viewport_w_scaling_enable: Bool32) {
        unsafe { self.command_buffer.cmd_set_viewport_w_scaling_enable_nv_unchecked(viewport_w_scaling_enable) }
    }

    pub fn set_viewport_w_scaling_nv(&mut self, first_viewport: u32, viewport_w_scalings: &[RawViewportWScalingNV]) {
        unsafe { self.command_buffer.cmd_set_viewport_w_scaling_nv_unchecked(first_viewport, viewport_w_scalings) }
    }

    pub fn set_viewport_with_count(&mut self, viewports: &[RawViewport]) {
        unsafe { self.command_buffer.cmd_set_viewport_with_count_unchecked(viewports) }
    }

    pub fn subpass_shading_huawei(&mut self) {
        unsafe { self.command_buffer.cmd_subpass_shading_huawei_unchecked() }
    }

    pub fn trace_rays_indirect2_khr(&mut self, indirect_device_address: DeviceAddress) {
        unsafe { self.command_buffer.cmd_trace_rays_indirect2_khr_unchecked(indirect_device_address) }
    }

    pub fn trace_rays_indirect_khr(&mut self, raygen_shader_binding_table: &RawStridedDeviceAddressRegionKHR, miss_shader_binding_table: &RawStridedDeviceAddressRegionKHR, hit_shader_binding_table: &RawStridedDeviceAddressRegionKHR, callable_shader_binding_table: &RawStridedDeviceAddressRegionKHR, indirect_device_address: DeviceAddress) {
        unsafe { self.command_buffer.cmd_trace_rays_indirect_khr_unchecked(raygen_shader_binding_table, miss_shader_binding_table, hit_shader_binding_table, callable_shader_binding_table, indirect_device_address) }
    }

    pub fn trace_rays_khr(&mut self, raygen_shader_binding_table: &RawStridedDeviceAddressRegionKHR, miss_shader_binding_table: &RawStridedDeviceAddressRegionKHR, hit_shader_binding_table: &RawStridedDeviceAddressRegionKHR, callable_shader_binding_table: &RawStridedDeviceAddressRegionKHR, width: u32, height: u32, depth: u32) {
        unsafe { self.command_buffer.cmd_trace_rays_khr_unchecked(raygen_shader_binding_table, miss_shader_binding_table, hit_shader_binding_table, callable_shader_binding_table, width, height, depth) }
    }

    pub fn trace_rays_nv(&mut self, raygen_shader_binding_table_buffer: &Buffer, raygen_shader_binding_offset: DeviceSize, miss_shader_binding_table_buffer: Option<&Buffer>, miss_shader_binding_offset: DeviceSize, miss_shader_binding_stride: DeviceSize, hit_shader_binding_table_buffer: Option<&Buffer>, hit_shader_binding_offset: DeviceSize, hit_shader_binding_stride: DeviceSize, callable_shader_binding_table_buffer: Option<&Buffer>, callable_shader_binding_offset: DeviceSize, callable_shader_binding_stride: DeviceSize, width: u32, height: u32, depth: u32) {
        unsafe { self.command_buffer.cmd_trace_rays_nv_unchecked(raygen_shader_binding_table_buffer, raygen_shader_binding_offset, miss_shader_binding_table_buffer, miss_shader_binding_offset, miss_shader_binding_stride, hit_shader_binding_table_buffer, hit_shader_binding_offset, hit_shader_binding_stride, callable_shader_binding_table_buffer, callable_shader_binding_offset, callable_shader_binding_stride, width, height, depth) }
    }

    pub fn update_buffer(&mut self, dst_buffer: &Buffer, dst_offset: DeviceSize, data: &[u8]) {
        unsafe { self.command_buffer.cmd_update_buffer_unchecked(dst_buffer, dst_offset, data) }
    }

    pub fn wait_events(&mut self, events: &[EventHandle], src_stage_mask: PipelineStageFlags, dst_stage_mask: PipelineStageFlags, memory_barriers: &[RawMemoryBarrier], buffer_memory_barriers: &[RawBufferMemoryBarrier], image_memory_barriers: &[RawImageMemoryBarrier]) {
        unsafe { self.command_buffer.cmd_wait_events_unchecked(events, src_stage_mask, dst_stage_mask, memory_barriers, buffer_memory_barriers, image_memory_barriers) }
    }

    pub fn wait_events2(&mut self, events: &[EventHandle], dependency_infos: &[RawDependencyInfo]) {
        unsafe { self.command_buffer.cmd_wait_events2_unchecked(events, dependency_infos) }
    }

    pub fn write_acceleration_structures_properties_khr(&mut self, acceleration_structures: &[AccelerationStructureKHRHandle], query_type: QueryType, query_pool: &QueryPool, first_query: u32) {
        unsafe { self.command_buffer.cmd_write_acceleration_structures_properties_khr_unchecked(acceleration_structures, query_type, query_pool, first_query) }
    }

    pub fn write_acceleration_structures_properties_nv(&mut self, acceleration_structures: &[AccelerationStructureNVHandle], query_type: QueryType, query_pool: &QueryPool, first_query: u32) {
        unsafe { self.command_buffer.cmd_write_acceleration_structures_properties_nv_unchecked(acceleration_structures, query_type, query_pool, first_query) }
    }

    pub fn write_buffer_marker2_amd(&mut self, stage: PipelineStageFlags2, dst_buffer: &Buffer, dst_offset: DeviceSize, marker: u32) {
        unsafe { self.command_buffer.cmd_write_buffer_marker2_amd_unchecked(stage, dst_buffer, dst_offset, marker) }
    }

    pub fn write_buffer_marker_amd(&mut self, pipeline_stage: PipelineStageFlagBits, dst_buffer: &Buffer, dst_offset: DeviceSize, marker: u32) {
        unsafe { self.command_buffer.cmd_write_buffer_marker_amd_unchecked(pipeline_stage, dst_buffer, dst_offset, marker) }
    }

    pub fn write_micromaps_properties_ext(&mut self, micromaps: &[MicromapEXTHandle], query_type: QueryType, query_pool: &QueryPool, first_query: u32) {
        unsafe { self.command_buffer.cmd_write_micromaps_properties_ext_unchecked(micromaps, query_type, query_pool, first_query) }
    }

    pub fn write_timestamp(&mut self, pipeline_stage: PipelineStageFlagBits, query_pool: &QueryPool, query: u32) {
        unsafe { self.command_buffer.cmd_write_timestamp_unchecked(pipeline_stage, query_pool, query) }
    }

    pub fn write_timestamp2(&mut self, stage: PipelineStageFlags2, query_pool: &QueryPool, query: u32) {
        unsafe { self.command_buffer.cmd_write_timestamp2_unchecked(stage, query_pool, query) }
    }
}
