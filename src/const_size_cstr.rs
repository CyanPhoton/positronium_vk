use std::cmp::Ordering;
use std::convert::TryFrom;
use std::ffi::{CStr, CString};
use std::fmt::{Debug, Display, Formatter};
use std::hash::{Hash, Hasher};
use std::str::Utf8Error;
use crate::UnsizedCStr;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum ConstSizeCStrErrors {
    UndersizedBuffer,
    NoTerminatingNullByte,
    InteriorNullByte(usize),
    InvalidUTF8(Utf8Error),
}

impl Display for ConstSizeCStrErrors {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ConstSizeCStrErrors::UndersizedBuffer => write!(f, "The buffer size of the ConstSizeCStr is too small to fit the string with null byte"),
            ConstSizeCStrErrors::NoTerminatingNullByte => write!(f, "The buffer of the ConstSizeCStr contains no null byte to indicate the end of the string"),
            ConstSizeCStrErrors::InteriorNullByte(i) => write!(f, "Tried to construct from str with an interior null byte as index: {}", i),
            ConstSizeCStrErrors::InvalidUTF8(e) => write!(f, "Tried to interpret ConstSizeCStr as a utf8 string but got error: {}", e)
        }
    }
}

///
/// A CStr like data structure, backed by an inline const size array of u8, which is equivalent to c_char as
/// c_char is either u8 or i8 which both have identical size and alignment, and since most std api's use u8
/// I will use that here.
///
/// Designed for use with the Vulkan api, and thus is intended to go both ways across the ffi barrier,
/// thus it lacks the ability to store the actual length of the contained string, so one must be
/// careful that many methods are O(n) which might normally be expected to be O(1).
///
#[repr(transparent)]
#[derive(Copy, Clone)]
pub struct ConstSizeCStr<const N: usize> {
    bytes: [u8; N]
}

impl<const N: usize> ConstSizeCStr<N> {
    ///
    /// Construct a ConstSizeCStr from a regular CStr with backing buffer sized as annotated.
    ///
    /// Will error if the length of the CStr (including null byte) is greater than the backing buffer.
    /// But not if it lacks a null-byte as I assume the CStr is valid, which it guarantees so long as
    /// it's not unsafely instantiated.
    ///
    /// Note: The only O(1) constructor as CStr already guarantees a correctly configured null byte.
    ///
    pub fn from_cstr<C: AsRef<CStr>>(cstr: C) -> Result<ConstSizeCStr<N>, ConstSizeCStrErrors> {
        let b = cstr.as_ref().to_bytes_with_nul();
        if b.len() > N {
            Err(ConstSizeCStrErrors::UndersizedBuffer)
        } else {
            Ok(Self::from_slice_unchecked(b))
        }
    }

    ///
    /// Construct a ConstSizeCStr from a &str, with backing buffer sized as annotated.
    ///
    /// Will return an error if the &str has interior null bytes,
    /// or if the &str (plus a null byte if needed) is larger than the size of the backing buffer
    ///
    pub fn from_str<S: AsRef<str>>(s: S) -> Result<ConstSizeCStr<N>, ConstSizeCStrErrors> {
        Self::from_slice(s.as_ref().as_bytes())
    }

    ///
    /// Note: It is strongly recommend to only use in cost setting, since it can panic at runtime otherwise,
    /// and it may also have worse runtime performance than the non const version
    ///
    /// Construct a ConstSizeCStr from a &str at compile time, with backing buffer sized as annotated.
    ///
    /// Will panic at compile time (when it const setting, runtime otherwise) if the &str contains an interior null byte,
    /// or if the &str (plus a null byte if needed) is larger than the size of the backing buffer
    ///
    pub const fn from_str_const(s: &str) -> ConstSizeCStr<N> {
        Self::from_slice_const(s.as_bytes())
    }

    ///
    /// Construct a ConstSizeCStr from a fixed size array of u8's with a size equal to that of the array.
    ///
    /// Will return an error if there is no null byte, note though that it can be anywhere and there can be
    /// as many as you want, the first one will simply be considered the end of the contents of the string.
    ///
    pub fn from_array(bytes: [u8; N]) -> Result<ConstSizeCStr<N>, ConstSizeCStrErrors> {
        Self::null_byte_location(&bytes)
            .map(|_| ConstSizeCStr {
                bytes
            })
    }

    ///
    /// Note: It is strongly recommend to only use in cost setting, since it can panic at runtime otherwise,
    /// and it may also have worse runtime performance than the non const version
    ///
    /// Construct a ConstSizeCStr from a fixed size array of u8's with a size equal to that of the array.
    ///
    /// Will return an error if there is no null byte, note though that it can be anywhere and there can be
    /// as many as you want, the first one will simply be considered the end of the contents of the string.
    ///
    pub const fn from_array_const(bytes: [u8; N]) -> ConstSizeCStr<N> {
        match Self::null_byte_location_const(&bytes) {
            Ok(_) => ConstSizeCStr {
                bytes
            },
            Err(_) => const_panic!("The buffer of the ConstSizeCStr contains no null byte to indicate the end of the string")
        }
    }

    ///
    /// Construct a ConstSizeCStr from a slice of u8's with backing buffer sized as annotated.
    ///
    /// Will return an error if the &[u8] has interior null bytes,
    /// or if the &[u8] (plus a null byte if needed) is larger than the size of the backing buffer
    ///
    pub fn from_slice(bytes: &[u8]) -> Result<ConstSizeCStr<N>, ConstSizeCStrErrors> {
        let bytes = match bytes.last() {
            Some(0) => &bytes[..bytes.len()-1], // Trim a provided terminating null byte
            _ => bytes
        };

        if bytes.len() > N {
            Err(ConstSizeCStrErrors::UndersizedBuffer)
        } else {
            match Self::null_byte_location(&bytes) {
                Ok(i) => Err(ConstSizeCStrErrors::InteriorNullByte(i)),
                Err(_) => Ok(Self::from_slice_unchecked(bytes))
            }
        }
    }

    ///
    /// Note: It is strongly recommend to only use in cost setting, since it can panic at runtime otherwise,
    /// and it may also have worse runtime performance than the non const version
    ///
    /// Construct a ConstSizeCStr from a slice of u8's at compile time with backing buffer sized as annotated.
    ///
    /// Will Will panic at compile time (when it const setting, runtime otherwise) if the &[u8] has interior null bytes,
    /// or if the &[u8] (plus a null byte if needed) is larger than the size of the backing buffer
    ///
    pub const fn from_slice_const(bytes: &[u8]) -> ConstSizeCStr<N> {
        let len = if bytes.is_empty() || bytes[bytes.len() - 1] != 0 {
            bytes.len()
        } else {
            bytes.len() - 1
        };

        if len >= N  { // Need to leave room for an extra byte for null byte
            const_panic!("The buffer size of the ConstSizeCStr is too small to fit the string with null byte")
        } else {
            match Self::null_byte_location_const(bytes) {
                Ok(i) => {
                    if i == len {
                        Self::from_slice_unchecked_const(bytes)
                    } else {
                        const_panic!("Tried to construct from str with an interior null byte")
                    }
                },
                Err(_) => Self::from_slice_unchecked_const(bytes)
            }
        }
    }

    fn from_slice_unchecked(b: &[u8]) -> ConstSizeCStr<N> {
        let mut bytes: [u8; N] = [0; N];

        let to_write: &mut [_] = &mut bytes[..b.len()];
        to_write.copy_from_slice(b);

        ConstSizeCStr {
            bytes
        }
    }

    const fn from_slice_unchecked_const(b: &[u8]) -> ConstSizeCStr<N> {
        let mut bytes: [u8; N] = [0; N];

        let mut i = 0;

        while i < b.len() {
            bytes[i] = b[i];

            i += 1;
        }

        ConstSizeCStr {
            bytes
        }
    }

    fn null_byte_location(bytes: &[u8]) -> Result<usize, ConstSizeCStrErrors> {
        memchr::memchr(0, bytes).ok_or(ConstSizeCStrErrors::NoTerminatingNullByte)
    }

    const fn null_byte_location_const(bytes: &[u8]) -> Result<usize, ConstSizeCStrErrors> {
        let mut i = 0;
        while i < bytes.len() {
            if bytes[i] == 0 {
                return Ok(i);
            }

            i += 1;
        }
        return Err(ConstSizeCStrErrors::NoTerminatingNullByte);
    }

    ///
    /// Calculates the length of the string, excluding the null byte.
    ///
    /// Will return an error if the string has no null byte (most likely coming from ffi)
    ///
    /// Note: This is not free and is O(n)
    ///
    pub fn len(&self) -> Result<usize, ConstSizeCStrErrors> {
        Self::null_byte_location(&self.bytes)
    }

    ///
    /// Calculates the length of the string, including the null byte.
    ///
    /// Will return an error if the string has no null byte (most likely coming from ffi)
    ///
    /// Note: This is not free and is O(n)
    ///
    pub fn len_with_null(&self) -> Result<usize, ConstSizeCStrErrors> {
        Self::null_byte_location(&self.bytes).map(|i| i + 1)
    }

    ///
    /// Returns the backing byte array
    ///
    pub fn into_bytes(self) -> [u8; N] {
        self.bytes
    }

    ///
    /// Returns a slice to the u8 array, excluding the null byte.
    ///
    /// Will return an error if the string has no null byte (most likely coming from ffi)
    ///
    /// Note: This is not free and is O(n)
    ///
    pub fn as_bytes(&self) -> Result<&[u8], ConstSizeCStrErrors> {
        self.len().map(|i|
            &self.bytes[..i]
        )
    }

    ///
    /// Returns a mutable slice to the u8 array, excluding the null byte.
    ///
    /// Will return an error if the string has no null byte (most likely coming from ffi)
    ///
    /// Note: This is not free and is O(n)
    ///
    pub fn as_bytes_mut(&mut self) -> Result<&mut [u8], ConstSizeCStrErrors> {
        self.len().map(move |i|
            &mut self.bytes[..i]
        )
    }

    ///
    /// Returns a slice to the u8 array, including the null byte.
    ///
    /// Will return an error if the string has no null byte (most likely coming from ffi)
    ///
    /// Note: This is not free and is O(n)
    ///
    pub fn as_bytes_with_null(&self) -> Result<&[u8], ConstSizeCStrErrors> {
        self.len_with_null().map(|i|
            &self.bytes[..i]
        )
    }

    ///
    /// Returns an array ref to the u8 array, all of it, regardless of the presence, location or absence of a null byte(s)
    ///
    /// Note: This is O(1) unlike other slice methods
    ///
    pub fn as_bytes_full(&self) -> &[u8; N] {
        &self.bytes
    }

    ///
    /// Interprets the ConstSizeCStr as a regular CStr
    ///
    /// Returns an error if the string has no null byte (most likely coming from ffi)
    ///
    /// Note: This is not free and is O(n)
    ///
    pub fn as_cstr(&self) -> Result<&CStr, ConstSizeCStrErrors> {
        self.as_bytes_with_null().map(|b| unsafe { CStr::from_bytes_with_nul_unchecked(b) })
    }

    ///
    /// Interprets the ConstSizeCStr as a regular CStr, then allocate on heap as a CString
    ///
    /// Returns an error if the string has no null byte (most likely coming from ffi)
    ///
    /// Note: This is not free and is O(n)
    ///
    pub fn to_c_string(&self) -> Result<CString, ConstSizeCStrErrors> {
        Ok(CString::from(self.as_cstr()?))
    }

    ///
    /// Interprets the ConstSizeCStr as a regular &str (without the terminating null byte)
    ///
    /// Returns an error if the string has no null byte (most likely coming from ffi),
    /// or if it us invalid utf-8
    ///
    /// Note: This is not free and is O(n)
    ///
    pub fn as_str(&self) -> Result<&str, ConstSizeCStrErrors> {
        match self.as_bytes() {
            Ok(b) => {
                match std::str::from_utf8(b) {
                    Ok(s) => Ok(s),
                    Err(e) => Err(ConstSizeCStrErrors::InvalidUTF8(e))
                }
            }
            Err(e) => Err(e)
        }
    }

    ///
    /// Interprets the ConstSizeCStr as a regular &str (without the terminating null byte)
    ///
    /// Returns an error if the string has no null byte (most likely coming from ffi),
    /// or if it us invalid utf-8
    ///
    /// Note: This is not free and is O(n)
    ///
    pub fn as_str_with_null(&self) -> Result<&str, ConstSizeCStrErrors> {
        match self.as_bytes_with_null() {
            Ok(b) => {
                match std::str::from_utf8(b) {
                    Ok(s) => Ok(s),
                    Err(e) => Err(ConstSizeCStrErrors::InvalidUTF8(e))
                }
            }
            Err(e) => Err(e)
        }
    }

    ///
    /// Interprets the ConstSizeCStr as a regular &mut str
    ///
    /// Returns an error if the string has no null byte (most likely coming from ffi),
    /// or if it us invalid utf-8
    ///
    /// Note: This is not free and is O(n)
    ///
    pub fn as_str_mut(&mut self) -> Result<&mut str, ConstSizeCStrErrors> {
        match self.as_bytes_mut() {
            Ok(b) => {
                match std::str::from_utf8_mut(b) {
                    Ok(s) => Ok(s),
                    Err(e) => Err(ConstSizeCStrErrors::InvalidUTF8(e))
                }
            }
            Err(e) => Err(e)
        }
    }

    ///
    /// Interprets the ConstSizeCStr as a regular &str and converts to a newly allocated String
    ///
    /// Returns an error if the string has no null byte (most likely coming from ffi),
    /// or if it us invalid utf-8
    ///
    /// Note: This is not free and is O(n)
    ///
    pub fn to_string(&self) -> Result<String, ConstSizeCStrErrors> {
        self.as_str().map(|s| s.to_string())
    }

    ///
    /// Interprets the ConstSizeCStr as a regular &str and converts to a newly allocated String
    ///
    /// Returns an error if the string has no null byte (most likely coming from ffi),
    /// or if it us invalid utf-8
    ///
    /// Note: This is not free and is O(n)
    ///
    pub fn to_string_with_null(&self) -> Result<String, ConstSizeCStrErrors> {
        self.as_str_with_null().map(|s| s.to_string())
    }
}

impl<const N: usize> TryFrom<&CStr> for ConstSizeCStr<N> {
    type Error = ConstSizeCStrErrors;

    fn try_from(value: &CStr) -> Result<Self, Self::Error> {
        Self::from_cstr(value)
    }
}

impl<const N: usize> TryFrom<&str> for ConstSizeCStr<N> {
    type Error = ConstSizeCStrErrors;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Self::from_str(value)
    }
}

impl<const N: usize> TryFrom<&String> for ConstSizeCStr<N> {
    type Error = ConstSizeCStrErrors;

    fn try_from(value: &String) -> Result<Self, Self::Error> {
        Self::from_str(value)
    }
}

impl<const N: usize> TryFrom<[u8; N]> for ConstSizeCStr<N> {
    type Error = ConstSizeCStrErrors;

    fn try_from(value: [u8; N]) -> Result<Self, Self::Error> {
        Self::from_array(value)
    }
}

impl<const N: usize, const M: usize> TryFrom<&[u8; N]> for ConstSizeCStr<M> {
    type Error = ConstSizeCStrErrors;

    fn try_from(value: &[u8; N]) -> Result<Self, Self::Error> {
        Self::from_slice(value)
    }
}

impl<const N: usize> TryFrom<&[u8]> for ConstSizeCStr<N> {
    type Error = ConstSizeCStrErrors;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        Self::from_slice(value)
    }
}

impl<'a, const N: usize> TryFrom<&'a ConstSizeCStr<N>> for &'a str {
    type Error = ConstSizeCStrErrors;

    fn try_from(value: &'a ConstSizeCStr<N>) -> Result<Self, Self::Error> {
        value.as_str()
    }
}

impl<const N: usize> TryFrom<ConstSizeCStr<N>> for CString {
    type Error = ConstSizeCStrErrors;

    fn try_from(value: ConstSizeCStr<N>) -> Result<Self, Self::Error> {
        value.to_c_string()
    }
}

impl<'a, const N: usize> TryFrom<&'a ConstSizeCStr<N>> for CString {
    type Error = ConstSizeCStrErrors;

    fn try_from(value: &'a ConstSizeCStr<N>) -> Result<Self, Self::Error> {
        value.to_c_string()
    }
}

impl<'a, const N: usize> TryFrom<&'a ConstSizeCStr<N>> for String {
    type Error = ConstSizeCStrErrors;

    fn try_from(value: &'a ConstSizeCStr<N>) -> Result<Self, Self::Error> {
        value.to_string()
    }
}

impl<const N: usize> Debug for ConstSizeCStr<N> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self.as_cstr() {
            Ok(c_str) => c_str.fmt(f),
            Err(e) => write!(f, "ConstSizeCStr: {:?}", e)
        }
    }
}

impl<const N: usize> Default for ConstSizeCStr<N> {
    fn default() -> Self {
        ConstSizeCStr {
            bytes: [0; N]
        }
    }
}

impl<const N: usize, const M: usize> PartialEq<ConstSizeCStr<M>> for ConstSizeCStr<N> {
    fn eq(&self, other: &ConstSizeCStr<M>) -> bool {
        self.as_bytes().unwrap().eq(other.as_bytes().unwrap())
    }
}

impl<const N: usize> PartialEq<CStr> for ConstSizeCStr<N> {
    fn eq(&self, other: &CStr) -> bool {
        self.as_bytes().unwrap().eq(other.to_bytes())
    }
}

impl<'a, const N: usize> PartialEq<UnsizedCStr<'a>> for ConstSizeCStr<N> {
    fn eq(&self, other: &UnsizedCStr<'a>) -> bool {
        self.as_bytes().unwrap().eq(other.as_bytes())
    }
}

impl<const N: usize> Eq for ConstSizeCStr<N> {}

impl<const N: usize, const M: usize> PartialOrd<ConstSizeCStr<N>> for ConstSizeCStr<M> {
    fn partial_cmp(&self, other: &ConstSizeCStr<N>) -> Option<Ordering> {
        self.as_bytes().unwrap().partial_cmp(other.as_bytes().unwrap())
    }
}

impl<const N: usize> PartialOrd<CStr> for ConstSizeCStr<N> {
    fn partial_cmp(&self, other: &CStr) -> Option<Ordering> {
        self.as_bytes().unwrap().partial_cmp(other.to_bytes())
    }
}

impl<'a, const N: usize> PartialOrd<UnsizedCStr<'a>> for ConstSizeCStr<N> {
    fn partial_cmp(&self, other: &UnsizedCStr<'a>) -> Option<Ordering> {
        self.as_bytes().unwrap().partial_cmp(other.as_bytes())
    }
}

impl<const N: usize> Ord for ConstSizeCStr<N> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.as_bytes().unwrap().cmp(other.as_bytes().unwrap())
    }
}

impl<const N: usize> Hash for ConstSizeCStr<N> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.as_bytes().unwrap().hash(state)
    }
}

#[cfg(test)]
mod test {
    use std::convert::TryInto;
    use std::ffi::CString;

    use super::*;

    const TEST0: ConstSizeCStr<128> = ConstSizeCStr::from_str_const("Hello, World!");

    const TEST1: ConstSizeCStr<128> = ConstSizeCStr::from_slice_const(b"Hello, World!\0");

    const TEST2: ConstSizeCStr<2048> = ConstSizeCStr::from_slice_const(b"Hello, World!\0");

    #[test]
    fn test() {
        let t: [u8; 2] = [42, 0];

        let a = ConstSizeCStr::from_array(t).unwrap();
        dbg!(a.as_cstr());

        assert_eq!(a.as_bytes(), Ok(&t[0..1]));
        assert_eq!(a.as_bytes_with_null(), Ok(&t[..]));

        let c = CStr::from_bytes_with_nul(b"Hello, World!\0").unwrap();

        let b = ConstSizeCStr::<32>::from_cstr(c).unwrap();

        assert_eq!(b.as_cstr().unwrap(), c);

        assert_eq!(b.as_bytes_with_null().map(|b| b.last()), Ok(Some(&0)));

        let d = ConstSizeCStr::<32>::from_cstr(CString::new("Hello, World!").unwrap()).unwrap();

        assert_eq!(d.as_cstr(), b.as_cstr());

        assert_eq!(format!("{:?}", d).as_str(), "\"Hello, World!\"");

        dbg!(d);

        assert_eq!(b, d);

        assert_eq!(b, ConstSizeCStr::<64>::from_slice(b"Hello, World!\0").unwrap());

        let e: ConstSizeCStr<64> = b"Test byte str\0".try_into().unwrap();

        println!("TEST 0: {:?}", TEST0);
        assert_eq!(b, TEST0);
        assert_eq!(b, TEST1);
        assert_eq!(b, TEST2);

        let f: ConstSizeCStr<2048> = ConstSizeCStr::from_str_const("Hello, World!");

        println!("f: {:?}", f);

        println!("f, {:?}", f.as_str())
    }
}