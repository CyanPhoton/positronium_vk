pub use super::*;
// Enums:
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ImageLayout {
    eUndefined,
    eGeneral,
    eColourAttachmentOptimal,
    eDepthStencilAttachmentOptimal,
    eDepthStencilReadOnlyOptimal,
    eShaderReadOnlyOptimal,
    eTransferSrcOptimal,
    eTransferDstOptimal,
    ePreinitialized,
    ePresentSrcKhr,
    eVideoDecodeDstKhr,
    eVideoDecodeSrcKhr,
    eVideoDecodeDpbKhr,
    eSharedPresentKhr,
    eDepthReadOnlyStencilAttachmentOptimal,
    eDepthAttachmentStencilReadOnlyOptimal,
    eFragmentShadingRateAttachmentOptimalKhr,
    eFragmentDensityMapOptimalExt,
    eDepthAttachmentOptimal,
    eDepthReadOnlyOptimal,
    eStencilAttachmentOptimal,
    eStencilReadOnlyOptimal,
    eVideoEncodeDstKhr,
    eVideoEncodeSrcKhr,
    eVideoEncodeDpbKhr,
    eReadOnlyOptimal,
    eAttachmentOptimal,
    eAttachmentFeedbackLoopOptimalExt,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ImageLayout {
    pub const eAttachmentOptimalKhr: Self = Self::eAttachmentOptimal;
    pub const eDepthAttachmentOptimalKhr: Self = Self::eDepthAttachmentOptimal;
    pub const eDepthAttachmentStencilReadOnlyOptimalKhr: Self = Self::eDepthAttachmentStencilReadOnlyOptimal;
    pub const eDepthReadOnlyOptimalKhr: Self = Self::eDepthReadOnlyOptimal;
    pub const eDepthReadOnlyStencilAttachmentOptimalKhr: Self = Self::eDepthReadOnlyStencilAttachmentOptimal;
    pub const eShadingRateOptimalNv: Self = Self::eFragmentShadingRateAttachmentOptimalKhr;
    pub const eReadOnlyOptimalKhr: Self = Self::eReadOnlyOptimal;
    pub const eStencilAttachmentOptimalKhr: Self = Self::eStencilAttachmentOptimal;
    pub const eStencilReadOnlyOptimalKhr: Self = Self::eStencilReadOnlyOptimal;

    pub fn into_raw(self) -> RawImageLayout {
        match self {
            Self::eUndefined => RawImageLayout::eUndefined,
            Self::eGeneral => RawImageLayout::eGeneral,
            Self::eColourAttachmentOptimal => RawImageLayout::eColourAttachmentOptimal,
            Self::eDepthStencilAttachmentOptimal => RawImageLayout::eDepthStencilAttachmentOptimal,
            Self::eDepthStencilReadOnlyOptimal => RawImageLayout::eDepthStencilReadOnlyOptimal,
            Self::eShaderReadOnlyOptimal => RawImageLayout::eShaderReadOnlyOptimal,
            Self::eTransferSrcOptimal => RawImageLayout::eTransferSrcOptimal,
            Self::eTransferDstOptimal => RawImageLayout::eTransferDstOptimal,
            Self::ePreinitialized => RawImageLayout::ePreinitialized,
            Self::ePresentSrcKhr => RawImageLayout::ePresentSrcKhr,
            Self::eVideoDecodeDstKhr => RawImageLayout::eVideoDecodeDstKhr,
            Self::eVideoDecodeSrcKhr => RawImageLayout::eVideoDecodeSrcKhr,
            Self::eVideoDecodeDpbKhr => RawImageLayout::eVideoDecodeDpbKhr,
            Self::eSharedPresentKhr => RawImageLayout::eSharedPresentKhr,
            Self::eDepthReadOnlyStencilAttachmentOptimal => RawImageLayout::eDepthReadOnlyStencilAttachmentOptimal,
            Self::eDepthAttachmentStencilReadOnlyOptimal => RawImageLayout::eDepthAttachmentStencilReadOnlyOptimal,
            Self::eFragmentShadingRateAttachmentOptimalKhr => RawImageLayout::eFragmentShadingRateAttachmentOptimalKhr,
            Self::eFragmentDensityMapOptimalExt => RawImageLayout::eFragmentDensityMapOptimalExt,
            Self::eDepthAttachmentOptimal => RawImageLayout::eDepthAttachmentOptimal,
            Self::eDepthReadOnlyOptimal => RawImageLayout::eDepthReadOnlyOptimal,
            Self::eStencilAttachmentOptimal => RawImageLayout::eStencilAttachmentOptimal,
            Self::eStencilReadOnlyOptimal => RawImageLayout::eStencilReadOnlyOptimal,
            Self::eVideoEncodeDstKhr => RawImageLayout::eVideoEncodeDstKhr,
            Self::eVideoEncodeSrcKhr => RawImageLayout::eVideoEncodeSrcKhr,
            Self::eVideoEncodeDpbKhr => RawImageLayout::eVideoEncodeDpbKhr,
            Self::eReadOnlyOptimal => RawImageLayout::eReadOnlyOptimal,
            Self::eAttachmentOptimal => RawImageLayout::eAttachmentOptimal,
            Self::eAttachmentFeedbackLoopOptimalExt => RawImageLayout::eAttachmentFeedbackLoopOptimalExt,
            Self::eUnknownVariant(v) => RawImageLayout(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum AttachmentLoadOp {
    eLoad,
    eClear,
    eDontCare,
    eNoneExt,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl AttachmentLoadOp {
    pub fn into_raw(self) -> RawAttachmentLoadOp {
        match self {
            Self::eLoad => RawAttachmentLoadOp::eLoad,
            Self::eClear => RawAttachmentLoadOp::eClear,
            Self::eDontCare => RawAttachmentLoadOp::eDontCare,
            Self::eNoneExt => RawAttachmentLoadOp::eNoneExt,
            Self::eUnknownVariant(v) => RawAttachmentLoadOp(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum AttachmentStoreOp {
    eStore,
    eDontCare,
    eNone,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl AttachmentStoreOp {
    pub const eNoneExt: Self = Self::eNone;
    pub const eNoneKhr: Self = Self::eNone;
    pub const eNoneQcom: Self = Self::eNone;

    pub fn into_raw(self) -> RawAttachmentStoreOp {
        match self {
            Self::eStore => RawAttachmentStoreOp::eStore,
            Self::eDontCare => RawAttachmentStoreOp::eDontCare,
            Self::eNone => RawAttachmentStoreOp::eNone,
            Self::eUnknownVariant(v) => RawAttachmentStoreOp(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ImageType {
    e1d,
    e2d,
    e3d,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ImageType {
    pub fn into_raw(self) -> RawImageType {
        match self {
            Self::e1d => RawImageType::e1d,
            Self::e2d => RawImageType::e2d,
            Self::e3d => RawImageType::e3d,
            Self::eUnknownVariant(v) => RawImageType(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ImageTiling {
    eOptimal,
    eLinear,
    eDrmFormatModifierExt,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ImageTiling {
    pub fn into_raw(self) -> RawImageTiling {
        match self {
            Self::eOptimal => RawImageTiling::eOptimal,
            Self::eLinear => RawImageTiling::eLinear,
            Self::eDrmFormatModifierExt => RawImageTiling::eDrmFormatModifierExt,
            Self::eUnknownVariant(v) => RawImageTiling(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ImageViewType {
    e1d,
    e2d,
    e3d,
    eCube,
    e1dArray,
    e2dArray,
    eCubeArray,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ImageViewType {
    pub fn into_raw(self) -> RawImageViewType {
        match self {
            Self::e1d => RawImageViewType::e1d,
            Self::e2d => RawImageViewType::e2d,
            Self::e3d => RawImageViewType::e3d,
            Self::eCube => RawImageViewType::eCube,
            Self::e1dArray => RawImageViewType::e1dArray,
            Self::e2dArray => RawImageViewType::e2dArray,
            Self::eCubeArray => RawImageViewType::eCubeArray,
            Self::eUnknownVariant(v) => RawImageViewType(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum CommandBufferLevel {
    ePrimary,
    eSecondary,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl CommandBufferLevel {
    pub fn into_raw(self) -> RawCommandBufferLevel {
        match self {
            Self::ePrimary => RawCommandBufferLevel::ePrimary,
            Self::eSecondary => RawCommandBufferLevel::eSecondary,
            Self::eUnknownVariant(v) => RawCommandBufferLevel(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ComponentSwizzle {
    eIdentity,
    eZero,
    eOne,
    eR,
    eG,
    eB,
    eA,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ComponentSwizzle {
    pub fn into_raw(self) -> RawComponentSwizzle {
        match self {
            Self::eIdentity => RawComponentSwizzle::eIdentity,
            Self::eZero => RawComponentSwizzle::eZero,
            Self::eOne => RawComponentSwizzle::eOne,
            Self::eR => RawComponentSwizzle::eR,
            Self::eG => RawComponentSwizzle::eG,
            Self::eB => RawComponentSwizzle::eB,
            Self::eA => RawComponentSwizzle::eA,
            Self::eUnknownVariant(v) => RawComponentSwizzle(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DescriptorType {
    eSampler,
    eCombinedImageSampler,
    eSampledImage,
    eStorageImage,
    eUniformTexelBuffer,
    eStorageTexelBuffer,
    eUniformBuffer,
    eStorageBuffer,
    eUniformBufferDynamic,
    eStorageBufferDynamic,
    eInputAttachment,
    eInlineUniformBlock,
    eAccelerationStructureKhr,
    eAccelerationStructureNv,
    eMutableExt,
    eSampleWeightImageQcom,
    eBlockMatchImageQcom,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl DescriptorType {
    pub const eInlineUniformBlockExt: Self = Self::eInlineUniformBlock;
    pub const eMutableValve: Self = Self::eMutableExt;

    pub fn into_raw(self) -> RawDescriptorType {
        match self {
            Self::eSampler => RawDescriptorType::eSampler,
            Self::eCombinedImageSampler => RawDescriptorType::eCombinedImageSampler,
            Self::eSampledImage => RawDescriptorType::eSampledImage,
            Self::eStorageImage => RawDescriptorType::eStorageImage,
            Self::eUniformTexelBuffer => RawDescriptorType::eUniformTexelBuffer,
            Self::eStorageTexelBuffer => RawDescriptorType::eStorageTexelBuffer,
            Self::eUniformBuffer => RawDescriptorType::eUniformBuffer,
            Self::eStorageBuffer => RawDescriptorType::eStorageBuffer,
            Self::eUniformBufferDynamic => RawDescriptorType::eUniformBufferDynamic,
            Self::eStorageBufferDynamic => RawDescriptorType::eStorageBufferDynamic,
            Self::eInputAttachment => RawDescriptorType::eInputAttachment,
            Self::eInlineUniformBlock => RawDescriptorType::eInlineUniformBlock,
            Self::eAccelerationStructureKhr => RawDescriptorType::eAccelerationStructureKhr,
            Self::eAccelerationStructureNv => RawDescriptorType::eAccelerationStructureNv,
            Self::eMutableExt => RawDescriptorType::eMutableExt,
            Self::eSampleWeightImageQcom => RawDescriptorType::eSampleWeightImageQcom,
            Self::eBlockMatchImageQcom => RawDescriptorType::eBlockMatchImageQcom,
            Self::eUnknownVariant(v) => RawDescriptorType(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum QueryType {
    eOcclusion,
    ePipelineStatistics,
    eTimestamp,
    eResultStatusOnlyKhr,
    eTransformFeedbackStreamExt,
    ePerformanceQueryKhr,
    eAccelerationStructureCompactedSizeKhr,
    eAccelerationStructureSerializationSizeKhr,
    eAccelerationStructureCompactedSizeNv,
    ePerformanceQueryIntel,
    eVideoEncodeBitstreamBufferRangeKhr,
    eMeshPrimitivesGeneratedExt,
    ePrimitivesGeneratedExt,
    eAccelerationStructureSerializationBottomLevelPointersKhr,
    eAccelerationStructureSizeKhr,
    eMicromapSerializationSizeExt,
    eMicromapCompactedSizeExt,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl QueryType {
    pub fn into_raw(self) -> RawQueryType {
        match self {
            Self::eOcclusion => RawQueryType::eOcclusion,
            Self::ePipelineStatistics => RawQueryType::ePipelineStatistics,
            Self::eTimestamp => RawQueryType::eTimestamp,
            Self::eResultStatusOnlyKhr => RawQueryType::eResultStatusOnlyKhr,
            Self::eTransformFeedbackStreamExt => RawQueryType::eTransformFeedbackStreamExt,
            Self::ePerformanceQueryKhr => RawQueryType::ePerformanceQueryKhr,
            Self::eAccelerationStructureCompactedSizeKhr => RawQueryType::eAccelerationStructureCompactedSizeKhr,
            Self::eAccelerationStructureSerializationSizeKhr => RawQueryType::eAccelerationStructureSerializationSizeKhr,
            Self::eAccelerationStructureCompactedSizeNv => RawQueryType::eAccelerationStructureCompactedSizeNv,
            Self::ePerformanceQueryIntel => RawQueryType::ePerformanceQueryIntel,
            Self::eVideoEncodeBitstreamBufferRangeKhr => RawQueryType::eVideoEncodeBitstreamBufferRangeKhr,
            Self::eMeshPrimitivesGeneratedExt => RawQueryType::eMeshPrimitivesGeneratedExt,
            Self::ePrimitivesGeneratedExt => RawQueryType::ePrimitivesGeneratedExt,
            Self::eAccelerationStructureSerializationBottomLevelPointersKhr => RawQueryType::eAccelerationStructureSerializationBottomLevelPointersKhr,
            Self::eAccelerationStructureSizeKhr => RawQueryType::eAccelerationStructureSizeKhr,
            Self::eMicromapSerializationSizeExt => RawQueryType::eMicromapSerializationSizeExt,
            Self::eMicromapCompactedSizeExt => RawQueryType::eMicromapCompactedSizeExt,
            Self::eUnknownVariant(v) => RawQueryType(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum BorderColour {
    eFloatTransparentBlack,
    eIntTransparentBlack,
    eFloatOpaqueBlack,
    eIntOpaqueBlack,
    eFloatOpaqueWhite,
    eIntOpaqueWhite,
    eFloatCustomExt,
    eIntCustomExt,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl BorderColour {
    pub fn into_raw(self) -> RawBorderColour {
        match self {
            Self::eFloatTransparentBlack => RawBorderColour::eFloatTransparentBlack,
            Self::eIntTransparentBlack => RawBorderColour::eIntTransparentBlack,
            Self::eFloatOpaqueBlack => RawBorderColour::eFloatOpaqueBlack,
            Self::eIntOpaqueBlack => RawBorderColour::eIntOpaqueBlack,
            Self::eFloatOpaqueWhite => RawBorderColour::eFloatOpaqueWhite,
            Self::eIntOpaqueWhite => RawBorderColour::eIntOpaqueWhite,
            Self::eFloatCustomExt => RawBorderColour::eFloatCustomExt,
            Self::eIntCustomExt => RawBorderColour::eIntCustomExt,
            Self::eUnknownVariant(v) => RawBorderColour(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PipelineBindPoint {
    eGraphics,
    eCompute,
    eRayTracingKhr,
    eSubpassShadingHuawei,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl PipelineBindPoint {
    pub const eRayTracingNv: Self = Self::eRayTracingKhr;

    pub fn into_raw(self) -> RawPipelineBindPoint {
        match self {
            Self::eGraphics => RawPipelineBindPoint::eGraphics,
            Self::eCompute => RawPipelineBindPoint::eCompute,
            Self::eRayTracingKhr => RawPipelineBindPoint::eRayTracingKhr,
            Self::eSubpassShadingHuawei => RawPipelineBindPoint::eSubpassShadingHuawei,
            Self::eUnknownVariant(v) => RawPipelineBindPoint(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PipelineCacheHeaderVersion {
    eOne,
    eSafetyCriticalOne,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl PipelineCacheHeaderVersion {
    pub fn into_raw(self) -> RawPipelineCacheHeaderVersion {
        match self {
            Self::eOne => RawPipelineCacheHeaderVersion::eOne,
            Self::eSafetyCriticalOne => RawPipelineCacheHeaderVersion::eSafetyCriticalOne,
            Self::eUnknownVariant(v) => RawPipelineCacheHeaderVersion(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PrimitiveTopology {
    ePointList,
    eLineList,
    eLineStrip,
    eTriangleList,
    eTriangleStrip,
    eTriangleFan,
    eLineListWithAdjacency,
    eLineStripWithAdjacency,
    eTriangleListWithAdjacency,
    eTriangleStripWithAdjacency,
    ePatchList,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl PrimitiveTopology {
    pub fn into_raw(self) -> RawPrimitiveTopology {
        match self {
            Self::ePointList => RawPrimitiveTopology::ePointList,
            Self::eLineList => RawPrimitiveTopology::eLineList,
            Self::eLineStrip => RawPrimitiveTopology::eLineStrip,
            Self::eTriangleList => RawPrimitiveTopology::eTriangleList,
            Self::eTriangleStrip => RawPrimitiveTopology::eTriangleStrip,
            Self::eTriangleFan => RawPrimitiveTopology::eTriangleFan,
            Self::eLineListWithAdjacency => RawPrimitiveTopology::eLineListWithAdjacency,
            Self::eLineStripWithAdjacency => RawPrimitiveTopology::eLineStripWithAdjacency,
            Self::eTriangleListWithAdjacency => RawPrimitiveTopology::eTriangleListWithAdjacency,
            Self::eTriangleStripWithAdjacency => RawPrimitiveTopology::eTriangleStripWithAdjacency,
            Self::ePatchList => RawPrimitiveTopology::ePatchList,
            Self::eUnknownVariant(v) => RawPrimitiveTopology(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SharingMode {
    eExclusive,
    eConcurrent,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl SharingMode {
    pub fn into_raw(self) -> RawSharingMode {
        match self {
            Self::eExclusive => RawSharingMode::eExclusive,
            Self::eConcurrent => RawSharingMode::eConcurrent,
            Self::eUnknownVariant(v) => RawSharingMode(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum IndexType {
    eUint16,
    eUint32,
    eNoneKhr,
    eUint8Ext,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl IndexType {
    pub const eNoneNv: Self = Self::eNoneKhr;

    pub fn into_raw(self) -> RawIndexType {
        match self {
            Self::eUint16 => RawIndexType::eUint16,
            Self::eUint32 => RawIndexType::eUint32,
            Self::eNoneKhr => RawIndexType::eNoneKhr,
            Self::eUint8Ext => RawIndexType::eUint8Ext,
            Self::eUnknownVariant(v) => RawIndexType(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum Filter {
    eNearest,
    eLinear,
    eCubicExt,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl Filter {
    pub const eCubicImg: Self = Self::eCubicExt;

    pub fn into_raw(self) -> RawFilter {
        match self {
            Self::eNearest => RawFilter::eNearest,
            Self::eLinear => RawFilter::eLinear,
            Self::eCubicExt => RawFilter::eCubicExt,
            Self::eUnknownVariant(v) => RawFilter(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SamplerMipmapMode {
    eNearest,
    eLinear,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl SamplerMipmapMode {
    pub fn into_raw(self) -> RawSamplerMipmapMode {
        match self {
            Self::eNearest => RawSamplerMipmapMode::eNearest,
            Self::eLinear => RawSamplerMipmapMode::eLinear,
            Self::eUnknownVariant(v) => RawSamplerMipmapMode(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SamplerAddressMode {
    eRepeat,
    eMirroredRepeat,
    eClampToEdge,
    eClampToBorder,
    eMirrorClampToEdge,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl SamplerAddressMode {
    pub const eMirrorClampToEdgeKhr: Self = Self::eMirrorClampToEdge;

    pub fn into_raw(self) -> RawSamplerAddressMode {
        match self {
            Self::eRepeat => RawSamplerAddressMode::eRepeat,
            Self::eMirroredRepeat => RawSamplerAddressMode::eMirroredRepeat,
            Self::eClampToEdge => RawSamplerAddressMode::eClampToEdge,
            Self::eClampToBorder => RawSamplerAddressMode::eClampToBorder,
            Self::eMirrorClampToEdge => RawSamplerAddressMode::eMirrorClampToEdge,
            Self::eUnknownVariant(v) => RawSamplerAddressMode(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum CompareOp {
    eNever,
    eLess,
    eEqual,
    eLessOrEqual,
    eGreater,
    eNotEqual,
    eGreaterOrEqual,
    eAlways,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl CompareOp {
    pub fn into_raw(self) -> RawCompareOp {
        match self {
            Self::eNever => RawCompareOp::eNever,
            Self::eLess => RawCompareOp::eLess,
            Self::eEqual => RawCompareOp::eEqual,
            Self::eLessOrEqual => RawCompareOp::eLessOrEqual,
            Self::eGreater => RawCompareOp::eGreater,
            Self::eNotEqual => RawCompareOp::eNotEqual,
            Self::eGreaterOrEqual => RawCompareOp::eGreaterOrEqual,
            Self::eAlways => RawCompareOp::eAlways,
            Self::eUnknownVariant(v) => RawCompareOp(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PolygonMode {
    eFill,
    eLine,
    ePoint,
    eFillRectangleNv,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl PolygonMode {
    pub fn into_raw(self) -> RawPolygonMode {
        match self {
            Self::eFill => RawPolygonMode::eFill,
            Self::eLine => RawPolygonMode::eLine,
            Self::ePoint => RawPolygonMode::ePoint,
            Self::eFillRectangleNv => RawPolygonMode::eFillRectangleNv,
            Self::eUnknownVariant(v) => RawPolygonMode(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum FrontFace {
    eCounterClockwise,
    eClockwise,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl FrontFace {
    pub fn into_raw(self) -> RawFrontFace {
        match self {
            Self::eCounterClockwise => RawFrontFace::eCounterClockwise,
            Self::eClockwise => RawFrontFace::eClockwise,
            Self::eUnknownVariant(v) => RawFrontFace(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum BlendFactor {
    eZero,
    eOne,
    eSrcColour,
    eOneMinusSrcColour,
    eDstColour,
    eOneMinusDstColour,
    eSrcAlpha,
    eOneMinusSrcAlpha,
    eDstAlpha,
    eOneMinusDstAlpha,
    eConstantColour,
    eOneMinusConstantColour,
    eConstantAlpha,
    eOneMinusConstantAlpha,
    eSrcAlphaSaturate,
    eSrc1Colour,
    eOneMinusSrc1Colour,
    eSrc1Alpha,
    eOneMinusSrc1Alpha,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl BlendFactor {
    pub fn into_raw(self) -> RawBlendFactor {
        match self {
            Self::eZero => RawBlendFactor::eZero,
            Self::eOne => RawBlendFactor::eOne,
            Self::eSrcColour => RawBlendFactor::eSrcColour,
            Self::eOneMinusSrcColour => RawBlendFactor::eOneMinusSrcColour,
            Self::eDstColour => RawBlendFactor::eDstColour,
            Self::eOneMinusDstColour => RawBlendFactor::eOneMinusDstColour,
            Self::eSrcAlpha => RawBlendFactor::eSrcAlpha,
            Self::eOneMinusSrcAlpha => RawBlendFactor::eOneMinusSrcAlpha,
            Self::eDstAlpha => RawBlendFactor::eDstAlpha,
            Self::eOneMinusDstAlpha => RawBlendFactor::eOneMinusDstAlpha,
            Self::eConstantColour => RawBlendFactor::eConstantColour,
            Self::eOneMinusConstantColour => RawBlendFactor::eOneMinusConstantColour,
            Self::eConstantAlpha => RawBlendFactor::eConstantAlpha,
            Self::eOneMinusConstantAlpha => RawBlendFactor::eOneMinusConstantAlpha,
            Self::eSrcAlphaSaturate => RawBlendFactor::eSrcAlphaSaturate,
            Self::eSrc1Colour => RawBlendFactor::eSrc1Colour,
            Self::eOneMinusSrc1Colour => RawBlendFactor::eOneMinusSrc1Colour,
            Self::eSrc1Alpha => RawBlendFactor::eSrc1Alpha,
            Self::eOneMinusSrc1Alpha => RawBlendFactor::eOneMinusSrc1Alpha,
            Self::eUnknownVariant(v) => RawBlendFactor(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum BlendOp {
    eAdd,
    eSubtract,
    eReverseSubtract,
    eMin,
    eMax,
    eZeroExt,
    eSrcExt,
    eDstExt,
    eSrcOverExt,
    eDstOverExt,
    eSrcInExt,
    eDstInExt,
    eSrcOutExt,
    eDstOutExt,
    eSrcAtopExt,
    eDstAtopExt,
    eXorExt,
    eMultiplyExt,
    eScreenExt,
    eOverlayExt,
    eDarkenExt,
    eLightenExt,
    eColourdodgeExt,
    eColourburnExt,
    eHardlightExt,
    eSoftlightExt,
    eDifferenceExt,
    eExclusionExt,
    eInvertExt,
    eInvertRgbExt,
    eLineardodgeExt,
    eLinearburnExt,
    eVividlightExt,
    eLinearlightExt,
    ePinlightExt,
    eHardmixExt,
    eHslHueExt,
    eHslSaturationExt,
    eHslColourExt,
    eHslLuminosityExt,
    ePlusExt,
    ePlusClampedExt,
    ePlusClampedAlphaExt,
    ePlusDarkerExt,
    eMinusExt,
    eMinusClampedExt,
    eContrastExt,
    eInvertOvgExt,
    eRedExt,
    eGreenExt,
    eBlueExt,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl BlendOp {
    pub fn into_raw(self) -> RawBlendOp {
        match self {
            Self::eAdd => RawBlendOp::eAdd,
            Self::eSubtract => RawBlendOp::eSubtract,
            Self::eReverseSubtract => RawBlendOp::eReverseSubtract,
            Self::eMin => RawBlendOp::eMin,
            Self::eMax => RawBlendOp::eMax,
            Self::eZeroExt => RawBlendOp::eZeroExt,
            Self::eSrcExt => RawBlendOp::eSrcExt,
            Self::eDstExt => RawBlendOp::eDstExt,
            Self::eSrcOverExt => RawBlendOp::eSrcOverExt,
            Self::eDstOverExt => RawBlendOp::eDstOverExt,
            Self::eSrcInExt => RawBlendOp::eSrcInExt,
            Self::eDstInExt => RawBlendOp::eDstInExt,
            Self::eSrcOutExt => RawBlendOp::eSrcOutExt,
            Self::eDstOutExt => RawBlendOp::eDstOutExt,
            Self::eSrcAtopExt => RawBlendOp::eSrcAtopExt,
            Self::eDstAtopExt => RawBlendOp::eDstAtopExt,
            Self::eXorExt => RawBlendOp::eXorExt,
            Self::eMultiplyExt => RawBlendOp::eMultiplyExt,
            Self::eScreenExt => RawBlendOp::eScreenExt,
            Self::eOverlayExt => RawBlendOp::eOverlayExt,
            Self::eDarkenExt => RawBlendOp::eDarkenExt,
            Self::eLightenExt => RawBlendOp::eLightenExt,
            Self::eColourdodgeExt => RawBlendOp::eColourdodgeExt,
            Self::eColourburnExt => RawBlendOp::eColourburnExt,
            Self::eHardlightExt => RawBlendOp::eHardlightExt,
            Self::eSoftlightExt => RawBlendOp::eSoftlightExt,
            Self::eDifferenceExt => RawBlendOp::eDifferenceExt,
            Self::eExclusionExt => RawBlendOp::eExclusionExt,
            Self::eInvertExt => RawBlendOp::eInvertExt,
            Self::eInvertRgbExt => RawBlendOp::eInvertRgbExt,
            Self::eLineardodgeExt => RawBlendOp::eLineardodgeExt,
            Self::eLinearburnExt => RawBlendOp::eLinearburnExt,
            Self::eVividlightExt => RawBlendOp::eVividlightExt,
            Self::eLinearlightExt => RawBlendOp::eLinearlightExt,
            Self::ePinlightExt => RawBlendOp::ePinlightExt,
            Self::eHardmixExt => RawBlendOp::eHardmixExt,
            Self::eHslHueExt => RawBlendOp::eHslHueExt,
            Self::eHslSaturationExt => RawBlendOp::eHslSaturationExt,
            Self::eHslColourExt => RawBlendOp::eHslColourExt,
            Self::eHslLuminosityExt => RawBlendOp::eHslLuminosityExt,
            Self::ePlusExt => RawBlendOp::ePlusExt,
            Self::ePlusClampedExt => RawBlendOp::ePlusClampedExt,
            Self::ePlusClampedAlphaExt => RawBlendOp::ePlusClampedAlphaExt,
            Self::ePlusDarkerExt => RawBlendOp::ePlusDarkerExt,
            Self::eMinusExt => RawBlendOp::eMinusExt,
            Self::eMinusClampedExt => RawBlendOp::eMinusClampedExt,
            Self::eContrastExt => RawBlendOp::eContrastExt,
            Self::eInvertOvgExt => RawBlendOp::eInvertOvgExt,
            Self::eRedExt => RawBlendOp::eRedExt,
            Self::eGreenExt => RawBlendOp::eGreenExt,
            Self::eBlueExt => RawBlendOp::eBlueExt,
            Self::eUnknownVariant(v) => RawBlendOp(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StencilOp {
    eKeep,
    eZero,
    eReplace,
    eIncrementAndClamp,
    eDecrementAndClamp,
    eInvert,
    eIncrementAndWrap,
    eDecrementAndWrap,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StencilOp {
    pub fn into_raw(self) -> RawStencilOp {
        match self {
            Self::eKeep => RawStencilOp::eKeep,
            Self::eZero => RawStencilOp::eZero,
            Self::eReplace => RawStencilOp::eReplace,
            Self::eIncrementAndClamp => RawStencilOp::eIncrementAndClamp,
            Self::eDecrementAndClamp => RawStencilOp::eDecrementAndClamp,
            Self::eInvert => RawStencilOp::eInvert,
            Self::eIncrementAndWrap => RawStencilOp::eIncrementAndWrap,
            Self::eDecrementAndWrap => RawStencilOp::eDecrementAndWrap,
            Self::eUnknownVariant(v) => RawStencilOp(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum LogicOp {
    eClear,
    eAnd,
    eAndReverse,
    eCopy,
    eAndInverted,
    eNoOp,
    eXor,
    eOr,
    eNor,
    eEquivalent,
    eInvert,
    eOrReverse,
    eCopyInverted,
    eOrInverted,
    eNand,
    eSet,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl LogicOp {
    pub fn into_raw(self) -> RawLogicOp {
        match self {
            Self::eClear => RawLogicOp::eClear,
            Self::eAnd => RawLogicOp::eAnd,
            Self::eAndReverse => RawLogicOp::eAndReverse,
            Self::eCopy => RawLogicOp::eCopy,
            Self::eAndInverted => RawLogicOp::eAndInverted,
            Self::eNoOp => RawLogicOp::eNoOp,
            Self::eXor => RawLogicOp::eXor,
            Self::eOr => RawLogicOp::eOr,
            Self::eNor => RawLogicOp::eNor,
            Self::eEquivalent => RawLogicOp::eEquivalent,
            Self::eInvert => RawLogicOp::eInvert,
            Self::eOrReverse => RawLogicOp::eOrReverse,
            Self::eCopyInverted => RawLogicOp::eCopyInverted,
            Self::eOrInverted => RawLogicOp::eOrInverted,
            Self::eNand => RawLogicOp::eNand,
            Self::eSet => RawLogicOp::eSet,
            Self::eUnknownVariant(v) => RawLogicOp(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum InternalAllocationType {
    eExecutable,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl InternalAllocationType {
    pub fn into_raw(self) -> RawInternalAllocationType {
        match self {
            Self::eExecutable => RawInternalAllocationType::eExecutable,
            Self::eUnknownVariant(v) => RawInternalAllocationType(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SystemAllocationScope {
    eCommand,
    eObject,
    eCache,
    eDevice,
    eInstance,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl SystemAllocationScope {
    pub fn into_raw(self) -> RawSystemAllocationScope {
        match self {
            Self::eCommand => RawSystemAllocationScope::eCommand,
            Self::eObject => RawSystemAllocationScope::eObject,
            Self::eCache => RawSystemAllocationScope::eCache,
            Self::eDevice => RawSystemAllocationScope::eDevice,
            Self::eInstance => RawSystemAllocationScope::eInstance,
            Self::eUnknownVariant(v) => RawSystemAllocationScope(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PhysicalDeviceType {
    eOther,
    eIntegratedGpu,
    eDiscreteGpu,
    eVirtualGpu,
    eCpu,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl PhysicalDeviceType {
    pub fn into_raw(self) -> RawPhysicalDeviceType {
        match self {
            Self::eOther => RawPhysicalDeviceType::eOther,
            Self::eIntegratedGpu => RawPhysicalDeviceType::eIntegratedGpu,
            Self::eDiscreteGpu => RawPhysicalDeviceType::eDiscreteGpu,
            Self::eVirtualGpu => RawPhysicalDeviceType::eVirtualGpu,
            Self::eCpu => RawPhysicalDeviceType::eCpu,
            Self::eUnknownVariant(v) => RawPhysicalDeviceType(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum VertexInputRate {
    eVertex,
    eInstance,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl VertexInputRate {
    pub fn into_raw(self) -> RawVertexInputRate {
        match self {
            Self::eVertex => RawVertexInputRate::eVertex,
            Self::eInstance => RawVertexInputRate::eInstance,
            Self::eUnknownVariant(v) => RawVertexInputRate(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum Format {
    eUndefined,
    eR4G4UnormPack8,
    eR4G4B4A4UnormPack16,
    eB4G4R4A4UnormPack16,
    eR5G6B5UnormPack16,
    eB5G6R5UnormPack16,
    eR5G5B5A1UnormPack16,
    eB5G5R5A1UnormPack16,
    eA1R5G5B5UnormPack16,
    eR8Unorm,
    eR8Snorm,
    eR8Uscaled,
    eR8Sscaled,
    eR8Uint,
    eR8Sint,
    eR8Srgb,
    eR8G8Unorm,
    eR8G8Snorm,
    eR8G8Uscaled,
    eR8G8Sscaled,
    eR8G8Uint,
    eR8G8Sint,
    eR8G8Srgb,
    eR8G8B8Unorm,
    eR8G8B8Snorm,
    eR8G8B8Uscaled,
    eR8G8B8Sscaled,
    eR8G8B8Uint,
    eR8G8B8Sint,
    eR8G8B8Srgb,
    eB8G8R8Unorm,
    eB8G8R8Snorm,
    eB8G8R8Uscaled,
    eB8G8R8Sscaled,
    eB8G8R8Uint,
    eB8G8R8Sint,
    eB8G8R8Srgb,
    eR8G8B8A8Unorm,
    eR8G8B8A8Snorm,
    eR8G8B8A8Uscaled,
    eR8G8B8A8Sscaled,
    eR8G8B8A8Uint,
    eR8G8B8A8Sint,
    eR8G8B8A8Srgb,
    eB8G8R8A8Unorm,
    eB8G8R8A8Snorm,
    eB8G8R8A8Uscaled,
    eB8G8R8A8Sscaled,
    eB8G8R8A8Uint,
    eB8G8R8A8Sint,
    eB8G8R8A8Srgb,
    eA8B8G8R8UnormPack32,
    eA8B8G8R8SnormPack32,
    eA8B8G8R8UscaledPack32,
    eA8B8G8R8SscaledPack32,
    eA8B8G8R8UintPack32,
    eA8B8G8R8SintPack32,
    eA8B8G8R8SrgbPack32,
    eA2R10G10B10UnormPack32,
    eA2R10G10B10SnormPack32,
    eA2R10G10B10UscaledPack32,
    eA2R10G10B10SscaledPack32,
    eA2R10G10B10UintPack32,
    eA2R10G10B10SintPack32,
    eA2B10G10R10UnormPack32,
    eA2B10G10R10SnormPack32,
    eA2B10G10R10UscaledPack32,
    eA2B10G10R10SscaledPack32,
    eA2B10G10R10UintPack32,
    eA2B10G10R10SintPack32,
    eR16Unorm,
    eR16Snorm,
    eR16Uscaled,
    eR16Sscaled,
    eR16Uint,
    eR16Sint,
    eR16Sfloat,
    eR16G16Unorm,
    eR16G16Snorm,
    eR16G16Uscaled,
    eR16G16Sscaled,
    eR16G16Uint,
    eR16G16Sint,
    eR16G16Sfloat,
    eR16G16B16Unorm,
    eR16G16B16Snorm,
    eR16G16B16Uscaled,
    eR16G16B16Sscaled,
    eR16G16B16Uint,
    eR16G16B16Sint,
    eR16G16B16Sfloat,
    eR16G16B16A16Unorm,
    eR16G16B16A16Snorm,
    eR16G16B16A16Uscaled,
    eR16G16B16A16Sscaled,
    eR16G16B16A16Uint,
    eR16G16B16A16Sint,
    eR16G16B16A16Sfloat,
    eR32Uint,
    eR32Sint,
    eR32Sfloat,
    eR32G32Uint,
    eR32G32Sint,
    eR32G32Sfloat,
    eR32G32B32Uint,
    eR32G32B32Sint,
    eR32G32B32Sfloat,
    eR32G32B32A32Uint,
    eR32G32B32A32Sint,
    eR32G32B32A32Sfloat,
    eR64Uint,
    eR64Sint,
    eR64Sfloat,
    eR64G64Uint,
    eR64G64Sint,
    eR64G64Sfloat,
    eR64G64B64Uint,
    eR64G64B64Sint,
    eR64G64B64Sfloat,
    eR64G64B64A64Uint,
    eR64G64B64A64Sint,
    eR64G64B64A64Sfloat,
    eB10G11R11UfloatPack32,
    eE5B9G9R9UfloatPack32,
    eD16Unorm,
    eX8D24UnormPack32,
    eD32Sfloat,
    eS8Uint,
    eD16UnormS8Uint,
    eD24UnormS8Uint,
    eD32SfloatS8Uint,
    eBc1RgbUnormBlock,
    eBc1RgbSrgbBlock,
    eBc1RgbaUnormBlock,
    eBc1RgbaSrgbBlock,
    eBc2UnormBlock,
    eBc2SrgbBlock,
    eBc3UnormBlock,
    eBc3SrgbBlock,
    eBc4UnormBlock,
    eBc4SnormBlock,
    eBc5UnormBlock,
    eBc5SnormBlock,
    eBc6hUfloatBlock,
    eBc6hSfloatBlock,
    eBc7UnormBlock,
    eBc7SrgbBlock,
    eEtc2R8G8B8UnormBlock,
    eEtc2R8G8B8SrgbBlock,
    eEtc2R8G8B8A1UnormBlock,
    eEtc2R8G8B8A1SrgbBlock,
    eEtc2R8G8B8A8UnormBlock,
    eEtc2R8G8B8A8SrgbBlock,
    eEacR11UnormBlock,
    eEacR11SnormBlock,
    eEacR11G11UnormBlock,
    eEacR11G11SnormBlock,
    eAstc4X4UnormBlock,
    eAstc4X4SrgbBlock,
    eAstc5X4UnormBlock,
    eAstc5X4SrgbBlock,
    eAstc5X5UnormBlock,
    eAstc5X5SrgbBlock,
    eAstc6X5UnormBlock,
    eAstc6X5SrgbBlock,
    eAstc6X6UnormBlock,
    eAstc6X6SrgbBlock,
    eAstc8X5UnormBlock,
    eAstc8X5SrgbBlock,
    eAstc8X6UnormBlock,
    eAstc8X6SrgbBlock,
    eAstc8X8UnormBlock,
    eAstc8X8SrgbBlock,
    eAstc10X5UnormBlock,
    eAstc10X5SrgbBlock,
    eAstc10X6UnormBlock,
    eAstc10X6SrgbBlock,
    eAstc10X8UnormBlock,
    eAstc10X8SrgbBlock,
    eAstc10X10UnormBlock,
    eAstc10X10SrgbBlock,
    eAstc12X10UnormBlock,
    eAstc12X10SrgbBlock,
    eAstc12X12UnormBlock,
    eAstc12X12SrgbBlock,
    ePvrtc12bppUnormBlockImg,
    ePvrtc14bppUnormBlockImg,
    ePvrtc22bppUnormBlockImg,
    ePvrtc24bppUnormBlockImg,
    ePvrtc12bppSrgbBlockImg,
    ePvrtc14bppSrgbBlockImg,
    ePvrtc22bppSrgbBlockImg,
    ePvrtc24bppSrgbBlockImg,
    eAstc4X4SfloatBlock,
    eAstc5X4SfloatBlock,
    eAstc5X5SfloatBlock,
    eAstc6X5SfloatBlock,
    eAstc6X6SfloatBlock,
    eAstc8X5SfloatBlock,
    eAstc8X6SfloatBlock,
    eAstc8X8SfloatBlock,
    eAstc10X5SfloatBlock,
    eAstc10X6SfloatBlock,
    eAstc10X8SfloatBlock,
    eAstc10X10SfloatBlock,
    eAstc12X10SfloatBlock,
    eAstc12X12SfloatBlock,
    eG8B8G8R8422Unorm,
    eB8G8R8G8422Unorm,
    eG8B8R83plane420Unorm,
    eG8B8R82plane420Unorm,
    eG8B8R83plane422Unorm,
    eG8B8R82plane422Unorm,
    eG8B8R83plane444Unorm,
    eR10X6UnormPack16,
    eR10X6G10X6Unorm2pack16,
    eR10X6G10X6B10X6A10X6Unorm4pack16,
    eG10X6B10X6G10X6R10X6422Unorm4pack16,
    eB10X6G10X6R10X6G10X6422Unorm4pack16,
    eG10X6B10X6R10X63plane420Unorm3pack16,
    eG10X6B10X6R10X62plane420Unorm3pack16,
    eG10X6B10X6R10X63plane422Unorm3pack16,
    eG10X6B10X6R10X62plane422Unorm3pack16,
    eG10X6B10X6R10X63plane444Unorm3pack16,
    eR12X4UnormPack16,
    eR12X4G12X4Unorm2pack16,
    eR12X4G12X4B12X4A12X4Unorm4pack16,
    eG12X4B12X4G12X4R12X4422Unorm4pack16,
    eB12X4G12X4R12X4G12X4422Unorm4pack16,
    eG12X4B12X4R12X43plane420Unorm3pack16,
    eG12X4B12X4R12X42plane420Unorm3pack16,
    eG12X4B12X4R12X43plane422Unorm3pack16,
    eG12X4B12X4R12X42plane422Unorm3pack16,
    eG12X4B12X4R12X43plane444Unorm3pack16,
    eG16B16G16R16422Unorm,
    eB16G16R16G16422Unorm,
    eG16B16R163plane420Unorm,
    eG16B16R162plane420Unorm,
    eG16B16R163plane422Unorm,
    eG16B16R162plane422Unorm,
    eG16B16R163plane444Unorm,
    eG8B8R82plane444Unorm,
    eG10X6B10X6R10X62plane444Unorm3pack16,
    eG12X4B12X4R12X42plane444Unorm3pack16,
    eG16B16R162plane444Unorm,
    eA4R4G4B4UnormPack16,
    eA4B4G4R4UnormPack16,
    eR16G16S105Nv,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl Format {
    pub const eA4B4G4R4UnormPack16Ext: Self = Self::eA4B4G4R4UnormPack16;
    pub const eA4R4G4B4UnormPack16Ext: Self = Self::eA4R4G4B4UnormPack16;
    pub const eAstc10X10SfloatBlockExt: Self = Self::eAstc10X10SfloatBlock;
    pub const eAstc10X5SfloatBlockExt: Self = Self::eAstc10X5SfloatBlock;
    pub const eAstc10X6SfloatBlockExt: Self = Self::eAstc10X6SfloatBlock;
    pub const eAstc10X8SfloatBlockExt: Self = Self::eAstc10X8SfloatBlock;
    pub const eAstc12X10SfloatBlockExt: Self = Self::eAstc12X10SfloatBlock;
    pub const eAstc12X12SfloatBlockExt: Self = Self::eAstc12X12SfloatBlock;
    pub const eAstc4X4SfloatBlockExt: Self = Self::eAstc4X4SfloatBlock;
    pub const eAstc5X4SfloatBlockExt: Self = Self::eAstc5X4SfloatBlock;
    pub const eAstc5X5SfloatBlockExt: Self = Self::eAstc5X5SfloatBlock;
    pub const eAstc6X5SfloatBlockExt: Self = Self::eAstc6X5SfloatBlock;
    pub const eAstc6X6SfloatBlockExt: Self = Self::eAstc6X6SfloatBlock;
    pub const eAstc8X5SfloatBlockExt: Self = Self::eAstc8X5SfloatBlock;
    pub const eAstc8X6SfloatBlockExt: Self = Self::eAstc8X6SfloatBlock;
    pub const eAstc8X8SfloatBlockExt: Self = Self::eAstc8X8SfloatBlock;
    pub const eB10X6G10X6R10X6G10X6422Unorm4pack16Khr: Self = Self::eB10X6G10X6R10X6G10X6422Unorm4pack16;
    pub const eB12X4G12X4R12X4G12X4422Unorm4pack16Khr: Self = Self::eB12X4G12X4R12X4G12X4422Unorm4pack16;
    pub const eB16G16R16G16422UnormKhr: Self = Self::eB16G16R16G16422Unorm;
    pub const eB8G8R8G8422UnormKhr: Self = Self::eB8G8R8G8422Unorm;
    pub const eG10X6B10X6G10X6R10X6422Unorm4pack16Khr: Self = Self::eG10X6B10X6G10X6R10X6422Unorm4pack16;
    pub const eG10X6B10X6R10X62plane420Unorm3pack16Khr: Self = Self::eG10X6B10X6R10X62plane420Unorm3pack16;
    pub const eG10X6B10X6R10X62plane422Unorm3pack16Khr: Self = Self::eG10X6B10X6R10X62plane422Unorm3pack16;
    pub const eG10X6B10X6R10X62plane444Unorm3pack16Ext: Self = Self::eG10X6B10X6R10X62plane444Unorm3pack16;
    pub const eG10X6B10X6R10X63plane420Unorm3pack16Khr: Self = Self::eG10X6B10X6R10X63plane420Unorm3pack16;
    pub const eG10X6B10X6R10X63plane422Unorm3pack16Khr: Self = Self::eG10X6B10X6R10X63plane422Unorm3pack16;
    pub const eG10X6B10X6R10X63plane444Unorm3pack16Khr: Self = Self::eG10X6B10X6R10X63plane444Unorm3pack16;
    pub const eG12X4B12X4G12X4R12X4422Unorm4pack16Khr: Self = Self::eG12X4B12X4G12X4R12X4422Unorm4pack16;
    pub const eG12X4B12X4R12X42plane420Unorm3pack16Khr: Self = Self::eG12X4B12X4R12X42plane420Unorm3pack16;
    pub const eG12X4B12X4R12X42plane422Unorm3pack16Khr: Self = Self::eG12X4B12X4R12X42plane422Unorm3pack16;
    pub const eG12X4B12X4R12X42plane444Unorm3pack16Ext: Self = Self::eG12X4B12X4R12X42plane444Unorm3pack16;
    pub const eG12X4B12X4R12X43plane420Unorm3pack16Khr: Self = Self::eG12X4B12X4R12X43plane420Unorm3pack16;
    pub const eG12X4B12X4R12X43plane422Unorm3pack16Khr: Self = Self::eG12X4B12X4R12X43plane422Unorm3pack16;
    pub const eG12X4B12X4R12X43plane444Unorm3pack16Khr: Self = Self::eG12X4B12X4R12X43plane444Unorm3pack16;
    pub const eG16B16G16R16422UnormKhr: Self = Self::eG16B16G16R16422Unorm;
    pub const eG16B16R162plane420UnormKhr: Self = Self::eG16B16R162plane420Unorm;
    pub const eG16B16R162plane422UnormKhr: Self = Self::eG16B16R162plane422Unorm;
    pub const eG16B16R162plane444UnormExt: Self = Self::eG16B16R162plane444Unorm;
    pub const eG16B16R163plane420UnormKhr: Self = Self::eG16B16R163plane420Unorm;
    pub const eG16B16R163plane422UnormKhr: Self = Self::eG16B16R163plane422Unorm;
    pub const eG16B16R163plane444UnormKhr: Self = Self::eG16B16R163plane444Unorm;
    pub const eG8B8G8R8422UnormKhr: Self = Self::eG8B8G8R8422Unorm;
    pub const eG8B8R82plane420UnormKhr: Self = Self::eG8B8R82plane420Unorm;
    pub const eG8B8R82plane422UnormKhr: Self = Self::eG8B8R82plane422Unorm;
    pub const eG8B8R82plane444UnormExt: Self = Self::eG8B8R82plane444Unorm;
    pub const eG8B8R83plane420UnormKhr: Self = Self::eG8B8R83plane420Unorm;
    pub const eG8B8R83plane422UnormKhr: Self = Self::eG8B8R83plane422Unorm;
    pub const eG8B8R83plane444UnormKhr: Self = Self::eG8B8R83plane444Unorm;
    pub const eR10X6G10X6B10X6A10X6Unorm4pack16Khr: Self = Self::eR10X6G10X6B10X6A10X6Unorm4pack16;
    pub const eR10X6G10X6Unorm2pack16Khr: Self = Self::eR10X6G10X6Unorm2pack16;
    pub const eR10X6UnormPack16Khr: Self = Self::eR10X6UnormPack16;
    pub const eR12X4G12X4B12X4A12X4Unorm4pack16Khr: Self = Self::eR12X4G12X4B12X4A12X4Unorm4pack16;
    pub const eR12X4G12X4Unorm2pack16Khr: Self = Self::eR12X4G12X4Unorm2pack16;
    pub const eR12X4UnormPack16Khr: Self = Self::eR12X4UnormPack16;

    pub fn into_raw(self) -> RawFormat {
        match self {
            Self::eUndefined => RawFormat::eUndefined,
            Self::eR4G4UnormPack8 => RawFormat::eR4G4UnormPack8,
            Self::eR4G4B4A4UnormPack16 => RawFormat::eR4G4B4A4UnormPack16,
            Self::eB4G4R4A4UnormPack16 => RawFormat::eB4G4R4A4UnormPack16,
            Self::eR5G6B5UnormPack16 => RawFormat::eR5G6B5UnormPack16,
            Self::eB5G6R5UnormPack16 => RawFormat::eB5G6R5UnormPack16,
            Self::eR5G5B5A1UnormPack16 => RawFormat::eR5G5B5A1UnormPack16,
            Self::eB5G5R5A1UnormPack16 => RawFormat::eB5G5R5A1UnormPack16,
            Self::eA1R5G5B5UnormPack16 => RawFormat::eA1R5G5B5UnormPack16,
            Self::eR8Unorm => RawFormat::eR8Unorm,
            Self::eR8Snorm => RawFormat::eR8Snorm,
            Self::eR8Uscaled => RawFormat::eR8Uscaled,
            Self::eR8Sscaled => RawFormat::eR8Sscaled,
            Self::eR8Uint => RawFormat::eR8Uint,
            Self::eR8Sint => RawFormat::eR8Sint,
            Self::eR8Srgb => RawFormat::eR8Srgb,
            Self::eR8G8Unorm => RawFormat::eR8G8Unorm,
            Self::eR8G8Snorm => RawFormat::eR8G8Snorm,
            Self::eR8G8Uscaled => RawFormat::eR8G8Uscaled,
            Self::eR8G8Sscaled => RawFormat::eR8G8Sscaled,
            Self::eR8G8Uint => RawFormat::eR8G8Uint,
            Self::eR8G8Sint => RawFormat::eR8G8Sint,
            Self::eR8G8Srgb => RawFormat::eR8G8Srgb,
            Self::eR8G8B8Unorm => RawFormat::eR8G8B8Unorm,
            Self::eR8G8B8Snorm => RawFormat::eR8G8B8Snorm,
            Self::eR8G8B8Uscaled => RawFormat::eR8G8B8Uscaled,
            Self::eR8G8B8Sscaled => RawFormat::eR8G8B8Sscaled,
            Self::eR8G8B8Uint => RawFormat::eR8G8B8Uint,
            Self::eR8G8B8Sint => RawFormat::eR8G8B8Sint,
            Self::eR8G8B8Srgb => RawFormat::eR8G8B8Srgb,
            Self::eB8G8R8Unorm => RawFormat::eB8G8R8Unorm,
            Self::eB8G8R8Snorm => RawFormat::eB8G8R8Snorm,
            Self::eB8G8R8Uscaled => RawFormat::eB8G8R8Uscaled,
            Self::eB8G8R8Sscaled => RawFormat::eB8G8R8Sscaled,
            Self::eB8G8R8Uint => RawFormat::eB8G8R8Uint,
            Self::eB8G8R8Sint => RawFormat::eB8G8R8Sint,
            Self::eB8G8R8Srgb => RawFormat::eB8G8R8Srgb,
            Self::eR8G8B8A8Unorm => RawFormat::eR8G8B8A8Unorm,
            Self::eR8G8B8A8Snorm => RawFormat::eR8G8B8A8Snorm,
            Self::eR8G8B8A8Uscaled => RawFormat::eR8G8B8A8Uscaled,
            Self::eR8G8B8A8Sscaled => RawFormat::eR8G8B8A8Sscaled,
            Self::eR8G8B8A8Uint => RawFormat::eR8G8B8A8Uint,
            Self::eR8G8B8A8Sint => RawFormat::eR8G8B8A8Sint,
            Self::eR8G8B8A8Srgb => RawFormat::eR8G8B8A8Srgb,
            Self::eB8G8R8A8Unorm => RawFormat::eB8G8R8A8Unorm,
            Self::eB8G8R8A8Snorm => RawFormat::eB8G8R8A8Snorm,
            Self::eB8G8R8A8Uscaled => RawFormat::eB8G8R8A8Uscaled,
            Self::eB8G8R8A8Sscaled => RawFormat::eB8G8R8A8Sscaled,
            Self::eB8G8R8A8Uint => RawFormat::eB8G8R8A8Uint,
            Self::eB8G8R8A8Sint => RawFormat::eB8G8R8A8Sint,
            Self::eB8G8R8A8Srgb => RawFormat::eB8G8R8A8Srgb,
            Self::eA8B8G8R8UnormPack32 => RawFormat::eA8B8G8R8UnormPack32,
            Self::eA8B8G8R8SnormPack32 => RawFormat::eA8B8G8R8SnormPack32,
            Self::eA8B8G8R8UscaledPack32 => RawFormat::eA8B8G8R8UscaledPack32,
            Self::eA8B8G8R8SscaledPack32 => RawFormat::eA8B8G8R8SscaledPack32,
            Self::eA8B8G8R8UintPack32 => RawFormat::eA8B8G8R8UintPack32,
            Self::eA8B8G8R8SintPack32 => RawFormat::eA8B8G8R8SintPack32,
            Self::eA8B8G8R8SrgbPack32 => RawFormat::eA8B8G8R8SrgbPack32,
            Self::eA2R10G10B10UnormPack32 => RawFormat::eA2R10G10B10UnormPack32,
            Self::eA2R10G10B10SnormPack32 => RawFormat::eA2R10G10B10SnormPack32,
            Self::eA2R10G10B10UscaledPack32 => RawFormat::eA2R10G10B10UscaledPack32,
            Self::eA2R10G10B10SscaledPack32 => RawFormat::eA2R10G10B10SscaledPack32,
            Self::eA2R10G10B10UintPack32 => RawFormat::eA2R10G10B10UintPack32,
            Self::eA2R10G10B10SintPack32 => RawFormat::eA2R10G10B10SintPack32,
            Self::eA2B10G10R10UnormPack32 => RawFormat::eA2B10G10R10UnormPack32,
            Self::eA2B10G10R10SnormPack32 => RawFormat::eA2B10G10R10SnormPack32,
            Self::eA2B10G10R10UscaledPack32 => RawFormat::eA2B10G10R10UscaledPack32,
            Self::eA2B10G10R10SscaledPack32 => RawFormat::eA2B10G10R10SscaledPack32,
            Self::eA2B10G10R10UintPack32 => RawFormat::eA2B10G10R10UintPack32,
            Self::eA2B10G10R10SintPack32 => RawFormat::eA2B10G10R10SintPack32,
            Self::eR16Unorm => RawFormat::eR16Unorm,
            Self::eR16Snorm => RawFormat::eR16Snorm,
            Self::eR16Uscaled => RawFormat::eR16Uscaled,
            Self::eR16Sscaled => RawFormat::eR16Sscaled,
            Self::eR16Uint => RawFormat::eR16Uint,
            Self::eR16Sint => RawFormat::eR16Sint,
            Self::eR16Sfloat => RawFormat::eR16Sfloat,
            Self::eR16G16Unorm => RawFormat::eR16G16Unorm,
            Self::eR16G16Snorm => RawFormat::eR16G16Snorm,
            Self::eR16G16Uscaled => RawFormat::eR16G16Uscaled,
            Self::eR16G16Sscaled => RawFormat::eR16G16Sscaled,
            Self::eR16G16Uint => RawFormat::eR16G16Uint,
            Self::eR16G16Sint => RawFormat::eR16G16Sint,
            Self::eR16G16Sfloat => RawFormat::eR16G16Sfloat,
            Self::eR16G16B16Unorm => RawFormat::eR16G16B16Unorm,
            Self::eR16G16B16Snorm => RawFormat::eR16G16B16Snorm,
            Self::eR16G16B16Uscaled => RawFormat::eR16G16B16Uscaled,
            Self::eR16G16B16Sscaled => RawFormat::eR16G16B16Sscaled,
            Self::eR16G16B16Uint => RawFormat::eR16G16B16Uint,
            Self::eR16G16B16Sint => RawFormat::eR16G16B16Sint,
            Self::eR16G16B16Sfloat => RawFormat::eR16G16B16Sfloat,
            Self::eR16G16B16A16Unorm => RawFormat::eR16G16B16A16Unorm,
            Self::eR16G16B16A16Snorm => RawFormat::eR16G16B16A16Snorm,
            Self::eR16G16B16A16Uscaled => RawFormat::eR16G16B16A16Uscaled,
            Self::eR16G16B16A16Sscaled => RawFormat::eR16G16B16A16Sscaled,
            Self::eR16G16B16A16Uint => RawFormat::eR16G16B16A16Uint,
            Self::eR16G16B16A16Sint => RawFormat::eR16G16B16A16Sint,
            Self::eR16G16B16A16Sfloat => RawFormat::eR16G16B16A16Sfloat,
            Self::eR32Uint => RawFormat::eR32Uint,
            Self::eR32Sint => RawFormat::eR32Sint,
            Self::eR32Sfloat => RawFormat::eR32Sfloat,
            Self::eR32G32Uint => RawFormat::eR32G32Uint,
            Self::eR32G32Sint => RawFormat::eR32G32Sint,
            Self::eR32G32Sfloat => RawFormat::eR32G32Sfloat,
            Self::eR32G32B32Uint => RawFormat::eR32G32B32Uint,
            Self::eR32G32B32Sint => RawFormat::eR32G32B32Sint,
            Self::eR32G32B32Sfloat => RawFormat::eR32G32B32Sfloat,
            Self::eR32G32B32A32Uint => RawFormat::eR32G32B32A32Uint,
            Self::eR32G32B32A32Sint => RawFormat::eR32G32B32A32Sint,
            Self::eR32G32B32A32Sfloat => RawFormat::eR32G32B32A32Sfloat,
            Self::eR64Uint => RawFormat::eR64Uint,
            Self::eR64Sint => RawFormat::eR64Sint,
            Self::eR64Sfloat => RawFormat::eR64Sfloat,
            Self::eR64G64Uint => RawFormat::eR64G64Uint,
            Self::eR64G64Sint => RawFormat::eR64G64Sint,
            Self::eR64G64Sfloat => RawFormat::eR64G64Sfloat,
            Self::eR64G64B64Uint => RawFormat::eR64G64B64Uint,
            Self::eR64G64B64Sint => RawFormat::eR64G64B64Sint,
            Self::eR64G64B64Sfloat => RawFormat::eR64G64B64Sfloat,
            Self::eR64G64B64A64Uint => RawFormat::eR64G64B64A64Uint,
            Self::eR64G64B64A64Sint => RawFormat::eR64G64B64A64Sint,
            Self::eR64G64B64A64Sfloat => RawFormat::eR64G64B64A64Sfloat,
            Self::eB10G11R11UfloatPack32 => RawFormat::eB10G11R11UfloatPack32,
            Self::eE5B9G9R9UfloatPack32 => RawFormat::eE5B9G9R9UfloatPack32,
            Self::eD16Unorm => RawFormat::eD16Unorm,
            Self::eX8D24UnormPack32 => RawFormat::eX8D24UnormPack32,
            Self::eD32Sfloat => RawFormat::eD32Sfloat,
            Self::eS8Uint => RawFormat::eS8Uint,
            Self::eD16UnormS8Uint => RawFormat::eD16UnormS8Uint,
            Self::eD24UnormS8Uint => RawFormat::eD24UnormS8Uint,
            Self::eD32SfloatS8Uint => RawFormat::eD32SfloatS8Uint,
            Self::eBc1RgbUnormBlock => RawFormat::eBc1RgbUnormBlock,
            Self::eBc1RgbSrgbBlock => RawFormat::eBc1RgbSrgbBlock,
            Self::eBc1RgbaUnormBlock => RawFormat::eBc1RgbaUnormBlock,
            Self::eBc1RgbaSrgbBlock => RawFormat::eBc1RgbaSrgbBlock,
            Self::eBc2UnormBlock => RawFormat::eBc2UnormBlock,
            Self::eBc2SrgbBlock => RawFormat::eBc2SrgbBlock,
            Self::eBc3UnormBlock => RawFormat::eBc3UnormBlock,
            Self::eBc3SrgbBlock => RawFormat::eBc3SrgbBlock,
            Self::eBc4UnormBlock => RawFormat::eBc4UnormBlock,
            Self::eBc4SnormBlock => RawFormat::eBc4SnormBlock,
            Self::eBc5UnormBlock => RawFormat::eBc5UnormBlock,
            Self::eBc5SnormBlock => RawFormat::eBc5SnormBlock,
            Self::eBc6hUfloatBlock => RawFormat::eBc6hUfloatBlock,
            Self::eBc6hSfloatBlock => RawFormat::eBc6hSfloatBlock,
            Self::eBc7UnormBlock => RawFormat::eBc7UnormBlock,
            Self::eBc7SrgbBlock => RawFormat::eBc7SrgbBlock,
            Self::eEtc2R8G8B8UnormBlock => RawFormat::eEtc2R8G8B8UnormBlock,
            Self::eEtc2R8G8B8SrgbBlock => RawFormat::eEtc2R8G8B8SrgbBlock,
            Self::eEtc2R8G8B8A1UnormBlock => RawFormat::eEtc2R8G8B8A1UnormBlock,
            Self::eEtc2R8G8B8A1SrgbBlock => RawFormat::eEtc2R8G8B8A1SrgbBlock,
            Self::eEtc2R8G8B8A8UnormBlock => RawFormat::eEtc2R8G8B8A8UnormBlock,
            Self::eEtc2R8G8B8A8SrgbBlock => RawFormat::eEtc2R8G8B8A8SrgbBlock,
            Self::eEacR11UnormBlock => RawFormat::eEacR11UnormBlock,
            Self::eEacR11SnormBlock => RawFormat::eEacR11SnormBlock,
            Self::eEacR11G11UnormBlock => RawFormat::eEacR11G11UnormBlock,
            Self::eEacR11G11SnormBlock => RawFormat::eEacR11G11SnormBlock,
            Self::eAstc4X4UnormBlock => RawFormat::eAstc4X4UnormBlock,
            Self::eAstc4X4SrgbBlock => RawFormat::eAstc4X4SrgbBlock,
            Self::eAstc5X4UnormBlock => RawFormat::eAstc5X4UnormBlock,
            Self::eAstc5X4SrgbBlock => RawFormat::eAstc5X4SrgbBlock,
            Self::eAstc5X5UnormBlock => RawFormat::eAstc5X5UnormBlock,
            Self::eAstc5X5SrgbBlock => RawFormat::eAstc5X5SrgbBlock,
            Self::eAstc6X5UnormBlock => RawFormat::eAstc6X5UnormBlock,
            Self::eAstc6X5SrgbBlock => RawFormat::eAstc6X5SrgbBlock,
            Self::eAstc6X6UnormBlock => RawFormat::eAstc6X6UnormBlock,
            Self::eAstc6X6SrgbBlock => RawFormat::eAstc6X6SrgbBlock,
            Self::eAstc8X5UnormBlock => RawFormat::eAstc8X5UnormBlock,
            Self::eAstc8X5SrgbBlock => RawFormat::eAstc8X5SrgbBlock,
            Self::eAstc8X6UnormBlock => RawFormat::eAstc8X6UnormBlock,
            Self::eAstc8X6SrgbBlock => RawFormat::eAstc8X6SrgbBlock,
            Self::eAstc8X8UnormBlock => RawFormat::eAstc8X8UnormBlock,
            Self::eAstc8X8SrgbBlock => RawFormat::eAstc8X8SrgbBlock,
            Self::eAstc10X5UnormBlock => RawFormat::eAstc10X5UnormBlock,
            Self::eAstc10X5SrgbBlock => RawFormat::eAstc10X5SrgbBlock,
            Self::eAstc10X6UnormBlock => RawFormat::eAstc10X6UnormBlock,
            Self::eAstc10X6SrgbBlock => RawFormat::eAstc10X6SrgbBlock,
            Self::eAstc10X8UnormBlock => RawFormat::eAstc10X8UnormBlock,
            Self::eAstc10X8SrgbBlock => RawFormat::eAstc10X8SrgbBlock,
            Self::eAstc10X10UnormBlock => RawFormat::eAstc10X10UnormBlock,
            Self::eAstc10X10SrgbBlock => RawFormat::eAstc10X10SrgbBlock,
            Self::eAstc12X10UnormBlock => RawFormat::eAstc12X10UnormBlock,
            Self::eAstc12X10SrgbBlock => RawFormat::eAstc12X10SrgbBlock,
            Self::eAstc12X12UnormBlock => RawFormat::eAstc12X12UnormBlock,
            Self::eAstc12X12SrgbBlock => RawFormat::eAstc12X12SrgbBlock,
            Self::ePvrtc12bppUnormBlockImg => RawFormat::ePvrtc12bppUnormBlockImg,
            Self::ePvrtc14bppUnormBlockImg => RawFormat::ePvrtc14bppUnormBlockImg,
            Self::ePvrtc22bppUnormBlockImg => RawFormat::ePvrtc22bppUnormBlockImg,
            Self::ePvrtc24bppUnormBlockImg => RawFormat::ePvrtc24bppUnormBlockImg,
            Self::ePvrtc12bppSrgbBlockImg => RawFormat::ePvrtc12bppSrgbBlockImg,
            Self::ePvrtc14bppSrgbBlockImg => RawFormat::ePvrtc14bppSrgbBlockImg,
            Self::ePvrtc22bppSrgbBlockImg => RawFormat::ePvrtc22bppSrgbBlockImg,
            Self::ePvrtc24bppSrgbBlockImg => RawFormat::ePvrtc24bppSrgbBlockImg,
            Self::eAstc4X4SfloatBlock => RawFormat::eAstc4X4SfloatBlock,
            Self::eAstc5X4SfloatBlock => RawFormat::eAstc5X4SfloatBlock,
            Self::eAstc5X5SfloatBlock => RawFormat::eAstc5X5SfloatBlock,
            Self::eAstc6X5SfloatBlock => RawFormat::eAstc6X5SfloatBlock,
            Self::eAstc6X6SfloatBlock => RawFormat::eAstc6X6SfloatBlock,
            Self::eAstc8X5SfloatBlock => RawFormat::eAstc8X5SfloatBlock,
            Self::eAstc8X6SfloatBlock => RawFormat::eAstc8X6SfloatBlock,
            Self::eAstc8X8SfloatBlock => RawFormat::eAstc8X8SfloatBlock,
            Self::eAstc10X5SfloatBlock => RawFormat::eAstc10X5SfloatBlock,
            Self::eAstc10X6SfloatBlock => RawFormat::eAstc10X6SfloatBlock,
            Self::eAstc10X8SfloatBlock => RawFormat::eAstc10X8SfloatBlock,
            Self::eAstc10X10SfloatBlock => RawFormat::eAstc10X10SfloatBlock,
            Self::eAstc12X10SfloatBlock => RawFormat::eAstc12X10SfloatBlock,
            Self::eAstc12X12SfloatBlock => RawFormat::eAstc12X12SfloatBlock,
            Self::eG8B8G8R8422Unorm => RawFormat::eG8B8G8R8422Unorm,
            Self::eB8G8R8G8422Unorm => RawFormat::eB8G8R8G8422Unorm,
            Self::eG8B8R83plane420Unorm => RawFormat::eG8B8R83plane420Unorm,
            Self::eG8B8R82plane420Unorm => RawFormat::eG8B8R82plane420Unorm,
            Self::eG8B8R83plane422Unorm => RawFormat::eG8B8R83plane422Unorm,
            Self::eG8B8R82plane422Unorm => RawFormat::eG8B8R82plane422Unorm,
            Self::eG8B8R83plane444Unorm => RawFormat::eG8B8R83plane444Unorm,
            Self::eR10X6UnormPack16 => RawFormat::eR10X6UnormPack16,
            Self::eR10X6G10X6Unorm2pack16 => RawFormat::eR10X6G10X6Unorm2pack16,
            Self::eR10X6G10X6B10X6A10X6Unorm4pack16 => RawFormat::eR10X6G10X6B10X6A10X6Unorm4pack16,
            Self::eG10X6B10X6G10X6R10X6422Unorm4pack16 => RawFormat::eG10X6B10X6G10X6R10X6422Unorm4pack16,
            Self::eB10X6G10X6R10X6G10X6422Unorm4pack16 => RawFormat::eB10X6G10X6R10X6G10X6422Unorm4pack16,
            Self::eG10X6B10X6R10X63plane420Unorm3pack16 => RawFormat::eG10X6B10X6R10X63plane420Unorm3pack16,
            Self::eG10X6B10X6R10X62plane420Unorm3pack16 => RawFormat::eG10X6B10X6R10X62plane420Unorm3pack16,
            Self::eG10X6B10X6R10X63plane422Unorm3pack16 => RawFormat::eG10X6B10X6R10X63plane422Unorm3pack16,
            Self::eG10X6B10X6R10X62plane422Unorm3pack16 => RawFormat::eG10X6B10X6R10X62plane422Unorm3pack16,
            Self::eG10X6B10X6R10X63plane444Unorm3pack16 => RawFormat::eG10X6B10X6R10X63plane444Unorm3pack16,
            Self::eR12X4UnormPack16 => RawFormat::eR12X4UnormPack16,
            Self::eR12X4G12X4Unorm2pack16 => RawFormat::eR12X4G12X4Unorm2pack16,
            Self::eR12X4G12X4B12X4A12X4Unorm4pack16 => RawFormat::eR12X4G12X4B12X4A12X4Unorm4pack16,
            Self::eG12X4B12X4G12X4R12X4422Unorm4pack16 => RawFormat::eG12X4B12X4G12X4R12X4422Unorm4pack16,
            Self::eB12X4G12X4R12X4G12X4422Unorm4pack16 => RawFormat::eB12X4G12X4R12X4G12X4422Unorm4pack16,
            Self::eG12X4B12X4R12X43plane420Unorm3pack16 => RawFormat::eG12X4B12X4R12X43plane420Unorm3pack16,
            Self::eG12X4B12X4R12X42plane420Unorm3pack16 => RawFormat::eG12X4B12X4R12X42plane420Unorm3pack16,
            Self::eG12X4B12X4R12X43plane422Unorm3pack16 => RawFormat::eG12X4B12X4R12X43plane422Unorm3pack16,
            Self::eG12X4B12X4R12X42plane422Unorm3pack16 => RawFormat::eG12X4B12X4R12X42plane422Unorm3pack16,
            Self::eG12X4B12X4R12X43plane444Unorm3pack16 => RawFormat::eG12X4B12X4R12X43plane444Unorm3pack16,
            Self::eG16B16G16R16422Unorm => RawFormat::eG16B16G16R16422Unorm,
            Self::eB16G16R16G16422Unorm => RawFormat::eB16G16R16G16422Unorm,
            Self::eG16B16R163plane420Unorm => RawFormat::eG16B16R163plane420Unorm,
            Self::eG16B16R162plane420Unorm => RawFormat::eG16B16R162plane420Unorm,
            Self::eG16B16R163plane422Unorm => RawFormat::eG16B16R163plane422Unorm,
            Self::eG16B16R162plane422Unorm => RawFormat::eG16B16R162plane422Unorm,
            Self::eG16B16R163plane444Unorm => RawFormat::eG16B16R163plane444Unorm,
            Self::eG8B8R82plane444Unorm => RawFormat::eG8B8R82plane444Unorm,
            Self::eG10X6B10X6R10X62plane444Unorm3pack16 => RawFormat::eG10X6B10X6R10X62plane444Unorm3pack16,
            Self::eG12X4B12X4R12X42plane444Unorm3pack16 => RawFormat::eG12X4B12X4R12X42plane444Unorm3pack16,
            Self::eG16B16R162plane444Unorm => RawFormat::eG16B16R162plane444Unorm,
            Self::eA4R4G4B4UnormPack16 => RawFormat::eA4R4G4B4UnormPack16,
            Self::eA4B4G4R4UnormPack16 => RawFormat::eA4B4G4R4UnormPack16,
            Self::eR16G16S105Nv => RawFormat::eR16G16S105Nv,
            Self::eUnknownVariant(v) => RawFormat(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StructureType {
    eApplicationInfo,
    eInstanceCreateInfo,
    eDeviceQueueCreateInfo,
    eDeviceCreateInfo,
    eSubmitInfo,
    eMemoryAllocateInfo,
    eMappedMemoryRange,
    eBindSparseInfo,
    eFenceCreateInfo,
    eSemaphoreCreateInfo,
    eEventCreateInfo,
    eQueryPoolCreateInfo,
    eBufferCreateInfo,
    eBufferViewCreateInfo,
    eImageCreateInfo,
    eImageViewCreateInfo,
    eShaderModuleCreateInfo,
    ePipelineCacheCreateInfo,
    ePipelineShaderStageCreateInfo,
    ePipelineVertexInputStateCreateInfo,
    ePipelineInputAssemblyStateCreateInfo,
    ePipelineTessellationStateCreateInfo,
    ePipelineViewportStateCreateInfo,
    ePipelineRasterizationStateCreateInfo,
    ePipelineMultisampleStateCreateInfo,
    ePipelineDepthStencilStateCreateInfo,
    ePipelineColourBlendStateCreateInfo,
    ePipelineDynamicStateCreateInfo,
    eGraphicsPipelineCreateInfo,
    eComputePipelineCreateInfo,
    ePipelineLayoutCreateInfo,
    eSamplerCreateInfo,
    eDescriptorSetLayoutCreateInfo,
    eDescriptorPoolCreateInfo,
    eDescriptorSetAllocateInfo,
    eWriteDescriptorSet,
    eCopyDescriptorSet,
    eFramebufferCreateInfo,
    eRenderPassCreateInfo,
    eCommandPoolCreateInfo,
    eCommandBufferAllocateInfo,
    eCommandBufferInheritanceInfo,
    eCommandBufferBeginInfo,
    eRenderPassBeginInfo,
    eBufferMemoryBarrier,
    eImageMemoryBarrier,
    eMemoryBarrier,
    eLoaderInstanceCreateInfo,
    eLoaderDeviceCreateInfo,
    ePhysicalDeviceVulkan11Features,
    ePhysicalDeviceVulkan11Properties,
    ePhysicalDeviceVulkan12Features,
    ePhysicalDeviceVulkan12Properties,
    ePhysicalDeviceVulkan13Features,
    ePhysicalDeviceVulkan13Properties,
    eSwapchainCreateInfoKhr,
    ePresentInfoKhr,
    eDisplayModeCreateInfoKhr,
    eDisplaySurfaceCreateInfoKhr,
    eDisplayPresentInfoKhr,
    eXlibSurfaceCreateInfoKhr,
    eXcbSurfaceCreateInfoKhr,
    eWaylandSurfaceCreateInfoKhr,
    eAndroidSurfaceCreateInfoKhr,
    eWin32SurfaceCreateInfoKhr,
    eDebugReportCallbackCreateInfoExt,
    ePipelineRasterizationStateRasterizationOrderAmd,
    eDebugMarkerObjectNameInfoExt,
    eDebugMarkerObjectTagInfoExt,
    eDebugMarkerMarkerInfoExt,
    eVideoProfileInfoKhr,
    eVideoCapabilitiesKhr,
    eVideoPictureResourceInfoKhr,
    eVideoSessionMemoryRequirementsKhr,
    eBindVideoSessionMemoryInfoKhr,
    eVideoSessionCreateInfoKhr,
    eVideoSessionParametersCreateInfoKhr,
    eVideoSessionParametersUpdateInfoKhr,
    eVideoBeginCodingInfoKhr,
    eVideoEndCodingInfoKhr,
    eVideoCodingControlInfoKhr,
    eVideoReferenceSlotInfoKhr,
    eQueueFamilyVideoPropertiesKhr,
    eVideoProfileListInfoKhr,
    ePhysicalDeviceVideoFormatInfoKhr,
    eVideoFormatPropertiesKhr,
    eQueueFamilyQueryResultStatusPropertiesKhr,
    eVideoDecodeInfoKhr,
    eVideoDecodeCapabilitiesKhr,
    eVideoDecodeUsageInfoKhr,
    eDedicatedAllocationImageCreateInfoNv,
    eDedicatedAllocationBufferCreateInfoNv,
    eDedicatedAllocationMemoryAllocateInfoNv,
    ePhysicalDeviceTransformFeedbackFeaturesExt,
    ePhysicalDeviceTransformFeedbackPropertiesExt,
    ePipelineRasterizationStateStreamCreateInfoExt,
    eCuModuleCreateInfoNvx,
    eCuFunctionCreateInfoNvx,
    eCuLaunchInfoNvx,
    eImageViewHandleInfoNvx,
    eImageViewAddressPropertiesNvx,
    eVideoEncodeH264CapabilitiesExt,
    eVideoEncodeH264SessionParametersCreateInfoExt,
    eVideoEncodeH264SessionParametersAddInfoExt,
    eVideoEncodeH264VclFrameInfoExt,
    eVideoEncodeH264DpbSlotInfoExt,
    eVideoEncodeH264NaluSliceInfoExt,
    eVideoEncodeH264EmitPictureParametersInfoExt,
    eVideoEncodeH264ProfileInfoExt,
    eVideoEncodeH264RateControlInfoExt,
    eVideoEncodeH264RateControlLayerInfoExt,
    eVideoEncodeH264ReferenceListsInfoExt,
    eVideoEncodeH265CapabilitiesExt,
    eVideoEncodeH265SessionParametersCreateInfoExt,
    eVideoEncodeH265SessionParametersAddInfoExt,
    eVideoEncodeH265VclFrameInfoExt,
    eVideoEncodeH265DpbSlotInfoExt,
    eVideoEncodeH265NaluSliceSegmentInfoExt,
    eVideoEncodeH265EmitPictureParametersInfoExt,
    eVideoEncodeH265ProfileInfoExt,
    eVideoEncodeH265ReferenceListsInfoExt,
    eVideoEncodeH265RateControlInfoExt,
    eVideoEncodeH265RateControlLayerInfoExt,
    eVideoDecodeH264CapabilitiesKhr,
    eVideoDecodeH264PictureInfoKhr,
    eVideoDecodeH264ProfileInfoKhr,
    eVideoDecodeH264SessionParametersCreateInfoKhr,
    eVideoDecodeH264SessionParametersAddInfoKhr,
    eVideoDecodeH264DpbSlotInfoKhr,
    eTextureLodGatherFormatPropertiesAmd,
    eRenderingInfo,
    eRenderingAttachmentInfo,
    ePipelineRenderingCreateInfo,
    ePhysicalDeviceDynamicRenderingFeatures,
    eCommandBufferInheritanceRenderingInfo,
    eRenderingFragmentShadingRateAttachmentInfoKhr,
    eRenderingFragmentDensityMapAttachmentInfoExt,
    eAttachmentSampleCountInfoAmd,
    eMultiviewPerViewAttributesInfoNvx,
    eStreamDescriptorSurfaceCreateInfoGgp,
    ePhysicalDeviceCornerSampledImageFeaturesNv,
    ePrivateVendorInfoReservedOffset0Nv,
    eRenderPassMultiviewCreateInfo,
    ePhysicalDeviceMultiviewFeatures,
    ePhysicalDeviceMultiviewProperties,
    eExternalMemoryImageCreateInfoNv,
    eExportMemoryAllocateInfoNv,
    eImportMemoryWin32HandleInfoNv,
    eExportMemoryWin32HandleInfoNv,
    eWin32KeyedMutexAcquireReleaseInfoNv,
    ePhysicalDeviceFeatures2,
    ePhysicalDeviceProperties2,
    eFormatProperties2,
    eImageFormatProperties2,
    ePhysicalDeviceImageFormatInfo2,
    eQueueFamilyProperties2,
    ePhysicalDeviceMemoryProperties2,
    eSparseImageFormatProperties2,
    ePhysicalDeviceSparseImageFormatInfo2,
    eMemoryAllocateFlagsInfo,
    eDeviceGroupRenderPassBeginInfo,
    eDeviceGroupCommandBufferBeginInfo,
    eDeviceGroupSubmitInfo,
    eDeviceGroupBindSparseInfo,
    eDeviceGroupPresentCapabilitiesKhr,
    eImageSwapchainCreateInfoKhr,
    eBindImageMemorySwapchainInfoKhr,
    eAcquireNextImageInfoKhr,
    eDeviceGroupPresentInfoKhr,
    eDeviceGroupSwapchainCreateInfoKhr,
    eBindBufferMemoryDeviceGroupInfo,
    eBindImageMemoryDeviceGroupInfo,
    eValidationFlagsExt,
    eViSurfaceCreateInfoNn,
    ePhysicalDeviceShaderDrawParametersFeatures,
    ePhysicalDeviceTextureCompressionAstcHdrFeatures,
    eImageViewAstcDecodeModeExt,
    ePhysicalDeviceAstcDecodeFeaturesExt,
    ePipelineRobustnessCreateInfoExt,
    ePhysicalDevicePipelineRobustnessFeaturesExt,
    ePhysicalDevicePipelineRobustnessPropertiesExt,
    ePhysicalDeviceGroupProperties,
    eDeviceGroupDeviceCreateInfo,
    ePhysicalDeviceExternalImageFormatInfo,
    eExternalImageFormatProperties,
    ePhysicalDeviceExternalBufferInfo,
    eExternalBufferProperties,
    ePhysicalDeviceIdProperties,
    eExternalMemoryBufferCreateInfo,
    eExternalMemoryImageCreateInfo,
    eExportMemoryAllocateInfo,
    eImportMemoryWin32HandleInfoKhr,
    eExportMemoryWin32HandleInfoKhr,
    eMemoryWin32HandlePropertiesKhr,
    eMemoryGetWin32HandleInfoKhr,
    eImportMemoryFdInfoKhr,
    eMemoryFdPropertiesKhr,
    eMemoryGetFdInfoKhr,
    eWin32KeyedMutexAcquireReleaseInfoKhr,
    ePhysicalDeviceExternalSemaphoreInfo,
    eExternalSemaphoreProperties,
    eExportSemaphoreCreateInfo,
    eImportSemaphoreWin32HandleInfoKhr,
    eExportSemaphoreWin32HandleInfoKhr,
    eD3D12FenceSubmitInfoKhr,
    eSemaphoreGetWin32HandleInfoKhr,
    eImportSemaphoreFdInfoKhr,
    eSemaphoreGetFdInfoKhr,
    ePhysicalDevicePushDescriptorPropertiesKhr,
    eCommandBufferInheritanceConditionalRenderingInfoExt,
    ePhysicalDeviceConditionalRenderingFeaturesExt,
    eConditionalRenderingBeginInfoExt,
    ePhysicalDeviceShaderFloat16Int8Features,
    ePhysicalDevice16bitStorageFeatures,
    ePresentRegionsKhr,
    eDescriptorUpdateTemplateCreateInfo,
    ePipelineViewportWScalingStateCreateInfoNv,
    eSurfaceCapabilities2Ext,
    eDisplayPowerInfoExt,
    eDeviceEventInfoExt,
    eDisplayEventInfoExt,
    eSwapchainCounterCreateInfoExt,
    ePresentTimesInfoGoogle,
    ePhysicalDeviceSubgroupProperties,
    ePhysicalDeviceMultiviewPerViewAttributesPropertiesNvx,
    ePipelineViewportSwizzleStateCreateInfoNv,
    ePhysicalDeviceDiscardRectanglePropertiesExt,
    ePipelineDiscardRectangleStateCreateInfoExt,
    ePhysicalDeviceConservativeRasterizationPropertiesExt,
    ePipelineRasterizationConservativeStateCreateInfoExt,
    ePhysicalDeviceDepthClipEnableFeaturesExt,
    ePipelineRasterizationDepthClipStateCreateInfoExt,
    eHdrMetadataExt,
    ePhysicalDeviceImagelessFramebufferFeatures,
    eFramebufferAttachmentsCreateInfo,
    eFramebufferAttachmentImageInfo,
    eRenderPassAttachmentBeginInfo,
    eAttachmentDescription2,
    eAttachmentReference2,
    eSubpassDescription2,
    eSubpassDependency2,
    eRenderPassCreateInfo2,
    eSubpassBeginInfo,
    eSubpassEndInfo,
    eSharedPresentSurfaceCapabilitiesKhr,
    ePhysicalDeviceExternalFenceInfo,
    eExternalFenceProperties,
    eExportFenceCreateInfo,
    eImportFenceWin32HandleInfoKhr,
    eExportFenceWin32HandleInfoKhr,
    eFenceGetWin32HandleInfoKhr,
    eImportFenceFdInfoKhr,
    eFenceGetFdInfoKhr,
    ePhysicalDevicePerformanceQueryFeaturesKhr,
    ePhysicalDevicePerformanceQueryPropertiesKhr,
    eQueryPoolPerformanceCreateInfoKhr,
    ePerformanceQuerySubmitInfoKhr,
    eAcquireProfilingLockInfoKhr,
    ePerformanceCounterKhr,
    ePerformanceCounterDescriptionKhr,
    ePerformanceQueryReservationInfoKhr,
    ePhysicalDevicePointClippingProperties,
    eRenderPassInputAttachmentAspectCreateInfo,
    eImageViewUsageCreateInfo,
    ePipelineTessellationDomainOriginStateCreateInfo,
    ePhysicalDeviceSurfaceInfo2Khr,
    eSurfaceCapabilities2Khr,
    eSurfaceFormat2Khr,
    ePhysicalDeviceVariablePointersFeatures,
    eDisplayProperties2Khr,
    eDisplayPlaneProperties2Khr,
    eDisplayModeProperties2Khr,
    eDisplayPlaneInfo2Khr,
    eDisplayPlaneCapabilities2Khr,
    eIosSurfaceCreateInfoMvk,
    eMacosSurfaceCreateInfoMvk,
    eMemoryDedicatedRequirements,
    eMemoryDedicatedAllocateInfo,
    eDebugUtilsObjectNameInfoExt,
    eDebugUtilsObjectTagInfoExt,
    eDebugUtilsLabelExt,
    eDebugUtilsMessengerCallbackDataExt,
    eDebugUtilsMessengerCreateInfoExt,
    eAndroidHardwareBufferUsageAndroid,
    eAndroidHardwareBufferPropertiesAndroid,
    eAndroidHardwareBufferFormatPropertiesAndroid,
    eImportAndroidHardwareBufferInfoAndroid,
    eMemoryGetAndroidHardwareBufferInfoAndroid,
    eExternalFormatAndroid,
    eAndroidHardwareBufferFormatProperties2Android,
    ePhysicalDeviceSamplerFilterMinmaxProperties,
    eSamplerReductionModeCreateInfo,
    ePhysicalDeviceInlineUniformBlockFeatures,
    ePhysicalDeviceInlineUniformBlockProperties,
    eWriteDescriptorSetInlineUniformBlock,
    eDescriptorPoolInlineUniformBlockCreateInfo,
    eSampleLocationsInfoExt,
    eRenderPassSampleLocationsBeginInfoExt,
    ePipelineSampleLocationsStateCreateInfoExt,
    ePhysicalDeviceSampleLocationsPropertiesExt,
    eMultisamplePropertiesExt,
    eProtectedSubmitInfo,
    ePhysicalDeviceProtectedMemoryFeatures,
    ePhysicalDeviceProtectedMemoryProperties,
    eDeviceQueueInfo2,
    eBufferMemoryRequirementsInfo2,
    eImageMemoryRequirementsInfo2,
    eImageSparseMemoryRequirementsInfo2,
    eMemoryRequirements2,
    eSparseImageMemoryRequirements2,
    eImageFormatListCreateInfo,
    ePhysicalDeviceBlendOperationAdvancedFeaturesExt,
    ePhysicalDeviceBlendOperationAdvancedPropertiesExt,
    ePipelineColourBlendAdvancedStateCreateInfoExt,
    ePipelineCoverageToColourStateCreateInfoNv,
    eAccelerationStructureBuildGeometryInfoKhr,
    eAccelerationStructureDeviceAddressInfoKhr,
    eAccelerationStructureGeometryAabbsDataKhr,
    eAccelerationStructureGeometryInstancesDataKhr,
    eAccelerationStructureGeometryTrianglesDataKhr,
    eAccelerationStructureGeometryKhr,
    eWriteDescriptorSetAccelerationStructureKhr,
    eAccelerationStructureVersionInfoKhr,
    eCopyAccelerationStructureInfoKhr,
    eCopyAccelerationStructureToMemoryInfoKhr,
    eCopyMemoryToAccelerationStructureInfoKhr,
    ePhysicalDeviceAccelerationStructureFeaturesKhr,
    ePhysicalDeviceAccelerationStructurePropertiesKhr,
    eRayTracingPipelineCreateInfoKhr,
    eRayTracingShaderGroupCreateInfoKhr,
    eAccelerationStructureCreateInfoKhr,
    eRayTracingPipelineInterfaceCreateInfoKhr,
    eAccelerationStructureBuildSizesInfoKhr,
    ePipelineCoverageModulationStateCreateInfoNv,
    ePhysicalDeviceShaderSmBuiltinsFeaturesNv,
    ePhysicalDeviceShaderSmBuiltinsPropertiesNv,
    eSamplerYcbcrConversionCreateInfo,
    eSamplerYcbcrConversionInfo,
    eBindImagePlaneMemoryInfo,
    eImagePlaneMemoryRequirementsInfo,
    ePhysicalDeviceSamplerYcbcrConversionFeatures,
    eSamplerYcbcrConversionImageFormatProperties,
    eBindBufferMemoryInfo,
    eBindImageMemoryInfo,
    eDrmFormatModifierPropertiesListExt,
    ePhysicalDeviceImageDrmFormatModifierInfoExt,
    eImageDrmFormatModifierListCreateInfoExt,
    eImageDrmFormatModifierExplicitCreateInfoExt,
    eImageDrmFormatModifierPropertiesExt,
    eDrmFormatModifierPropertiesList2Ext,
    eValidationCacheCreateInfoExt,
    eShaderModuleValidationCacheCreateInfoExt,
    eDescriptorSetLayoutBindingFlagsCreateInfo,
    ePhysicalDeviceDescriptorIndexingFeatures,
    ePhysicalDeviceDescriptorIndexingProperties,
    eDescriptorSetVariableDescriptorCountAllocateInfo,
    eDescriptorSetVariableDescriptorCountLayoutSupport,
    ePhysicalDevicePortabilitySubsetFeaturesKhr,
    ePhysicalDevicePortabilitySubsetPropertiesKhr,
    ePipelineViewportShadingRateImageStateCreateInfoNv,
    ePhysicalDeviceShadingRateImageFeaturesNv,
    ePhysicalDeviceShadingRateImagePropertiesNv,
    ePipelineViewportCoarseSampleOrderStateCreateInfoNv,
    eRayTracingPipelineCreateInfoNv,
    eAccelerationStructureCreateInfoNv,
    eGeometryNv,
    eGeometryTrianglesNv,
    eGeometryAabbNv,
    eBindAccelerationStructureMemoryInfoNv,
    eWriteDescriptorSetAccelerationStructureNv,
    eAccelerationStructureMemoryRequirementsInfoNv,
    ePhysicalDeviceRayTracingPropertiesNv,
    eRayTracingShaderGroupCreateInfoNv,
    eAccelerationStructureInfoNv,
    ePhysicalDeviceRepresentativeFragmentTestFeaturesNv,
    ePipelineRepresentativeFragmentTestStateCreateInfoNv,
    ePhysicalDeviceMaintenance3Properties,
    eDescriptorSetLayoutSupport,
    ePhysicalDeviceImageViewImageFormatInfoExt,
    eFilterCubicImageViewImageFormatPropertiesExt,
    eDeviceQueueGlobalPriorityCreateInfoKhr,
    ePhysicalDeviceShaderSubgroupExtendedTypesFeatures,
    ePhysicalDevice8bitStorageFeatures,
    eImportMemoryHostPointerInfoExt,
    eMemoryHostPointerPropertiesExt,
    ePhysicalDeviceExternalMemoryHostPropertiesExt,
    ePhysicalDeviceShaderAtomicInt64Features,
    ePhysicalDeviceShaderClockFeaturesKhr,
    ePipelineCompilerControlCreateInfoAmd,
    eCalibratedTimestampInfoExt,
    ePhysicalDeviceShaderCorePropertiesAmd,
    eVideoDecodeH265CapabilitiesKhr,
    eVideoDecodeH265SessionParametersCreateInfoKhr,
    eVideoDecodeH265SessionParametersAddInfoKhr,
    eVideoDecodeH265ProfileInfoKhr,
    eVideoDecodeH265PictureInfoKhr,
    eVideoDecodeH265DpbSlotInfoKhr,
    eDeviceMemoryOverallocationCreateInfoAmd,
    ePhysicalDeviceVertexAttributeDivisorPropertiesExt,
    ePipelineVertexInputDivisorStateCreateInfoExt,
    ePhysicalDeviceVertexAttributeDivisorFeaturesExt,
    ePresentFrameTokenGgp,
    ePipelineCreationFeedbackCreateInfo,
    ePhysicalDeviceDriverProperties,
    ePhysicalDeviceFloatControlsProperties,
    ePhysicalDeviceDepthStencilResolveProperties,
    eSubpassDescriptionDepthStencilResolve,
    ePhysicalDeviceComputeShaderDerivativesFeaturesNv,
    ePhysicalDeviceMeshShaderFeaturesNv,
    ePhysicalDeviceMeshShaderPropertiesNv,
    ePhysicalDeviceFragmentShaderBarycentricFeaturesKhr,
    ePhysicalDeviceShaderImageFootprintFeaturesNv,
    ePipelineViewportExclusiveScissorStateCreateInfoNv,
    ePhysicalDeviceExclusiveScissorFeaturesNv,
    eCheckpointDataNv,
    eQueueFamilyCheckpointPropertiesNv,
    ePhysicalDeviceTimelineSemaphoreFeatures,
    ePhysicalDeviceTimelineSemaphoreProperties,
    eSemaphoreTypeCreateInfo,
    eTimelineSemaphoreSubmitInfo,
    eSemaphoreWaitInfo,
    eSemaphoreSignalInfo,
    ePhysicalDeviceShaderIntegerFunctions2FeaturesIntel,
    eQueryPoolPerformanceQueryCreateInfoIntel,
    eInitializePerformanceApiInfoIntel,
    ePerformanceMarkerInfoIntel,
    ePerformanceStreamMarkerInfoIntel,
    ePerformanceOverrideInfoIntel,
    ePerformanceConfigurationAcquireInfoIntel,
    ePhysicalDeviceVulkanMemoryModelFeatures,
    ePhysicalDevicePciBusInfoPropertiesExt,
    eDisplayNativeHdrSurfaceCapabilitiesAmd,
    eSwapchainDisplayNativeHdrCreateInfoAmd,
    eImagepipeSurfaceCreateInfoFuchsia,
    ePhysicalDeviceShaderTerminateInvocationFeatures,
    eMetalSurfaceCreateInfoExt,
    ePhysicalDeviceFragmentDensityMapFeaturesExt,
    ePhysicalDeviceFragmentDensityMapPropertiesExt,
    eRenderPassFragmentDensityMapCreateInfoExt,
    ePhysicalDeviceScalarBlockLayoutFeatures,
    ePhysicalDeviceSubgroupSizeControlProperties,
    ePipelineShaderStageRequiredSubgroupSizeCreateInfo,
    ePhysicalDeviceSubgroupSizeControlFeatures,
    eFragmentShadingRateAttachmentInfoKhr,
    ePipelineFragmentShadingRateStateCreateInfoKhr,
    ePhysicalDeviceFragmentShadingRatePropertiesKhr,
    ePhysicalDeviceFragmentShadingRateFeaturesKhr,
    ePhysicalDeviceFragmentShadingRateKhr,
    ePhysicalDeviceShaderCoreProperties2Amd,
    ePhysicalDeviceCoherentMemoryFeaturesAmd,
    ePhysicalDeviceShaderImageAtomicInt64FeaturesExt,
    ePhysicalDeviceMemoryBudgetPropertiesExt,
    ePhysicalDeviceMemoryPriorityFeaturesExt,
    eMemoryPriorityAllocateInfoExt,
    eSurfaceProtectedCapabilitiesKhr,
    ePhysicalDeviceDedicatedAllocationImageAliasingFeaturesNv,
    ePhysicalDeviceSeparateDepthStencilLayoutsFeatures,
    eAttachmentReferenceStencilLayout,
    eAttachmentDescriptionStencilLayout,
    ePhysicalDeviceBufferDeviceAddressFeaturesExt,
    eBufferDeviceAddressInfo,
    eBufferDeviceAddressCreateInfoExt,
    ePhysicalDeviceToolProperties,
    eImageStencilUsageCreateInfo,
    eValidationFeaturesExt,
    ePhysicalDevicePresentWaitFeaturesKhr,
    ePhysicalDeviceCooperativeMatrixFeaturesNv,
    eCooperativeMatrixPropertiesNv,
    ePhysicalDeviceCooperativeMatrixPropertiesNv,
    ePhysicalDeviceCoverageReductionModeFeaturesNv,
    ePipelineCoverageReductionStateCreateInfoNv,
    eFramebufferMixedSamplesCombinationNv,
    ePhysicalDeviceFragmentShaderInterlockFeaturesExt,
    ePhysicalDeviceYcbcrImageArraysFeaturesExt,
    ePhysicalDeviceUniformBufferStandardLayoutFeatures,
    ePhysicalDeviceProvokingVertexFeaturesExt,
    ePipelineRasterizationProvokingVertexStateCreateInfoExt,
    ePhysicalDeviceProvokingVertexPropertiesExt,
    eSurfaceFullScreenExclusiveInfoExt,
    eSurfaceFullScreenExclusiveWin32InfoExt,
    eSurfaceCapabilitiesFullScreenExclusiveExt,
    eHeadlessSurfaceCreateInfoExt,
    ePhysicalDeviceBufferDeviceAddressFeatures,
    eBufferOpaqueCaptureAddressCreateInfo,
    eMemoryOpaqueCaptureAddressAllocateInfo,
    eDeviceMemoryOpaqueCaptureAddressInfo,
    ePhysicalDeviceLineRasterizationFeaturesExt,
    ePipelineRasterizationLineStateCreateInfoExt,
    ePhysicalDeviceLineRasterizationPropertiesExt,
    ePhysicalDeviceShaderAtomicFloatFeaturesExt,
    ePhysicalDeviceHostQueryResetFeatures,
    ePhysicalDeviceIndexTypeUint8FeaturesExt,
    ePhysicalDeviceExtendedDynamicStateFeaturesExt,
    ePhysicalDevicePipelineExecutablePropertiesFeaturesKhr,
    ePipelineInfoKhr,
    ePipelineExecutablePropertiesKhr,
    ePipelineExecutableInfoKhr,
    ePipelineExecutableStatisticKhr,
    ePipelineExecutableInternalRepresentationKhr,
    ePhysicalDeviceShaderAtomicFloat2FeaturesExt,
    eSurfacePresentModeExt,
    eSurfacePresentScalingCapabilitiesExt,
    eSurfacePresentModeCompatibilityExt,
    ePhysicalDeviceSwapchainMaintenance1FeaturesExt,
    eSwapchainPresentFenceInfoExt,
    eSwapchainPresentModesCreateInfoExt,
    eSwapchainPresentModeInfoExt,
    eSwapchainPresentScalingCreateInfoExt,
    eReleaseSwapchainImagesInfoExt,
    ePhysicalDeviceShaderDemoteToHelperInvocationFeatures,
    ePhysicalDeviceDeviceGeneratedCommandsPropertiesNv,
    eGraphicsShaderGroupCreateInfoNv,
    eGraphicsPipelineShaderGroupsCreateInfoNv,
    eIndirectCommandsLayoutTokenNv,
    eIndirectCommandsLayoutCreateInfoNv,
    eGeneratedCommandsInfoNv,
    eGeneratedCommandsMemoryRequirementsInfoNv,
    ePhysicalDeviceDeviceGeneratedCommandsFeaturesNv,
    ePhysicalDeviceInheritedViewportScissorFeaturesNv,
    eCommandBufferInheritanceViewportScissorInfoNv,
    ePhysicalDeviceShaderIntegerDotProductFeatures,
    ePhysicalDeviceShaderIntegerDotProductProperties,
    ePhysicalDeviceTexelBufferAlignmentFeaturesExt,
    ePhysicalDeviceTexelBufferAlignmentProperties,
    eCommandBufferInheritanceRenderPassTransformInfoQcom,
    eRenderPassTransformBeginInfoQcom,
    ePhysicalDeviceDeviceMemoryReportFeaturesExt,
    eDeviceDeviceMemoryReportCreateInfoExt,
    eDeviceMemoryReportCallbackDataExt,
    ePhysicalDeviceRobustness2FeaturesExt,
    ePhysicalDeviceRobustness2PropertiesExt,
    eSamplerCustomBorderColourCreateInfoExt,
    ePhysicalDeviceCustomBorderColourPropertiesExt,
    ePhysicalDeviceCustomBorderColourFeaturesExt,
    ePipelineLibraryCreateInfoKhr,
    ePhysicalDevicePresentBarrierFeaturesNv,
    eSurfaceCapabilitiesPresentBarrierNv,
    eSwapchainPresentBarrierCreateInfoNv,
    ePresentIdKhr,
    ePhysicalDevicePresentIdFeaturesKhr,
    ePhysicalDevicePrivateDataFeatures,
    eDevicePrivateDataCreateInfo,
    ePrivateDataSlotCreateInfo,
    ePhysicalDevicePipelineCreationCacheControlFeatures,
    ePhysicalDeviceVulkanSc10Features,
    ePhysicalDeviceVulkanSc10Properties,
    eDeviceObjectReservationCreateInfo,
    eCommandPoolMemoryReservationCreateInfo,
    eCommandPoolMemoryConsumption,
    ePipelinePoolSize,
    eFaultData,
    eFaultCallbackInfo,
    ePipelineOfflineCreateInfo,
    eVideoEncodeInfoKhr,
    eVideoEncodeRateControlInfoKhr,
    eVideoEncodeRateControlLayerInfoKhr,
    eVideoEncodeCapabilitiesKhr,
    eVideoEncodeUsageInfoKhr,
    ePhysicalDeviceDiagnosticsConfigFeaturesNv,
    eDeviceDiagnosticsConfigCreateInfoNv,
    eRefreshObjectListKhr,
    eQueryLowLatencySupportNv,
    eExportMetalObjectCreateInfoExt,
    eExportMetalObjectsInfoExt,
    eExportMetalDeviceInfoExt,
    eExportMetalCommandQueueInfoExt,
    eExportMetalBufferInfoExt,
    eImportMetalBufferInfoExt,
    eExportMetalTextureInfoExt,
    eImportMetalTextureInfoExt,
    eExportMetalIoSurfaceInfoExt,
    eImportMetalIoSurfaceInfoExt,
    eExportMetalSharedEventInfoExt,
    eImportMetalSharedEventInfoExt,
    eMemoryBarrier2,
    eBufferMemoryBarrier2,
    eImageMemoryBarrier2,
    eDependencyInfo,
    eSubmitInfo2,
    eSemaphoreSubmitInfo,
    eCommandBufferSubmitInfo,
    ePhysicalDeviceSynchronization2Features,
    eQueueFamilyCheckpointProperties2Nv,
    eCheckpointData2Nv,
    ePhysicalDeviceDescriptorBufferPropertiesExt,
    ePhysicalDeviceDescriptorBufferDensityMapPropertiesExt,
    ePhysicalDeviceDescriptorBufferFeaturesExt,
    eDescriptorAddressInfoExt,
    eDescriptorGetInfoExt,
    eBufferCaptureDescriptorDataInfoExt,
    eImageCaptureDescriptorDataInfoExt,
    eImageViewCaptureDescriptorDataInfoExt,
    eSamplerCaptureDescriptorDataInfoExt,
    eAccelerationStructureCaptureDescriptorDataInfoExt,
    eOpaqueCaptureDescriptorDataCreateInfoExt,
    eDescriptorBufferBindingInfoExt,
    eDescriptorBufferBindingPushDescriptorBufferHandleExt,
    ePhysicalDeviceGraphicsPipelineLibraryFeaturesExt,
    ePhysicalDeviceGraphicsPipelineLibraryPropertiesExt,
    eGraphicsPipelineLibraryCreateInfoExt,
    ePhysicalDeviceShaderEarlyAndLateFragmentTestsFeaturesAmd,
    ePhysicalDeviceFragmentShaderBarycentricPropertiesKhr,
    ePhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKhr,
    ePhysicalDeviceZeroInitializeWorkgroupMemoryFeatures,
    ePhysicalDeviceFragmentShadingRateEnumsPropertiesNv,
    ePhysicalDeviceFragmentShadingRateEnumsFeaturesNv,
    ePipelineFragmentShadingRateEnumStateCreateInfoNv,
    eAccelerationStructureGeometryMotionTrianglesDataNv,
    ePhysicalDeviceRayTracingMotionBlurFeaturesNv,
    eAccelerationStructureMotionInfoNv,
    ePhysicalDeviceMeshShaderFeaturesExt,
    ePhysicalDeviceMeshShaderPropertiesExt,
    ePhysicalDeviceYcbcr2Plane444FormatsFeaturesExt,
    ePhysicalDeviceFragmentDensityMap2FeaturesExt,
    ePhysicalDeviceFragmentDensityMap2PropertiesExt,
    eCopyCommandTransformInfoQcom,
    ePhysicalDeviceImageRobustnessFeatures,
    ePhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKhr,
    eCopyBufferInfo2,
    eCopyImageInfo2,
    eCopyBufferToImageInfo2,
    eCopyImageToBufferInfo2,
    eBlitImageInfo2,
    eResolveImageInfo2,
    eBufferCopy2,
    eImageCopy2,
    eImageBlit2,
    eBufferImageCopy2,
    eImageResolve2,
    ePhysicalDeviceImageCompressionControlFeaturesExt,
    eImageCompressionControlExt,
    eSubresourceLayout2Ext,
    eImageSubresource2Ext,
    eImageCompressionPropertiesExt,
    ePhysicalDeviceAttachmentFeedbackLoopLayoutFeaturesExt,
    ePhysicalDevice4444FormatsFeaturesExt,
    ePhysicalDeviceFaultFeaturesExt,
    eDeviceFaultCountsExt,
    eDeviceFaultInfoExt,
    ePhysicalDeviceRasterizationOrderAttachmentAccessFeaturesExt,
    ePhysicalDeviceRgba10X6FormatsFeaturesExt,
    eDirectfbSurfaceCreateInfoExt,
    ePhysicalDeviceRayTracingPipelineFeaturesKhr,
    ePhysicalDeviceRayTracingPipelinePropertiesKhr,
    ePhysicalDeviceRayQueryFeaturesKhr,
    ePhysicalDeviceMutableDescriptorTypeFeaturesExt,
    eMutableDescriptorTypeCreateInfoExt,
    ePhysicalDeviceVertexInputDynamicStateFeaturesExt,
    eVertexInputBindingDescription2Ext,
    eVertexInputAttributeDescription2Ext,
    ePhysicalDeviceDrmPropertiesExt,
    ePhysicalDeviceAddressBindingReportFeaturesExt,
    eDeviceAddressBindingCallbackDataExt,
    ePhysicalDeviceDepthClipControlFeaturesExt,
    ePipelineViewportDepthClipControlCreateInfoExt,
    ePhysicalDevicePrimitiveTopologyListRestartFeaturesExt,
    eFormatProperties3,
    eImportMemoryZirconHandleInfoFuchsia,
    eMemoryZirconHandlePropertiesFuchsia,
    eMemoryGetZirconHandleInfoFuchsia,
    eImportSemaphoreZirconHandleInfoFuchsia,
    eSemaphoreGetZirconHandleInfoFuchsia,
    eBufferCollectionCreateInfoFuchsia,
    eImportMemoryBufferCollectionFuchsia,
    eBufferCollectionImageCreateInfoFuchsia,
    eBufferCollectionPropertiesFuchsia,
    eBufferConstraintsInfoFuchsia,
    eBufferCollectionBufferCreateInfoFuchsia,
    eImageConstraintsInfoFuchsia,
    eImageFormatConstraintsInfoFuchsia,
    eSysmemColourSpaceFuchsia,
    eBufferCollectionConstraintsInfoFuchsia,
    eSubpassShadingPipelineCreateInfoHuawei,
    ePhysicalDeviceSubpassShadingFeaturesHuawei,
    ePhysicalDeviceSubpassShadingPropertiesHuawei,
    ePhysicalDeviceInvocationMaskFeaturesHuawei,
    eMemoryGetRemoteAddressInfoNv,
    ePhysicalDeviceExternalMemoryRdmaFeaturesNv,
    ePipelinePropertiesIdentifierExt,
    ePhysicalDevicePipelinePropertiesFeaturesExt,
    eImportFenceSciSyncInfoNv,
    eExportFenceSciSyncInfoNv,
    eFenceGetSciSyncInfoNv,
    eSciSyncAttributesInfoNv,
    eImportSemaphoreSciSyncInfoNv,
    eExportSemaphoreSciSyncInfoNv,
    eSemaphoreGetSciSyncInfoNv,
    ePhysicalDeviceExternalSciSyncFeaturesNv,
    eImportMemorySciBufInfoNv,
    eExportMemorySciBufInfoNv,
    eMemoryGetSciBufInfoNv,
    eMemorySciBufPropertiesNv,
    ePhysicalDeviceExternalMemorySciBufFeaturesNv,
    ePhysicalDeviceMultisampledRenderToSingleSampledFeaturesExt,
    eSubpassResolvePerformanceQueryExt,
    eMultisampledRenderToSingleSampledInfoExt,
    ePhysicalDeviceExtendedDynamicState2FeaturesExt,
    eScreenSurfaceCreateInfoQnx,
    ePhysicalDeviceColourWriteEnableFeaturesExt,
    ePipelineColourWriteCreateInfoExt,
    ePhysicalDevicePrimitivesGeneratedQueryFeaturesExt,
    ePhysicalDeviceRayTracingMaintenance1FeaturesKhr,
    ePhysicalDeviceGlobalPriorityQueryFeaturesKhr,
    eQueueFamilyGlobalPriorityPropertiesKhr,
    ePhysicalDeviceImageViewMinLodFeaturesExt,
    eImageViewMinLodCreateInfoExt,
    ePhysicalDeviceMultiDrawFeaturesExt,
    ePhysicalDeviceMultiDrawPropertiesExt,
    ePhysicalDeviceImage2dViewOf3dFeaturesExt,
    eMicromapBuildInfoExt,
    eMicromapVersionInfoExt,
    eCopyMicromapInfoExt,
    eCopyMicromapToMemoryInfoExt,
    eCopyMemoryToMicromapInfoExt,
    ePhysicalDeviceOpacityMicromapFeaturesExt,
    ePhysicalDeviceOpacityMicromapPropertiesExt,
    eMicromapCreateInfoExt,
    eMicromapBuildSizesInfoExt,
    eAccelerationStructureTrianglesOpacityMicromapExt,
    ePhysicalDeviceClusterCullingShaderFeaturesHuawei,
    ePhysicalDeviceClusterCullingShaderPropertiesHuawei,
    ePhysicalDeviceBorderColourSwizzleFeaturesExt,
    eSamplerBorderColourComponentMappingCreateInfoExt,
    ePhysicalDevicePageableDeviceLocalMemoryFeaturesExt,
    ePhysicalDeviceMaintenance4Features,
    ePhysicalDeviceMaintenance4Properties,
    eDeviceBufferMemoryRequirements,
    eDeviceImageMemoryRequirements,
    ePhysicalDeviceShaderCorePropertiesArm,
    ePhysicalDeviceImageSlicedViewOf3dFeaturesExt,
    eImageViewSlicedCreateInfoExt,
    ePhysicalDeviceDescriptorSetHostMappingFeaturesValve,
    eDescriptorSetBindingReferenceValve,
    eDescriptorSetLayoutHostMappingInfoValve,
    ePhysicalDeviceDepthClampZeroOneFeaturesExt,
    ePhysicalDeviceNonSeamlessCubeMapFeaturesExt,
    ePhysicalDeviceFragmentDensityMapOffsetFeaturesQcom,
    ePhysicalDeviceFragmentDensityMapOffsetPropertiesQcom,
    eSubpassFragmentDensityMapOffsetEndInfoQcom,
    ePhysicalDeviceCopyMemoryIndirectFeaturesNv,
    ePhysicalDeviceCopyMemoryIndirectPropertiesNv,
    ePhysicalDeviceMemoryDecompressionFeaturesNv,
    ePhysicalDeviceMemoryDecompressionPropertiesNv,
    ePhysicalDeviceLinearColourAttachmentFeaturesNv,
    eApplicationParametersExt,
    ePhysicalDeviceImageCompressionControlSwapchainFeaturesExt,
    ePhysicalDeviceImageProcessingFeaturesQcom,
    ePhysicalDeviceImageProcessingPropertiesQcom,
    eImageViewSampleWeightCreateInfoQcom,
    ePhysicalDeviceExtendedDynamicState3FeaturesExt,
    ePhysicalDeviceExtendedDynamicState3PropertiesExt,
    ePhysicalDeviceSubpassMergeFeedbackFeaturesExt,
    eRenderPassCreationControlExt,
    eRenderPassCreationFeedbackCreateInfoExt,
    eRenderPassSubpassFeedbackCreateInfoExt,
    eDirectDriverLoadingInfoLunarg,
    eDirectDriverLoadingListLunarg,
    ePhysicalDeviceShaderModuleIdentifierFeaturesExt,
    ePhysicalDeviceShaderModuleIdentifierPropertiesExt,
    ePipelineShaderStageModuleIdentifierCreateInfoExt,
    eShaderModuleIdentifierExt,
    ePhysicalDeviceOpticalFlowFeaturesNv,
    ePhysicalDeviceOpticalFlowPropertiesNv,
    eOpticalFlowImageFormatInfoNv,
    eOpticalFlowImageFormatPropertiesNv,
    eOpticalFlowSessionCreateInfoNv,
    eOpticalFlowExecuteInfoNv,
    eOpticalFlowSessionCreatePrivateDataInfoNv,
    ePhysicalDeviceLegacyDitheringFeaturesExt,
    ePhysicalDevicePipelineProtectedAccessFeaturesExt,
    ePhysicalDeviceTilePropertiesFeaturesQcom,
    eTilePropertiesQcom,
    ePhysicalDeviceAmigoProfilingFeaturesSec,
    eAmigoProfilingSubmitInfoSec,
    ePhysicalDeviceMultiviewPerViewViewportsFeaturesQcom,
    eSemaphoreSciSyncPoolCreateInfoNv,
    eSemaphoreSciSyncCreateInfoNv,
    ePhysicalDeviceExternalSciSync2FeaturesNv,
    eDeviceSemaphoreSciSyncPoolReservationCreateInfoNv,
    ePhysicalDeviceRayTracingInvocationReorderFeaturesNv,
    ePhysicalDeviceRayTracingInvocationReorderPropertiesNv,
    ePhysicalDeviceShaderCoreBuiltinsFeaturesArm,
    ePhysicalDeviceShaderCoreBuiltinsPropertiesArm,
    ePhysicalDevicePipelineLibraryGroupHandlesFeaturesExt,
    ePhysicalDeviceMultiviewPerViewRenderAreasFeaturesQcom,
    eMultiviewPerViewRenderAreasRenderPassBeginInfoQcom,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StructureType {
    pub const eAttachmentDescription2Khr: Self = Self::eAttachmentDescription2;
    pub const eAttachmentDescriptionStencilLayoutKhr: Self = Self::eAttachmentDescriptionStencilLayout;
    pub const eAttachmentReference2Khr: Self = Self::eAttachmentReference2;
    pub const eAttachmentReferenceStencilLayoutKhr: Self = Self::eAttachmentReferenceStencilLayout;
    pub const eAttachmentSampleCountInfoNv: Self = Self::eAttachmentSampleCountInfoAmd;
    pub const eBindBufferMemoryDeviceGroupInfoKhr: Self = Self::eBindBufferMemoryDeviceGroupInfo;
    pub const eBindBufferMemoryInfoKhr: Self = Self::eBindBufferMemoryInfo;
    pub const eBindImageMemoryDeviceGroupInfoKhr: Self = Self::eBindImageMemoryDeviceGroupInfo;
    pub const eBindImageMemoryInfoKhr: Self = Self::eBindImageMemoryInfo;
    pub const eBindImagePlaneMemoryInfoKhr: Self = Self::eBindImagePlaneMemoryInfo;
    pub const eBlitImageInfo2Khr: Self = Self::eBlitImageInfo2;
    pub const eBufferCopy2Khr: Self = Self::eBufferCopy2;
    pub const eBufferDeviceAddressInfoExt: Self = Self::eBufferDeviceAddressInfo;
    pub const eBufferDeviceAddressInfoKhr: Self = Self::eBufferDeviceAddressInfo;
    pub const eBufferImageCopy2Khr: Self = Self::eBufferImageCopy2;
    pub const eBufferMemoryBarrier2Khr: Self = Self::eBufferMemoryBarrier2;
    pub const eBufferMemoryRequirementsInfo2Khr: Self = Self::eBufferMemoryRequirementsInfo2;
    pub const eBufferOpaqueCaptureAddressCreateInfoKhr: Self = Self::eBufferOpaqueCaptureAddressCreateInfo;
    pub const eCommandBufferInheritanceRenderingInfoKhr: Self = Self::eCommandBufferInheritanceRenderingInfo;
    pub const eCommandBufferSubmitInfoKhr: Self = Self::eCommandBufferSubmitInfo;
    pub const eCopyBufferInfo2Khr: Self = Self::eCopyBufferInfo2;
    pub const eCopyBufferToImageInfo2Khr: Self = Self::eCopyBufferToImageInfo2;
    pub const eCopyImageInfo2Khr: Self = Self::eCopyImageInfo2;
    pub const eCopyImageToBufferInfo2Khr: Self = Self::eCopyImageToBufferInfo2;
    pub const eDebugReportCreateInfoExt: Self = Self::eDebugReportCallbackCreateInfoExt;
    pub const eDependencyInfoKhr: Self = Self::eDependencyInfo;
    pub const eDescriptorPoolInlineUniformBlockCreateInfoExt: Self = Self::eDescriptorPoolInlineUniformBlockCreateInfo;
    pub const eDescriptorSetLayoutBindingFlagsCreateInfoExt: Self = Self::eDescriptorSetLayoutBindingFlagsCreateInfo;
    pub const eDescriptorSetLayoutSupportKhr: Self = Self::eDescriptorSetLayoutSupport;
    pub const eDescriptorSetVariableDescriptorCountAllocateInfoExt: Self = Self::eDescriptorSetVariableDescriptorCountAllocateInfo;
    pub const eDescriptorSetVariableDescriptorCountLayoutSupportExt: Self = Self::eDescriptorSetVariableDescriptorCountLayoutSupport;
    pub const eDescriptorUpdateTemplateCreateInfoKhr: Self = Self::eDescriptorUpdateTemplateCreateInfo;
    pub const eDeviceBufferMemoryRequirementsKhr: Self = Self::eDeviceBufferMemoryRequirements;
    pub const eDeviceGroupBindSparseInfoKhr: Self = Self::eDeviceGroupBindSparseInfo;
    pub const eDeviceGroupCommandBufferBeginInfoKhr: Self = Self::eDeviceGroupCommandBufferBeginInfo;
    pub const eDeviceGroupDeviceCreateInfoKhr: Self = Self::eDeviceGroupDeviceCreateInfo;
    pub const eDeviceGroupRenderPassBeginInfoKhr: Self = Self::eDeviceGroupRenderPassBeginInfo;
    pub const eDeviceGroupSubmitInfoKhr: Self = Self::eDeviceGroupSubmitInfo;
    pub const eDeviceImageMemoryRequirementsKhr: Self = Self::eDeviceImageMemoryRequirements;
    pub const eDeviceMemoryOpaqueCaptureAddressInfoKhr: Self = Self::eDeviceMemoryOpaqueCaptureAddressInfo;
    pub const eDevicePrivateDataCreateInfoExt: Self = Self::eDevicePrivateDataCreateInfo;
    pub const eDeviceQueueGlobalPriorityCreateInfoExt: Self = Self::eDeviceQueueGlobalPriorityCreateInfoKhr;
    pub const eExportFenceCreateInfoKhr: Self = Self::eExportFenceCreateInfo;
    pub const eExportMemoryAllocateInfoKhr: Self = Self::eExportMemoryAllocateInfo;
    pub const eExportSemaphoreCreateInfoKhr: Self = Self::eExportSemaphoreCreateInfo;
    pub const eExternalBufferPropertiesKhr: Self = Self::eExternalBufferProperties;
    pub const eExternalFencePropertiesKhr: Self = Self::eExternalFenceProperties;
    pub const eExternalImageFormatPropertiesKhr: Self = Self::eExternalImageFormatProperties;
    pub const eExternalMemoryBufferCreateInfoKhr: Self = Self::eExternalMemoryBufferCreateInfo;
    pub const eExternalMemoryImageCreateInfoKhr: Self = Self::eExternalMemoryImageCreateInfo;
    pub const eExternalSemaphorePropertiesKhr: Self = Self::eExternalSemaphoreProperties;
    pub const eFormatProperties2Khr: Self = Self::eFormatProperties2;
    pub const eFormatProperties3Khr: Self = Self::eFormatProperties3;
    pub const eFramebufferAttachmentsCreateInfoKhr: Self = Self::eFramebufferAttachmentsCreateInfo;
    pub const eFramebufferAttachmentImageInfoKhr: Self = Self::eFramebufferAttachmentImageInfo;
    pub const eImageBlit2Khr: Self = Self::eImageBlit2;
    pub const eImageCopy2Khr: Self = Self::eImageCopy2;
    pub const eImageFormatListCreateInfoKhr: Self = Self::eImageFormatListCreateInfo;
    pub const eImageFormatProperties2Khr: Self = Self::eImageFormatProperties2;
    pub const eImageMemoryBarrier2Khr: Self = Self::eImageMemoryBarrier2;
    pub const eImageMemoryRequirementsInfo2Khr: Self = Self::eImageMemoryRequirementsInfo2;
    pub const eImagePlaneMemoryRequirementsInfoKhr: Self = Self::eImagePlaneMemoryRequirementsInfo;
    pub const eImageResolve2Khr: Self = Self::eImageResolve2;
    pub const eImageSparseMemoryRequirementsInfo2Khr: Self = Self::eImageSparseMemoryRequirementsInfo2;
    pub const eImageStencilUsageCreateInfoExt: Self = Self::eImageStencilUsageCreateInfo;
    pub const eImageViewUsageCreateInfoKhr: Self = Self::eImageViewUsageCreateInfo;
    pub const eMemoryAllocateFlagsInfoKhr: Self = Self::eMemoryAllocateFlagsInfo;
    pub const eMemoryBarrier2Khr: Self = Self::eMemoryBarrier2;
    pub const eMemoryDedicatedAllocateInfoKhr: Self = Self::eMemoryDedicatedAllocateInfo;
    pub const eMemoryDedicatedRequirementsKhr: Self = Self::eMemoryDedicatedRequirements;
    pub const eMemoryOpaqueCaptureAddressAllocateInfoKhr: Self = Self::eMemoryOpaqueCaptureAddressAllocateInfo;
    pub const eMemoryRequirements2Khr: Self = Self::eMemoryRequirements2;
    pub const eMutableDescriptorTypeCreateInfoValve: Self = Self::eMutableDescriptorTypeCreateInfoExt;
    pub const ePhysicalDevice16bitStorageFeaturesKhr: Self = Self::ePhysicalDevice16bitStorageFeatures;
    pub const ePhysicalDevice8bitStorageFeaturesKhr: Self = Self::ePhysicalDevice8bitStorageFeatures;
    pub const ePhysicalDeviceBufferDeviceAddressFeaturesKhr: Self = Self::ePhysicalDeviceBufferDeviceAddressFeatures;
    pub const ePhysicalDeviceBufferAddressFeaturesExt: Self = Self::ePhysicalDeviceBufferDeviceAddressFeaturesExt;
    pub const ePhysicalDeviceDepthStencilResolvePropertiesKhr: Self = Self::ePhysicalDeviceDepthStencilResolveProperties;
    pub const ePhysicalDeviceDescriptorIndexingFeaturesExt: Self = Self::ePhysicalDeviceDescriptorIndexingFeatures;
    pub const ePhysicalDeviceDescriptorIndexingPropertiesExt: Self = Self::ePhysicalDeviceDescriptorIndexingProperties;
    pub const ePhysicalDeviceDriverPropertiesKhr: Self = Self::ePhysicalDeviceDriverProperties;
    pub const ePhysicalDeviceDynamicRenderingFeaturesKhr: Self = Self::ePhysicalDeviceDynamicRenderingFeatures;
    pub const ePhysicalDeviceExternalBufferInfoKhr: Self = Self::ePhysicalDeviceExternalBufferInfo;
    pub const ePhysicalDeviceExternalFenceInfoKhr: Self = Self::ePhysicalDeviceExternalFenceInfo;
    pub const ePhysicalDeviceExternalImageFormatInfoKhr: Self = Self::ePhysicalDeviceExternalImageFormatInfo;
    pub const ePhysicalDeviceExternalSemaphoreInfoKhr: Self = Self::ePhysicalDeviceExternalSemaphoreInfo;
    pub const ePhysicalDeviceFeatures2Khr: Self = Self::ePhysicalDeviceFeatures2;
    pub const ePhysicalDeviceFloatControlsPropertiesKhr: Self = Self::ePhysicalDeviceFloatControlsProperties;
    pub const ePhysicalDeviceFragmentShaderBarycentricFeaturesNv: Self = Self::ePhysicalDeviceFragmentShaderBarycentricFeaturesKhr;
    pub const ePhysicalDeviceGlobalPriorityQueryFeaturesExt: Self = Self::ePhysicalDeviceGlobalPriorityQueryFeaturesKhr;
    pub const ePhysicalDeviceGroupPropertiesKhr: Self = Self::ePhysicalDeviceGroupProperties;
    pub const ePhysicalDeviceHostQueryResetFeaturesExt: Self = Self::ePhysicalDeviceHostQueryResetFeatures;
    pub const ePhysicalDeviceIdPropertiesKhr: Self = Self::ePhysicalDeviceIdProperties;
    pub const ePhysicalDeviceImagelessFramebufferFeaturesKhr: Self = Self::ePhysicalDeviceImagelessFramebufferFeatures;
    pub const ePhysicalDeviceImageFormatInfo2Khr: Self = Self::ePhysicalDeviceImageFormatInfo2;
    pub const ePhysicalDeviceImageRobustnessFeaturesExt: Self = Self::ePhysicalDeviceImageRobustnessFeatures;
    pub const ePhysicalDeviceInlineUniformBlockFeaturesExt: Self = Self::ePhysicalDeviceInlineUniformBlockFeatures;
    pub const ePhysicalDeviceInlineUniformBlockPropertiesExt: Self = Self::ePhysicalDeviceInlineUniformBlockProperties;
    pub const ePhysicalDeviceMaintenance3PropertiesKhr: Self = Self::ePhysicalDeviceMaintenance3Properties;
    pub const ePhysicalDeviceMaintenance4FeaturesKhr: Self = Self::ePhysicalDeviceMaintenance4Features;
    pub const ePhysicalDeviceMaintenance4PropertiesKhr: Self = Self::ePhysicalDeviceMaintenance4Properties;
    pub const ePhysicalDeviceMemoryProperties2Khr: Self = Self::ePhysicalDeviceMemoryProperties2;
    pub const ePhysicalDeviceMultiviewFeaturesKhr: Self = Self::ePhysicalDeviceMultiviewFeatures;
    pub const ePhysicalDeviceMultiviewPropertiesKhr: Self = Self::ePhysicalDeviceMultiviewProperties;
    pub const ePhysicalDeviceMutableDescriptorTypeFeaturesValve: Self = Self::ePhysicalDeviceMutableDescriptorTypeFeaturesExt;
    pub const ePhysicalDevicePipelineCreationCacheControlFeaturesExt: Self = Self::ePhysicalDevicePipelineCreationCacheControlFeatures;
    pub const ePhysicalDevicePointClippingPropertiesKhr: Self = Self::ePhysicalDevicePointClippingProperties;
    pub const ePhysicalDevicePrivateDataFeaturesExt: Self = Self::ePhysicalDevicePrivateDataFeatures;
    pub const ePhysicalDeviceProperties2Khr: Self = Self::ePhysicalDeviceProperties2;
    pub const ePhysicalDeviceRasterizationOrderAttachmentAccessFeaturesArm: Self = Self::ePhysicalDeviceRasterizationOrderAttachmentAccessFeaturesExt;
    pub const ePhysicalDeviceSamplerFilterMinmaxPropertiesExt: Self = Self::ePhysicalDeviceSamplerFilterMinmaxProperties;
    pub const ePhysicalDeviceSamplerYcbcrConversionFeaturesKhr: Self = Self::ePhysicalDeviceSamplerYcbcrConversionFeatures;
    pub const ePhysicalDeviceScalarBlockLayoutFeaturesExt: Self = Self::ePhysicalDeviceScalarBlockLayoutFeatures;
    pub const ePhysicalDeviceSeparateDepthStencilLayoutsFeaturesKhr: Self = Self::ePhysicalDeviceSeparateDepthStencilLayoutsFeatures;
    pub const ePhysicalDeviceShaderAtomicInt64FeaturesKhr: Self = Self::ePhysicalDeviceShaderAtomicInt64Features;
    pub const ePhysicalDeviceShaderDemoteToHelperInvocationFeaturesExt: Self = Self::ePhysicalDeviceShaderDemoteToHelperInvocationFeatures;
    pub const ePhysicalDeviceShaderDrawParameterFeatures: Self = Self::ePhysicalDeviceShaderDrawParametersFeatures;
    pub const ePhysicalDeviceFloat16Int8FeaturesKhr: Self = Self::ePhysicalDeviceShaderFloat16Int8Features;
    pub const ePhysicalDeviceShaderFloat16Int8FeaturesKhr: Self = Self::ePhysicalDeviceShaderFloat16Int8Features;
    pub const ePhysicalDeviceShaderIntegerDotProductFeaturesKhr: Self = Self::ePhysicalDeviceShaderIntegerDotProductFeatures;
    pub const ePhysicalDeviceShaderIntegerDotProductPropertiesKhr: Self = Self::ePhysicalDeviceShaderIntegerDotProductProperties;
    pub const ePhysicalDeviceShaderSubgroupExtendedTypesFeaturesKhr: Self = Self::ePhysicalDeviceShaderSubgroupExtendedTypesFeatures;
    pub const ePhysicalDeviceShaderTerminateInvocationFeaturesKhr: Self = Self::ePhysicalDeviceShaderTerminateInvocationFeatures;
    pub const ePhysicalDeviceSparseImageFormatInfo2Khr: Self = Self::ePhysicalDeviceSparseImageFormatInfo2;
    pub const ePhysicalDeviceSubgroupSizeControlFeaturesExt: Self = Self::ePhysicalDeviceSubgroupSizeControlFeatures;
    pub const ePhysicalDeviceSubgroupSizeControlPropertiesExt: Self = Self::ePhysicalDeviceSubgroupSizeControlProperties;
    pub const ePhysicalDeviceSynchronization2FeaturesKhr: Self = Self::ePhysicalDeviceSynchronization2Features;
    pub const ePhysicalDeviceTexelBufferAlignmentPropertiesExt: Self = Self::ePhysicalDeviceTexelBufferAlignmentProperties;
    pub const ePhysicalDeviceTextureCompressionAstcHdrFeaturesExt: Self = Self::ePhysicalDeviceTextureCompressionAstcHdrFeatures;
    pub const ePhysicalDeviceTimelineSemaphoreFeaturesKhr: Self = Self::ePhysicalDeviceTimelineSemaphoreFeatures;
    pub const ePhysicalDeviceTimelineSemaphorePropertiesKhr: Self = Self::ePhysicalDeviceTimelineSemaphoreProperties;
    pub const ePhysicalDeviceToolPropertiesExt: Self = Self::ePhysicalDeviceToolProperties;
    pub const ePhysicalDeviceUniformBufferStandardLayoutFeaturesKhr: Self = Self::ePhysicalDeviceUniformBufferStandardLayoutFeatures;
    pub const ePhysicalDeviceVariablePointersFeaturesKhr: Self = Self::ePhysicalDeviceVariablePointersFeatures;
    pub const ePhysicalDeviceVariablePointerFeatures: Self = Self::ePhysicalDeviceVariablePointersFeatures;
    pub const ePhysicalDeviceVariablePointerFeaturesKhr: Self = Self::ePhysicalDeviceVariablePointersFeaturesKhr;
    pub const ePhysicalDeviceVulkanMemoryModelFeaturesKhr: Self = Self::ePhysicalDeviceVulkanMemoryModelFeatures;
    pub const ePhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKhr: Self = Self::ePhysicalDeviceZeroInitializeWorkgroupMemoryFeatures;
    pub const ePipelineCreationFeedbackCreateInfoExt: Self = Self::ePipelineCreationFeedbackCreateInfo;
    pub const ePipelineInfoExt: Self = Self::ePipelineInfoKhr;
    pub const ePipelineRenderingCreateInfoKhr: Self = Self::ePipelineRenderingCreateInfo;
    pub const ePipelineShaderStageRequiredSubgroupSizeCreateInfoExt: Self = Self::ePipelineShaderStageRequiredSubgroupSizeCreateInfo;
    pub const ePipelineTessellationDomainOriginStateCreateInfoKhr: Self = Self::ePipelineTessellationDomainOriginStateCreateInfo;
    pub const ePrivateDataSlotCreateInfoExt: Self = Self::ePrivateDataSlotCreateInfo;
    pub const eQueryPoolCreateInfoIntel: Self = Self::eQueryPoolPerformanceQueryCreateInfoIntel;
    pub const eQueueFamilyGlobalPriorityPropertiesExt: Self = Self::eQueueFamilyGlobalPriorityPropertiesKhr;
    pub const eQueueFamilyProperties2Khr: Self = Self::eQueueFamilyProperties2;
    pub const eRenderingAttachmentInfoKhr: Self = Self::eRenderingAttachmentInfo;
    pub const eRenderingInfoKhr: Self = Self::eRenderingInfo;
    pub const eRenderPassAttachmentBeginInfoKhr: Self = Self::eRenderPassAttachmentBeginInfo;
    pub const eRenderPassCreateInfo2Khr: Self = Self::eRenderPassCreateInfo2;
    pub const eRenderPassInputAttachmentAspectCreateInfoKhr: Self = Self::eRenderPassInputAttachmentAspectCreateInfo;
    pub const eRenderPassMultiviewCreateInfoKhr: Self = Self::eRenderPassMultiviewCreateInfo;
    pub const eResolveImageInfo2Khr: Self = Self::eResolveImageInfo2;
    pub const eSamplerReductionModeCreateInfoExt: Self = Self::eSamplerReductionModeCreateInfo;
    pub const eSamplerYcbcrConversionCreateInfoKhr: Self = Self::eSamplerYcbcrConversionCreateInfo;
    pub const eSamplerYcbcrConversionImageFormatPropertiesKhr: Self = Self::eSamplerYcbcrConversionImageFormatProperties;
    pub const eSamplerYcbcrConversionInfoKhr: Self = Self::eSamplerYcbcrConversionInfo;
    pub const eSemaphoreSignalInfoKhr: Self = Self::eSemaphoreSignalInfo;
    pub const eSemaphoreSubmitInfoKhr: Self = Self::eSemaphoreSubmitInfo;
    pub const eSemaphoreTypeCreateInfoKhr: Self = Self::eSemaphoreTypeCreateInfo;
    pub const eSemaphoreWaitInfoKhr: Self = Self::eSemaphoreWaitInfo;
    pub const eSparseImageFormatProperties2Khr: Self = Self::eSparseImageFormatProperties2;
    pub const eSparseImageMemoryRequirements2Khr: Self = Self::eSparseImageMemoryRequirements2;
    pub const eSubmitInfo2Khr: Self = Self::eSubmitInfo2;
    pub const eSubpassBeginInfoKhr: Self = Self::eSubpassBeginInfo;
    pub const eSubpassDependency2Khr: Self = Self::eSubpassDependency2;
    pub const eSubpassDescription2Khr: Self = Self::eSubpassDescription2;
    pub const eSubpassDescriptionDepthStencilResolveKhr: Self = Self::eSubpassDescriptionDepthStencilResolve;
    pub const eSubpassEndInfoKhr: Self = Self::eSubpassEndInfo;
    pub const eSurfaceCapabilities2Ext: Self = Self::eSurfaceCapabilities2Ext;
    pub const eTimelineSemaphoreSubmitInfoKhr: Self = Self::eTimelineSemaphoreSubmitInfo;
    pub const eWriteDescriptorSetInlineUniformBlockExt: Self = Self::eWriteDescriptorSetInlineUniformBlock;

    pub fn into_raw(self) -> RawStructureType {
        match self {
            Self::eApplicationInfo => RawStructureType::eApplicationInfo,
            Self::eInstanceCreateInfo => RawStructureType::eInstanceCreateInfo,
            Self::eDeviceQueueCreateInfo => RawStructureType::eDeviceQueueCreateInfo,
            Self::eDeviceCreateInfo => RawStructureType::eDeviceCreateInfo,
            Self::eSubmitInfo => RawStructureType::eSubmitInfo,
            Self::eMemoryAllocateInfo => RawStructureType::eMemoryAllocateInfo,
            Self::eMappedMemoryRange => RawStructureType::eMappedMemoryRange,
            Self::eBindSparseInfo => RawStructureType::eBindSparseInfo,
            Self::eFenceCreateInfo => RawStructureType::eFenceCreateInfo,
            Self::eSemaphoreCreateInfo => RawStructureType::eSemaphoreCreateInfo,
            Self::eEventCreateInfo => RawStructureType::eEventCreateInfo,
            Self::eQueryPoolCreateInfo => RawStructureType::eQueryPoolCreateInfo,
            Self::eBufferCreateInfo => RawStructureType::eBufferCreateInfo,
            Self::eBufferViewCreateInfo => RawStructureType::eBufferViewCreateInfo,
            Self::eImageCreateInfo => RawStructureType::eImageCreateInfo,
            Self::eImageViewCreateInfo => RawStructureType::eImageViewCreateInfo,
            Self::eShaderModuleCreateInfo => RawStructureType::eShaderModuleCreateInfo,
            Self::ePipelineCacheCreateInfo => RawStructureType::ePipelineCacheCreateInfo,
            Self::ePipelineShaderStageCreateInfo => RawStructureType::ePipelineShaderStageCreateInfo,
            Self::ePipelineVertexInputStateCreateInfo => RawStructureType::ePipelineVertexInputStateCreateInfo,
            Self::ePipelineInputAssemblyStateCreateInfo => RawStructureType::ePipelineInputAssemblyStateCreateInfo,
            Self::ePipelineTessellationStateCreateInfo => RawStructureType::ePipelineTessellationStateCreateInfo,
            Self::ePipelineViewportStateCreateInfo => RawStructureType::ePipelineViewportStateCreateInfo,
            Self::ePipelineRasterizationStateCreateInfo => RawStructureType::ePipelineRasterizationStateCreateInfo,
            Self::ePipelineMultisampleStateCreateInfo => RawStructureType::ePipelineMultisampleStateCreateInfo,
            Self::ePipelineDepthStencilStateCreateInfo => RawStructureType::ePipelineDepthStencilStateCreateInfo,
            Self::ePipelineColourBlendStateCreateInfo => RawStructureType::ePipelineColourBlendStateCreateInfo,
            Self::ePipelineDynamicStateCreateInfo => RawStructureType::ePipelineDynamicStateCreateInfo,
            Self::eGraphicsPipelineCreateInfo => RawStructureType::eGraphicsPipelineCreateInfo,
            Self::eComputePipelineCreateInfo => RawStructureType::eComputePipelineCreateInfo,
            Self::ePipelineLayoutCreateInfo => RawStructureType::ePipelineLayoutCreateInfo,
            Self::eSamplerCreateInfo => RawStructureType::eSamplerCreateInfo,
            Self::eDescriptorSetLayoutCreateInfo => RawStructureType::eDescriptorSetLayoutCreateInfo,
            Self::eDescriptorPoolCreateInfo => RawStructureType::eDescriptorPoolCreateInfo,
            Self::eDescriptorSetAllocateInfo => RawStructureType::eDescriptorSetAllocateInfo,
            Self::eWriteDescriptorSet => RawStructureType::eWriteDescriptorSet,
            Self::eCopyDescriptorSet => RawStructureType::eCopyDescriptorSet,
            Self::eFramebufferCreateInfo => RawStructureType::eFramebufferCreateInfo,
            Self::eRenderPassCreateInfo => RawStructureType::eRenderPassCreateInfo,
            Self::eCommandPoolCreateInfo => RawStructureType::eCommandPoolCreateInfo,
            Self::eCommandBufferAllocateInfo => RawStructureType::eCommandBufferAllocateInfo,
            Self::eCommandBufferInheritanceInfo => RawStructureType::eCommandBufferInheritanceInfo,
            Self::eCommandBufferBeginInfo => RawStructureType::eCommandBufferBeginInfo,
            Self::eRenderPassBeginInfo => RawStructureType::eRenderPassBeginInfo,
            Self::eBufferMemoryBarrier => RawStructureType::eBufferMemoryBarrier,
            Self::eImageMemoryBarrier => RawStructureType::eImageMemoryBarrier,
            Self::eMemoryBarrier => RawStructureType::eMemoryBarrier,
            Self::eLoaderInstanceCreateInfo => RawStructureType::eLoaderInstanceCreateInfo,
            Self::eLoaderDeviceCreateInfo => RawStructureType::eLoaderDeviceCreateInfo,
            Self::ePhysicalDeviceVulkan11Features => RawStructureType::ePhysicalDeviceVulkan11Features,
            Self::ePhysicalDeviceVulkan11Properties => RawStructureType::ePhysicalDeviceVulkan11Properties,
            Self::ePhysicalDeviceVulkan12Features => RawStructureType::ePhysicalDeviceVulkan12Features,
            Self::ePhysicalDeviceVulkan12Properties => RawStructureType::ePhysicalDeviceVulkan12Properties,
            Self::ePhysicalDeviceVulkan13Features => RawStructureType::ePhysicalDeviceVulkan13Features,
            Self::ePhysicalDeviceVulkan13Properties => RawStructureType::ePhysicalDeviceVulkan13Properties,
            Self::eSwapchainCreateInfoKhr => RawStructureType::eSwapchainCreateInfoKhr,
            Self::ePresentInfoKhr => RawStructureType::ePresentInfoKhr,
            Self::eDisplayModeCreateInfoKhr => RawStructureType::eDisplayModeCreateInfoKhr,
            Self::eDisplaySurfaceCreateInfoKhr => RawStructureType::eDisplaySurfaceCreateInfoKhr,
            Self::eDisplayPresentInfoKhr => RawStructureType::eDisplayPresentInfoKhr,
            Self::eXlibSurfaceCreateInfoKhr => RawStructureType::eXlibSurfaceCreateInfoKhr,
            Self::eXcbSurfaceCreateInfoKhr => RawStructureType::eXcbSurfaceCreateInfoKhr,
            Self::eWaylandSurfaceCreateInfoKhr => RawStructureType::eWaylandSurfaceCreateInfoKhr,
            Self::eAndroidSurfaceCreateInfoKhr => RawStructureType::eAndroidSurfaceCreateInfoKhr,
            Self::eWin32SurfaceCreateInfoKhr => RawStructureType::eWin32SurfaceCreateInfoKhr,
            Self::eDebugReportCallbackCreateInfoExt => RawStructureType::eDebugReportCallbackCreateInfoExt,
            Self::ePipelineRasterizationStateRasterizationOrderAmd => RawStructureType::ePipelineRasterizationStateRasterizationOrderAmd,
            Self::eDebugMarkerObjectNameInfoExt => RawStructureType::eDebugMarkerObjectNameInfoExt,
            Self::eDebugMarkerObjectTagInfoExt => RawStructureType::eDebugMarkerObjectTagInfoExt,
            Self::eDebugMarkerMarkerInfoExt => RawStructureType::eDebugMarkerMarkerInfoExt,
            Self::eVideoProfileInfoKhr => RawStructureType::eVideoProfileInfoKhr,
            Self::eVideoCapabilitiesKhr => RawStructureType::eVideoCapabilitiesKhr,
            Self::eVideoPictureResourceInfoKhr => RawStructureType::eVideoPictureResourceInfoKhr,
            Self::eVideoSessionMemoryRequirementsKhr => RawStructureType::eVideoSessionMemoryRequirementsKhr,
            Self::eBindVideoSessionMemoryInfoKhr => RawStructureType::eBindVideoSessionMemoryInfoKhr,
            Self::eVideoSessionCreateInfoKhr => RawStructureType::eVideoSessionCreateInfoKhr,
            Self::eVideoSessionParametersCreateInfoKhr => RawStructureType::eVideoSessionParametersCreateInfoKhr,
            Self::eVideoSessionParametersUpdateInfoKhr => RawStructureType::eVideoSessionParametersUpdateInfoKhr,
            Self::eVideoBeginCodingInfoKhr => RawStructureType::eVideoBeginCodingInfoKhr,
            Self::eVideoEndCodingInfoKhr => RawStructureType::eVideoEndCodingInfoKhr,
            Self::eVideoCodingControlInfoKhr => RawStructureType::eVideoCodingControlInfoKhr,
            Self::eVideoReferenceSlotInfoKhr => RawStructureType::eVideoReferenceSlotInfoKhr,
            Self::eQueueFamilyVideoPropertiesKhr => RawStructureType::eQueueFamilyVideoPropertiesKhr,
            Self::eVideoProfileListInfoKhr => RawStructureType::eVideoProfileListInfoKhr,
            Self::ePhysicalDeviceVideoFormatInfoKhr => RawStructureType::ePhysicalDeviceVideoFormatInfoKhr,
            Self::eVideoFormatPropertiesKhr => RawStructureType::eVideoFormatPropertiesKhr,
            Self::eQueueFamilyQueryResultStatusPropertiesKhr => RawStructureType::eQueueFamilyQueryResultStatusPropertiesKhr,
            Self::eVideoDecodeInfoKhr => RawStructureType::eVideoDecodeInfoKhr,
            Self::eVideoDecodeCapabilitiesKhr => RawStructureType::eVideoDecodeCapabilitiesKhr,
            Self::eVideoDecodeUsageInfoKhr => RawStructureType::eVideoDecodeUsageInfoKhr,
            Self::eDedicatedAllocationImageCreateInfoNv => RawStructureType::eDedicatedAllocationImageCreateInfoNv,
            Self::eDedicatedAllocationBufferCreateInfoNv => RawStructureType::eDedicatedAllocationBufferCreateInfoNv,
            Self::eDedicatedAllocationMemoryAllocateInfoNv => RawStructureType::eDedicatedAllocationMemoryAllocateInfoNv,
            Self::ePhysicalDeviceTransformFeedbackFeaturesExt => RawStructureType::ePhysicalDeviceTransformFeedbackFeaturesExt,
            Self::ePhysicalDeviceTransformFeedbackPropertiesExt => RawStructureType::ePhysicalDeviceTransformFeedbackPropertiesExt,
            Self::ePipelineRasterizationStateStreamCreateInfoExt => RawStructureType::ePipelineRasterizationStateStreamCreateInfoExt,
            Self::eCuModuleCreateInfoNvx => RawStructureType::eCuModuleCreateInfoNvx,
            Self::eCuFunctionCreateInfoNvx => RawStructureType::eCuFunctionCreateInfoNvx,
            Self::eCuLaunchInfoNvx => RawStructureType::eCuLaunchInfoNvx,
            Self::eImageViewHandleInfoNvx => RawStructureType::eImageViewHandleInfoNvx,
            Self::eImageViewAddressPropertiesNvx => RawStructureType::eImageViewAddressPropertiesNvx,
            Self::eVideoEncodeH264CapabilitiesExt => RawStructureType::eVideoEncodeH264CapabilitiesExt,
            Self::eVideoEncodeH264SessionParametersCreateInfoExt => RawStructureType::eVideoEncodeH264SessionParametersCreateInfoExt,
            Self::eVideoEncodeH264SessionParametersAddInfoExt => RawStructureType::eVideoEncodeH264SessionParametersAddInfoExt,
            Self::eVideoEncodeH264VclFrameInfoExt => RawStructureType::eVideoEncodeH264VclFrameInfoExt,
            Self::eVideoEncodeH264DpbSlotInfoExt => RawStructureType::eVideoEncodeH264DpbSlotInfoExt,
            Self::eVideoEncodeH264NaluSliceInfoExt => RawStructureType::eVideoEncodeH264NaluSliceInfoExt,
            Self::eVideoEncodeH264EmitPictureParametersInfoExt => RawStructureType::eVideoEncodeH264EmitPictureParametersInfoExt,
            Self::eVideoEncodeH264ProfileInfoExt => RawStructureType::eVideoEncodeH264ProfileInfoExt,
            Self::eVideoEncodeH264RateControlInfoExt => RawStructureType::eVideoEncodeH264RateControlInfoExt,
            Self::eVideoEncodeH264RateControlLayerInfoExt => RawStructureType::eVideoEncodeH264RateControlLayerInfoExt,
            Self::eVideoEncodeH264ReferenceListsInfoExt => RawStructureType::eVideoEncodeH264ReferenceListsInfoExt,
            Self::eVideoEncodeH265CapabilitiesExt => RawStructureType::eVideoEncodeH265CapabilitiesExt,
            Self::eVideoEncodeH265SessionParametersCreateInfoExt => RawStructureType::eVideoEncodeH265SessionParametersCreateInfoExt,
            Self::eVideoEncodeH265SessionParametersAddInfoExt => RawStructureType::eVideoEncodeH265SessionParametersAddInfoExt,
            Self::eVideoEncodeH265VclFrameInfoExt => RawStructureType::eVideoEncodeH265VclFrameInfoExt,
            Self::eVideoEncodeH265DpbSlotInfoExt => RawStructureType::eVideoEncodeH265DpbSlotInfoExt,
            Self::eVideoEncodeH265NaluSliceSegmentInfoExt => RawStructureType::eVideoEncodeH265NaluSliceSegmentInfoExt,
            Self::eVideoEncodeH265EmitPictureParametersInfoExt => RawStructureType::eVideoEncodeH265EmitPictureParametersInfoExt,
            Self::eVideoEncodeH265ProfileInfoExt => RawStructureType::eVideoEncodeH265ProfileInfoExt,
            Self::eVideoEncodeH265ReferenceListsInfoExt => RawStructureType::eVideoEncodeH265ReferenceListsInfoExt,
            Self::eVideoEncodeH265RateControlInfoExt => RawStructureType::eVideoEncodeH265RateControlInfoExt,
            Self::eVideoEncodeH265RateControlLayerInfoExt => RawStructureType::eVideoEncodeH265RateControlLayerInfoExt,
            Self::eVideoDecodeH264CapabilitiesKhr => RawStructureType::eVideoDecodeH264CapabilitiesKhr,
            Self::eVideoDecodeH264PictureInfoKhr => RawStructureType::eVideoDecodeH264PictureInfoKhr,
            Self::eVideoDecodeH264ProfileInfoKhr => RawStructureType::eVideoDecodeH264ProfileInfoKhr,
            Self::eVideoDecodeH264SessionParametersCreateInfoKhr => RawStructureType::eVideoDecodeH264SessionParametersCreateInfoKhr,
            Self::eVideoDecodeH264SessionParametersAddInfoKhr => RawStructureType::eVideoDecodeH264SessionParametersAddInfoKhr,
            Self::eVideoDecodeH264DpbSlotInfoKhr => RawStructureType::eVideoDecodeH264DpbSlotInfoKhr,
            Self::eTextureLodGatherFormatPropertiesAmd => RawStructureType::eTextureLodGatherFormatPropertiesAmd,
            Self::eRenderingInfo => RawStructureType::eRenderingInfo,
            Self::eRenderingAttachmentInfo => RawStructureType::eRenderingAttachmentInfo,
            Self::ePipelineRenderingCreateInfo => RawStructureType::ePipelineRenderingCreateInfo,
            Self::ePhysicalDeviceDynamicRenderingFeatures => RawStructureType::ePhysicalDeviceDynamicRenderingFeatures,
            Self::eCommandBufferInheritanceRenderingInfo => RawStructureType::eCommandBufferInheritanceRenderingInfo,
            Self::eRenderingFragmentShadingRateAttachmentInfoKhr => RawStructureType::eRenderingFragmentShadingRateAttachmentInfoKhr,
            Self::eRenderingFragmentDensityMapAttachmentInfoExt => RawStructureType::eRenderingFragmentDensityMapAttachmentInfoExt,
            Self::eAttachmentSampleCountInfoAmd => RawStructureType::eAttachmentSampleCountInfoAmd,
            Self::eMultiviewPerViewAttributesInfoNvx => RawStructureType::eMultiviewPerViewAttributesInfoNvx,
            Self::eStreamDescriptorSurfaceCreateInfoGgp => RawStructureType::eStreamDescriptorSurfaceCreateInfoGgp,
            Self::ePhysicalDeviceCornerSampledImageFeaturesNv => RawStructureType::ePhysicalDeviceCornerSampledImageFeaturesNv,
            Self::ePrivateVendorInfoReservedOffset0Nv => RawStructureType::ePrivateVendorInfoReservedOffset0Nv,
            Self::eRenderPassMultiviewCreateInfo => RawStructureType::eRenderPassMultiviewCreateInfo,
            Self::ePhysicalDeviceMultiviewFeatures => RawStructureType::ePhysicalDeviceMultiviewFeatures,
            Self::ePhysicalDeviceMultiviewProperties => RawStructureType::ePhysicalDeviceMultiviewProperties,
            Self::eExternalMemoryImageCreateInfoNv => RawStructureType::eExternalMemoryImageCreateInfoNv,
            Self::eExportMemoryAllocateInfoNv => RawStructureType::eExportMemoryAllocateInfoNv,
            Self::eImportMemoryWin32HandleInfoNv => RawStructureType::eImportMemoryWin32HandleInfoNv,
            Self::eExportMemoryWin32HandleInfoNv => RawStructureType::eExportMemoryWin32HandleInfoNv,
            Self::eWin32KeyedMutexAcquireReleaseInfoNv => RawStructureType::eWin32KeyedMutexAcquireReleaseInfoNv,
            Self::ePhysicalDeviceFeatures2 => RawStructureType::ePhysicalDeviceFeatures2,
            Self::ePhysicalDeviceProperties2 => RawStructureType::ePhysicalDeviceProperties2,
            Self::eFormatProperties2 => RawStructureType::eFormatProperties2,
            Self::eImageFormatProperties2 => RawStructureType::eImageFormatProperties2,
            Self::ePhysicalDeviceImageFormatInfo2 => RawStructureType::ePhysicalDeviceImageFormatInfo2,
            Self::eQueueFamilyProperties2 => RawStructureType::eQueueFamilyProperties2,
            Self::ePhysicalDeviceMemoryProperties2 => RawStructureType::ePhysicalDeviceMemoryProperties2,
            Self::eSparseImageFormatProperties2 => RawStructureType::eSparseImageFormatProperties2,
            Self::ePhysicalDeviceSparseImageFormatInfo2 => RawStructureType::ePhysicalDeviceSparseImageFormatInfo2,
            Self::eMemoryAllocateFlagsInfo => RawStructureType::eMemoryAllocateFlagsInfo,
            Self::eDeviceGroupRenderPassBeginInfo => RawStructureType::eDeviceGroupRenderPassBeginInfo,
            Self::eDeviceGroupCommandBufferBeginInfo => RawStructureType::eDeviceGroupCommandBufferBeginInfo,
            Self::eDeviceGroupSubmitInfo => RawStructureType::eDeviceGroupSubmitInfo,
            Self::eDeviceGroupBindSparseInfo => RawStructureType::eDeviceGroupBindSparseInfo,
            Self::eDeviceGroupPresentCapabilitiesKhr => RawStructureType::eDeviceGroupPresentCapabilitiesKhr,
            Self::eImageSwapchainCreateInfoKhr => RawStructureType::eImageSwapchainCreateInfoKhr,
            Self::eBindImageMemorySwapchainInfoKhr => RawStructureType::eBindImageMemorySwapchainInfoKhr,
            Self::eAcquireNextImageInfoKhr => RawStructureType::eAcquireNextImageInfoKhr,
            Self::eDeviceGroupPresentInfoKhr => RawStructureType::eDeviceGroupPresentInfoKhr,
            Self::eDeviceGroupSwapchainCreateInfoKhr => RawStructureType::eDeviceGroupSwapchainCreateInfoKhr,
            Self::eBindBufferMemoryDeviceGroupInfo => RawStructureType::eBindBufferMemoryDeviceGroupInfo,
            Self::eBindImageMemoryDeviceGroupInfo => RawStructureType::eBindImageMemoryDeviceGroupInfo,
            Self::eValidationFlagsExt => RawStructureType::eValidationFlagsExt,
            Self::eViSurfaceCreateInfoNn => RawStructureType::eViSurfaceCreateInfoNn,
            Self::ePhysicalDeviceShaderDrawParametersFeatures => RawStructureType::ePhysicalDeviceShaderDrawParametersFeatures,
            Self::ePhysicalDeviceTextureCompressionAstcHdrFeatures => RawStructureType::ePhysicalDeviceTextureCompressionAstcHdrFeatures,
            Self::eImageViewAstcDecodeModeExt => RawStructureType::eImageViewAstcDecodeModeExt,
            Self::ePhysicalDeviceAstcDecodeFeaturesExt => RawStructureType::ePhysicalDeviceAstcDecodeFeaturesExt,
            Self::ePipelineRobustnessCreateInfoExt => RawStructureType::ePipelineRobustnessCreateInfoExt,
            Self::ePhysicalDevicePipelineRobustnessFeaturesExt => RawStructureType::ePhysicalDevicePipelineRobustnessFeaturesExt,
            Self::ePhysicalDevicePipelineRobustnessPropertiesExt => RawStructureType::ePhysicalDevicePipelineRobustnessPropertiesExt,
            Self::ePhysicalDeviceGroupProperties => RawStructureType::ePhysicalDeviceGroupProperties,
            Self::eDeviceGroupDeviceCreateInfo => RawStructureType::eDeviceGroupDeviceCreateInfo,
            Self::ePhysicalDeviceExternalImageFormatInfo => RawStructureType::ePhysicalDeviceExternalImageFormatInfo,
            Self::eExternalImageFormatProperties => RawStructureType::eExternalImageFormatProperties,
            Self::ePhysicalDeviceExternalBufferInfo => RawStructureType::ePhysicalDeviceExternalBufferInfo,
            Self::eExternalBufferProperties => RawStructureType::eExternalBufferProperties,
            Self::ePhysicalDeviceIdProperties => RawStructureType::ePhysicalDeviceIdProperties,
            Self::eExternalMemoryBufferCreateInfo => RawStructureType::eExternalMemoryBufferCreateInfo,
            Self::eExternalMemoryImageCreateInfo => RawStructureType::eExternalMemoryImageCreateInfo,
            Self::eExportMemoryAllocateInfo => RawStructureType::eExportMemoryAllocateInfo,
            Self::eImportMemoryWin32HandleInfoKhr => RawStructureType::eImportMemoryWin32HandleInfoKhr,
            Self::eExportMemoryWin32HandleInfoKhr => RawStructureType::eExportMemoryWin32HandleInfoKhr,
            Self::eMemoryWin32HandlePropertiesKhr => RawStructureType::eMemoryWin32HandlePropertiesKhr,
            Self::eMemoryGetWin32HandleInfoKhr => RawStructureType::eMemoryGetWin32HandleInfoKhr,
            Self::eImportMemoryFdInfoKhr => RawStructureType::eImportMemoryFdInfoKhr,
            Self::eMemoryFdPropertiesKhr => RawStructureType::eMemoryFdPropertiesKhr,
            Self::eMemoryGetFdInfoKhr => RawStructureType::eMemoryGetFdInfoKhr,
            Self::eWin32KeyedMutexAcquireReleaseInfoKhr => RawStructureType::eWin32KeyedMutexAcquireReleaseInfoKhr,
            Self::ePhysicalDeviceExternalSemaphoreInfo => RawStructureType::ePhysicalDeviceExternalSemaphoreInfo,
            Self::eExternalSemaphoreProperties => RawStructureType::eExternalSemaphoreProperties,
            Self::eExportSemaphoreCreateInfo => RawStructureType::eExportSemaphoreCreateInfo,
            Self::eImportSemaphoreWin32HandleInfoKhr => RawStructureType::eImportSemaphoreWin32HandleInfoKhr,
            Self::eExportSemaphoreWin32HandleInfoKhr => RawStructureType::eExportSemaphoreWin32HandleInfoKhr,
            Self::eD3D12FenceSubmitInfoKhr => RawStructureType::eD3D12FenceSubmitInfoKhr,
            Self::eSemaphoreGetWin32HandleInfoKhr => RawStructureType::eSemaphoreGetWin32HandleInfoKhr,
            Self::eImportSemaphoreFdInfoKhr => RawStructureType::eImportSemaphoreFdInfoKhr,
            Self::eSemaphoreGetFdInfoKhr => RawStructureType::eSemaphoreGetFdInfoKhr,
            Self::ePhysicalDevicePushDescriptorPropertiesKhr => RawStructureType::ePhysicalDevicePushDescriptorPropertiesKhr,
            Self::eCommandBufferInheritanceConditionalRenderingInfoExt => RawStructureType::eCommandBufferInheritanceConditionalRenderingInfoExt,
            Self::ePhysicalDeviceConditionalRenderingFeaturesExt => RawStructureType::ePhysicalDeviceConditionalRenderingFeaturesExt,
            Self::eConditionalRenderingBeginInfoExt => RawStructureType::eConditionalRenderingBeginInfoExt,
            Self::ePhysicalDeviceShaderFloat16Int8Features => RawStructureType::ePhysicalDeviceShaderFloat16Int8Features,
            Self::ePhysicalDevice16bitStorageFeatures => RawStructureType::ePhysicalDevice16bitStorageFeatures,
            Self::ePresentRegionsKhr => RawStructureType::ePresentRegionsKhr,
            Self::eDescriptorUpdateTemplateCreateInfo => RawStructureType::eDescriptorUpdateTemplateCreateInfo,
            Self::ePipelineViewportWScalingStateCreateInfoNv => RawStructureType::ePipelineViewportWScalingStateCreateInfoNv,
            Self::eSurfaceCapabilities2Ext => RawStructureType::eSurfaceCapabilities2Ext,
            Self::eDisplayPowerInfoExt => RawStructureType::eDisplayPowerInfoExt,
            Self::eDeviceEventInfoExt => RawStructureType::eDeviceEventInfoExt,
            Self::eDisplayEventInfoExt => RawStructureType::eDisplayEventInfoExt,
            Self::eSwapchainCounterCreateInfoExt => RawStructureType::eSwapchainCounterCreateInfoExt,
            Self::ePresentTimesInfoGoogle => RawStructureType::ePresentTimesInfoGoogle,
            Self::ePhysicalDeviceSubgroupProperties => RawStructureType::ePhysicalDeviceSubgroupProperties,
            Self::ePhysicalDeviceMultiviewPerViewAttributesPropertiesNvx => RawStructureType::ePhysicalDeviceMultiviewPerViewAttributesPropertiesNvx,
            Self::ePipelineViewportSwizzleStateCreateInfoNv => RawStructureType::ePipelineViewportSwizzleStateCreateInfoNv,
            Self::ePhysicalDeviceDiscardRectanglePropertiesExt => RawStructureType::ePhysicalDeviceDiscardRectanglePropertiesExt,
            Self::ePipelineDiscardRectangleStateCreateInfoExt => RawStructureType::ePipelineDiscardRectangleStateCreateInfoExt,
            Self::ePhysicalDeviceConservativeRasterizationPropertiesExt => RawStructureType::ePhysicalDeviceConservativeRasterizationPropertiesExt,
            Self::ePipelineRasterizationConservativeStateCreateInfoExt => RawStructureType::ePipelineRasterizationConservativeStateCreateInfoExt,
            Self::ePhysicalDeviceDepthClipEnableFeaturesExt => RawStructureType::ePhysicalDeviceDepthClipEnableFeaturesExt,
            Self::ePipelineRasterizationDepthClipStateCreateInfoExt => RawStructureType::ePipelineRasterizationDepthClipStateCreateInfoExt,
            Self::eHdrMetadataExt => RawStructureType::eHdrMetadataExt,
            Self::ePhysicalDeviceImagelessFramebufferFeatures => RawStructureType::ePhysicalDeviceImagelessFramebufferFeatures,
            Self::eFramebufferAttachmentsCreateInfo => RawStructureType::eFramebufferAttachmentsCreateInfo,
            Self::eFramebufferAttachmentImageInfo => RawStructureType::eFramebufferAttachmentImageInfo,
            Self::eRenderPassAttachmentBeginInfo => RawStructureType::eRenderPassAttachmentBeginInfo,
            Self::eAttachmentDescription2 => RawStructureType::eAttachmentDescription2,
            Self::eAttachmentReference2 => RawStructureType::eAttachmentReference2,
            Self::eSubpassDescription2 => RawStructureType::eSubpassDescription2,
            Self::eSubpassDependency2 => RawStructureType::eSubpassDependency2,
            Self::eRenderPassCreateInfo2 => RawStructureType::eRenderPassCreateInfo2,
            Self::eSubpassBeginInfo => RawStructureType::eSubpassBeginInfo,
            Self::eSubpassEndInfo => RawStructureType::eSubpassEndInfo,
            Self::eSharedPresentSurfaceCapabilitiesKhr => RawStructureType::eSharedPresentSurfaceCapabilitiesKhr,
            Self::ePhysicalDeviceExternalFenceInfo => RawStructureType::ePhysicalDeviceExternalFenceInfo,
            Self::eExternalFenceProperties => RawStructureType::eExternalFenceProperties,
            Self::eExportFenceCreateInfo => RawStructureType::eExportFenceCreateInfo,
            Self::eImportFenceWin32HandleInfoKhr => RawStructureType::eImportFenceWin32HandleInfoKhr,
            Self::eExportFenceWin32HandleInfoKhr => RawStructureType::eExportFenceWin32HandleInfoKhr,
            Self::eFenceGetWin32HandleInfoKhr => RawStructureType::eFenceGetWin32HandleInfoKhr,
            Self::eImportFenceFdInfoKhr => RawStructureType::eImportFenceFdInfoKhr,
            Self::eFenceGetFdInfoKhr => RawStructureType::eFenceGetFdInfoKhr,
            Self::ePhysicalDevicePerformanceQueryFeaturesKhr => RawStructureType::ePhysicalDevicePerformanceQueryFeaturesKhr,
            Self::ePhysicalDevicePerformanceQueryPropertiesKhr => RawStructureType::ePhysicalDevicePerformanceQueryPropertiesKhr,
            Self::eQueryPoolPerformanceCreateInfoKhr => RawStructureType::eQueryPoolPerformanceCreateInfoKhr,
            Self::ePerformanceQuerySubmitInfoKhr => RawStructureType::ePerformanceQuerySubmitInfoKhr,
            Self::eAcquireProfilingLockInfoKhr => RawStructureType::eAcquireProfilingLockInfoKhr,
            Self::ePerformanceCounterKhr => RawStructureType::ePerformanceCounterKhr,
            Self::ePerformanceCounterDescriptionKhr => RawStructureType::ePerformanceCounterDescriptionKhr,
            Self::ePerformanceQueryReservationInfoKhr => RawStructureType::ePerformanceQueryReservationInfoKhr,
            Self::ePhysicalDevicePointClippingProperties => RawStructureType::ePhysicalDevicePointClippingProperties,
            Self::eRenderPassInputAttachmentAspectCreateInfo => RawStructureType::eRenderPassInputAttachmentAspectCreateInfo,
            Self::eImageViewUsageCreateInfo => RawStructureType::eImageViewUsageCreateInfo,
            Self::ePipelineTessellationDomainOriginStateCreateInfo => RawStructureType::ePipelineTessellationDomainOriginStateCreateInfo,
            Self::ePhysicalDeviceSurfaceInfo2Khr => RawStructureType::ePhysicalDeviceSurfaceInfo2Khr,
            Self::eSurfaceCapabilities2Khr => RawStructureType::eSurfaceCapabilities2Khr,
            Self::eSurfaceFormat2Khr => RawStructureType::eSurfaceFormat2Khr,
            Self::ePhysicalDeviceVariablePointersFeatures => RawStructureType::ePhysicalDeviceVariablePointersFeatures,
            Self::eDisplayProperties2Khr => RawStructureType::eDisplayProperties2Khr,
            Self::eDisplayPlaneProperties2Khr => RawStructureType::eDisplayPlaneProperties2Khr,
            Self::eDisplayModeProperties2Khr => RawStructureType::eDisplayModeProperties2Khr,
            Self::eDisplayPlaneInfo2Khr => RawStructureType::eDisplayPlaneInfo2Khr,
            Self::eDisplayPlaneCapabilities2Khr => RawStructureType::eDisplayPlaneCapabilities2Khr,
            Self::eIosSurfaceCreateInfoMvk => RawStructureType::eIosSurfaceCreateInfoMvk,
            Self::eMacosSurfaceCreateInfoMvk => RawStructureType::eMacosSurfaceCreateInfoMvk,
            Self::eMemoryDedicatedRequirements => RawStructureType::eMemoryDedicatedRequirements,
            Self::eMemoryDedicatedAllocateInfo => RawStructureType::eMemoryDedicatedAllocateInfo,
            Self::eDebugUtilsObjectNameInfoExt => RawStructureType::eDebugUtilsObjectNameInfoExt,
            Self::eDebugUtilsObjectTagInfoExt => RawStructureType::eDebugUtilsObjectTagInfoExt,
            Self::eDebugUtilsLabelExt => RawStructureType::eDebugUtilsLabelExt,
            Self::eDebugUtilsMessengerCallbackDataExt => RawStructureType::eDebugUtilsMessengerCallbackDataExt,
            Self::eDebugUtilsMessengerCreateInfoExt => RawStructureType::eDebugUtilsMessengerCreateInfoExt,
            Self::eAndroidHardwareBufferUsageAndroid => RawStructureType::eAndroidHardwareBufferUsageAndroid,
            Self::eAndroidHardwareBufferPropertiesAndroid => RawStructureType::eAndroidHardwareBufferPropertiesAndroid,
            Self::eAndroidHardwareBufferFormatPropertiesAndroid => RawStructureType::eAndroidHardwareBufferFormatPropertiesAndroid,
            Self::eImportAndroidHardwareBufferInfoAndroid => RawStructureType::eImportAndroidHardwareBufferInfoAndroid,
            Self::eMemoryGetAndroidHardwareBufferInfoAndroid => RawStructureType::eMemoryGetAndroidHardwareBufferInfoAndroid,
            Self::eExternalFormatAndroid => RawStructureType::eExternalFormatAndroid,
            Self::eAndroidHardwareBufferFormatProperties2Android => RawStructureType::eAndroidHardwareBufferFormatProperties2Android,
            Self::ePhysicalDeviceSamplerFilterMinmaxProperties => RawStructureType::ePhysicalDeviceSamplerFilterMinmaxProperties,
            Self::eSamplerReductionModeCreateInfo => RawStructureType::eSamplerReductionModeCreateInfo,
            Self::ePhysicalDeviceInlineUniformBlockFeatures => RawStructureType::ePhysicalDeviceInlineUniformBlockFeatures,
            Self::ePhysicalDeviceInlineUniformBlockProperties => RawStructureType::ePhysicalDeviceInlineUniformBlockProperties,
            Self::eWriteDescriptorSetInlineUniformBlock => RawStructureType::eWriteDescriptorSetInlineUniformBlock,
            Self::eDescriptorPoolInlineUniformBlockCreateInfo => RawStructureType::eDescriptorPoolInlineUniformBlockCreateInfo,
            Self::eSampleLocationsInfoExt => RawStructureType::eSampleLocationsInfoExt,
            Self::eRenderPassSampleLocationsBeginInfoExt => RawStructureType::eRenderPassSampleLocationsBeginInfoExt,
            Self::ePipelineSampleLocationsStateCreateInfoExt => RawStructureType::ePipelineSampleLocationsStateCreateInfoExt,
            Self::ePhysicalDeviceSampleLocationsPropertiesExt => RawStructureType::ePhysicalDeviceSampleLocationsPropertiesExt,
            Self::eMultisamplePropertiesExt => RawStructureType::eMultisamplePropertiesExt,
            Self::eProtectedSubmitInfo => RawStructureType::eProtectedSubmitInfo,
            Self::ePhysicalDeviceProtectedMemoryFeatures => RawStructureType::ePhysicalDeviceProtectedMemoryFeatures,
            Self::ePhysicalDeviceProtectedMemoryProperties => RawStructureType::ePhysicalDeviceProtectedMemoryProperties,
            Self::eDeviceQueueInfo2 => RawStructureType::eDeviceQueueInfo2,
            Self::eBufferMemoryRequirementsInfo2 => RawStructureType::eBufferMemoryRequirementsInfo2,
            Self::eImageMemoryRequirementsInfo2 => RawStructureType::eImageMemoryRequirementsInfo2,
            Self::eImageSparseMemoryRequirementsInfo2 => RawStructureType::eImageSparseMemoryRequirementsInfo2,
            Self::eMemoryRequirements2 => RawStructureType::eMemoryRequirements2,
            Self::eSparseImageMemoryRequirements2 => RawStructureType::eSparseImageMemoryRequirements2,
            Self::eImageFormatListCreateInfo => RawStructureType::eImageFormatListCreateInfo,
            Self::ePhysicalDeviceBlendOperationAdvancedFeaturesExt => RawStructureType::ePhysicalDeviceBlendOperationAdvancedFeaturesExt,
            Self::ePhysicalDeviceBlendOperationAdvancedPropertiesExt => RawStructureType::ePhysicalDeviceBlendOperationAdvancedPropertiesExt,
            Self::ePipelineColourBlendAdvancedStateCreateInfoExt => RawStructureType::ePipelineColourBlendAdvancedStateCreateInfoExt,
            Self::ePipelineCoverageToColourStateCreateInfoNv => RawStructureType::ePipelineCoverageToColourStateCreateInfoNv,
            Self::eAccelerationStructureBuildGeometryInfoKhr => RawStructureType::eAccelerationStructureBuildGeometryInfoKhr,
            Self::eAccelerationStructureDeviceAddressInfoKhr => RawStructureType::eAccelerationStructureDeviceAddressInfoKhr,
            Self::eAccelerationStructureGeometryAabbsDataKhr => RawStructureType::eAccelerationStructureGeometryAabbsDataKhr,
            Self::eAccelerationStructureGeometryInstancesDataKhr => RawStructureType::eAccelerationStructureGeometryInstancesDataKhr,
            Self::eAccelerationStructureGeometryTrianglesDataKhr => RawStructureType::eAccelerationStructureGeometryTrianglesDataKhr,
            Self::eAccelerationStructureGeometryKhr => RawStructureType::eAccelerationStructureGeometryKhr,
            Self::eWriteDescriptorSetAccelerationStructureKhr => RawStructureType::eWriteDescriptorSetAccelerationStructureKhr,
            Self::eAccelerationStructureVersionInfoKhr => RawStructureType::eAccelerationStructureVersionInfoKhr,
            Self::eCopyAccelerationStructureInfoKhr => RawStructureType::eCopyAccelerationStructureInfoKhr,
            Self::eCopyAccelerationStructureToMemoryInfoKhr => RawStructureType::eCopyAccelerationStructureToMemoryInfoKhr,
            Self::eCopyMemoryToAccelerationStructureInfoKhr => RawStructureType::eCopyMemoryToAccelerationStructureInfoKhr,
            Self::ePhysicalDeviceAccelerationStructureFeaturesKhr => RawStructureType::ePhysicalDeviceAccelerationStructureFeaturesKhr,
            Self::ePhysicalDeviceAccelerationStructurePropertiesKhr => RawStructureType::ePhysicalDeviceAccelerationStructurePropertiesKhr,
            Self::eRayTracingPipelineCreateInfoKhr => RawStructureType::eRayTracingPipelineCreateInfoKhr,
            Self::eRayTracingShaderGroupCreateInfoKhr => RawStructureType::eRayTracingShaderGroupCreateInfoKhr,
            Self::eAccelerationStructureCreateInfoKhr => RawStructureType::eAccelerationStructureCreateInfoKhr,
            Self::eRayTracingPipelineInterfaceCreateInfoKhr => RawStructureType::eRayTracingPipelineInterfaceCreateInfoKhr,
            Self::eAccelerationStructureBuildSizesInfoKhr => RawStructureType::eAccelerationStructureBuildSizesInfoKhr,
            Self::ePipelineCoverageModulationStateCreateInfoNv => RawStructureType::ePipelineCoverageModulationStateCreateInfoNv,
            Self::ePhysicalDeviceShaderSmBuiltinsFeaturesNv => RawStructureType::ePhysicalDeviceShaderSmBuiltinsFeaturesNv,
            Self::ePhysicalDeviceShaderSmBuiltinsPropertiesNv => RawStructureType::ePhysicalDeviceShaderSmBuiltinsPropertiesNv,
            Self::eSamplerYcbcrConversionCreateInfo => RawStructureType::eSamplerYcbcrConversionCreateInfo,
            Self::eSamplerYcbcrConversionInfo => RawStructureType::eSamplerYcbcrConversionInfo,
            Self::eBindImagePlaneMemoryInfo => RawStructureType::eBindImagePlaneMemoryInfo,
            Self::eImagePlaneMemoryRequirementsInfo => RawStructureType::eImagePlaneMemoryRequirementsInfo,
            Self::ePhysicalDeviceSamplerYcbcrConversionFeatures => RawStructureType::ePhysicalDeviceSamplerYcbcrConversionFeatures,
            Self::eSamplerYcbcrConversionImageFormatProperties => RawStructureType::eSamplerYcbcrConversionImageFormatProperties,
            Self::eBindBufferMemoryInfo => RawStructureType::eBindBufferMemoryInfo,
            Self::eBindImageMemoryInfo => RawStructureType::eBindImageMemoryInfo,
            Self::eDrmFormatModifierPropertiesListExt => RawStructureType::eDrmFormatModifierPropertiesListExt,
            Self::ePhysicalDeviceImageDrmFormatModifierInfoExt => RawStructureType::ePhysicalDeviceImageDrmFormatModifierInfoExt,
            Self::eImageDrmFormatModifierListCreateInfoExt => RawStructureType::eImageDrmFormatModifierListCreateInfoExt,
            Self::eImageDrmFormatModifierExplicitCreateInfoExt => RawStructureType::eImageDrmFormatModifierExplicitCreateInfoExt,
            Self::eImageDrmFormatModifierPropertiesExt => RawStructureType::eImageDrmFormatModifierPropertiesExt,
            Self::eDrmFormatModifierPropertiesList2Ext => RawStructureType::eDrmFormatModifierPropertiesList2Ext,
            Self::eValidationCacheCreateInfoExt => RawStructureType::eValidationCacheCreateInfoExt,
            Self::eShaderModuleValidationCacheCreateInfoExt => RawStructureType::eShaderModuleValidationCacheCreateInfoExt,
            Self::eDescriptorSetLayoutBindingFlagsCreateInfo => RawStructureType::eDescriptorSetLayoutBindingFlagsCreateInfo,
            Self::ePhysicalDeviceDescriptorIndexingFeatures => RawStructureType::ePhysicalDeviceDescriptorIndexingFeatures,
            Self::ePhysicalDeviceDescriptorIndexingProperties => RawStructureType::ePhysicalDeviceDescriptorIndexingProperties,
            Self::eDescriptorSetVariableDescriptorCountAllocateInfo => RawStructureType::eDescriptorSetVariableDescriptorCountAllocateInfo,
            Self::eDescriptorSetVariableDescriptorCountLayoutSupport => RawStructureType::eDescriptorSetVariableDescriptorCountLayoutSupport,
            Self::ePhysicalDevicePortabilitySubsetFeaturesKhr => RawStructureType::ePhysicalDevicePortabilitySubsetFeaturesKhr,
            Self::ePhysicalDevicePortabilitySubsetPropertiesKhr => RawStructureType::ePhysicalDevicePortabilitySubsetPropertiesKhr,
            Self::ePipelineViewportShadingRateImageStateCreateInfoNv => RawStructureType::ePipelineViewportShadingRateImageStateCreateInfoNv,
            Self::ePhysicalDeviceShadingRateImageFeaturesNv => RawStructureType::ePhysicalDeviceShadingRateImageFeaturesNv,
            Self::ePhysicalDeviceShadingRateImagePropertiesNv => RawStructureType::ePhysicalDeviceShadingRateImagePropertiesNv,
            Self::ePipelineViewportCoarseSampleOrderStateCreateInfoNv => RawStructureType::ePipelineViewportCoarseSampleOrderStateCreateInfoNv,
            Self::eRayTracingPipelineCreateInfoNv => RawStructureType::eRayTracingPipelineCreateInfoNv,
            Self::eAccelerationStructureCreateInfoNv => RawStructureType::eAccelerationStructureCreateInfoNv,
            Self::eGeometryNv => RawStructureType::eGeometryNv,
            Self::eGeometryTrianglesNv => RawStructureType::eGeometryTrianglesNv,
            Self::eGeometryAabbNv => RawStructureType::eGeometryAabbNv,
            Self::eBindAccelerationStructureMemoryInfoNv => RawStructureType::eBindAccelerationStructureMemoryInfoNv,
            Self::eWriteDescriptorSetAccelerationStructureNv => RawStructureType::eWriteDescriptorSetAccelerationStructureNv,
            Self::eAccelerationStructureMemoryRequirementsInfoNv => RawStructureType::eAccelerationStructureMemoryRequirementsInfoNv,
            Self::ePhysicalDeviceRayTracingPropertiesNv => RawStructureType::ePhysicalDeviceRayTracingPropertiesNv,
            Self::eRayTracingShaderGroupCreateInfoNv => RawStructureType::eRayTracingShaderGroupCreateInfoNv,
            Self::eAccelerationStructureInfoNv => RawStructureType::eAccelerationStructureInfoNv,
            Self::ePhysicalDeviceRepresentativeFragmentTestFeaturesNv => RawStructureType::ePhysicalDeviceRepresentativeFragmentTestFeaturesNv,
            Self::ePipelineRepresentativeFragmentTestStateCreateInfoNv => RawStructureType::ePipelineRepresentativeFragmentTestStateCreateInfoNv,
            Self::ePhysicalDeviceMaintenance3Properties => RawStructureType::ePhysicalDeviceMaintenance3Properties,
            Self::eDescriptorSetLayoutSupport => RawStructureType::eDescriptorSetLayoutSupport,
            Self::ePhysicalDeviceImageViewImageFormatInfoExt => RawStructureType::ePhysicalDeviceImageViewImageFormatInfoExt,
            Self::eFilterCubicImageViewImageFormatPropertiesExt => RawStructureType::eFilterCubicImageViewImageFormatPropertiesExt,
            Self::eDeviceQueueGlobalPriorityCreateInfoKhr => RawStructureType::eDeviceQueueGlobalPriorityCreateInfoKhr,
            Self::ePhysicalDeviceShaderSubgroupExtendedTypesFeatures => RawStructureType::ePhysicalDeviceShaderSubgroupExtendedTypesFeatures,
            Self::ePhysicalDevice8bitStorageFeatures => RawStructureType::ePhysicalDevice8bitStorageFeatures,
            Self::eImportMemoryHostPointerInfoExt => RawStructureType::eImportMemoryHostPointerInfoExt,
            Self::eMemoryHostPointerPropertiesExt => RawStructureType::eMemoryHostPointerPropertiesExt,
            Self::ePhysicalDeviceExternalMemoryHostPropertiesExt => RawStructureType::ePhysicalDeviceExternalMemoryHostPropertiesExt,
            Self::ePhysicalDeviceShaderAtomicInt64Features => RawStructureType::ePhysicalDeviceShaderAtomicInt64Features,
            Self::ePhysicalDeviceShaderClockFeaturesKhr => RawStructureType::ePhysicalDeviceShaderClockFeaturesKhr,
            Self::ePipelineCompilerControlCreateInfoAmd => RawStructureType::ePipelineCompilerControlCreateInfoAmd,
            Self::eCalibratedTimestampInfoExt => RawStructureType::eCalibratedTimestampInfoExt,
            Self::ePhysicalDeviceShaderCorePropertiesAmd => RawStructureType::ePhysicalDeviceShaderCorePropertiesAmd,
            Self::eVideoDecodeH265CapabilitiesKhr => RawStructureType::eVideoDecodeH265CapabilitiesKhr,
            Self::eVideoDecodeH265SessionParametersCreateInfoKhr => RawStructureType::eVideoDecodeH265SessionParametersCreateInfoKhr,
            Self::eVideoDecodeH265SessionParametersAddInfoKhr => RawStructureType::eVideoDecodeH265SessionParametersAddInfoKhr,
            Self::eVideoDecodeH265ProfileInfoKhr => RawStructureType::eVideoDecodeH265ProfileInfoKhr,
            Self::eVideoDecodeH265PictureInfoKhr => RawStructureType::eVideoDecodeH265PictureInfoKhr,
            Self::eVideoDecodeH265DpbSlotInfoKhr => RawStructureType::eVideoDecodeH265DpbSlotInfoKhr,
            Self::eDeviceMemoryOverallocationCreateInfoAmd => RawStructureType::eDeviceMemoryOverallocationCreateInfoAmd,
            Self::ePhysicalDeviceVertexAttributeDivisorPropertiesExt => RawStructureType::ePhysicalDeviceVertexAttributeDivisorPropertiesExt,
            Self::ePipelineVertexInputDivisorStateCreateInfoExt => RawStructureType::ePipelineVertexInputDivisorStateCreateInfoExt,
            Self::ePhysicalDeviceVertexAttributeDivisorFeaturesExt => RawStructureType::ePhysicalDeviceVertexAttributeDivisorFeaturesExt,
            Self::ePresentFrameTokenGgp => RawStructureType::ePresentFrameTokenGgp,
            Self::ePipelineCreationFeedbackCreateInfo => RawStructureType::ePipelineCreationFeedbackCreateInfo,
            Self::ePhysicalDeviceDriverProperties => RawStructureType::ePhysicalDeviceDriverProperties,
            Self::ePhysicalDeviceFloatControlsProperties => RawStructureType::ePhysicalDeviceFloatControlsProperties,
            Self::ePhysicalDeviceDepthStencilResolveProperties => RawStructureType::ePhysicalDeviceDepthStencilResolveProperties,
            Self::eSubpassDescriptionDepthStencilResolve => RawStructureType::eSubpassDescriptionDepthStencilResolve,
            Self::ePhysicalDeviceComputeShaderDerivativesFeaturesNv => RawStructureType::ePhysicalDeviceComputeShaderDerivativesFeaturesNv,
            Self::ePhysicalDeviceMeshShaderFeaturesNv => RawStructureType::ePhysicalDeviceMeshShaderFeaturesNv,
            Self::ePhysicalDeviceMeshShaderPropertiesNv => RawStructureType::ePhysicalDeviceMeshShaderPropertiesNv,
            Self::ePhysicalDeviceFragmentShaderBarycentricFeaturesKhr => RawStructureType::ePhysicalDeviceFragmentShaderBarycentricFeaturesKhr,
            Self::ePhysicalDeviceShaderImageFootprintFeaturesNv => RawStructureType::ePhysicalDeviceShaderImageFootprintFeaturesNv,
            Self::ePipelineViewportExclusiveScissorStateCreateInfoNv => RawStructureType::ePipelineViewportExclusiveScissorStateCreateInfoNv,
            Self::ePhysicalDeviceExclusiveScissorFeaturesNv => RawStructureType::ePhysicalDeviceExclusiveScissorFeaturesNv,
            Self::eCheckpointDataNv => RawStructureType::eCheckpointDataNv,
            Self::eQueueFamilyCheckpointPropertiesNv => RawStructureType::eQueueFamilyCheckpointPropertiesNv,
            Self::ePhysicalDeviceTimelineSemaphoreFeatures => RawStructureType::ePhysicalDeviceTimelineSemaphoreFeatures,
            Self::ePhysicalDeviceTimelineSemaphoreProperties => RawStructureType::ePhysicalDeviceTimelineSemaphoreProperties,
            Self::eSemaphoreTypeCreateInfo => RawStructureType::eSemaphoreTypeCreateInfo,
            Self::eTimelineSemaphoreSubmitInfo => RawStructureType::eTimelineSemaphoreSubmitInfo,
            Self::eSemaphoreWaitInfo => RawStructureType::eSemaphoreWaitInfo,
            Self::eSemaphoreSignalInfo => RawStructureType::eSemaphoreSignalInfo,
            Self::ePhysicalDeviceShaderIntegerFunctions2FeaturesIntel => RawStructureType::ePhysicalDeviceShaderIntegerFunctions2FeaturesIntel,
            Self::eQueryPoolPerformanceQueryCreateInfoIntel => RawStructureType::eQueryPoolPerformanceQueryCreateInfoIntel,
            Self::eInitializePerformanceApiInfoIntel => RawStructureType::eInitializePerformanceApiInfoIntel,
            Self::ePerformanceMarkerInfoIntel => RawStructureType::ePerformanceMarkerInfoIntel,
            Self::ePerformanceStreamMarkerInfoIntel => RawStructureType::ePerformanceStreamMarkerInfoIntel,
            Self::ePerformanceOverrideInfoIntel => RawStructureType::ePerformanceOverrideInfoIntel,
            Self::ePerformanceConfigurationAcquireInfoIntel => RawStructureType::ePerformanceConfigurationAcquireInfoIntel,
            Self::ePhysicalDeviceVulkanMemoryModelFeatures => RawStructureType::ePhysicalDeviceVulkanMemoryModelFeatures,
            Self::ePhysicalDevicePciBusInfoPropertiesExt => RawStructureType::ePhysicalDevicePciBusInfoPropertiesExt,
            Self::eDisplayNativeHdrSurfaceCapabilitiesAmd => RawStructureType::eDisplayNativeHdrSurfaceCapabilitiesAmd,
            Self::eSwapchainDisplayNativeHdrCreateInfoAmd => RawStructureType::eSwapchainDisplayNativeHdrCreateInfoAmd,
            Self::eImagepipeSurfaceCreateInfoFuchsia => RawStructureType::eImagepipeSurfaceCreateInfoFuchsia,
            Self::ePhysicalDeviceShaderTerminateInvocationFeatures => RawStructureType::ePhysicalDeviceShaderTerminateInvocationFeatures,
            Self::eMetalSurfaceCreateInfoExt => RawStructureType::eMetalSurfaceCreateInfoExt,
            Self::ePhysicalDeviceFragmentDensityMapFeaturesExt => RawStructureType::ePhysicalDeviceFragmentDensityMapFeaturesExt,
            Self::ePhysicalDeviceFragmentDensityMapPropertiesExt => RawStructureType::ePhysicalDeviceFragmentDensityMapPropertiesExt,
            Self::eRenderPassFragmentDensityMapCreateInfoExt => RawStructureType::eRenderPassFragmentDensityMapCreateInfoExt,
            Self::ePhysicalDeviceScalarBlockLayoutFeatures => RawStructureType::ePhysicalDeviceScalarBlockLayoutFeatures,
            Self::ePhysicalDeviceSubgroupSizeControlProperties => RawStructureType::ePhysicalDeviceSubgroupSizeControlProperties,
            Self::ePipelineShaderStageRequiredSubgroupSizeCreateInfo => RawStructureType::ePipelineShaderStageRequiredSubgroupSizeCreateInfo,
            Self::ePhysicalDeviceSubgroupSizeControlFeatures => RawStructureType::ePhysicalDeviceSubgroupSizeControlFeatures,
            Self::eFragmentShadingRateAttachmentInfoKhr => RawStructureType::eFragmentShadingRateAttachmentInfoKhr,
            Self::ePipelineFragmentShadingRateStateCreateInfoKhr => RawStructureType::ePipelineFragmentShadingRateStateCreateInfoKhr,
            Self::ePhysicalDeviceFragmentShadingRatePropertiesKhr => RawStructureType::ePhysicalDeviceFragmentShadingRatePropertiesKhr,
            Self::ePhysicalDeviceFragmentShadingRateFeaturesKhr => RawStructureType::ePhysicalDeviceFragmentShadingRateFeaturesKhr,
            Self::ePhysicalDeviceFragmentShadingRateKhr => RawStructureType::ePhysicalDeviceFragmentShadingRateKhr,
            Self::ePhysicalDeviceShaderCoreProperties2Amd => RawStructureType::ePhysicalDeviceShaderCoreProperties2Amd,
            Self::ePhysicalDeviceCoherentMemoryFeaturesAmd => RawStructureType::ePhysicalDeviceCoherentMemoryFeaturesAmd,
            Self::ePhysicalDeviceShaderImageAtomicInt64FeaturesExt => RawStructureType::ePhysicalDeviceShaderImageAtomicInt64FeaturesExt,
            Self::ePhysicalDeviceMemoryBudgetPropertiesExt => RawStructureType::ePhysicalDeviceMemoryBudgetPropertiesExt,
            Self::ePhysicalDeviceMemoryPriorityFeaturesExt => RawStructureType::ePhysicalDeviceMemoryPriorityFeaturesExt,
            Self::eMemoryPriorityAllocateInfoExt => RawStructureType::eMemoryPriorityAllocateInfoExt,
            Self::eSurfaceProtectedCapabilitiesKhr => RawStructureType::eSurfaceProtectedCapabilitiesKhr,
            Self::ePhysicalDeviceDedicatedAllocationImageAliasingFeaturesNv => RawStructureType::ePhysicalDeviceDedicatedAllocationImageAliasingFeaturesNv,
            Self::ePhysicalDeviceSeparateDepthStencilLayoutsFeatures => RawStructureType::ePhysicalDeviceSeparateDepthStencilLayoutsFeatures,
            Self::eAttachmentReferenceStencilLayout => RawStructureType::eAttachmentReferenceStencilLayout,
            Self::eAttachmentDescriptionStencilLayout => RawStructureType::eAttachmentDescriptionStencilLayout,
            Self::ePhysicalDeviceBufferDeviceAddressFeaturesExt => RawStructureType::ePhysicalDeviceBufferDeviceAddressFeaturesExt,
            Self::eBufferDeviceAddressInfo => RawStructureType::eBufferDeviceAddressInfo,
            Self::eBufferDeviceAddressCreateInfoExt => RawStructureType::eBufferDeviceAddressCreateInfoExt,
            Self::ePhysicalDeviceToolProperties => RawStructureType::ePhysicalDeviceToolProperties,
            Self::eImageStencilUsageCreateInfo => RawStructureType::eImageStencilUsageCreateInfo,
            Self::eValidationFeaturesExt => RawStructureType::eValidationFeaturesExt,
            Self::ePhysicalDevicePresentWaitFeaturesKhr => RawStructureType::ePhysicalDevicePresentWaitFeaturesKhr,
            Self::ePhysicalDeviceCooperativeMatrixFeaturesNv => RawStructureType::ePhysicalDeviceCooperativeMatrixFeaturesNv,
            Self::eCooperativeMatrixPropertiesNv => RawStructureType::eCooperativeMatrixPropertiesNv,
            Self::ePhysicalDeviceCooperativeMatrixPropertiesNv => RawStructureType::ePhysicalDeviceCooperativeMatrixPropertiesNv,
            Self::ePhysicalDeviceCoverageReductionModeFeaturesNv => RawStructureType::ePhysicalDeviceCoverageReductionModeFeaturesNv,
            Self::ePipelineCoverageReductionStateCreateInfoNv => RawStructureType::ePipelineCoverageReductionStateCreateInfoNv,
            Self::eFramebufferMixedSamplesCombinationNv => RawStructureType::eFramebufferMixedSamplesCombinationNv,
            Self::ePhysicalDeviceFragmentShaderInterlockFeaturesExt => RawStructureType::ePhysicalDeviceFragmentShaderInterlockFeaturesExt,
            Self::ePhysicalDeviceYcbcrImageArraysFeaturesExt => RawStructureType::ePhysicalDeviceYcbcrImageArraysFeaturesExt,
            Self::ePhysicalDeviceUniformBufferStandardLayoutFeatures => RawStructureType::ePhysicalDeviceUniformBufferStandardLayoutFeatures,
            Self::ePhysicalDeviceProvokingVertexFeaturesExt => RawStructureType::ePhysicalDeviceProvokingVertexFeaturesExt,
            Self::ePipelineRasterizationProvokingVertexStateCreateInfoExt => RawStructureType::ePipelineRasterizationProvokingVertexStateCreateInfoExt,
            Self::ePhysicalDeviceProvokingVertexPropertiesExt => RawStructureType::ePhysicalDeviceProvokingVertexPropertiesExt,
            Self::eSurfaceFullScreenExclusiveInfoExt => RawStructureType::eSurfaceFullScreenExclusiveInfoExt,
            Self::eSurfaceFullScreenExclusiveWin32InfoExt => RawStructureType::eSurfaceFullScreenExclusiveWin32InfoExt,
            Self::eSurfaceCapabilitiesFullScreenExclusiveExt => RawStructureType::eSurfaceCapabilitiesFullScreenExclusiveExt,
            Self::eHeadlessSurfaceCreateInfoExt => RawStructureType::eHeadlessSurfaceCreateInfoExt,
            Self::ePhysicalDeviceBufferDeviceAddressFeatures => RawStructureType::ePhysicalDeviceBufferDeviceAddressFeatures,
            Self::eBufferOpaqueCaptureAddressCreateInfo => RawStructureType::eBufferOpaqueCaptureAddressCreateInfo,
            Self::eMemoryOpaqueCaptureAddressAllocateInfo => RawStructureType::eMemoryOpaqueCaptureAddressAllocateInfo,
            Self::eDeviceMemoryOpaqueCaptureAddressInfo => RawStructureType::eDeviceMemoryOpaqueCaptureAddressInfo,
            Self::ePhysicalDeviceLineRasterizationFeaturesExt => RawStructureType::ePhysicalDeviceLineRasterizationFeaturesExt,
            Self::ePipelineRasterizationLineStateCreateInfoExt => RawStructureType::ePipelineRasterizationLineStateCreateInfoExt,
            Self::ePhysicalDeviceLineRasterizationPropertiesExt => RawStructureType::ePhysicalDeviceLineRasterizationPropertiesExt,
            Self::ePhysicalDeviceShaderAtomicFloatFeaturesExt => RawStructureType::ePhysicalDeviceShaderAtomicFloatFeaturesExt,
            Self::ePhysicalDeviceHostQueryResetFeatures => RawStructureType::ePhysicalDeviceHostQueryResetFeatures,
            Self::ePhysicalDeviceIndexTypeUint8FeaturesExt => RawStructureType::ePhysicalDeviceIndexTypeUint8FeaturesExt,
            Self::ePhysicalDeviceExtendedDynamicStateFeaturesExt => RawStructureType::ePhysicalDeviceExtendedDynamicStateFeaturesExt,
            Self::ePhysicalDevicePipelineExecutablePropertiesFeaturesKhr => RawStructureType::ePhysicalDevicePipelineExecutablePropertiesFeaturesKhr,
            Self::ePipelineInfoKhr => RawStructureType::ePipelineInfoKhr,
            Self::ePipelineExecutablePropertiesKhr => RawStructureType::ePipelineExecutablePropertiesKhr,
            Self::ePipelineExecutableInfoKhr => RawStructureType::ePipelineExecutableInfoKhr,
            Self::ePipelineExecutableStatisticKhr => RawStructureType::ePipelineExecutableStatisticKhr,
            Self::ePipelineExecutableInternalRepresentationKhr => RawStructureType::ePipelineExecutableInternalRepresentationKhr,
            Self::ePhysicalDeviceShaderAtomicFloat2FeaturesExt => RawStructureType::ePhysicalDeviceShaderAtomicFloat2FeaturesExt,
            Self::eSurfacePresentModeExt => RawStructureType::eSurfacePresentModeExt,
            Self::eSurfacePresentScalingCapabilitiesExt => RawStructureType::eSurfacePresentScalingCapabilitiesExt,
            Self::eSurfacePresentModeCompatibilityExt => RawStructureType::eSurfacePresentModeCompatibilityExt,
            Self::ePhysicalDeviceSwapchainMaintenance1FeaturesExt => RawStructureType::ePhysicalDeviceSwapchainMaintenance1FeaturesExt,
            Self::eSwapchainPresentFenceInfoExt => RawStructureType::eSwapchainPresentFenceInfoExt,
            Self::eSwapchainPresentModesCreateInfoExt => RawStructureType::eSwapchainPresentModesCreateInfoExt,
            Self::eSwapchainPresentModeInfoExt => RawStructureType::eSwapchainPresentModeInfoExt,
            Self::eSwapchainPresentScalingCreateInfoExt => RawStructureType::eSwapchainPresentScalingCreateInfoExt,
            Self::eReleaseSwapchainImagesInfoExt => RawStructureType::eReleaseSwapchainImagesInfoExt,
            Self::ePhysicalDeviceShaderDemoteToHelperInvocationFeatures => RawStructureType::ePhysicalDeviceShaderDemoteToHelperInvocationFeatures,
            Self::ePhysicalDeviceDeviceGeneratedCommandsPropertiesNv => RawStructureType::ePhysicalDeviceDeviceGeneratedCommandsPropertiesNv,
            Self::eGraphicsShaderGroupCreateInfoNv => RawStructureType::eGraphicsShaderGroupCreateInfoNv,
            Self::eGraphicsPipelineShaderGroupsCreateInfoNv => RawStructureType::eGraphicsPipelineShaderGroupsCreateInfoNv,
            Self::eIndirectCommandsLayoutTokenNv => RawStructureType::eIndirectCommandsLayoutTokenNv,
            Self::eIndirectCommandsLayoutCreateInfoNv => RawStructureType::eIndirectCommandsLayoutCreateInfoNv,
            Self::eGeneratedCommandsInfoNv => RawStructureType::eGeneratedCommandsInfoNv,
            Self::eGeneratedCommandsMemoryRequirementsInfoNv => RawStructureType::eGeneratedCommandsMemoryRequirementsInfoNv,
            Self::ePhysicalDeviceDeviceGeneratedCommandsFeaturesNv => RawStructureType::ePhysicalDeviceDeviceGeneratedCommandsFeaturesNv,
            Self::ePhysicalDeviceInheritedViewportScissorFeaturesNv => RawStructureType::ePhysicalDeviceInheritedViewportScissorFeaturesNv,
            Self::eCommandBufferInheritanceViewportScissorInfoNv => RawStructureType::eCommandBufferInheritanceViewportScissorInfoNv,
            Self::ePhysicalDeviceShaderIntegerDotProductFeatures => RawStructureType::ePhysicalDeviceShaderIntegerDotProductFeatures,
            Self::ePhysicalDeviceShaderIntegerDotProductProperties => RawStructureType::ePhysicalDeviceShaderIntegerDotProductProperties,
            Self::ePhysicalDeviceTexelBufferAlignmentFeaturesExt => RawStructureType::ePhysicalDeviceTexelBufferAlignmentFeaturesExt,
            Self::ePhysicalDeviceTexelBufferAlignmentProperties => RawStructureType::ePhysicalDeviceTexelBufferAlignmentProperties,
            Self::eCommandBufferInheritanceRenderPassTransformInfoQcom => RawStructureType::eCommandBufferInheritanceRenderPassTransformInfoQcom,
            Self::eRenderPassTransformBeginInfoQcom => RawStructureType::eRenderPassTransformBeginInfoQcom,
            Self::ePhysicalDeviceDeviceMemoryReportFeaturesExt => RawStructureType::ePhysicalDeviceDeviceMemoryReportFeaturesExt,
            Self::eDeviceDeviceMemoryReportCreateInfoExt => RawStructureType::eDeviceDeviceMemoryReportCreateInfoExt,
            Self::eDeviceMemoryReportCallbackDataExt => RawStructureType::eDeviceMemoryReportCallbackDataExt,
            Self::ePhysicalDeviceRobustness2FeaturesExt => RawStructureType::ePhysicalDeviceRobustness2FeaturesExt,
            Self::ePhysicalDeviceRobustness2PropertiesExt => RawStructureType::ePhysicalDeviceRobustness2PropertiesExt,
            Self::eSamplerCustomBorderColourCreateInfoExt => RawStructureType::eSamplerCustomBorderColourCreateInfoExt,
            Self::ePhysicalDeviceCustomBorderColourPropertiesExt => RawStructureType::ePhysicalDeviceCustomBorderColourPropertiesExt,
            Self::ePhysicalDeviceCustomBorderColourFeaturesExt => RawStructureType::ePhysicalDeviceCustomBorderColourFeaturesExt,
            Self::ePipelineLibraryCreateInfoKhr => RawStructureType::ePipelineLibraryCreateInfoKhr,
            Self::ePhysicalDevicePresentBarrierFeaturesNv => RawStructureType::ePhysicalDevicePresentBarrierFeaturesNv,
            Self::eSurfaceCapabilitiesPresentBarrierNv => RawStructureType::eSurfaceCapabilitiesPresentBarrierNv,
            Self::eSwapchainPresentBarrierCreateInfoNv => RawStructureType::eSwapchainPresentBarrierCreateInfoNv,
            Self::ePresentIdKhr => RawStructureType::ePresentIdKhr,
            Self::ePhysicalDevicePresentIdFeaturesKhr => RawStructureType::ePhysicalDevicePresentIdFeaturesKhr,
            Self::ePhysicalDevicePrivateDataFeatures => RawStructureType::ePhysicalDevicePrivateDataFeatures,
            Self::eDevicePrivateDataCreateInfo => RawStructureType::eDevicePrivateDataCreateInfo,
            Self::ePrivateDataSlotCreateInfo => RawStructureType::ePrivateDataSlotCreateInfo,
            Self::ePhysicalDevicePipelineCreationCacheControlFeatures => RawStructureType::ePhysicalDevicePipelineCreationCacheControlFeatures,
            Self::ePhysicalDeviceVulkanSc10Features => RawStructureType::ePhysicalDeviceVulkanSc10Features,
            Self::ePhysicalDeviceVulkanSc10Properties => RawStructureType::ePhysicalDeviceVulkanSc10Properties,
            Self::eDeviceObjectReservationCreateInfo => RawStructureType::eDeviceObjectReservationCreateInfo,
            Self::eCommandPoolMemoryReservationCreateInfo => RawStructureType::eCommandPoolMemoryReservationCreateInfo,
            Self::eCommandPoolMemoryConsumption => RawStructureType::eCommandPoolMemoryConsumption,
            Self::ePipelinePoolSize => RawStructureType::ePipelinePoolSize,
            Self::eFaultData => RawStructureType::eFaultData,
            Self::eFaultCallbackInfo => RawStructureType::eFaultCallbackInfo,
            Self::ePipelineOfflineCreateInfo => RawStructureType::ePipelineOfflineCreateInfo,
            Self::eVideoEncodeInfoKhr => RawStructureType::eVideoEncodeInfoKhr,
            Self::eVideoEncodeRateControlInfoKhr => RawStructureType::eVideoEncodeRateControlInfoKhr,
            Self::eVideoEncodeRateControlLayerInfoKhr => RawStructureType::eVideoEncodeRateControlLayerInfoKhr,
            Self::eVideoEncodeCapabilitiesKhr => RawStructureType::eVideoEncodeCapabilitiesKhr,
            Self::eVideoEncodeUsageInfoKhr => RawStructureType::eVideoEncodeUsageInfoKhr,
            Self::ePhysicalDeviceDiagnosticsConfigFeaturesNv => RawStructureType::ePhysicalDeviceDiagnosticsConfigFeaturesNv,
            Self::eDeviceDiagnosticsConfigCreateInfoNv => RawStructureType::eDeviceDiagnosticsConfigCreateInfoNv,
            Self::eRefreshObjectListKhr => RawStructureType::eRefreshObjectListKhr,
            Self::eQueryLowLatencySupportNv => RawStructureType::eQueryLowLatencySupportNv,
            Self::eExportMetalObjectCreateInfoExt => RawStructureType::eExportMetalObjectCreateInfoExt,
            Self::eExportMetalObjectsInfoExt => RawStructureType::eExportMetalObjectsInfoExt,
            Self::eExportMetalDeviceInfoExt => RawStructureType::eExportMetalDeviceInfoExt,
            Self::eExportMetalCommandQueueInfoExt => RawStructureType::eExportMetalCommandQueueInfoExt,
            Self::eExportMetalBufferInfoExt => RawStructureType::eExportMetalBufferInfoExt,
            Self::eImportMetalBufferInfoExt => RawStructureType::eImportMetalBufferInfoExt,
            Self::eExportMetalTextureInfoExt => RawStructureType::eExportMetalTextureInfoExt,
            Self::eImportMetalTextureInfoExt => RawStructureType::eImportMetalTextureInfoExt,
            Self::eExportMetalIoSurfaceInfoExt => RawStructureType::eExportMetalIoSurfaceInfoExt,
            Self::eImportMetalIoSurfaceInfoExt => RawStructureType::eImportMetalIoSurfaceInfoExt,
            Self::eExportMetalSharedEventInfoExt => RawStructureType::eExportMetalSharedEventInfoExt,
            Self::eImportMetalSharedEventInfoExt => RawStructureType::eImportMetalSharedEventInfoExt,
            Self::eMemoryBarrier2 => RawStructureType::eMemoryBarrier2,
            Self::eBufferMemoryBarrier2 => RawStructureType::eBufferMemoryBarrier2,
            Self::eImageMemoryBarrier2 => RawStructureType::eImageMemoryBarrier2,
            Self::eDependencyInfo => RawStructureType::eDependencyInfo,
            Self::eSubmitInfo2 => RawStructureType::eSubmitInfo2,
            Self::eSemaphoreSubmitInfo => RawStructureType::eSemaphoreSubmitInfo,
            Self::eCommandBufferSubmitInfo => RawStructureType::eCommandBufferSubmitInfo,
            Self::ePhysicalDeviceSynchronization2Features => RawStructureType::ePhysicalDeviceSynchronization2Features,
            Self::eQueueFamilyCheckpointProperties2Nv => RawStructureType::eQueueFamilyCheckpointProperties2Nv,
            Self::eCheckpointData2Nv => RawStructureType::eCheckpointData2Nv,
            Self::ePhysicalDeviceDescriptorBufferPropertiesExt => RawStructureType::ePhysicalDeviceDescriptorBufferPropertiesExt,
            Self::ePhysicalDeviceDescriptorBufferDensityMapPropertiesExt => RawStructureType::ePhysicalDeviceDescriptorBufferDensityMapPropertiesExt,
            Self::ePhysicalDeviceDescriptorBufferFeaturesExt => RawStructureType::ePhysicalDeviceDescriptorBufferFeaturesExt,
            Self::eDescriptorAddressInfoExt => RawStructureType::eDescriptorAddressInfoExt,
            Self::eDescriptorGetInfoExt => RawStructureType::eDescriptorGetInfoExt,
            Self::eBufferCaptureDescriptorDataInfoExt => RawStructureType::eBufferCaptureDescriptorDataInfoExt,
            Self::eImageCaptureDescriptorDataInfoExt => RawStructureType::eImageCaptureDescriptorDataInfoExt,
            Self::eImageViewCaptureDescriptorDataInfoExt => RawStructureType::eImageViewCaptureDescriptorDataInfoExt,
            Self::eSamplerCaptureDescriptorDataInfoExt => RawStructureType::eSamplerCaptureDescriptorDataInfoExt,
            Self::eAccelerationStructureCaptureDescriptorDataInfoExt => RawStructureType::eAccelerationStructureCaptureDescriptorDataInfoExt,
            Self::eOpaqueCaptureDescriptorDataCreateInfoExt => RawStructureType::eOpaqueCaptureDescriptorDataCreateInfoExt,
            Self::eDescriptorBufferBindingInfoExt => RawStructureType::eDescriptorBufferBindingInfoExt,
            Self::eDescriptorBufferBindingPushDescriptorBufferHandleExt => RawStructureType::eDescriptorBufferBindingPushDescriptorBufferHandleExt,
            Self::ePhysicalDeviceGraphicsPipelineLibraryFeaturesExt => RawStructureType::ePhysicalDeviceGraphicsPipelineLibraryFeaturesExt,
            Self::ePhysicalDeviceGraphicsPipelineLibraryPropertiesExt => RawStructureType::ePhysicalDeviceGraphicsPipelineLibraryPropertiesExt,
            Self::eGraphicsPipelineLibraryCreateInfoExt => RawStructureType::eGraphicsPipelineLibraryCreateInfoExt,
            Self::ePhysicalDeviceShaderEarlyAndLateFragmentTestsFeaturesAmd => RawStructureType::ePhysicalDeviceShaderEarlyAndLateFragmentTestsFeaturesAmd,
            Self::ePhysicalDeviceFragmentShaderBarycentricPropertiesKhr => RawStructureType::ePhysicalDeviceFragmentShaderBarycentricPropertiesKhr,
            Self::ePhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKhr => RawStructureType::ePhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKhr,
            Self::ePhysicalDeviceZeroInitializeWorkgroupMemoryFeatures => RawStructureType::ePhysicalDeviceZeroInitializeWorkgroupMemoryFeatures,
            Self::ePhysicalDeviceFragmentShadingRateEnumsPropertiesNv => RawStructureType::ePhysicalDeviceFragmentShadingRateEnumsPropertiesNv,
            Self::ePhysicalDeviceFragmentShadingRateEnumsFeaturesNv => RawStructureType::ePhysicalDeviceFragmentShadingRateEnumsFeaturesNv,
            Self::ePipelineFragmentShadingRateEnumStateCreateInfoNv => RawStructureType::ePipelineFragmentShadingRateEnumStateCreateInfoNv,
            Self::eAccelerationStructureGeometryMotionTrianglesDataNv => RawStructureType::eAccelerationStructureGeometryMotionTrianglesDataNv,
            Self::ePhysicalDeviceRayTracingMotionBlurFeaturesNv => RawStructureType::ePhysicalDeviceRayTracingMotionBlurFeaturesNv,
            Self::eAccelerationStructureMotionInfoNv => RawStructureType::eAccelerationStructureMotionInfoNv,
            Self::ePhysicalDeviceMeshShaderFeaturesExt => RawStructureType::ePhysicalDeviceMeshShaderFeaturesExt,
            Self::ePhysicalDeviceMeshShaderPropertiesExt => RawStructureType::ePhysicalDeviceMeshShaderPropertiesExt,
            Self::ePhysicalDeviceYcbcr2Plane444FormatsFeaturesExt => RawStructureType::ePhysicalDeviceYcbcr2Plane444FormatsFeaturesExt,
            Self::ePhysicalDeviceFragmentDensityMap2FeaturesExt => RawStructureType::ePhysicalDeviceFragmentDensityMap2FeaturesExt,
            Self::ePhysicalDeviceFragmentDensityMap2PropertiesExt => RawStructureType::ePhysicalDeviceFragmentDensityMap2PropertiesExt,
            Self::eCopyCommandTransformInfoQcom => RawStructureType::eCopyCommandTransformInfoQcom,
            Self::ePhysicalDeviceImageRobustnessFeatures => RawStructureType::ePhysicalDeviceImageRobustnessFeatures,
            Self::ePhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKhr => RawStructureType::ePhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKhr,
            Self::eCopyBufferInfo2 => RawStructureType::eCopyBufferInfo2,
            Self::eCopyImageInfo2 => RawStructureType::eCopyImageInfo2,
            Self::eCopyBufferToImageInfo2 => RawStructureType::eCopyBufferToImageInfo2,
            Self::eCopyImageToBufferInfo2 => RawStructureType::eCopyImageToBufferInfo2,
            Self::eBlitImageInfo2 => RawStructureType::eBlitImageInfo2,
            Self::eResolveImageInfo2 => RawStructureType::eResolveImageInfo2,
            Self::eBufferCopy2 => RawStructureType::eBufferCopy2,
            Self::eImageCopy2 => RawStructureType::eImageCopy2,
            Self::eImageBlit2 => RawStructureType::eImageBlit2,
            Self::eBufferImageCopy2 => RawStructureType::eBufferImageCopy2,
            Self::eImageResolve2 => RawStructureType::eImageResolve2,
            Self::ePhysicalDeviceImageCompressionControlFeaturesExt => RawStructureType::ePhysicalDeviceImageCompressionControlFeaturesExt,
            Self::eImageCompressionControlExt => RawStructureType::eImageCompressionControlExt,
            Self::eSubresourceLayout2Ext => RawStructureType::eSubresourceLayout2Ext,
            Self::eImageSubresource2Ext => RawStructureType::eImageSubresource2Ext,
            Self::eImageCompressionPropertiesExt => RawStructureType::eImageCompressionPropertiesExt,
            Self::ePhysicalDeviceAttachmentFeedbackLoopLayoutFeaturesExt => RawStructureType::ePhysicalDeviceAttachmentFeedbackLoopLayoutFeaturesExt,
            Self::ePhysicalDevice4444FormatsFeaturesExt => RawStructureType::ePhysicalDevice4444FormatsFeaturesExt,
            Self::ePhysicalDeviceFaultFeaturesExt => RawStructureType::ePhysicalDeviceFaultFeaturesExt,
            Self::eDeviceFaultCountsExt => RawStructureType::eDeviceFaultCountsExt,
            Self::eDeviceFaultInfoExt => RawStructureType::eDeviceFaultInfoExt,
            Self::ePhysicalDeviceRasterizationOrderAttachmentAccessFeaturesExt => RawStructureType::ePhysicalDeviceRasterizationOrderAttachmentAccessFeaturesExt,
            Self::ePhysicalDeviceRgba10X6FormatsFeaturesExt => RawStructureType::ePhysicalDeviceRgba10X6FormatsFeaturesExt,
            Self::eDirectfbSurfaceCreateInfoExt => RawStructureType::eDirectfbSurfaceCreateInfoExt,
            Self::ePhysicalDeviceRayTracingPipelineFeaturesKhr => RawStructureType::ePhysicalDeviceRayTracingPipelineFeaturesKhr,
            Self::ePhysicalDeviceRayTracingPipelinePropertiesKhr => RawStructureType::ePhysicalDeviceRayTracingPipelinePropertiesKhr,
            Self::ePhysicalDeviceRayQueryFeaturesKhr => RawStructureType::ePhysicalDeviceRayQueryFeaturesKhr,
            Self::ePhysicalDeviceMutableDescriptorTypeFeaturesExt => RawStructureType::ePhysicalDeviceMutableDescriptorTypeFeaturesExt,
            Self::eMutableDescriptorTypeCreateInfoExt => RawStructureType::eMutableDescriptorTypeCreateInfoExt,
            Self::ePhysicalDeviceVertexInputDynamicStateFeaturesExt => RawStructureType::ePhysicalDeviceVertexInputDynamicStateFeaturesExt,
            Self::eVertexInputBindingDescription2Ext => RawStructureType::eVertexInputBindingDescription2Ext,
            Self::eVertexInputAttributeDescription2Ext => RawStructureType::eVertexInputAttributeDescription2Ext,
            Self::ePhysicalDeviceDrmPropertiesExt => RawStructureType::ePhysicalDeviceDrmPropertiesExt,
            Self::ePhysicalDeviceAddressBindingReportFeaturesExt => RawStructureType::ePhysicalDeviceAddressBindingReportFeaturesExt,
            Self::eDeviceAddressBindingCallbackDataExt => RawStructureType::eDeviceAddressBindingCallbackDataExt,
            Self::ePhysicalDeviceDepthClipControlFeaturesExt => RawStructureType::ePhysicalDeviceDepthClipControlFeaturesExt,
            Self::ePipelineViewportDepthClipControlCreateInfoExt => RawStructureType::ePipelineViewportDepthClipControlCreateInfoExt,
            Self::ePhysicalDevicePrimitiveTopologyListRestartFeaturesExt => RawStructureType::ePhysicalDevicePrimitiveTopologyListRestartFeaturesExt,
            Self::eFormatProperties3 => RawStructureType::eFormatProperties3,
            Self::eImportMemoryZirconHandleInfoFuchsia => RawStructureType::eImportMemoryZirconHandleInfoFuchsia,
            Self::eMemoryZirconHandlePropertiesFuchsia => RawStructureType::eMemoryZirconHandlePropertiesFuchsia,
            Self::eMemoryGetZirconHandleInfoFuchsia => RawStructureType::eMemoryGetZirconHandleInfoFuchsia,
            Self::eImportSemaphoreZirconHandleInfoFuchsia => RawStructureType::eImportSemaphoreZirconHandleInfoFuchsia,
            Self::eSemaphoreGetZirconHandleInfoFuchsia => RawStructureType::eSemaphoreGetZirconHandleInfoFuchsia,
            Self::eBufferCollectionCreateInfoFuchsia => RawStructureType::eBufferCollectionCreateInfoFuchsia,
            Self::eImportMemoryBufferCollectionFuchsia => RawStructureType::eImportMemoryBufferCollectionFuchsia,
            Self::eBufferCollectionImageCreateInfoFuchsia => RawStructureType::eBufferCollectionImageCreateInfoFuchsia,
            Self::eBufferCollectionPropertiesFuchsia => RawStructureType::eBufferCollectionPropertiesFuchsia,
            Self::eBufferConstraintsInfoFuchsia => RawStructureType::eBufferConstraintsInfoFuchsia,
            Self::eBufferCollectionBufferCreateInfoFuchsia => RawStructureType::eBufferCollectionBufferCreateInfoFuchsia,
            Self::eImageConstraintsInfoFuchsia => RawStructureType::eImageConstraintsInfoFuchsia,
            Self::eImageFormatConstraintsInfoFuchsia => RawStructureType::eImageFormatConstraintsInfoFuchsia,
            Self::eSysmemColourSpaceFuchsia => RawStructureType::eSysmemColourSpaceFuchsia,
            Self::eBufferCollectionConstraintsInfoFuchsia => RawStructureType::eBufferCollectionConstraintsInfoFuchsia,
            Self::eSubpassShadingPipelineCreateInfoHuawei => RawStructureType::eSubpassShadingPipelineCreateInfoHuawei,
            Self::ePhysicalDeviceSubpassShadingFeaturesHuawei => RawStructureType::ePhysicalDeviceSubpassShadingFeaturesHuawei,
            Self::ePhysicalDeviceSubpassShadingPropertiesHuawei => RawStructureType::ePhysicalDeviceSubpassShadingPropertiesHuawei,
            Self::ePhysicalDeviceInvocationMaskFeaturesHuawei => RawStructureType::ePhysicalDeviceInvocationMaskFeaturesHuawei,
            Self::eMemoryGetRemoteAddressInfoNv => RawStructureType::eMemoryGetRemoteAddressInfoNv,
            Self::ePhysicalDeviceExternalMemoryRdmaFeaturesNv => RawStructureType::ePhysicalDeviceExternalMemoryRdmaFeaturesNv,
            Self::ePipelinePropertiesIdentifierExt => RawStructureType::ePipelinePropertiesIdentifierExt,
            Self::ePhysicalDevicePipelinePropertiesFeaturesExt => RawStructureType::ePhysicalDevicePipelinePropertiesFeaturesExt,
            Self::eImportFenceSciSyncInfoNv => RawStructureType::eImportFenceSciSyncInfoNv,
            Self::eExportFenceSciSyncInfoNv => RawStructureType::eExportFenceSciSyncInfoNv,
            Self::eFenceGetSciSyncInfoNv => RawStructureType::eFenceGetSciSyncInfoNv,
            Self::eSciSyncAttributesInfoNv => RawStructureType::eSciSyncAttributesInfoNv,
            Self::eImportSemaphoreSciSyncInfoNv => RawStructureType::eImportSemaphoreSciSyncInfoNv,
            Self::eExportSemaphoreSciSyncInfoNv => RawStructureType::eExportSemaphoreSciSyncInfoNv,
            Self::eSemaphoreGetSciSyncInfoNv => RawStructureType::eSemaphoreGetSciSyncInfoNv,
            Self::ePhysicalDeviceExternalSciSyncFeaturesNv => RawStructureType::ePhysicalDeviceExternalSciSyncFeaturesNv,
            Self::eImportMemorySciBufInfoNv => RawStructureType::eImportMemorySciBufInfoNv,
            Self::eExportMemorySciBufInfoNv => RawStructureType::eExportMemorySciBufInfoNv,
            Self::eMemoryGetSciBufInfoNv => RawStructureType::eMemoryGetSciBufInfoNv,
            Self::eMemorySciBufPropertiesNv => RawStructureType::eMemorySciBufPropertiesNv,
            Self::ePhysicalDeviceExternalMemorySciBufFeaturesNv => RawStructureType::ePhysicalDeviceExternalMemorySciBufFeaturesNv,
            Self::ePhysicalDeviceMultisampledRenderToSingleSampledFeaturesExt => RawStructureType::ePhysicalDeviceMultisampledRenderToSingleSampledFeaturesExt,
            Self::eSubpassResolvePerformanceQueryExt => RawStructureType::eSubpassResolvePerformanceQueryExt,
            Self::eMultisampledRenderToSingleSampledInfoExt => RawStructureType::eMultisampledRenderToSingleSampledInfoExt,
            Self::ePhysicalDeviceExtendedDynamicState2FeaturesExt => RawStructureType::ePhysicalDeviceExtendedDynamicState2FeaturesExt,
            Self::eScreenSurfaceCreateInfoQnx => RawStructureType::eScreenSurfaceCreateInfoQnx,
            Self::ePhysicalDeviceColourWriteEnableFeaturesExt => RawStructureType::ePhysicalDeviceColourWriteEnableFeaturesExt,
            Self::ePipelineColourWriteCreateInfoExt => RawStructureType::ePipelineColourWriteCreateInfoExt,
            Self::ePhysicalDevicePrimitivesGeneratedQueryFeaturesExt => RawStructureType::ePhysicalDevicePrimitivesGeneratedQueryFeaturesExt,
            Self::ePhysicalDeviceRayTracingMaintenance1FeaturesKhr => RawStructureType::ePhysicalDeviceRayTracingMaintenance1FeaturesKhr,
            Self::ePhysicalDeviceGlobalPriorityQueryFeaturesKhr => RawStructureType::ePhysicalDeviceGlobalPriorityQueryFeaturesKhr,
            Self::eQueueFamilyGlobalPriorityPropertiesKhr => RawStructureType::eQueueFamilyGlobalPriorityPropertiesKhr,
            Self::ePhysicalDeviceImageViewMinLodFeaturesExt => RawStructureType::ePhysicalDeviceImageViewMinLodFeaturesExt,
            Self::eImageViewMinLodCreateInfoExt => RawStructureType::eImageViewMinLodCreateInfoExt,
            Self::ePhysicalDeviceMultiDrawFeaturesExt => RawStructureType::ePhysicalDeviceMultiDrawFeaturesExt,
            Self::ePhysicalDeviceMultiDrawPropertiesExt => RawStructureType::ePhysicalDeviceMultiDrawPropertiesExt,
            Self::ePhysicalDeviceImage2dViewOf3dFeaturesExt => RawStructureType::ePhysicalDeviceImage2dViewOf3dFeaturesExt,
            Self::eMicromapBuildInfoExt => RawStructureType::eMicromapBuildInfoExt,
            Self::eMicromapVersionInfoExt => RawStructureType::eMicromapVersionInfoExt,
            Self::eCopyMicromapInfoExt => RawStructureType::eCopyMicromapInfoExt,
            Self::eCopyMicromapToMemoryInfoExt => RawStructureType::eCopyMicromapToMemoryInfoExt,
            Self::eCopyMemoryToMicromapInfoExt => RawStructureType::eCopyMemoryToMicromapInfoExt,
            Self::ePhysicalDeviceOpacityMicromapFeaturesExt => RawStructureType::ePhysicalDeviceOpacityMicromapFeaturesExt,
            Self::ePhysicalDeviceOpacityMicromapPropertiesExt => RawStructureType::ePhysicalDeviceOpacityMicromapPropertiesExt,
            Self::eMicromapCreateInfoExt => RawStructureType::eMicromapCreateInfoExt,
            Self::eMicromapBuildSizesInfoExt => RawStructureType::eMicromapBuildSizesInfoExt,
            Self::eAccelerationStructureTrianglesOpacityMicromapExt => RawStructureType::eAccelerationStructureTrianglesOpacityMicromapExt,
            Self::ePhysicalDeviceClusterCullingShaderFeaturesHuawei => RawStructureType::ePhysicalDeviceClusterCullingShaderFeaturesHuawei,
            Self::ePhysicalDeviceClusterCullingShaderPropertiesHuawei => RawStructureType::ePhysicalDeviceClusterCullingShaderPropertiesHuawei,
            Self::ePhysicalDeviceBorderColourSwizzleFeaturesExt => RawStructureType::ePhysicalDeviceBorderColourSwizzleFeaturesExt,
            Self::eSamplerBorderColourComponentMappingCreateInfoExt => RawStructureType::eSamplerBorderColourComponentMappingCreateInfoExt,
            Self::ePhysicalDevicePageableDeviceLocalMemoryFeaturesExt => RawStructureType::ePhysicalDevicePageableDeviceLocalMemoryFeaturesExt,
            Self::ePhysicalDeviceMaintenance4Features => RawStructureType::ePhysicalDeviceMaintenance4Features,
            Self::ePhysicalDeviceMaintenance4Properties => RawStructureType::ePhysicalDeviceMaintenance4Properties,
            Self::eDeviceBufferMemoryRequirements => RawStructureType::eDeviceBufferMemoryRequirements,
            Self::eDeviceImageMemoryRequirements => RawStructureType::eDeviceImageMemoryRequirements,
            Self::ePhysicalDeviceShaderCorePropertiesArm => RawStructureType::ePhysicalDeviceShaderCorePropertiesArm,
            Self::ePhysicalDeviceImageSlicedViewOf3dFeaturesExt => RawStructureType::ePhysicalDeviceImageSlicedViewOf3dFeaturesExt,
            Self::eImageViewSlicedCreateInfoExt => RawStructureType::eImageViewSlicedCreateInfoExt,
            Self::ePhysicalDeviceDescriptorSetHostMappingFeaturesValve => RawStructureType::ePhysicalDeviceDescriptorSetHostMappingFeaturesValve,
            Self::eDescriptorSetBindingReferenceValve => RawStructureType::eDescriptorSetBindingReferenceValve,
            Self::eDescriptorSetLayoutHostMappingInfoValve => RawStructureType::eDescriptorSetLayoutHostMappingInfoValve,
            Self::ePhysicalDeviceDepthClampZeroOneFeaturesExt => RawStructureType::ePhysicalDeviceDepthClampZeroOneFeaturesExt,
            Self::ePhysicalDeviceNonSeamlessCubeMapFeaturesExt => RawStructureType::ePhysicalDeviceNonSeamlessCubeMapFeaturesExt,
            Self::ePhysicalDeviceFragmentDensityMapOffsetFeaturesQcom => RawStructureType::ePhysicalDeviceFragmentDensityMapOffsetFeaturesQcom,
            Self::ePhysicalDeviceFragmentDensityMapOffsetPropertiesQcom => RawStructureType::ePhysicalDeviceFragmentDensityMapOffsetPropertiesQcom,
            Self::eSubpassFragmentDensityMapOffsetEndInfoQcom => RawStructureType::eSubpassFragmentDensityMapOffsetEndInfoQcom,
            Self::ePhysicalDeviceCopyMemoryIndirectFeaturesNv => RawStructureType::ePhysicalDeviceCopyMemoryIndirectFeaturesNv,
            Self::ePhysicalDeviceCopyMemoryIndirectPropertiesNv => RawStructureType::ePhysicalDeviceCopyMemoryIndirectPropertiesNv,
            Self::ePhysicalDeviceMemoryDecompressionFeaturesNv => RawStructureType::ePhysicalDeviceMemoryDecompressionFeaturesNv,
            Self::ePhysicalDeviceMemoryDecompressionPropertiesNv => RawStructureType::ePhysicalDeviceMemoryDecompressionPropertiesNv,
            Self::ePhysicalDeviceLinearColourAttachmentFeaturesNv => RawStructureType::ePhysicalDeviceLinearColourAttachmentFeaturesNv,
            Self::eApplicationParametersExt => RawStructureType::eApplicationParametersExt,
            Self::ePhysicalDeviceImageCompressionControlSwapchainFeaturesExt => RawStructureType::ePhysicalDeviceImageCompressionControlSwapchainFeaturesExt,
            Self::ePhysicalDeviceImageProcessingFeaturesQcom => RawStructureType::ePhysicalDeviceImageProcessingFeaturesQcom,
            Self::ePhysicalDeviceImageProcessingPropertiesQcom => RawStructureType::ePhysicalDeviceImageProcessingPropertiesQcom,
            Self::eImageViewSampleWeightCreateInfoQcom => RawStructureType::eImageViewSampleWeightCreateInfoQcom,
            Self::ePhysicalDeviceExtendedDynamicState3FeaturesExt => RawStructureType::ePhysicalDeviceExtendedDynamicState3FeaturesExt,
            Self::ePhysicalDeviceExtendedDynamicState3PropertiesExt => RawStructureType::ePhysicalDeviceExtendedDynamicState3PropertiesExt,
            Self::ePhysicalDeviceSubpassMergeFeedbackFeaturesExt => RawStructureType::ePhysicalDeviceSubpassMergeFeedbackFeaturesExt,
            Self::eRenderPassCreationControlExt => RawStructureType::eRenderPassCreationControlExt,
            Self::eRenderPassCreationFeedbackCreateInfoExt => RawStructureType::eRenderPassCreationFeedbackCreateInfoExt,
            Self::eRenderPassSubpassFeedbackCreateInfoExt => RawStructureType::eRenderPassSubpassFeedbackCreateInfoExt,
            Self::eDirectDriverLoadingInfoLunarg => RawStructureType::eDirectDriverLoadingInfoLunarg,
            Self::eDirectDriverLoadingListLunarg => RawStructureType::eDirectDriverLoadingListLunarg,
            Self::ePhysicalDeviceShaderModuleIdentifierFeaturesExt => RawStructureType::ePhysicalDeviceShaderModuleIdentifierFeaturesExt,
            Self::ePhysicalDeviceShaderModuleIdentifierPropertiesExt => RawStructureType::ePhysicalDeviceShaderModuleIdentifierPropertiesExt,
            Self::ePipelineShaderStageModuleIdentifierCreateInfoExt => RawStructureType::ePipelineShaderStageModuleIdentifierCreateInfoExt,
            Self::eShaderModuleIdentifierExt => RawStructureType::eShaderModuleIdentifierExt,
            Self::ePhysicalDeviceOpticalFlowFeaturesNv => RawStructureType::ePhysicalDeviceOpticalFlowFeaturesNv,
            Self::ePhysicalDeviceOpticalFlowPropertiesNv => RawStructureType::ePhysicalDeviceOpticalFlowPropertiesNv,
            Self::eOpticalFlowImageFormatInfoNv => RawStructureType::eOpticalFlowImageFormatInfoNv,
            Self::eOpticalFlowImageFormatPropertiesNv => RawStructureType::eOpticalFlowImageFormatPropertiesNv,
            Self::eOpticalFlowSessionCreateInfoNv => RawStructureType::eOpticalFlowSessionCreateInfoNv,
            Self::eOpticalFlowExecuteInfoNv => RawStructureType::eOpticalFlowExecuteInfoNv,
            Self::eOpticalFlowSessionCreatePrivateDataInfoNv => RawStructureType::eOpticalFlowSessionCreatePrivateDataInfoNv,
            Self::ePhysicalDeviceLegacyDitheringFeaturesExt => RawStructureType::ePhysicalDeviceLegacyDitheringFeaturesExt,
            Self::ePhysicalDevicePipelineProtectedAccessFeaturesExt => RawStructureType::ePhysicalDevicePipelineProtectedAccessFeaturesExt,
            Self::ePhysicalDeviceTilePropertiesFeaturesQcom => RawStructureType::ePhysicalDeviceTilePropertiesFeaturesQcom,
            Self::eTilePropertiesQcom => RawStructureType::eTilePropertiesQcom,
            Self::ePhysicalDeviceAmigoProfilingFeaturesSec => RawStructureType::ePhysicalDeviceAmigoProfilingFeaturesSec,
            Self::eAmigoProfilingSubmitInfoSec => RawStructureType::eAmigoProfilingSubmitInfoSec,
            Self::ePhysicalDeviceMultiviewPerViewViewportsFeaturesQcom => RawStructureType::ePhysicalDeviceMultiviewPerViewViewportsFeaturesQcom,
            Self::eSemaphoreSciSyncPoolCreateInfoNv => RawStructureType::eSemaphoreSciSyncPoolCreateInfoNv,
            Self::eSemaphoreSciSyncCreateInfoNv => RawStructureType::eSemaphoreSciSyncCreateInfoNv,
            Self::ePhysicalDeviceExternalSciSync2FeaturesNv => RawStructureType::ePhysicalDeviceExternalSciSync2FeaturesNv,
            Self::eDeviceSemaphoreSciSyncPoolReservationCreateInfoNv => RawStructureType::eDeviceSemaphoreSciSyncPoolReservationCreateInfoNv,
            Self::ePhysicalDeviceRayTracingInvocationReorderFeaturesNv => RawStructureType::ePhysicalDeviceRayTracingInvocationReorderFeaturesNv,
            Self::ePhysicalDeviceRayTracingInvocationReorderPropertiesNv => RawStructureType::ePhysicalDeviceRayTracingInvocationReorderPropertiesNv,
            Self::ePhysicalDeviceShaderCoreBuiltinsFeaturesArm => RawStructureType::ePhysicalDeviceShaderCoreBuiltinsFeaturesArm,
            Self::ePhysicalDeviceShaderCoreBuiltinsPropertiesArm => RawStructureType::ePhysicalDeviceShaderCoreBuiltinsPropertiesArm,
            Self::ePhysicalDevicePipelineLibraryGroupHandlesFeaturesExt => RawStructureType::ePhysicalDevicePipelineLibraryGroupHandlesFeaturesExt,
            Self::ePhysicalDeviceMultiviewPerViewRenderAreasFeaturesQcom => RawStructureType::ePhysicalDeviceMultiviewPerViewRenderAreasFeaturesQcom,
            Self::eMultiviewPerViewRenderAreasRenderPassBeginInfoQcom => RawStructureType::eMultiviewPerViewRenderAreasRenderPassBeginInfoQcom,
            Self::eUnknownVariant(v) => RawStructureType(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SubpassContents {
    eInline,
    eSecondaryCommandBuffers,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl SubpassContents {
    pub fn into_raw(self) -> RawSubpassContents {
        match self {
            Self::eInline => RawSubpassContents::eInline,
            Self::eSecondaryCommandBuffers => RawSubpassContents::eSecondaryCommandBuffers,
            Self::eUnknownVariant(v) => RawSubpassContents(v)
        }
    }
}

pub(crate) use result::*;
pub mod result {
    use super::*;
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
    #[allow(non_camel_case_types)]
    #[non_exhaustive]
    pub enum Result {
        eErrorCompressionExhaustedExt,
        eErrorNoPipelineMatch,
        eErrorInvalidPipelineCacheData,
        eErrorInvalidOpaqueCaptureAddress,
        eErrorFullScreenExclusiveModeLostExt,
        eErrorNotPermittedKhr,
        eErrorFragmentation,
        eErrorInvalidDrmFormatModifierPlaneLayoutExt,
        eErrorInvalidExternalHandle,
        eErrorOutOfPoolMemory,
        eErrorVideoStdVersionNotSupportedKhr,
        eErrorVideoProfileCodecNotSupportedKhr,
        eErrorVideoProfileFormatNotSupportedKhr,
        eErrorVideoProfileOperationNotSupportedKhr,
        eErrorVideoPictureLayoutNotSupportedKhr,
        eErrorImageUsageNotSupportedKhr,
        eErrorInvalidShaderNv,
        eErrorValidationFailedExt,
        eErrorIncompatibleDisplayKhr,
        eErrorOutOfDateKhr,
        eErrorNativeWindowInUseKhr,
        eErrorSurfaceLostKhr,
        eErrorUnknown,
        eErrorFragmentedPool,
        eErrorFormatNotSupported,
        eErrorTooManyObjects,
        eErrorIncompatibleDriver,
        eErrorFeatureNotPresent,
        eErrorExtensionNotPresent,
        eErrorLayerNotPresent,
        eErrorMemoryMapFailed,
        eErrorDeviceLost,
        eErrorInitializationFailed,
        eErrorOutOfDeviceMemory,
        eErrorOutOfHostMemory,
        eSuccess,
        eNotReady,
        eTimeout,
        eEventSet,
        eEventReset,
        eIncomplete,
        eSuboptimalKhr,
        eThreadIdleKhr,
        eThreadDoneKhr,
        eOperationDeferredKhr,
        eOperationNotDeferredKhr,
        ePipelineCompileRequired,
        eUnknownVariant(i32)
    }

    #[allow(non_upper_case_globals)]
    impl Result {
        pub const eErrorFragmentationExt: Self = Self::eErrorFragmentation;
        pub const eErrorInvalidExternalHandleKhr: Self = Self::eErrorInvalidExternalHandle;
        pub const eErrorInvalidDeviceAddressExt: Self = Self::eErrorInvalidOpaqueCaptureAddress;
        pub const eErrorInvalidOpaqueCaptureAddressKhr: Self = Self::eErrorInvalidOpaqueCaptureAddress;
        pub const eErrorNotPermittedExt: Self = Self::eErrorNotPermittedKhr;
        pub const eErrorOutOfPoolMemoryKhr: Self = Self::eErrorOutOfPoolMemory;
        pub const eErrorPipelineCompileRequiredExt: Self = Self::ePipelineCompileRequired;
        pub const ePipelineCompileRequiredExt: Self = Self::ePipelineCompileRequired;

        pub fn into_raw(self) -> RawResult {
            match self {
                Self::eErrorCompressionExhaustedExt => RawResult::eErrorCompressionExhaustedExt,
                Self::eErrorNoPipelineMatch => RawResult::eErrorNoPipelineMatch,
                Self::eErrorInvalidPipelineCacheData => RawResult::eErrorInvalidPipelineCacheData,
                Self::eErrorInvalidOpaqueCaptureAddress => RawResult::eErrorInvalidOpaqueCaptureAddress,
                Self::eErrorFullScreenExclusiveModeLostExt => RawResult::eErrorFullScreenExclusiveModeLostExt,
                Self::eErrorNotPermittedKhr => RawResult::eErrorNotPermittedKhr,
                Self::eErrorFragmentation => RawResult::eErrorFragmentation,
                Self::eErrorInvalidDrmFormatModifierPlaneLayoutExt => RawResult::eErrorInvalidDrmFormatModifierPlaneLayoutExt,
                Self::eErrorInvalidExternalHandle => RawResult::eErrorInvalidExternalHandle,
                Self::eErrorOutOfPoolMemory => RawResult::eErrorOutOfPoolMemory,
                Self::eErrorVideoStdVersionNotSupportedKhr => RawResult::eErrorVideoStdVersionNotSupportedKhr,
                Self::eErrorVideoProfileCodecNotSupportedKhr => RawResult::eErrorVideoProfileCodecNotSupportedKhr,
                Self::eErrorVideoProfileFormatNotSupportedKhr => RawResult::eErrorVideoProfileFormatNotSupportedKhr,
                Self::eErrorVideoProfileOperationNotSupportedKhr => RawResult::eErrorVideoProfileOperationNotSupportedKhr,
                Self::eErrorVideoPictureLayoutNotSupportedKhr => RawResult::eErrorVideoPictureLayoutNotSupportedKhr,
                Self::eErrorImageUsageNotSupportedKhr => RawResult::eErrorImageUsageNotSupportedKhr,
                Self::eErrorInvalidShaderNv => RawResult::eErrorInvalidShaderNv,
                Self::eErrorValidationFailedExt => RawResult::eErrorValidationFailedExt,
                Self::eErrorIncompatibleDisplayKhr => RawResult::eErrorIncompatibleDisplayKhr,
                Self::eErrorOutOfDateKhr => RawResult::eErrorOutOfDateKhr,
                Self::eErrorNativeWindowInUseKhr => RawResult::eErrorNativeWindowInUseKhr,
                Self::eErrorSurfaceLostKhr => RawResult::eErrorSurfaceLostKhr,
                Self::eErrorUnknown => RawResult::eErrorUnknown,
                Self::eErrorFragmentedPool => RawResult::eErrorFragmentedPool,
                Self::eErrorFormatNotSupported => RawResult::eErrorFormatNotSupported,
                Self::eErrorTooManyObjects => RawResult::eErrorTooManyObjects,
                Self::eErrorIncompatibleDriver => RawResult::eErrorIncompatibleDriver,
                Self::eErrorFeatureNotPresent => RawResult::eErrorFeatureNotPresent,
                Self::eErrorExtensionNotPresent => RawResult::eErrorExtensionNotPresent,
                Self::eErrorLayerNotPresent => RawResult::eErrorLayerNotPresent,
                Self::eErrorMemoryMapFailed => RawResult::eErrorMemoryMapFailed,
                Self::eErrorDeviceLost => RawResult::eErrorDeviceLost,
                Self::eErrorInitializationFailed => RawResult::eErrorInitializationFailed,
                Self::eErrorOutOfDeviceMemory => RawResult::eErrorOutOfDeviceMemory,
                Self::eErrorOutOfHostMemory => RawResult::eErrorOutOfHostMemory,
                Self::eSuccess => RawResult::eSuccess,
                Self::eNotReady => RawResult::eNotReady,
                Self::eTimeout => RawResult::eTimeout,
                Self::eEventSet => RawResult::eEventSet,
                Self::eEventReset => RawResult::eEventReset,
                Self::eIncomplete => RawResult::eIncomplete,
                Self::eSuboptimalKhr => RawResult::eSuboptimalKhr,
                Self::eThreadIdleKhr => RawResult::eThreadIdleKhr,
                Self::eThreadDoneKhr => RawResult::eThreadDoneKhr,
                Self::eOperationDeferredKhr => RawResult::eOperationDeferredKhr,
                Self::eOperationNotDeferredKhr => RawResult::eOperationNotDeferredKhr,
                Self::ePipelineCompileRequired => RawResult::ePipelineCompileRequired,
                Self::eUnknownVariant(v) => RawResult(v)
            }
        }
    }

}
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DynamicState {
    eViewport,
    eScissor,
    eLineWidth,
    eDepthBias,
    eBlendConstants,
    eDepthBounds,
    eStencilCompareMask,
    eStencilWriteMask,
    eStencilReference,
    eViewportWScalingNv,
    eDiscardRectangleExt,
    eDiscardRectangleEnableExt,
    eDiscardRectangleModeExt,
    eSampleLocationsExt,
    eViewportShadingRatePaletteNv,
    eViewportCoarseSampleOrderNv,
    eExclusiveScissorEnableNv,
    eExclusiveScissorNv,
    eFragmentShadingRateKhr,
    eLineStippleExt,
    eCullMode,
    eFrontFace,
    ePrimitiveTopology,
    eViewportWithCount,
    eScissorWithCount,
    eVertexInputBindingStride,
    eDepthTestEnable,
    eDepthWriteEnable,
    eDepthCompareOp,
    eDepthBoundsTestEnable,
    eStencilTestEnable,
    eStencilOp,
    eRayTracingPipelineStackSizeKhr,
    eVertexInputExt,
    ePatchControlPointsExt,
    eRasterizerDiscardEnable,
    eDepthBiasEnable,
    eLogicOpExt,
    ePrimitiveRestartEnable,
    eColourWriteEnableExt,
    eTessellationDomainOriginExt,
    eDepthClampEnableExt,
    ePolygonModeExt,
    eRasterizationSamplesExt,
    eSampleMaskExt,
    eAlphaToCoverageEnableExt,
    eAlphaToOneEnableExt,
    eLogicOpEnableExt,
    eColourBlendEnableExt,
    eColourBlendEquationExt,
    eColourWriteMaskExt,
    eRasterizationStreamExt,
    eConservativeRasterizationModeExt,
    eExtraPrimitiveOverestimationSizeExt,
    eDepthClipEnableExt,
    eSampleLocationsEnableExt,
    eColourBlendAdvancedExt,
    eProvokingVertexModeExt,
    eLineRasterizationModeExt,
    eLineStippleEnableExt,
    eDepthClipNegativeOneToOneExt,
    eViewportWScalingEnableNv,
    eViewportSwizzleNv,
    eCoverageToColourEnableNv,
    eCoverageToColourLocationNv,
    eCoverageModulationModeNv,
    eCoverageModulationTableEnableNv,
    eCoverageModulationTableNv,
    eShadingRateImageEnableNv,
    eRepresentativeFragmentTestEnableNv,
    eCoverageReductionModeNv,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl DynamicState {
    pub const eCullModeExt: Self = Self::eCullMode;
    pub const eDepthBiasEnableExt: Self = Self::eDepthBiasEnable;
    pub const eDepthBoundsTestEnableExt: Self = Self::eDepthBoundsTestEnable;
    pub const eDepthCompareOpExt: Self = Self::eDepthCompareOp;
    pub const eDepthTestEnableExt: Self = Self::eDepthTestEnable;
    pub const eDepthWriteEnableExt: Self = Self::eDepthWriteEnable;
    pub const eFrontFaceExt: Self = Self::eFrontFace;
    pub const ePrimitiveRestartEnableExt: Self = Self::ePrimitiveRestartEnable;
    pub const ePrimitiveTopologyExt: Self = Self::ePrimitiveTopology;
    pub const eRasterizerDiscardEnableExt: Self = Self::eRasterizerDiscardEnable;
    pub const eScissorWithCountExt: Self = Self::eScissorWithCount;
    pub const eStencilOpExt: Self = Self::eStencilOp;
    pub const eStencilTestEnableExt: Self = Self::eStencilTestEnable;
    pub const eVertexInputBindingStrideExt: Self = Self::eVertexInputBindingStride;
    pub const eViewportWithCountExt: Self = Self::eViewportWithCount;

    pub fn into_raw(self) -> RawDynamicState {
        match self {
            Self::eViewport => RawDynamicState::eViewport,
            Self::eScissor => RawDynamicState::eScissor,
            Self::eLineWidth => RawDynamicState::eLineWidth,
            Self::eDepthBias => RawDynamicState::eDepthBias,
            Self::eBlendConstants => RawDynamicState::eBlendConstants,
            Self::eDepthBounds => RawDynamicState::eDepthBounds,
            Self::eStencilCompareMask => RawDynamicState::eStencilCompareMask,
            Self::eStencilWriteMask => RawDynamicState::eStencilWriteMask,
            Self::eStencilReference => RawDynamicState::eStencilReference,
            Self::eViewportWScalingNv => RawDynamicState::eViewportWScalingNv,
            Self::eDiscardRectangleExt => RawDynamicState::eDiscardRectangleExt,
            Self::eDiscardRectangleEnableExt => RawDynamicState::eDiscardRectangleEnableExt,
            Self::eDiscardRectangleModeExt => RawDynamicState::eDiscardRectangleModeExt,
            Self::eSampleLocationsExt => RawDynamicState::eSampleLocationsExt,
            Self::eViewportShadingRatePaletteNv => RawDynamicState::eViewportShadingRatePaletteNv,
            Self::eViewportCoarseSampleOrderNv => RawDynamicState::eViewportCoarseSampleOrderNv,
            Self::eExclusiveScissorEnableNv => RawDynamicState::eExclusiveScissorEnableNv,
            Self::eExclusiveScissorNv => RawDynamicState::eExclusiveScissorNv,
            Self::eFragmentShadingRateKhr => RawDynamicState::eFragmentShadingRateKhr,
            Self::eLineStippleExt => RawDynamicState::eLineStippleExt,
            Self::eCullMode => RawDynamicState::eCullMode,
            Self::eFrontFace => RawDynamicState::eFrontFace,
            Self::ePrimitiveTopology => RawDynamicState::ePrimitiveTopology,
            Self::eViewportWithCount => RawDynamicState::eViewportWithCount,
            Self::eScissorWithCount => RawDynamicState::eScissorWithCount,
            Self::eVertexInputBindingStride => RawDynamicState::eVertexInputBindingStride,
            Self::eDepthTestEnable => RawDynamicState::eDepthTestEnable,
            Self::eDepthWriteEnable => RawDynamicState::eDepthWriteEnable,
            Self::eDepthCompareOp => RawDynamicState::eDepthCompareOp,
            Self::eDepthBoundsTestEnable => RawDynamicState::eDepthBoundsTestEnable,
            Self::eStencilTestEnable => RawDynamicState::eStencilTestEnable,
            Self::eStencilOp => RawDynamicState::eStencilOp,
            Self::eRayTracingPipelineStackSizeKhr => RawDynamicState::eRayTracingPipelineStackSizeKhr,
            Self::eVertexInputExt => RawDynamicState::eVertexInputExt,
            Self::ePatchControlPointsExt => RawDynamicState::ePatchControlPointsExt,
            Self::eRasterizerDiscardEnable => RawDynamicState::eRasterizerDiscardEnable,
            Self::eDepthBiasEnable => RawDynamicState::eDepthBiasEnable,
            Self::eLogicOpExt => RawDynamicState::eLogicOpExt,
            Self::ePrimitiveRestartEnable => RawDynamicState::ePrimitiveRestartEnable,
            Self::eColourWriteEnableExt => RawDynamicState::eColourWriteEnableExt,
            Self::eTessellationDomainOriginExt => RawDynamicState::eTessellationDomainOriginExt,
            Self::eDepthClampEnableExt => RawDynamicState::eDepthClampEnableExt,
            Self::ePolygonModeExt => RawDynamicState::ePolygonModeExt,
            Self::eRasterizationSamplesExt => RawDynamicState::eRasterizationSamplesExt,
            Self::eSampleMaskExt => RawDynamicState::eSampleMaskExt,
            Self::eAlphaToCoverageEnableExt => RawDynamicState::eAlphaToCoverageEnableExt,
            Self::eAlphaToOneEnableExt => RawDynamicState::eAlphaToOneEnableExt,
            Self::eLogicOpEnableExt => RawDynamicState::eLogicOpEnableExt,
            Self::eColourBlendEnableExt => RawDynamicState::eColourBlendEnableExt,
            Self::eColourBlendEquationExt => RawDynamicState::eColourBlendEquationExt,
            Self::eColourWriteMaskExt => RawDynamicState::eColourWriteMaskExt,
            Self::eRasterizationStreamExt => RawDynamicState::eRasterizationStreamExt,
            Self::eConservativeRasterizationModeExt => RawDynamicState::eConservativeRasterizationModeExt,
            Self::eExtraPrimitiveOverestimationSizeExt => RawDynamicState::eExtraPrimitiveOverestimationSizeExt,
            Self::eDepthClipEnableExt => RawDynamicState::eDepthClipEnableExt,
            Self::eSampleLocationsEnableExt => RawDynamicState::eSampleLocationsEnableExt,
            Self::eColourBlendAdvancedExt => RawDynamicState::eColourBlendAdvancedExt,
            Self::eProvokingVertexModeExt => RawDynamicState::eProvokingVertexModeExt,
            Self::eLineRasterizationModeExt => RawDynamicState::eLineRasterizationModeExt,
            Self::eLineStippleEnableExt => RawDynamicState::eLineStippleEnableExt,
            Self::eDepthClipNegativeOneToOneExt => RawDynamicState::eDepthClipNegativeOneToOneExt,
            Self::eViewportWScalingEnableNv => RawDynamicState::eViewportWScalingEnableNv,
            Self::eViewportSwizzleNv => RawDynamicState::eViewportSwizzleNv,
            Self::eCoverageToColourEnableNv => RawDynamicState::eCoverageToColourEnableNv,
            Self::eCoverageToColourLocationNv => RawDynamicState::eCoverageToColourLocationNv,
            Self::eCoverageModulationModeNv => RawDynamicState::eCoverageModulationModeNv,
            Self::eCoverageModulationTableEnableNv => RawDynamicState::eCoverageModulationTableEnableNv,
            Self::eCoverageModulationTableNv => RawDynamicState::eCoverageModulationTableNv,
            Self::eShadingRateImageEnableNv => RawDynamicState::eShadingRateImageEnableNv,
            Self::eRepresentativeFragmentTestEnableNv => RawDynamicState::eRepresentativeFragmentTestEnableNv,
            Self::eCoverageReductionModeNv => RawDynamicState::eCoverageReductionModeNv,
            Self::eUnknownVariant(v) => RawDynamicState(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DescriptorUpdateTemplateType {
    eDescriptorSet,
    ePushDescriptorsKhr,
    eUnknownVariant(i32)
}

pub type DescriptorUpdateTemplateTypeKHR = DescriptorUpdateTemplateType;

#[allow(non_upper_case_globals)]
impl DescriptorUpdateTemplateType {
    pub const eDescriptorSetKhr: Self = Self::eDescriptorSet;

    pub fn into_raw(self) -> RawDescriptorUpdateTemplateType {
        match self {
            Self::eDescriptorSet => RawDescriptorUpdateTemplateType::eDescriptorSet,
            Self::ePushDescriptorsKhr => RawDescriptorUpdateTemplateType::ePushDescriptorsKhr,
            Self::eUnknownVariant(v) => RawDescriptorUpdateTemplateType(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ObjectType {
    eUnknown,
    eInstance,
    ePhysicalDevice,
    eDevice,
    eQueue,
    eSemaphore,
    eCommandBuffer,
    eFence,
    eDeviceMemory,
    eBuffer,
    eImage,
    eEvent,
    eQueryPool,
    eBufferView,
    eImageView,
    eShaderModule,
    ePipelineCache,
    ePipelineLayout,
    eRenderPass,
    ePipeline,
    eDescriptorSetLayout,
    eSampler,
    eDescriptorPool,
    eDescriptorSet,
    eFramebuffer,
    eCommandPool,
    eSurfaceKhr,
    eSwapchainKhr,
    eDisplayKhr,
    eDisplayModeKhr,
    eDebugReportCallbackExt,
    eVideoSessionKhr,
    eVideoSessionParametersKhr,
    eCuModuleNvx,
    eCuFunctionNvx,
    eDescriptorUpdateTemplate,
    eDebugUtilsMessengerExt,
    eAccelerationStructureKhr,
    eSamplerYcbcrConversion,
    eValidationCacheExt,
    eAccelerationStructureNv,
    ePerformanceConfigurationIntel,
    eDeferredOperationKhr,
    eIndirectCommandsLayoutNv,
    ePrivateDataSlot,
    eBufferCollectionFuchsia,
    eMicromapExt,
    eOpticalFlowSessionNv,
    eSemaphoreSciSyncPoolNv,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ObjectType {
    pub const eDescriptorUpdateTemplateKhr: Self = Self::eDescriptorUpdateTemplate;
    pub const ePrivateDataSlotExt: Self = Self::ePrivateDataSlot;
    pub const eSamplerYcbcrConversionKhr: Self = Self::eSamplerYcbcrConversion;

    pub fn into_raw(self) -> RawObjectType {
        match self {
            Self::eUnknown => RawObjectType::eUnknown,
            Self::eInstance => RawObjectType::eInstance,
            Self::ePhysicalDevice => RawObjectType::ePhysicalDevice,
            Self::eDevice => RawObjectType::eDevice,
            Self::eQueue => RawObjectType::eQueue,
            Self::eSemaphore => RawObjectType::eSemaphore,
            Self::eCommandBuffer => RawObjectType::eCommandBuffer,
            Self::eFence => RawObjectType::eFence,
            Self::eDeviceMemory => RawObjectType::eDeviceMemory,
            Self::eBuffer => RawObjectType::eBuffer,
            Self::eImage => RawObjectType::eImage,
            Self::eEvent => RawObjectType::eEvent,
            Self::eQueryPool => RawObjectType::eQueryPool,
            Self::eBufferView => RawObjectType::eBufferView,
            Self::eImageView => RawObjectType::eImageView,
            Self::eShaderModule => RawObjectType::eShaderModule,
            Self::ePipelineCache => RawObjectType::ePipelineCache,
            Self::ePipelineLayout => RawObjectType::ePipelineLayout,
            Self::eRenderPass => RawObjectType::eRenderPass,
            Self::ePipeline => RawObjectType::ePipeline,
            Self::eDescriptorSetLayout => RawObjectType::eDescriptorSetLayout,
            Self::eSampler => RawObjectType::eSampler,
            Self::eDescriptorPool => RawObjectType::eDescriptorPool,
            Self::eDescriptorSet => RawObjectType::eDescriptorSet,
            Self::eFramebuffer => RawObjectType::eFramebuffer,
            Self::eCommandPool => RawObjectType::eCommandPool,
            Self::eSurfaceKhr => RawObjectType::eSurfaceKhr,
            Self::eSwapchainKhr => RawObjectType::eSwapchainKhr,
            Self::eDisplayKhr => RawObjectType::eDisplayKhr,
            Self::eDisplayModeKhr => RawObjectType::eDisplayModeKhr,
            Self::eDebugReportCallbackExt => RawObjectType::eDebugReportCallbackExt,
            Self::eVideoSessionKhr => RawObjectType::eVideoSessionKhr,
            Self::eVideoSessionParametersKhr => RawObjectType::eVideoSessionParametersKhr,
            Self::eCuModuleNvx => RawObjectType::eCuModuleNvx,
            Self::eCuFunctionNvx => RawObjectType::eCuFunctionNvx,
            Self::eDescriptorUpdateTemplate => RawObjectType::eDescriptorUpdateTemplate,
            Self::eDebugUtilsMessengerExt => RawObjectType::eDebugUtilsMessengerExt,
            Self::eAccelerationStructureKhr => RawObjectType::eAccelerationStructureKhr,
            Self::eSamplerYcbcrConversion => RawObjectType::eSamplerYcbcrConversion,
            Self::eValidationCacheExt => RawObjectType::eValidationCacheExt,
            Self::eAccelerationStructureNv => RawObjectType::eAccelerationStructureNv,
            Self::ePerformanceConfigurationIntel => RawObjectType::ePerformanceConfigurationIntel,
            Self::eDeferredOperationKhr => RawObjectType::eDeferredOperationKhr,
            Self::eIndirectCommandsLayoutNv => RawObjectType::eIndirectCommandsLayoutNv,
            Self::ePrivateDataSlot => RawObjectType::ePrivateDataSlot,
            Self::eBufferCollectionFuchsia => RawObjectType::eBufferCollectionFuchsia,
            Self::eMicromapExt => RawObjectType::eMicromapExt,
            Self::eOpticalFlowSessionNv => RawObjectType::eOpticalFlowSessionNv,
            Self::eSemaphoreSciSyncPoolNv => RawObjectType::eSemaphoreSciSyncPoolNv,
            Self::eUnknownVariant(v) => RawObjectType(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum RayTracingInvocationReorderModeNV {
    eNone,
    eReorder,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl RayTracingInvocationReorderModeNV {
    pub fn into_raw(self) -> RawRayTracingInvocationReorderModeNV {
        match self {
            Self::eNone => RawRayTracingInvocationReorderModeNV::eNone,
            Self::eReorder => RawRayTracingInvocationReorderModeNV::eReorder,
            Self::eUnknownVariant(v) => RawRayTracingInvocationReorderModeNV(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DirectDriverLoadingModeLUNARG {
    eExclusive,
    eInclusive,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl DirectDriverLoadingModeLUNARG {
    pub fn into_raw(self) -> RawDirectDriverLoadingModeLUNARG {
        match self {
            Self::eExclusive => RawDirectDriverLoadingModeLUNARG::eExclusive,
            Self::eInclusive => RawDirectDriverLoadingModeLUNARG::eInclusive,
            Self::eUnknownVariant(v) => RawDirectDriverLoadingModeLUNARG(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SemaphoreType {
    eBinary,
    eTimeline,
    eUnknownVariant(i32)
}

pub type SemaphoreTypeKHR = SemaphoreType;

#[allow(non_upper_case_globals)]
impl SemaphoreType {
    pub const eBinaryKhr: Self = Self::eBinary;
    pub const eTimelineKhr: Self = Self::eTimeline;

    pub fn into_raw(self) -> RawSemaphoreType {
        match self {
            Self::eBinary => RawSemaphoreType::eBinary,
            Self::eTimeline => RawSemaphoreType::eTimeline,
            Self::eUnknownVariant(v) => RawSemaphoreType(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PresentModeKHR {
    eImmediate,
    eMailbox,
    eFifo,
    eFifoRelaxed,
    eSharedDemandRefresh,
    eSharedContinuousRefresh,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl PresentModeKHR {
    pub fn into_raw(self) -> RawPresentModeKHR {
        match self {
            Self::eImmediate => RawPresentModeKHR::eImmediate,
            Self::eMailbox => RawPresentModeKHR::eMailbox,
            Self::eFifo => RawPresentModeKHR::eFifo,
            Self::eFifoRelaxed => RawPresentModeKHR::eFifoRelaxed,
            Self::eSharedDemandRefresh => RawPresentModeKHR::eSharedDemandRefresh,
            Self::eSharedContinuousRefresh => RawPresentModeKHR::eSharedContinuousRefresh,
            Self::eUnknownVariant(v) => RawPresentModeKHR(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ColourSpaceKHR {
    eSrgbNonlinear,
    eDisplayP3NonlinearExt,
    eExtendedSrgbLinearExt,
    eDisplayP3LinearExt,
    eDciP3NonlinearExt,
    eBt709LinearExt,
    eBt709NonlinearExt,
    eBt2020LinearExt,
    eHdr10St2084Ext,
    eDolbyvisionExt,
    eHdr10HlgExt,
    eAdobergbLinearExt,
    eAdobergbNonlinearExt,
    ePassThroughExt,
    eExtendedSrgbNonlinearExt,
    eDisplayNativeAmd,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ColourSpaceKHR {
    pub const eDciP3LinearExt: Self = Self::eDisplayP3LinearExt;
    pub const eColourspaceSrgbNonlinear: Self = Self::eSrgbNonlinear;

    pub fn into_raw(self) -> RawColourSpaceKHR {
        match self {
            Self::eSrgbNonlinear => RawColourSpaceKHR::eSrgbNonlinear,
            Self::eDisplayP3NonlinearExt => RawColourSpaceKHR::eDisplayP3NonlinearExt,
            Self::eExtendedSrgbLinearExt => RawColourSpaceKHR::eExtendedSrgbLinearExt,
            Self::eDisplayP3LinearExt => RawColourSpaceKHR::eDisplayP3LinearExt,
            Self::eDciP3NonlinearExt => RawColourSpaceKHR::eDciP3NonlinearExt,
            Self::eBt709LinearExt => RawColourSpaceKHR::eBt709LinearExt,
            Self::eBt709NonlinearExt => RawColourSpaceKHR::eBt709NonlinearExt,
            Self::eBt2020LinearExt => RawColourSpaceKHR::eBt2020LinearExt,
            Self::eHdr10St2084Ext => RawColourSpaceKHR::eHdr10St2084Ext,
            Self::eDolbyvisionExt => RawColourSpaceKHR::eDolbyvisionExt,
            Self::eHdr10HlgExt => RawColourSpaceKHR::eHdr10HlgExt,
            Self::eAdobergbLinearExt => RawColourSpaceKHR::eAdobergbLinearExt,
            Self::eAdobergbNonlinearExt => RawColourSpaceKHR::eAdobergbNonlinearExt,
            Self::ePassThroughExt => RawColourSpaceKHR::ePassThroughExt,
            Self::eExtendedSrgbNonlinearExt => RawColourSpaceKHR::eExtendedSrgbNonlinearExt,
            Self::eDisplayNativeAmd => RawColourSpaceKHR::eDisplayNativeAmd,
            Self::eUnknownVariant(v) => RawColourSpaceKHR(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum TimeDomainEXT {
    eDevice,
    eClockMonotonic,
    eClockMonotonicRaw,
    eQueryPerformanceCounter,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl TimeDomainEXT {
    pub fn into_raw(self) -> RawTimeDomainEXT {
        match self {
            Self::eDevice => RawTimeDomainEXT::eDevice,
            Self::eClockMonotonic => RawTimeDomainEXT::eClockMonotonic,
            Self::eClockMonotonicRaw => RawTimeDomainEXT::eClockMonotonicRaw,
            Self::eQueryPerformanceCounter => RawTimeDomainEXT::eQueryPerformanceCounter,
            Self::eUnknownVariant(v) => RawTimeDomainEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DebugReportObjectTypeEXT {
    eUnknown,
    eInstance,
    ePhysicalDevice,
    eDevice,
    eQueue,
    eSemaphore,
    eCommandBuffer,
    eFence,
    eDeviceMemory,
    eBuffer,
    eImage,
    eEvent,
    eQueryPool,
    eBufferView,
    eImageView,
    eShaderModule,
    ePipelineCache,
    ePipelineLayout,
    eRenderPass,
    ePipeline,
    eDescriptorSetLayout,
    eSampler,
    eDescriptorPool,
    eDescriptorSet,
    eFramebuffer,
    eCommandPool,
    eSurfaceKhr,
    eSwapchainKhr,
    eDebugReportCallbackExt,
    eDisplayKhr,
    eDisplayModeKhr,
    eValidationCacheExt,
    eCuModuleNvx,
    eCuFunctionNvx,
    eDescriptorUpdateTemplate,
    eAccelerationStructureKhr,
    eSamplerYcbcrConversion,
    eAccelerationStructureNv,
    eBufferCollectionFuchsia,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl DebugReportObjectTypeEXT {
    pub const eDebugReport: Self = Self::eDebugReportCallbackExt;
    pub const eDescriptorUpdateTemplateKhr: Self = Self::eDescriptorUpdateTemplate;
    pub const eSamplerYcbcrConversionKhr: Self = Self::eSamplerYcbcrConversion;
    pub const eValidationCache: Self = Self::eValidationCacheExt;

    pub fn into_raw(self) -> RawDebugReportObjectTypeEXT {
        match self {
            Self::eUnknown => RawDebugReportObjectTypeEXT::eUnknown,
            Self::eInstance => RawDebugReportObjectTypeEXT::eInstance,
            Self::ePhysicalDevice => RawDebugReportObjectTypeEXT::ePhysicalDevice,
            Self::eDevice => RawDebugReportObjectTypeEXT::eDevice,
            Self::eQueue => RawDebugReportObjectTypeEXT::eQueue,
            Self::eSemaphore => RawDebugReportObjectTypeEXT::eSemaphore,
            Self::eCommandBuffer => RawDebugReportObjectTypeEXT::eCommandBuffer,
            Self::eFence => RawDebugReportObjectTypeEXT::eFence,
            Self::eDeviceMemory => RawDebugReportObjectTypeEXT::eDeviceMemory,
            Self::eBuffer => RawDebugReportObjectTypeEXT::eBuffer,
            Self::eImage => RawDebugReportObjectTypeEXT::eImage,
            Self::eEvent => RawDebugReportObjectTypeEXT::eEvent,
            Self::eQueryPool => RawDebugReportObjectTypeEXT::eQueryPool,
            Self::eBufferView => RawDebugReportObjectTypeEXT::eBufferView,
            Self::eImageView => RawDebugReportObjectTypeEXT::eImageView,
            Self::eShaderModule => RawDebugReportObjectTypeEXT::eShaderModule,
            Self::ePipelineCache => RawDebugReportObjectTypeEXT::ePipelineCache,
            Self::ePipelineLayout => RawDebugReportObjectTypeEXT::ePipelineLayout,
            Self::eRenderPass => RawDebugReportObjectTypeEXT::eRenderPass,
            Self::ePipeline => RawDebugReportObjectTypeEXT::ePipeline,
            Self::eDescriptorSetLayout => RawDebugReportObjectTypeEXT::eDescriptorSetLayout,
            Self::eSampler => RawDebugReportObjectTypeEXT::eSampler,
            Self::eDescriptorPool => RawDebugReportObjectTypeEXT::eDescriptorPool,
            Self::eDescriptorSet => RawDebugReportObjectTypeEXT::eDescriptorSet,
            Self::eFramebuffer => RawDebugReportObjectTypeEXT::eFramebuffer,
            Self::eCommandPool => RawDebugReportObjectTypeEXT::eCommandPool,
            Self::eSurfaceKhr => RawDebugReportObjectTypeEXT::eSurfaceKhr,
            Self::eSwapchainKhr => RawDebugReportObjectTypeEXT::eSwapchainKhr,
            Self::eDebugReportCallbackExt => RawDebugReportObjectTypeEXT::eDebugReportCallbackExt,
            Self::eDisplayKhr => RawDebugReportObjectTypeEXT::eDisplayKhr,
            Self::eDisplayModeKhr => RawDebugReportObjectTypeEXT::eDisplayModeKhr,
            Self::eValidationCacheExt => RawDebugReportObjectTypeEXT::eValidationCacheExt,
            Self::eCuModuleNvx => RawDebugReportObjectTypeEXT::eCuModuleNvx,
            Self::eCuFunctionNvx => RawDebugReportObjectTypeEXT::eCuFunctionNvx,
            Self::eDescriptorUpdateTemplate => RawDebugReportObjectTypeEXT::eDescriptorUpdateTemplate,
            Self::eAccelerationStructureKhr => RawDebugReportObjectTypeEXT::eAccelerationStructureKhr,
            Self::eSamplerYcbcrConversion => RawDebugReportObjectTypeEXT::eSamplerYcbcrConversion,
            Self::eAccelerationStructureNv => RawDebugReportObjectTypeEXT::eAccelerationStructureNv,
            Self::eBufferCollectionFuchsia => RawDebugReportObjectTypeEXT::eBufferCollectionFuchsia,
            Self::eUnknownVariant(v) => RawDebugReportObjectTypeEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DeviceMemoryReportEventTypeEXT {
    eAllocate,
    eFree,
    eImport,
    eUnimport,
    eAllocationFailed,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl DeviceMemoryReportEventTypeEXT {
    pub fn into_raw(self) -> RawDeviceMemoryReportEventTypeEXT {
        match self {
            Self::eAllocate => RawDeviceMemoryReportEventTypeEXT::eAllocate,
            Self::eFree => RawDeviceMemoryReportEventTypeEXT::eFree,
            Self::eImport => RawDeviceMemoryReportEventTypeEXT::eImport,
            Self::eUnimport => RawDeviceMemoryReportEventTypeEXT::eUnimport,
            Self::eAllocationFailed => RawDeviceMemoryReportEventTypeEXT::eAllocationFailed,
            Self::eUnknownVariant(v) => RawDeviceMemoryReportEventTypeEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum RasterizationOrderAMD {
    eStrict,
    eRelaxed,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl RasterizationOrderAMD {
    pub fn into_raw(self) -> RawRasterizationOrderAMD {
        match self {
            Self::eStrict => RawRasterizationOrderAMD::eStrict,
            Self::eRelaxed => RawRasterizationOrderAMD::eRelaxed,
            Self::eUnknownVariant(v) => RawRasterizationOrderAMD(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ValidationCheckEXT {
    eAll,
    eShaders,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ValidationCheckEXT {
    pub fn into_raw(self) -> RawValidationCheckEXT {
        match self {
            Self::eAll => RawValidationCheckEXT::eAll,
            Self::eShaders => RawValidationCheckEXT::eShaders,
            Self::eUnknownVariant(v) => RawValidationCheckEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ValidationFeatureEnableEXT {
    eGpuAssisted,
    eGpuAssistedReserveBindingSlot,
    eBestPractices,
    eDebugPrintf,
    eSynchronizationValidation,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ValidationFeatureEnableEXT {
    pub fn into_raw(self) -> RawValidationFeatureEnableEXT {
        match self {
            Self::eGpuAssisted => RawValidationFeatureEnableEXT::eGpuAssisted,
            Self::eGpuAssistedReserveBindingSlot => RawValidationFeatureEnableEXT::eGpuAssistedReserveBindingSlot,
            Self::eBestPractices => RawValidationFeatureEnableEXT::eBestPractices,
            Self::eDebugPrintf => RawValidationFeatureEnableEXT::eDebugPrintf,
            Self::eSynchronizationValidation => RawValidationFeatureEnableEXT::eSynchronizationValidation,
            Self::eUnknownVariant(v) => RawValidationFeatureEnableEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ValidationFeatureDisableEXT {
    eAll,
    eShaders,
    eThreadSafety,
    eApiParameters,
    eObjectLifetimes,
    eCoreChecks,
    eUniqueHandles,
    eShaderValidationCache,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ValidationFeatureDisableEXT {
    pub fn into_raw(self) -> RawValidationFeatureDisableEXT {
        match self {
            Self::eAll => RawValidationFeatureDisableEXT::eAll,
            Self::eShaders => RawValidationFeatureDisableEXT::eShaders,
            Self::eThreadSafety => RawValidationFeatureDisableEXT::eThreadSafety,
            Self::eApiParameters => RawValidationFeatureDisableEXT::eApiParameters,
            Self::eObjectLifetimes => RawValidationFeatureDisableEXT::eObjectLifetimes,
            Self::eCoreChecks => RawValidationFeatureDisableEXT::eCoreChecks,
            Self::eUniqueHandles => RawValidationFeatureDisableEXT::eUniqueHandles,
            Self::eShaderValidationCache => RawValidationFeatureDisableEXT::eShaderValidationCache,
            Self::eUnknownVariant(v) => RawValidationFeatureDisableEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum IndirectCommandsTokenTypeNV {
    eShaderGroup,
    eStateFlags,
    eIndexBuffer,
    eVertexBuffer,
    ePushConstant,
    eDrawIndexed,
    eDraw,
    eDrawTasks,
    eDrawMeshTasks,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl IndirectCommandsTokenTypeNV {
    pub fn into_raw(self) -> RawIndirectCommandsTokenTypeNV {
        match self {
            Self::eShaderGroup => RawIndirectCommandsTokenTypeNV::eShaderGroup,
            Self::eStateFlags => RawIndirectCommandsTokenTypeNV::eStateFlags,
            Self::eIndexBuffer => RawIndirectCommandsTokenTypeNV::eIndexBuffer,
            Self::eVertexBuffer => RawIndirectCommandsTokenTypeNV::eVertexBuffer,
            Self::ePushConstant => RawIndirectCommandsTokenTypeNV::ePushConstant,
            Self::eDrawIndexed => RawIndirectCommandsTokenTypeNV::eDrawIndexed,
            Self::eDraw => RawIndirectCommandsTokenTypeNV::eDraw,
            Self::eDrawTasks => RawIndirectCommandsTokenTypeNV::eDrawTasks,
            Self::eDrawMeshTasks => RawIndirectCommandsTokenTypeNV::eDrawMeshTasks,
            Self::eUnknownVariant(v) => RawIndirectCommandsTokenTypeNV(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DisplayPowerStateEXT {
    eOff,
    eSuspend,
    eOn,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl DisplayPowerStateEXT {
    pub fn into_raw(self) -> RawDisplayPowerStateEXT {
        match self {
            Self::eOff => RawDisplayPowerStateEXT::eOff,
            Self::eSuspend => RawDisplayPowerStateEXT::eSuspend,
            Self::eOn => RawDisplayPowerStateEXT::eOn,
            Self::eUnknownVariant(v) => RawDisplayPowerStateEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DeviceEventTypeEXT {
    eDisplayHotplug,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl DeviceEventTypeEXT {
    pub fn into_raw(self) -> RawDeviceEventTypeEXT {
        match self {
            Self::eDisplayHotplug => RawDeviceEventTypeEXT::eDisplayHotplug,
            Self::eUnknownVariant(v) => RawDeviceEventTypeEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DisplayEventTypeEXT {
    eFirstPixelOut,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl DisplayEventTypeEXT {
    pub fn into_raw(self) -> RawDisplayEventTypeEXT {
        match self {
            Self::eFirstPixelOut => RawDisplayEventTypeEXT::eFirstPixelOut,
            Self::eUnknownVariant(v) => RawDisplayEventTypeEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ViewportCoordinateSwizzleNV {
    ePositiveX,
    eNegativeX,
    ePositiveY,
    eNegativeY,
    ePositiveZ,
    eNegativeZ,
    ePositiveW,
    eNegativeW,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ViewportCoordinateSwizzleNV {
    pub fn into_raw(self) -> RawViewportCoordinateSwizzleNV {
        match self {
            Self::ePositiveX => RawViewportCoordinateSwizzleNV::ePositiveX,
            Self::eNegativeX => RawViewportCoordinateSwizzleNV::eNegativeX,
            Self::ePositiveY => RawViewportCoordinateSwizzleNV::ePositiveY,
            Self::eNegativeY => RawViewportCoordinateSwizzleNV::eNegativeY,
            Self::ePositiveZ => RawViewportCoordinateSwizzleNV::ePositiveZ,
            Self::eNegativeZ => RawViewportCoordinateSwizzleNV::eNegativeZ,
            Self::ePositiveW => RawViewportCoordinateSwizzleNV::ePositiveW,
            Self::eNegativeW => RawViewportCoordinateSwizzleNV::eNegativeW,
            Self::eUnknownVariant(v) => RawViewportCoordinateSwizzleNV(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DiscardRectangleModeEXT {
    eInclusive,
    eExclusive,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl DiscardRectangleModeEXT {
    pub fn into_raw(self) -> RawDiscardRectangleModeEXT {
        match self {
            Self::eInclusive => RawDiscardRectangleModeEXT::eInclusive,
            Self::eExclusive => RawDiscardRectangleModeEXT::eExclusive,
            Self::eUnknownVariant(v) => RawDiscardRectangleModeEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PointClippingBehavior {
    eAllClipPlanes,
    eUserClipPlanesOnly,
    eUnknownVariant(i32)
}

pub type PointClippingBehaviorKHR = PointClippingBehavior;

#[allow(non_upper_case_globals)]
impl PointClippingBehavior {
    pub const eAllClipPlanesKhr: Self = Self::eAllClipPlanes;
    pub const eUserClipPlanesOnlyKhr: Self = Self::eUserClipPlanesOnly;

    pub fn into_raw(self) -> RawPointClippingBehavior {
        match self {
            Self::eAllClipPlanes => RawPointClippingBehavior::eAllClipPlanes,
            Self::eUserClipPlanesOnly => RawPointClippingBehavior::eUserClipPlanesOnly,
            Self::eUnknownVariant(v) => RawPointClippingBehavior(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SamplerReductionMode {
    eWeightedAverage,
    eMin,
    eMax,
    eUnknownVariant(i32)
}

pub type SamplerReductionModeEXT = SamplerReductionMode;

#[allow(non_upper_case_globals)]
impl SamplerReductionMode {
    pub const eMaxExt: Self = Self::eMax;
    pub const eMinExt: Self = Self::eMin;
    pub const eWeightedAverageExt: Self = Self::eWeightedAverage;

    pub fn into_raw(self) -> RawSamplerReductionMode {
        match self {
            Self::eWeightedAverage => RawSamplerReductionMode::eWeightedAverage,
            Self::eMin => RawSamplerReductionMode::eMin,
            Self::eMax => RawSamplerReductionMode::eMax,
            Self::eUnknownVariant(v) => RawSamplerReductionMode(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum TessellationDomainOrigin {
    eUpperLeft,
    eLowerLeft,
    eUnknownVariant(i32)
}

pub type TessellationDomainOriginKHR = TessellationDomainOrigin;

#[allow(non_upper_case_globals)]
impl TessellationDomainOrigin {
    pub const eLowerLeftKhr: Self = Self::eLowerLeft;
    pub const eUpperLeftKhr: Self = Self::eUpperLeft;

    pub fn into_raw(self) -> RawTessellationDomainOrigin {
        match self {
            Self::eUpperLeft => RawTessellationDomainOrigin::eUpperLeft,
            Self::eLowerLeft => RawTessellationDomainOrigin::eLowerLeft,
            Self::eUnknownVariant(v) => RawTessellationDomainOrigin(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SamplerYcbcrModelConversion {
    eRgbIdentity,
    eYcbcrIdentity,
    eYcbcr709,
    eYcbcr601,
    eYcbcr2020,
    eUnknownVariant(i32)
}

pub type SamplerYcbcrModelConversionKHR = SamplerYcbcrModelConversion;

#[allow(non_upper_case_globals)]
impl SamplerYcbcrModelConversion {
    pub const eRgbIdentityKhr: Self = Self::eRgbIdentity;
    pub const eYcbcr2020Khr: Self = Self::eYcbcr2020;
    pub const eYcbcr601Khr: Self = Self::eYcbcr601;
    pub const eYcbcr709Khr: Self = Self::eYcbcr709;
    pub const eYcbcrIdentityKhr: Self = Self::eYcbcrIdentity;

    pub fn into_raw(self) -> RawSamplerYcbcrModelConversion {
        match self {
            Self::eRgbIdentity => RawSamplerYcbcrModelConversion::eRgbIdentity,
            Self::eYcbcrIdentity => RawSamplerYcbcrModelConversion::eYcbcrIdentity,
            Self::eYcbcr709 => RawSamplerYcbcrModelConversion::eYcbcr709,
            Self::eYcbcr601 => RawSamplerYcbcrModelConversion::eYcbcr601,
            Self::eYcbcr2020 => RawSamplerYcbcrModelConversion::eYcbcr2020,
            Self::eUnknownVariant(v) => RawSamplerYcbcrModelConversion(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SamplerYcbcrRange {
    eItuFull,
    eItuNarrow,
    eUnknownVariant(i32)
}

pub type SamplerYcbcrRangeKHR = SamplerYcbcrRange;

#[allow(non_upper_case_globals)]
impl SamplerYcbcrRange {
    pub const eItuFullKhr: Self = Self::eItuFull;
    pub const eItuNarrowKhr: Self = Self::eItuNarrow;

    pub fn into_raw(self) -> RawSamplerYcbcrRange {
        match self {
            Self::eItuFull => RawSamplerYcbcrRange::eItuFull,
            Self::eItuNarrow => RawSamplerYcbcrRange::eItuNarrow,
            Self::eUnknownVariant(v) => RawSamplerYcbcrRange(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ChromaLocation {
    eCositedEven,
    eMidpoint,
    eUnknownVariant(i32)
}

pub type ChromaLocationKHR = ChromaLocation;

#[allow(non_upper_case_globals)]
impl ChromaLocation {
    pub const eCositedEvenKhr: Self = Self::eCositedEven;
    pub const eMidpointKhr: Self = Self::eMidpoint;

    pub fn into_raw(self) -> RawChromaLocation {
        match self {
            Self::eCositedEven => RawChromaLocation::eCositedEven,
            Self::eMidpoint => RawChromaLocation::eMidpoint,
            Self::eUnknownVariant(v) => RawChromaLocation(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum BlendOverlapEXT {
    eUncorrelated,
    eDisjoint,
    eConjoint,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl BlendOverlapEXT {
    pub fn into_raw(self) -> RawBlendOverlapEXT {
        match self {
            Self::eUncorrelated => RawBlendOverlapEXT::eUncorrelated,
            Self::eDisjoint => RawBlendOverlapEXT::eDisjoint,
            Self::eConjoint => RawBlendOverlapEXT::eConjoint,
            Self::eUnknownVariant(v) => RawBlendOverlapEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum CoverageModulationModeNV {
    eNone,
    eRgb,
    eAlpha,
    eRgba,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl CoverageModulationModeNV {
    pub fn into_raw(self) -> RawCoverageModulationModeNV {
        match self {
            Self::eNone => RawCoverageModulationModeNV::eNone,
            Self::eRgb => RawCoverageModulationModeNV::eRgb,
            Self::eAlpha => RawCoverageModulationModeNV::eAlpha,
            Self::eRgba => RawCoverageModulationModeNV::eRgba,
            Self::eUnknownVariant(v) => RawCoverageModulationModeNV(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum CoverageReductionModeNV {
    eMerge,
    eTruncate,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl CoverageReductionModeNV {
    pub fn into_raw(self) -> RawCoverageReductionModeNV {
        match self {
            Self::eMerge => RawCoverageReductionModeNV::eMerge,
            Self::eTruncate => RawCoverageReductionModeNV::eTruncate,
            Self::eUnknownVariant(v) => RawCoverageReductionModeNV(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ValidationCacheHeaderVersionEXT {
    eOne,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ValidationCacheHeaderVersionEXT {
    pub fn into_raw(self) -> RawValidationCacheHeaderVersionEXT {
        match self {
            Self::eOne => RawValidationCacheHeaderVersionEXT::eOne,
            Self::eUnknownVariant(v) => RawValidationCacheHeaderVersionEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ShaderInfoTypeAMD {
    eStatistics,
    eBinary,
    eDisassembly,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ShaderInfoTypeAMD {
    pub fn into_raw(self) -> RawShaderInfoTypeAMD {
        match self {
            Self::eStatistics => RawShaderInfoTypeAMD::eStatistics,
            Self::eBinary => RawShaderInfoTypeAMD::eBinary,
            Self::eDisassembly => RawShaderInfoTypeAMD::eDisassembly,
            Self::eUnknownVariant(v) => RawShaderInfoTypeAMD(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum QueueGlobalPriorityKHR {
    eLow,
    eMedium,
    eHigh,
    eRealtime,
    eUnknownVariant(i32)
}

pub type QueueGlobalPriorityEXT = QueueGlobalPriorityKHR;

#[allow(non_upper_case_globals)]
impl QueueGlobalPriorityKHR {
    pub const eHighExt: Self = Self::eHigh;
    pub const eLowExt: Self = Self::eLow;
    pub const eMediumExt: Self = Self::eMedium;
    pub const eRealtimeExt: Self = Self::eRealtime;

    pub fn into_raw(self) -> RawQueueGlobalPriorityKHR {
        match self {
            Self::eLow => RawQueueGlobalPriorityKHR::eLow,
            Self::eMedium => RawQueueGlobalPriorityKHR::eMedium,
            Self::eHigh => RawQueueGlobalPriorityKHR::eHigh,
            Self::eRealtime => RawQueueGlobalPriorityKHR::eRealtime,
            Self::eUnknownVariant(v) => RawQueueGlobalPriorityKHR(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ConservativeRasterizationModeEXT {
    eDisabled,
    eOverestimate,
    eUnderestimate,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ConservativeRasterizationModeEXT {
    pub fn into_raw(self) -> RawConservativeRasterizationModeEXT {
        match self {
            Self::eDisabled => RawConservativeRasterizationModeEXT::eDisabled,
            Self::eOverestimate => RawConservativeRasterizationModeEXT::eOverestimate,
            Self::eUnderestimate => RawConservativeRasterizationModeEXT::eUnderestimate,
            Self::eUnknownVariant(v) => RawConservativeRasterizationModeEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum VendorId {
    eViv,
    eVsi,
    eKazan,
    eCodeplay,
    eMesa,
    ePocl,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl VendorId {
    pub fn into_raw(self) -> RawVendorId {
        match self {
            Self::eViv => RawVendorId::eViv,
            Self::eVsi => RawVendorId::eVsi,
            Self::eKazan => RawVendorId::eKazan,
            Self::eCodeplay => RawVendorId::eCodeplay,
            Self::eMesa => RawVendorId::eMesa,
            Self::ePocl => RawVendorId::ePocl,
            Self::eUnknownVariant(v) => RawVendorId(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DriverId {
    eAmdProprietary,
    eAmdOpenSource,
    eMesaRadv,
    eNvidiaProprietary,
    eIntelProprietaryWindows,
    eIntelOpenSourceMesa,
    eImaginationProprietary,
    eQualcommProprietary,
    eArmProprietary,
    eGoogleSwiftshader,
    eGgpProprietary,
    eBroadcomProprietary,
    eMesaLlvmpipe,
    eMoltenvk,
    eCoreaviProprietary,
    eJuiceProprietary,
    eVerisiliconProprietary,
    eMesaTurnip,
    eMesaV3dv,
    eMesaPanvk,
    eSamsungProprietary,
    eMesaVenus,
    eMesaDozen,
    eMesaNvk,
    eImaginationOpenSourceMesa,
    eUnknownVariant(i32)
}

pub type DriverIdKHR = DriverId;

#[allow(non_upper_case_globals)]
impl DriverId {
    pub const eAmdOpenSourceKhr: Self = Self::eAmdOpenSource;
    pub const eAmdProprietaryKhr: Self = Self::eAmdProprietary;
    pub const eArmProprietaryKhr: Self = Self::eArmProprietary;
    pub const eBroadcomProprietaryKhr: Self = Self::eBroadcomProprietary;
    pub const eGgpProprietaryKhr: Self = Self::eGgpProprietary;
    pub const eGoogleSwiftshaderKhr: Self = Self::eGoogleSwiftshader;
    pub const eImaginationProprietaryKhr: Self = Self::eImaginationProprietary;
    pub const eIntelOpenSourceMesaKhr: Self = Self::eIntelOpenSourceMesa;
    pub const eIntelProprietaryWindowsKhr: Self = Self::eIntelProprietaryWindows;
    pub const eMesaRadvKhr: Self = Self::eMesaRadv;
    pub const eNvidiaProprietaryKhr: Self = Self::eNvidiaProprietary;
    pub const eQualcommProprietaryKhr: Self = Self::eQualcommProprietary;

    pub fn into_raw(self) -> RawDriverId {
        match self {
            Self::eAmdProprietary => RawDriverId::eAmdProprietary,
            Self::eAmdOpenSource => RawDriverId::eAmdOpenSource,
            Self::eMesaRadv => RawDriverId::eMesaRadv,
            Self::eNvidiaProprietary => RawDriverId::eNvidiaProprietary,
            Self::eIntelProprietaryWindows => RawDriverId::eIntelProprietaryWindows,
            Self::eIntelOpenSourceMesa => RawDriverId::eIntelOpenSourceMesa,
            Self::eImaginationProprietary => RawDriverId::eImaginationProprietary,
            Self::eQualcommProprietary => RawDriverId::eQualcommProprietary,
            Self::eArmProprietary => RawDriverId::eArmProprietary,
            Self::eGoogleSwiftshader => RawDriverId::eGoogleSwiftshader,
            Self::eGgpProprietary => RawDriverId::eGgpProprietary,
            Self::eBroadcomProprietary => RawDriverId::eBroadcomProprietary,
            Self::eMesaLlvmpipe => RawDriverId::eMesaLlvmpipe,
            Self::eMoltenvk => RawDriverId::eMoltenvk,
            Self::eCoreaviProprietary => RawDriverId::eCoreaviProprietary,
            Self::eJuiceProprietary => RawDriverId::eJuiceProprietary,
            Self::eVerisiliconProprietary => RawDriverId::eVerisiliconProprietary,
            Self::eMesaTurnip => RawDriverId::eMesaTurnip,
            Self::eMesaV3dv => RawDriverId::eMesaV3dv,
            Self::eMesaPanvk => RawDriverId::eMesaPanvk,
            Self::eSamsungProprietary => RawDriverId::eSamsungProprietary,
            Self::eMesaVenus => RawDriverId::eMesaVenus,
            Self::eMesaDozen => RawDriverId::eMesaDozen,
            Self::eMesaNvk => RawDriverId::eMesaNvk,
            Self::eImaginationOpenSourceMesa => RawDriverId::eImaginationOpenSourceMesa,
            Self::eUnknownVariant(v) => RawDriverId(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ShadingRatePaletteEntryNV {
    eNoInvocations,
    e16InvocationsPerPixel,
    e8InvocationsPerPixel,
    e4InvocationsPerPixel,
    e2InvocationsPerPixel,
    e1InvocationPerPixel,
    e1InvocationPer2X1Pixels,
    e1InvocationPer1X2Pixels,
    e1InvocationPer2X2Pixels,
    e1InvocationPer4X2Pixels,
    e1InvocationPer2X4Pixels,
    e1InvocationPer4X4Pixels,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ShadingRatePaletteEntryNV {
    pub fn into_raw(self) -> RawShadingRatePaletteEntryNV {
        match self {
            Self::eNoInvocations => RawShadingRatePaletteEntryNV::eNoInvocations,
            Self::e16InvocationsPerPixel => RawShadingRatePaletteEntryNV::e16InvocationsPerPixel,
            Self::e8InvocationsPerPixel => RawShadingRatePaletteEntryNV::e8InvocationsPerPixel,
            Self::e4InvocationsPerPixel => RawShadingRatePaletteEntryNV::e4InvocationsPerPixel,
            Self::e2InvocationsPerPixel => RawShadingRatePaletteEntryNV::e2InvocationsPerPixel,
            Self::e1InvocationPerPixel => RawShadingRatePaletteEntryNV::e1InvocationPerPixel,
            Self::e1InvocationPer2X1Pixels => RawShadingRatePaletteEntryNV::e1InvocationPer2X1Pixels,
            Self::e1InvocationPer1X2Pixels => RawShadingRatePaletteEntryNV::e1InvocationPer1X2Pixels,
            Self::e1InvocationPer2X2Pixels => RawShadingRatePaletteEntryNV::e1InvocationPer2X2Pixels,
            Self::e1InvocationPer4X2Pixels => RawShadingRatePaletteEntryNV::e1InvocationPer4X2Pixels,
            Self::e1InvocationPer2X4Pixels => RawShadingRatePaletteEntryNV::e1InvocationPer2X4Pixels,
            Self::e1InvocationPer4X4Pixels => RawShadingRatePaletteEntryNV::e1InvocationPer4X4Pixels,
            Self::eUnknownVariant(v) => RawShadingRatePaletteEntryNV(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum CoarseSampleOrderTypeNV {
    eDefault,
    eCustom,
    ePixelMajor,
    eSampleMajor,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl CoarseSampleOrderTypeNV {
    pub fn into_raw(self) -> RawCoarseSampleOrderTypeNV {
        match self {
            Self::eDefault => RawCoarseSampleOrderTypeNV::eDefault,
            Self::eCustom => RawCoarseSampleOrderTypeNV::eCustom,
            Self::ePixelMajor => RawCoarseSampleOrderTypeNV::ePixelMajor,
            Self::eSampleMajor => RawCoarseSampleOrderTypeNV::eSampleMajor,
            Self::eUnknownVariant(v) => RawCoarseSampleOrderTypeNV(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum CopyAccelerationStructureModeKHR {
    eClone,
    eCompact,
    eSerialize,
    eDeserialize,
    eUnknownVariant(i32)
}

pub type CopyAccelerationStructureModeNV = CopyAccelerationStructureModeKHR;

#[allow(non_upper_case_globals)]
impl CopyAccelerationStructureModeKHR {
    pub const eCloneNv: Self = Self::eClone;
    pub const eCompactNv: Self = Self::eCompact;

    pub fn into_raw(self) -> RawCopyAccelerationStructureModeKHR {
        match self {
            Self::eClone => RawCopyAccelerationStructureModeKHR::eClone,
            Self::eCompact => RawCopyAccelerationStructureModeKHR::eCompact,
            Self::eSerialize => RawCopyAccelerationStructureModeKHR::eSerialize,
            Self::eDeserialize => RawCopyAccelerationStructureModeKHR::eDeserialize,
            Self::eUnknownVariant(v) => RawCopyAccelerationStructureModeKHR(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum BuildAccelerationStructureModeKHR {
    eBuild,
    eUpdate,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl BuildAccelerationStructureModeKHR {
    pub fn into_raw(self) -> RawBuildAccelerationStructureModeKHR {
        match self {
            Self::eBuild => RawBuildAccelerationStructureModeKHR::eBuild,
            Self::eUpdate => RawBuildAccelerationStructureModeKHR::eUpdate,
            Self::eUnknownVariant(v) => RawBuildAccelerationStructureModeKHR(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum AccelerationStructureTypeKHR {
    eTopLevel,
    eBottomLevel,
    eGeneric,
    eUnknownVariant(i32)
}

pub type AccelerationStructureTypeNV = AccelerationStructureTypeKHR;

#[allow(non_upper_case_globals)]
impl AccelerationStructureTypeKHR {
    pub const eBottomLevelNv: Self = Self::eBottomLevel;
    pub const eTopLevelNv: Self = Self::eTopLevel;

    pub fn into_raw(self) -> RawAccelerationStructureTypeKHR {
        match self {
            Self::eTopLevel => RawAccelerationStructureTypeKHR::eTopLevel,
            Self::eBottomLevel => RawAccelerationStructureTypeKHR::eBottomLevel,
            Self::eGeneric => RawAccelerationStructureTypeKHR::eGeneric,
            Self::eUnknownVariant(v) => RawAccelerationStructureTypeKHR(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum GeometryTypeKHR {
    eTriangles,
    eAabbs,
    eInstances,
    eUnknownVariant(i32)
}

pub type GeometryTypeNV = GeometryTypeKHR;

#[allow(non_upper_case_globals)]
impl GeometryTypeKHR {
    pub const eAabbsNv: Self = Self::eAabbs;
    pub const eTrianglesNv: Self = Self::eTriangles;

    pub fn into_raw(self) -> RawGeometryTypeKHR {
        match self {
            Self::eTriangles => RawGeometryTypeKHR::eTriangles,
            Self::eAabbs => RawGeometryTypeKHR::eAabbs,
            Self::eInstances => RawGeometryTypeKHR::eInstances,
            Self::eUnknownVariant(v) => RawGeometryTypeKHR(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum AccelerationStructureMemoryRequirementsTypeNV {
    eObject,
    eBuildScratch,
    eUpdateScratch,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl AccelerationStructureMemoryRequirementsTypeNV {
    pub fn into_raw(self) -> RawAccelerationStructureMemoryRequirementsTypeNV {
        match self {
            Self::eObject => RawAccelerationStructureMemoryRequirementsTypeNV::eObject,
            Self::eBuildScratch => RawAccelerationStructureMemoryRequirementsTypeNV::eBuildScratch,
            Self::eUpdateScratch => RawAccelerationStructureMemoryRequirementsTypeNV::eUpdateScratch,
            Self::eUnknownVariant(v) => RawAccelerationStructureMemoryRequirementsTypeNV(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum AccelerationStructureBuildTypeKHR {
    eHost,
    eDevice,
    eHostOrDevice,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl AccelerationStructureBuildTypeKHR {
    pub fn into_raw(self) -> RawAccelerationStructureBuildTypeKHR {
        match self {
            Self::eHost => RawAccelerationStructureBuildTypeKHR::eHost,
            Self::eDevice => RawAccelerationStructureBuildTypeKHR::eDevice,
            Self::eHostOrDevice => RawAccelerationStructureBuildTypeKHR::eHostOrDevice,
            Self::eUnknownVariant(v) => RawAccelerationStructureBuildTypeKHR(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum RayTracingShaderGroupTypeKHR {
    eGeneral,
    eTrianglesHitGroup,
    eProceduralHitGroup,
    eUnknownVariant(i32)
}

pub type RayTracingShaderGroupTypeNV = RayTracingShaderGroupTypeKHR;

#[allow(non_upper_case_globals)]
impl RayTracingShaderGroupTypeKHR {
    pub const eGeneralNv: Self = Self::eGeneral;
    pub const eProceduralHitGroupNv: Self = Self::eProceduralHitGroup;
    pub const eTrianglesHitGroupNv: Self = Self::eTrianglesHitGroup;

    pub fn into_raw(self) -> RawRayTracingShaderGroupTypeKHR {
        match self {
            Self::eGeneral => RawRayTracingShaderGroupTypeKHR::eGeneral,
            Self::eTrianglesHitGroup => RawRayTracingShaderGroupTypeKHR::eTrianglesHitGroup,
            Self::eProceduralHitGroup => RawRayTracingShaderGroupTypeKHR::eProceduralHitGroup,
            Self::eUnknownVariant(v) => RawRayTracingShaderGroupTypeKHR(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum AccelerationStructureCompatibilityKHR {
    eCompatible,
    eIncompatible,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl AccelerationStructureCompatibilityKHR {
    pub fn into_raw(self) -> RawAccelerationStructureCompatibilityKHR {
        match self {
            Self::eCompatible => RawAccelerationStructureCompatibilityKHR::eCompatible,
            Self::eIncompatible => RawAccelerationStructureCompatibilityKHR::eIncompatible,
            Self::eUnknownVariant(v) => RawAccelerationStructureCompatibilityKHR(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ShaderGroupShaderKHR {
    eGeneral,
    eClosestHit,
    eAnyHit,
    eIntersection,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ShaderGroupShaderKHR {
    pub fn into_raw(self) -> RawShaderGroupShaderKHR {
        match self {
            Self::eGeneral => RawShaderGroupShaderKHR::eGeneral,
            Self::eClosestHit => RawShaderGroupShaderKHR::eClosestHit,
            Self::eAnyHit => RawShaderGroupShaderKHR::eAnyHit,
            Self::eIntersection => RawShaderGroupShaderKHR::eIntersection,
            Self::eUnknownVariant(v) => RawShaderGroupShaderKHR(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum MemoryOverallocationBehaviorAMD {
    eDefault,
    eAllowed,
    eDisallowed,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl MemoryOverallocationBehaviorAMD {
    pub fn into_raw(self) -> RawMemoryOverallocationBehaviorAMD {
        match self {
            Self::eDefault => RawMemoryOverallocationBehaviorAMD::eDefault,
            Self::eAllowed => RawMemoryOverallocationBehaviorAMD::eAllowed,
            Self::eDisallowed => RawMemoryOverallocationBehaviorAMD::eDisallowed,
            Self::eUnknownVariant(v) => RawMemoryOverallocationBehaviorAMD(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ScopeNV {
    eDevice,
    eWorkgroup,
    eSubgroup,
    eQueueFamily,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ScopeNV {
    pub fn into_raw(self) -> RawScopeNV {
        match self {
            Self::eDevice => RawScopeNV::eDevice,
            Self::eWorkgroup => RawScopeNV::eWorkgroup,
            Self::eSubgroup => RawScopeNV::eSubgroup,
            Self::eQueueFamily => RawScopeNV::eQueueFamily,
            Self::eUnknownVariant(v) => RawScopeNV(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ComponentTypeNV {
    eFloat16,
    eFloat32,
    eFloat64,
    eSint8,
    eSint16,
    eSint32,
    eSint64,
    eUint8,
    eUint16,
    eUint32,
    eUint64,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ComponentTypeNV {
    pub fn into_raw(self) -> RawComponentTypeNV {
        match self {
            Self::eFloat16 => RawComponentTypeNV::eFloat16,
            Self::eFloat32 => RawComponentTypeNV::eFloat32,
            Self::eFloat64 => RawComponentTypeNV::eFloat64,
            Self::eSint8 => RawComponentTypeNV::eSint8,
            Self::eSint16 => RawComponentTypeNV::eSint16,
            Self::eSint32 => RawComponentTypeNV::eSint32,
            Self::eSint64 => RawComponentTypeNV::eSint64,
            Self::eUint8 => RawComponentTypeNV::eUint8,
            Self::eUint16 => RawComponentTypeNV::eUint16,
            Self::eUint32 => RawComponentTypeNV::eUint32,
            Self::eUint64 => RawComponentTypeNV::eUint64,
            Self::eUnknownVariant(v) => RawComponentTypeNV(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum FullScreenExclusiveEXT {
    eDefault,
    eAllowed,
    eDisallowed,
    eApplicationControlled,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl FullScreenExclusiveEXT {
    pub fn into_raw(self) -> RawFullScreenExclusiveEXT {
        match self {
            Self::eDefault => RawFullScreenExclusiveEXT::eDefault,
            Self::eAllowed => RawFullScreenExclusiveEXT::eAllowed,
            Self::eDisallowed => RawFullScreenExclusiveEXT::eDisallowed,
            Self::eApplicationControlled => RawFullScreenExclusiveEXT::eApplicationControlled,
            Self::eUnknownVariant(v) => RawFullScreenExclusiveEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PerformanceCounterScopeKHR {
    eCommandBuffer,
    eRenderPass,
    eCommand,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl PerformanceCounterScopeKHR {
    pub const eQueryScopeCommandBuffer: Self = Self::eCommandBuffer;
    pub const eQueryScopeCommand: Self = Self::eCommand;
    pub const eQueryScopeRenderPass: Self = Self::eRenderPass;

    pub fn into_raw(self) -> RawPerformanceCounterScopeKHR {
        match self {
            Self::eCommandBuffer => RawPerformanceCounterScopeKHR::eCommandBuffer,
            Self::eRenderPass => RawPerformanceCounterScopeKHR::eRenderPass,
            Self::eCommand => RawPerformanceCounterScopeKHR::eCommand,
            Self::eUnknownVariant(v) => RawPerformanceCounterScopeKHR(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PerformanceCounterUnitKHR {
    eGeneric,
    ePercentage,
    eNanoseconds,
    eBytes,
    eBytesPerSecond,
    eKelvin,
    eWatts,
    eVolts,
    eAmps,
    eHertz,
    eCycles,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl PerformanceCounterUnitKHR {
    pub fn into_raw(self) -> RawPerformanceCounterUnitKHR {
        match self {
            Self::eGeneric => RawPerformanceCounterUnitKHR::eGeneric,
            Self::ePercentage => RawPerformanceCounterUnitKHR::ePercentage,
            Self::eNanoseconds => RawPerformanceCounterUnitKHR::eNanoseconds,
            Self::eBytes => RawPerformanceCounterUnitKHR::eBytes,
            Self::eBytesPerSecond => RawPerformanceCounterUnitKHR::eBytesPerSecond,
            Self::eKelvin => RawPerformanceCounterUnitKHR::eKelvin,
            Self::eWatts => RawPerformanceCounterUnitKHR::eWatts,
            Self::eVolts => RawPerformanceCounterUnitKHR::eVolts,
            Self::eAmps => RawPerformanceCounterUnitKHR::eAmps,
            Self::eHertz => RawPerformanceCounterUnitKHR::eHertz,
            Self::eCycles => RawPerformanceCounterUnitKHR::eCycles,
            Self::eUnknownVariant(v) => RawPerformanceCounterUnitKHR(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PerformanceCounterStorageKHR {
    eInt32,
    eInt64,
    eUint32,
    eUint64,
    eFloat32,
    eFloat64,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl PerformanceCounterStorageKHR {
    pub fn into_raw(self) -> RawPerformanceCounterStorageKHR {
        match self {
            Self::eInt32 => RawPerformanceCounterStorageKHR::eInt32,
            Self::eInt64 => RawPerformanceCounterStorageKHR::eInt64,
            Self::eUint32 => RawPerformanceCounterStorageKHR::eUint32,
            Self::eUint64 => RawPerformanceCounterStorageKHR::eUint64,
            Self::eFloat32 => RawPerformanceCounterStorageKHR::eFloat32,
            Self::eFloat64 => RawPerformanceCounterStorageKHR::eFloat64,
            Self::eUnknownVariant(v) => RawPerformanceCounterStorageKHR(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PerformanceConfigurationTypeINTEL {
    eCommandQueueMetricsDiscoveryActivated,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl PerformanceConfigurationTypeINTEL {
    pub fn into_raw(self) -> RawPerformanceConfigurationTypeINTEL {
        match self {
            Self::eCommandQueueMetricsDiscoveryActivated => RawPerformanceConfigurationTypeINTEL::eCommandQueueMetricsDiscoveryActivated,
            Self::eUnknownVariant(v) => RawPerformanceConfigurationTypeINTEL(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum QueryPoolSamplingModeINTEL {
    eManual,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl QueryPoolSamplingModeINTEL {
    pub fn into_raw(self) -> RawQueryPoolSamplingModeINTEL {
        match self {
            Self::eManual => RawQueryPoolSamplingModeINTEL::eManual,
            Self::eUnknownVariant(v) => RawQueryPoolSamplingModeINTEL(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PerformanceOverrideTypeINTEL {
    eNullHardware,
    eFlushGpuCaches,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl PerformanceOverrideTypeINTEL {
    pub fn into_raw(self) -> RawPerformanceOverrideTypeINTEL {
        match self {
            Self::eNullHardware => RawPerformanceOverrideTypeINTEL::eNullHardware,
            Self::eFlushGpuCaches => RawPerformanceOverrideTypeINTEL::eFlushGpuCaches,
            Self::eUnknownVariant(v) => RawPerformanceOverrideTypeINTEL(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PerformanceParameterTypeINTEL {
    eHwCountersSupported,
    eStreamMarkerValidBits,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl PerformanceParameterTypeINTEL {
    pub fn into_raw(self) -> RawPerformanceParameterTypeINTEL {
        match self {
            Self::eHwCountersSupported => RawPerformanceParameterTypeINTEL::eHwCountersSupported,
            Self::eStreamMarkerValidBits => RawPerformanceParameterTypeINTEL::eStreamMarkerValidBits,
            Self::eUnknownVariant(v) => RawPerformanceParameterTypeINTEL(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PerformanceValueTypeINTEL {
    eUint32,
    eUint64,
    eFloat,
    eBool,
    eString,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl PerformanceValueTypeINTEL {
    pub fn into_raw(self) -> RawPerformanceValueTypeINTEL {
        match self {
            Self::eUint32 => RawPerformanceValueTypeINTEL::eUint32,
            Self::eUint64 => RawPerformanceValueTypeINTEL::eUint64,
            Self::eFloat => RawPerformanceValueTypeINTEL::eFloat,
            Self::eBool => RawPerformanceValueTypeINTEL::eBool,
            Self::eString => RawPerformanceValueTypeINTEL::eString,
            Self::eUnknownVariant(v) => RawPerformanceValueTypeINTEL(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ShaderFloatControlsIndependence {
    e32BitOnly,
    eAll,
    eNone,
    eUnknownVariant(i32)
}

pub type ShaderFloatControlsIndependenceKHR = ShaderFloatControlsIndependence;

#[allow(non_upper_case_globals)]
impl ShaderFloatControlsIndependence {
    pub const e32BitOnlyKhr: Self = Self::e32BitOnly;
    pub const eAllKhr: Self = Self::eAll;
    pub const eNoneKhr: Self = Self::eNone;

    pub fn into_raw(self) -> RawShaderFloatControlsIndependence {
        match self {
            Self::e32BitOnly => RawShaderFloatControlsIndependence::e32BitOnly,
            Self::eAll => RawShaderFloatControlsIndependence::eAll,
            Self::eNone => RawShaderFloatControlsIndependence::eNone,
            Self::eUnknownVariant(v) => RawShaderFloatControlsIndependence(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PipelineExecutableStatisticFormatKHR {
    eBool32,
    eInt64,
    eUint64,
    eFloat64,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl PipelineExecutableStatisticFormatKHR {
    pub fn into_raw(self) -> RawPipelineExecutableStatisticFormatKHR {
        match self {
            Self::eBool32 => RawPipelineExecutableStatisticFormatKHR::eBool32,
            Self::eInt64 => RawPipelineExecutableStatisticFormatKHR::eInt64,
            Self::eUint64 => RawPipelineExecutableStatisticFormatKHR::eUint64,
            Self::eFloat64 => RawPipelineExecutableStatisticFormatKHR::eFloat64,
            Self::eUnknownVariant(v) => RawPipelineExecutableStatisticFormatKHR(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum LineRasterizationModeEXT {
    eDefault,
    eRectangular,
    eBresenham,
    eRectangularSmooth,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl LineRasterizationModeEXT {
    pub fn into_raw(self) -> RawLineRasterizationModeEXT {
        match self {
            Self::eDefault => RawLineRasterizationModeEXT::eDefault,
            Self::eRectangular => RawLineRasterizationModeEXT::eRectangular,
            Self::eBresenham => RawLineRasterizationModeEXT::eBresenham,
            Self::eRectangularSmooth => RawLineRasterizationModeEXT::eRectangularSmooth,
            Self::eUnknownVariant(v) => RawLineRasterizationModeEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum FragmentShadingRateCombinerOpKHR {
    eKeep,
    eReplace,
    eMin,
    eMax,
    eMul,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl FragmentShadingRateCombinerOpKHR {
    pub fn into_raw(self) -> RawFragmentShadingRateCombinerOpKHR {
        match self {
            Self::eKeep => RawFragmentShadingRateCombinerOpKHR::eKeep,
            Self::eReplace => RawFragmentShadingRateCombinerOpKHR::eReplace,
            Self::eMin => RawFragmentShadingRateCombinerOpKHR::eMin,
            Self::eMax => RawFragmentShadingRateCombinerOpKHR::eMax,
            Self::eMul => RawFragmentShadingRateCombinerOpKHR::eMul,
            Self::eUnknownVariant(v) => RawFragmentShadingRateCombinerOpKHR(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum FragmentShadingRateNV {
    e1InvocationPerPixel,
    e1InvocationPer1X2Pixels,
    e1InvocationPer2X1Pixels,
    e1InvocationPer2X2Pixels,
    e1InvocationPer2X4Pixels,
    e1InvocationPer4X2Pixels,
    e1InvocationPer4X4Pixels,
    e2InvocationsPerPixel,
    e4InvocationsPerPixel,
    e8InvocationsPerPixel,
    e16InvocationsPerPixel,
    eNoInvocations,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl FragmentShadingRateNV {
    pub fn into_raw(self) -> RawFragmentShadingRateNV {
        match self {
            Self::e1InvocationPerPixel => RawFragmentShadingRateNV::e1InvocationPerPixel,
            Self::e1InvocationPer1X2Pixels => RawFragmentShadingRateNV::e1InvocationPer1X2Pixels,
            Self::e1InvocationPer2X1Pixels => RawFragmentShadingRateNV::e1InvocationPer2X1Pixels,
            Self::e1InvocationPer2X2Pixels => RawFragmentShadingRateNV::e1InvocationPer2X2Pixels,
            Self::e1InvocationPer2X4Pixels => RawFragmentShadingRateNV::e1InvocationPer2X4Pixels,
            Self::e1InvocationPer4X2Pixels => RawFragmentShadingRateNV::e1InvocationPer4X2Pixels,
            Self::e1InvocationPer4X4Pixels => RawFragmentShadingRateNV::e1InvocationPer4X4Pixels,
            Self::e2InvocationsPerPixel => RawFragmentShadingRateNV::e2InvocationsPerPixel,
            Self::e4InvocationsPerPixel => RawFragmentShadingRateNV::e4InvocationsPerPixel,
            Self::e8InvocationsPerPixel => RawFragmentShadingRateNV::e8InvocationsPerPixel,
            Self::e16InvocationsPerPixel => RawFragmentShadingRateNV::e16InvocationsPerPixel,
            Self::eNoInvocations => RawFragmentShadingRateNV::eNoInvocations,
            Self::eUnknownVariant(v) => RawFragmentShadingRateNV(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum FragmentShadingRateTypeNV {
    eFragmentSize,
    eEnums,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl FragmentShadingRateTypeNV {
    pub fn into_raw(self) -> RawFragmentShadingRateTypeNV {
        match self {
            Self::eFragmentSize => RawFragmentShadingRateTypeNV::eFragmentSize,
            Self::eEnums => RawFragmentShadingRateTypeNV::eEnums,
            Self::eUnknownVariant(v) => RawFragmentShadingRateTypeNV(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SubpassMergeStatusEXT {
    eMerged,
    eDisallowed,
    eNotMergedSideEffects,
    eNotMergedSamplesMismatch,
    eNotMergedViewsMismatch,
    eNotMergedAliasing,
    eNotMergedDependencies,
    eNotMergedIncompatibleInputAttachment,
    eNotMergedTooManyAttachments,
    eNotMergedInsufficientStorage,
    eNotMergedDepthStencilCount,
    eNotMergedResolveAttachmentReuse,
    eNotMergedSingleSubpass,
    eNotMergedUnspecified,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl SubpassMergeStatusEXT {
    pub fn into_raw(self) -> RawSubpassMergeStatusEXT {
        match self {
            Self::eMerged => RawSubpassMergeStatusEXT::eMerged,
            Self::eDisallowed => RawSubpassMergeStatusEXT::eDisallowed,
            Self::eNotMergedSideEffects => RawSubpassMergeStatusEXT::eNotMergedSideEffects,
            Self::eNotMergedSamplesMismatch => RawSubpassMergeStatusEXT::eNotMergedSamplesMismatch,
            Self::eNotMergedViewsMismatch => RawSubpassMergeStatusEXT::eNotMergedViewsMismatch,
            Self::eNotMergedAliasing => RawSubpassMergeStatusEXT::eNotMergedAliasing,
            Self::eNotMergedDependencies => RawSubpassMergeStatusEXT::eNotMergedDependencies,
            Self::eNotMergedIncompatibleInputAttachment => RawSubpassMergeStatusEXT::eNotMergedIncompatibleInputAttachment,
            Self::eNotMergedTooManyAttachments => RawSubpassMergeStatusEXT::eNotMergedTooManyAttachments,
            Self::eNotMergedInsufficientStorage => RawSubpassMergeStatusEXT::eNotMergedInsufficientStorage,
            Self::eNotMergedDepthStencilCount => RawSubpassMergeStatusEXT::eNotMergedDepthStencilCount,
            Self::eNotMergedResolveAttachmentReuse => RawSubpassMergeStatusEXT::eNotMergedResolveAttachmentReuse,
            Self::eNotMergedSingleSubpass => RawSubpassMergeStatusEXT::eNotMergedSingleSubpass,
            Self::eNotMergedUnspecified => RawSubpassMergeStatusEXT::eNotMergedUnspecified,
            Self::eUnknownVariant(v) => RawSubpassMergeStatusEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ProvokingVertexModeEXT {
    eFirstVertex,
    eLastVertex,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ProvokingVertexModeEXT {
    pub fn into_raw(self) -> RawProvokingVertexModeEXT {
        match self {
            Self::eFirstVertex => RawProvokingVertexModeEXT::eFirstVertex,
            Self::eLastVertex => RawProvokingVertexModeEXT::eLastVertex,
            Self::eUnknownVariant(v) => RawProvokingVertexModeEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum AccelerationStructureMotionInstanceTypeNV {
    eStatic,
    eMatrixMotion,
    eSrtMotion,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl AccelerationStructureMotionInstanceTypeNV {
    pub fn into_raw(self) -> RawAccelerationStructureMotionInstanceTypeNV {
        match self {
            Self::eStatic => RawAccelerationStructureMotionInstanceTypeNV::eStatic,
            Self::eMatrixMotion => RawAccelerationStructureMotionInstanceTypeNV::eMatrixMotion,
            Self::eSrtMotion => RawAccelerationStructureMotionInstanceTypeNV::eSrtMotion,
            Self::eUnknownVariant(v) => RawAccelerationStructureMotionInstanceTypeNV(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DeviceAddressBindingTypeEXT {
    eBind,
    eUnbind,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl DeviceAddressBindingTypeEXT {
    pub fn into_raw(self) -> RawDeviceAddressBindingTypeEXT {
        match self {
            Self::eBind => RawDeviceAddressBindingTypeEXT::eBind,
            Self::eUnbind => RawDeviceAddressBindingTypeEXT::eUnbind,
            Self::eUnknownVariant(v) => RawDeviceAddressBindingTypeEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum QueryResultStatusKHR {
    eError,
    eNotReady,
    eComplete,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl QueryResultStatusKHR {
    pub fn into_raw(self) -> RawQueryResultStatusKHR {
        match self {
            Self::eError => RawQueryResultStatusKHR::eError,
            Self::eNotReady => RawQueryResultStatusKHR::eNotReady,
            Self::eComplete => RawQueryResultStatusKHR::eComplete,
            Self::eUnknownVariant(v) => RawQueryResultStatusKHR(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PipelineRobustnessBufferBehaviorEXT {
    eDeviceDefault,
    eDisabled,
    eRobustBufferAccess,
    eRobustBufferAccess2,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl PipelineRobustnessBufferBehaviorEXT {
    pub fn into_raw(self) -> RawPipelineRobustnessBufferBehaviorEXT {
        match self {
            Self::eDeviceDefault => RawPipelineRobustnessBufferBehaviorEXT::eDeviceDefault,
            Self::eDisabled => RawPipelineRobustnessBufferBehaviorEXT::eDisabled,
            Self::eRobustBufferAccess => RawPipelineRobustnessBufferBehaviorEXT::eRobustBufferAccess,
            Self::eRobustBufferAccess2 => RawPipelineRobustnessBufferBehaviorEXT::eRobustBufferAccess2,
            Self::eUnknownVariant(v) => RawPipelineRobustnessBufferBehaviorEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PipelineRobustnessImageBehaviorEXT {
    eDeviceDefault,
    eDisabled,
    eRobustImageAccess,
    eRobustImageAccess2,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl PipelineRobustnessImageBehaviorEXT {
    pub fn into_raw(self) -> RawPipelineRobustnessImageBehaviorEXT {
        match self {
            Self::eDeviceDefault => RawPipelineRobustnessImageBehaviorEXT::eDeviceDefault,
            Self::eDisabled => RawPipelineRobustnessImageBehaviorEXT::eDisabled,
            Self::eRobustImageAccess => RawPipelineRobustnessImageBehaviorEXT::eRobustImageAccess,
            Self::eRobustImageAccess2 => RawPipelineRobustnessImageBehaviorEXT::eRobustImageAccess2,
            Self::eUnknownVariant(v) => RawPipelineRobustnessImageBehaviorEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum OpticalFlowPerformanceLevelNV {
    eUnknown,
    eSlow,
    eMedium,
    eFast,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl OpticalFlowPerformanceLevelNV {
    pub fn into_raw(self) -> RawOpticalFlowPerformanceLevelNV {
        match self {
            Self::eUnknown => RawOpticalFlowPerformanceLevelNV::eUnknown,
            Self::eSlow => RawOpticalFlowPerformanceLevelNV::eSlow,
            Self::eMedium => RawOpticalFlowPerformanceLevelNV::eMedium,
            Self::eFast => RawOpticalFlowPerformanceLevelNV::eFast,
            Self::eUnknownVariant(v) => RawOpticalFlowPerformanceLevelNV(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum OpticalFlowSessionBindingPointNV {
    eUnknown,
    eInput,
    eReference,
    eHint,
    eFlowVector,
    eBackwardFlowVector,
    eCost,
    eBackwardCost,
    eGlobalFlow,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl OpticalFlowSessionBindingPointNV {
    pub fn into_raw(self) -> RawOpticalFlowSessionBindingPointNV {
        match self {
            Self::eUnknown => RawOpticalFlowSessionBindingPointNV::eUnknown,
            Self::eInput => RawOpticalFlowSessionBindingPointNV::eInput,
            Self::eReference => RawOpticalFlowSessionBindingPointNV::eReference,
            Self::eHint => RawOpticalFlowSessionBindingPointNV::eHint,
            Self::eFlowVector => RawOpticalFlowSessionBindingPointNV::eFlowVector,
            Self::eBackwardFlowVector => RawOpticalFlowSessionBindingPointNV::eBackwardFlowVector,
            Self::eCost => RawOpticalFlowSessionBindingPointNV::eCost,
            Self::eBackwardCost => RawOpticalFlowSessionBindingPointNV::eBackwardCost,
            Self::eGlobalFlow => RawOpticalFlowSessionBindingPointNV::eGlobalFlow,
            Self::eUnknownVariant(v) => RawOpticalFlowSessionBindingPointNV(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum MicromapTypeEXT {
    eOpacityMicromap,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl MicromapTypeEXT {
    pub fn into_raw(self) -> RawMicromapTypeEXT {
        match self {
            Self::eOpacityMicromap => RawMicromapTypeEXT::eOpacityMicromap,
            Self::eUnknownVariant(v) => RawMicromapTypeEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum CopyMicromapModeEXT {
    eClone,
    eSerialize,
    eDeserialize,
    eCompact,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl CopyMicromapModeEXT {
    pub fn into_raw(self) -> RawCopyMicromapModeEXT {
        match self {
            Self::eClone => RawCopyMicromapModeEXT::eClone,
            Self::eSerialize => RawCopyMicromapModeEXT::eSerialize,
            Self::eDeserialize => RawCopyMicromapModeEXT::eDeserialize,
            Self::eCompact => RawCopyMicromapModeEXT::eCompact,
            Self::eUnknownVariant(v) => RawCopyMicromapModeEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum BuildMicromapModeEXT {
    eBuild,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl BuildMicromapModeEXT {
    pub fn into_raw(self) -> RawBuildMicromapModeEXT {
        match self {
            Self::eBuild => RawBuildMicromapModeEXT::eBuild,
            Self::eUnknownVariant(v) => RawBuildMicromapModeEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum OpacityMicromapFormatEXT {
    e2State,
    e4State,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl OpacityMicromapFormatEXT {
    pub fn into_raw(self) -> RawOpacityMicromapFormatEXT {
        match self {
            Self::e2State => RawOpacityMicromapFormatEXT::e2State,
            Self::e4State => RawOpacityMicromapFormatEXT::e4State,
            Self::eUnknownVariant(v) => RawOpacityMicromapFormatEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum OpacityMicromapSpecialIndexEXT {
    eFullyUnknownOpaque,
    eFullyUnknownTransparent,
    eFullyOpaque,
    eFullyTransparent,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl OpacityMicromapSpecialIndexEXT {
    pub fn into_raw(self) -> RawOpacityMicromapSpecialIndexEXT {
        match self {
            Self::eFullyUnknownOpaque => RawOpacityMicromapSpecialIndexEXT::eFullyUnknownOpaque,
            Self::eFullyUnknownTransparent => RawOpacityMicromapSpecialIndexEXT::eFullyUnknownTransparent,
            Self::eFullyOpaque => RawOpacityMicromapSpecialIndexEXT::eFullyOpaque,
            Self::eFullyTransparent => RawOpacityMicromapSpecialIndexEXT::eFullyTransparent,
            Self::eUnknownVariant(v) => RawOpacityMicromapSpecialIndexEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DeviceFaultAddressTypeEXT {
    eNone,
    eReadInvalid,
    eWriteInvalid,
    eExecuteInvalid,
    eInstructionPointerUnknown,
    eInstructionPointerInvalid,
    eInstructionPointerFault,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl DeviceFaultAddressTypeEXT {
    pub fn into_raw(self) -> RawDeviceFaultAddressTypeEXT {
        match self {
            Self::eNone => RawDeviceFaultAddressTypeEXT::eNone,
            Self::eReadInvalid => RawDeviceFaultAddressTypeEXT::eReadInvalid,
            Self::eWriteInvalid => RawDeviceFaultAddressTypeEXT::eWriteInvalid,
            Self::eExecuteInvalid => RawDeviceFaultAddressTypeEXT::eExecuteInvalid,
            Self::eInstructionPointerUnknown => RawDeviceFaultAddressTypeEXT::eInstructionPointerUnknown,
            Self::eInstructionPointerInvalid => RawDeviceFaultAddressTypeEXT::eInstructionPointerInvalid,
            Self::eInstructionPointerFault => RawDeviceFaultAddressTypeEXT::eInstructionPointerFault,
            Self::eUnknownVariant(v) => RawDeviceFaultAddressTypeEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DeviceFaultVendorBinaryHeaderVersionEXT {
    eOne,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl DeviceFaultVendorBinaryHeaderVersionEXT {
    pub fn into_raw(self) -> RawDeviceFaultVendorBinaryHeaderVersionEXT {
        match self {
            Self::eOne => RawDeviceFaultVendorBinaryHeaderVersionEXT::eOne,
            Self::eUnknownVariant(v) => RawDeviceFaultVendorBinaryHeaderVersionEXT(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH264ChromaFormatIdc {
    eMonochrome,
    e420,
    e422,
    e444,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH264ChromaFormatIdc {
    pub fn into_raw(self) -> RawStdVideoH264ChromaFormatIdc {
        match self {
            Self::eMonochrome => RawStdVideoH264ChromaFormatIdc::eMonochrome,
            Self::e420 => RawStdVideoH264ChromaFormatIdc::e420,
            Self::e422 => RawStdVideoH264ChromaFormatIdc::e422,
            Self::e444 => RawStdVideoH264ChromaFormatIdc::e444,
            Self::eInvalid => RawStdVideoH264ChromaFormatIdc::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH264ChromaFormatIdc(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH264ProfileIdc {
    eBaseline,
    eMain,
    eHigh,
    eHigh444Predictive,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH264ProfileIdc {
    pub fn into_raw(self) -> RawStdVideoH264ProfileIdc {
        match self {
            Self::eBaseline => RawStdVideoH264ProfileIdc::eBaseline,
            Self::eMain => RawStdVideoH264ProfileIdc::eMain,
            Self::eHigh => RawStdVideoH264ProfileIdc::eHigh,
            Self::eHigh444Predictive => RawStdVideoH264ProfileIdc::eHigh444Predictive,
            Self::eInvalid => RawStdVideoH264ProfileIdc::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH264ProfileIdc(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH264LevelIdc {
    e10,
    e11,
    e12,
    e13,
    e20,
    e21,
    e22,
    e30,
    e31,
    e32,
    e40,
    e41,
    e42,
    e50,
    e51,
    e52,
    e60,
    e61,
    e62,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH264LevelIdc {
    pub fn into_raw(self) -> RawStdVideoH264LevelIdc {
        match self {
            Self::e10 => RawStdVideoH264LevelIdc::e10,
            Self::e11 => RawStdVideoH264LevelIdc::e11,
            Self::e12 => RawStdVideoH264LevelIdc::e12,
            Self::e13 => RawStdVideoH264LevelIdc::e13,
            Self::e20 => RawStdVideoH264LevelIdc::e20,
            Self::e21 => RawStdVideoH264LevelIdc::e21,
            Self::e22 => RawStdVideoH264LevelIdc::e22,
            Self::e30 => RawStdVideoH264LevelIdc::e30,
            Self::e31 => RawStdVideoH264LevelIdc::e31,
            Self::e32 => RawStdVideoH264LevelIdc::e32,
            Self::e40 => RawStdVideoH264LevelIdc::e40,
            Self::e41 => RawStdVideoH264LevelIdc::e41,
            Self::e42 => RawStdVideoH264LevelIdc::e42,
            Self::e50 => RawStdVideoH264LevelIdc::e50,
            Self::e51 => RawStdVideoH264LevelIdc::e51,
            Self::e52 => RawStdVideoH264LevelIdc::e52,
            Self::e60 => RawStdVideoH264LevelIdc::e60,
            Self::e61 => RawStdVideoH264LevelIdc::e61,
            Self::e62 => RawStdVideoH264LevelIdc::e62,
            Self::eInvalid => RawStdVideoH264LevelIdc::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH264LevelIdc(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH264PocType {
    e0,
    e1,
    e2,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH264PocType {
    pub fn into_raw(self) -> RawStdVideoH264PocType {
        match self {
            Self::e0 => RawStdVideoH264PocType::e0,
            Self::e1 => RawStdVideoH264PocType::e1,
            Self::e2 => RawStdVideoH264PocType::e2,
            Self::eInvalid => RawStdVideoH264PocType::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH264PocType(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH264AspectRatioIdc {
    eUnspecified,
    eSquare,
    e1211,
    e1011,
    e1611,
    e4033,
    e2411,
    e2011,
    e3211,
    e8033,
    e1811,
    e1511,
    e6433,
    e16099,
    e43,
    e32,
    e21,
    eExtendedSar,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH264AspectRatioIdc {
    pub fn into_raw(self) -> RawStdVideoH264AspectRatioIdc {
        match self {
            Self::eUnspecified => RawStdVideoH264AspectRatioIdc::eUnspecified,
            Self::eSquare => RawStdVideoH264AspectRatioIdc::eSquare,
            Self::e1211 => RawStdVideoH264AspectRatioIdc::e1211,
            Self::e1011 => RawStdVideoH264AspectRatioIdc::e1011,
            Self::e1611 => RawStdVideoH264AspectRatioIdc::e1611,
            Self::e4033 => RawStdVideoH264AspectRatioIdc::e4033,
            Self::e2411 => RawStdVideoH264AspectRatioIdc::e2411,
            Self::e2011 => RawStdVideoH264AspectRatioIdc::e2011,
            Self::e3211 => RawStdVideoH264AspectRatioIdc::e3211,
            Self::e8033 => RawStdVideoH264AspectRatioIdc::e8033,
            Self::e1811 => RawStdVideoH264AspectRatioIdc::e1811,
            Self::e1511 => RawStdVideoH264AspectRatioIdc::e1511,
            Self::e6433 => RawStdVideoH264AspectRatioIdc::e6433,
            Self::e16099 => RawStdVideoH264AspectRatioIdc::e16099,
            Self::e43 => RawStdVideoH264AspectRatioIdc::e43,
            Self::e32 => RawStdVideoH264AspectRatioIdc::e32,
            Self::e21 => RawStdVideoH264AspectRatioIdc::e21,
            Self::eExtendedSar => RawStdVideoH264AspectRatioIdc::eExtendedSar,
            Self::eInvalid => RawStdVideoH264AspectRatioIdc::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH264AspectRatioIdc(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH264WeightedBipredIdc {
    eDefault,
    eExplicit,
    eImplicit,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH264WeightedBipredIdc {
    pub fn into_raw(self) -> RawStdVideoH264WeightedBipredIdc {
        match self {
            Self::eDefault => RawStdVideoH264WeightedBipredIdc::eDefault,
            Self::eExplicit => RawStdVideoH264WeightedBipredIdc::eExplicit,
            Self::eImplicit => RawStdVideoH264WeightedBipredIdc::eImplicit,
            Self::eInvalid => RawStdVideoH264WeightedBipredIdc::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH264WeightedBipredIdc(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH264ModificationOfPicNumsIdc {
    eShortTermSubtract,
    eShortTermAdd,
    eLongTerm,
    eEnd,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH264ModificationOfPicNumsIdc {
    pub fn into_raw(self) -> RawStdVideoH264ModificationOfPicNumsIdc {
        match self {
            Self::eShortTermSubtract => RawStdVideoH264ModificationOfPicNumsIdc::eShortTermSubtract,
            Self::eShortTermAdd => RawStdVideoH264ModificationOfPicNumsIdc::eShortTermAdd,
            Self::eLongTerm => RawStdVideoH264ModificationOfPicNumsIdc::eLongTerm,
            Self::eEnd => RawStdVideoH264ModificationOfPicNumsIdc::eEnd,
            Self::eInvalid => RawStdVideoH264ModificationOfPicNumsIdc::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH264ModificationOfPicNumsIdc(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH264MemMgmtControlOp {
    eEnd,
    eUnmarkShortTerm,
    eUnmarkLongTerm,
    eMarkLongTerm,
    eSetMaxLongTermIndex,
    eUnmarkAll,
    eMarkCurrentAsLongTerm,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH264MemMgmtControlOp {
    pub fn into_raw(self) -> RawStdVideoH264MemMgmtControlOp {
        match self {
            Self::eEnd => RawStdVideoH264MemMgmtControlOp::eEnd,
            Self::eUnmarkShortTerm => RawStdVideoH264MemMgmtControlOp::eUnmarkShortTerm,
            Self::eUnmarkLongTerm => RawStdVideoH264MemMgmtControlOp::eUnmarkLongTerm,
            Self::eMarkLongTerm => RawStdVideoH264MemMgmtControlOp::eMarkLongTerm,
            Self::eSetMaxLongTermIndex => RawStdVideoH264MemMgmtControlOp::eSetMaxLongTermIndex,
            Self::eUnmarkAll => RawStdVideoH264MemMgmtControlOp::eUnmarkAll,
            Self::eMarkCurrentAsLongTerm => RawStdVideoH264MemMgmtControlOp::eMarkCurrentAsLongTerm,
            Self::eInvalid => RawStdVideoH264MemMgmtControlOp::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH264MemMgmtControlOp(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH264CabacInitIdc {
    e0,
    e1,
    e2,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH264CabacInitIdc {
    pub fn into_raw(self) -> RawStdVideoH264CabacInitIdc {
        match self {
            Self::e0 => RawStdVideoH264CabacInitIdc::e0,
            Self::e1 => RawStdVideoH264CabacInitIdc::e1,
            Self::e2 => RawStdVideoH264CabacInitIdc::e2,
            Self::eInvalid => RawStdVideoH264CabacInitIdc::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH264CabacInitIdc(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH264DisableDeblockingFilterIdc {
    eDisabled,
    eEnabled,
    ePartial,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH264DisableDeblockingFilterIdc {
    pub fn into_raw(self) -> RawStdVideoH264DisableDeblockingFilterIdc {
        match self {
            Self::eDisabled => RawStdVideoH264DisableDeblockingFilterIdc::eDisabled,
            Self::eEnabled => RawStdVideoH264DisableDeblockingFilterIdc::eEnabled,
            Self::ePartial => RawStdVideoH264DisableDeblockingFilterIdc::ePartial,
            Self::eInvalid => RawStdVideoH264DisableDeblockingFilterIdc::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH264DisableDeblockingFilterIdc(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH264SliceType {
    eP,
    eB,
    eI,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH264SliceType {
    pub fn into_raw(self) -> RawStdVideoH264SliceType {
        match self {
            Self::eP => RawStdVideoH264SliceType::eP,
            Self::eB => RawStdVideoH264SliceType::eB,
            Self::eI => RawStdVideoH264SliceType::eI,
            Self::eInvalid => RawStdVideoH264SliceType::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH264SliceType(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH264PictureType {
    eP,
    eB,
    eI,
    eIdr,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH264PictureType {
    pub fn into_raw(self) -> RawStdVideoH264PictureType {
        match self {
            Self::eP => RawStdVideoH264PictureType::eP,
            Self::eB => RawStdVideoH264PictureType::eB,
            Self::eI => RawStdVideoH264PictureType::eI,
            Self::eIdr => RawStdVideoH264PictureType::eIdr,
            Self::eInvalid => RawStdVideoH264PictureType::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH264PictureType(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH264NonVclNaluType {
    eSps,
    ePps,
    eAud,
    ePrefix,
    eEndOfSequence,
    eEndOfStream,
    ePrecoded,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH264NonVclNaluType {
    pub fn into_raw(self) -> RawStdVideoH264NonVclNaluType {
        match self {
            Self::eSps => RawStdVideoH264NonVclNaluType::eSps,
            Self::ePps => RawStdVideoH264NonVclNaluType::ePps,
            Self::eAud => RawStdVideoH264NonVclNaluType::eAud,
            Self::ePrefix => RawStdVideoH264NonVclNaluType::ePrefix,
            Self::eEndOfSequence => RawStdVideoH264NonVclNaluType::eEndOfSequence,
            Self::eEndOfStream => RawStdVideoH264NonVclNaluType::eEndOfStream,
            Self::ePrecoded => RawStdVideoH264NonVclNaluType::ePrecoded,
            Self::eInvalid => RawStdVideoH264NonVclNaluType::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH264NonVclNaluType(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoDecodeH264FieldOrderCount {
    eTop,
    eBottom,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoDecodeH264FieldOrderCount {
    pub fn into_raw(self) -> RawStdVideoDecodeH264FieldOrderCount {
        match self {
            Self::eTop => RawStdVideoDecodeH264FieldOrderCount::eTop,
            Self::eBottom => RawStdVideoDecodeH264FieldOrderCount::eBottom,
            Self::eInvalid => RawStdVideoDecodeH264FieldOrderCount::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoDecodeH264FieldOrderCount(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH265ChromaFormatIdc {
    eMonochrome,
    e420,
    e422,
    e444,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH265ChromaFormatIdc {
    pub fn into_raw(self) -> RawStdVideoH265ChromaFormatIdc {
        match self {
            Self::eMonochrome => RawStdVideoH265ChromaFormatIdc::eMonochrome,
            Self::e420 => RawStdVideoH265ChromaFormatIdc::e420,
            Self::e422 => RawStdVideoH265ChromaFormatIdc::e422,
            Self::e444 => RawStdVideoH265ChromaFormatIdc::e444,
            Self::eInvalid => RawStdVideoH265ChromaFormatIdc::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH265ChromaFormatIdc(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH265ProfileIdc {
    eMain,
    eMain10,
    eMainStillPicture,
    eFormatRangeExtensions,
    eSccExtensions,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH265ProfileIdc {
    pub fn into_raw(self) -> RawStdVideoH265ProfileIdc {
        match self {
            Self::eMain => RawStdVideoH265ProfileIdc::eMain,
            Self::eMain10 => RawStdVideoH265ProfileIdc::eMain10,
            Self::eMainStillPicture => RawStdVideoH265ProfileIdc::eMainStillPicture,
            Self::eFormatRangeExtensions => RawStdVideoH265ProfileIdc::eFormatRangeExtensions,
            Self::eSccExtensions => RawStdVideoH265ProfileIdc::eSccExtensions,
            Self::eInvalid => RawStdVideoH265ProfileIdc::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH265ProfileIdc(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH265LevelIdc {
    e10,
    e20,
    e21,
    e30,
    e31,
    e40,
    e41,
    e50,
    e51,
    e52,
    e60,
    e61,
    e62,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH265LevelIdc {
    pub fn into_raw(self) -> RawStdVideoH265LevelIdc {
        match self {
            Self::e10 => RawStdVideoH265LevelIdc::e10,
            Self::e20 => RawStdVideoH265LevelIdc::e20,
            Self::e21 => RawStdVideoH265LevelIdc::e21,
            Self::e30 => RawStdVideoH265LevelIdc::e30,
            Self::e31 => RawStdVideoH265LevelIdc::e31,
            Self::e40 => RawStdVideoH265LevelIdc::e40,
            Self::e41 => RawStdVideoH265LevelIdc::e41,
            Self::e50 => RawStdVideoH265LevelIdc::e50,
            Self::e51 => RawStdVideoH265LevelIdc::e51,
            Self::e52 => RawStdVideoH265LevelIdc::e52,
            Self::e60 => RawStdVideoH265LevelIdc::e60,
            Self::e61 => RawStdVideoH265LevelIdc::e61,
            Self::e62 => RawStdVideoH265LevelIdc::e62,
            Self::eInvalid => RawStdVideoH265LevelIdc::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH265LevelIdc(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH265SliceType {
    eB,
    eP,
    eI,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH265SliceType {
    pub fn into_raw(self) -> RawStdVideoH265SliceType {
        match self {
            Self::eB => RawStdVideoH265SliceType::eB,
            Self::eP => RawStdVideoH265SliceType::eP,
            Self::eI => RawStdVideoH265SliceType::eI,
            Self::eInvalid => RawStdVideoH265SliceType::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH265SliceType(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH265PictureType {
    eP,
    eB,
    eI,
    eIdr,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH265PictureType {
    pub fn into_raw(self) -> RawStdVideoH265PictureType {
        match self {
            Self::eP => RawStdVideoH265PictureType::eP,
            Self::eB => RawStdVideoH265PictureType::eB,
            Self::eI => RawStdVideoH265PictureType::eI,
            Self::eIdr => RawStdVideoH265PictureType::eIdr,
            Self::eInvalid => RawStdVideoH265PictureType::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH265PictureType(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StdVideoH265AspectRatioIdc {
    eUnspecified,
    eSquare,
    e1211,
    e1011,
    e1611,
    e4033,
    e2411,
    e2011,
    e3211,
    e8033,
    e1811,
    e1511,
    e6433,
    e16099,
    e43,
    e32,
    e21,
    eExtendedSar,
    eInvalid,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl StdVideoH265AspectRatioIdc {
    pub fn into_raw(self) -> RawStdVideoH265AspectRatioIdc {
        match self {
            Self::eUnspecified => RawStdVideoH265AspectRatioIdc::eUnspecified,
            Self::eSquare => RawStdVideoH265AspectRatioIdc::eSquare,
            Self::e1211 => RawStdVideoH265AspectRatioIdc::e1211,
            Self::e1011 => RawStdVideoH265AspectRatioIdc::e1011,
            Self::e1611 => RawStdVideoH265AspectRatioIdc::e1611,
            Self::e4033 => RawStdVideoH265AspectRatioIdc::e4033,
            Self::e2411 => RawStdVideoH265AspectRatioIdc::e2411,
            Self::e2011 => RawStdVideoH265AspectRatioIdc::e2011,
            Self::e3211 => RawStdVideoH265AspectRatioIdc::e3211,
            Self::e8033 => RawStdVideoH265AspectRatioIdc::e8033,
            Self::e1811 => RawStdVideoH265AspectRatioIdc::e1811,
            Self::e1511 => RawStdVideoH265AspectRatioIdc::e1511,
            Self::e6433 => RawStdVideoH265AspectRatioIdc::e6433,
            Self::e16099 => RawStdVideoH265AspectRatioIdc::e16099,
            Self::e43 => RawStdVideoH265AspectRatioIdc::e43,
            Self::e32 => RawStdVideoH265AspectRatioIdc::e32,
            Self::e21 => RawStdVideoH265AspectRatioIdc::e21,
            Self::eExtendedSar => RawStdVideoH265AspectRatioIdc::eExtendedSar,
            Self::eInvalid => RawStdVideoH265AspectRatioIdc::eInvalid,
            Self::eUnknownVariant(v) => RawStdVideoH265AspectRatioIdc(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SuccessCode {
    eSuccess,
    eNotReady,
    eTimeout,
    eEventSet,
    eEventReset,
    eIncomplete,
    eSuboptimalKhr,
    eThreadIdleKhr,
    eThreadDoneKhr,
    eOperationDeferredKhr,
    eOperationNotDeferredKhr,
    ePipelineCompileRequired,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl SuccessCode {
    pub fn into_raw(self) -> RawSuccessCode {
        match self {
            Self::eSuccess => RawSuccessCode::eSuccess,
            Self::eNotReady => RawSuccessCode::eNotReady,
            Self::eTimeout => RawSuccessCode::eTimeout,
            Self::eEventSet => RawSuccessCode::eEventSet,
            Self::eEventReset => RawSuccessCode::eEventReset,
            Self::eIncomplete => RawSuccessCode::eIncomplete,
            Self::eSuboptimalKhr => RawSuccessCode::eSuboptimalKhr,
            Self::eThreadIdleKhr => RawSuccessCode::eThreadIdleKhr,
            Self::eThreadDoneKhr => RawSuccessCode::eThreadDoneKhr,
            Self::eOperationDeferredKhr => RawSuccessCode::eOperationDeferredKhr,
            Self::eOperationNotDeferredKhr => RawSuccessCode::eOperationNotDeferredKhr,
            Self::ePipelineCompileRequired => RawSuccessCode::ePipelineCompileRequired,
            Self::eUnknownVariant(v) => RawSuccessCode(v)
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ErrorCode {
    eErrorCompressionExhaustedExt,
    eErrorNoPipelineMatch,
    eErrorInvalidPipelineCacheData,
    eErrorInvalidOpaqueCaptureAddress,
    eErrorFullScreenExclusiveModeLostExt,
    eErrorNotPermittedKhr,
    eErrorFragmentation,
    eErrorInvalidDrmFormatModifierPlaneLayoutExt,
    eErrorInvalidExternalHandle,
    eErrorOutOfPoolMemory,
    eErrorVideoStdVersionNotSupportedKhr,
    eErrorVideoProfileCodecNotSupportedKhr,
    eErrorVideoProfileFormatNotSupportedKhr,
    eErrorVideoProfileOperationNotSupportedKhr,
    eErrorVideoPictureLayoutNotSupportedKhr,
    eErrorImageUsageNotSupportedKhr,
    eErrorInvalidShaderNv,
    eErrorValidationFailedExt,
    eErrorIncompatibleDisplayKhr,
    eErrorOutOfDateKhr,
    eErrorNativeWindowInUseKhr,
    eErrorSurfaceLostKhr,
    eErrorUnknown,
    eErrorFragmentedPool,
    eErrorFormatNotSupported,
    eErrorTooManyObjects,
    eErrorIncompatibleDriver,
    eErrorFeatureNotPresent,
    eErrorExtensionNotPresent,
    eErrorLayerNotPresent,
    eErrorMemoryMapFailed,
    eErrorDeviceLost,
    eErrorInitializationFailed,
    eErrorOutOfDeviceMemory,
    eErrorOutOfHostMemory,
    eUnknownVariant(i32)
}

#[allow(non_upper_case_globals)]
impl ErrorCode {
    pub fn into_raw(self) -> RawErrorCode {
        match self {
            Self::eErrorCompressionExhaustedExt => RawErrorCode::eErrorCompressionExhaustedExt,
            Self::eErrorNoPipelineMatch => RawErrorCode::eErrorNoPipelineMatch,
            Self::eErrorInvalidPipelineCacheData => RawErrorCode::eErrorInvalidPipelineCacheData,
            Self::eErrorInvalidOpaqueCaptureAddress => RawErrorCode::eErrorInvalidOpaqueCaptureAddress,
            Self::eErrorFullScreenExclusiveModeLostExt => RawErrorCode::eErrorFullScreenExclusiveModeLostExt,
            Self::eErrorNotPermittedKhr => RawErrorCode::eErrorNotPermittedKhr,
            Self::eErrorFragmentation => RawErrorCode::eErrorFragmentation,
            Self::eErrorInvalidDrmFormatModifierPlaneLayoutExt => RawErrorCode::eErrorInvalidDrmFormatModifierPlaneLayoutExt,
            Self::eErrorInvalidExternalHandle => RawErrorCode::eErrorInvalidExternalHandle,
            Self::eErrorOutOfPoolMemory => RawErrorCode::eErrorOutOfPoolMemory,
            Self::eErrorVideoStdVersionNotSupportedKhr => RawErrorCode::eErrorVideoStdVersionNotSupportedKhr,
            Self::eErrorVideoProfileCodecNotSupportedKhr => RawErrorCode::eErrorVideoProfileCodecNotSupportedKhr,
            Self::eErrorVideoProfileFormatNotSupportedKhr => RawErrorCode::eErrorVideoProfileFormatNotSupportedKhr,
            Self::eErrorVideoProfileOperationNotSupportedKhr => RawErrorCode::eErrorVideoProfileOperationNotSupportedKhr,
            Self::eErrorVideoPictureLayoutNotSupportedKhr => RawErrorCode::eErrorVideoPictureLayoutNotSupportedKhr,
            Self::eErrorImageUsageNotSupportedKhr => RawErrorCode::eErrorImageUsageNotSupportedKhr,
            Self::eErrorInvalidShaderNv => RawErrorCode::eErrorInvalidShaderNv,
            Self::eErrorValidationFailedExt => RawErrorCode::eErrorValidationFailedExt,
            Self::eErrorIncompatibleDisplayKhr => RawErrorCode::eErrorIncompatibleDisplayKhr,
            Self::eErrorOutOfDateKhr => RawErrorCode::eErrorOutOfDateKhr,
            Self::eErrorNativeWindowInUseKhr => RawErrorCode::eErrorNativeWindowInUseKhr,
            Self::eErrorSurfaceLostKhr => RawErrorCode::eErrorSurfaceLostKhr,
            Self::eErrorUnknown => RawErrorCode::eErrorUnknown,
            Self::eErrorFragmentedPool => RawErrorCode::eErrorFragmentedPool,
            Self::eErrorFormatNotSupported => RawErrorCode::eErrorFormatNotSupported,
            Self::eErrorTooManyObjects => RawErrorCode::eErrorTooManyObjects,
            Self::eErrorIncompatibleDriver => RawErrorCode::eErrorIncompatibleDriver,
            Self::eErrorFeatureNotPresent => RawErrorCode::eErrorFeatureNotPresent,
            Self::eErrorExtensionNotPresent => RawErrorCode::eErrorExtensionNotPresent,
            Self::eErrorLayerNotPresent => RawErrorCode::eErrorLayerNotPresent,
            Self::eErrorMemoryMapFailed => RawErrorCode::eErrorMemoryMapFailed,
            Self::eErrorDeviceLost => RawErrorCode::eErrorDeviceLost,
            Self::eErrorInitializationFailed => RawErrorCode::eErrorInitializationFailed,
            Self::eErrorOutOfDeviceMemory => RawErrorCode::eErrorOutOfDeviceMemory,
            Self::eErrorOutOfHostMemory => RawErrorCode::eErrorOutOfHostMemory,
            Self::eUnknownVariant(v) => RawErrorCode(v)
        }
    }
}


pub(crate) use raw_enums::*;
pub mod raw_enums {
    use super::*;
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawImageLayout(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawImageLayout {
        pub const eUndefined: Self = RawImageLayout(0);
        pub const eGeneral: Self = RawImageLayout(1);
        pub const eColourAttachmentOptimal: Self = RawImageLayout(2);
        pub const eDepthStencilAttachmentOptimal: Self = RawImageLayout(3);
        pub const eDepthStencilReadOnlyOptimal: Self = RawImageLayout(4);
        pub const eShaderReadOnlyOptimal: Self = RawImageLayout(5);
        pub const eTransferSrcOptimal: Self = RawImageLayout(6);
        pub const eTransferDstOptimal: Self = RawImageLayout(7);
        pub const ePreinitialized: Self = RawImageLayout(8);
        pub const ePresentSrcKhr: Self = RawImageLayout(1000001002);
        pub const eVideoDecodeDstKhr: Self = RawImageLayout(1000024000);
        pub const eVideoDecodeSrcKhr: Self = RawImageLayout(1000024001);
        pub const eVideoDecodeDpbKhr: Self = RawImageLayout(1000024002);
        pub const eSharedPresentKhr: Self = RawImageLayout(1000111000);
        pub const eDepthReadOnlyStencilAttachmentOptimal: Self = RawImageLayout(1000117000);
        pub const eDepthAttachmentStencilReadOnlyOptimal: Self = RawImageLayout(1000117001);
        pub const eFragmentShadingRateAttachmentOptimalKhr: Self = RawImageLayout(1000164003);
        pub const eFragmentDensityMapOptimalExt: Self = RawImageLayout(1000218000);
        pub const eDepthAttachmentOptimal: Self = RawImageLayout(1000241000);
        pub const eDepthReadOnlyOptimal: Self = RawImageLayout(1000241001);
        pub const eStencilAttachmentOptimal: Self = RawImageLayout(1000241002);
        pub const eStencilReadOnlyOptimal: Self = RawImageLayout(1000241003);
        pub const eVideoEncodeDstKhr: Self = RawImageLayout(1000299000);
        pub const eVideoEncodeSrcKhr: Self = RawImageLayout(1000299001);
        pub const eVideoEncodeDpbKhr: Self = RawImageLayout(1000299002);
        pub const eReadOnlyOptimal: Self = RawImageLayout(1000314000);
        pub const eAttachmentOptimal: Self = RawImageLayout(1000314001);
        pub const eAttachmentFeedbackLoopOptimalExt: Self = RawImageLayout(1000339000);
        pub const eAttachmentOptimalKhr: Self = Self::eAttachmentOptimal;
        pub const eDepthAttachmentOptimalKhr: Self = Self::eDepthAttachmentOptimal;
        pub const eDepthAttachmentStencilReadOnlyOptimalKhr: Self = Self::eDepthAttachmentStencilReadOnlyOptimal;
        pub const eDepthReadOnlyOptimalKhr: Self = Self::eDepthReadOnlyOptimal;
        pub const eDepthReadOnlyStencilAttachmentOptimalKhr: Self = Self::eDepthReadOnlyStencilAttachmentOptimal;
        pub const eShadingRateOptimalNv: Self = Self::eFragmentShadingRateAttachmentOptimalKhr;
        pub const eReadOnlyOptimalKhr: Self = Self::eReadOnlyOptimal;
        pub const eStencilAttachmentOptimalKhr: Self = Self::eStencilAttachmentOptimal;
        pub const eStencilReadOnlyOptimalKhr: Self = Self::eStencilReadOnlyOptimal;

        pub fn normalise(self) -> ImageLayout {
            match self {
                Self::eUndefined => ImageLayout::eUndefined,
                Self::eGeneral => ImageLayout::eGeneral,
                Self::eColourAttachmentOptimal => ImageLayout::eColourAttachmentOptimal,
                Self::eDepthStencilAttachmentOptimal => ImageLayout::eDepthStencilAttachmentOptimal,
                Self::eDepthStencilReadOnlyOptimal => ImageLayout::eDepthStencilReadOnlyOptimal,
                Self::eShaderReadOnlyOptimal => ImageLayout::eShaderReadOnlyOptimal,
                Self::eTransferSrcOptimal => ImageLayout::eTransferSrcOptimal,
                Self::eTransferDstOptimal => ImageLayout::eTransferDstOptimal,
                Self::ePreinitialized => ImageLayout::ePreinitialized,
                Self::ePresentSrcKhr => ImageLayout::ePresentSrcKhr,
                Self::eVideoDecodeDstKhr => ImageLayout::eVideoDecodeDstKhr,
                Self::eVideoDecodeSrcKhr => ImageLayout::eVideoDecodeSrcKhr,
                Self::eVideoDecodeDpbKhr => ImageLayout::eVideoDecodeDpbKhr,
                Self::eSharedPresentKhr => ImageLayout::eSharedPresentKhr,
                Self::eDepthReadOnlyStencilAttachmentOptimal => ImageLayout::eDepthReadOnlyStencilAttachmentOptimal,
                Self::eDepthAttachmentStencilReadOnlyOptimal => ImageLayout::eDepthAttachmentStencilReadOnlyOptimal,
                Self::eFragmentShadingRateAttachmentOptimalKhr => ImageLayout::eFragmentShadingRateAttachmentOptimalKhr,
                Self::eFragmentDensityMapOptimalExt => ImageLayout::eFragmentDensityMapOptimalExt,
                Self::eDepthAttachmentOptimal => ImageLayout::eDepthAttachmentOptimal,
                Self::eDepthReadOnlyOptimal => ImageLayout::eDepthReadOnlyOptimal,
                Self::eStencilAttachmentOptimal => ImageLayout::eStencilAttachmentOptimal,
                Self::eStencilReadOnlyOptimal => ImageLayout::eStencilReadOnlyOptimal,
                Self::eVideoEncodeDstKhr => ImageLayout::eVideoEncodeDstKhr,
                Self::eVideoEncodeSrcKhr => ImageLayout::eVideoEncodeSrcKhr,
                Self::eVideoEncodeDpbKhr => ImageLayout::eVideoEncodeDpbKhr,
                Self::eReadOnlyOptimal => ImageLayout::eReadOnlyOptimal,
                Self::eAttachmentOptimal => ImageLayout::eAttachmentOptimal,
                Self::eAttachmentFeedbackLoopOptimalExt => ImageLayout::eAttachmentFeedbackLoopOptimalExt,
                v => ImageLayout::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawImageLayout {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawAttachmentLoadOp(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawAttachmentLoadOp {
        pub const eLoad: Self = RawAttachmentLoadOp(0);
        pub const eClear: Self = RawAttachmentLoadOp(1);
        pub const eDontCare: Self = RawAttachmentLoadOp(2);
        pub const eNoneExt: Self = RawAttachmentLoadOp(1000400000);

        pub fn normalise(self) -> AttachmentLoadOp {
            match self {
                Self::eLoad => AttachmentLoadOp::eLoad,
                Self::eClear => AttachmentLoadOp::eClear,
                Self::eDontCare => AttachmentLoadOp::eDontCare,
                Self::eNoneExt => AttachmentLoadOp::eNoneExt,
                v => AttachmentLoadOp::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawAttachmentLoadOp {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawAttachmentStoreOp(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawAttachmentStoreOp {
        pub const eStore: Self = RawAttachmentStoreOp(0);
        pub const eDontCare: Self = RawAttachmentStoreOp(1);
        pub const eNone: Self = RawAttachmentStoreOp(1000301000);
        pub const eNoneExt: Self = Self::eNone;
        pub const eNoneKhr: Self = Self::eNone;
        pub const eNoneQcom: Self = Self::eNone;

        pub fn normalise(self) -> AttachmentStoreOp {
            match self {
                Self::eStore => AttachmentStoreOp::eStore,
                Self::eDontCare => AttachmentStoreOp::eDontCare,
                Self::eNone => AttachmentStoreOp::eNone,
                v => AttachmentStoreOp::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawAttachmentStoreOp {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawImageType(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawImageType {
        pub const e1d: Self = RawImageType(0);
        pub const e2d: Self = RawImageType(1);
        pub const e3d: Self = RawImageType(2);

        pub fn normalise(self) -> ImageType {
            match self {
                Self::e1d => ImageType::e1d,
                Self::e2d => ImageType::e2d,
                Self::e3d => ImageType::e3d,
                v => ImageType::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawImageType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawImageTiling(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawImageTiling {
        pub const eOptimal: Self = RawImageTiling(0);
        pub const eLinear: Self = RawImageTiling(1);
        pub const eDrmFormatModifierExt: Self = RawImageTiling(1000158000);

        pub fn normalise(self) -> ImageTiling {
            match self {
                Self::eOptimal => ImageTiling::eOptimal,
                Self::eLinear => ImageTiling::eLinear,
                Self::eDrmFormatModifierExt => ImageTiling::eDrmFormatModifierExt,
                v => ImageTiling::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawImageTiling {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawImageViewType(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawImageViewType {
        pub const e1d: Self = RawImageViewType(0);
        pub const e2d: Self = RawImageViewType(1);
        pub const e3d: Self = RawImageViewType(2);
        pub const eCube: Self = RawImageViewType(3);
        pub const e1dArray: Self = RawImageViewType(4);
        pub const e2dArray: Self = RawImageViewType(5);
        pub const eCubeArray: Self = RawImageViewType(6);

        pub fn normalise(self) -> ImageViewType {
            match self {
                Self::e1d => ImageViewType::e1d,
                Self::e2d => ImageViewType::e2d,
                Self::e3d => ImageViewType::e3d,
                Self::eCube => ImageViewType::eCube,
                Self::e1dArray => ImageViewType::e1dArray,
                Self::e2dArray => ImageViewType::e2dArray,
                Self::eCubeArray => ImageViewType::eCubeArray,
                v => ImageViewType::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawImageViewType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawCommandBufferLevel(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawCommandBufferLevel {
        pub const ePrimary: Self = RawCommandBufferLevel(0);
        pub const eSecondary: Self = RawCommandBufferLevel(1);

        pub fn normalise(self) -> CommandBufferLevel {
            match self {
                Self::ePrimary => CommandBufferLevel::ePrimary,
                Self::eSecondary => CommandBufferLevel::eSecondary,
                v => CommandBufferLevel::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawCommandBufferLevel {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawComponentSwizzle(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawComponentSwizzle {
        pub const eIdentity: Self = RawComponentSwizzle(0);
        pub const eZero: Self = RawComponentSwizzle(1);
        pub const eOne: Self = RawComponentSwizzle(2);
        pub const eR: Self = RawComponentSwizzle(3);
        pub const eG: Self = RawComponentSwizzle(4);
        pub const eB: Self = RawComponentSwizzle(5);
        pub const eA: Self = RawComponentSwizzle(6);

        pub fn normalise(self) -> ComponentSwizzle {
            match self {
                Self::eIdentity => ComponentSwizzle::eIdentity,
                Self::eZero => ComponentSwizzle::eZero,
                Self::eOne => ComponentSwizzle::eOne,
                Self::eR => ComponentSwizzle::eR,
                Self::eG => ComponentSwizzle::eG,
                Self::eB => ComponentSwizzle::eB,
                Self::eA => ComponentSwizzle::eA,
                v => ComponentSwizzle::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawComponentSwizzle {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawDescriptorType(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawDescriptorType {
        pub const eSampler: Self = RawDescriptorType(0);
        pub const eCombinedImageSampler: Self = RawDescriptorType(1);
        pub const eSampledImage: Self = RawDescriptorType(2);
        pub const eStorageImage: Self = RawDescriptorType(3);
        pub const eUniformTexelBuffer: Self = RawDescriptorType(4);
        pub const eStorageTexelBuffer: Self = RawDescriptorType(5);
        pub const eUniformBuffer: Self = RawDescriptorType(6);
        pub const eStorageBuffer: Self = RawDescriptorType(7);
        pub const eUniformBufferDynamic: Self = RawDescriptorType(8);
        pub const eStorageBufferDynamic: Self = RawDescriptorType(9);
        pub const eInputAttachment: Self = RawDescriptorType(10);
        pub const eInlineUniformBlock: Self = RawDescriptorType(1000138000);
        pub const eAccelerationStructureKhr: Self = RawDescriptorType(1000150000);
        pub const eAccelerationStructureNv: Self = RawDescriptorType(1000165000);
        pub const eMutableExt: Self = RawDescriptorType(1000351000);
        pub const eSampleWeightImageQcom: Self = RawDescriptorType(1000440000);
        pub const eBlockMatchImageQcom: Self = RawDescriptorType(1000440001);
        pub const eInlineUniformBlockExt: Self = Self::eInlineUniformBlock;
        pub const eMutableValve: Self = Self::eMutableExt;

        pub fn normalise(self) -> DescriptorType {
            match self {
                Self::eSampler => DescriptorType::eSampler,
                Self::eCombinedImageSampler => DescriptorType::eCombinedImageSampler,
                Self::eSampledImage => DescriptorType::eSampledImage,
                Self::eStorageImage => DescriptorType::eStorageImage,
                Self::eUniformTexelBuffer => DescriptorType::eUniformTexelBuffer,
                Self::eStorageTexelBuffer => DescriptorType::eStorageTexelBuffer,
                Self::eUniformBuffer => DescriptorType::eUniformBuffer,
                Self::eStorageBuffer => DescriptorType::eStorageBuffer,
                Self::eUniformBufferDynamic => DescriptorType::eUniformBufferDynamic,
                Self::eStorageBufferDynamic => DescriptorType::eStorageBufferDynamic,
                Self::eInputAttachment => DescriptorType::eInputAttachment,
                Self::eInlineUniformBlock => DescriptorType::eInlineUniformBlock,
                Self::eAccelerationStructureKhr => DescriptorType::eAccelerationStructureKhr,
                Self::eAccelerationStructureNv => DescriptorType::eAccelerationStructureNv,
                Self::eMutableExt => DescriptorType::eMutableExt,
                Self::eSampleWeightImageQcom => DescriptorType::eSampleWeightImageQcom,
                Self::eBlockMatchImageQcom => DescriptorType::eBlockMatchImageQcom,
                v => DescriptorType::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawDescriptorType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawQueryType(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawQueryType {
        pub const eOcclusion: Self = RawQueryType(0);
        pub const ePipelineStatistics: Self = RawQueryType(1);
        pub const eTimestamp: Self = RawQueryType(2);
        pub const eResultStatusOnlyKhr: Self = RawQueryType(1000023000);
        pub const eTransformFeedbackStreamExt: Self = RawQueryType(1000028004);
        pub const ePerformanceQueryKhr: Self = RawQueryType(1000116000);
        pub const eAccelerationStructureCompactedSizeKhr: Self = RawQueryType(1000150000);
        pub const eAccelerationStructureSerializationSizeKhr: Self = RawQueryType(1000150001);
        pub const eAccelerationStructureCompactedSizeNv: Self = RawQueryType(1000165000);
        pub const ePerformanceQueryIntel: Self = RawQueryType(1000210000);
        pub const eVideoEncodeBitstreamBufferRangeKhr: Self = RawQueryType(1000299000);
        pub const eMeshPrimitivesGeneratedExt: Self = RawQueryType(1000328000);
        pub const ePrimitivesGeneratedExt: Self = RawQueryType(1000382000);
        pub const eAccelerationStructureSerializationBottomLevelPointersKhr: Self = RawQueryType(1000386000);
        pub const eAccelerationStructureSizeKhr: Self = RawQueryType(1000386001);
        pub const eMicromapSerializationSizeExt: Self = RawQueryType(1000396000);
        pub const eMicromapCompactedSizeExt: Self = RawQueryType(1000396001);

        pub fn normalise(self) -> QueryType {
            match self {
                Self::eOcclusion => QueryType::eOcclusion,
                Self::ePipelineStatistics => QueryType::ePipelineStatistics,
                Self::eTimestamp => QueryType::eTimestamp,
                Self::eResultStatusOnlyKhr => QueryType::eResultStatusOnlyKhr,
                Self::eTransformFeedbackStreamExt => QueryType::eTransformFeedbackStreamExt,
                Self::ePerformanceQueryKhr => QueryType::ePerformanceQueryKhr,
                Self::eAccelerationStructureCompactedSizeKhr => QueryType::eAccelerationStructureCompactedSizeKhr,
                Self::eAccelerationStructureSerializationSizeKhr => QueryType::eAccelerationStructureSerializationSizeKhr,
                Self::eAccelerationStructureCompactedSizeNv => QueryType::eAccelerationStructureCompactedSizeNv,
                Self::ePerformanceQueryIntel => QueryType::ePerformanceQueryIntel,
                Self::eVideoEncodeBitstreamBufferRangeKhr => QueryType::eVideoEncodeBitstreamBufferRangeKhr,
                Self::eMeshPrimitivesGeneratedExt => QueryType::eMeshPrimitivesGeneratedExt,
                Self::ePrimitivesGeneratedExt => QueryType::ePrimitivesGeneratedExt,
                Self::eAccelerationStructureSerializationBottomLevelPointersKhr => QueryType::eAccelerationStructureSerializationBottomLevelPointersKhr,
                Self::eAccelerationStructureSizeKhr => QueryType::eAccelerationStructureSizeKhr,
                Self::eMicromapSerializationSizeExt => QueryType::eMicromapSerializationSizeExt,
                Self::eMicromapCompactedSizeExt => QueryType::eMicromapCompactedSizeExt,
                v => QueryType::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawQueryType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawBorderColour(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawBorderColour {
        pub const eFloatTransparentBlack: Self = RawBorderColour(0);
        pub const eIntTransparentBlack: Self = RawBorderColour(1);
        pub const eFloatOpaqueBlack: Self = RawBorderColour(2);
        pub const eIntOpaqueBlack: Self = RawBorderColour(3);
        pub const eFloatOpaqueWhite: Self = RawBorderColour(4);
        pub const eIntOpaqueWhite: Self = RawBorderColour(5);
        pub const eFloatCustomExt: Self = RawBorderColour(1000287003);
        pub const eIntCustomExt: Self = RawBorderColour(1000287004);

        pub fn normalise(self) -> BorderColour {
            match self {
                Self::eFloatTransparentBlack => BorderColour::eFloatTransparentBlack,
                Self::eIntTransparentBlack => BorderColour::eIntTransparentBlack,
                Self::eFloatOpaqueBlack => BorderColour::eFloatOpaqueBlack,
                Self::eIntOpaqueBlack => BorderColour::eIntOpaqueBlack,
                Self::eFloatOpaqueWhite => BorderColour::eFloatOpaqueWhite,
                Self::eIntOpaqueWhite => BorderColour::eIntOpaqueWhite,
                Self::eFloatCustomExt => BorderColour::eFloatCustomExt,
                Self::eIntCustomExt => BorderColour::eIntCustomExt,
                v => BorderColour::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawBorderColour {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawPipelineBindPoint(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawPipelineBindPoint {
        pub const eGraphics: Self = RawPipelineBindPoint(0);
        pub const eCompute: Self = RawPipelineBindPoint(1);
        pub const eRayTracingKhr: Self = RawPipelineBindPoint(1000165000);
        pub const eSubpassShadingHuawei: Self = RawPipelineBindPoint(1000369003);
        pub const eRayTracingNv: Self = Self::eRayTracingKhr;

        pub fn normalise(self) -> PipelineBindPoint {
            match self {
                Self::eGraphics => PipelineBindPoint::eGraphics,
                Self::eCompute => PipelineBindPoint::eCompute,
                Self::eRayTracingKhr => PipelineBindPoint::eRayTracingKhr,
                Self::eSubpassShadingHuawei => PipelineBindPoint::eSubpassShadingHuawei,
                v => PipelineBindPoint::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawPipelineBindPoint {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawPipelineCacheHeaderVersion(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawPipelineCacheHeaderVersion {
        pub const eOne: Self = RawPipelineCacheHeaderVersion(1);
        pub const eSafetyCriticalOne: Self = RawPipelineCacheHeaderVersion(1000298001);

        pub fn normalise(self) -> PipelineCacheHeaderVersion {
            match self {
                Self::eOne => PipelineCacheHeaderVersion::eOne,
                Self::eSafetyCriticalOne => PipelineCacheHeaderVersion::eSafetyCriticalOne,
                v => PipelineCacheHeaderVersion::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawPipelineCacheHeaderVersion {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawPrimitiveTopology(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawPrimitiveTopology {
        pub const ePointList: Self = RawPrimitiveTopology(0);
        pub const eLineList: Self = RawPrimitiveTopology(1);
        pub const eLineStrip: Self = RawPrimitiveTopology(2);
        pub const eTriangleList: Self = RawPrimitiveTopology(3);
        pub const eTriangleStrip: Self = RawPrimitiveTopology(4);
        pub const eTriangleFan: Self = RawPrimitiveTopology(5);
        pub const eLineListWithAdjacency: Self = RawPrimitiveTopology(6);
        pub const eLineStripWithAdjacency: Self = RawPrimitiveTopology(7);
        pub const eTriangleListWithAdjacency: Self = RawPrimitiveTopology(8);
        pub const eTriangleStripWithAdjacency: Self = RawPrimitiveTopology(9);
        pub const ePatchList: Self = RawPrimitiveTopology(10);

        pub fn normalise(self) -> PrimitiveTopology {
            match self {
                Self::ePointList => PrimitiveTopology::ePointList,
                Self::eLineList => PrimitiveTopology::eLineList,
                Self::eLineStrip => PrimitiveTopology::eLineStrip,
                Self::eTriangleList => PrimitiveTopology::eTriangleList,
                Self::eTriangleStrip => PrimitiveTopology::eTriangleStrip,
                Self::eTriangleFan => PrimitiveTopology::eTriangleFan,
                Self::eLineListWithAdjacency => PrimitiveTopology::eLineListWithAdjacency,
                Self::eLineStripWithAdjacency => PrimitiveTopology::eLineStripWithAdjacency,
                Self::eTriangleListWithAdjacency => PrimitiveTopology::eTriangleListWithAdjacency,
                Self::eTriangleStripWithAdjacency => PrimitiveTopology::eTriangleStripWithAdjacency,
                Self::ePatchList => PrimitiveTopology::ePatchList,
                v => PrimitiveTopology::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawPrimitiveTopology {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawSharingMode(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawSharingMode {
        pub const eExclusive: Self = RawSharingMode(0);
        pub const eConcurrent: Self = RawSharingMode(1);

        pub fn normalise(self) -> SharingMode {
            match self {
                Self::eExclusive => SharingMode::eExclusive,
                Self::eConcurrent => SharingMode::eConcurrent,
                v => SharingMode::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawSharingMode {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawIndexType(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawIndexType {
        pub const eUint16: Self = RawIndexType(0);
        pub const eUint32: Self = RawIndexType(1);
        pub const eNoneKhr: Self = RawIndexType(1000165000);
        pub const eUint8Ext: Self = RawIndexType(1000265000);
        pub const eNoneNv: Self = Self::eNoneKhr;

        pub fn normalise(self) -> IndexType {
            match self {
                Self::eUint16 => IndexType::eUint16,
                Self::eUint32 => IndexType::eUint32,
                Self::eNoneKhr => IndexType::eNoneKhr,
                Self::eUint8Ext => IndexType::eUint8Ext,
                v => IndexType::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawIndexType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawFilter(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawFilter {
        pub const eNearest: Self = RawFilter(0);
        pub const eLinear: Self = RawFilter(1);
        pub const eCubicExt: Self = RawFilter(1000015000);
        pub const eCubicImg: Self = Self::eCubicExt;

        pub fn normalise(self) -> Filter {
            match self {
                Self::eNearest => Filter::eNearest,
                Self::eLinear => Filter::eLinear,
                Self::eCubicExt => Filter::eCubicExt,
                v => Filter::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawFilter {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawSamplerMipmapMode(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawSamplerMipmapMode {
        pub const eNearest: Self = RawSamplerMipmapMode(0);
        pub const eLinear: Self = RawSamplerMipmapMode(1);

        pub fn normalise(self) -> SamplerMipmapMode {
            match self {
                Self::eNearest => SamplerMipmapMode::eNearest,
                Self::eLinear => SamplerMipmapMode::eLinear,
                v => SamplerMipmapMode::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawSamplerMipmapMode {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawSamplerAddressMode(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawSamplerAddressMode {
        pub const eRepeat: Self = RawSamplerAddressMode(0);
        pub const eMirroredRepeat: Self = RawSamplerAddressMode(1);
        pub const eClampToEdge: Self = RawSamplerAddressMode(2);
        pub const eClampToBorder: Self = RawSamplerAddressMode(3);
        pub const eMirrorClampToEdge: Self = RawSamplerAddressMode(4);
        pub const eMirrorClampToEdgeKhr: Self = Self::eMirrorClampToEdge;

        pub fn normalise(self) -> SamplerAddressMode {
            match self {
                Self::eRepeat => SamplerAddressMode::eRepeat,
                Self::eMirroredRepeat => SamplerAddressMode::eMirroredRepeat,
                Self::eClampToEdge => SamplerAddressMode::eClampToEdge,
                Self::eClampToBorder => SamplerAddressMode::eClampToBorder,
                Self::eMirrorClampToEdge => SamplerAddressMode::eMirrorClampToEdge,
                v => SamplerAddressMode::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawSamplerAddressMode {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawCompareOp(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawCompareOp {
        pub const eNever: Self = RawCompareOp(0);
        pub const eLess: Self = RawCompareOp(1);
        pub const eEqual: Self = RawCompareOp(2);
        pub const eLessOrEqual: Self = RawCompareOp(3);
        pub const eGreater: Self = RawCompareOp(4);
        pub const eNotEqual: Self = RawCompareOp(5);
        pub const eGreaterOrEqual: Self = RawCompareOp(6);
        pub const eAlways: Self = RawCompareOp(7);

        pub fn normalise(self) -> CompareOp {
            match self {
                Self::eNever => CompareOp::eNever,
                Self::eLess => CompareOp::eLess,
                Self::eEqual => CompareOp::eEqual,
                Self::eLessOrEqual => CompareOp::eLessOrEqual,
                Self::eGreater => CompareOp::eGreater,
                Self::eNotEqual => CompareOp::eNotEqual,
                Self::eGreaterOrEqual => CompareOp::eGreaterOrEqual,
                Self::eAlways => CompareOp::eAlways,
                v => CompareOp::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawCompareOp {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawPolygonMode(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawPolygonMode {
        pub const eFill: Self = RawPolygonMode(0);
        pub const eLine: Self = RawPolygonMode(1);
        pub const ePoint: Self = RawPolygonMode(2);
        pub const eFillRectangleNv: Self = RawPolygonMode(1000153000);

        pub fn normalise(self) -> PolygonMode {
            match self {
                Self::eFill => PolygonMode::eFill,
                Self::eLine => PolygonMode::eLine,
                Self::ePoint => PolygonMode::ePoint,
                Self::eFillRectangleNv => PolygonMode::eFillRectangleNv,
                v => PolygonMode::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawPolygonMode {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawFrontFace(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawFrontFace {
        pub const eCounterClockwise: Self = RawFrontFace(0);
        pub const eClockwise: Self = RawFrontFace(1);

        pub fn normalise(self) -> FrontFace {
            match self {
                Self::eCounterClockwise => FrontFace::eCounterClockwise,
                Self::eClockwise => FrontFace::eClockwise,
                v => FrontFace::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawFrontFace {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawBlendFactor(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawBlendFactor {
        pub const eZero: Self = RawBlendFactor(0);
        pub const eOne: Self = RawBlendFactor(1);
        pub const eSrcColour: Self = RawBlendFactor(2);
        pub const eOneMinusSrcColour: Self = RawBlendFactor(3);
        pub const eDstColour: Self = RawBlendFactor(4);
        pub const eOneMinusDstColour: Self = RawBlendFactor(5);
        pub const eSrcAlpha: Self = RawBlendFactor(6);
        pub const eOneMinusSrcAlpha: Self = RawBlendFactor(7);
        pub const eDstAlpha: Self = RawBlendFactor(8);
        pub const eOneMinusDstAlpha: Self = RawBlendFactor(9);
        pub const eConstantColour: Self = RawBlendFactor(10);
        pub const eOneMinusConstantColour: Self = RawBlendFactor(11);
        pub const eConstantAlpha: Self = RawBlendFactor(12);
        pub const eOneMinusConstantAlpha: Self = RawBlendFactor(13);
        pub const eSrcAlphaSaturate: Self = RawBlendFactor(14);
        pub const eSrc1Colour: Self = RawBlendFactor(15);
        pub const eOneMinusSrc1Colour: Self = RawBlendFactor(16);
        pub const eSrc1Alpha: Self = RawBlendFactor(17);
        pub const eOneMinusSrc1Alpha: Self = RawBlendFactor(18);

        pub fn normalise(self) -> BlendFactor {
            match self {
                Self::eZero => BlendFactor::eZero,
                Self::eOne => BlendFactor::eOne,
                Self::eSrcColour => BlendFactor::eSrcColour,
                Self::eOneMinusSrcColour => BlendFactor::eOneMinusSrcColour,
                Self::eDstColour => BlendFactor::eDstColour,
                Self::eOneMinusDstColour => BlendFactor::eOneMinusDstColour,
                Self::eSrcAlpha => BlendFactor::eSrcAlpha,
                Self::eOneMinusSrcAlpha => BlendFactor::eOneMinusSrcAlpha,
                Self::eDstAlpha => BlendFactor::eDstAlpha,
                Self::eOneMinusDstAlpha => BlendFactor::eOneMinusDstAlpha,
                Self::eConstantColour => BlendFactor::eConstantColour,
                Self::eOneMinusConstantColour => BlendFactor::eOneMinusConstantColour,
                Self::eConstantAlpha => BlendFactor::eConstantAlpha,
                Self::eOneMinusConstantAlpha => BlendFactor::eOneMinusConstantAlpha,
                Self::eSrcAlphaSaturate => BlendFactor::eSrcAlphaSaturate,
                Self::eSrc1Colour => BlendFactor::eSrc1Colour,
                Self::eOneMinusSrc1Colour => BlendFactor::eOneMinusSrc1Colour,
                Self::eSrc1Alpha => BlendFactor::eSrc1Alpha,
                Self::eOneMinusSrc1Alpha => BlendFactor::eOneMinusSrc1Alpha,
                v => BlendFactor::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawBlendFactor {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawBlendOp(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawBlendOp {
        pub const eAdd: Self = RawBlendOp(0);
        pub const eSubtract: Self = RawBlendOp(1);
        pub const eReverseSubtract: Self = RawBlendOp(2);
        pub const eMin: Self = RawBlendOp(3);
        pub const eMax: Self = RawBlendOp(4);
        pub const eZeroExt: Self = RawBlendOp(1000148000);
        pub const eSrcExt: Self = RawBlendOp(1000148001);
        pub const eDstExt: Self = RawBlendOp(1000148002);
        pub const eSrcOverExt: Self = RawBlendOp(1000148003);
        pub const eDstOverExt: Self = RawBlendOp(1000148004);
        pub const eSrcInExt: Self = RawBlendOp(1000148005);
        pub const eDstInExt: Self = RawBlendOp(1000148006);
        pub const eSrcOutExt: Self = RawBlendOp(1000148007);
        pub const eDstOutExt: Self = RawBlendOp(1000148008);
        pub const eSrcAtopExt: Self = RawBlendOp(1000148009);
        pub const eDstAtopExt: Self = RawBlendOp(1000148010);
        pub const eXorExt: Self = RawBlendOp(1000148011);
        pub const eMultiplyExt: Self = RawBlendOp(1000148012);
        pub const eScreenExt: Self = RawBlendOp(1000148013);
        pub const eOverlayExt: Self = RawBlendOp(1000148014);
        pub const eDarkenExt: Self = RawBlendOp(1000148015);
        pub const eLightenExt: Self = RawBlendOp(1000148016);
        pub const eColourdodgeExt: Self = RawBlendOp(1000148017);
        pub const eColourburnExt: Self = RawBlendOp(1000148018);
        pub const eHardlightExt: Self = RawBlendOp(1000148019);
        pub const eSoftlightExt: Self = RawBlendOp(1000148020);
        pub const eDifferenceExt: Self = RawBlendOp(1000148021);
        pub const eExclusionExt: Self = RawBlendOp(1000148022);
        pub const eInvertExt: Self = RawBlendOp(1000148023);
        pub const eInvertRgbExt: Self = RawBlendOp(1000148024);
        pub const eLineardodgeExt: Self = RawBlendOp(1000148025);
        pub const eLinearburnExt: Self = RawBlendOp(1000148026);
        pub const eVividlightExt: Self = RawBlendOp(1000148027);
        pub const eLinearlightExt: Self = RawBlendOp(1000148028);
        pub const ePinlightExt: Self = RawBlendOp(1000148029);
        pub const eHardmixExt: Self = RawBlendOp(1000148030);
        pub const eHslHueExt: Self = RawBlendOp(1000148031);
        pub const eHslSaturationExt: Self = RawBlendOp(1000148032);
        pub const eHslColourExt: Self = RawBlendOp(1000148033);
        pub const eHslLuminosityExt: Self = RawBlendOp(1000148034);
        pub const ePlusExt: Self = RawBlendOp(1000148035);
        pub const ePlusClampedExt: Self = RawBlendOp(1000148036);
        pub const ePlusClampedAlphaExt: Self = RawBlendOp(1000148037);
        pub const ePlusDarkerExt: Self = RawBlendOp(1000148038);
        pub const eMinusExt: Self = RawBlendOp(1000148039);
        pub const eMinusClampedExt: Self = RawBlendOp(1000148040);
        pub const eContrastExt: Self = RawBlendOp(1000148041);
        pub const eInvertOvgExt: Self = RawBlendOp(1000148042);
        pub const eRedExt: Self = RawBlendOp(1000148043);
        pub const eGreenExt: Self = RawBlendOp(1000148044);
        pub const eBlueExt: Self = RawBlendOp(1000148045);

        pub fn normalise(self) -> BlendOp {
            match self {
                Self::eAdd => BlendOp::eAdd,
                Self::eSubtract => BlendOp::eSubtract,
                Self::eReverseSubtract => BlendOp::eReverseSubtract,
                Self::eMin => BlendOp::eMin,
                Self::eMax => BlendOp::eMax,
                Self::eZeroExt => BlendOp::eZeroExt,
                Self::eSrcExt => BlendOp::eSrcExt,
                Self::eDstExt => BlendOp::eDstExt,
                Self::eSrcOverExt => BlendOp::eSrcOverExt,
                Self::eDstOverExt => BlendOp::eDstOverExt,
                Self::eSrcInExt => BlendOp::eSrcInExt,
                Self::eDstInExt => BlendOp::eDstInExt,
                Self::eSrcOutExt => BlendOp::eSrcOutExt,
                Self::eDstOutExt => BlendOp::eDstOutExt,
                Self::eSrcAtopExt => BlendOp::eSrcAtopExt,
                Self::eDstAtopExt => BlendOp::eDstAtopExt,
                Self::eXorExt => BlendOp::eXorExt,
                Self::eMultiplyExt => BlendOp::eMultiplyExt,
                Self::eScreenExt => BlendOp::eScreenExt,
                Self::eOverlayExt => BlendOp::eOverlayExt,
                Self::eDarkenExt => BlendOp::eDarkenExt,
                Self::eLightenExt => BlendOp::eLightenExt,
                Self::eColourdodgeExt => BlendOp::eColourdodgeExt,
                Self::eColourburnExt => BlendOp::eColourburnExt,
                Self::eHardlightExt => BlendOp::eHardlightExt,
                Self::eSoftlightExt => BlendOp::eSoftlightExt,
                Self::eDifferenceExt => BlendOp::eDifferenceExt,
                Self::eExclusionExt => BlendOp::eExclusionExt,
                Self::eInvertExt => BlendOp::eInvertExt,
                Self::eInvertRgbExt => BlendOp::eInvertRgbExt,
                Self::eLineardodgeExt => BlendOp::eLineardodgeExt,
                Self::eLinearburnExt => BlendOp::eLinearburnExt,
                Self::eVividlightExt => BlendOp::eVividlightExt,
                Self::eLinearlightExt => BlendOp::eLinearlightExt,
                Self::ePinlightExt => BlendOp::ePinlightExt,
                Self::eHardmixExt => BlendOp::eHardmixExt,
                Self::eHslHueExt => BlendOp::eHslHueExt,
                Self::eHslSaturationExt => BlendOp::eHslSaturationExt,
                Self::eHslColourExt => BlendOp::eHslColourExt,
                Self::eHslLuminosityExt => BlendOp::eHslLuminosityExt,
                Self::ePlusExt => BlendOp::ePlusExt,
                Self::ePlusClampedExt => BlendOp::ePlusClampedExt,
                Self::ePlusClampedAlphaExt => BlendOp::ePlusClampedAlphaExt,
                Self::ePlusDarkerExt => BlendOp::ePlusDarkerExt,
                Self::eMinusExt => BlendOp::eMinusExt,
                Self::eMinusClampedExt => BlendOp::eMinusClampedExt,
                Self::eContrastExt => BlendOp::eContrastExt,
                Self::eInvertOvgExt => BlendOp::eInvertOvgExt,
                Self::eRedExt => BlendOp::eRedExt,
                Self::eGreenExt => BlendOp::eGreenExt,
                Self::eBlueExt => BlendOp::eBlueExt,
                v => BlendOp::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawBlendOp {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStencilOp(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStencilOp {
        pub const eKeep: Self = RawStencilOp(0);
        pub const eZero: Self = RawStencilOp(1);
        pub const eReplace: Self = RawStencilOp(2);
        pub const eIncrementAndClamp: Self = RawStencilOp(3);
        pub const eDecrementAndClamp: Self = RawStencilOp(4);
        pub const eInvert: Self = RawStencilOp(5);
        pub const eIncrementAndWrap: Self = RawStencilOp(6);
        pub const eDecrementAndWrap: Self = RawStencilOp(7);

        pub fn normalise(self) -> StencilOp {
            match self {
                Self::eKeep => StencilOp::eKeep,
                Self::eZero => StencilOp::eZero,
                Self::eReplace => StencilOp::eReplace,
                Self::eIncrementAndClamp => StencilOp::eIncrementAndClamp,
                Self::eDecrementAndClamp => StencilOp::eDecrementAndClamp,
                Self::eInvert => StencilOp::eInvert,
                Self::eIncrementAndWrap => StencilOp::eIncrementAndWrap,
                Self::eDecrementAndWrap => StencilOp::eDecrementAndWrap,
                v => StencilOp::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStencilOp {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawLogicOp(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawLogicOp {
        pub const eClear: Self = RawLogicOp(0);
        pub const eAnd: Self = RawLogicOp(1);
        pub const eAndReverse: Self = RawLogicOp(2);
        pub const eCopy: Self = RawLogicOp(3);
        pub const eAndInverted: Self = RawLogicOp(4);
        pub const eNoOp: Self = RawLogicOp(5);
        pub const eXor: Self = RawLogicOp(6);
        pub const eOr: Self = RawLogicOp(7);
        pub const eNor: Self = RawLogicOp(8);
        pub const eEquivalent: Self = RawLogicOp(9);
        pub const eInvert: Self = RawLogicOp(10);
        pub const eOrReverse: Self = RawLogicOp(11);
        pub const eCopyInverted: Self = RawLogicOp(12);
        pub const eOrInverted: Self = RawLogicOp(13);
        pub const eNand: Self = RawLogicOp(14);
        pub const eSet: Self = RawLogicOp(15);

        pub fn normalise(self) -> LogicOp {
            match self {
                Self::eClear => LogicOp::eClear,
                Self::eAnd => LogicOp::eAnd,
                Self::eAndReverse => LogicOp::eAndReverse,
                Self::eCopy => LogicOp::eCopy,
                Self::eAndInverted => LogicOp::eAndInverted,
                Self::eNoOp => LogicOp::eNoOp,
                Self::eXor => LogicOp::eXor,
                Self::eOr => LogicOp::eOr,
                Self::eNor => LogicOp::eNor,
                Self::eEquivalent => LogicOp::eEquivalent,
                Self::eInvert => LogicOp::eInvert,
                Self::eOrReverse => LogicOp::eOrReverse,
                Self::eCopyInverted => LogicOp::eCopyInverted,
                Self::eOrInverted => LogicOp::eOrInverted,
                Self::eNand => LogicOp::eNand,
                Self::eSet => LogicOp::eSet,
                v => LogicOp::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawLogicOp {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawInternalAllocationType(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawInternalAllocationType {
        pub const eExecutable: Self = RawInternalAllocationType(0);

        pub fn normalise(self) -> InternalAllocationType {
            match self {
                Self::eExecutable => InternalAllocationType::eExecutable,
                v => InternalAllocationType::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawInternalAllocationType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawSystemAllocationScope(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawSystemAllocationScope {
        pub const eCommand: Self = RawSystemAllocationScope(0);
        pub const eObject: Self = RawSystemAllocationScope(1);
        pub const eCache: Self = RawSystemAllocationScope(2);
        pub const eDevice: Self = RawSystemAllocationScope(3);
        pub const eInstance: Self = RawSystemAllocationScope(4);

        pub fn normalise(self) -> SystemAllocationScope {
            match self {
                Self::eCommand => SystemAllocationScope::eCommand,
                Self::eObject => SystemAllocationScope::eObject,
                Self::eCache => SystemAllocationScope::eCache,
                Self::eDevice => SystemAllocationScope::eDevice,
                Self::eInstance => SystemAllocationScope::eInstance,
                v => SystemAllocationScope::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawSystemAllocationScope {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawPhysicalDeviceType(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawPhysicalDeviceType {
        pub const eOther: Self = RawPhysicalDeviceType(0);
        pub const eIntegratedGpu: Self = RawPhysicalDeviceType(1);
        pub const eDiscreteGpu: Self = RawPhysicalDeviceType(2);
        pub const eVirtualGpu: Self = RawPhysicalDeviceType(3);
        pub const eCpu: Self = RawPhysicalDeviceType(4);

        pub fn normalise(self) -> PhysicalDeviceType {
            match self {
                Self::eOther => PhysicalDeviceType::eOther,
                Self::eIntegratedGpu => PhysicalDeviceType::eIntegratedGpu,
                Self::eDiscreteGpu => PhysicalDeviceType::eDiscreteGpu,
                Self::eVirtualGpu => PhysicalDeviceType::eVirtualGpu,
                Self::eCpu => PhysicalDeviceType::eCpu,
                v => PhysicalDeviceType::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawPhysicalDeviceType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawVertexInputRate(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawVertexInputRate {
        pub const eVertex: Self = RawVertexInputRate(0);
        pub const eInstance: Self = RawVertexInputRate(1);

        pub fn normalise(self) -> VertexInputRate {
            match self {
                Self::eVertex => VertexInputRate::eVertex,
                Self::eInstance => VertexInputRate::eInstance,
                v => VertexInputRate::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawVertexInputRate {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawFormat(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawFormat {
        pub const eUndefined: Self = RawFormat(0);
        pub const eR4G4UnormPack8: Self = RawFormat(1);
        pub const eR4G4B4A4UnormPack16: Self = RawFormat(2);
        pub const eB4G4R4A4UnormPack16: Self = RawFormat(3);
        pub const eR5G6B5UnormPack16: Self = RawFormat(4);
        pub const eB5G6R5UnormPack16: Self = RawFormat(5);
        pub const eR5G5B5A1UnormPack16: Self = RawFormat(6);
        pub const eB5G5R5A1UnormPack16: Self = RawFormat(7);
        pub const eA1R5G5B5UnormPack16: Self = RawFormat(8);
        pub const eR8Unorm: Self = RawFormat(9);
        pub const eR8Snorm: Self = RawFormat(10);
        pub const eR8Uscaled: Self = RawFormat(11);
        pub const eR8Sscaled: Self = RawFormat(12);
        pub const eR8Uint: Self = RawFormat(13);
        pub const eR8Sint: Self = RawFormat(14);
        pub const eR8Srgb: Self = RawFormat(15);
        pub const eR8G8Unorm: Self = RawFormat(16);
        pub const eR8G8Snorm: Self = RawFormat(17);
        pub const eR8G8Uscaled: Self = RawFormat(18);
        pub const eR8G8Sscaled: Self = RawFormat(19);
        pub const eR8G8Uint: Self = RawFormat(20);
        pub const eR8G8Sint: Self = RawFormat(21);
        pub const eR8G8Srgb: Self = RawFormat(22);
        pub const eR8G8B8Unorm: Self = RawFormat(23);
        pub const eR8G8B8Snorm: Self = RawFormat(24);
        pub const eR8G8B8Uscaled: Self = RawFormat(25);
        pub const eR8G8B8Sscaled: Self = RawFormat(26);
        pub const eR8G8B8Uint: Self = RawFormat(27);
        pub const eR8G8B8Sint: Self = RawFormat(28);
        pub const eR8G8B8Srgb: Self = RawFormat(29);
        pub const eB8G8R8Unorm: Self = RawFormat(30);
        pub const eB8G8R8Snorm: Self = RawFormat(31);
        pub const eB8G8R8Uscaled: Self = RawFormat(32);
        pub const eB8G8R8Sscaled: Self = RawFormat(33);
        pub const eB8G8R8Uint: Self = RawFormat(34);
        pub const eB8G8R8Sint: Self = RawFormat(35);
        pub const eB8G8R8Srgb: Self = RawFormat(36);
        pub const eR8G8B8A8Unorm: Self = RawFormat(37);
        pub const eR8G8B8A8Snorm: Self = RawFormat(38);
        pub const eR8G8B8A8Uscaled: Self = RawFormat(39);
        pub const eR8G8B8A8Sscaled: Self = RawFormat(40);
        pub const eR8G8B8A8Uint: Self = RawFormat(41);
        pub const eR8G8B8A8Sint: Self = RawFormat(42);
        pub const eR8G8B8A8Srgb: Self = RawFormat(43);
        pub const eB8G8R8A8Unorm: Self = RawFormat(44);
        pub const eB8G8R8A8Snorm: Self = RawFormat(45);
        pub const eB8G8R8A8Uscaled: Self = RawFormat(46);
        pub const eB8G8R8A8Sscaled: Self = RawFormat(47);
        pub const eB8G8R8A8Uint: Self = RawFormat(48);
        pub const eB8G8R8A8Sint: Self = RawFormat(49);
        pub const eB8G8R8A8Srgb: Self = RawFormat(50);
        pub const eA8B8G8R8UnormPack32: Self = RawFormat(51);
        pub const eA8B8G8R8SnormPack32: Self = RawFormat(52);
        pub const eA8B8G8R8UscaledPack32: Self = RawFormat(53);
        pub const eA8B8G8R8SscaledPack32: Self = RawFormat(54);
        pub const eA8B8G8R8UintPack32: Self = RawFormat(55);
        pub const eA8B8G8R8SintPack32: Self = RawFormat(56);
        pub const eA8B8G8R8SrgbPack32: Self = RawFormat(57);
        pub const eA2R10G10B10UnormPack32: Self = RawFormat(58);
        pub const eA2R10G10B10SnormPack32: Self = RawFormat(59);
        pub const eA2R10G10B10UscaledPack32: Self = RawFormat(60);
        pub const eA2R10G10B10SscaledPack32: Self = RawFormat(61);
        pub const eA2R10G10B10UintPack32: Self = RawFormat(62);
        pub const eA2R10G10B10SintPack32: Self = RawFormat(63);
        pub const eA2B10G10R10UnormPack32: Self = RawFormat(64);
        pub const eA2B10G10R10SnormPack32: Self = RawFormat(65);
        pub const eA2B10G10R10UscaledPack32: Self = RawFormat(66);
        pub const eA2B10G10R10SscaledPack32: Self = RawFormat(67);
        pub const eA2B10G10R10UintPack32: Self = RawFormat(68);
        pub const eA2B10G10R10SintPack32: Self = RawFormat(69);
        pub const eR16Unorm: Self = RawFormat(70);
        pub const eR16Snorm: Self = RawFormat(71);
        pub const eR16Uscaled: Self = RawFormat(72);
        pub const eR16Sscaled: Self = RawFormat(73);
        pub const eR16Uint: Self = RawFormat(74);
        pub const eR16Sint: Self = RawFormat(75);
        pub const eR16Sfloat: Self = RawFormat(76);
        pub const eR16G16Unorm: Self = RawFormat(77);
        pub const eR16G16Snorm: Self = RawFormat(78);
        pub const eR16G16Uscaled: Self = RawFormat(79);
        pub const eR16G16Sscaled: Self = RawFormat(80);
        pub const eR16G16Uint: Self = RawFormat(81);
        pub const eR16G16Sint: Self = RawFormat(82);
        pub const eR16G16Sfloat: Self = RawFormat(83);
        pub const eR16G16B16Unorm: Self = RawFormat(84);
        pub const eR16G16B16Snorm: Self = RawFormat(85);
        pub const eR16G16B16Uscaled: Self = RawFormat(86);
        pub const eR16G16B16Sscaled: Self = RawFormat(87);
        pub const eR16G16B16Uint: Self = RawFormat(88);
        pub const eR16G16B16Sint: Self = RawFormat(89);
        pub const eR16G16B16Sfloat: Self = RawFormat(90);
        pub const eR16G16B16A16Unorm: Self = RawFormat(91);
        pub const eR16G16B16A16Snorm: Self = RawFormat(92);
        pub const eR16G16B16A16Uscaled: Self = RawFormat(93);
        pub const eR16G16B16A16Sscaled: Self = RawFormat(94);
        pub const eR16G16B16A16Uint: Self = RawFormat(95);
        pub const eR16G16B16A16Sint: Self = RawFormat(96);
        pub const eR16G16B16A16Sfloat: Self = RawFormat(97);
        pub const eR32Uint: Self = RawFormat(98);
        pub const eR32Sint: Self = RawFormat(99);
        pub const eR32Sfloat: Self = RawFormat(100);
        pub const eR32G32Uint: Self = RawFormat(101);
        pub const eR32G32Sint: Self = RawFormat(102);
        pub const eR32G32Sfloat: Self = RawFormat(103);
        pub const eR32G32B32Uint: Self = RawFormat(104);
        pub const eR32G32B32Sint: Self = RawFormat(105);
        pub const eR32G32B32Sfloat: Self = RawFormat(106);
        pub const eR32G32B32A32Uint: Self = RawFormat(107);
        pub const eR32G32B32A32Sint: Self = RawFormat(108);
        pub const eR32G32B32A32Sfloat: Self = RawFormat(109);
        pub const eR64Uint: Self = RawFormat(110);
        pub const eR64Sint: Self = RawFormat(111);
        pub const eR64Sfloat: Self = RawFormat(112);
        pub const eR64G64Uint: Self = RawFormat(113);
        pub const eR64G64Sint: Self = RawFormat(114);
        pub const eR64G64Sfloat: Self = RawFormat(115);
        pub const eR64G64B64Uint: Self = RawFormat(116);
        pub const eR64G64B64Sint: Self = RawFormat(117);
        pub const eR64G64B64Sfloat: Self = RawFormat(118);
        pub const eR64G64B64A64Uint: Self = RawFormat(119);
        pub const eR64G64B64A64Sint: Self = RawFormat(120);
        pub const eR64G64B64A64Sfloat: Self = RawFormat(121);
        pub const eB10G11R11UfloatPack32: Self = RawFormat(122);
        pub const eE5B9G9R9UfloatPack32: Self = RawFormat(123);
        pub const eD16Unorm: Self = RawFormat(124);
        pub const eX8D24UnormPack32: Self = RawFormat(125);
        pub const eD32Sfloat: Self = RawFormat(126);
        pub const eS8Uint: Self = RawFormat(127);
        pub const eD16UnormS8Uint: Self = RawFormat(128);
        pub const eD24UnormS8Uint: Self = RawFormat(129);
        pub const eD32SfloatS8Uint: Self = RawFormat(130);
        pub const eBc1RgbUnormBlock: Self = RawFormat(131);
        pub const eBc1RgbSrgbBlock: Self = RawFormat(132);
        pub const eBc1RgbaUnormBlock: Self = RawFormat(133);
        pub const eBc1RgbaSrgbBlock: Self = RawFormat(134);
        pub const eBc2UnormBlock: Self = RawFormat(135);
        pub const eBc2SrgbBlock: Self = RawFormat(136);
        pub const eBc3UnormBlock: Self = RawFormat(137);
        pub const eBc3SrgbBlock: Self = RawFormat(138);
        pub const eBc4UnormBlock: Self = RawFormat(139);
        pub const eBc4SnormBlock: Self = RawFormat(140);
        pub const eBc5UnormBlock: Self = RawFormat(141);
        pub const eBc5SnormBlock: Self = RawFormat(142);
        pub const eBc6hUfloatBlock: Self = RawFormat(143);
        pub const eBc6hSfloatBlock: Self = RawFormat(144);
        pub const eBc7UnormBlock: Self = RawFormat(145);
        pub const eBc7SrgbBlock: Self = RawFormat(146);
        pub const eEtc2R8G8B8UnormBlock: Self = RawFormat(147);
        pub const eEtc2R8G8B8SrgbBlock: Self = RawFormat(148);
        pub const eEtc2R8G8B8A1UnormBlock: Self = RawFormat(149);
        pub const eEtc2R8G8B8A1SrgbBlock: Self = RawFormat(150);
        pub const eEtc2R8G8B8A8UnormBlock: Self = RawFormat(151);
        pub const eEtc2R8G8B8A8SrgbBlock: Self = RawFormat(152);
        pub const eEacR11UnormBlock: Self = RawFormat(153);
        pub const eEacR11SnormBlock: Self = RawFormat(154);
        pub const eEacR11G11UnormBlock: Self = RawFormat(155);
        pub const eEacR11G11SnormBlock: Self = RawFormat(156);
        pub const eAstc4X4UnormBlock: Self = RawFormat(157);
        pub const eAstc4X4SrgbBlock: Self = RawFormat(158);
        pub const eAstc5X4UnormBlock: Self = RawFormat(159);
        pub const eAstc5X4SrgbBlock: Self = RawFormat(160);
        pub const eAstc5X5UnormBlock: Self = RawFormat(161);
        pub const eAstc5X5SrgbBlock: Self = RawFormat(162);
        pub const eAstc6X5UnormBlock: Self = RawFormat(163);
        pub const eAstc6X5SrgbBlock: Self = RawFormat(164);
        pub const eAstc6X6UnormBlock: Self = RawFormat(165);
        pub const eAstc6X6SrgbBlock: Self = RawFormat(166);
        pub const eAstc8X5UnormBlock: Self = RawFormat(167);
        pub const eAstc8X5SrgbBlock: Self = RawFormat(168);
        pub const eAstc8X6UnormBlock: Self = RawFormat(169);
        pub const eAstc8X6SrgbBlock: Self = RawFormat(170);
        pub const eAstc8X8UnormBlock: Self = RawFormat(171);
        pub const eAstc8X8SrgbBlock: Self = RawFormat(172);
        pub const eAstc10X5UnormBlock: Self = RawFormat(173);
        pub const eAstc10X5SrgbBlock: Self = RawFormat(174);
        pub const eAstc10X6UnormBlock: Self = RawFormat(175);
        pub const eAstc10X6SrgbBlock: Self = RawFormat(176);
        pub const eAstc10X8UnormBlock: Self = RawFormat(177);
        pub const eAstc10X8SrgbBlock: Self = RawFormat(178);
        pub const eAstc10X10UnormBlock: Self = RawFormat(179);
        pub const eAstc10X10SrgbBlock: Self = RawFormat(180);
        pub const eAstc12X10UnormBlock: Self = RawFormat(181);
        pub const eAstc12X10SrgbBlock: Self = RawFormat(182);
        pub const eAstc12X12UnormBlock: Self = RawFormat(183);
        pub const eAstc12X12SrgbBlock: Self = RawFormat(184);
        pub const ePvrtc12bppUnormBlockImg: Self = RawFormat(1000054000);
        pub const ePvrtc14bppUnormBlockImg: Self = RawFormat(1000054001);
        pub const ePvrtc22bppUnormBlockImg: Self = RawFormat(1000054002);
        pub const ePvrtc24bppUnormBlockImg: Self = RawFormat(1000054003);
        pub const ePvrtc12bppSrgbBlockImg: Self = RawFormat(1000054004);
        pub const ePvrtc14bppSrgbBlockImg: Self = RawFormat(1000054005);
        pub const ePvrtc22bppSrgbBlockImg: Self = RawFormat(1000054006);
        pub const ePvrtc24bppSrgbBlockImg: Self = RawFormat(1000054007);
        pub const eAstc4X4SfloatBlock: Self = RawFormat(1000066000);
        pub const eAstc5X4SfloatBlock: Self = RawFormat(1000066001);
        pub const eAstc5X5SfloatBlock: Self = RawFormat(1000066002);
        pub const eAstc6X5SfloatBlock: Self = RawFormat(1000066003);
        pub const eAstc6X6SfloatBlock: Self = RawFormat(1000066004);
        pub const eAstc8X5SfloatBlock: Self = RawFormat(1000066005);
        pub const eAstc8X6SfloatBlock: Self = RawFormat(1000066006);
        pub const eAstc8X8SfloatBlock: Self = RawFormat(1000066007);
        pub const eAstc10X5SfloatBlock: Self = RawFormat(1000066008);
        pub const eAstc10X6SfloatBlock: Self = RawFormat(1000066009);
        pub const eAstc10X8SfloatBlock: Self = RawFormat(1000066010);
        pub const eAstc10X10SfloatBlock: Self = RawFormat(1000066011);
        pub const eAstc12X10SfloatBlock: Self = RawFormat(1000066012);
        pub const eAstc12X12SfloatBlock: Self = RawFormat(1000066013);
        pub const eG8B8G8R8422Unorm: Self = RawFormat(1000156000);
        pub const eB8G8R8G8422Unorm: Self = RawFormat(1000156001);
        pub const eG8B8R83plane420Unorm: Self = RawFormat(1000156002);
        pub const eG8B8R82plane420Unorm: Self = RawFormat(1000156003);
        pub const eG8B8R83plane422Unorm: Self = RawFormat(1000156004);
        pub const eG8B8R82plane422Unorm: Self = RawFormat(1000156005);
        pub const eG8B8R83plane444Unorm: Self = RawFormat(1000156006);
        pub const eR10X6UnormPack16: Self = RawFormat(1000156007);
        pub const eR10X6G10X6Unorm2pack16: Self = RawFormat(1000156008);
        pub const eR10X6G10X6B10X6A10X6Unorm4pack16: Self = RawFormat(1000156009);
        pub const eG10X6B10X6G10X6R10X6422Unorm4pack16: Self = RawFormat(1000156010);
        pub const eB10X6G10X6R10X6G10X6422Unorm4pack16: Self = RawFormat(1000156011);
        pub const eG10X6B10X6R10X63plane420Unorm3pack16: Self = RawFormat(1000156012);
        pub const eG10X6B10X6R10X62plane420Unorm3pack16: Self = RawFormat(1000156013);
        pub const eG10X6B10X6R10X63plane422Unorm3pack16: Self = RawFormat(1000156014);
        pub const eG10X6B10X6R10X62plane422Unorm3pack16: Self = RawFormat(1000156015);
        pub const eG10X6B10X6R10X63plane444Unorm3pack16: Self = RawFormat(1000156016);
        pub const eR12X4UnormPack16: Self = RawFormat(1000156017);
        pub const eR12X4G12X4Unorm2pack16: Self = RawFormat(1000156018);
        pub const eR12X4G12X4B12X4A12X4Unorm4pack16: Self = RawFormat(1000156019);
        pub const eG12X4B12X4G12X4R12X4422Unorm4pack16: Self = RawFormat(1000156020);
        pub const eB12X4G12X4R12X4G12X4422Unorm4pack16: Self = RawFormat(1000156021);
        pub const eG12X4B12X4R12X43plane420Unorm3pack16: Self = RawFormat(1000156022);
        pub const eG12X4B12X4R12X42plane420Unorm3pack16: Self = RawFormat(1000156023);
        pub const eG12X4B12X4R12X43plane422Unorm3pack16: Self = RawFormat(1000156024);
        pub const eG12X4B12X4R12X42plane422Unorm3pack16: Self = RawFormat(1000156025);
        pub const eG12X4B12X4R12X43plane444Unorm3pack16: Self = RawFormat(1000156026);
        pub const eG16B16G16R16422Unorm: Self = RawFormat(1000156027);
        pub const eB16G16R16G16422Unorm: Self = RawFormat(1000156028);
        pub const eG16B16R163plane420Unorm: Self = RawFormat(1000156029);
        pub const eG16B16R162plane420Unorm: Self = RawFormat(1000156030);
        pub const eG16B16R163plane422Unorm: Self = RawFormat(1000156031);
        pub const eG16B16R162plane422Unorm: Self = RawFormat(1000156032);
        pub const eG16B16R163plane444Unorm: Self = RawFormat(1000156033);
        pub const eG8B8R82plane444Unorm: Self = RawFormat(1000330000);
        pub const eG10X6B10X6R10X62plane444Unorm3pack16: Self = RawFormat(1000330001);
        pub const eG12X4B12X4R12X42plane444Unorm3pack16: Self = RawFormat(1000330002);
        pub const eG16B16R162plane444Unorm: Self = RawFormat(1000330003);
        pub const eA4R4G4B4UnormPack16: Self = RawFormat(1000340000);
        pub const eA4B4G4R4UnormPack16: Self = RawFormat(1000340001);
        pub const eR16G16S105Nv: Self = RawFormat(1000464000);
        pub const eA4B4G4R4UnormPack16Ext: Self = Self::eA4B4G4R4UnormPack16;
        pub const eA4R4G4B4UnormPack16Ext: Self = Self::eA4R4G4B4UnormPack16;
        pub const eAstc10X10SfloatBlockExt: Self = Self::eAstc10X10SfloatBlock;
        pub const eAstc10X5SfloatBlockExt: Self = Self::eAstc10X5SfloatBlock;
        pub const eAstc10X6SfloatBlockExt: Self = Self::eAstc10X6SfloatBlock;
        pub const eAstc10X8SfloatBlockExt: Self = Self::eAstc10X8SfloatBlock;
        pub const eAstc12X10SfloatBlockExt: Self = Self::eAstc12X10SfloatBlock;
        pub const eAstc12X12SfloatBlockExt: Self = Self::eAstc12X12SfloatBlock;
        pub const eAstc4X4SfloatBlockExt: Self = Self::eAstc4X4SfloatBlock;
        pub const eAstc5X4SfloatBlockExt: Self = Self::eAstc5X4SfloatBlock;
        pub const eAstc5X5SfloatBlockExt: Self = Self::eAstc5X5SfloatBlock;
        pub const eAstc6X5SfloatBlockExt: Self = Self::eAstc6X5SfloatBlock;
        pub const eAstc6X6SfloatBlockExt: Self = Self::eAstc6X6SfloatBlock;
        pub const eAstc8X5SfloatBlockExt: Self = Self::eAstc8X5SfloatBlock;
        pub const eAstc8X6SfloatBlockExt: Self = Self::eAstc8X6SfloatBlock;
        pub const eAstc8X8SfloatBlockExt: Self = Self::eAstc8X8SfloatBlock;
        pub const eB10X6G10X6R10X6G10X6422Unorm4pack16Khr: Self = Self::eB10X6G10X6R10X6G10X6422Unorm4pack16;
        pub const eB12X4G12X4R12X4G12X4422Unorm4pack16Khr: Self = Self::eB12X4G12X4R12X4G12X4422Unorm4pack16;
        pub const eB16G16R16G16422UnormKhr: Self = Self::eB16G16R16G16422Unorm;
        pub const eB8G8R8G8422UnormKhr: Self = Self::eB8G8R8G8422Unorm;
        pub const eG10X6B10X6G10X6R10X6422Unorm4pack16Khr: Self = Self::eG10X6B10X6G10X6R10X6422Unorm4pack16;
        pub const eG10X6B10X6R10X62plane420Unorm3pack16Khr: Self = Self::eG10X6B10X6R10X62plane420Unorm3pack16;
        pub const eG10X6B10X6R10X62plane422Unorm3pack16Khr: Self = Self::eG10X6B10X6R10X62plane422Unorm3pack16;
        pub const eG10X6B10X6R10X62plane444Unorm3pack16Ext: Self = Self::eG10X6B10X6R10X62plane444Unorm3pack16;
        pub const eG10X6B10X6R10X63plane420Unorm3pack16Khr: Self = Self::eG10X6B10X6R10X63plane420Unorm3pack16;
        pub const eG10X6B10X6R10X63plane422Unorm3pack16Khr: Self = Self::eG10X6B10X6R10X63plane422Unorm3pack16;
        pub const eG10X6B10X6R10X63plane444Unorm3pack16Khr: Self = Self::eG10X6B10X6R10X63plane444Unorm3pack16;
        pub const eG12X4B12X4G12X4R12X4422Unorm4pack16Khr: Self = Self::eG12X4B12X4G12X4R12X4422Unorm4pack16;
        pub const eG12X4B12X4R12X42plane420Unorm3pack16Khr: Self = Self::eG12X4B12X4R12X42plane420Unorm3pack16;
        pub const eG12X4B12X4R12X42plane422Unorm3pack16Khr: Self = Self::eG12X4B12X4R12X42plane422Unorm3pack16;
        pub const eG12X4B12X4R12X42plane444Unorm3pack16Ext: Self = Self::eG12X4B12X4R12X42plane444Unorm3pack16;
        pub const eG12X4B12X4R12X43plane420Unorm3pack16Khr: Self = Self::eG12X4B12X4R12X43plane420Unorm3pack16;
        pub const eG12X4B12X4R12X43plane422Unorm3pack16Khr: Self = Self::eG12X4B12X4R12X43plane422Unorm3pack16;
        pub const eG12X4B12X4R12X43plane444Unorm3pack16Khr: Self = Self::eG12X4B12X4R12X43plane444Unorm3pack16;
        pub const eG16B16G16R16422UnormKhr: Self = Self::eG16B16G16R16422Unorm;
        pub const eG16B16R162plane420UnormKhr: Self = Self::eG16B16R162plane420Unorm;
        pub const eG16B16R162plane422UnormKhr: Self = Self::eG16B16R162plane422Unorm;
        pub const eG16B16R162plane444UnormExt: Self = Self::eG16B16R162plane444Unorm;
        pub const eG16B16R163plane420UnormKhr: Self = Self::eG16B16R163plane420Unorm;
        pub const eG16B16R163plane422UnormKhr: Self = Self::eG16B16R163plane422Unorm;
        pub const eG16B16R163plane444UnormKhr: Self = Self::eG16B16R163plane444Unorm;
        pub const eG8B8G8R8422UnormKhr: Self = Self::eG8B8G8R8422Unorm;
        pub const eG8B8R82plane420UnormKhr: Self = Self::eG8B8R82plane420Unorm;
        pub const eG8B8R82plane422UnormKhr: Self = Self::eG8B8R82plane422Unorm;
        pub const eG8B8R82plane444UnormExt: Self = Self::eG8B8R82plane444Unorm;
        pub const eG8B8R83plane420UnormKhr: Self = Self::eG8B8R83plane420Unorm;
        pub const eG8B8R83plane422UnormKhr: Self = Self::eG8B8R83plane422Unorm;
        pub const eG8B8R83plane444UnormKhr: Self = Self::eG8B8R83plane444Unorm;
        pub const eR10X6G10X6B10X6A10X6Unorm4pack16Khr: Self = Self::eR10X6G10X6B10X6A10X6Unorm4pack16;
        pub const eR10X6G10X6Unorm2pack16Khr: Self = Self::eR10X6G10X6Unorm2pack16;
        pub const eR10X6UnormPack16Khr: Self = Self::eR10X6UnormPack16;
        pub const eR12X4G12X4B12X4A12X4Unorm4pack16Khr: Self = Self::eR12X4G12X4B12X4A12X4Unorm4pack16;
        pub const eR12X4G12X4Unorm2pack16Khr: Self = Self::eR12X4G12X4Unorm2pack16;
        pub const eR12X4UnormPack16Khr: Self = Self::eR12X4UnormPack16;

        pub fn normalise(self) -> Format {
            match self {
                Self::eUndefined => Format::eUndefined,
                Self::eR4G4UnormPack8 => Format::eR4G4UnormPack8,
                Self::eR4G4B4A4UnormPack16 => Format::eR4G4B4A4UnormPack16,
                Self::eB4G4R4A4UnormPack16 => Format::eB4G4R4A4UnormPack16,
                Self::eR5G6B5UnormPack16 => Format::eR5G6B5UnormPack16,
                Self::eB5G6R5UnormPack16 => Format::eB5G6R5UnormPack16,
                Self::eR5G5B5A1UnormPack16 => Format::eR5G5B5A1UnormPack16,
                Self::eB5G5R5A1UnormPack16 => Format::eB5G5R5A1UnormPack16,
                Self::eA1R5G5B5UnormPack16 => Format::eA1R5G5B5UnormPack16,
                Self::eR8Unorm => Format::eR8Unorm,
                Self::eR8Snorm => Format::eR8Snorm,
                Self::eR8Uscaled => Format::eR8Uscaled,
                Self::eR8Sscaled => Format::eR8Sscaled,
                Self::eR8Uint => Format::eR8Uint,
                Self::eR8Sint => Format::eR8Sint,
                Self::eR8Srgb => Format::eR8Srgb,
                Self::eR8G8Unorm => Format::eR8G8Unorm,
                Self::eR8G8Snorm => Format::eR8G8Snorm,
                Self::eR8G8Uscaled => Format::eR8G8Uscaled,
                Self::eR8G8Sscaled => Format::eR8G8Sscaled,
                Self::eR8G8Uint => Format::eR8G8Uint,
                Self::eR8G8Sint => Format::eR8G8Sint,
                Self::eR8G8Srgb => Format::eR8G8Srgb,
                Self::eR8G8B8Unorm => Format::eR8G8B8Unorm,
                Self::eR8G8B8Snorm => Format::eR8G8B8Snorm,
                Self::eR8G8B8Uscaled => Format::eR8G8B8Uscaled,
                Self::eR8G8B8Sscaled => Format::eR8G8B8Sscaled,
                Self::eR8G8B8Uint => Format::eR8G8B8Uint,
                Self::eR8G8B8Sint => Format::eR8G8B8Sint,
                Self::eR8G8B8Srgb => Format::eR8G8B8Srgb,
                Self::eB8G8R8Unorm => Format::eB8G8R8Unorm,
                Self::eB8G8R8Snorm => Format::eB8G8R8Snorm,
                Self::eB8G8R8Uscaled => Format::eB8G8R8Uscaled,
                Self::eB8G8R8Sscaled => Format::eB8G8R8Sscaled,
                Self::eB8G8R8Uint => Format::eB8G8R8Uint,
                Self::eB8G8R8Sint => Format::eB8G8R8Sint,
                Self::eB8G8R8Srgb => Format::eB8G8R8Srgb,
                Self::eR8G8B8A8Unorm => Format::eR8G8B8A8Unorm,
                Self::eR8G8B8A8Snorm => Format::eR8G8B8A8Snorm,
                Self::eR8G8B8A8Uscaled => Format::eR8G8B8A8Uscaled,
                Self::eR8G8B8A8Sscaled => Format::eR8G8B8A8Sscaled,
                Self::eR8G8B8A8Uint => Format::eR8G8B8A8Uint,
                Self::eR8G8B8A8Sint => Format::eR8G8B8A8Sint,
                Self::eR8G8B8A8Srgb => Format::eR8G8B8A8Srgb,
                Self::eB8G8R8A8Unorm => Format::eB8G8R8A8Unorm,
                Self::eB8G8R8A8Snorm => Format::eB8G8R8A8Snorm,
                Self::eB8G8R8A8Uscaled => Format::eB8G8R8A8Uscaled,
                Self::eB8G8R8A8Sscaled => Format::eB8G8R8A8Sscaled,
                Self::eB8G8R8A8Uint => Format::eB8G8R8A8Uint,
                Self::eB8G8R8A8Sint => Format::eB8G8R8A8Sint,
                Self::eB8G8R8A8Srgb => Format::eB8G8R8A8Srgb,
                Self::eA8B8G8R8UnormPack32 => Format::eA8B8G8R8UnormPack32,
                Self::eA8B8G8R8SnormPack32 => Format::eA8B8G8R8SnormPack32,
                Self::eA8B8G8R8UscaledPack32 => Format::eA8B8G8R8UscaledPack32,
                Self::eA8B8G8R8SscaledPack32 => Format::eA8B8G8R8SscaledPack32,
                Self::eA8B8G8R8UintPack32 => Format::eA8B8G8R8UintPack32,
                Self::eA8B8G8R8SintPack32 => Format::eA8B8G8R8SintPack32,
                Self::eA8B8G8R8SrgbPack32 => Format::eA8B8G8R8SrgbPack32,
                Self::eA2R10G10B10UnormPack32 => Format::eA2R10G10B10UnormPack32,
                Self::eA2R10G10B10SnormPack32 => Format::eA2R10G10B10SnormPack32,
                Self::eA2R10G10B10UscaledPack32 => Format::eA2R10G10B10UscaledPack32,
                Self::eA2R10G10B10SscaledPack32 => Format::eA2R10G10B10SscaledPack32,
                Self::eA2R10G10B10UintPack32 => Format::eA2R10G10B10UintPack32,
                Self::eA2R10G10B10SintPack32 => Format::eA2R10G10B10SintPack32,
                Self::eA2B10G10R10UnormPack32 => Format::eA2B10G10R10UnormPack32,
                Self::eA2B10G10R10SnormPack32 => Format::eA2B10G10R10SnormPack32,
                Self::eA2B10G10R10UscaledPack32 => Format::eA2B10G10R10UscaledPack32,
                Self::eA2B10G10R10SscaledPack32 => Format::eA2B10G10R10SscaledPack32,
                Self::eA2B10G10R10UintPack32 => Format::eA2B10G10R10UintPack32,
                Self::eA2B10G10R10SintPack32 => Format::eA2B10G10R10SintPack32,
                Self::eR16Unorm => Format::eR16Unorm,
                Self::eR16Snorm => Format::eR16Snorm,
                Self::eR16Uscaled => Format::eR16Uscaled,
                Self::eR16Sscaled => Format::eR16Sscaled,
                Self::eR16Uint => Format::eR16Uint,
                Self::eR16Sint => Format::eR16Sint,
                Self::eR16Sfloat => Format::eR16Sfloat,
                Self::eR16G16Unorm => Format::eR16G16Unorm,
                Self::eR16G16Snorm => Format::eR16G16Snorm,
                Self::eR16G16Uscaled => Format::eR16G16Uscaled,
                Self::eR16G16Sscaled => Format::eR16G16Sscaled,
                Self::eR16G16Uint => Format::eR16G16Uint,
                Self::eR16G16Sint => Format::eR16G16Sint,
                Self::eR16G16Sfloat => Format::eR16G16Sfloat,
                Self::eR16G16B16Unorm => Format::eR16G16B16Unorm,
                Self::eR16G16B16Snorm => Format::eR16G16B16Snorm,
                Self::eR16G16B16Uscaled => Format::eR16G16B16Uscaled,
                Self::eR16G16B16Sscaled => Format::eR16G16B16Sscaled,
                Self::eR16G16B16Uint => Format::eR16G16B16Uint,
                Self::eR16G16B16Sint => Format::eR16G16B16Sint,
                Self::eR16G16B16Sfloat => Format::eR16G16B16Sfloat,
                Self::eR16G16B16A16Unorm => Format::eR16G16B16A16Unorm,
                Self::eR16G16B16A16Snorm => Format::eR16G16B16A16Snorm,
                Self::eR16G16B16A16Uscaled => Format::eR16G16B16A16Uscaled,
                Self::eR16G16B16A16Sscaled => Format::eR16G16B16A16Sscaled,
                Self::eR16G16B16A16Uint => Format::eR16G16B16A16Uint,
                Self::eR16G16B16A16Sint => Format::eR16G16B16A16Sint,
                Self::eR16G16B16A16Sfloat => Format::eR16G16B16A16Sfloat,
                Self::eR32Uint => Format::eR32Uint,
                Self::eR32Sint => Format::eR32Sint,
                Self::eR32Sfloat => Format::eR32Sfloat,
                Self::eR32G32Uint => Format::eR32G32Uint,
                Self::eR32G32Sint => Format::eR32G32Sint,
                Self::eR32G32Sfloat => Format::eR32G32Sfloat,
                Self::eR32G32B32Uint => Format::eR32G32B32Uint,
                Self::eR32G32B32Sint => Format::eR32G32B32Sint,
                Self::eR32G32B32Sfloat => Format::eR32G32B32Sfloat,
                Self::eR32G32B32A32Uint => Format::eR32G32B32A32Uint,
                Self::eR32G32B32A32Sint => Format::eR32G32B32A32Sint,
                Self::eR32G32B32A32Sfloat => Format::eR32G32B32A32Sfloat,
                Self::eR64Uint => Format::eR64Uint,
                Self::eR64Sint => Format::eR64Sint,
                Self::eR64Sfloat => Format::eR64Sfloat,
                Self::eR64G64Uint => Format::eR64G64Uint,
                Self::eR64G64Sint => Format::eR64G64Sint,
                Self::eR64G64Sfloat => Format::eR64G64Sfloat,
                Self::eR64G64B64Uint => Format::eR64G64B64Uint,
                Self::eR64G64B64Sint => Format::eR64G64B64Sint,
                Self::eR64G64B64Sfloat => Format::eR64G64B64Sfloat,
                Self::eR64G64B64A64Uint => Format::eR64G64B64A64Uint,
                Self::eR64G64B64A64Sint => Format::eR64G64B64A64Sint,
                Self::eR64G64B64A64Sfloat => Format::eR64G64B64A64Sfloat,
                Self::eB10G11R11UfloatPack32 => Format::eB10G11R11UfloatPack32,
                Self::eE5B9G9R9UfloatPack32 => Format::eE5B9G9R9UfloatPack32,
                Self::eD16Unorm => Format::eD16Unorm,
                Self::eX8D24UnormPack32 => Format::eX8D24UnormPack32,
                Self::eD32Sfloat => Format::eD32Sfloat,
                Self::eS8Uint => Format::eS8Uint,
                Self::eD16UnormS8Uint => Format::eD16UnormS8Uint,
                Self::eD24UnormS8Uint => Format::eD24UnormS8Uint,
                Self::eD32SfloatS8Uint => Format::eD32SfloatS8Uint,
                Self::eBc1RgbUnormBlock => Format::eBc1RgbUnormBlock,
                Self::eBc1RgbSrgbBlock => Format::eBc1RgbSrgbBlock,
                Self::eBc1RgbaUnormBlock => Format::eBc1RgbaUnormBlock,
                Self::eBc1RgbaSrgbBlock => Format::eBc1RgbaSrgbBlock,
                Self::eBc2UnormBlock => Format::eBc2UnormBlock,
                Self::eBc2SrgbBlock => Format::eBc2SrgbBlock,
                Self::eBc3UnormBlock => Format::eBc3UnormBlock,
                Self::eBc3SrgbBlock => Format::eBc3SrgbBlock,
                Self::eBc4UnormBlock => Format::eBc4UnormBlock,
                Self::eBc4SnormBlock => Format::eBc4SnormBlock,
                Self::eBc5UnormBlock => Format::eBc5UnormBlock,
                Self::eBc5SnormBlock => Format::eBc5SnormBlock,
                Self::eBc6hUfloatBlock => Format::eBc6hUfloatBlock,
                Self::eBc6hSfloatBlock => Format::eBc6hSfloatBlock,
                Self::eBc7UnormBlock => Format::eBc7UnormBlock,
                Self::eBc7SrgbBlock => Format::eBc7SrgbBlock,
                Self::eEtc2R8G8B8UnormBlock => Format::eEtc2R8G8B8UnormBlock,
                Self::eEtc2R8G8B8SrgbBlock => Format::eEtc2R8G8B8SrgbBlock,
                Self::eEtc2R8G8B8A1UnormBlock => Format::eEtc2R8G8B8A1UnormBlock,
                Self::eEtc2R8G8B8A1SrgbBlock => Format::eEtc2R8G8B8A1SrgbBlock,
                Self::eEtc2R8G8B8A8UnormBlock => Format::eEtc2R8G8B8A8UnormBlock,
                Self::eEtc2R8G8B8A8SrgbBlock => Format::eEtc2R8G8B8A8SrgbBlock,
                Self::eEacR11UnormBlock => Format::eEacR11UnormBlock,
                Self::eEacR11SnormBlock => Format::eEacR11SnormBlock,
                Self::eEacR11G11UnormBlock => Format::eEacR11G11UnormBlock,
                Self::eEacR11G11SnormBlock => Format::eEacR11G11SnormBlock,
                Self::eAstc4X4UnormBlock => Format::eAstc4X4UnormBlock,
                Self::eAstc4X4SrgbBlock => Format::eAstc4X4SrgbBlock,
                Self::eAstc5X4UnormBlock => Format::eAstc5X4UnormBlock,
                Self::eAstc5X4SrgbBlock => Format::eAstc5X4SrgbBlock,
                Self::eAstc5X5UnormBlock => Format::eAstc5X5UnormBlock,
                Self::eAstc5X5SrgbBlock => Format::eAstc5X5SrgbBlock,
                Self::eAstc6X5UnormBlock => Format::eAstc6X5UnormBlock,
                Self::eAstc6X5SrgbBlock => Format::eAstc6X5SrgbBlock,
                Self::eAstc6X6UnormBlock => Format::eAstc6X6UnormBlock,
                Self::eAstc6X6SrgbBlock => Format::eAstc6X6SrgbBlock,
                Self::eAstc8X5UnormBlock => Format::eAstc8X5UnormBlock,
                Self::eAstc8X5SrgbBlock => Format::eAstc8X5SrgbBlock,
                Self::eAstc8X6UnormBlock => Format::eAstc8X6UnormBlock,
                Self::eAstc8X6SrgbBlock => Format::eAstc8X6SrgbBlock,
                Self::eAstc8X8UnormBlock => Format::eAstc8X8UnormBlock,
                Self::eAstc8X8SrgbBlock => Format::eAstc8X8SrgbBlock,
                Self::eAstc10X5UnormBlock => Format::eAstc10X5UnormBlock,
                Self::eAstc10X5SrgbBlock => Format::eAstc10X5SrgbBlock,
                Self::eAstc10X6UnormBlock => Format::eAstc10X6UnormBlock,
                Self::eAstc10X6SrgbBlock => Format::eAstc10X6SrgbBlock,
                Self::eAstc10X8UnormBlock => Format::eAstc10X8UnormBlock,
                Self::eAstc10X8SrgbBlock => Format::eAstc10X8SrgbBlock,
                Self::eAstc10X10UnormBlock => Format::eAstc10X10UnormBlock,
                Self::eAstc10X10SrgbBlock => Format::eAstc10X10SrgbBlock,
                Self::eAstc12X10UnormBlock => Format::eAstc12X10UnormBlock,
                Self::eAstc12X10SrgbBlock => Format::eAstc12X10SrgbBlock,
                Self::eAstc12X12UnormBlock => Format::eAstc12X12UnormBlock,
                Self::eAstc12X12SrgbBlock => Format::eAstc12X12SrgbBlock,
                Self::ePvrtc12bppUnormBlockImg => Format::ePvrtc12bppUnormBlockImg,
                Self::ePvrtc14bppUnormBlockImg => Format::ePvrtc14bppUnormBlockImg,
                Self::ePvrtc22bppUnormBlockImg => Format::ePvrtc22bppUnormBlockImg,
                Self::ePvrtc24bppUnormBlockImg => Format::ePvrtc24bppUnormBlockImg,
                Self::ePvrtc12bppSrgbBlockImg => Format::ePvrtc12bppSrgbBlockImg,
                Self::ePvrtc14bppSrgbBlockImg => Format::ePvrtc14bppSrgbBlockImg,
                Self::ePvrtc22bppSrgbBlockImg => Format::ePvrtc22bppSrgbBlockImg,
                Self::ePvrtc24bppSrgbBlockImg => Format::ePvrtc24bppSrgbBlockImg,
                Self::eAstc4X4SfloatBlock => Format::eAstc4X4SfloatBlock,
                Self::eAstc5X4SfloatBlock => Format::eAstc5X4SfloatBlock,
                Self::eAstc5X5SfloatBlock => Format::eAstc5X5SfloatBlock,
                Self::eAstc6X5SfloatBlock => Format::eAstc6X5SfloatBlock,
                Self::eAstc6X6SfloatBlock => Format::eAstc6X6SfloatBlock,
                Self::eAstc8X5SfloatBlock => Format::eAstc8X5SfloatBlock,
                Self::eAstc8X6SfloatBlock => Format::eAstc8X6SfloatBlock,
                Self::eAstc8X8SfloatBlock => Format::eAstc8X8SfloatBlock,
                Self::eAstc10X5SfloatBlock => Format::eAstc10X5SfloatBlock,
                Self::eAstc10X6SfloatBlock => Format::eAstc10X6SfloatBlock,
                Self::eAstc10X8SfloatBlock => Format::eAstc10X8SfloatBlock,
                Self::eAstc10X10SfloatBlock => Format::eAstc10X10SfloatBlock,
                Self::eAstc12X10SfloatBlock => Format::eAstc12X10SfloatBlock,
                Self::eAstc12X12SfloatBlock => Format::eAstc12X12SfloatBlock,
                Self::eG8B8G8R8422Unorm => Format::eG8B8G8R8422Unorm,
                Self::eB8G8R8G8422Unorm => Format::eB8G8R8G8422Unorm,
                Self::eG8B8R83plane420Unorm => Format::eG8B8R83plane420Unorm,
                Self::eG8B8R82plane420Unorm => Format::eG8B8R82plane420Unorm,
                Self::eG8B8R83plane422Unorm => Format::eG8B8R83plane422Unorm,
                Self::eG8B8R82plane422Unorm => Format::eG8B8R82plane422Unorm,
                Self::eG8B8R83plane444Unorm => Format::eG8B8R83plane444Unorm,
                Self::eR10X6UnormPack16 => Format::eR10X6UnormPack16,
                Self::eR10X6G10X6Unorm2pack16 => Format::eR10X6G10X6Unorm2pack16,
                Self::eR10X6G10X6B10X6A10X6Unorm4pack16 => Format::eR10X6G10X6B10X6A10X6Unorm4pack16,
                Self::eG10X6B10X6G10X6R10X6422Unorm4pack16 => Format::eG10X6B10X6G10X6R10X6422Unorm4pack16,
                Self::eB10X6G10X6R10X6G10X6422Unorm4pack16 => Format::eB10X6G10X6R10X6G10X6422Unorm4pack16,
                Self::eG10X6B10X6R10X63plane420Unorm3pack16 => Format::eG10X6B10X6R10X63plane420Unorm3pack16,
                Self::eG10X6B10X6R10X62plane420Unorm3pack16 => Format::eG10X6B10X6R10X62plane420Unorm3pack16,
                Self::eG10X6B10X6R10X63plane422Unorm3pack16 => Format::eG10X6B10X6R10X63plane422Unorm3pack16,
                Self::eG10X6B10X6R10X62plane422Unorm3pack16 => Format::eG10X6B10X6R10X62plane422Unorm3pack16,
                Self::eG10X6B10X6R10X63plane444Unorm3pack16 => Format::eG10X6B10X6R10X63plane444Unorm3pack16,
                Self::eR12X4UnormPack16 => Format::eR12X4UnormPack16,
                Self::eR12X4G12X4Unorm2pack16 => Format::eR12X4G12X4Unorm2pack16,
                Self::eR12X4G12X4B12X4A12X4Unorm4pack16 => Format::eR12X4G12X4B12X4A12X4Unorm4pack16,
                Self::eG12X4B12X4G12X4R12X4422Unorm4pack16 => Format::eG12X4B12X4G12X4R12X4422Unorm4pack16,
                Self::eB12X4G12X4R12X4G12X4422Unorm4pack16 => Format::eB12X4G12X4R12X4G12X4422Unorm4pack16,
                Self::eG12X4B12X4R12X43plane420Unorm3pack16 => Format::eG12X4B12X4R12X43plane420Unorm3pack16,
                Self::eG12X4B12X4R12X42plane420Unorm3pack16 => Format::eG12X4B12X4R12X42plane420Unorm3pack16,
                Self::eG12X4B12X4R12X43plane422Unorm3pack16 => Format::eG12X4B12X4R12X43plane422Unorm3pack16,
                Self::eG12X4B12X4R12X42plane422Unorm3pack16 => Format::eG12X4B12X4R12X42plane422Unorm3pack16,
                Self::eG12X4B12X4R12X43plane444Unorm3pack16 => Format::eG12X4B12X4R12X43plane444Unorm3pack16,
                Self::eG16B16G16R16422Unorm => Format::eG16B16G16R16422Unorm,
                Self::eB16G16R16G16422Unorm => Format::eB16G16R16G16422Unorm,
                Self::eG16B16R163plane420Unorm => Format::eG16B16R163plane420Unorm,
                Self::eG16B16R162plane420Unorm => Format::eG16B16R162plane420Unorm,
                Self::eG16B16R163plane422Unorm => Format::eG16B16R163plane422Unorm,
                Self::eG16B16R162plane422Unorm => Format::eG16B16R162plane422Unorm,
                Self::eG16B16R163plane444Unorm => Format::eG16B16R163plane444Unorm,
                Self::eG8B8R82plane444Unorm => Format::eG8B8R82plane444Unorm,
                Self::eG10X6B10X6R10X62plane444Unorm3pack16 => Format::eG10X6B10X6R10X62plane444Unorm3pack16,
                Self::eG12X4B12X4R12X42plane444Unorm3pack16 => Format::eG12X4B12X4R12X42plane444Unorm3pack16,
                Self::eG16B16R162plane444Unorm => Format::eG16B16R162plane444Unorm,
                Self::eA4R4G4B4UnormPack16 => Format::eA4R4G4B4UnormPack16,
                Self::eA4B4G4R4UnormPack16 => Format::eA4B4G4R4UnormPack16,
                Self::eR16G16S105Nv => Format::eR16G16S105Nv,
                v => Format::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawFormat {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStructureType(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStructureType {
        pub const eApplicationInfo: Self = RawStructureType(0);
        pub const eInstanceCreateInfo: Self = RawStructureType(1);
        pub const eDeviceQueueCreateInfo: Self = RawStructureType(2);
        pub const eDeviceCreateInfo: Self = RawStructureType(3);
        pub const eSubmitInfo: Self = RawStructureType(4);
        pub const eMemoryAllocateInfo: Self = RawStructureType(5);
        pub const eMappedMemoryRange: Self = RawStructureType(6);
        pub const eBindSparseInfo: Self = RawStructureType(7);
        pub const eFenceCreateInfo: Self = RawStructureType(8);
        pub const eSemaphoreCreateInfo: Self = RawStructureType(9);
        pub const eEventCreateInfo: Self = RawStructureType(10);
        pub const eQueryPoolCreateInfo: Self = RawStructureType(11);
        pub const eBufferCreateInfo: Self = RawStructureType(12);
        pub const eBufferViewCreateInfo: Self = RawStructureType(13);
        pub const eImageCreateInfo: Self = RawStructureType(14);
        pub const eImageViewCreateInfo: Self = RawStructureType(15);
        pub const eShaderModuleCreateInfo: Self = RawStructureType(16);
        pub const ePipelineCacheCreateInfo: Self = RawStructureType(17);
        pub const ePipelineShaderStageCreateInfo: Self = RawStructureType(18);
        pub const ePipelineVertexInputStateCreateInfo: Self = RawStructureType(19);
        pub const ePipelineInputAssemblyStateCreateInfo: Self = RawStructureType(20);
        pub const ePipelineTessellationStateCreateInfo: Self = RawStructureType(21);
        pub const ePipelineViewportStateCreateInfo: Self = RawStructureType(22);
        pub const ePipelineRasterizationStateCreateInfo: Self = RawStructureType(23);
        pub const ePipelineMultisampleStateCreateInfo: Self = RawStructureType(24);
        pub const ePipelineDepthStencilStateCreateInfo: Self = RawStructureType(25);
        pub const ePipelineColourBlendStateCreateInfo: Self = RawStructureType(26);
        pub const ePipelineDynamicStateCreateInfo: Self = RawStructureType(27);
        pub const eGraphicsPipelineCreateInfo: Self = RawStructureType(28);
        pub const eComputePipelineCreateInfo: Self = RawStructureType(29);
        pub const ePipelineLayoutCreateInfo: Self = RawStructureType(30);
        pub const eSamplerCreateInfo: Self = RawStructureType(31);
        pub const eDescriptorSetLayoutCreateInfo: Self = RawStructureType(32);
        pub const eDescriptorPoolCreateInfo: Self = RawStructureType(33);
        pub const eDescriptorSetAllocateInfo: Self = RawStructureType(34);
        pub const eWriteDescriptorSet: Self = RawStructureType(35);
        pub const eCopyDescriptorSet: Self = RawStructureType(36);
        pub const eFramebufferCreateInfo: Self = RawStructureType(37);
        pub const eRenderPassCreateInfo: Self = RawStructureType(38);
        pub const eCommandPoolCreateInfo: Self = RawStructureType(39);
        pub const eCommandBufferAllocateInfo: Self = RawStructureType(40);
        pub const eCommandBufferInheritanceInfo: Self = RawStructureType(41);
        pub const eCommandBufferBeginInfo: Self = RawStructureType(42);
        pub const eRenderPassBeginInfo: Self = RawStructureType(43);
        pub const eBufferMemoryBarrier: Self = RawStructureType(44);
        pub const eImageMemoryBarrier: Self = RawStructureType(45);
        pub const eMemoryBarrier: Self = RawStructureType(46);
        pub const eLoaderInstanceCreateInfo: Self = RawStructureType(47);
        pub const eLoaderDeviceCreateInfo: Self = RawStructureType(48);
        pub const ePhysicalDeviceVulkan11Features: Self = RawStructureType(49);
        pub const ePhysicalDeviceVulkan11Properties: Self = RawStructureType(50);
        pub const ePhysicalDeviceVulkan12Features: Self = RawStructureType(51);
        pub const ePhysicalDeviceVulkan12Properties: Self = RawStructureType(52);
        pub const ePhysicalDeviceVulkan13Features: Self = RawStructureType(53);
        pub const ePhysicalDeviceVulkan13Properties: Self = RawStructureType(54);
        pub const eSwapchainCreateInfoKhr: Self = RawStructureType(1000001000);
        pub const ePresentInfoKhr: Self = RawStructureType(1000001001);
        pub const eDisplayModeCreateInfoKhr: Self = RawStructureType(1000002000);
        pub const eDisplaySurfaceCreateInfoKhr: Self = RawStructureType(1000002001);
        pub const eDisplayPresentInfoKhr: Self = RawStructureType(1000003000);
        pub const eXlibSurfaceCreateInfoKhr: Self = RawStructureType(1000004000);
        pub const eXcbSurfaceCreateInfoKhr: Self = RawStructureType(1000005000);
        pub const eWaylandSurfaceCreateInfoKhr: Self = RawStructureType(1000006000);
        pub const eAndroidSurfaceCreateInfoKhr: Self = RawStructureType(1000008000);
        pub const eWin32SurfaceCreateInfoKhr: Self = RawStructureType(1000009000);
        pub const eDebugReportCallbackCreateInfoExt: Self = RawStructureType(1000011000);
        pub const ePipelineRasterizationStateRasterizationOrderAmd: Self = RawStructureType(1000018000);
        pub const eDebugMarkerObjectNameInfoExt: Self = RawStructureType(1000022000);
        pub const eDebugMarkerObjectTagInfoExt: Self = RawStructureType(1000022001);
        pub const eDebugMarkerMarkerInfoExt: Self = RawStructureType(1000022002);
        pub const eVideoProfileInfoKhr: Self = RawStructureType(1000023000);
        pub const eVideoCapabilitiesKhr: Self = RawStructureType(1000023001);
        pub const eVideoPictureResourceInfoKhr: Self = RawStructureType(1000023002);
        pub const eVideoSessionMemoryRequirementsKhr: Self = RawStructureType(1000023003);
        pub const eBindVideoSessionMemoryInfoKhr: Self = RawStructureType(1000023004);
        pub const eVideoSessionCreateInfoKhr: Self = RawStructureType(1000023005);
        pub const eVideoSessionParametersCreateInfoKhr: Self = RawStructureType(1000023006);
        pub const eVideoSessionParametersUpdateInfoKhr: Self = RawStructureType(1000023007);
        pub const eVideoBeginCodingInfoKhr: Self = RawStructureType(1000023008);
        pub const eVideoEndCodingInfoKhr: Self = RawStructureType(1000023009);
        pub const eVideoCodingControlInfoKhr: Self = RawStructureType(1000023010);
        pub const eVideoReferenceSlotInfoKhr: Self = RawStructureType(1000023011);
        pub const eQueueFamilyVideoPropertiesKhr: Self = RawStructureType(1000023012);
        pub const eVideoProfileListInfoKhr: Self = RawStructureType(1000023013);
        pub const ePhysicalDeviceVideoFormatInfoKhr: Self = RawStructureType(1000023014);
        pub const eVideoFormatPropertiesKhr: Self = RawStructureType(1000023015);
        pub const eQueueFamilyQueryResultStatusPropertiesKhr: Self = RawStructureType(1000023016);
        pub const eVideoDecodeInfoKhr: Self = RawStructureType(1000024000);
        pub const eVideoDecodeCapabilitiesKhr: Self = RawStructureType(1000024001);
        pub const eVideoDecodeUsageInfoKhr: Self = RawStructureType(1000024002);
        pub const eDedicatedAllocationImageCreateInfoNv: Self = RawStructureType(1000026000);
        pub const eDedicatedAllocationBufferCreateInfoNv: Self = RawStructureType(1000026001);
        pub const eDedicatedAllocationMemoryAllocateInfoNv: Self = RawStructureType(1000026002);
        pub const ePhysicalDeviceTransformFeedbackFeaturesExt: Self = RawStructureType(1000028000);
        pub const ePhysicalDeviceTransformFeedbackPropertiesExt: Self = RawStructureType(1000028001);
        pub const ePipelineRasterizationStateStreamCreateInfoExt: Self = RawStructureType(1000028002);
        pub const eCuModuleCreateInfoNvx: Self = RawStructureType(1000029000);
        pub const eCuFunctionCreateInfoNvx: Self = RawStructureType(1000029001);
        pub const eCuLaunchInfoNvx: Self = RawStructureType(1000029002);
        pub const eImageViewHandleInfoNvx: Self = RawStructureType(1000030000);
        pub const eImageViewAddressPropertiesNvx: Self = RawStructureType(1000030001);
        pub const eVideoEncodeH264CapabilitiesExt: Self = RawStructureType(1000038000);
        pub const eVideoEncodeH264SessionParametersCreateInfoExt: Self = RawStructureType(1000038001);
        pub const eVideoEncodeH264SessionParametersAddInfoExt: Self = RawStructureType(1000038002);
        pub const eVideoEncodeH264VclFrameInfoExt: Self = RawStructureType(1000038003);
        pub const eVideoEncodeH264DpbSlotInfoExt: Self = RawStructureType(1000038004);
        pub const eVideoEncodeH264NaluSliceInfoExt: Self = RawStructureType(1000038005);
        pub const eVideoEncodeH264EmitPictureParametersInfoExt: Self = RawStructureType(1000038006);
        pub const eVideoEncodeH264ProfileInfoExt: Self = RawStructureType(1000038007);
        pub const eVideoEncodeH264RateControlInfoExt: Self = RawStructureType(1000038008);
        pub const eVideoEncodeH264RateControlLayerInfoExt: Self = RawStructureType(1000038009);
        pub const eVideoEncodeH264ReferenceListsInfoExt: Self = RawStructureType(1000038010);
        pub const eVideoEncodeH265CapabilitiesExt: Self = RawStructureType(1000039000);
        pub const eVideoEncodeH265SessionParametersCreateInfoExt: Self = RawStructureType(1000039001);
        pub const eVideoEncodeH265SessionParametersAddInfoExt: Self = RawStructureType(1000039002);
        pub const eVideoEncodeH265VclFrameInfoExt: Self = RawStructureType(1000039003);
        pub const eVideoEncodeH265DpbSlotInfoExt: Self = RawStructureType(1000039004);
        pub const eVideoEncodeH265NaluSliceSegmentInfoExt: Self = RawStructureType(1000039005);
        pub const eVideoEncodeH265EmitPictureParametersInfoExt: Self = RawStructureType(1000039006);
        pub const eVideoEncodeH265ProfileInfoExt: Self = RawStructureType(1000039007);
        pub const eVideoEncodeH265ReferenceListsInfoExt: Self = RawStructureType(1000039008);
        pub const eVideoEncodeH265RateControlInfoExt: Self = RawStructureType(1000039009);
        pub const eVideoEncodeH265RateControlLayerInfoExt: Self = RawStructureType(1000039010);
        pub const eVideoDecodeH264CapabilitiesKhr: Self = RawStructureType(1000040000);
        pub const eVideoDecodeH264PictureInfoKhr: Self = RawStructureType(1000040001);
        pub const eVideoDecodeH264ProfileInfoKhr: Self = RawStructureType(1000040003);
        pub const eVideoDecodeH264SessionParametersCreateInfoKhr: Self = RawStructureType(1000040004);
        pub const eVideoDecodeH264SessionParametersAddInfoKhr: Self = RawStructureType(1000040005);
        pub const eVideoDecodeH264DpbSlotInfoKhr: Self = RawStructureType(1000040006);
        pub const eTextureLodGatherFormatPropertiesAmd: Self = RawStructureType(1000041000);
        pub const eRenderingInfo: Self = RawStructureType(1000044000);
        pub const eRenderingAttachmentInfo: Self = RawStructureType(1000044001);
        pub const ePipelineRenderingCreateInfo: Self = RawStructureType(1000044002);
        pub const ePhysicalDeviceDynamicRenderingFeatures: Self = RawStructureType(1000044003);
        pub const eCommandBufferInheritanceRenderingInfo: Self = RawStructureType(1000044004);
        pub const eRenderingFragmentShadingRateAttachmentInfoKhr: Self = RawStructureType(1000044006);
        pub const eRenderingFragmentDensityMapAttachmentInfoExt: Self = RawStructureType(1000044007);
        pub const eAttachmentSampleCountInfoAmd: Self = RawStructureType(1000044008);
        pub const eMultiviewPerViewAttributesInfoNvx: Self = RawStructureType(1000044009);
        pub const eStreamDescriptorSurfaceCreateInfoGgp: Self = RawStructureType(1000049000);
        pub const ePhysicalDeviceCornerSampledImageFeaturesNv: Self = RawStructureType(1000050000);
        pub const ePrivateVendorInfoReservedOffset0Nv: Self = RawStructureType(1000051000);
        pub const eRenderPassMultiviewCreateInfo: Self = RawStructureType(1000053000);
        pub const ePhysicalDeviceMultiviewFeatures: Self = RawStructureType(1000053001);
        pub const ePhysicalDeviceMultiviewProperties: Self = RawStructureType(1000053002);
        pub const eExternalMemoryImageCreateInfoNv: Self = RawStructureType(1000056000);
        pub const eExportMemoryAllocateInfoNv: Self = RawStructureType(1000056001);
        pub const eImportMemoryWin32HandleInfoNv: Self = RawStructureType(1000057000);
        pub const eExportMemoryWin32HandleInfoNv: Self = RawStructureType(1000057001);
        pub const eWin32KeyedMutexAcquireReleaseInfoNv: Self = RawStructureType(1000058000);
        pub const ePhysicalDeviceFeatures2: Self = RawStructureType(1000059000);
        pub const ePhysicalDeviceProperties2: Self = RawStructureType(1000059001);
        pub const eFormatProperties2: Self = RawStructureType(1000059002);
        pub const eImageFormatProperties2: Self = RawStructureType(1000059003);
        pub const ePhysicalDeviceImageFormatInfo2: Self = RawStructureType(1000059004);
        pub const eQueueFamilyProperties2: Self = RawStructureType(1000059005);
        pub const ePhysicalDeviceMemoryProperties2: Self = RawStructureType(1000059006);
        pub const eSparseImageFormatProperties2: Self = RawStructureType(1000059007);
        pub const ePhysicalDeviceSparseImageFormatInfo2: Self = RawStructureType(1000059008);
        pub const eMemoryAllocateFlagsInfo: Self = RawStructureType(1000060000);
        pub const eDeviceGroupRenderPassBeginInfo: Self = RawStructureType(1000060003);
        pub const eDeviceGroupCommandBufferBeginInfo: Self = RawStructureType(1000060004);
        pub const eDeviceGroupSubmitInfo: Self = RawStructureType(1000060005);
        pub const eDeviceGroupBindSparseInfo: Self = RawStructureType(1000060006);
        pub const eDeviceGroupPresentCapabilitiesKhr: Self = RawStructureType(1000060007);
        pub const eImageSwapchainCreateInfoKhr: Self = RawStructureType(1000060008);
        pub const eBindImageMemorySwapchainInfoKhr: Self = RawStructureType(1000060009);
        pub const eAcquireNextImageInfoKhr: Self = RawStructureType(1000060010);
        pub const eDeviceGroupPresentInfoKhr: Self = RawStructureType(1000060011);
        pub const eDeviceGroupSwapchainCreateInfoKhr: Self = RawStructureType(1000060012);
        pub const eBindBufferMemoryDeviceGroupInfo: Self = RawStructureType(1000060013);
        pub const eBindImageMemoryDeviceGroupInfo: Self = RawStructureType(1000060014);
        pub const eValidationFlagsExt: Self = RawStructureType(1000061000);
        pub const eViSurfaceCreateInfoNn: Self = RawStructureType(1000062000);
        pub const ePhysicalDeviceShaderDrawParametersFeatures: Self = RawStructureType(1000063000);
        pub const ePhysicalDeviceTextureCompressionAstcHdrFeatures: Self = RawStructureType(1000066000);
        pub const eImageViewAstcDecodeModeExt: Self = RawStructureType(1000067000);
        pub const ePhysicalDeviceAstcDecodeFeaturesExt: Self = RawStructureType(1000067001);
        pub const ePipelineRobustnessCreateInfoExt: Self = RawStructureType(1000068000);
        pub const ePhysicalDevicePipelineRobustnessFeaturesExt: Self = RawStructureType(1000068001);
        pub const ePhysicalDevicePipelineRobustnessPropertiesExt: Self = RawStructureType(1000068002);
        pub const ePhysicalDeviceGroupProperties: Self = RawStructureType(1000070000);
        pub const eDeviceGroupDeviceCreateInfo: Self = RawStructureType(1000070001);
        pub const ePhysicalDeviceExternalImageFormatInfo: Self = RawStructureType(1000071000);
        pub const eExternalImageFormatProperties: Self = RawStructureType(1000071001);
        pub const ePhysicalDeviceExternalBufferInfo: Self = RawStructureType(1000071002);
        pub const eExternalBufferProperties: Self = RawStructureType(1000071003);
        pub const ePhysicalDeviceIdProperties: Self = RawStructureType(1000071004);
        pub const eExternalMemoryBufferCreateInfo: Self = RawStructureType(1000072000);
        pub const eExternalMemoryImageCreateInfo: Self = RawStructureType(1000072001);
        pub const eExportMemoryAllocateInfo: Self = RawStructureType(1000072002);
        pub const eImportMemoryWin32HandleInfoKhr: Self = RawStructureType(1000073000);
        pub const eExportMemoryWin32HandleInfoKhr: Self = RawStructureType(1000073001);
        pub const eMemoryWin32HandlePropertiesKhr: Self = RawStructureType(1000073002);
        pub const eMemoryGetWin32HandleInfoKhr: Self = RawStructureType(1000073003);
        pub const eImportMemoryFdInfoKhr: Self = RawStructureType(1000074000);
        pub const eMemoryFdPropertiesKhr: Self = RawStructureType(1000074001);
        pub const eMemoryGetFdInfoKhr: Self = RawStructureType(1000074002);
        pub const eWin32KeyedMutexAcquireReleaseInfoKhr: Self = RawStructureType(1000075000);
        pub const ePhysicalDeviceExternalSemaphoreInfo: Self = RawStructureType(1000076000);
        pub const eExternalSemaphoreProperties: Self = RawStructureType(1000076001);
        pub const eExportSemaphoreCreateInfo: Self = RawStructureType(1000077000);
        pub const eImportSemaphoreWin32HandleInfoKhr: Self = RawStructureType(1000078000);
        pub const eExportSemaphoreWin32HandleInfoKhr: Self = RawStructureType(1000078001);
        pub const eD3D12FenceSubmitInfoKhr: Self = RawStructureType(1000078002);
        pub const eSemaphoreGetWin32HandleInfoKhr: Self = RawStructureType(1000078003);
        pub const eImportSemaphoreFdInfoKhr: Self = RawStructureType(1000079000);
        pub const eSemaphoreGetFdInfoKhr: Self = RawStructureType(1000079001);
        pub const ePhysicalDevicePushDescriptorPropertiesKhr: Self = RawStructureType(1000080000);
        pub const eCommandBufferInheritanceConditionalRenderingInfoExt: Self = RawStructureType(1000081000);
        pub const ePhysicalDeviceConditionalRenderingFeaturesExt: Self = RawStructureType(1000081001);
        pub const eConditionalRenderingBeginInfoExt: Self = RawStructureType(1000081002);
        pub const ePhysicalDeviceShaderFloat16Int8Features: Self = RawStructureType(1000082000);
        pub const ePhysicalDevice16bitStorageFeatures: Self = RawStructureType(1000083000);
        pub const ePresentRegionsKhr: Self = RawStructureType(1000084000);
        pub const eDescriptorUpdateTemplateCreateInfo: Self = RawStructureType(1000085000);
        pub const ePipelineViewportWScalingStateCreateInfoNv: Self = RawStructureType(1000087000);
        pub const eSurfaceCapabilities2Ext: Self = RawStructureType(1000090000);
        pub const eDisplayPowerInfoExt: Self = RawStructureType(1000091000);
        pub const eDeviceEventInfoExt: Self = RawStructureType(1000091001);
        pub const eDisplayEventInfoExt: Self = RawStructureType(1000091002);
        pub const eSwapchainCounterCreateInfoExt: Self = RawStructureType(1000091003);
        pub const ePresentTimesInfoGoogle: Self = RawStructureType(1000092000);
        pub const ePhysicalDeviceSubgroupProperties: Self = RawStructureType(1000094000);
        pub const ePhysicalDeviceMultiviewPerViewAttributesPropertiesNvx: Self = RawStructureType(1000097000);
        pub const ePipelineViewportSwizzleStateCreateInfoNv: Self = RawStructureType(1000098000);
        pub const ePhysicalDeviceDiscardRectanglePropertiesExt: Self = RawStructureType(1000099000);
        pub const ePipelineDiscardRectangleStateCreateInfoExt: Self = RawStructureType(1000099001);
        pub const ePhysicalDeviceConservativeRasterizationPropertiesExt: Self = RawStructureType(1000101000);
        pub const ePipelineRasterizationConservativeStateCreateInfoExt: Self = RawStructureType(1000101001);
        pub const ePhysicalDeviceDepthClipEnableFeaturesExt: Self = RawStructureType(1000102000);
        pub const ePipelineRasterizationDepthClipStateCreateInfoExt: Self = RawStructureType(1000102001);
        pub const eHdrMetadataExt: Self = RawStructureType(1000105000);
        pub const ePhysicalDeviceImagelessFramebufferFeatures: Self = RawStructureType(1000108000);
        pub const eFramebufferAttachmentsCreateInfo: Self = RawStructureType(1000108001);
        pub const eFramebufferAttachmentImageInfo: Self = RawStructureType(1000108002);
        pub const eRenderPassAttachmentBeginInfo: Self = RawStructureType(1000108003);
        pub const eAttachmentDescription2: Self = RawStructureType(1000109000);
        pub const eAttachmentReference2: Self = RawStructureType(1000109001);
        pub const eSubpassDescription2: Self = RawStructureType(1000109002);
        pub const eSubpassDependency2: Self = RawStructureType(1000109003);
        pub const eRenderPassCreateInfo2: Self = RawStructureType(1000109004);
        pub const eSubpassBeginInfo: Self = RawStructureType(1000109005);
        pub const eSubpassEndInfo: Self = RawStructureType(1000109006);
        pub const eSharedPresentSurfaceCapabilitiesKhr: Self = RawStructureType(1000111000);
        pub const ePhysicalDeviceExternalFenceInfo: Self = RawStructureType(1000112000);
        pub const eExternalFenceProperties: Self = RawStructureType(1000112001);
        pub const eExportFenceCreateInfo: Self = RawStructureType(1000113000);
        pub const eImportFenceWin32HandleInfoKhr: Self = RawStructureType(1000114000);
        pub const eExportFenceWin32HandleInfoKhr: Self = RawStructureType(1000114001);
        pub const eFenceGetWin32HandleInfoKhr: Self = RawStructureType(1000114002);
        pub const eImportFenceFdInfoKhr: Self = RawStructureType(1000115000);
        pub const eFenceGetFdInfoKhr: Self = RawStructureType(1000115001);
        pub const ePhysicalDevicePerformanceQueryFeaturesKhr: Self = RawStructureType(1000116000);
        pub const ePhysicalDevicePerformanceQueryPropertiesKhr: Self = RawStructureType(1000116001);
        pub const eQueryPoolPerformanceCreateInfoKhr: Self = RawStructureType(1000116002);
        pub const ePerformanceQuerySubmitInfoKhr: Self = RawStructureType(1000116003);
        pub const eAcquireProfilingLockInfoKhr: Self = RawStructureType(1000116004);
        pub const ePerformanceCounterKhr: Self = RawStructureType(1000116005);
        pub const ePerformanceCounterDescriptionKhr: Self = RawStructureType(1000116006);
        pub const ePerformanceQueryReservationInfoKhr: Self = RawStructureType(1000116007);
        pub const ePhysicalDevicePointClippingProperties: Self = RawStructureType(1000117000);
        pub const eRenderPassInputAttachmentAspectCreateInfo: Self = RawStructureType(1000117001);
        pub const eImageViewUsageCreateInfo: Self = RawStructureType(1000117002);
        pub const ePipelineTessellationDomainOriginStateCreateInfo: Self = RawStructureType(1000117003);
        pub const ePhysicalDeviceSurfaceInfo2Khr: Self = RawStructureType(1000119000);
        pub const eSurfaceCapabilities2Khr: Self = RawStructureType(1000119001);
        pub const eSurfaceFormat2Khr: Self = RawStructureType(1000119002);
        pub const ePhysicalDeviceVariablePointersFeatures: Self = RawStructureType(1000120000);
        pub const eDisplayProperties2Khr: Self = RawStructureType(1000121000);
        pub const eDisplayPlaneProperties2Khr: Self = RawStructureType(1000121001);
        pub const eDisplayModeProperties2Khr: Self = RawStructureType(1000121002);
        pub const eDisplayPlaneInfo2Khr: Self = RawStructureType(1000121003);
        pub const eDisplayPlaneCapabilities2Khr: Self = RawStructureType(1000121004);
        pub const eIosSurfaceCreateInfoMvk: Self = RawStructureType(1000122000);
        pub const eMacosSurfaceCreateInfoMvk: Self = RawStructureType(1000123000);
        pub const eMemoryDedicatedRequirements: Self = RawStructureType(1000127000);
        pub const eMemoryDedicatedAllocateInfo: Self = RawStructureType(1000127001);
        pub const eDebugUtilsObjectNameInfoExt: Self = RawStructureType(1000128000);
        pub const eDebugUtilsObjectTagInfoExt: Self = RawStructureType(1000128001);
        pub const eDebugUtilsLabelExt: Self = RawStructureType(1000128002);
        pub const eDebugUtilsMessengerCallbackDataExt: Self = RawStructureType(1000128003);
        pub const eDebugUtilsMessengerCreateInfoExt: Self = RawStructureType(1000128004);
        pub const eAndroidHardwareBufferUsageAndroid: Self = RawStructureType(1000129000);
        pub const eAndroidHardwareBufferPropertiesAndroid: Self = RawStructureType(1000129001);
        pub const eAndroidHardwareBufferFormatPropertiesAndroid: Self = RawStructureType(1000129002);
        pub const eImportAndroidHardwareBufferInfoAndroid: Self = RawStructureType(1000129003);
        pub const eMemoryGetAndroidHardwareBufferInfoAndroid: Self = RawStructureType(1000129004);
        pub const eExternalFormatAndroid: Self = RawStructureType(1000129005);
        pub const eAndroidHardwareBufferFormatProperties2Android: Self = RawStructureType(1000129006);
        pub const ePhysicalDeviceSamplerFilterMinmaxProperties: Self = RawStructureType(1000130000);
        pub const eSamplerReductionModeCreateInfo: Self = RawStructureType(1000130001);
        pub const ePhysicalDeviceInlineUniformBlockFeatures: Self = RawStructureType(1000138000);
        pub const ePhysicalDeviceInlineUniformBlockProperties: Self = RawStructureType(1000138001);
        pub const eWriteDescriptorSetInlineUniformBlock: Self = RawStructureType(1000138002);
        pub const eDescriptorPoolInlineUniformBlockCreateInfo: Self = RawStructureType(1000138003);
        pub const eSampleLocationsInfoExt: Self = RawStructureType(1000143000);
        pub const eRenderPassSampleLocationsBeginInfoExt: Self = RawStructureType(1000143001);
        pub const ePipelineSampleLocationsStateCreateInfoExt: Self = RawStructureType(1000143002);
        pub const ePhysicalDeviceSampleLocationsPropertiesExt: Self = RawStructureType(1000143003);
        pub const eMultisamplePropertiesExt: Self = RawStructureType(1000143004);
        pub const eProtectedSubmitInfo: Self = RawStructureType(1000145000);
        pub const ePhysicalDeviceProtectedMemoryFeatures: Self = RawStructureType(1000145001);
        pub const ePhysicalDeviceProtectedMemoryProperties: Self = RawStructureType(1000145002);
        pub const eDeviceQueueInfo2: Self = RawStructureType(1000145003);
        pub const eBufferMemoryRequirementsInfo2: Self = RawStructureType(1000146000);
        pub const eImageMemoryRequirementsInfo2: Self = RawStructureType(1000146001);
        pub const eImageSparseMemoryRequirementsInfo2: Self = RawStructureType(1000146002);
        pub const eMemoryRequirements2: Self = RawStructureType(1000146003);
        pub const eSparseImageMemoryRequirements2: Self = RawStructureType(1000146004);
        pub const eImageFormatListCreateInfo: Self = RawStructureType(1000147000);
        pub const ePhysicalDeviceBlendOperationAdvancedFeaturesExt: Self = RawStructureType(1000148000);
        pub const ePhysicalDeviceBlendOperationAdvancedPropertiesExt: Self = RawStructureType(1000148001);
        pub const ePipelineColourBlendAdvancedStateCreateInfoExt: Self = RawStructureType(1000148002);
        pub const ePipelineCoverageToColourStateCreateInfoNv: Self = RawStructureType(1000149000);
        pub const eAccelerationStructureBuildGeometryInfoKhr: Self = RawStructureType(1000150000);
        pub const eAccelerationStructureDeviceAddressInfoKhr: Self = RawStructureType(1000150002);
        pub const eAccelerationStructureGeometryAabbsDataKhr: Self = RawStructureType(1000150003);
        pub const eAccelerationStructureGeometryInstancesDataKhr: Self = RawStructureType(1000150004);
        pub const eAccelerationStructureGeometryTrianglesDataKhr: Self = RawStructureType(1000150005);
        pub const eAccelerationStructureGeometryKhr: Self = RawStructureType(1000150006);
        pub const eWriteDescriptorSetAccelerationStructureKhr: Self = RawStructureType(1000150007);
        pub const eAccelerationStructureVersionInfoKhr: Self = RawStructureType(1000150009);
        pub const eCopyAccelerationStructureInfoKhr: Self = RawStructureType(1000150010);
        pub const eCopyAccelerationStructureToMemoryInfoKhr: Self = RawStructureType(1000150011);
        pub const eCopyMemoryToAccelerationStructureInfoKhr: Self = RawStructureType(1000150012);
        pub const ePhysicalDeviceAccelerationStructureFeaturesKhr: Self = RawStructureType(1000150013);
        pub const ePhysicalDeviceAccelerationStructurePropertiesKhr: Self = RawStructureType(1000150014);
        pub const eRayTracingPipelineCreateInfoKhr: Self = RawStructureType(1000150015);
        pub const eRayTracingShaderGroupCreateInfoKhr: Self = RawStructureType(1000150016);
        pub const eAccelerationStructureCreateInfoKhr: Self = RawStructureType(1000150017);
        pub const eRayTracingPipelineInterfaceCreateInfoKhr: Self = RawStructureType(1000150018);
        pub const eAccelerationStructureBuildSizesInfoKhr: Self = RawStructureType(1000150020);
        pub const ePipelineCoverageModulationStateCreateInfoNv: Self = RawStructureType(1000152000);
        pub const ePhysicalDeviceShaderSmBuiltinsFeaturesNv: Self = RawStructureType(1000154000);
        pub const ePhysicalDeviceShaderSmBuiltinsPropertiesNv: Self = RawStructureType(1000154001);
        pub const eSamplerYcbcrConversionCreateInfo: Self = RawStructureType(1000156000);
        pub const eSamplerYcbcrConversionInfo: Self = RawStructureType(1000156001);
        pub const eBindImagePlaneMemoryInfo: Self = RawStructureType(1000156002);
        pub const eImagePlaneMemoryRequirementsInfo: Self = RawStructureType(1000156003);
        pub const ePhysicalDeviceSamplerYcbcrConversionFeatures: Self = RawStructureType(1000156004);
        pub const eSamplerYcbcrConversionImageFormatProperties: Self = RawStructureType(1000156005);
        pub const eBindBufferMemoryInfo: Self = RawStructureType(1000157000);
        pub const eBindImageMemoryInfo: Self = RawStructureType(1000157001);
        pub const eDrmFormatModifierPropertiesListExt: Self = RawStructureType(1000158000);
        pub const ePhysicalDeviceImageDrmFormatModifierInfoExt: Self = RawStructureType(1000158002);
        pub const eImageDrmFormatModifierListCreateInfoExt: Self = RawStructureType(1000158003);
        pub const eImageDrmFormatModifierExplicitCreateInfoExt: Self = RawStructureType(1000158004);
        pub const eImageDrmFormatModifierPropertiesExt: Self = RawStructureType(1000158005);
        pub const eDrmFormatModifierPropertiesList2Ext: Self = RawStructureType(1000158006);
        pub const eValidationCacheCreateInfoExt: Self = RawStructureType(1000160000);
        pub const eShaderModuleValidationCacheCreateInfoExt: Self = RawStructureType(1000160001);
        pub const eDescriptorSetLayoutBindingFlagsCreateInfo: Self = RawStructureType(1000161000);
        pub const ePhysicalDeviceDescriptorIndexingFeatures: Self = RawStructureType(1000161001);
        pub const ePhysicalDeviceDescriptorIndexingProperties: Self = RawStructureType(1000161002);
        pub const eDescriptorSetVariableDescriptorCountAllocateInfo: Self = RawStructureType(1000161003);
        pub const eDescriptorSetVariableDescriptorCountLayoutSupport: Self = RawStructureType(1000161004);
        pub const ePhysicalDevicePortabilitySubsetFeaturesKhr: Self = RawStructureType(1000163000);
        pub const ePhysicalDevicePortabilitySubsetPropertiesKhr: Self = RawStructureType(1000163001);
        pub const ePipelineViewportShadingRateImageStateCreateInfoNv: Self = RawStructureType(1000164000);
        pub const ePhysicalDeviceShadingRateImageFeaturesNv: Self = RawStructureType(1000164001);
        pub const ePhysicalDeviceShadingRateImagePropertiesNv: Self = RawStructureType(1000164002);
        pub const ePipelineViewportCoarseSampleOrderStateCreateInfoNv: Self = RawStructureType(1000164005);
        pub const eRayTracingPipelineCreateInfoNv: Self = RawStructureType(1000165000);
        pub const eAccelerationStructureCreateInfoNv: Self = RawStructureType(1000165001);
        pub const eGeometryNv: Self = RawStructureType(1000165003);
        pub const eGeometryTrianglesNv: Self = RawStructureType(1000165004);
        pub const eGeometryAabbNv: Self = RawStructureType(1000165005);
        pub const eBindAccelerationStructureMemoryInfoNv: Self = RawStructureType(1000165006);
        pub const eWriteDescriptorSetAccelerationStructureNv: Self = RawStructureType(1000165007);
        pub const eAccelerationStructureMemoryRequirementsInfoNv: Self = RawStructureType(1000165008);
        pub const ePhysicalDeviceRayTracingPropertiesNv: Self = RawStructureType(1000165009);
        pub const eRayTracingShaderGroupCreateInfoNv: Self = RawStructureType(1000165011);
        pub const eAccelerationStructureInfoNv: Self = RawStructureType(1000165012);
        pub const ePhysicalDeviceRepresentativeFragmentTestFeaturesNv: Self = RawStructureType(1000166000);
        pub const ePipelineRepresentativeFragmentTestStateCreateInfoNv: Self = RawStructureType(1000166001);
        pub const ePhysicalDeviceMaintenance3Properties: Self = RawStructureType(1000168000);
        pub const eDescriptorSetLayoutSupport: Self = RawStructureType(1000168001);
        pub const ePhysicalDeviceImageViewImageFormatInfoExt: Self = RawStructureType(1000170000);
        pub const eFilterCubicImageViewImageFormatPropertiesExt: Self = RawStructureType(1000170001);
        pub const eDeviceQueueGlobalPriorityCreateInfoKhr: Self = RawStructureType(1000174000);
        pub const ePhysicalDeviceShaderSubgroupExtendedTypesFeatures: Self = RawStructureType(1000175000);
        pub const ePhysicalDevice8bitStorageFeatures: Self = RawStructureType(1000177000);
        pub const eImportMemoryHostPointerInfoExt: Self = RawStructureType(1000178000);
        pub const eMemoryHostPointerPropertiesExt: Self = RawStructureType(1000178001);
        pub const ePhysicalDeviceExternalMemoryHostPropertiesExt: Self = RawStructureType(1000178002);
        pub const ePhysicalDeviceShaderAtomicInt64Features: Self = RawStructureType(1000180000);
        pub const ePhysicalDeviceShaderClockFeaturesKhr: Self = RawStructureType(1000181000);
        pub const ePipelineCompilerControlCreateInfoAmd: Self = RawStructureType(1000183000);
        pub const eCalibratedTimestampInfoExt: Self = RawStructureType(1000184000);
        pub const ePhysicalDeviceShaderCorePropertiesAmd: Self = RawStructureType(1000185000);
        pub const eVideoDecodeH265CapabilitiesKhr: Self = RawStructureType(1000187000);
        pub const eVideoDecodeH265SessionParametersCreateInfoKhr: Self = RawStructureType(1000187001);
        pub const eVideoDecodeH265SessionParametersAddInfoKhr: Self = RawStructureType(1000187002);
        pub const eVideoDecodeH265ProfileInfoKhr: Self = RawStructureType(1000187003);
        pub const eVideoDecodeH265PictureInfoKhr: Self = RawStructureType(1000187004);
        pub const eVideoDecodeH265DpbSlotInfoKhr: Self = RawStructureType(1000187005);
        pub const eDeviceMemoryOverallocationCreateInfoAmd: Self = RawStructureType(1000189000);
        pub const ePhysicalDeviceVertexAttributeDivisorPropertiesExt: Self = RawStructureType(1000190000);
        pub const ePipelineVertexInputDivisorStateCreateInfoExt: Self = RawStructureType(1000190001);
        pub const ePhysicalDeviceVertexAttributeDivisorFeaturesExt: Self = RawStructureType(1000190002);
        pub const ePresentFrameTokenGgp: Self = RawStructureType(1000191000);
        pub const ePipelineCreationFeedbackCreateInfo: Self = RawStructureType(1000192000);
        pub const ePhysicalDeviceDriverProperties: Self = RawStructureType(1000196000);
        pub const ePhysicalDeviceFloatControlsProperties: Self = RawStructureType(1000197000);
        pub const ePhysicalDeviceDepthStencilResolveProperties: Self = RawStructureType(1000199000);
        pub const eSubpassDescriptionDepthStencilResolve: Self = RawStructureType(1000199001);
        pub const ePhysicalDeviceComputeShaderDerivativesFeaturesNv: Self = RawStructureType(1000201000);
        pub const ePhysicalDeviceMeshShaderFeaturesNv: Self = RawStructureType(1000202000);
        pub const ePhysicalDeviceMeshShaderPropertiesNv: Self = RawStructureType(1000202001);
        pub const ePhysicalDeviceFragmentShaderBarycentricFeaturesKhr: Self = RawStructureType(1000203000);
        pub const ePhysicalDeviceShaderImageFootprintFeaturesNv: Self = RawStructureType(1000204000);
        pub const ePipelineViewportExclusiveScissorStateCreateInfoNv: Self = RawStructureType(1000205000);
        pub const ePhysicalDeviceExclusiveScissorFeaturesNv: Self = RawStructureType(1000205002);
        pub const eCheckpointDataNv: Self = RawStructureType(1000206000);
        pub const eQueueFamilyCheckpointPropertiesNv: Self = RawStructureType(1000206001);
        pub const ePhysicalDeviceTimelineSemaphoreFeatures: Self = RawStructureType(1000207000);
        pub const ePhysicalDeviceTimelineSemaphoreProperties: Self = RawStructureType(1000207001);
        pub const eSemaphoreTypeCreateInfo: Self = RawStructureType(1000207002);
        pub const eTimelineSemaphoreSubmitInfo: Self = RawStructureType(1000207003);
        pub const eSemaphoreWaitInfo: Self = RawStructureType(1000207004);
        pub const eSemaphoreSignalInfo: Self = RawStructureType(1000207005);
        pub const ePhysicalDeviceShaderIntegerFunctions2FeaturesIntel: Self = RawStructureType(1000209000);
        pub const eQueryPoolPerformanceQueryCreateInfoIntel: Self = RawStructureType(1000210000);
        pub const eInitializePerformanceApiInfoIntel: Self = RawStructureType(1000210001);
        pub const ePerformanceMarkerInfoIntel: Self = RawStructureType(1000210002);
        pub const ePerformanceStreamMarkerInfoIntel: Self = RawStructureType(1000210003);
        pub const ePerformanceOverrideInfoIntel: Self = RawStructureType(1000210004);
        pub const ePerformanceConfigurationAcquireInfoIntel: Self = RawStructureType(1000210005);
        pub const ePhysicalDeviceVulkanMemoryModelFeatures: Self = RawStructureType(1000211000);
        pub const ePhysicalDevicePciBusInfoPropertiesExt: Self = RawStructureType(1000212000);
        pub const eDisplayNativeHdrSurfaceCapabilitiesAmd: Self = RawStructureType(1000213000);
        pub const eSwapchainDisplayNativeHdrCreateInfoAmd: Self = RawStructureType(1000213001);
        pub const eImagepipeSurfaceCreateInfoFuchsia: Self = RawStructureType(1000214000);
        pub const ePhysicalDeviceShaderTerminateInvocationFeatures: Self = RawStructureType(1000215000);
        pub const eMetalSurfaceCreateInfoExt: Self = RawStructureType(1000217000);
        pub const ePhysicalDeviceFragmentDensityMapFeaturesExt: Self = RawStructureType(1000218000);
        pub const ePhysicalDeviceFragmentDensityMapPropertiesExt: Self = RawStructureType(1000218001);
        pub const eRenderPassFragmentDensityMapCreateInfoExt: Self = RawStructureType(1000218002);
        pub const ePhysicalDeviceScalarBlockLayoutFeatures: Self = RawStructureType(1000221000);
        pub const ePhysicalDeviceSubgroupSizeControlProperties: Self = RawStructureType(1000225000);
        pub const ePipelineShaderStageRequiredSubgroupSizeCreateInfo: Self = RawStructureType(1000225001);
        pub const ePhysicalDeviceSubgroupSizeControlFeatures: Self = RawStructureType(1000225002);
        pub const eFragmentShadingRateAttachmentInfoKhr: Self = RawStructureType(1000226000);
        pub const ePipelineFragmentShadingRateStateCreateInfoKhr: Self = RawStructureType(1000226001);
        pub const ePhysicalDeviceFragmentShadingRatePropertiesKhr: Self = RawStructureType(1000226002);
        pub const ePhysicalDeviceFragmentShadingRateFeaturesKhr: Self = RawStructureType(1000226003);
        pub const ePhysicalDeviceFragmentShadingRateKhr: Self = RawStructureType(1000226004);
        pub const ePhysicalDeviceShaderCoreProperties2Amd: Self = RawStructureType(1000227000);
        pub const ePhysicalDeviceCoherentMemoryFeaturesAmd: Self = RawStructureType(1000229000);
        pub const ePhysicalDeviceShaderImageAtomicInt64FeaturesExt: Self = RawStructureType(1000234000);
        pub const ePhysicalDeviceMemoryBudgetPropertiesExt: Self = RawStructureType(1000237000);
        pub const ePhysicalDeviceMemoryPriorityFeaturesExt: Self = RawStructureType(1000238000);
        pub const eMemoryPriorityAllocateInfoExt: Self = RawStructureType(1000238001);
        pub const eSurfaceProtectedCapabilitiesKhr: Self = RawStructureType(1000239000);
        pub const ePhysicalDeviceDedicatedAllocationImageAliasingFeaturesNv: Self = RawStructureType(1000240000);
        pub const ePhysicalDeviceSeparateDepthStencilLayoutsFeatures: Self = RawStructureType(1000241000);
        pub const eAttachmentReferenceStencilLayout: Self = RawStructureType(1000241001);
        pub const eAttachmentDescriptionStencilLayout: Self = RawStructureType(1000241002);
        pub const ePhysicalDeviceBufferDeviceAddressFeaturesExt: Self = RawStructureType(1000244000);
        pub const eBufferDeviceAddressInfo: Self = RawStructureType(1000244001);
        pub const eBufferDeviceAddressCreateInfoExt: Self = RawStructureType(1000244002);
        pub const ePhysicalDeviceToolProperties: Self = RawStructureType(1000245000);
        pub const eImageStencilUsageCreateInfo: Self = RawStructureType(1000246000);
        pub const eValidationFeaturesExt: Self = RawStructureType(1000247000);
        pub const ePhysicalDevicePresentWaitFeaturesKhr: Self = RawStructureType(1000248000);
        pub const ePhysicalDeviceCooperativeMatrixFeaturesNv: Self = RawStructureType(1000249000);
        pub const eCooperativeMatrixPropertiesNv: Self = RawStructureType(1000249001);
        pub const ePhysicalDeviceCooperativeMatrixPropertiesNv: Self = RawStructureType(1000249002);
        pub const ePhysicalDeviceCoverageReductionModeFeaturesNv: Self = RawStructureType(1000250000);
        pub const ePipelineCoverageReductionStateCreateInfoNv: Self = RawStructureType(1000250001);
        pub const eFramebufferMixedSamplesCombinationNv: Self = RawStructureType(1000250002);
        pub const ePhysicalDeviceFragmentShaderInterlockFeaturesExt: Self = RawStructureType(1000251000);
        pub const ePhysicalDeviceYcbcrImageArraysFeaturesExt: Self = RawStructureType(1000252000);
        pub const ePhysicalDeviceUniformBufferStandardLayoutFeatures: Self = RawStructureType(1000253000);
        pub const ePhysicalDeviceProvokingVertexFeaturesExt: Self = RawStructureType(1000254000);
        pub const ePipelineRasterizationProvokingVertexStateCreateInfoExt: Self = RawStructureType(1000254001);
        pub const ePhysicalDeviceProvokingVertexPropertiesExt: Self = RawStructureType(1000254002);
        pub const eSurfaceFullScreenExclusiveInfoExt: Self = RawStructureType(1000255000);
        pub const eSurfaceFullScreenExclusiveWin32InfoExt: Self = RawStructureType(1000255001);
        pub const eSurfaceCapabilitiesFullScreenExclusiveExt: Self = RawStructureType(1000255002);
        pub const eHeadlessSurfaceCreateInfoExt: Self = RawStructureType(1000256000);
        pub const ePhysicalDeviceBufferDeviceAddressFeatures: Self = RawStructureType(1000257000);
        pub const eBufferOpaqueCaptureAddressCreateInfo: Self = RawStructureType(1000257002);
        pub const eMemoryOpaqueCaptureAddressAllocateInfo: Self = RawStructureType(1000257003);
        pub const eDeviceMemoryOpaqueCaptureAddressInfo: Self = RawStructureType(1000257004);
        pub const ePhysicalDeviceLineRasterizationFeaturesExt: Self = RawStructureType(1000259000);
        pub const ePipelineRasterizationLineStateCreateInfoExt: Self = RawStructureType(1000259001);
        pub const ePhysicalDeviceLineRasterizationPropertiesExt: Self = RawStructureType(1000259002);
        pub const ePhysicalDeviceShaderAtomicFloatFeaturesExt: Self = RawStructureType(1000260000);
        pub const ePhysicalDeviceHostQueryResetFeatures: Self = RawStructureType(1000261000);
        pub const ePhysicalDeviceIndexTypeUint8FeaturesExt: Self = RawStructureType(1000265000);
        pub const ePhysicalDeviceExtendedDynamicStateFeaturesExt: Self = RawStructureType(1000267000);
        pub const ePhysicalDevicePipelineExecutablePropertiesFeaturesKhr: Self = RawStructureType(1000269000);
        pub const ePipelineInfoKhr: Self = RawStructureType(1000269001);
        pub const ePipelineExecutablePropertiesKhr: Self = RawStructureType(1000269002);
        pub const ePipelineExecutableInfoKhr: Self = RawStructureType(1000269003);
        pub const ePipelineExecutableStatisticKhr: Self = RawStructureType(1000269004);
        pub const ePipelineExecutableInternalRepresentationKhr: Self = RawStructureType(1000269005);
        pub const ePhysicalDeviceShaderAtomicFloat2FeaturesExt: Self = RawStructureType(1000273000);
        pub const eSurfacePresentModeExt: Self = RawStructureType(1000274000);
        pub const eSurfacePresentScalingCapabilitiesExt: Self = RawStructureType(1000274001);
        pub const eSurfacePresentModeCompatibilityExt: Self = RawStructureType(1000274002);
        pub const ePhysicalDeviceSwapchainMaintenance1FeaturesExt: Self = RawStructureType(1000275000);
        pub const eSwapchainPresentFenceInfoExt: Self = RawStructureType(1000275001);
        pub const eSwapchainPresentModesCreateInfoExt: Self = RawStructureType(1000275002);
        pub const eSwapchainPresentModeInfoExt: Self = RawStructureType(1000275003);
        pub const eSwapchainPresentScalingCreateInfoExt: Self = RawStructureType(1000275004);
        pub const eReleaseSwapchainImagesInfoExt: Self = RawStructureType(1000275005);
        pub const ePhysicalDeviceShaderDemoteToHelperInvocationFeatures: Self = RawStructureType(1000276000);
        pub const ePhysicalDeviceDeviceGeneratedCommandsPropertiesNv: Self = RawStructureType(1000277000);
        pub const eGraphicsShaderGroupCreateInfoNv: Self = RawStructureType(1000277001);
        pub const eGraphicsPipelineShaderGroupsCreateInfoNv: Self = RawStructureType(1000277002);
        pub const eIndirectCommandsLayoutTokenNv: Self = RawStructureType(1000277003);
        pub const eIndirectCommandsLayoutCreateInfoNv: Self = RawStructureType(1000277004);
        pub const eGeneratedCommandsInfoNv: Self = RawStructureType(1000277005);
        pub const eGeneratedCommandsMemoryRequirementsInfoNv: Self = RawStructureType(1000277006);
        pub const ePhysicalDeviceDeviceGeneratedCommandsFeaturesNv: Self = RawStructureType(1000277007);
        pub const ePhysicalDeviceInheritedViewportScissorFeaturesNv: Self = RawStructureType(1000278000);
        pub const eCommandBufferInheritanceViewportScissorInfoNv: Self = RawStructureType(1000278001);
        pub const ePhysicalDeviceShaderIntegerDotProductFeatures: Self = RawStructureType(1000280000);
        pub const ePhysicalDeviceShaderIntegerDotProductProperties: Self = RawStructureType(1000280001);
        pub const ePhysicalDeviceTexelBufferAlignmentFeaturesExt: Self = RawStructureType(1000281000);
        pub const ePhysicalDeviceTexelBufferAlignmentProperties: Self = RawStructureType(1000281001);
        pub const eCommandBufferInheritanceRenderPassTransformInfoQcom: Self = RawStructureType(1000282000);
        pub const eRenderPassTransformBeginInfoQcom: Self = RawStructureType(1000282001);
        pub const ePhysicalDeviceDeviceMemoryReportFeaturesExt: Self = RawStructureType(1000284000);
        pub const eDeviceDeviceMemoryReportCreateInfoExt: Self = RawStructureType(1000284001);
        pub const eDeviceMemoryReportCallbackDataExt: Self = RawStructureType(1000284002);
        pub const ePhysicalDeviceRobustness2FeaturesExt: Self = RawStructureType(1000286000);
        pub const ePhysicalDeviceRobustness2PropertiesExt: Self = RawStructureType(1000286001);
        pub const eSamplerCustomBorderColourCreateInfoExt: Self = RawStructureType(1000287000);
        pub const ePhysicalDeviceCustomBorderColourPropertiesExt: Self = RawStructureType(1000287001);
        pub const ePhysicalDeviceCustomBorderColourFeaturesExt: Self = RawStructureType(1000287002);
        pub const ePipelineLibraryCreateInfoKhr: Self = RawStructureType(1000290000);
        pub const ePhysicalDevicePresentBarrierFeaturesNv: Self = RawStructureType(1000292000);
        pub const eSurfaceCapabilitiesPresentBarrierNv: Self = RawStructureType(1000292001);
        pub const eSwapchainPresentBarrierCreateInfoNv: Self = RawStructureType(1000292002);
        pub const ePresentIdKhr: Self = RawStructureType(1000294000);
        pub const ePhysicalDevicePresentIdFeaturesKhr: Self = RawStructureType(1000294001);
        pub const ePhysicalDevicePrivateDataFeatures: Self = RawStructureType(1000295000);
        pub const eDevicePrivateDataCreateInfo: Self = RawStructureType(1000295001);
        pub const ePrivateDataSlotCreateInfo: Self = RawStructureType(1000295002);
        pub const ePhysicalDevicePipelineCreationCacheControlFeatures: Self = RawStructureType(1000297000);
        pub const ePhysicalDeviceVulkanSc10Features: Self = RawStructureType(1000298000);
        pub const ePhysicalDeviceVulkanSc10Properties: Self = RawStructureType(1000298001);
        pub const eDeviceObjectReservationCreateInfo: Self = RawStructureType(1000298002);
        pub const eCommandPoolMemoryReservationCreateInfo: Self = RawStructureType(1000298003);
        pub const eCommandPoolMemoryConsumption: Self = RawStructureType(1000298004);
        pub const ePipelinePoolSize: Self = RawStructureType(1000298005);
        pub const eFaultData: Self = RawStructureType(1000298007);
        pub const eFaultCallbackInfo: Self = RawStructureType(1000298008);
        pub const ePipelineOfflineCreateInfo: Self = RawStructureType(1000298010);
        pub const eVideoEncodeInfoKhr: Self = RawStructureType(1000299000);
        pub const eVideoEncodeRateControlInfoKhr: Self = RawStructureType(1000299001);
        pub const eVideoEncodeRateControlLayerInfoKhr: Self = RawStructureType(1000299002);
        pub const eVideoEncodeCapabilitiesKhr: Self = RawStructureType(1000299003);
        pub const eVideoEncodeUsageInfoKhr: Self = RawStructureType(1000299004);
        pub const ePhysicalDeviceDiagnosticsConfigFeaturesNv: Self = RawStructureType(1000300000);
        pub const eDeviceDiagnosticsConfigCreateInfoNv: Self = RawStructureType(1000300001);
        pub const eRefreshObjectListKhr: Self = RawStructureType(1000308000);
        pub const eQueryLowLatencySupportNv: Self = RawStructureType(1000310000);
        pub const eExportMetalObjectCreateInfoExt: Self = RawStructureType(1000311000);
        pub const eExportMetalObjectsInfoExt: Self = RawStructureType(1000311001);
        pub const eExportMetalDeviceInfoExt: Self = RawStructureType(1000311002);
        pub const eExportMetalCommandQueueInfoExt: Self = RawStructureType(1000311003);
        pub const eExportMetalBufferInfoExt: Self = RawStructureType(1000311004);
        pub const eImportMetalBufferInfoExt: Self = RawStructureType(1000311005);
        pub const eExportMetalTextureInfoExt: Self = RawStructureType(1000311006);
        pub const eImportMetalTextureInfoExt: Self = RawStructureType(1000311007);
        pub const eExportMetalIoSurfaceInfoExt: Self = RawStructureType(1000311008);
        pub const eImportMetalIoSurfaceInfoExt: Self = RawStructureType(1000311009);
        pub const eExportMetalSharedEventInfoExt: Self = RawStructureType(1000311010);
        pub const eImportMetalSharedEventInfoExt: Self = RawStructureType(1000311011);
        pub const eMemoryBarrier2: Self = RawStructureType(1000314000);
        pub const eBufferMemoryBarrier2: Self = RawStructureType(1000314001);
        pub const eImageMemoryBarrier2: Self = RawStructureType(1000314002);
        pub const eDependencyInfo: Self = RawStructureType(1000314003);
        pub const eSubmitInfo2: Self = RawStructureType(1000314004);
        pub const eSemaphoreSubmitInfo: Self = RawStructureType(1000314005);
        pub const eCommandBufferSubmitInfo: Self = RawStructureType(1000314006);
        pub const ePhysicalDeviceSynchronization2Features: Self = RawStructureType(1000314007);
        pub const eQueueFamilyCheckpointProperties2Nv: Self = RawStructureType(1000314008);
        pub const eCheckpointData2Nv: Self = RawStructureType(1000314009);
        pub const ePhysicalDeviceDescriptorBufferPropertiesExt: Self = RawStructureType(1000316000);
        pub const ePhysicalDeviceDescriptorBufferDensityMapPropertiesExt: Self = RawStructureType(1000316001);
        pub const ePhysicalDeviceDescriptorBufferFeaturesExt: Self = RawStructureType(1000316002);
        pub const eDescriptorAddressInfoExt: Self = RawStructureType(1000316003);
        pub const eDescriptorGetInfoExt: Self = RawStructureType(1000316004);
        pub const eBufferCaptureDescriptorDataInfoExt: Self = RawStructureType(1000316005);
        pub const eImageCaptureDescriptorDataInfoExt: Self = RawStructureType(1000316006);
        pub const eImageViewCaptureDescriptorDataInfoExt: Self = RawStructureType(1000316007);
        pub const eSamplerCaptureDescriptorDataInfoExt: Self = RawStructureType(1000316008);
        pub const eAccelerationStructureCaptureDescriptorDataInfoExt: Self = RawStructureType(1000316009);
        pub const eOpaqueCaptureDescriptorDataCreateInfoExt: Self = RawStructureType(1000316010);
        pub const eDescriptorBufferBindingInfoExt: Self = RawStructureType(1000316011);
        pub const eDescriptorBufferBindingPushDescriptorBufferHandleExt: Self = RawStructureType(1000316012);
        pub const ePhysicalDeviceGraphicsPipelineLibraryFeaturesExt: Self = RawStructureType(1000320000);
        pub const ePhysicalDeviceGraphicsPipelineLibraryPropertiesExt: Self = RawStructureType(1000320001);
        pub const eGraphicsPipelineLibraryCreateInfoExt: Self = RawStructureType(1000320002);
        pub const ePhysicalDeviceShaderEarlyAndLateFragmentTestsFeaturesAmd: Self = RawStructureType(1000321000);
        pub const ePhysicalDeviceFragmentShaderBarycentricPropertiesKhr: Self = RawStructureType(1000322000);
        pub const ePhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKhr: Self = RawStructureType(1000323000);
        pub const ePhysicalDeviceZeroInitializeWorkgroupMemoryFeatures: Self = RawStructureType(1000325000);
        pub const ePhysicalDeviceFragmentShadingRateEnumsPropertiesNv: Self = RawStructureType(1000326000);
        pub const ePhysicalDeviceFragmentShadingRateEnumsFeaturesNv: Self = RawStructureType(1000326001);
        pub const ePipelineFragmentShadingRateEnumStateCreateInfoNv: Self = RawStructureType(1000326002);
        pub const eAccelerationStructureGeometryMotionTrianglesDataNv: Self = RawStructureType(1000327000);
        pub const ePhysicalDeviceRayTracingMotionBlurFeaturesNv: Self = RawStructureType(1000327001);
        pub const eAccelerationStructureMotionInfoNv: Self = RawStructureType(1000327002);
        pub const ePhysicalDeviceMeshShaderFeaturesExt: Self = RawStructureType(1000328000);
        pub const ePhysicalDeviceMeshShaderPropertiesExt: Self = RawStructureType(1000328001);
        pub const ePhysicalDeviceYcbcr2Plane444FormatsFeaturesExt: Self = RawStructureType(1000330000);
        pub const ePhysicalDeviceFragmentDensityMap2FeaturesExt: Self = RawStructureType(1000332000);
        pub const ePhysicalDeviceFragmentDensityMap2PropertiesExt: Self = RawStructureType(1000332001);
        pub const eCopyCommandTransformInfoQcom: Self = RawStructureType(1000333000);
        pub const ePhysicalDeviceImageRobustnessFeatures: Self = RawStructureType(1000335000);
        pub const ePhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKhr: Self = RawStructureType(1000336000);
        pub const eCopyBufferInfo2: Self = RawStructureType(1000337000);
        pub const eCopyImageInfo2: Self = RawStructureType(1000337001);
        pub const eCopyBufferToImageInfo2: Self = RawStructureType(1000337002);
        pub const eCopyImageToBufferInfo2: Self = RawStructureType(1000337003);
        pub const eBlitImageInfo2: Self = RawStructureType(1000337004);
        pub const eResolveImageInfo2: Self = RawStructureType(1000337005);
        pub const eBufferCopy2: Self = RawStructureType(1000337006);
        pub const eImageCopy2: Self = RawStructureType(1000337007);
        pub const eImageBlit2: Self = RawStructureType(1000337008);
        pub const eBufferImageCopy2: Self = RawStructureType(1000337009);
        pub const eImageResolve2: Self = RawStructureType(1000337010);
        pub const ePhysicalDeviceImageCompressionControlFeaturesExt: Self = RawStructureType(1000338000);
        pub const eImageCompressionControlExt: Self = RawStructureType(1000338001);
        pub const eSubresourceLayout2Ext: Self = RawStructureType(1000338002);
        pub const eImageSubresource2Ext: Self = RawStructureType(1000338003);
        pub const eImageCompressionPropertiesExt: Self = RawStructureType(1000338004);
        pub const ePhysicalDeviceAttachmentFeedbackLoopLayoutFeaturesExt: Self = RawStructureType(1000339000);
        pub const ePhysicalDevice4444FormatsFeaturesExt: Self = RawStructureType(1000340000);
        pub const ePhysicalDeviceFaultFeaturesExt: Self = RawStructureType(1000341000);
        pub const eDeviceFaultCountsExt: Self = RawStructureType(1000341001);
        pub const eDeviceFaultInfoExt: Self = RawStructureType(1000341002);
        pub const ePhysicalDeviceRasterizationOrderAttachmentAccessFeaturesExt: Self = RawStructureType(1000342000);
        pub const ePhysicalDeviceRgba10X6FormatsFeaturesExt: Self = RawStructureType(1000344000);
        pub const eDirectfbSurfaceCreateInfoExt: Self = RawStructureType(1000346000);
        pub const ePhysicalDeviceRayTracingPipelineFeaturesKhr: Self = RawStructureType(1000347000);
        pub const ePhysicalDeviceRayTracingPipelinePropertiesKhr: Self = RawStructureType(1000347001);
        pub const ePhysicalDeviceRayQueryFeaturesKhr: Self = RawStructureType(1000348013);
        pub const ePhysicalDeviceMutableDescriptorTypeFeaturesExt: Self = RawStructureType(1000351000);
        pub const eMutableDescriptorTypeCreateInfoExt: Self = RawStructureType(1000351002);
        pub const ePhysicalDeviceVertexInputDynamicStateFeaturesExt: Self = RawStructureType(1000352000);
        pub const eVertexInputBindingDescription2Ext: Self = RawStructureType(1000352001);
        pub const eVertexInputAttributeDescription2Ext: Self = RawStructureType(1000352002);
        pub const ePhysicalDeviceDrmPropertiesExt: Self = RawStructureType(1000353000);
        pub const ePhysicalDeviceAddressBindingReportFeaturesExt: Self = RawStructureType(1000354000);
        pub const eDeviceAddressBindingCallbackDataExt: Self = RawStructureType(1000354001);
        pub const ePhysicalDeviceDepthClipControlFeaturesExt: Self = RawStructureType(1000355000);
        pub const ePipelineViewportDepthClipControlCreateInfoExt: Self = RawStructureType(1000355001);
        pub const ePhysicalDevicePrimitiveTopologyListRestartFeaturesExt: Self = RawStructureType(1000356000);
        pub const eFormatProperties3: Self = RawStructureType(1000360000);
        pub const eImportMemoryZirconHandleInfoFuchsia: Self = RawStructureType(1000364000);
        pub const eMemoryZirconHandlePropertiesFuchsia: Self = RawStructureType(1000364001);
        pub const eMemoryGetZirconHandleInfoFuchsia: Self = RawStructureType(1000364002);
        pub const eImportSemaphoreZirconHandleInfoFuchsia: Self = RawStructureType(1000365000);
        pub const eSemaphoreGetZirconHandleInfoFuchsia: Self = RawStructureType(1000365001);
        pub const eBufferCollectionCreateInfoFuchsia: Self = RawStructureType(1000366000);
        pub const eImportMemoryBufferCollectionFuchsia: Self = RawStructureType(1000366001);
        pub const eBufferCollectionImageCreateInfoFuchsia: Self = RawStructureType(1000366002);
        pub const eBufferCollectionPropertiesFuchsia: Self = RawStructureType(1000366003);
        pub const eBufferConstraintsInfoFuchsia: Self = RawStructureType(1000366004);
        pub const eBufferCollectionBufferCreateInfoFuchsia: Self = RawStructureType(1000366005);
        pub const eImageConstraintsInfoFuchsia: Self = RawStructureType(1000366006);
        pub const eImageFormatConstraintsInfoFuchsia: Self = RawStructureType(1000366007);
        pub const eSysmemColourSpaceFuchsia: Self = RawStructureType(1000366008);
        pub const eBufferCollectionConstraintsInfoFuchsia: Self = RawStructureType(1000366009);
        pub const eSubpassShadingPipelineCreateInfoHuawei: Self = RawStructureType(1000369000);
        pub const ePhysicalDeviceSubpassShadingFeaturesHuawei: Self = RawStructureType(1000369001);
        pub const ePhysicalDeviceSubpassShadingPropertiesHuawei: Self = RawStructureType(1000369002);
        pub const ePhysicalDeviceInvocationMaskFeaturesHuawei: Self = RawStructureType(1000370000);
        pub const eMemoryGetRemoteAddressInfoNv: Self = RawStructureType(1000371000);
        pub const ePhysicalDeviceExternalMemoryRdmaFeaturesNv: Self = RawStructureType(1000371001);
        pub const ePipelinePropertiesIdentifierExt: Self = RawStructureType(1000372000);
        pub const ePhysicalDevicePipelinePropertiesFeaturesExt: Self = RawStructureType(1000372001);
        pub const eImportFenceSciSyncInfoNv: Self = RawStructureType(1000373000);
        pub const eExportFenceSciSyncInfoNv: Self = RawStructureType(1000373001);
        pub const eFenceGetSciSyncInfoNv: Self = RawStructureType(1000373002);
        pub const eSciSyncAttributesInfoNv: Self = RawStructureType(1000373003);
        pub const eImportSemaphoreSciSyncInfoNv: Self = RawStructureType(1000373004);
        pub const eExportSemaphoreSciSyncInfoNv: Self = RawStructureType(1000373005);
        pub const eSemaphoreGetSciSyncInfoNv: Self = RawStructureType(1000373006);
        pub const ePhysicalDeviceExternalSciSyncFeaturesNv: Self = RawStructureType(1000373007);
        pub const eImportMemorySciBufInfoNv: Self = RawStructureType(1000374000);
        pub const eExportMemorySciBufInfoNv: Self = RawStructureType(1000374001);
        pub const eMemoryGetSciBufInfoNv: Self = RawStructureType(1000374002);
        pub const eMemorySciBufPropertiesNv: Self = RawStructureType(1000374003);
        pub const ePhysicalDeviceExternalMemorySciBufFeaturesNv: Self = RawStructureType(1000374004);
        pub const ePhysicalDeviceMultisampledRenderToSingleSampledFeaturesExt: Self = RawStructureType(1000376000);
        pub const eSubpassResolvePerformanceQueryExt: Self = RawStructureType(1000376001);
        pub const eMultisampledRenderToSingleSampledInfoExt: Self = RawStructureType(1000376002);
        pub const ePhysicalDeviceExtendedDynamicState2FeaturesExt: Self = RawStructureType(1000377000);
        pub const eScreenSurfaceCreateInfoQnx: Self = RawStructureType(1000378000);
        pub const ePhysicalDeviceColourWriteEnableFeaturesExt: Self = RawStructureType(1000381000);
        pub const ePipelineColourWriteCreateInfoExt: Self = RawStructureType(1000381001);
        pub const ePhysicalDevicePrimitivesGeneratedQueryFeaturesExt: Self = RawStructureType(1000382000);
        pub const ePhysicalDeviceRayTracingMaintenance1FeaturesKhr: Self = RawStructureType(1000386000);
        pub const ePhysicalDeviceGlobalPriorityQueryFeaturesKhr: Self = RawStructureType(1000388000);
        pub const eQueueFamilyGlobalPriorityPropertiesKhr: Self = RawStructureType(1000388001);
        pub const ePhysicalDeviceImageViewMinLodFeaturesExt: Self = RawStructureType(1000391000);
        pub const eImageViewMinLodCreateInfoExt: Self = RawStructureType(1000391001);
        pub const ePhysicalDeviceMultiDrawFeaturesExt: Self = RawStructureType(1000392000);
        pub const ePhysicalDeviceMultiDrawPropertiesExt: Self = RawStructureType(1000392001);
        pub const ePhysicalDeviceImage2dViewOf3dFeaturesExt: Self = RawStructureType(1000393000);
        pub const eMicromapBuildInfoExt: Self = RawStructureType(1000396000);
        pub const eMicromapVersionInfoExt: Self = RawStructureType(1000396001);
        pub const eCopyMicromapInfoExt: Self = RawStructureType(1000396002);
        pub const eCopyMicromapToMemoryInfoExt: Self = RawStructureType(1000396003);
        pub const eCopyMemoryToMicromapInfoExt: Self = RawStructureType(1000396004);
        pub const ePhysicalDeviceOpacityMicromapFeaturesExt: Self = RawStructureType(1000396005);
        pub const ePhysicalDeviceOpacityMicromapPropertiesExt: Self = RawStructureType(1000396006);
        pub const eMicromapCreateInfoExt: Self = RawStructureType(1000396007);
        pub const eMicromapBuildSizesInfoExt: Self = RawStructureType(1000396008);
        pub const eAccelerationStructureTrianglesOpacityMicromapExt: Self = RawStructureType(1000396009);
        pub const ePhysicalDeviceClusterCullingShaderFeaturesHuawei: Self = RawStructureType(1000404000);
        pub const ePhysicalDeviceClusterCullingShaderPropertiesHuawei: Self = RawStructureType(1000404001);
        pub const ePhysicalDeviceBorderColourSwizzleFeaturesExt: Self = RawStructureType(1000411000);
        pub const eSamplerBorderColourComponentMappingCreateInfoExt: Self = RawStructureType(1000411001);
        pub const ePhysicalDevicePageableDeviceLocalMemoryFeaturesExt: Self = RawStructureType(1000412000);
        pub const ePhysicalDeviceMaintenance4Features: Self = RawStructureType(1000413000);
        pub const ePhysicalDeviceMaintenance4Properties: Self = RawStructureType(1000413001);
        pub const eDeviceBufferMemoryRequirements: Self = RawStructureType(1000413002);
        pub const eDeviceImageMemoryRequirements: Self = RawStructureType(1000413003);
        pub const ePhysicalDeviceShaderCorePropertiesArm: Self = RawStructureType(1000415000);
        pub const ePhysicalDeviceImageSlicedViewOf3dFeaturesExt: Self = RawStructureType(1000418000);
        pub const eImageViewSlicedCreateInfoExt: Self = RawStructureType(1000418001);
        pub const ePhysicalDeviceDescriptorSetHostMappingFeaturesValve: Self = RawStructureType(1000420000);
        pub const eDescriptorSetBindingReferenceValve: Self = RawStructureType(1000420001);
        pub const eDescriptorSetLayoutHostMappingInfoValve: Self = RawStructureType(1000420002);
        pub const ePhysicalDeviceDepthClampZeroOneFeaturesExt: Self = RawStructureType(1000421000);
        pub const ePhysicalDeviceNonSeamlessCubeMapFeaturesExt: Self = RawStructureType(1000422000);
        pub const ePhysicalDeviceFragmentDensityMapOffsetFeaturesQcom: Self = RawStructureType(1000425000);
        pub const ePhysicalDeviceFragmentDensityMapOffsetPropertiesQcom: Self = RawStructureType(1000425001);
        pub const eSubpassFragmentDensityMapOffsetEndInfoQcom: Self = RawStructureType(1000425002);
        pub const ePhysicalDeviceCopyMemoryIndirectFeaturesNv: Self = RawStructureType(1000426000);
        pub const ePhysicalDeviceCopyMemoryIndirectPropertiesNv: Self = RawStructureType(1000426001);
        pub const ePhysicalDeviceMemoryDecompressionFeaturesNv: Self = RawStructureType(1000427000);
        pub const ePhysicalDeviceMemoryDecompressionPropertiesNv: Self = RawStructureType(1000427001);
        pub const ePhysicalDeviceLinearColourAttachmentFeaturesNv: Self = RawStructureType(1000430000);
        pub const eApplicationParametersExt: Self = RawStructureType(1000435000);
        pub const ePhysicalDeviceImageCompressionControlSwapchainFeaturesExt: Self = RawStructureType(1000437000);
        pub const ePhysicalDeviceImageProcessingFeaturesQcom: Self = RawStructureType(1000440000);
        pub const ePhysicalDeviceImageProcessingPropertiesQcom: Self = RawStructureType(1000440001);
        pub const eImageViewSampleWeightCreateInfoQcom: Self = RawStructureType(1000440002);
        pub const ePhysicalDeviceExtendedDynamicState3FeaturesExt: Self = RawStructureType(1000455000);
        pub const ePhysicalDeviceExtendedDynamicState3PropertiesExt: Self = RawStructureType(1000455001);
        pub const ePhysicalDeviceSubpassMergeFeedbackFeaturesExt: Self = RawStructureType(1000458000);
        pub const eRenderPassCreationControlExt: Self = RawStructureType(1000458001);
        pub const eRenderPassCreationFeedbackCreateInfoExt: Self = RawStructureType(1000458002);
        pub const eRenderPassSubpassFeedbackCreateInfoExt: Self = RawStructureType(1000458003);
        pub const eDirectDriverLoadingInfoLunarg: Self = RawStructureType(1000459000);
        pub const eDirectDriverLoadingListLunarg: Self = RawStructureType(1000459001);
        pub const ePhysicalDeviceShaderModuleIdentifierFeaturesExt: Self = RawStructureType(1000462000);
        pub const ePhysicalDeviceShaderModuleIdentifierPropertiesExt: Self = RawStructureType(1000462001);
        pub const ePipelineShaderStageModuleIdentifierCreateInfoExt: Self = RawStructureType(1000462002);
        pub const eShaderModuleIdentifierExt: Self = RawStructureType(1000462003);
        pub const ePhysicalDeviceOpticalFlowFeaturesNv: Self = RawStructureType(1000464000);
        pub const ePhysicalDeviceOpticalFlowPropertiesNv: Self = RawStructureType(1000464001);
        pub const eOpticalFlowImageFormatInfoNv: Self = RawStructureType(1000464002);
        pub const eOpticalFlowImageFormatPropertiesNv: Self = RawStructureType(1000464003);
        pub const eOpticalFlowSessionCreateInfoNv: Self = RawStructureType(1000464004);
        pub const eOpticalFlowExecuteInfoNv: Self = RawStructureType(1000464005);
        pub const eOpticalFlowSessionCreatePrivateDataInfoNv: Self = RawStructureType(1000464010);
        pub const ePhysicalDeviceLegacyDitheringFeaturesExt: Self = RawStructureType(1000465000);
        pub const ePhysicalDevicePipelineProtectedAccessFeaturesExt: Self = RawStructureType(1000466000);
        pub const ePhysicalDeviceTilePropertiesFeaturesQcom: Self = RawStructureType(1000484000);
        pub const eTilePropertiesQcom: Self = RawStructureType(1000484001);
        pub const ePhysicalDeviceAmigoProfilingFeaturesSec: Self = RawStructureType(1000485000);
        pub const eAmigoProfilingSubmitInfoSec: Self = RawStructureType(1000485001);
        pub const ePhysicalDeviceMultiviewPerViewViewportsFeaturesQcom: Self = RawStructureType(1000488000);
        pub const eSemaphoreSciSyncPoolCreateInfoNv: Self = RawStructureType(1000489000);
        pub const eSemaphoreSciSyncCreateInfoNv: Self = RawStructureType(1000489001);
        pub const ePhysicalDeviceExternalSciSync2FeaturesNv: Self = RawStructureType(1000489002);
        pub const eDeviceSemaphoreSciSyncPoolReservationCreateInfoNv: Self = RawStructureType(1000489003);
        pub const ePhysicalDeviceRayTracingInvocationReorderFeaturesNv: Self = RawStructureType(1000490000);
        pub const ePhysicalDeviceRayTracingInvocationReorderPropertiesNv: Self = RawStructureType(1000490001);
        pub const ePhysicalDeviceShaderCoreBuiltinsFeaturesArm: Self = RawStructureType(1000497000);
        pub const ePhysicalDeviceShaderCoreBuiltinsPropertiesArm: Self = RawStructureType(1000497001);
        pub const ePhysicalDevicePipelineLibraryGroupHandlesFeaturesExt: Self = RawStructureType(1000498000);
        pub const ePhysicalDeviceMultiviewPerViewRenderAreasFeaturesQcom: Self = RawStructureType(1000510000);
        pub const eMultiviewPerViewRenderAreasRenderPassBeginInfoQcom: Self = RawStructureType(1000510001);
        pub const eAttachmentDescription2Khr: Self = Self::eAttachmentDescription2;
        pub const eAttachmentDescriptionStencilLayoutKhr: Self = Self::eAttachmentDescriptionStencilLayout;
        pub const eAttachmentReference2Khr: Self = Self::eAttachmentReference2;
        pub const eAttachmentReferenceStencilLayoutKhr: Self = Self::eAttachmentReferenceStencilLayout;
        pub const eAttachmentSampleCountInfoNv: Self = Self::eAttachmentSampleCountInfoAmd;
        pub const eBindBufferMemoryDeviceGroupInfoKhr: Self = Self::eBindBufferMemoryDeviceGroupInfo;
        pub const eBindBufferMemoryInfoKhr: Self = Self::eBindBufferMemoryInfo;
        pub const eBindImageMemoryDeviceGroupInfoKhr: Self = Self::eBindImageMemoryDeviceGroupInfo;
        pub const eBindImageMemoryInfoKhr: Self = Self::eBindImageMemoryInfo;
        pub const eBindImagePlaneMemoryInfoKhr: Self = Self::eBindImagePlaneMemoryInfo;
        pub const eBlitImageInfo2Khr: Self = Self::eBlitImageInfo2;
        pub const eBufferCopy2Khr: Self = Self::eBufferCopy2;
        pub const eBufferDeviceAddressInfoExt: Self = Self::eBufferDeviceAddressInfo;
        pub const eBufferDeviceAddressInfoKhr: Self = Self::eBufferDeviceAddressInfo;
        pub const eBufferImageCopy2Khr: Self = Self::eBufferImageCopy2;
        pub const eBufferMemoryBarrier2Khr: Self = Self::eBufferMemoryBarrier2;
        pub const eBufferMemoryRequirementsInfo2Khr: Self = Self::eBufferMemoryRequirementsInfo2;
        pub const eBufferOpaqueCaptureAddressCreateInfoKhr: Self = Self::eBufferOpaqueCaptureAddressCreateInfo;
        pub const eCommandBufferInheritanceRenderingInfoKhr: Self = Self::eCommandBufferInheritanceRenderingInfo;
        pub const eCommandBufferSubmitInfoKhr: Self = Self::eCommandBufferSubmitInfo;
        pub const eCopyBufferInfo2Khr: Self = Self::eCopyBufferInfo2;
        pub const eCopyBufferToImageInfo2Khr: Self = Self::eCopyBufferToImageInfo2;
        pub const eCopyImageInfo2Khr: Self = Self::eCopyImageInfo2;
        pub const eCopyImageToBufferInfo2Khr: Self = Self::eCopyImageToBufferInfo2;
        pub const eDebugReportCreateInfoExt: Self = Self::eDebugReportCallbackCreateInfoExt;
        pub const eDependencyInfoKhr: Self = Self::eDependencyInfo;
        pub const eDescriptorPoolInlineUniformBlockCreateInfoExt: Self = Self::eDescriptorPoolInlineUniformBlockCreateInfo;
        pub const eDescriptorSetLayoutBindingFlagsCreateInfoExt: Self = Self::eDescriptorSetLayoutBindingFlagsCreateInfo;
        pub const eDescriptorSetLayoutSupportKhr: Self = Self::eDescriptorSetLayoutSupport;
        pub const eDescriptorSetVariableDescriptorCountAllocateInfoExt: Self = Self::eDescriptorSetVariableDescriptorCountAllocateInfo;
        pub const eDescriptorSetVariableDescriptorCountLayoutSupportExt: Self = Self::eDescriptorSetVariableDescriptorCountLayoutSupport;
        pub const eDescriptorUpdateTemplateCreateInfoKhr: Self = Self::eDescriptorUpdateTemplateCreateInfo;
        pub const eDeviceBufferMemoryRequirementsKhr: Self = Self::eDeviceBufferMemoryRequirements;
        pub const eDeviceGroupBindSparseInfoKhr: Self = Self::eDeviceGroupBindSparseInfo;
        pub const eDeviceGroupCommandBufferBeginInfoKhr: Self = Self::eDeviceGroupCommandBufferBeginInfo;
        pub const eDeviceGroupDeviceCreateInfoKhr: Self = Self::eDeviceGroupDeviceCreateInfo;
        pub const eDeviceGroupRenderPassBeginInfoKhr: Self = Self::eDeviceGroupRenderPassBeginInfo;
        pub const eDeviceGroupSubmitInfoKhr: Self = Self::eDeviceGroupSubmitInfo;
        pub const eDeviceImageMemoryRequirementsKhr: Self = Self::eDeviceImageMemoryRequirements;
        pub const eDeviceMemoryOpaqueCaptureAddressInfoKhr: Self = Self::eDeviceMemoryOpaqueCaptureAddressInfo;
        pub const eDevicePrivateDataCreateInfoExt: Self = Self::eDevicePrivateDataCreateInfo;
        pub const eDeviceQueueGlobalPriorityCreateInfoExt: Self = Self::eDeviceQueueGlobalPriorityCreateInfoKhr;
        pub const eExportFenceCreateInfoKhr: Self = Self::eExportFenceCreateInfo;
        pub const eExportMemoryAllocateInfoKhr: Self = Self::eExportMemoryAllocateInfo;
        pub const eExportSemaphoreCreateInfoKhr: Self = Self::eExportSemaphoreCreateInfo;
        pub const eExternalBufferPropertiesKhr: Self = Self::eExternalBufferProperties;
        pub const eExternalFencePropertiesKhr: Self = Self::eExternalFenceProperties;
        pub const eExternalImageFormatPropertiesKhr: Self = Self::eExternalImageFormatProperties;
        pub const eExternalMemoryBufferCreateInfoKhr: Self = Self::eExternalMemoryBufferCreateInfo;
        pub const eExternalMemoryImageCreateInfoKhr: Self = Self::eExternalMemoryImageCreateInfo;
        pub const eExternalSemaphorePropertiesKhr: Self = Self::eExternalSemaphoreProperties;
        pub const eFormatProperties2Khr: Self = Self::eFormatProperties2;
        pub const eFormatProperties3Khr: Self = Self::eFormatProperties3;
        pub const eFramebufferAttachmentsCreateInfoKhr: Self = Self::eFramebufferAttachmentsCreateInfo;
        pub const eFramebufferAttachmentImageInfoKhr: Self = Self::eFramebufferAttachmentImageInfo;
        pub const eImageBlit2Khr: Self = Self::eImageBlit2;
        pub const eImageCopy2Khr: Self = Self::eImageCopy2;
        pub const eImageFormatListCreateInfoKhr: Self = Self::eImageFormatListCreateInfo;
        pub const eImageFormatProperties2Khr: Self = Self::eImageFormatProperties2;
        pub const eImageMemoryBarrier2Khr: Self = Self::eImageMemoryBarrier2;
        pub const eImageMemoryRequirementsInfo2Khr: Self = Self::eImageMemoryRequirementsInfo2;
        pub const eImagePlaneMemoryRequirementsInfoKhr: Self = Self::eImagePlaneMemoryRequirementsInfo;
        pub const eImageResolve2Khr: Self = Self::eImageResolve2;
        pub const eImageSparseMemoryRequirementsInfo2Khr: Self = Self::eImageSparseMemoryRequirementsInfo2;
        pub const eImageStencilUsageCreateInfoExt: Self = Self::eImageStencilUsageCreateInfo;
        pub const eImageViewUsageCreateInfoKhr: Self = Self::eImageViewUsageCreateInfo;
        pub const eMemoryAllocateFlagsInfoKhr: Self = Self::eMemoryAllocateFlagsInfo;
        pub const eMemoryBarrier2Khr: Self = Self::eMemoryBarrier2;
        pub const eMemoryDedicatedAllocateInfoKhr: Self = Self::eMemoryDedicatedAllocateInfo;
        pub const eMemoryDedicatedRequirementsKhr: Self = Self::eMemoryDedicatedRequirements;
        pub const eMemoryOpaqueCaptureAddressAllocateInfoKhr: Self = Self::eMemoryOpaqueCaptureAddressAllocateInfo;
        pub const eMemoryRequirements2Khr: Self = Self::eMemoryRequirements2;
        pub const eMutableDescriptorTypeCreateInfoValve: Self = Self::eMutableDescriptorTypeCreateInfoExt;
        pub const ePhysicalDevice16bitStorageFeaturesKhr: Self = Self::ePhysicalDevice16bitStorageFeatures;
        pub const ePhysicalDevice8bitStorageFeaturesKhr: Self = Self::ePhysicalDevice8bitStorageFeatures;
        pub const ePhysicalDeviceBufferDeviceAddressFeaturesKhr: Self = Self::ePhysicalDeviceBufferDeviceAddressFeatures;
        pub const ePhysicalDeviceBufferAddressFeaturesExt: Self = Self::ePhysicalDeviceBufferDeviceAddressFeaturesExt;
        pub const ePhysicalDeviceDepthStencilResolvePropertiesKhr: Self = Self::ePhysicalDeviceDepthStencilResolveProperties;
        pub const ePhysicalDeviceDescriptorIndexingFeaturesExt: Self = Self::ePhysicalDeviceDescriptorIndexingFeatures;
        pub const ePhysicalDeviceDescriptorIndexingPropertiesExt: Self = Self::ePhysicalDeviceDescriptorIndexingProperties;
        pub const ePhysicalDeviceDriverPropertiesKhr: Self = Self::ePhysicalDeviceDriverProperties;
        pub const ePhysicalDeviceDynamicRenderingFeaturesKhr: Self = Self::ePhysicalDeviceDynamicRenderingFeatures;
        pub const ePhysicalDeviceExternalBufferInfoKhr: Self = Self::ePhysicalDeviceExternalBufferInfo;
        pub const ePhysicalDeviceExternalFenceInfoKhr: Self = Self::ePhysicalDeviceExternalFenceInfo;
        pub const ePhysicalDeviceExternalImageFormatInfoKhr: Self = Self::ePhysicalDeviceExternalImageFormatInfo;
        pub const ePhysicalDeviceExternalSemaphoreInfoKhr: Self = Self::ePhysicalDeviceExternalSemaphoreInfo;
        pub const ePhysicalDeviceFeatures2Khr: Self = Self::ePhysicalDeviceFeatures2;
        pub const ePhysicalDeviceFloatControlsPropertiesKhr: Self = Self::ePhysicalDeviceFloatControlsProperties;
        pub const ePhysicalDeviceFragmentShaderBarycentricFeaturesNv: Self = Self::ePhysicalDeviceFragmentShaderBarycentricFeaturesKhr;
        pub const ePhysicalDeviceGlobalPriorityQueryFeaturesExt: Self = Self::ePhysicalDeviceGlobalPriorityQueryFeaturesKhr;
        pub const ePhysicalDeviceGroupPropertiesKhr: Self = Self::ePhysicalDeviceGroupProperties;
        pub const ePhysicalDeviceHostQueryResetFeaturesExt: Self = Self::ePhysicalDeviceHostQueryResetFeatures;
        pub const ePhysicalDeviceIdPropertiesKhr: Self = Self::ePhysicalDeviceIdProperties;
        pub const ePhysicalDeviceImagelessFramebufferFeaturesKhr: Self = Self::ePhysicalDeviceImagelessFramebufferFeatures;
        pub const ePhysicalDeviceImageFormatInfo2Khr: Self = Self::ePhysicalDeviceImageFormatInfo2;
        pub const ePhysicalDeviceImageRobustnessFeaturesExt: Self = Self::ePhysicalDeviceImageRobustnessFeatures;
        pub const ePhysicalDeviceInlineUniformBlockFeaturesExt: Self = Self::ePhysicalDeviceInlineUniformBlockFeatures;
        pub const ePhysicalDeviceInlineUniformBlockPropertiesExt: Self = Self::ePhysicalDeviceInlineUniformBlockProperties;
        pub const ePhysicalDeviceMaintenance3PropertiesKhr: Self = Self::ePhysicalDeviceMaintenance3Properties;
        pub const ePhysicalDeviceMaintenance4FeaturesKhr: Self = Self::ePhysicalDeviceMaintenance4Features;
        pub const ePhysicalDeviceMaintenance4PropertiesKhr: Self = Self::ePhysicalDeviceMaintenance4Properties;
        pub const ePhysicalDeviceMemoryProperties2Khr: Self = Self::ePhysicalDeviceMemoryProperties2;
        pub const ePhysicalDeviceMultiviewFeaturesKhr: Self = Self::ePhysicalDeviceMultiviewFeatures;
        pub const ePhysicalDeviceMultiviewPropertiesKhr: Self = Self::ePhysicalDeviceMultiviewProperties;
        pub const ePhysicalDeviceMutableDescriptorTypeFeaturesValve: Self = Self::ePhysicalDeviceMutableDescriptorTypeFeaturesExt;
        pub const ePhysicalDevicePipelineCreationCacheControlFeaturesExt: Self = Self::ePhysicalDevicePipelineCreationCacheControlFeatures;
        pub const ePhysicalDevicePointClippingPropertiesKhr: Self = Self::ePhysicalDevicePointClippingProperties;
        pub const ePhysicalDevicePrivateDataFeaturesExt: Self = Self::ePhysicalDevicePrivateDataFeatures;
        pub const ePhysicalDeviceProperties2Khr: Self = Self::ePhysicalDeviceProperties2;
        pub const ePhysicalDeviceRasterizationOrderAttachmentAccessFeaturesArm: Self = Self::ePhysicalDeviceRasterizationOrderAttachmentAccessFeaturesExt;
        pub const ePhysicalDeviceSamplerFilterMinmaxPropertiesExt: Self = Self::ePhysicalDeviceSamplerFilterMinmaxProperties;
        pub const ePhysicalDeviceSamplerYcbcrConversionFeaturesKhr: Self = Self::ePhysicalDeviceSamplerYcbcrConversionFeatures;
        pub const ePhysicalDeviceScalarBlockLayoutFeaturesExt: Self = Self::ePhysicalDeviceScalarBlockLayoutFeatures;
        pub const ePhysicalDeviceSeparateDepthStencilLayoutsFeaturesKhr: Self = Self::ePhysicalDeviceSeparateDepthStencilLayoutsFeatures;
        pub const ePhysicalDeviceShaderAtomicInt64FeaturesKhr: Self = Self::ePhysicalDeviceShaderAtomicInt64Features;
        pub const ePhysicalDeviceShaderDemoteToHelperInvocationFeaturesExt: Self = Self::ePhysicalDeviceShaderDemoteToHelperInvocationFeatures;
        pub const ePhysicalDeviceShaderDrawParameterFeatures: Self = Self::ePhysicalDeviceShaderDrawParametersFeatures;
        pub const ePhysicalDeviceFloat16Int8FeaturesKhr: Self = Self::ePhysicalDeviceShaderFloat16Int8Features;
        pub const ePhysicalDeviceShaderFloat16Int8FeaturesKhr: Self = Self::ePhysicalDeviceShaderFloat16Int8Features;
        pub const ePhysicalDeviceShaderIntegerDotProductFeaturesKhr: Self = Self::ePhysicalDeviceShaderIntegerDotProductFeatures;
        pub const ePhysicalDeviceShaderIntegerDotProductPropertiesKhr: Self = Self::ePhysicalDeviceShaderIntegerDotProductProperties;
        pub const ePhysicalDeviceShaderSubgroupExtendedTypesFeaturesKhr: Self = Self::ePhysicalDeviceShaderSubgroupExtendedTypesFeatures;
        pub const ePhysicalDeviceShaderTerminateInvocationFeaturesKhr: Self = Self::ePhysicalDeviceShaderTerminateInvocationFeatures;
        pub const ePhysicalDeviceSparseImageFormatInfo2Khr: Self = Self::ePhysicalDeviceSparseImageFormatInfo2;
        pub const ePhysicalDeviceSubgroupSizeControlFeaturesExt: Self = Self::ePhysicalDeviceSubgroupSizeControlFeatures;
        pub const ePhysicalDeviceSubgroupSizeControlPropertiesExt: Self = Self::ePhysicalDeviceSubgroupSizeControlProperties;
        pub const ePhysicalDeviceSynchronization2FeaturesKhr: Self = Self::ePhysicalDeviceSynchronization2Features;
        pub const ePhysicalDeviceTexelBufferAlignmentPropertiesExt: Self = Self::ePhysicalDeviceTexelBufferAlignmentProperties;
        pub const ePhysicalDeviceTextureCompressionAstcHdrFeaturesExt: Self = Self::ePhysicalDeviceTextureCompressionAstcHdrFeatures;
        pub const ePhysicalDeviceTimelineSemaphoreFeaturesKhr: Self = Self::ePhysicalDeviceTimelineSemaphoreFeatures;
        pub const ePhysicalDeviceTimelineSemaphorePropertiesKhr: Self = Self::ePhysicalDeviceTimelineSemaphoreProperties;
        pub const ePhysicalDeviceToolPropertiesExt: Self = Self::ePhysicalDeviceToolProperties;
        pub const ePhysicalDeviceUniformBufferStandardLayoutFeaturesKhr: Self = Self::ePhysicalDeviceUniformBufferStandardLayoutFeatures;
        pub const ePhysicalDeviceVariablePointersFeaturesKhr: Self = Self::ePhysicalDeviceVariablePointersFeatures;
        pub const ePhysicalDeviceVariablePointerFeatures: Self = Self::ePhysicalDeviceVariablePointersFeatures;
        pub const ePhysicalDeviceVariablePointerFeaturesKhr: Self = Self::ePhysicalDeviceVariablePointersFeaturesKhr;
        pub const ePhysicalDeviceVulkanMemoryModelFeaturesKhr: Self = Self::ePhysicalDeviceVulkanMemoryModelFeatures;
        pub const ePhysicalDeviceZeroInitializeWorkgroupMemoryFeaturesKhr: Self = Self::ePhysicalDeviceZeroInitializeWorkgroupMemoryFeatures;
        pub const ePipelineCreationFeedbackCreateInfoExt: Self = Self::ePipelineCreationFeedbackCreateInfo;
        pub const ePipelineInfoExt: Self = Self::ePipelineInfoKhr;
        pub const ePipelineRenderingCreateInfoKhr: Self = Self::ePipelineRenderingCreateInfo;
        pub const ePipelineShaderStageRequiredSubgroupSizeCreateInfoExt: Self = Self::ePipelineShaderStageRequiredSubgroupSizeCreateInfo;
        pub const ePipelineTessellationDomainOriginStateCreateInfoKhr: Self = Self::ePipelineTessellationDomainOriginStateCreateInfo;
        pub const ePrivateDataSlotCreateInfoExt: Self = Self::ePrivateDataSlotCreateInfo;
        pub const eQueryPoolCreateInfoIntel: Self = Self::eQueryPoolPerformanceQueryCreateInfoIntel;
        pub const eQueueFamilyGlobalPriorityPropertiesExt: Self = Self::eQueueFamilyGlobalPriorityPropertiesKhr;
        pub const eQueueFamilyProperties2Khr: Self = Self::eQueueFamilyProperties2;
        pub const eRenderingAttachmentInfoKhr: Self = Self::eRenderingAttachmentInfo;
        pub const eRenderingInfoKhr: Self = Self::eRenderingInfo;
        pub const eRenderPassAttachmentBeginInfoKhr: Self = Self::eRenderPassAttachmentBeginInfo;
        pub const eRenderPassCreateInfo2Khr: Self = Self::eRenderPassCreateInfo2;
        pub const eRenderPassInputAttachmentAspectCreateInfoKhr: Self = Self::eRenderPassInputAttachmentAspectCreateInfo;
        pub const eRenderPassMultiviewCreateInfoKhr: Self = Self::eRenderPassMultiviewCreateInfo;
        pub const eResolveImageInfo2Khr: Self = Self::eResolveImageInfo2;
        pub const eSamplerReductionModeCreateInfoExt: Self = Self::eSamplerReductionModeCreateInfo;
        pub const eSamplerYcbcrConversionCreateInfoKhr: Self = Self::eSamplerYcbcrConversionCreateInfo;
        pub const eSamplerYcbcrConversionImageFormatPropertiesKhr: Self = Self::eSamplerYcbcrConversionImageFormatProperties;
        pub const eSamplerYcbcrConversionInfoKhr: Self = Self::eSamplerYcbcrConversionInfo;
        pub const eSemaphoreSignalInfoKhr: Self = Self::eSemaphoreSignalInfo;
        pub const eSemaphoreSubmitInfoKhr: Self = Self::eSemaphoreSubmitInfo;
        pub const eSemaphoreTypeCreateInfoKhr: Self = Self::eSemaphoreTypeCreateInfo;
        pub const eSemaphoreWaitInfoKhr: Self = Self::eSemaphoreWaitInfo;
        pub const eSparseImageFormatProperties2Khr: Self = Self::eSparseImageFormatProperties2;
        pub const eSparseImageMemoryRequirements2Khr: Self = Self::eSparseImageMemoryRequirements2;
        pub const eSubmitInfo2Khr: Self = Self::eSubmitInfo2;
        pub const eSubpassBeginInfoKhr: Self = Self::eSubpassBeginInfo;
        pub const eSubpassDependency2Khr: Self = Self::eSubpassDependency2;
        pub const eSubpassDescription2Khr: Self = Self::eSubpassDescription2;
        pub const eSubpassDescriptionDepthStencilResolveKhr: Self = Self::eSubpassDescriptionDepthStencilResolve;
        pub const eSubpassEndInfoKhr: Self = Self::eSubpassEndInfo;
        pub const eTimelineSemaphoreSubmitInfoKhr: Self = Self::eTimelineSemaphoreSubmitInfo;
        pub const eWriteDescriptorSetInlineUniformBlockExt: Self = Self::eWriteDescriptorSetInlineUniformBlock;

        pub fn normalise(self) -> StructureType {
            match self {
                Self::eApplicationInfo => StructureType::eApplicationInfo,
                Self::eInstanceCreateInfo => StructureType::eInstanceCreateInfo,
                Self::eDeviceQueueCreateInfo => StructureType::eDeviceQueueCreateInfo,
                Self::eDeviceCreateInfo => StructureType::eDeviceCreateInfo,
                Self::eSubmitInfo => StructureType::eSubmitInfo,
                Self::eMemoryAllocateInfo => StructureType::eMemoryAllocateInfo,
                Self::eMappedMemoryRange => StructureType::eMappedMemoryRange,
                Self::eBindSparseInfo => StructureType::eBindSparseInfo,
                Self::eFenceCreateInfo => StructureType::eFenceCreateInfo,
                Self::eSemaphoreCreateInfo => StructureType::eSemaphoreCreateInfo,
                Self::eEventCreateInfo => StructureType::eEventCreateInfo,
                Self::eQueryPoolCreateInfo => StructureType::eQueryPoolCreateInfo,
                Self::eBufferCreateInfo => StructureType::eBufferCreateInfo,
                Self::eBufferViewCreateInfo => StructureType::eBufferViewCreateInfo,
                Self::eImageCreateInfo => StructureType::eImageCreateInfo,
                Self::eImageViewCreateInfo => StructureType::eImageViewCreateInfo,
                Self::eShaderModuleCreateInfo => StructureType::eShaderModuleCreateInfo,
                Self::ePipelineCacheCreateInfo => StructureType::ePipelineCacheCreateInfo,
                Self::ePipelineShaderStageCreateInfo => StructureType::ePipelineShaderStageCreateInfo,
                Self::ePipelineVertexInputStateCreateInfo => StructureType::ePipelineVertexInputStateCreateInfo,
                Self::ePipelineInputAssemblyStateCreateInfo => StructureType::ePipelineInputAssemblyStateCreateInfo,
                Self::ePipelineTessellationStateCreateInfo => StructureType::ePipelineTessellationStateCreateInfo,
                Self::ePipelineViewportStateCreateInfo => StructureType::ePipelineViewportStateCreateInfo,
                Self::ePipelineRasterizationStateCreateInfo => StructureType::ePipelineRasterizationStateCreateInfo,
                Self::ePipelineMultisampleStateCreateInfo => StructureType::ePipelineMultisampleStateCreateInfo,
                Self::ePipelineDepthStencilStateCreateInfo => StructureType::ePipelineDepthStencilStateCreateInfo,
                Self::ePipelineColourBlendStateCreateInfo => StructureType::ePipelineColourBlendStateCreateInfo,
                Self::ePipelineDynamicStateCreateInfo => StructureType::ePipelineDynamicStateCreateInfo,
                Self::eGraphicsPipelineCreateInfo => StructureType::eGraphicsPipelineCreateInfo,
                Self::eComputePipelineCreateInfo => StructureType::eComputePipelineCreateInfo,
                Self::ePipelineLayoutCreateInfo => StructureType::ePipelineLayoutCreateInfo,
                Self::eSamplerCreateInfo => StructureType::eSamplerCreateInfo,
                Self::eDescriptorSetLayoutCreateInfo => StructureType::eDescriptorSetLayoutCreateInfo,
                Self::eDescriptorPoolCreateInfo => StructureType::eDescriptorPoolCreateInfo,
                Self::eDescriptorSetAllocateInfo => StructureType::eDescriptorSetAllocateInfo,
                Self::eWriteDescriptorSet => StructureType::eWriteDescriptorSet,
                Self::eCopyDescriptorSet => StructureType::eCopyDescriptorSet,
                Self::eFramebufferCreateInfo => StructureType::eFramebufferCreateInfo,
                Self::eRenderPassCreateInfo => StructureType::eRenderPassCreateInfo,
                Self::eCommandPoolCreateInfo => StructureType::eCommandPoolCreateInfo,
                Self::eCommandBufferAllocateInfo => StructureType::eCommandBufferAllocateInfo,
                Self::eCommandBufferInheritanceInfo => StructureType::eCommandBufferInheritanceInfo,
                Self::eCommandBufferBeginInfo => StructureType::eCommandBufferBeginInfo,
                Self::eRenderPassBeginInfo => StructureType::eRenderPassBeginInfo,
                Self::eBufferMemoryBarrier => StructureType::eBufferMemoryBarrier,
                Self::eImageMemoryBarrier => StructureType::eImageMemoryBarrier,
                Self::eMemoryBarrier => StructureType::eMemoryBarrier,
                Self::eLoaderInstanceCreateInfo => StructureType::eLoaderInstanceCreateInfo,
                Self::eLoaderDeviceCreateInfo => StructureType::eLoaderDeviceCreateInfo,
                Self::ePhysicalDeviceVulkan11Features => StructureType::ePhysicalDeviceVulkan11Features,
                Self::ePhysicalDeviceVulkan11Properties => StructureType::ePhysicalDeviceVulkan11Properties,
                Self::ePhysicalDeviceVulkan12Features => StructureType::ePhysicalDeviceVulkan12Features,
                Self::ePhysicalDeviceVulkan12Properties => StructureType::ePhysicalDeviceVulkan12Properties,
                Self::ePhysicalDeviceVulkan13Features => StructureType::ePhysicalDeviceVulkan13Features,
                Self::ePhysicalDeviceVulkan13Properties => StructureType::ePhysicalDeviceVulkan13Properties,
                Self::eSwapchainCreateInfoKhr => StructureType::eSwapchainCreateInfoKhr,
                Self::ePresentInfoKhr => StructureType::ePresentInfoKhr,
                Self::eDisplayModeCreateInfoKhr => StructureType::eDisplayModeCreateInfoKhr,
                Self::eDisplaySurfaceCreateInfoKhr => StructureType::eDisplaySurfaceCreateInfoKhr,
                Self::eDisplayPresentInfoKhr => StructureType::eDisplayPresentInfoKhr,
                Self::eXlibSurfaceCreateInfoKhr => StructureType::eXlibSurfaceCreateInfoKhr,
                Self::eXcbSurfaceCreateInfoKhr => StructureType::eXcbSurfaceCreateInfoKhr,
                Self::eWaylandSurfaceCreateInfoKhr => StructureType::eWaylandSurfaceCreateInfoKhr,
                Self::eAndroidSurfaceCreateInfoKhr => StructureType::eAndroidSurfaceCreateInfoKhr,
                Self::eWin32SurfaceCreateInfoKhr => StructureType::eWin32SurfaceCreateInfoKhr,
                Self::eDebugReportCallbackCreateInfoExt => StructureType::eDebugReportCallbackCreateInfoExt,
                Self::ePipelineRasterizationStateRasterizationOrderAmd => StructureType::ePipelineRasterizationStateRasterizationOrderAmd,
                Self::eDebugMarkerObjectNameInfoExt => StructureType::eDebugMarkerObjectNameInfoExt,
                Self::eDebugMarkerObjectTagInfoExt => StructureType::eDebugMarkerObjectTagInfoExt,
                Self::eDebugMarkerMarkerInfoExt => StructureType::eDebugMarkerMarkerInfoExt,
                Self::eVideoProfileInfoKhr => StructureType::eVideoProfileInfoKhr,
                Self::eVideoCapabilitiesKhr => StructureType::eVideoCapabilitiesKhr,
                Self::eVideoPictureResourceInfoKhr => StructureType::eVideoPictureResourceInfoKhr,
                Self::eVideoSessionMemoryRequirementsKhr => StructureType::eVideoSessionMemoryRequirementsKhr,
                Self::eBindVideoSessionMemoryInfoKhr => StructureType::eBindVideoSessionMemoryInfoKhr,
                Self::eVideoSessionCreateInfoKhr => StructureType::eVideoSessionCreateInfoKhr,
                Self::eVideoSessionParametersCreateInfoKhr => StructureType::eVideoSessionParametersCreateInfoKhr,
                Self::eVideoSessionParametersUpdateInfoKhr => StructureType::eVideoSessionParametersUpdateInfoKhr,
                Self::eVideoBeginCodingInfoKhr => StructureType::eVideoBeginCodingInfoKhr,
                Self::eVideoEndCodingInfoKhr => StructureType::eVideoEndCodingInfoKhr,
                Self::eVideoCodingControlInfoKhr => StructureType::eVideoCodingControlInfoKhr,
                Self::eVideoReferenceSlotInfoKhr => StructureType::eVideoReferenceSlotInfoKhr,
                Self::eQueueFamilyVideoPropertiesKhr => StructureType::eQueueFamilyVideoPropertiesKhr,
                Self::eVideoProfileListInfoKhr => StructureType::eVideoProfileListInfoKhr,
                Self::ePhysicalDeviceVideoFormatInfoKhr => StructureType::ePhysicalDeviceVideoFormatInfoKhr,
                Self::eVideoFormatPropertiesKhr => StructureType::eVideoFormatPropertiesKhr,
                Self::eQueueFamilyQueryResultStatusPropertiesKhr => StructureType::eQueueFamilyQueryResultStatusPropertiesKhr,
                Self::eVideoDecodeInfoKhr => StructureType::eVideoDecodeInfoKhr,
                Self::eVideoDecodeCapabilitiesKhr => StructureType::eVideoDecodeCapabilitiesKhr,
                Self::eVideoDecodeUsageInfoKhr => StructureType::eVideoDecodeUsageInfoKhr,
                Self::eDedicatedAllocationImageCreateInfoNv => StructureType::eDedicatedAllocationImageCreateInfoNv,
                Self::eDedicatedAllocationBufferCreateInfoNv => StructureType::eDedicatedAllocationBufferCreateInfoNv,
                Self::eDedicatedAllocationMemoryAllocateInfoNv => StructureType::eDedicatedAllocationMemoryAllocateInfoNv,
                Self::ePhysicalDeviceTransformFeedbackFeaturesExt => StructureType::ePhysicalDeviceTransformFeedbackFeaturesExt,
                Self::ePhysicalDeviceTransformFeedbackPropertiesExt => StructureType::ePhysicalDeviceTransformFeedbackPropertiesExt,
                Self::ePipelineRasterizationStateStreamCreateInfoExt => StructureType::ePipelineRasterizationStateStreamCreateInfoExt,
                Self::eCuModuleCreateInfoNvx => StructureType::eCuModuleCreateInfoNvx,
                Self::eCuFunctionCreateInfoNvx => StructureType::eCuFunctionCreateInfoNvx,
                Self::eCuLaunchInfoNvx => StructureType::eCuLaunchInfoNvx,
                Self::eImageViewHandleInfoNvx => StructureType::eImageViewHandleInfoNvx,
                Self::eImageViewAddressPropertiesNvx => StructureType::eImageViewAddressPropertiesNvx,
                Self::eVideoEncodeH264CapabilitiesExt => StructureType::eVideoEncodeH264CapabilitiesExt,
                Self::eVideoEncodeH264SessionParametersCreateInfoExt => StructureType::eVideoEncodeH264SessionParametersCreateInfoExt,
                Self::eVideoEncodeH264SessionParametersAddInfoExt => StructureType::eVideoEncodeH264SessionParametersAddInfoExt,
                Self::eVideoEncodeH264VclFrameInfoExt => StructureType::eVideoEncodeH264VclFrameInfoExt,
                Self::eVideoEncodeH264DpbSlotInfoExt => StructureType::eVideoEncodeH264DpbSlotInfoExt,
                Self::eVideoEncodeH264NaluSliceInfoExt => StructureType::eVideoEncodeH264NaluSliceInfoExt,
                Self::eVideoEncodeH264EmitPictureParametersInfoExt => StructureType::eVideoEncodeH264EmitPictureParametersInfoExt,
                Self::eVideoEncodeH264ProfileInfoExt => StructureType::eVideoEncodeH264ProfileInfoExt,
                Self::eVideoEncodeH264RateControlInfoExt => StructureType::eVideoEncodeH264RateControlInfoExt,
                Self::eVideoEncodeH264RateControlLayerInfoExt => StructureType::eVideoEncodeH264RateControlLayerInfoExt,
                Self::eVideoEncodeH264ReferenceListsInfoExt => StructureType::eVideoEncodeH264ReferenceListsInfoExt,
                Self::eVideoEncodeH265CapabilitiesExt => StructureType::eVideoEncodeH265CapabilitiesExt,
                Self::eVideoEncodeH265SessionParametersCreateInfoExt => StructureType::eVideoEncodeH265SessionParametersCreateInfoExt,
                Self::eVideoEncodeH265SessionParametersAddInfoExt => StructureType::eVideoEncodeH265SessionParametersAddInfoExt,
                Self::eVideoEncodeH265VclFrameInfoExt => StructureType::eVideoEncodeH265VclFrameInfoExt,
                Self::eVideoEncodeH265DpbSlotInfoExt => StructureType::eVideoEncodeH265DpbSlotInfoExt,
                Self::eVideoEncodeH265NaluSliceSegmentInfoExt => StructureType::eVideoEncodeH265NaluSliceSegmentInfoExt,
                Self::eVideoEncodeH265EmitPictureParametersInfoExt => StructureType::eVideoEncodeH265EmitPictureParametersInfoExt,
                Self::eVideoEncodeH265ProfileInfoExt => StructureType::eVideoEncodeH265ProfileInfoExt,
                Self::eVideoEncodeH265ReferenceListsInfoExt => StructureType::eVideoEncodeH265ReferenceListsInfoExt,
                Self::eVideoEncodeH265RateControlInfoExt => StructureType::eVideoEncodeH265RateControlInfoExt,
                Self::eVideoEncodeH265RateControlLayerInfoExt => StructureType::eVideoEncodeH265RateControlLayerInfoExt,
                Self::eVideoDecodeH264CapabilitiesKhr => StructureType::eVideoDecodeH264CapabilitiesKhr,
                Self::eVideoDecodeH264PictureInfoKhr => StructureType::eVideoDecodeH264PictureInfoKhr,
                Self::eVideoDecodeH264ProfileInfoKhr => StructureType::eVideoDecodeH264ProfileInfoKhr,
                Self::eVideoDecodeH264SessionParametersCreateInfoKhr => StructureType::eVideoDecodeH264SessionParametersCreateInfoKhr,
                Self::eVideoDecodeH264SessionParametersAddInfoKhr => StructureType::eVideoDecodeH264SessionParametersAddInfoKhr,
                Self::eVideoDecodeH264DpbSlotInfoKhr => StructureType::eVideoDecodeH264DpbSlotInfoKhr,
                Self::eTextureLodGatherFormatPropertiesAmd => StructureType::eTextureLodGatherFormatPropertiesAmd,
                Self::eRenderingInfo => StructureType::eRenderingInfo,
                Self::eRenderingAttachmentInfo => StructureType::eRenderingAttachmentInfo,
                Self::ePipelineRenderingCreateInfo => StructureType::ePipelineRenderingCreateInfo,
                Self::ePhysicalDeviceDynamicRenderingFeatures => StructureType::ePhysicalDeviceDynamicRenderingFeatures,
                Self::eCommandBufferInheritanceRenderingInfo => StructureType::eCommandBufferInheritanceRenderingInfo,
                Self::eRenderingFragmentShadingRateAttachmentInfoKhr => StructureType::eRenderingFragmentShadingRateAttachmentInfoKhr,
                Self::eRenderingFragmentDensityMapAttachmentInfoExt => StructureType::eRenderingFragmentDensityMapAttachmentInfoExt,
                Self::eAttachmentSampleCountInfoAmd => StructureType::eAttachmentSampleCountInfoAmd,
                Self::eMultiviewPerViewAttributesInfoNvx => StructureType::eMultiviewPerViewAttributesInfoNvx,
                Self::eStreamDescriptorSurfaceCreateInfoGgp => StructureType::eStreamDescriptorSurfaceCreateInfoGgp,
                Self::ePhysicalDeviceCornerSampledImageFeaturesNv => StructureType::ePhysicalDeviceCornerSampledImageFeaturesNv,
                Self::ePrivateVendorInfoReservedOffset0Nv => StructureType::ePrivateVendorInfoReservedOffset0Nv,
                Self::eRenderPassMultiviewCreateInfo => StructureType::eRenderPassMultiviewCreateInfo,
                Self::ePhysicalDeviceMultiviewFeatures => StructureType::ePhysicalDeviceMultiviewFeatures,
                Self::ePhysicalDeviceMultiviewProperties => StructureType::ePhysicalDeviceMultiviewProperties,
                Self::eExternalMemoryImageCreateInfoNv => StructureType::eExternalMemoryImageCreateInfoNv,
                Self::eExportMemoryAllocateInfoNv => StructureType::eExportMemoryAllocateInfoNv,
                Self::eImportMemoryWin32HandleInfoNv => StructureType::eImportMemoryWin32HandleInfoNv,
                Self::eExportMemoryWin32HandleInfoNv => StructureType::eExportMemoryWin32HandleInfoNv,
                Self::eWin32KeyedMutexAcquireReleaseInfoNv => StructureType::eWin32KeyedMutexAcquireReleaseInfoNv,
                Self::ePhysicalDeviceFeatures2 => StructureType::ePhysicalDeviceFeatures2,
                Self::ePhysicalDeviceProperties2 => StructureType::ePhysicalDeviceProperties2,
                Self::eFormatProperties2 => StructureType::eFormatProperties2,
                Self::eImageFormatProperties2 => StructureType::eImageFormatProperties2,
                Self::ePhysicalDeviceImageFormatInfo2 => StructureType::ePhysicalDeviceImageFormatInfo2,
                Self::eQueueFamilyProperties2 => StructureType::eQueueFamilyProperties2,
                Self::ePhysicalDeviceMemoryProperties2 => StructureType::ePhysicalDeviceMemoryProperties2,
                Self::eSparseImageFormatProperties2 => StructureType::eSparseImageFormatProperties2,
                Self::ePhysicalDeviceSparseImageFormatInfo2 => StructureType::ePhysicalDeviceSparseImageFormatInfo2,
                Self::eMemoryAllocateFlagsInfo => StructureType::eMemoryAllocateFlagsInfo,
                Self::eDeviceGroupRenderPassBeginInfo => StructureType::eDeviceGroupRenderPassBeginInfo,
                Self::eDeviceGroupCommandBufferBeginInfo => StructureType::eDeviceGroupCommandBufferBeginInfo,
                Self::eDeviceGroupSubmitInfo => StructureType::eDeviceGroupSubmitInfo,
                Self::eDeviceGroupBindSparseInfo => StructureType::eDeviceGroupBindSparseInfo,
                Self::eDeviceGroupPresentCapabilitiesKhr => StructureType::eDeviceGroupPresentCapabilitiesKhr,
                Self::eImageSwapchainCreateInfoKhr => StructureType::eImageSwapchainCreateInfoKhr,
                Self::eBindImageMemorySwapchainInfoKhr => StructureType::eBindImageMemorySwapchainInfoKhr,
                Self::eAcquireNextImageInfoKhr => StructureType::eAcquireNextImageInfoKhr,
                Self::eDeviceGroupPresentInfoKhr => StructureType::eDeviceGroupPresentInfoKhr,
                Self::eDeviceGroupSwapchainCreateInfoKhr => StructureType::eDeviceGroupSwapchainCreateInfoKhr,
                Self::eBindBufferMemoryDeviceGroupInfo => StructureType::eBindBufferMemoryDeviceGroupInfo,
                Self::eBindImageMemoryDeviceGroupInfo => StructureType::eBindImageMemoryDeviceGroupInfo,
                Self::eValidationFlagsExt => StructureType::eValidationFlagsExt,
                Self::eViSurfaceCreateInfoNn => StructureType::eViSurfaceCreateInfoNn,
                Self::ePhysicalDeviceShaderDrawParametersFeatures => StructureType::ePhysicalDeviceShaderDrawParametersFeatures,
                Self::ePhysicalDeviceTextureCompressionAstcHdrFeatures => StructureType::ePhysicalDeviceTextureCompressionAstcHdrFeatures,
                Self::eImageViewAstcDecodeModeExt => StructureType::eImageViewAstcDecodeModeExt,
                Self::ePhysicalDeviceAstcDecodeFeaturesExt => StructureType::ePhysicalDeviceAstcDecodeFeaturesExt,
                Self::ePipelineRobustnessCreateInfoExt => StructureType::ePipelineRobustnessCreateInfoExt,
                Self::ePhysicalDevicePipelineRobustnessFeaturesExt => StructureType::ePhysicalDevicePipelineRobustnessFeaturesExt,
                Self::ePhysicalDevicePipelineRobustnessPropertiesExt => StructureType::ePhysicalDevicePipelineRobustnessPropertiesExt,
                Self::ePhysicalDeviceGroupProperties => StructureType::ePhysicalDeviceGroupProperties,
                Self::eDeviceGroupDeviceCreateInfo => StructureType::eDeviceGroupDeviceCreateInfo,
                Self::ePhysicalDeviceExternalImageFormatInfo => StructureType::ePhysicalDeviceExternalImageFormatInfo,
                Self::eExternalImageFormatProperties => StructureType::eExternalImageFormatProperties,
                Self::ePhysicalDeviceExternalBufferInfo => StructureType::ePhysicalDeviceExternalBufferInfo,
                Self::eExternalBufferProperties => StructureType::eExternalBufferProperties,
                Self::ePhysicalDeviceIdProperties => StructureType::ePhysicalDeviceIdProperties,
                Self::eExternalMemoryBufferCreateInfo => StructureType::eExternalMemoryBufferCreateInfo,
                Self::eExternalMemoryImageCreateInfo => StructureType::eExternalMemoryImageCreateInfo,
                Self::eExportMemoryAllocateInfo => StructureType::eExportMemoryAllocateInfo,
                Self::eImportMemoryWin32HandleInfoKhr => StructureType::eImportMemoryWin32HandleInfoKhr,
                Self::eExportMemoryWin32HandleInfoKhr => StructureType::eExportMemoryWin32HandleInfoKhr,
                Self::eMemoryWin32HandlePropertiesKhr => StructureType::eMemoryWin32HandlePropertiesKhr,
                Self::eMemoryGetWin32HandleInfoKhr => StructureType::eMemoryGetWin32HandleInfoKhr,
                Self::eImportMemoryFdInfoKhr => StructureType::eImportMemoryFdInfoKhr,
                Self::eMemoryFdPropertiesKhr => StructureType::eMemoryFdPropertiesKhr,
                Self::eMemoryGetFdInfoKhr => StructureType::eMemoryGetFdInfoKhr,
                Self::eWin32KeyedMutexAcquireReleaseInfoKhr => StructureType::eWin32KeyedMutexAcquireReleaseInfoKhr,
                Self::ePhysicalDeviceExternalSemaphoreInfo => StructureType::ePhysicalDeviceExternalSemaphoreInfo,
                Self::eExternalSemaphoreProperties => StructureType::eExternalSemaphoreProperties,
                Self::eExportSemaphoreCreateInfo => StructureType::eExportSemaphoreCreateInfo,
                Self::eImportSemaphoreWin32HandleInfoKhr => StructureType::eImportSemaphoreWin32HandleInfoKhr,
                Self::eExportSemaphoreWin32HandleInfoKhr => StructureType::eExportSemaphoreWin32HandleInfoKhr,
                Self::eD3D12FenceSubmitInfoKhr => StructureType::eD3D12FenceSubmitInfoKhr,
                Self::eSemaphoreGetWin32HandleInfoKhr => StructureType::eSemaphoreGetWin32HandleInfoKhr,
                Self::eImportSemaphoreFdInfoKhr => StructureType::eImportSemaphoreFdInfoKhr,
                Self::eSemaphoreGetFdInfoKhr => StructureType::eSemaphoreGetFdInfoKhr,
                Self::ePhysicalDevicePushDescriptorPropertiesKhr => StructureType::ePhysicalDevicePushDescriptorPropertiesKhr,
                Self::eCommandBufferInheritanceConditionalRenderingInfoExt => StructureType::eCommandBufferInheritanceConditionalRenderingInfoExt,
                Self::ePhysicalDeviceConditionalRenderingFeaturesExt => StructureType::ePhysicalDeviceConditionalRenderingFeaturesExt,
                Self::eConditionalRenderingBeginInfoExt => StructureType::eConditionalRenderingBeginInfoExt,
                Self::ePhysicalDeviceShaderFloat16Int8Features => StructureType::ePhysicalDeviceShaderFloat16Int8Features,
                Self::ePhysicalDevice16bitStorageFeatures => StructureType::ePhysicalDevice16bitStorageFeatures,
                Self::ePresentRegionsKhr => StructureType::ePresentRegionsKhr,
                Self::eDescriptorUpdateTemplateCreateInfo => StructureType::eDescriptorUpdateTemplateCreateInfo,
                Self::ePipelineViewportWScalingStateCreateInfoNv => StructureType::ePipelineViewportWScalingStateCreateInfoNv,
                Self::eSurfaceCapabilities2Ext => StructureType::eSurfaceCapabilities2Ext,
                Self::eDisplayPowerInfoExt => StructureType::eDisplayPowerInfoExt,
                Self::eDeviceEventInfoExt => StructureType::eDeviceEventInfoExt,
                Self::eDisplayEventInfoExt => StructureType::eDisplayEventInfoExt,
                Self::eSwapchainCounterCreateInfoExt => StructureType::eSwapchainCounterCreateInfoExt,
                Self::ePresentTimesInfoGoogle => StructureType::ePresentTimesInfoGoogle,
                Self::ePhysicalDeviceSubgroupProperties => StructureType::ePhysicalDeviceSubgroupProperties,
                Self::ePhysicalDeviceMultiviewPerViewAttributesPropertiesNvx => StructureType::ePhysicalDeviceMultiviewPerViewAttributesPropertiesNvx,
                Self::ePipelineViewportSwizzleStateCreateInfoNv => StructureType::ePipelineViewportSwizzleStateCreateInfoNv,
                Self::ePhysicalDeviceDiscardRectanglePropertiesExt => StructureType::ePhysicalDeviceDiscardRectanglePropertiesExt,
                Self::ePipelineDiscardRectangleStateCreateInfoExt => StructureType::ePipelineDiscardRectangleStateCreateInfoExt,
                Self::ePhysicalDeviceConservativeRasterizationPropertiesExt => StructureType::ePhysicalDeviceConservativeRasterizationPropertiesExt,
                Self::ePipelineRasterizationConservativeStateCreateInfoExt => StructureType::ePipelineRasterizationConservativeStateCreateInfoExt,
                Self::ePhysicalDeviceDepthClipEnableFeaturesExt => StructureType::ePhysicalDeviceDepthClipEnableFeaturesExt,
                Self::ePipelineRasterizationDepthClipStateCreateInfoExt => StructureType::ePipelineRasterizationDepthClipStateCreateInfoExt,
                Self::eHdrMetadataExt => StructureType::eHdrMetadataExt,
                Self::ePhysicalDeviceImagelessFramebufferFeatures => StructureType::ePhysicalDeviceImagelessFramebufferFeatures,
                Self::eFramebufferAttachmentsCreateInfo => StructureType::eFramebufferAttachmentsCreateInfo,
                Self::eFramebufferAttachmentImageInfo => StructureType::eFramebufferAttachmentImageInfo,
                Self::eRenderPassAttachmentBeginInfo => StructureType::eRenderPassAttachmentBeginInfo,
                Self::eAttachmentDescription2 => StructureType::eAttachmentDescription2,
                Self::eAttachmentReference2 => StructureType::eAttachmentReference2,
                Self::eSubpassDescription2 => StructureType::eSubpassDescription2,
                Self::eSubpassDependency2 => StructureType::eSubpassDependency2,
                Self::eRenderPassCreateInfo2 => StructureType::eRenderPassCreateInfo2,
                Self::eSubpassBeginInfo => StructureType::eSubpassBeginInfo,
                Self::eSubpassEndInfo => StructureType::eSubpassEndInfo,
                Self::eSharedPresentSurfaceCapabilitiesKhr => StructureType::eSharedPresentSurfaceCapabilitiesKhr,
                Self::ePhysicalDeviceExternalFenceInfo => StructureType::ePhysicalDeviceExternalFenceInfo,
                Self::eExternalFenceProperties => StructureType::eExternalFenceProperties,
                Self::eExportFenceCreateInfo => StructureType::eExportFenceCreateInfo,
                Self::eImportFenceWin32HandleInfoKhr => StructureType::eImportFenceWin32HandleInfoKhr,
                Self::eExportFenceWin32HandleInfoKhr => StructureType::eExportFenceWin32HandleInfoKhr,
                Self::eFenceGetWin32HandleInfoKhr => StructureType::eFenceGetWin32HandleInfoKhr,
                Self::eImportFenceFdInfoKhr => StructureType::eImportFenceFdInfoKhr,
                Self::eFenceGetFdInfoKhr => StructureType::eFenceGetFdInfoKhr,
                Self::ePhysicalDevicePerformanceQueryFeaturesKhr => StructureType::ePhysicalDevicePerformanceQueryFeaturesKhr,
                Self::ePhysicalDevicePerformanceQueryPropertiesKhr => StructureType::ePhysicalDevicePerformanceQueryPropertiesKhr,
                Self::eQueryPoolPerformanceCreateInfoKhr => StructureType::eQueryPoolPerformanceCreateInfoKhr,
                Self::ePerformanceQuerySubmitInfoKhr => StructureType::ePerformanceQuerySubmitInfoKhr,
                Self::eAcquireProfilingLockInfoKhr => StructureType::eAcquireProfilingLockInfoKhr,
                Self::ePerformanceCounterKhr => StructureType::ePerformanceCounterKhr,
                Self::ePerformanceCounterDescriptionKhr => StructureType::ePerformanceCounterDescriptionKhr,
                Self::ePerformanceQueryReservationInfoKhr => StructureType::ePerformanceQueryReservationInfoKhr,
                Self::ePhysicalDevicePointClippingProperties => StructureType::ePhysicalDevicePointClippingProperties,
                Self::eRenderPassInputAttachmentAspectCreateInfo => StructureType::eRenderPassInputAttachmentAspectCreateInfo,
                Self::eImageViewUsageCreateInfo => StructureType::eImageViewUsageCreateInfo,
                Self::ePipelineTessellationDomainOriginStateCreateInfo => StructureType::ePipelineTessellationDomainOriginStateCreateInfo,
                Self::ePhysicalDeviceSurfaceInfo2Khr => StructureType::ePhysicalDeviceSurfaceInfo2Khr,
                Self::eSurfaceCapabilities2Khr => StructureType::eSurfaceCapabilities2Khr,
                Self::eSurfaceFormat2Khr => StructureType::eSurfaceFormat2Khr,
                Self::ePhysicalDeviceVariablePointersFeatures => StructureType::ePhysicalDeviceVariablePointersFeatures,
                Self::eDisplayProperties2Khr => StructureType::eDisplayProperties2Khr,
                Self::eDisplayPlaneProperties2Khr => StructureType::eDisplayPlaneProperties2Khr,
                Self::eDisplayModeProperties2Khr => StructureType::eDisplayModeProperties2Khr,
                Self::eDisplayPlaneInfo2Khr => StructureType::eDisplayPlaneInfo2Khr,
                Self::eDisplayPlaneCapabilities2Khr => StructureType::eDisplayPlaneCapabilities2Khr,
                Self::eIosSurfaceCreateInfoMvk => StructureType::eIosSurfaceCreateInfoMvk,
                Self::eMacosSurfaceCreateInfoMvk => StructureType::eMacosSurfaceCreateInfoMvk,
                Self::eMemoryDedicatedRequirements => StructureType::eMemoryDedicatedRequirements,
                Self::eMemoryDedicatedAllocateInfo => StructureType::eMemoryDedicatedAllocateInfo,
                Self::eDebugUtilsObjectNameInfoExt => StructureType::eDebugUtilsObjectNameInfoExt,
                Self::eDebugUtilsObjectTagInfoExt => StructureType::eDebugUtilsObjectTagInfoExt,
                Self::eDebugUtilsLabelExt => StructureType::eDebugUtilsLabelExt,
                Self::eDebugUtilsMessengerCallbackDataExt => StructureType::eDebugUtilsMessengerCallbackDataExt,
                Self::eDebugUtilsMessengerCreateInfoExt => StructureType::eDebugUtilsMessengerCreateInfoExt,
                Self::eAndroidHardwareBufferUsageAndroid => StructureType::eAndroidHardwareBufferUsageAndroid,
                Self::eAndroidHardwareBufferPropertiesAndroid => StructureType::eAndroidHardwareBufferPropertiesAndroid,
                Self::eAndroidHardwareBufferFormatPropertiesAndroid => StructureType::eAndroidHardwareBufferFormatPropertiesAndroid,
                Self::eImportAndroidHardwareBufferInfoAndroid => StructureType::eImportAndroidHardwareBufferInfoAndroid,
                Self::eMemoryGetAndroidHardwareBufferInfoAndroid => StructureType::eMemoryGetAndroidHardwareBufferInfoAndroid,
                Self::eExternalFormatAndroid => StructureType::eExternalFormatAndroid,
                Self::eAndroidHardwareBufferFormatProperties2Android => StructureType::eAndroidHardwareBufferFormatProperties2Android,
                Self::ePhysicalDeviceSamplerFilterMinmaxProperties => StructureType::ePhysicalDeviceSamplerFilterMinmaxProperties,
                Self::eSamplerReductionModeCreateInfo => StructureType::eSamplerReductionModeCreateInfo,
                Self::ePhysicalDeviceInlineUniformBlockFeatures => StructureType::ePhysicalDeviceInlineUniformBlockFeatures,
                Self::ePhysicalDeviceInlineUniformBlockProperties => StructureType::ePhysicalDeviceInlineUniformBlockProperties,
                Self::eWriteDescriptorSetInlineUniformBlock => StructureType::eWriteDescriptorSetInlineUniformBlock,
                Self::eDescriptorPoolInlineUniformBlockCreateInfo => StructureType::eDescriptorPoolInlineUniformBlockCreateInfo,
                Self::eSampleLocationsInfoExt => StructureType::eSampleLocationsInfoExt,
                Self::eRenderPassSampleLocationsBeginInfoExt => StructureType::eRenderPassSampleLocationsBeginInfoExt,
                Self::ePipelineSampleLocationsStateCreateInfoExt => StructureType::ePipelineSampleLocationsStateCreateInfoExt,
                Self::ePhysicalDeviceSampleLocationsPropertiesExt => StructureType::ePhysicalDeviceSampleLocationsPropertiesExt,
                Self::eMultisamplePropertiesExt => StructureType::eMultisamplePropertiesExt,
                Self::eProtectedSubmitInfo => StructureType::eProtectedSubmitInfo,
                Self::ePhysicalDeviceProtectedMemoryFeatures => StructureType::ePhysicalDeviceProtectedMemoryFeatures,
                Self::ePhysicalDeviceProtectedMemoryProperties => StructureType::ePhysicalDeviceProtectedMemoryProperties,
                Self::eDeviceQueueInfo2 => StructureType::eDeviceQueueInfo2,
                Self::eBufferMemoryRequirementsInfo2 => StructureType::eBufferMemoryRequirementsInfo2,
                Self::eImageMemoryRequirementsInfo2 => StructureType::eImageMemoryRequirementsInfo2,
                Self::eImageSparseMemoryRequirementsInfo2 => StructureType::eImageSparseMemoryRequirementsInfo2,
                Self::eMemoryRequirements2 => StructureType::eMemoryRequirements2,
                Self::eSparseImageMemoryRequirements2 => StructureType::eSparseImageMemoryRequirements2,
                Self::eImageFormatListCreateInfo => StructureType::eImageFormatListCreateInfo,
                Self::ePhysicalDeviceBlendOperationAdvancedFeaturesExt => StructureType::ePhysicalDeviceBlendOperationAdvancedFeaturesExt,
                Self::ePhysicalDeviceBlendOperationAdvancedPropertiesExt => StructureType::ePhysicalDeviceBlendOperationAdvancedPropertiesExt,
                Self::ePipelineColourBlendAdvancedStateCreateInfoExt => StructureType::ePipelineColourBlendAdvancedStateCreateInfoExt,
                Self::ePipelineCoverageToColourStateCreateInfoNv => StructureType::ePipelineCoverageToColourStateCreateInfoNv,
                Self::eAccelerationStructureBuildGeometryInfoKhr => StructureType::eAccelerationStructureBuildGeometryInfoKhr,
                Self::eAccelerationStructureDeviceAddressInfoKhr => StructureType::eAccelerationStructureDeviceAddressInfoKhr,
                Self::eAccelerationStructureGeometryAabbsDataKhr => StructureType::eAccelerationStructureGeometryAabbsDataKhr,
                Self::eAccelerationStructureGeometryInstancesDataKhr => StructureType::eAccelerationStructureGeometryInstancesDataKhr,
                Self::eAccelerationStructureGeometryTrianglesDataKhr => StructureType::eAccelerationStructureGeometryTrianglesDataKhr,
                Self::eAccelerationStructureGeometryKhr => StructureType::eAccelerationStructureGeometryKhr,
                Self::eWriteDescriptorSetAccelerationStructureKhr => StructureType::eWriteDescriptorSetAccelerationStructureKhr,
                Self::eAccelerationStructureVersionInfoKhr => StructureType::eAccelerationStructureVersionInfoKhr,
                Self::eCopyAccelerationStructureInfoKhr => StructureType::eCopyAccelerationStructureInfoKhr,
                Self::eCopyAccelerationStructureToMemoryInfoKhr => StructureType::eCopyAccelerationStructureToMemoryInfoKhr,
                Self::eCopyMemoryToAccelerationStructureInfoKhr => StructureType::eCopyMemoryToAccelerationStructureInfoKhr,
                Self::ePhysicalDeviceAccelerationStructureFeaturesKhr => StructureType::ePhysicalDeviceAccelerationStructureFeaturesKhr,
                Self::ePhysicalDeviceAccelerationStructurePropertiesKhr => StructureType::ePhysicalDeviceAccelerationStructurePropertiesKhr,
                Self::eRayTracingPipelineCreateInfoKhr => StructureType::eRayTracingPipelineCreateInfoKhr,
                Self::eRayTracingShaderGroupCreateInfoKhr => StructureType::eRayTracingShaderGroupCreateInfoKhr,
                Self::eAccelerationStructureCreateInfoKhr => StructureType::eAccelerationStructureCreateInfoKhr,
                Self::eRayTracingPipelineInterfaceCreateInfoKhr => StructureType::eRayTracingPipelineInterfaceCreateInfoKhr,
                Self::eAccelerationStructureBuildSizesInfoKhr => StructureType::eAccelerationStructureBuildSizesInfoKhr,
                Self::ePipelineCoverageModulationStateCreateInfoNv => StructureType::ePipelineCoverageModulationStateCreateInfoNv,
                Self::ePhysicalDeviceShaderSmBuiltinsFeaturesNv => StructureType::ePhysicalDeviceShaderSmBuiltinsFeaturesNv,
                Self::ePhysicalDeviceShaderSmBuiltinsPropertiesNv => StructureType::ePhysicalDeviceShaderSmBuiltinsPropertiesNv,
                Self::eSamplerYcbcrConversionCreateInfo => StructureType::eSamplerYcbcrConversionCreateInfo,
                Self::eSamplerYcbcrConversionInfo => StructureType::eSamplerYcbcrConversionInfo,
                Self::eBindImagePlaneMemoryInfo => StructureType::eBindImagePlaneMemoryInfo,
                Self::eImagePlaneMemoryRequirementsInfo => StructureType::eImagePlaneMemoryRequirementsInfo,
                Self::ePhysicalDeviceSamplerYcbcrConversionFeatures => StructureType::ePhysicalDeviceSamplerYcbcrConversionFeatures,
                Self::eSamplerYcbcrConversionImageFormatProperties => StructureType::eSamplerYcbcrConversionImageFormatProperties,
                Self::eBindBufferMemoryInfo => StructureType::eBindBufferMemoryInfo,
                Self::eBindImageMemoryInfo => StructureType::eBindImageMemoryInfo,
                Self::eDrmFormatModifierPropertiesListExt => StructureType::eDrmFormatModifierPropertiesListExt,
                Self::ePhysicalDeviceImageDrmFormatModifierInfoExt => StructureType::ePhysicalDeviceImageDrmFormatModifierInfoExt,
                Self::eImageDrmFormatModifierListCreateInfoExt => StructureType::eImageDrmFormatModifierListCreateInfoExt,
                Self::eImageDrmFormatModifierExplicitCreateInfoExt => StructureType::eImageDrmFormatModifierExplicitCreateInfoExt,
                Self::eImageDrmFormatModifierPropertiesExt => StructureType::eImageDrmFormatModifierPropertiesExt,
                Self::eDrmFormatModifierPropertiesList2Ext => StructureType::eDrmFormatModifierPropertiesList2Ext,
                Self::eValidationCacheCreateInfoExt => StructureType::eValidationCacheCreateInfoExt,
                Self::eShaderModuleValidationCacheCreateInfoExt => StructureType::eShaderModuleValidationCacheCreateInfoExt,
                Self::eDescriptorSetLayoutBindingFlagsCreateInfo => StructureType::eDescriptorSetLayoutBindingFlagsCreateInfo,
                Self::ePhysicalDeviceDescriptorIndexingFeatures => StructureType::ePhysicalDeviceDescriptorIndexingFeatures,
                Self::ePhysicalDeviceDescriptorIndexingProperties => StructureType::ePhysicalDeviceDescriptorIndexingProperties,
                Self::eDescriptorSetVariableDescriptorCountAllocateInfo => StructureType::eDescriptorSetVariableDescriptorCountAllocateInfo,
                Self::eDescriptorSetVariableDescriptorCountLayoutSupport => StructureType::eDescriptorSetVariableDescriptorCountLayoutSupport,
                Self::ePhysicalDevicePortabilitySubsetFeaturesKhr => StructureType::ePhysicalDevicePortabilitySubsetFeaturesKhr,
                Self::ePhysicalDevicePortabilitySubsetPropertiesKhr => StructureType::ePhysicalDevicePortabilitySubsetPropertiesKhr,
                Self::ePipelineViewportShadingRateImageStateCreateInfoNv => StructureType::ePipelineViewportShadingRateImageStateCreateInfoNv,
                Self::ePhysicalDeviceShadingRateImageFeaturesNv => StructureType::ePhysicalDeviceShadingRateImageFeaturesNv,
                Self::ePhysicalDeviceShadingRateImagePropertiesNv => StructureType::ePhysicalDeviceShadingRateImagePropertiesNv,
                Self::ePipelineViewportCoarseSampleOrderStateCreateInfoNv => StructureType::ePipelineViewportCoarseSampleOrderStateCreateInfoNv,
                Self::eRayTracingPipelineCreateInfoNv => StructureType::eRayTracingPipelineCreateInfoNv,
                Self::eAccelerationStructureCreateInfoNv => StructureType::eAccelerationStructureCreateInfoNv,
                Self::eGeometryNv => StructureType::eGeometryNv,
                Self::eGeometryTrianglesNv => StructureType::eGeometryTrianglesNv,
                Self::eGeometryAabbNv => StructureType::eGeometryAabbNv,
                Self::eBindAccelerationStructureMemoryInfoNv => StructureType::eBindAccelerationStructureMemoryInfoNv,
                Self::eWriteDescriptorSetAccelerationStructureNv => StructureType::eWriteDescriptorSetAccelerationStructureNv,
                Self::eAccelerationStructureMemoryRequirementsInfoNv => StructureType::eAccelerationStructureMemoryRequirementsInfoNv,
                Self::ePhysicalDeviceRayTracingPropertiesNv => StructureType::ePhysicalDeviceRayTracingPropertiesNv,
                Self::eRayTracingShaderGroupCreateInfoNv => StructureType::eRayTracingShaderGroupCreateInfoNv,
                Self::eAccelerationStructureInfoNv => StructureType::eAccelerationStructureInfoNv,
                Self::ePhysicalDeviceRepresentativeFragmentTestFeaturesNv => StructureType::ePhysicalDeviceRepresentativeFragmentTestFeaturesNv,
                Self::ePipelineRepresentativeFragmentTestStateCreateInfoNv => StructureType::ePipelineRepresentativeFragmentTestStateCreateInfoNv,
                Self::ePhysicalDeviceMaintenance3Properties => StructureType::ePhysicalDeviceMaintenance3Properties,
                Self::eDescriptorSetLayoutSupport => StructureType::eDescriptorSetLayoutSupport,
                Self::ePhysicalDeviceImageViewImageFormatInfoExt => StructureType::ePhysicalDeviceImageViewImageFormatInfoExt,
                Self::eFilterCubicImageViewImageFormatPropertiesExt => StructureType::eFilterCubicImageViewImageFormatPropertiesExt,
                Self::eDeviceQueueGlobalPriorityCreateInfoKhr => StructureType::eDeviceQueueGlobalPriorityCreateInfoKhr,
                Self::ePhysicalDeviceShaderSubgroupExtendedTypesFeatures => StructureType::ePhysicalDeviceShaderSubgroupExtendedTypesFeatures,
                Self::ePhysicalDevice8bitStorageFeatures => StructureType::ePhysicalDevice8bitStorageFeatures,
                Self::eImportMemoryHostPointerInfoExt => StructureType::eImportMemoryHostPointerInfoExt,
                Self::eMemoryHostPointerPropertiesExt => StructureType::eMemoryHostPointerPropertiesExt,
                Self::ePhysicalDeviceExternalMemoryHostPropertiesExt => StructureType::ePhysicalDeviceExternalMemoryHostPropertiesExt,
                Self::ePhysicalDeviceShaderAtomicInt64Features => StructureType::ePhysicalDeviceShaderAtomicInt64Features,
                Self::ePhysicalDeviceShaderClockFeaturesKhr => StructureType::ePhysicalDeviceShaderClockFeaturesKhr,
                Self::ePipelineCompilerControlCreateInfoAmd => StructureType::ePipelineCompilerControlCreateInfoAmd,
                Self::eCalibratedTimestampInfoExt => StructureType::eCalibratedTimestampInfoExt,
                Self::ePhysicalDeviceShaderCorePropertiesAmd => StructureType::ePhysicalDeviceShaderCorePropertiesAmd,
                Self::eVideoDecodeH265CapabilitiesKhr => StructureType::eVideoDecodeH265CapabilitiesKhr,
                Self::eVideoDecodeH265SessionParametersCreateInfoKhr => StructureType::eVideoDecodeH265SessionParametersCreateInfoKhr,
                Self::eVideoDecodeH265SessionParametersAddInfoKhr => StructureType::eVideoDecodeH265SessionParametersAddInfoKhr,
                Self::eVideoDecodeH265ProfileInfoKhr => StructureType::eVideoDecodeH265ProfileInfoKhr,
                Self::eVideoDecodeH265PictureInfoKhr => StructureType::eVideoDecodeH265PictureInfoKhr,
                Self::eVideoDecodeH265DpbSlotInfoKhr => StructureType::eVideoDecodeH265DpbSlotInfoKhr,
                Self::eDeviceMemoryOverallocationCreateInfoAmd => StructureType::eDeviceMemoryOverallocationCreateInfoAmd,
                Self::ePhysicalDeviceVertexAttributeDivisorPropertiesExt => StructureType::ePhysicalDeviceVertexAttributeDivisorPropertiesExt,
                Self::ePipelineVertexInputDivisorStateCreateInfoExt => StructureType::ePipelineVertexInputDivisorStateCreateInfoExt,
                Self::ePhysicalDeviceVertexAttributeDivisorFeaturesExt => StructureType::ePhysicalDeviceVertexAttributeDivisorFeaturesExt,
                Self::ePresentFrameTokenGgp => StructureType::ePresentFrameTokenGgp,
                Self::ePipelineCreationFeedbackCreateInfo => StructureType::ePipelineCreationFeedbackCreateInfo,
                Self::ePhysicalDeviceDriverProperties => StructureType::ePhysicalDeviceDriverProperties,
                Self::ePhysicalDeviceFloatControlsProperties => StructureType::ePhysicalDeviceFloatControlsProperties,
                Self::ePhysicalDeviceDepthStencilResolveProperties => StructureType::ePhysicalDeviceDepthStencilResolveProperties,
                Self::eSubpassDescriptionDepthStencilResolve => StructureType::eSubpassDescriptionDepthStencilResolve,
                Self::ePhysicalDeviceComputeShaderDerivativesFeaturesNv => StructureType::ePhysicalDeviceComputeShaderDerivativesFeaturesNv,
                Self::ePhysicalDeviceMeshShaderFeaturesNv => StructureType::ePhysicalDeviceMeshShaderFeaturesNv,
                Self::ePhysicalDeviceMeshShaderPropertiesNv => StructureType::ePhysicalDeviceMeshShaderPropertiesNv,
                Self::ePhysicalDeviceFragmentShaderBarycentricFeaturesKhr => StructureType::ePhysicalDeviceFragmentShaderBarycentricFeaturesKhr,
                Self::ePhysicalDeviceShaderImageFootprintFeaturesNv => StructureType::ePhysicalDeviceShaderImageFootprintFeaturesNv,
                Self::ePipelineViewportExclusiveScissorStateCreateInfoNv => StructureType::ePipelineViewportExclusiveScissorStateCreateInfoNv,
                Self::ePhysicalDeviceExclusiveScissorFeaturesNv => StructureType::ePhysicalDeviceExclusiveScissorFeaturesNv,
                Self::eCheckpointDataNv => StructureType::eCheckpointDataNv,
                Self::eQueueFamilyCheckpointPropertiesNv => StructureType::eQueueFamilyCheckpointPropertiesNv,
                Self::ePhysicalDeviceTimelineSemaphoreFeatures => StructureType::ePhysicalDeviceTimelineSemaphoreFeatures,
                Self::ePhysicalDeviceTimelineSemaphoreProperties => StructureType::ePhysicalDeviceTimelineSemaphoreProperties,
                Self::eSemaphoreTypeCreateInfo => StructureType::eSemaphoreTypeCreateInfo,
                Self::eTimelineSemaphoreSubmitInfo => StructureType::eTimelineSemaphoreSubmitInfo,
                Self::eSemaphoreWaitInfo => StructureType::eSemaphoreWaitInfo,
                Self::eSemaphoreSignalInfo => StructureType::eSemaphoreSignalInfo,
                Self::ePhysicalDeviceShaderIntegerFunctions2FeaturesIntel => StructureType::ePhysicalDeviceShaderIntegerFunctions2FeaturesIntel,
                Self::eQueryPoolPerformanceQueryCreateInfoIntel => StructureType::eQueryPoolPerformanceQueryCreateInfoIntel,
                Self::eInitializePerformanceApiInfoIntel => StructureType::eInitializePerformanceApiInfoIntel,
                Self::ePerformanceMarkerInfoIntel => StructureType::ePerformanceMarkerInfoIntel,
                Self::ePerformanceStreamMarkerInfoIntel => StructureType::ePerformanceStreamMarkerInfoIntel,
                Self::ePerformanceOverrideInfoIntel => StructureType::ePerformanceOverrideInfoIntel,
                Self::ePerformanceConfigurationAcquireInfoIntel => StructureType::ePerformanceConfigurationAcquireInfoIntel,
                Self::ePhysicalDeviceVulkanMemoryModelFeatures => StructureType::ePhysicalDeviceVulkanMemoryModelFeatures,
                Self::ePhysicalDevicePciBusInfoPropertiesExt => StructureType::ePhysicalDevicePciBusInfoPropertiesExt,
                Self::eDisplayNativeHdrSurfaceCapabilitiesAmd => StructureType::eDisplayNativeHdrSurfaceCapabilitiesAmd,
                Self::eSwapchainDisplayNativeHdrCreateInfoAmd => StructureType::eSwapchainDisplayNativeHdrCreateInfoAmd,
                Self::eImagepipeSurfaceCreateInfoFuchsia => StructureType::eImagepipeSurfaceCreateInfoFuchsia,
                Self::ePhysicalDeviceShaderTerminateInvocationFeatures => StructureType::ePhysicalDeviceShaderTerminateInvocationFeatures,
                Self::eMetalSurfaceCreateInfoExt => StructureType::eMetalSurfaceCreateInfoExt,
                Self::ePhysicalDeviceFragmentDensityMapFeaturesExt => StructureType::ePhysicalDeviceFragmentDensityMapFeaturesExt,
                Self::ePhysicalDeviceFragmentDensityMapPropertiesExt => StructureType::ePhysicalDeviceFragmentDensityMapPropertiesExt,
                Self::eRenderPassFragmentDensityMapCreateInfoExt => StructureType::eRenderPassFragmentDensityMapCreateInfoExt,
                Self::ePhysicalDeviceScalarBlockLayoutFeatures => StructureType::ePhysicalDeviceScalarBlockLayoutFeatures,
                Self::ePhysicalDeviceSubgroupSizeControlProperties => StructureType::ePhysicalDeviceSubgroupSizeControlProperties,
                Self::ePipelineShaderStageRequiredSubgroupSizeCreateInfo => StructureType::ePipelineShaderStageRequiredSubgroupSizeCreateInfo,
                Self::ePhysicalDeviceSubgroupSizeControlFeatures => StructureType::ePhysicalDeviceSubgroupSizeControlFeatures,
                Self::eFragmentShadingRateAttachmentInfoKhr => StructureType::eFragmentShadingRateAttachmentInfoKhr,
                Self::ePipelineFragmentShadingRateStateCreateInfoKhr => StructureType::ePipelineFragmentShadingRateStateCreateInfoKhr,
                Self::ePhysicalDeviceFragmentShadingRatePropertiesKhr => StructureType::ePhysicalDeviceFragmentShadingRatePropertiesKhr,
                Self::ePhysicalDeviceFragmentShadingRateFeaturesKhr => StructureType::ePhysicalDeviceFragmentShadingRateFeaturesKhr,
                Self::ePhysicalDeviceFragmentShadingRateKhr => StructureType::ePhysicalDeviceFragmentShadingRateKhr,
                Self::ePhysicalDeviceShaderCoreProperties2Amd => StructureType::ePhysicalDeviceShaderCoreProperties2Amd,
                Self::ePhysicalDeviceCoherentMemoryFeaturesAmd => StructureType::ePhysicalDeviceCoherentMemoryFeaturesAmd,
                Self::ePhysicalDeviceShaderImageAtomicInt64FeaturesExt => StructureType::ePhysicalDeviceShaderImageAtomicInt64FeaturesExt,
                Self::ePhysicalDeviceMemoryBudgetPropertiesExt => StructureType::ePhysicalDeviceMemoryBudgetPropertiesExt,
                Self::ePhysicalDeviceMemoryPriorityFeaturesExt => StructureType::ePhysicalDeviceMemoryPriorityFeaturesExt,
                Self::eMemoryPriorityAllocateInfoExt => StructureType::eMemoryPriorityAllocateInfoExt,
                Self::eSurfaceProtectedCapabilitiesKhr => StructureType::eSurfaceProtectedCapabilitiesKhr,
                Self::ePhysicalDeviceDedicatedAllocationImageAliasingFeaturesNv => StructureType::ePhysicalDeviceDedicatedAllocationImageAliasingFeaturesNv,
                Self::ePhysicalDeviceSeparateDepthStencilLayoutsFeatures => StructureType::ePhysicalDeviceSeparateDepthStencilLayoutsFeatures,
                Self::eAttachmentReferenceStencilLayout => StructureType::eAttachmentReferenceStencilLayout,
                Self::eAttachmentDescriptionStencilLayout => StructureType::eAttachmentDescriptionStencilLayout,
                Self::ePhysicalDeviceBufferDeviceAddressFeaturesExt => StructureType::ePhysicalDeviceBufferDeviceAddressFeaturesExt,
                Self::eBufferDeviceAddressInfo => StructureType::eBufferDeviceAddressInfo,
                Self::eBufferDeviceAddressCreateInfoExt => StructureType::eBufferDeviceAddressCreateInfoExt,
                Self::ePhysicalDeviceToolProperties => StructureType::ePhysicalDeviceToolProperties,
                Self::eImageStencilUsageCreateInfo => StructureType::eImageStencilUsageCreateInfo,
                Self::eValidationFeaturesExt => StructureType::eValidationFeaturesExt,
                Self::ePhysicalDevicePresentWaitFeaturesKhr => StructureType::ePhysicalDevicePresentWaitFeaturesKhr,
                Self::ePhysicalDeviceCooperativeMatrixFeaturesNv => StructureType::ePhysicalDeviceCooperativeMatrixFeaturesNv,
                Self::eCooperativeMatrixPropertiesNv => StructureType::eCooperativeMatrixPropertiesNv,
                Self::ePhysicalDeviceCooperativeMatrixPropertiesNv => StructureType::ePhysicalDeviceCooperativeMatrixPropertiesNv,
                Self::ePhysicalDeviceCoverageReductionModeFeaturesNv => StructureType::ePhysicalDeviceCoverageReductionModeFeaturesNv,
                Self::ePipelineCoverageReductionStateCreateInfoNv => StructureType::ePipelineCoverageReductionStateCreateInfoNv,
                Self::eFramebufferMixedSamplesCombinationNv => StructureType::eFramebufferMixedSamplesCombinationNv,
                Self::ePhysicalDeviceFragmentShaderInterlockFeaturesExt => StructureType::ePhysicalDeviceFragmentShaderInterlockFeaturesExt,
                Self::ePhysicalDeviceYcbcrImageArraysFeaturesExt => StructureType::ePhysicalDeviceYcbcrImageArraysFeaturesExt,
                Self::ePhysicalDeviceUniformBufferStandardLayoutFeatures => StructureType::ePhysicalDeviceUniformBufferStandardLayoutFeatures,
                Self::ePhysicalDeviceProvokingVertexFeaturesExt => StructureType::ePhysicalDeviceProvokingVertexFeaturesExt,
                Self::ePipelineRasterizationProvokingVertexStateCreateInfoExt => StructureType::ePipelineRasterizationProvokingVertexStateCreateInfoExt,
                Self::ePhysicalDeviceProvokingVertexPropertiesExt => StructureType::ePhysicalDeviceProvokingVertexPropertiesExt,
                Self::eSurfaceFullScreenExclusiveInfoExt => StructureType::eSurfaceFullScreenExclusiveInfoExt,
                Self::eSurfaceFullScreenExclusiveWin32InfoExt => StructureType::eSurfaceFullScreenExclusiveWin32InfoExt,
                Self::eSurfaceCapabilitiesFullScreenExclusiveExt => StructureType::eSurfaceCapabilitiesFullScreenExclusiveExt,
                Self::eHeadlessSurfaceCreateInfoExt => StructureType::eHeadlessSurfaceCreateInfoExt,
                Self::ePhysicalDeviceBufferDeviceAddressFeatures => StructureType::ePhysicalDeviceBufferDeviceAddressFeatures,
                Self::eBufferOpaqueCaptureAddressCreateInfo => StructureType::eBufferOpaqueCaptureAddressCreateInfo,
                Self::eMemoryOpaqueCaptureAddressAllocateInfo => StructureType::eMemoryOpaqueCaptureAddressAllocateInfo,
                Self::eDeviceMemoryOpaqueCaptureAddressInfo => StructureType::eDeviceMemoryOpaqueCaptureAddressInfo,
                Self::ePhysicalDeviceLineRasterizationFeaturesExt => StructureType::ePhysicalDeviceLineRasterizationFeaturesExt,
                Self::ePipelineRasterizationLineStateCreateInfoExt => StructureType::ePipelineRasterizationLineStateCreateInfoExt,
                Self::ePhysicalDeviceLineRasterizationPropertiesExt => StructureType::ePhysicalDeviceLineRasterizationPropertiesExt,
                Self::ePhysicalDeviceShaderAtomicFloatFeaturesExt => StructureType::ePhysicalDeviceShaderAtomicFloatFeaturesExt,
                Self::ePhysicalDeviceHostQueryResetFeatures => StructureType::ePhysicalDeviceHostQueryResetFeatures,
                Self::ePhysicalDeviceIndexTypeUint8FeaturesExt => StructureType::ePhysicalDeviceIndexTypeUint8FeaturesExt,
                Self::ePhysicalDeviceExtendedDynamicStateFeaturesExt => StructureType::ePhysicalDeviceExtendedDynamicStateFeaturesExt,
                Self::ePhysicalDevicePipelineExecutablePropertiesFeaturesKhr => StructureType::ePhysicalDevicePipelineExecutablePropertiesFeaturesKhr,
                Self::ePipelineInfoKhr => StructureType::ePipelineInfoKhr,
                Self::ePipelineExecutablePropertiesKhr => StructureType::ePipelineExecutablePropertiesKhr,
                Self::ePipelineExecutableInfoKhr => StructureType::ePipelineExecutableInfoKhr,
                Self::ePipelineExecutableStatisticKhr => StructureType::ePipelineExecutableStatisticKhr,
                Self::ePipelineExecutableInternalRepresentationKhr => StructureType::ePipelineExecutableInternalRepresentationKhr,
                Self::ePhysicalDeviceShaderAtomicFloat2FeaturesExt => StructureType::ePhysicalDeviceShaderAtomicFloat2FeaturesExt,
                Self::eSurfacePresentModeExt => StructureType::eSurfacePresentModeExt,
                Self::eSurfacePresentScalingCapabilitiesExt => StructureType::eSurfacePresentScalingCapabilitiesExt,
                Self::eSurfacePresentModeCompatibilityExt => StructureType::eSurfacePresentModeCompatibilityExt,
                Self::ePhysicalDeviceSwapchainMaintenance1FeaturesExt => StructureType::ePhysicalDeviceSwapchainMaintenance1FeaturesExt,
                Self::eSwapchainPresentFenceInfoExt => StructureType::eSwapchainPresentFenceInfoExt,
                Self::eSwapchainPresentModesCreateInfoExt => StructureType::eSwapchainPresentModesCreateInfoExt,
                Self::eSwapchainPresentModeInfoExt => StructureType::eSwapchainPresentModeInfoExt,
                Self::eSwapchainPresentScalingCreateInfoExt => StructureType::eSwapchainPresentScalingCreateInfoExt,
                Self::eReleaseSwapchainImagesInfoExt => StructureType::eReleaseSwapchainImagesInfoExt,
                Self::ePhysicalDeviceShaderDemoteToHelperInvocationFeatures => StructureType::ePhysicalDeviceShaderDemoteToHelperInvocationFeatures,
                Self::ePhysicalDeviceDeviceGeneratedCommandsPropertiesNv => StructureType::ePhysicalDeviceDeviceGeneratedCommandsPropertiesNv,
                Self::eGraphicsShaderGroupCreateInfoNv => StructureType::eGraphicsShaderGroupCreateInfoNv,
                Self::eGraphicsPipelineShaderGroupsCreateInfoNv => StructureType::eGraphicsPipelineShaderGroupsCreateInfoNv,
                Self::eIndirectCommandsLayoutTokenNv => StructureType::eIndirectCommandsLayoutTokenNv,
                Self::eIndirectCommandsLayoutCreateInfoNv => StructureType::eIndirectCommandsLayoutCreateInfoNv,
                Self::eGeneratedCommandsInfoNv => StructureType::eGeneratedCommandsInfoNv,
                Self::eGeneratedCommandsMemoryRequirementsInfoNv => StructureType::eGeneratedCommandsMemoryRequirementsInfoNv,
                Self::ePhysicalDeviceDeviceGeneratedCommandsFeaturesNv => StructureType::ePhysicalDeviceDeviceGeneratedCommandsFeaturesNv,
                Self::ePhysicalDeviceInheritedViewportScissorFeaturesNv => StructureType::ePhysicalDeviceInheritedViewportScissorFeaturesNv,
                Self::eCommandBufferInheritanceViewportScissorInfoNv => StructureType::eCommandBufferInheritanceViewportScissorInfoNv,
                Self::ePhysicalDeviceShaderIntegerDotProductFeatures => StructureType::ePhysicalDeviceShaderIntegerDotProductFeatures,
                Self::ePhysicalDeviceShaderIntegerDotProductProperties => StructureType::ePhysicalDeviceShaderIntegerDotProductProperties,
                Self::ePhysicalDeviceTexelBufferAlignmentFeaturesExt => StructureType::ePhysicalDeviceTexelBufferAlignmentFeaturesExt,
                Self::ePhysicalDeviceTexelBufferAlignmentProperties => StructureType::ePhysicalDeviceTexelBufferAlignmentProperties,
                Self::eCommandBufferInheritanceRenderPassTransformInfoQcom => StructureType::eCommandBufferInheritanceRenderPassTransformInfoQcom,
                Self::eRenderPassTransformBeginInfoQcom => StructureType::eRenderPassTransformBeginInfoQcom,
                Self::ePhysicalDeviceDeviceMemoryReportFeaturesExt => StructureType::ePhysicalDeviceDeviceMemoryReportFeaturesExt,
                Self::eDeviceDeviceMemoryReportCreateInfoExt => StructureType::eDeviceDeviceMemoryReportCreateInfoExt,
                Self::eDeviceMemoryReportCallbackDataExt => StructureType::eDeviceMemoryReportCallbackDataExt,
                Self::ePhysicalDeviceRobustness2FeaturesExt => StructureType::ePhysicalDeviceRobustness2FeaturesExt,
                Self::ePhysicalDeviceRobustness2PropertiesExt => StructureType::ePhysicalDeviceRobustness2PropertiesExt,
                Self::eSamplerCustomBorderColourCreateInfoExt => StructureType::eSamplerCustomBorderColourCreateInfoExt,
                Self::ePhysicalDeviceCustomBorderColourPropertiesExt => StructureType::ePhysicalDeviceCustomBorderColourPropertiesExt,
                Self::ePhysicalDeviceCustomBorderColourFeaturesExt => StructureType::ePhysicalDeviceCustomBorderColourFeaturesExt,
                Self::ePipelineLibraryCreateInfoKhr => StructureType::ePipelineLibraryCreateInfoKhr,
                Self::ePhysicalDevicePresentBarrierFeaturesNv => StructureType::ePhysicalDevicePresentBarrierFeaturesNv,
                Self::eSurfaceCapabilitiesPresentBarrierNv => StructureType::eSurfaceCapabilitiesPresentBarrierNv,
                Self::eSwapchainPresentBarrierCreateInfoNv => StructureType::eSwapchainPresentBarrierCreateInfoNv,
                Self::ePresentIdKhr => StructureType::ePresentIdKhr,
                Self::ePhysicalDevicePresentIdFeaturesKhr => StructureType::ePhysicalDevicePresentIdFeaturesKhr,
                Self::ePhysicalDevicePrivateDataFeatures => StructureType::ePhysicalDevicePrivateDataFeatures,
                Self::eDevicePrivateDataCreateInfo => StructureType::eDevicePrivateDataCreateInfo,
                Self::ePrivateDataSlotCreateInfo => StructureType::ePrivateDataSlotCreateInfo,
                Self::ePhysicalDevicePipelineCreationCacheControlFeatures => StructureType::ePhysicalDevicePipelineCreationCacheControlFeatures,
                Self::ePhysicalDeviceVulkanSc10Features => StructureType::ePhysicalDeviceVulkanSc10Features,
                Self::ePhysicalDeviceVulkanSc10Properties => StructureType::ePhysicalDeviceVulkanSc10Properties,
                Self::eDeviceObjectReservationCreateInfo => StructureType::eDeviceObjectReservationCreateInfo,
                Self::eCommandPoolMemoryReservationCreateInfo => StructureType::eCommandPoolMemoryReservationCreateInfo,
                Self::eCommandPoolMemoryConsumption => StructureType::eCommandPoolMemoryConsumption,
                Self::ePipelinePoolSize => StructureType::ePipelinePoolSize,
                Self::eFaultData => StructureType::eFaultData,
                Self::eFaultCallbackInfo => StructureType::eFaultCallbackInfo,
                Self::ePipelineOfflineCreateInfo => StructureType::ePipelineOfflineCreateInfo,
                Self::eVideoEncodeInfoKhr => StructureType::eVideoEncodeInfoKhr,
                Self::eVideoEncodeRateControlInfoKhr => StructureType::eVideoEncodeRateControlInfoKhr,
                Self::eVideoEncodeRateControlLayerInfoKhr => StructureType::eVideoEncodeRateControlLayerInfoKhr,
                Self::eVideoEncodeCapabilitiesKhr => StructureType::eVideoEncodeCapabilitiesKhr,
                Self::eVideoEncodeUsageInfoKhr => StructureType::eVideoEncodeUsageInfoKhr,
                Self::ePhysicalDeviceDiagnosticsConfigFeaturesNv => StructureType::ePhysicalDeviceDiagnosticsConfigFeaturesNv,
                Self::eDeviceDiagnosticsConfigCreateInfoNv => StructureType::eDeviceDiagnosticsConfigCreateInfoNv,
                Self::eRefreshObjectListKhr => StructureType::eRefreshObjectListKhr,
                Self::eQueryLowLatencySupportNv => StructureType::eQueryLowLatencySupportNv,
                Self::eExportMetalObjectCreateInfoExt => StructureType::eExportMetalObjectCreateInfoExt,
                Self::eExportMetalObjectsInfoExt => StructureType::eExportMetalObjectsInfoExt,
                Self::eExportMetalDeviceInfoExt => StructureType::eExportMetalDeviceInfoExt,
                Self::eExportMetalCommandQueueInfoExt => StructureType::eExportMetalCommandQueueInfoExt,
                Self::eExportMetalBufferInfoExt => StructureType::eExportMetalBufferInfoExt,
                Self::eImportMetalBufferInfoExt => StructureType::eImportMetalBufferInfoExt,
                Self::eExportMetalTextureInfoExt => StructureType::eExportMetalTextureInfoExt,
                Self::eImportMetalTextureInfoExt => StructureType::eImportMetalTextureInfoExt,
                Self::eExportMetalIoSurfaceInfoExt => StructureType::eExportMetalIoSurfaceInfoExt,
                Self::eImportMetalIoSurfaceInfoExt => StructureType::eImportMetalIoSurfaceInfoExt,
                Self::eExportMetalSharedEventInfoExt => StructureType::eExportMetalSharedEventInfoExt,
                Self::eImportMetalSharedEventInfoExt => StructureType::eImportMetalSharedEventInfoExt,
                Self::eMemoryBarrier2 => StructureType::eMemoryBarrier2,
                Self::eBufferMemoryBarrier2 => StructureType::eBufferMemoryBarrier2,
                Self::eImageMemoryBarrier2 => StructureType::eImageMemoryBarrier2,
                Self::eDependencyInfo => StructureType::eDependencyInfo,
                Self::eSubmitInfo2 => StructureType::eSubmitInfo2,
                Self::eSemaphoreSubmitInfo => StructureType::eSemaphoreSubmitInfo,
                Self::eCommandBufferSubmitInfo => StructureType::eCommandBufferSubmitInfo,
                Self::ePhysicalDeviceSynchronization2Features => StructureType::ePhysicalDeviceSynchronization2Features,
                Self::eQueueFamilyCheckpointProperties2Nv => StructureType::eQueueFamilyCheckpointProperties2Nv,
                Self::eCheckpointData2Nv => StructureType::eCheckpointData2Nv,
                Self::ePhysicalDeviceDescriptorBufferPropertiesExt => StructureType::ePhysicalDeviceDescriptorBufferPropertiesExt,
                Self::ePhysicalDeviceDescriptorBufferDensityMapPropertiesExt => StructureType::ePhysicalDeviceDescriptorBufferDensityMapPropertiesExt,
                Self::ePhysicalDeviceDescriptorBufferFeaturesExt => StructureType::ePhysicalDeviceDescriptorBufferFeaturesExt,
                Self::eDescriptorAddressInfoExt => StructureType::eDescriptorAddressInfoExt,
                Self::eDescriptorGetInfoExt => StructureType::eDescriptorGetInfoExt,
                Self::eBufferCaptureDescriptorDataInfoExt => StructureType::eBufferCaptureDescriptorDataInfoExt,
                Self::eImageCaptureDescriptorDataInfoExt => StructureType::eImageCaptureDescriptorDataInfoExt,
                Self::eImageViewCaptureDescriptorDataInfoExt => StructureType::eImageViewCaptureDescriptorDataInfoExt,
                Self::eSamplerCaptureDescriptorDataInfoExt => StructureType::eSamplerCaptureDescriptorDataInfoExt,
                Self::eAccelerationStructureCaptureDescriptorDataInfoExt => StructureType::eAccelerationStructureCaptureDescriptorDataInfoExt,
                Self::eOpaqueCaptureDescriptorDataCreateInfoExt => StructureType::eOpaqueCaptureDescriptorDataCreateInfoExt,
                Self::eDescriptorBufferBindingInfoExt => StructureType::eDescriptorBufferBindingInfoExt,
                Self::eDescriptorBufferBindingPushDescriptorBufferHandleExt => StructureType::eDescriptorBufferBindingPushDescriptorBufferHandleExt,
                Self::ePhysicalDeviceGraphicsPipelineLibraryFeaturesExt => StructureType::ePhysicalDeviceGraphicsPipelineLibraryFeaturesExt,
                Self::ePhysicalDeviceGraphicsPipelineLibraryPropertiesExt => StructureType::ePhysicalDeviceGraphicsPipelineLibraryPropertiesExt,
                Self::eGraphicsPipelineLibraryCreateInfoExt => StructureType::eGraphicsPipelineLibraryCreateInfoExt,
                Self::ePhysicalDeviceShaderEarlyAndLateFragmentTestsFeaturesAmd => StructureType::ePhysicalDeviceShaderEarlyAndLateFragmentTestsFeaturesAmd,
                Self::ePhysicalDeviceFragmentShaderBarycentricPropertiesKhr => StructureType::ePhysicalDeviceFragmentShaderBarycentricPropertiesKhr,
                Self::ePhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKhr => StructureType::ePhysicalDeviceShaderSubgroupUniformControlFlowFeaturesKhr,
                Self::ePhysicalDeviceZeroInitializeWorkgroupMemoryFeatures => StructureType::ePhysicalDeviceZeroInitializeWorkgroupMemoryFeatures,
                Self::ePhysicalDeviceFragmentShadingRateEnumsPropertiesNv => StructureType::ePhysicalDeviceFragmentShadingRateEnumsPropertiesNv,
                Self::ePhysicalDeviceFragmentShadingRateEnumsFeaturesNv => StructureType::ePhysicalDeviceFragmentShadingRateEnumsFeaturesNv,
                Self::ePipelineFragmentShadingRateEnumStateCreateInfoNv => StructureType::ePipelineFragmentShadingRateEnumStateCreateInfoNv,
                Self::eAccelerationStructureGeometryMotionTrianglesDataNv => StructureType::eAccelerationStructureGeometryMotionTrianglesDataNv,
                Self::ePhysicalDeviceRayTracingMotionBlurFeaturesNv => StructureType::ePhysicalDeviceRayTracingMotionBlurFeaturesNv,
                Self::eAccelerationStructureMotionInfoNv => StructureType::eAccelerationStructureMotionInfoNv,
                Self::ePhysicalDeviceMeshShaderFeaturesExt => StructureType::ePhysicalDeviceMeshShaderFeaturesExt,
                Self::ePhysicalDeviceMeshShaderPropertiesExt => StructureType::ePhysicalDeviceMeshShaderPropertiesExt,
                Self::ePhysicalDeviceYcbcr2Plane444FormatsFeaturesExt => StructureType::ePhysicalDeviceYcbcr2Plane444FormatsFeaturesExt,
                Self::ePhysicalDeviceFragmentDensityMap2FeaturesExt => StructureType::ePhysicalDeviceFragmentDensityMap2FeaturesExt,
                Self::ePhysicalDeviceFragmentDensityMap2PropertiesExt => StructureType::ePhysicalDeviceFragmentDensityMap2PropertiesExt,
                Self::eCopyCommandTransformInfoQcom => StructureType::eCopyCommandTransformInfoQcom,
                Self::ePhysicalDeviceImageRobustnessFeatures => StructureType::ePhysicalDeviceImageRobustnessFeatures,
                Self::ePhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKhr => StructureType::ePhysicalDeviceWorkgroupMemoryExplicitLayoutFeaturesKhr,
                Self::eCopyBufferInfo2 => StructureType::eCopyBufferInfo2,
                Self::eCopyImageInfo2 => StructureType::eCopyImageInfo2,
                Self::eCopyBufferToImageInfo2 => StructureType::eCopyBufferToImageInfo2,
                Self::eCopyImageToBufferInfo2 => StructureType::eCopyImageToBufferInfo2,
                Self::eBlitImageInfo2 => StructureType::eBlitImageInfo2,
                Self::eResolveImageInfo2 => StructureType::eResolveImageInfo2,
                Self::eBufferCopy2 => StructureType::eBufferCopy2,
                Self::eImageCopy2 => StructureType::eImageCopy2,
                Self::eImageBlit2 => StructureType::eImageBlit2,
                Self::eBufferImageCopy2 => StructureType::eBufferImageCopy2,
                Self::eImageResolve2 => StructureType::eImageResolve2,
                Self::ePhysicalDeviceImageCompressionControlFeaturesExt => StructureType::ePhysicalDeviceImageCompressionControlFeaturesExt,
                Self::eImageCompressionControlExt => StructureType::eImageCompressionControlExt,
                Self::eSubresourceLayout2Ext => StructureType::eSubresourceLayout2Ext,
                Self::eImageSubresource2Ext => StructureType::eImageSubresource2Ext,
                Self::eImageCompressionPropertiesExt => StructureType::eImageCompressionPropertiesExt,
                Self::ePhysicalDeviceAttachmentFeedbackLoopLayoutFeaturesExt => StructureType::ePhysicalDeviceAttachmentFeedbackLoopLayoutFeaturesExt,
                Self::ePhysicalDevice4444FormatsFeaturesExt => StructureType::ePhysicalDevice4444FormatsFeaturesExt,
                Self::ePhysicalDeviceFaultFeaturesExt => StructureType::ePhysicalDeviceFaultFeaturesExt,
                Self::eDeviceFaultCountsExt => StructureType::eDeviceFaultCountsExt,
                Self::eDeviceFaultInfoExt => StructureType::eDeviceFaultInfoExt,
                Self::ePhysicalDeviceRasterizationOrderAttachmentAccessFeaturesExt => StructureType::ePhysicalDeviceRasterizationOrderAttachmentAccessFeaturesExt,
                Self::ePhysicalDeviceRgba10X6FormatsFeaturesExt => StructureType::ePhysicalDeviceRgba10X6FormatsFeaturesExt,
                Self::eDirectfbSurfaceCreateInfoExt => StructureType::eDirectfbSurfaceCreateInfoExt,
                Self::ePhysicalDeviceRayTracingPipelineFeaturesKhr => StructureType::ePhysicalDeviceRayTracingPipelineFeaturesKhr,
                Self::ePhysicalDeviceRayTracingPipelinePropertiesKhr => StructureType::ePhysicalDeviceRayTracingPipelinePropertiesKhr,
                Self::ePhysicalDeviceRayQueryFeaturesKhr => StructureType::ePhysicalDeviceRayQueryFeaturesKhr,
                Self::ePhysicalDeviceMutableDescriptorTypeFeaturesExt => StructureType::ePhysicalDeviceMutableDescriptorTypeFeaturesExt,
                Self::eMutableDescriptorTypeCreateInfoExt => StructureType::eMutableDescriptorTypeCreateInfoExt,
                Self::ePhysicalDeviceVertexInputDynamicStateFeaturesExt => StructureType::ePhysicalDeviceVertexInputDynamicStateFeaturesExt,
                Self::eVertexInputBindingDescription2Ext => StructureType::eVertexInputBindingDescription2Ext,
                Self::eVertexInputAttributeDescription2Ext => StructureType::eVertexInputAttributeDescription2Ext,
                Self::ePhysicalDeviceDrmPropertiesExt => StructureType::ePhysicalDeviceDrmPropertiesExt,
                Self::ePhysicalDeviceAddressBindingReportFeaturesExt => StructureType::ePhysicalDeviceAddressBindingReportFeaturesExt,
                Self::eDeviceAddressBindingCallbackDataExt => StructureType::eDeviceAddressBindingCallbackDataExt,
                Self::ePhysicalDeviceDepthClipControlFeaturesExt => StructureType::ePhysicalDeviceDepthClipControlFeaturesExt,
                Self::ePipelineViewportDepthClipControlCreateInfoExt => StructureType::ePipelineViewportDepthClipControlCreateInfoExt,
                Self::ePhysicalDevicePrimitiveTopologyListRestartFeaturesExt => StructureType::ePhysicalDevicePrimitiveTopologyListRestartFeaturesExt,
                Self::eFormatProperties3 => StructureType::eFormatProperties3,
                Self::eImportMemoryZirconHandleInfoFuchsia => StructureType::eImportMemoryZirconHandleInfoFuchsia,
                Self::eMemoryZirconHandlePropertiesFuchsia => StructureType::eMemoryZirconHandlePropertiesFuchsia,
                Self::eMemoryGetZirconHandleInfoFuchsia => StructureType::eMemoryGetZirconHandleInfoFuchsia,
                Self::eImportSemaphoreZirconHandleInfoFuchsia => StructureType::eImportSemaphoreZirconHandleInfoFuchsia,
                Self::eSemaphoreGetZirconHandleInfoFuchsia => StructureType::eSemaphoreGetZirconHandleInfoFuchsia,
                Self::eBufferCollectionCreateInfoFuchsia => StructureType::eBufferCollectionCreateInfoFuchsia,
                Self::eImportMemoryBufferCollectionFuchsia => StructureType::eImportMemoryBufferCollectionFuchsia,
                Self::eBufferCollectionImageCreateInfoFuchsia => StructureType::eBufferCollectionImageCreateInfoFuchsia,
                Self::eBufferCollectionPropertiesFuchsia => StructureType::eBufferCollectionPropertiesFuchsia,
                Self::eBufferConstraintsInfoFuchsia => StructureType::eBufferConstraintsInfoFuchsia,
                Self::eBufferCollectionBufferCreateInfoFuchsia => StructureType::eBufferCollectionBufferCreateInfoFuchsia,
                Self::eImageConstraintsInfoFuchsia => StructureType::eImageConstraintsInfoFuchsia,
                Self::eImageFormatConstraintsInfoFuchsia => StructureType::eImageFormatConstraintsInfoFuchsia,
                Self::eSysmemColourSpaceFuchsia => StructureType::eSysmemColourSpaceFuchsia,
                Self::eBufferCollectionConstraintsInfoFuchsia => StructureType::eBufferCollectionConstraintsInfoFuchsia,
                Self::eSubpassShadingPipelineCreateInfoHuawei => StructureType::eSubpassShadingPipelineCreateInfoHuawei,
                Self::ePhysicalDeviceSubpassShadingFeaturesHuawei => StructureType::ePhysicalDeviceSubpassShadingFeaturesHuawei,
                Self::ePhysicalDeviceSubpassShadingPropertiesHuawei => StructureType::ePhysicalDeviceSubpassShadingPropertiesHuawei,
                Self::ePhysicalDeviceInvocationMaskFeaturesHuawei => StructureType::ePhysicalDeviceInvocationMaskFeaturesHuawei,
                Self::eMemoryGetRemoteAddressInfoNv => StructureType::eMemoryGetRemoteAddressInfoNv,
                Self::ePhysicalDeviceExternalMemoryRdmaFeaturesNv => StructureType::ePhysicalDeviceExternalMemoryRdmaFeaturesNv,
                Self::ePipelinePropertiesIdentifierExt => StructureType::ePipelinePropertiesIdentifierExt,
                Self::ePhysicalDevicePipelinePropertiesFeaturesExt => StructureType::ePhysicalDevicePipelinePropertiesFeaturesExt,
                Self::eImportFenceSciSyncInfoNv => StructureType::eImportFenceSciSyncInfoNv,
                Self::eExportFenceSciSyncInfoNv => StructureType::eExportFenceSciSyncInfoNv,
                Self::eFenceGetSciSyncInfoNv => StructureType::eFenceGetSciSyncInfoNv,
                Self::eSciSyncAttributesInfoNv => StructureType::eSciSyncAttributesInfoNv,
                Self::eImportSemaphoreSciSyncInfoNv => StructureType::eImportSemaphoreSciSyncInfoNv,
                Self::eExportSemaphoreSciSyncInfoNv => StructureType::eExportSemaphoreSciSyncInfoNv,
                Self::eSemaphoreGetSciSyncInfoNv => StructureType::eSemaphoreGetSciSyncInfoNv,
                Self::ePhysicalDeviceExternalSciSyncFeaturesNv => StructureType::ePhysicalDeviceExternalSciSyncFeaturesNv,
                Self::eImportMemorySciBufInfoNv => StructureType::eImportMemorySciBufInfoNv,
                Self::eExportMemorySciBufInfoNv => StructureType::eExportMemorySciBufInfoNv,
                Self::eMemoryGetSciBufInfoNv => StructureType::eMemoryGetSciBufInfoNv,
                Self::eMemorySciBufPropertiesNv => StructureType::eMemorySciBufPropertiesNv,
                Self::ePhysicalDeviceExternalMemorySciBufFeaturesNv => StructureType::ePhysicalDeviceExternalMemorySciBufFeaturesNv,
                Self::ePhysicalDeviceMultisampledRenderToSingleSampledFeaturesExt => StructureType::ePhysicalDeviceMultisampledRenderToSingleSampledFeaturesExt,
                Self::eSubpassResolvePerformanceQueryExt => StructureType::eSubpassResolvePerformanceQueryExt,
                Self::eMultisampledRenderToSingleSampledInfoExt => StructureType::eMultisampledRenderToSingleSampledInfoExt,
                Self::ePhysicalDeviceExtendedDynamicState2FeaturesExt => StructureType::ePhysicalDeviceExtendedDynamicState2FeaturesExt,
                Self::eScreenSurfaceCreateInfoQnx => StructureType::eScreenSurfaceCreateInfoQnx,
                Self::ePhysicalDeviceColourWriteEnableFeaturesExt => StructureType::ePhysicalDeviceColourWriteEnableFeaturesExt,
                Self::ePipelineColourWriteCreateInfoExt => StructureType::ePipelineColourWriteCreateInfoExt,
                Self::ePhysicalDevicePrimitivesGeneratedQueryFeaturesExt => StructureType::ePhysicalDevicePrimitivesGeneratedQueryFeaturesExt,
                Self::ePhysicalDeviceRayTracingMaintenance1FeaturesKhr => StructureType::ePhysicalDeviceRayTracingMaintenance1FeaturesKhr,
                Self::ePhysicalDeviceGlobalPriorityQueryFeaturesKhr => StructureType::ePhysicalDeviceGlobalPriorityQueryFeaturesKhr,
                Self::eQueueFamilyGlobalPriorityPropertiesKhr => StructureType::eQueueFamilyGlobalPriorityPropertiesKhr,
                Self::ePhysicalDeviceImageViewMinLodFeaturesExt => StructureType::ePhysicalDeviceImageViewMinLodFeaturesExt,
                Self::eImageViewMinLodCreateInfoExt => StructureType::eImageViewMinLodCreateInfoExt,
                Self::ePhysicalDeviceMultiDrawFeaturesExt => StructureType::ePhysicalDeviceMultiDrawFeaturesExt,
                Self::ePhysicalDeviceMultiDrawPropertiesExt => StructureType::ePhysicalDeviceMultiDrawPropertiesExt,
                Self::ePhysicalDeviceImage2dViewOf3dFeaturesExt => StructureType::ePhysicalDeviceImage2dViewOf3dFeaturesExt,
                Self::eMicromapBuildInfoExt => StructureType::eMicromapBuildInfoExt,
                Self::eMicromapVersionInfoExt => StructureType::eMicromapVersionInfoExt,
                Self::eCopyMicromapInfoExt => StructureType::eCopyMicromapInfoExt,
                Self::eCopyMicromapToMemoryInfoExt => StructureType::eCopyMicromapToMemoryInfoExt,
                Self::eCopyMemoryToMicromapInfoExt => StructureType::eCopyMemoryToMicromapInfoExt,
                Self::ePhysicalDeviceOpacityMicromapFeaturesExt => StructureType::ePhysicalDeviceOpacityMicromapFeaturesExt,
                Self::ePhysicalDeviceOpacityMicromapPropertiesExt => StructureType::ePhysicalDeviceOpacityMicromapPropertiesExt,
                Self::eMicromapCreateInfoExt => StructureType::eMicromapCreateInfoExt,
                Self::eMicromapBuildSizesInfoExt => StructureType::eMicromapBuildSizesInfoExt,
                Self::eAccelerationStructureTrianglesOpacityMicromapExt => StructureType::eAccelerationStructureTrianglesOpacityMicromapExt,
                Self::ePhysicalDeviceClusterCullingShaderFeaturesHuawei => StructureType::ePhysicalDeviceClusterCullingShaderFeaturesHuawei,
                Self::ePhysicalDeviceClusterCullingShaderPropertiesHuawei => StructureType::ePhysicalDeviceClusterCullingShaderPropertiesHuawei,
                Self::ePhysicalDeviceBorderColourSwizzleFeaturesExt => StructureType::ePhysicalDeviceBorderColourSwizzleFeaturesExt,
                Self::eSamplerBorderColourComponentMappingCreateInfoExt => StructureType::eSamplerBorderColourComponentMappingCreateInfoExt,
                Self::ePhysicalDevicePageableDeviceLocalMemoryFeaturesExt => StructureType::ePhysicalDevicePageableDeviceLocalMemoryFeaturesExt,
                Self::ePhysicalDeviceMaintenance4Features => StructureType::ePhysicalDeviceMaintenance4Features,
                Self::ePhysicalDeviceMaintenance4Properties => StructureType::ePhysicalDeviceMaintenance4Properties,
                Self::eDeviceBufferMemoryRequirements => StructureType::eDeviceBufferMemoryRequirements,
                Self::eDeviceImageMemoryRequirements => StructureType::eDeviceImageMemoryRequirements,
                Self::ePhysicalDeviceShaderCorePropertiesArm => StructureType::ePhysicalDeviceShaderCorePropertiesArm,
                Self::ePhysicalDeviceImageSlicedViewOf3dFeaturesExt => StructureType::ePhysicalDeviceImageSlicedViewOf3dFeaturesExt,
                Self::eImageViewSlicedCreateInfoExt => StructureType::eImageViewSlicedCreateInfoExt,
                Self::ePhysicalDeviceDescriptorSetHostMappingFeaturesValve => StructureType::ePhysicalDeviceDescriptorSetHostMappingFeaturesValve,
                Self::eDescriptorSetBindingReferenceValve => StructureType::eDescriptorSetBindingReferenceValve,
                Self::eDescriptorSetLayoutHostMappingInfoValve => StructureType::eDescriptorSetLayoutHostMappingInfoValve,
                Self::ePhysicalDeviceDepthClampZeroOneFeaturesExt => StructureType::ePhysicalDeviceDepthClampZeroOneFeaturesExt,
                Self::ePhysicalDeviceNonSeamlessCubeMapFeaturesExt => StructureType::ePhysicalDeviceNonSeamlessCubeMapFeaturesExt,
                Self::ePhysicalDeviceFragmentDensityMapOffsetFeaturesQcom => StructureType::ePhysicalDeviceFragmentDensityMapOffsetFeaturesQcom,
                Self::ePhysicalDeviceFragmentDensityMapOffsetPropertiesQcom => StructureType::ePhysicalDeviceFragmentDensityMapOffsetPropertiesQcom,
                Self::eSubpassFragmentDensityMapOffsetEndInfoQcom => StructureType::eSubpassFragmentDensityMapOffsetEndInfoQcom,
                Self::ePhysicalDeviceCopyMemoryIndirectFeaturesNv => StructureType::ePhysicalDeviceCopyMemoryIndirectFeaturesNv,
                Self::ePhysicalDeviceCopyMemoryIndirectPropertiesNv => StructureType::ePhysicalDeviceCopyMemoryIndirectPropertiesNv,
                Self::ePhysicalDeviceMemoryDecompressionFeaturesNv => StructureType::ePhysicalDeviceMemoryDecompressionFeaturesNv,
                Self::ePhysicalDeviceMemoryDecompressionPropertiesNv => StructureType::ePhysicalDeviceMemoryDecompressionPropertiesNv,
                Self::ePhysicalDeviceLinearColourAttachmentFeaturesNv => StructureType::ePhysicalDeviceLinearColourAttachmentFeaturesNv,
                Self::eApplicationParametersExt => StructureType::eApplicationParametersExt,
                Self::ePhysicalDeviceImageCompressionControlSwapchainFeaturesExt => StructureType::ePhysicalDeviceImageCompressionControlSwapchainFeaturesExt,
                Self::ePhysicalDeviceImageProcessingFeaturesQcom => StructureType::ePhysicalDeviceImageProcessingFeaturesQcom,
                Self::ePhysicalDeviceImageProcessingPropertiesQcom => StructureType::ePhysicalDeviceImageProcessingPropertiesQcom,
                Self::eImageViewSampleWeightCreateInfoQcom => StructureType::eImageViewSampleWeightCreateInfoQcom,
                Self::ePhysicalDeviceExtendedDynamicState3FeaturesExt => StructureType::ePhysicalDeviceExtendedDynamicState3FeaturesExt,
                Self::ePhysicalDeviceExtendedDynamicState3PropertiesExt => StructureType::ePhysicalDeviceExtendedDynamicState3PropertiesExt,
                Self::ePhysicalDeviceSubpassMergeFeedbackFeaturesExt => StructureType::ePhysicalDeviceSubpassMergeFeedbackFeaturesExt,
                Self::eRenderPassCreationControlExt => StructureType::eRenderPassCreationControlExt,
                Self::eRenderPassCreationFeedbackCreateInfoExt => StructureType::eRenderPassCreationFeedbackCreateInfoExt,
                Self::eRenderPassSubpassFeedbackCreateInfoExt => StructureType::eRenderPassSubpassFeedbackCreateInfoExt,
                Self::eDirectDriverLoadingInfoLunarg => StructureType::eDirectDriverLoadingInfoLunarg,
                Self::eDirectDriverLoadingListLunarg => StructureType::eDirectDriverLoadingListLunarg,
                Self::ePhysicalDeviceShaderModuleIdentifierFeaturesExt => StructureType::ePhysicalDeviceShaderModuleIdentifierFeaturesExt,
                Self::ePhysicalDeviceShaderModuleIdentifierPropertiesExt => StructureType::ePhysicalDeviceShaderModuleIdentifierPropertiesExt,
                Self::ePipelineShaderStageModuleIdentifierCreateInfoExt => StructureType::ePipelineShaderStageModuleIdentifierCreateInfoExt,
                Self::eShaderModuleIdentifierExt => StructureType::eShaderModuleIdentifierExt,
                Self::ePhysicalDeviceOpticalFlowFeaturesNv => StructureType::ePhysicalDeviceOpticalFlowFeaturesNv,
                Self::ePhysicalDeviceOpticalFlowPropertiesNv => StructureType::ePhysicalDeviceOpticalFlowPropertiesNv,
                Self::eOpticalFlowImageFormatInfoNv => StructureType::eOpticalFlowImageFormatInfoNv,
                Self::eOpticalFlowImageFormatPropertiesNv => StructureType::eOpticalFlowImageFormatPropertiesNv,
                Self::eOpticalFlowSessionCreateInfoNv => StructureType::eOpticalFlowSessionCreateInfoNv,
                Self::eOpticalFlowExecuteInfoNv => StructureType::eOpticalFlowExecuteInfoNv,
                Self::eOpticalFlowSessionCreatePrivateDataInfoNv => StructureType::eOpticalFlowSessionCreatePrivateDataInfoNv,
                Self::ePhysicalDeviceLegacyDitheringFeaturesExt => StructureType::ePhysicalDeviceLegacyDitheringFeaturesExt,
                Self::ePhysicalDevicePipelineProtectedAccessFeaturesExt => StructureType::ePhysicalDevicePipelineProtectedAccessFeaturesExt,
                Self::ePhysicalDeviceTilePropertiesFeaturesQcom => StructureType::ePhysicalDeviceTilePropertiesFeaturesQcom,
                Self::eTilePropertiesQcom => StructureType::eTilePropertiesQcom,
                Self::ePhysicalDeviceAmigoProfilingFeaturesSec => StructureType::ePhysicalDeviceAmigoProfilingFeaturesSec,
                Self::eAmigoProfilingSubmitInfoSec => StructureType::eAmigoProfilingSubmitInfoSec,
                Self::ePhysicalDeviceMultiviewPerViewViewportsFeaturesQcom => StructureType::ePhysicalDeviceMultiviewPerViewViewportsFeaturesQcom,
                Self::eSemaphoreSciSyncPoolCreateInfoNv => StructureType::eSemaphoreSciSyncPoolCreateInfoNv,
                Self::eSemaphoreSciSyncCreateInfoNv => StructureType::eSemaphoreSciSyncCreateInfoNv,
                Self::ePhysicalDeviceExternalSciSync2FeaturesNv => StructureType::ePhysicalDeviceExternalSciSync2FeaturesNv,
                Self::eDeviceSemaphoreSciSyncPoolReservationCreateInfoNv => StructureType::eDeviceSemaphoreSciSyncPoolReservationCreateInfoNv,
                Self::ePhysicalDeviceRayTracingInvocationReorderFeaturesNv => StructureType::ePhysicalDeviceRayTracingInvocationReorderFeaturesNv,
                Self::ePhysicalDeviceRayTracingInvocationReorderPropertiesNv => StructureType::ePhysicalDeviceRayTracingInvocationReorderPropertiesNv,
                Self::ePhysicalDeviceShaderCoreBuiltinsFeaturesArm => StructureType::ePhysicalDeviceShaderCoreBuiltinsFeaturesArm,
                Self::ePhysicalDeviceShaderCoreBuiltinsPropertiesArm => StructureType::ePhysicalDeviceShaderCoreBuiltinsPropertiesArm,
                Self::ePhysicalDevicePipelineLibraryGroupHandlesFeaturesExt => StructureType::ePhysicalDevicePipelineLibraryGroupHandlesFeaturesExt,
                Self::ePhysicalDeviceMultiviewPerViewRenderAreasFeaturesQcom => StructureType::ePhysicalDeviceMultiviewPerViewRenderAreasFeaturesQcom,
                Self::eMultiviewPerViewRenderAreasRenderPassBeginInfoQcom => StructureType::eMultiviewPerViewRenderAreasRenderPassBeginInfoQcom,
                v => StructureType::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStructureType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawSubpassContents(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawSubpassContents {
        pub const eInline: Self = RawSubpassContents(0);
        pub const eSecondaryCommandBuffers: Self = RawSubpassContents(1);

        pub fn normalise(self) -> SubpassContents {
            match self {
                Self::eInline => SubpassContents::eInline,
                Self::eSecondaryCommandBuffers => SubpassContents::eSecondaryCommandBuffers,
                v => SubpassContents::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawSubpassContents {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawResult(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawResult {
        pub const eErrorCompressionExhaustedExt: Self = RawResult(-1000338000);
        pub const eErrorNoPipelineMatch: Self = RawResult(-1000298001);
        pub const eErrorInvalidPipelineCacheData: Self = RawResult(-1000298000);
        pub const eErrorInvalidOpaqueCaptureAddress: Self = RawResult(-1000257000);
        pub const eErrorFullScreenExclusiveModeLostExt: Self = RawResult(-1000255000);
        pub const eErrorNotPermittedKhr: Self = RawResult(-1000174001);
        pub const eErrorFragmentation: Self = RawResult(-1000161000);
        pub const eErrorInvalidDrmFormatModifierPlaneLayoutExt: Self = RawResult(-1000158000);
        pub const eErrorInvalidExternalHandle: Self = RawResult(-1000072003);
        pub const eErrorOutOfPoolMemory: Self = RawResult(-1000069000);
        pub const eErrorVideoStdVersionNotSupportedKhr: Self = RawResult(-1000023005);
        pub const eErrorVideoProfileCodecNotSupportedKhr: Self = RawResult(-1000023004);
        pub const eErrorVideoProfileFormatNotSupportedKhr: Self = RawResult(-1000023003);
        pub const eErrorVideoProfileOperationNotSupportedKhr: Self = RawResult(-1000023002);
        pub const eErrorVideoPictureLayoutNotSupportedKhr: Self = RawResult(-1000023001);
        pub const eErrorImageUsageNotSupportedKhr: Self = RawResult(-1000023000);
        pub const eErrorInvalidShaderNv: Self = RawResult(-1000012000);
        pub const eErrorValidationFailedExt: Self = RawResult(-1000011001);
        pub const eErrorIncompatibleDisplayKhr: Self = RawResult(-1000003001);
        pub const eErrorOutOfDateKhr: Self = RawResult(-1000001004);
        pub const eErrorNativeWindowInUseKhr: Self = RawResult(-1000000001);
        pub const eErrorSurfaceLostKhr: Self = RawResult(-1000000000);
        pub const eErrorUnknown: Self = RawResult(-13);
        pub const eErrorFragmentedPool: Self = RawResult(-12);
        pub const eErrorFormatNotSupported: Self = RawResult(-11);
        pub const eErrorTooManyObjects: Self = RawResult(-10);
        pub const eErrorIncompatibleDriver: Self = RawResult(-9);
        pub const eErrorFeatureNotPresent: Self = RawResult(-8);
        pub const eErrorExtensionNotPresent: Self = RawResult(-7);
        pub const eErrorLayerNotPresent: Self = RawResult(-6);
        pub const eErrorMemoryMapFailed: Self = RawResult(-5);
        pub const eErrorDeviceLost: Self = RawResult(-4);
        pub const eErrorInitializationFailed: Self = RawResult(-3);
        pub const eErrorOutOfDeviceMemory: Self = RawResult(-2);
        pub const eErrorOutOfHostMemory: Self = RawResult(-1);
        pub const eSuccess: Self = RawResult(0);
        pub const eNotReady: Self = RawResult(1);
        pub const eTimeout: Self = RawResult(2);
        pub const eEventSet: Self = RawResult(3);
        pub const eEventReset: Self = RawResult(4);
        pub const eIncomplete: Self = RawResult(5);
        pub const eSuboptimalKhr: Self = RawResult(1000001003);
        pub const eThreadIdleKhr: Self = RawResult(1000268000);
        pub const eThreadDoneKhr: Self = RawResult(1000268001);
        pub const eOperationDeferredKhr: Self = RawResult(1000268002);
        pub const eOperationNotDeferredKhr: Self = RawResult(1000268003);
        pub const ePipelineCompileRequired: Self = RawResult(1000297000);
        pub const eErrorFragmentationExt: Self = Self::eErrorFragmentation;
        pub const eErrorInvalidExternalHandleKhr: Self = Self::eErrorInvalidExternalHandle;
        pub const eErrorInvalidDeviceAddressExt: Self = Self::eErrorInvalidOpaqueCaptureAddress;
        pub const eErrorInvalidOpaqueCaptureAddressKhr: Self = Self::eErrorInvalidOpaqueCaptureAddress;
        pub const eErrorNotPermittedExt: Self = Self::eErrorNotPermittedKhr;
        pub const eErrorOutOfPoolMemoryKhr: Self = Self::eErrorOutOfPoolMemory;
        pub const eErrorPipelineCompileRequiredExt: Self = Self::ePipelineCompileRequired;
        pub const ePipelineCompileRequiredExt: Self = Self::ePipelineCompileRequired;

        pub fn normalise(self) -> Result {
            match self {
                Self::eErrorCompressionExhaustedExt => Result::eErrorCompressionExhaustedExt,
                Self::eErrorNoPipelineMatch => Result::eErrorNoPipelineMatch,
                Self::eErrorInvalidPipelineCacheData => Result::eErrorInvalidPipelineCacheData,
                Self::eErrorInvalidOpaqueCaptureAddress => Result::eErrorInvalidOpaqueCaptureAddress,
                Self::eErrorFullScreenExclusiveModeLostExt => Result::eErrorFullScreenExclusiveModeLostExt,
                Self::eErrorNotPermittedKhr => Result::eErrorNotPermittedKhr,
                Self::eErrorFragmentation => Result::eErrorFragmentation,
                Self::eErrorInvalidDrmFormatModifierPlaneLayoutExt => Result::eErrorInvalidDrmFormatModifierPlaneLayoutExt,
                Self::eErrorInvalidExternalHandle => Result::eErrorInvalidExternalHandle,
                Self::eErrorOutOfPoolMemory => Result::eErrorOutOfPoolMemory,
                Self::eErrorVideoStdVersionNotSupportedKhr => Result::eErrorVideoStdVersionNotSupportedKhr,
                Self::eErrorVideoProfileCodecNotSupportedKhr => Result::eErrorVideoProfileCodecNotSupportedKhr,
                Self::eErrorVideoProfileFormatNotSupportedKhr => Result::eErrorVideoProfileFormatNotSupportedKhr,
                Self::eErrorVideoProfileOperationNotSupportedKhr => Result::eErrorVideoProfileOperationNotSupportedKhr,
                Self::eErrorVideoPictureLayoutNotSupportedKhr => Result::eErrorVideoPictureLayoutNotSupportedKhr,
                Self::eErrorImageUsageNotSupportedKhr => Result::eErrorImageUsageNotSupportedKhr,
                Self::eErrorInvalidShaderNv => Result::eErrorInvalidShaderNv,
                Self::eErrorValidationFailedExt => Result::eErrorValidationFailedExt,
                Self::eErrorIncompatibleDisplayKhr => Result::eErrorIncompatibleDisplayKhr,
                Self::eErrorOutOfDateKhr => Result::eErrorOutOfDateKhr,
                Self::eErrorNativeWindowInUseKhr => Result::eErrorNativeWindowInUseKhr,
                Self::eErrorSurfaceLostKhr => Result::eErrorSurfaceLostKhr,
                Self::eErrorUnknown => Result::eErrorUnknown,
                Self::eErrorFragmentedPool => Result::eErrorFragmentedPool,
                Self::eErrorFormatNotSupported => Result::eErrorFormatNotSupported,
                Self::eErrorTooManyObjects => Result::eErrorTooManyObjects,
                Self::eErrorIncompatibleDriver => Result::eErrorIncompatibleDriver,
                Self::eErrorFeatureNotPresent => Result::eErrorFeatureNotPresent,
                Self::eErrorExtensionNotPresent => Result::eErrorExtensionNotPresent,
                Self::eErrorLayerNotPresent => Result::eErrorLayerNotPresent,
                Self::eErrorMemoryMapFailed => Result::eErrorMemoryMapFailed,
                Self::eErrorDeviceLost => Result::eErrorDeviceLost,
                Self::eErrorInitializationFailed => Result::eErrorInitializationFailed,
                Self::eErrorOutOfDeviceMemory => Result::eErrorOutOfDeviceMemory,
                Self::eErrorOutOfHostMemory => Result::eErrorOutOfHostMemory,
                Self::eSuccess => Result::eSuccess,
                Self::eNotReady => Result::eNotReady,
                Self::eTimeout => Result::eTimeout,
                Self::eEventSet => Result::eEventSet,
                Self::eEventReset => Result::eEventReset,
                Self::eIncomplete => Result::eIncomplete,
                Self::eSuboptimalKhr => Result::eSuboptimalKhr,
                Self::eThreadIdleKhr => Result::eThreadIdleKhr,
                Self::eThreadDoneKhr => Result::eThreadDoneKhr,
                Self::eOperationDeferredKhr => Result::eOperationDeferredKhr,
                Self::eOperationNotDeferredKhr => Result::eOperationNotDeferredKhr,
                Self::ePipelineCompileRequired => Result::ePipelineCompileRequired,
                v => Result::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawResult {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawDynamicState(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawDynamicState {
        pub const eViewport: Self = RawDynamicState(0);
        pub const eScissor: Self = RawDynamicState(1);
        pub const eLineWidth: Self = RawDynamicState(2);
        pub const eDepthBias: Self = RawDynamicState(3);
        pub const eBlendConstants: Self = RawDynamicState(4);
        pub const eDepthBounds: Self = RawDynamicState(5);
        pub const eStencilCompareMask: Self = RawDynamicState(6);
        pub const eStencilWriteMask: Self = RawDynamicState(7);
        pub const eStencilReference: Self = RawDynamicState(8);
        pub const eViewportWScalingNv: Self = RawDynamicState(1000087000);
        pub const eDiscardRectangleExt: Self = RawDynamicState(1000099000);
        pub const eDiscardRectangleEnableExt: Self = RawDynamicState(1000099001);
        pub const eDiscardRectangleModeExt: Self = RawDynamicState(1000099002);
        pub const eSampleLocationsExt: Self = RawDynamicState(1000143000);
        pub const eViewportShadingRatePaletteNv: Self = RawDynamicState(1000164004);
        pub const eViewportCoarseSampleOrderNv: Self = RawDynamicState(1000164006);
        pub const eExclusiveScissorEnableNv: Self = RawDynamicState(1000205000);
        pub const eExclusiveScissorNv: Self = RawDynamicState(1000205001);
        pub const eFragmentShadingRateKhr: Self = RawDynamicState(1000226000);
        pub const eLineStippleExt: Self = RawDynamicState(1000259000);
        pub const eCullMode: Self = RawDynamicState(1000267000);
        pub const eFrontFace: Self = RawDynamicState(1000267001);
        pub const ePrimitiveTopology: Self = RawDynamicState(1000267002);
        pub const eViewportWithCount: Self = RawDynamicState(1000267003);
        pub const eScissorWithCount: Self = RawDynamicState(1000267004);
        pub const eVertexInputBindingStride: Self = RawDynamicState(1000267005);
        pub const eDepthTestEnable: Self = RawDynamicState(1000267006);
        pub const eDepthWriteEnable: Self = RawDynamicState(1000267007);
        pub const eDepthCompareOp: Self = RawDynamicState(1000267008);
        pub const eDepthBoundsTestEnable: Self = RawDynamicState(1000267009);
        pub const eStencilTestEnable: Self = RawDynamicState(1000267010);
        pub const eStencilOp: Self = RawDynamicState(1000267011);
        pub const eRayTracingPipelineStackSizeKhr: Self = RawDynamicState(1000347000);
        pub const eVertexInputExt: Self = RawDynamicState(1000352000);
        pub const ePatchControlPointsExt: Self = RawDynamicState(1000377000);
        pub const eRasterizerDiscardEnable: Self = RawDynamicState(1000377001);
        pub const eDepthBiasEnable: Self = RawDynamicState(1000377002);
        pub const eLogicOpExt: Self = RawDynamicState(1000377003);
        pub const ePrimitiveRestartEnable: Self = RawDynamicState(1000377004);
        pub const eColourWriteEnableExt: Self = RawDynamicState(1000381000);
        pub const eTessellationDomainOriginExt: Self = RawDynamicState(1000455002);
        pub const eDepthClampEnableExt: Self = RawDynamicState(1000455003);
        pub const ePolygonModeExt: Self = RawDynamicState(1000455004);
        pub const eRasterizationSamplesExt: Self = RawDynamicState(1000455005);
        pub const eSampleMaskExt: Self = RawDynamicState(1000455006);
        pub const eAlphaToCoverageEnableExt: Self = RawDynamicState(1000455007);
        pub const eAlphaToOneEnableExt: Self = RawDynamicState(1000455008);
        pub const eLogicOpEnableExt: Self = RawDynamicState(1000455009);
        pub const eColourBlendEnableExt: Self = RawDynamicState(1000455010);
        pub const eColourBlendEquationExt: Self = RawDynamicState(1000455011);
        pub const eColourWriteMaskExt: Self = RawDynamicState(1000455012);
        pub const eRasterizationStreamExt: Self = RawDynamicState(1000455013);
        pub const eConservativeRasterizationModeExt: Self = RawDynamicState(1000455014);
        pub const eExtraPrimitiveOverestimationSizeExt: Self = RawDynamicState(1000455015);
        pub const eDepthClipEnableExt: Self = RawDynamicState(1000455016);
        pub const eSampleLocationsEnableExt: Self = RawDynamicState(1000455017);
        pub const eColourBlendAdvancedExt: Self = RawDynamicState(1000455018);
        pub const eProvokingVertexModeExt: Self = RawDynamicState(1000455019);
        pub const eLineRasterizationModeExt: Self = RawDynamicState(1000455020);
        pub const eLineStippleEnableExt: Self = RawDynamicState(1000455021);
        pub const eDepthClipNegativeOneToOneExt: Self = RawDynamicState(1000455022);
        pub const eViewportWScalingEnableNv: Self = RawDynamicState(1000455023);
        pub const eViewportSwizzleNv: Self = RawDynamicState(1000455024);
        pub const eCoverageToColourEnableNv: Self = RawDynamicState(1000455025);
        pub const eCoverageToColourLocationNv: Self = RawDynamicState(1000455026);
        pub const eCoverageModulationModeNv: Self = RawDynamicState(1000455027);
        pub const eCoverageModulationTableEnableNv: Self = RawDynamicState(1000455028);
        pub const eCoverageModulationTableNv: Self = RawDynamicState(1000455029);
        pub const eShadingRateImageEnableNv: Self = RawDynamicState(1000455030);
        pub const eRepresentativeFragmentTestEnableNv: Self = RawDynamicState(1000455031);
        pub const eCoverageReductionModeNv: Self = RawDynamicState(1000455032);
        pub const eCullModeExt: Self = Self::eCullMode;
        pub const eDepthBiasEnableExt: Self = Self::eDepthBiasEnable;
        pub const eDepthBoundsTestEnableExt: Self = Self::eDepthBoundsTestEnable;
        pub const eDepthCompareOpExt: Self = Self::eDepthCompareOp;
        pub const eDepthTestEnableExt: Self = Self::eDepthTestEnable;
        pub const eDepthWriteEnableExt: Self = Self::eDepthWriteEnable;
        pub const eFrontFaceExt: Self = Self::eFrontFace;
        pub const ePrimitiveRestartEnableExt: Self = Self::ePrimitiveRestartEnable;
        pub const ePrimitiveTopologyExt: Self = Self::ePrimitiveTopology;
        pub const eRasterizerDiscardEnableExt: Self = Self::eRasterizerDiscardEnable;
        pub const eScissorWithCountExt: Self = Self::eScissorWithCount;
        pub const eStencilOpExt: Self = Self::eStencilOp;
        pub const eStencilTestEnableExt: Self = Self::eStencilTestEnable;
        pub const eVertexInputBindingStrideExt: Self = Self::eVertexInputBindingStride;
        pub const eViewportWithCountExt: Self = Self::eViewportWithCount;

        pub fn normalise(self) -> DynamicState {
            match self {
                Self::eViewport => DynamicState::eViewport,
                Self::eScissor => DynamicState::eScissor,
                Self::eLineWidth => DynamicState::eLineWidth,
                Self::eDepthBias => DynamicState::eDepthBias,
                Self::eBlendConstants => DynamicState::eBlendConstants,
                Self::eDepthBounds => DynamicState::eDepthBounds,
                Self::eStencilCompareMask => DynamicState::eStencilCompareMask,
                Self::eStencilWriteMask => DynamicState::eStencilWriteMask,
                Self::eStencilReference => DynamicState::eStencilReference,
                Self::eViewportWScalingNv => DynamicState::eViewportWScalingNv,
                Self::eDiscardRectangleExt => DynamicState::eDiscardRectangleExt,
                Self::eDiscardRectangleEnableExt => DynamicState::eDiscardRectangleEnableExt,
                Self::eDiscardRectangleModeExt => DynamicState::eDiscardRectangleModeExt,
                Self::eSampleLocationsExt => DynamicState::eSampleLocationsExt,
                Self::eViewportShadingRatePaletteNv => DynamicState::eViewportShadingRatePaletteNv,
                Self::eViewportCoarseSampleOrderNv => DynamicState::eViewportCoarseSampleOrderNv,
                Self::eExclusiveScissorEnableNv => DynamicState::eExclusiveScissorEnableNv,
                Self::eExclusiveScissorNv => DynamicState::eExclusiveScissorNv,
                Self::eFragmentShadingRateKhr => DynamicState::eFragmentShadingRateKhr,
                Self::eLineStippleExt => DynamicState::eLineStippleExt,
                Self::eCullMode => DynamicState::eCullMode,
                Self::eFrontFace => DynamicState::eFrontFace,
                Self::ePrimitiveTopology => DynamicState::ePrimitiveTopology,
                Self::eViewportWithCount => DynamicState::eViewportWithCount,
                Self::eScissorWithCount => DynamicState::eScissorWithCount,
                Self::eVertexInputBindingStride => DynamicState::eVertexInputBindingStride,
                Self::eDepthTestEnable => DynamicState::eDepthTestEnable,
                Self::eDepthWriteEnable => DynamicState::eDepthWriteEnable,
                Self::eDepthCompareOp => DynamicState::eDepthCompareOp,
                Self::eDepthBoundsTestEnable => DynamicState::eDepthBoundsTestEnable,
                Self::eStencilTestEnable => DynamicState::eStencilTestEnable,
                Self::eStencilOp => DynamicState::eStencilOp,
                Self::eRayTracingPipelineStackSizeKhr => DynamicState::eRayTracingPipelineStackSizeKhr,
                Self::eVertexInputExt => DynamicState::eVertexInputExt,
                Self::ePatchControlPointsExt => DynamicState::ePatchControlPointsExt,
                Self::eRasterizerDiscardEnable => DynamicState::eRasterizerDiscardEnable,
                Self::eDepthBiasEnable => DynamicState::eDepthBiasEnable,
                Self::eLogicOpExt => DynamicState::eLogicOpExt,
                Self::ePrimitiveRestartEnable => DynamicState::ePrimitiveRestartEnable,
                Self::eColourWriteEnableExt => DynamicState::eColourWriteEnableExt,
                Self::eTessellationDomainOriginExt => DynamicState::eTessellationDomainOriginExt,
                Self::eDepthClampEnableExt => DynamicState::eDepthClampEnableExt,
                Self::ePolygonModeExt => DynamicState::ePolygonModeExt,
                Self::eRasterizationSamplesExt => DynamicState::eRasterizationSamplesExt,
                Self::eSampleMaskExt => DynamicState::eSampleMaskExt,
                Self::eAlphaToCoverageEnableExt => DynamicState::eAlphaToCoverageEnableExt,
                Self::eAlphaToOneEnableExt => DynamicState::eAlphaToOneEnableExt,
                Self::eLogicOpEnableExt => DynamicState::eLogicOpEnableExt,
                Self::eColourBlendEnableExt => DynamicState::eColourBlendEnableExt,
                Self::eColourBlendEquationExt => DynamicState::eColourBlendEquationExt,
                Self::eColourWriteMaskExt => DynamicState::eColourWriteMaskExt,
                Self::eRasterizationStreamExt => DynamicState::eRasterizationStreamExt,
                Self::eConservativeRasterizationModeExt => DynamicState::eConservativeRasterizationModeExt,
                Self::eExtraPrimitiveOverestimationSizeExt => DynamicState::eExtraPrimitiveOverestimationSizeExt,
                Self::eDepthClipEnableExt => DynamicState::eDepthClipEnableExt,
                Self::eSampleLocationsEnableExt => DynamicState::eSampleLocationsEnableExt,
                Self::eColourBlendAdvancedExt => DynamicState::eColourBlendAdvancedExt,
                Self::eProvokingVertexModeExt => DynamicState::eProvokingVertexModeExt,
                Self::eLineRasterizationModeExt => DynamicState::eLineRasterizationModeExt,
                Self::eLineStippleEnableExt => DynamicState::eLineStippleEnableExt,
                Self::eDepthClipNegativeOneToOneExt => DynamicState::eDepthClipNegativeOneToOneExt,
                Self::eViewportWScalingEnableNv => DynamicState::eViewportWScalingEnableNv,
                Self::eViewportSwizzleNv => DynamicState::eViewportSwizzleNv,
                Self::eCoverageToColourEnableNv => DynamicState::eCoverageToColourEnableNv,
                Self::eCoverageToColourLocationNv => DynamicState::eCoverageToColourLocationNv,
                Self::eCoverageModulationModeNv => DynamicState::eCoverageModulationModeNv,
                Self::eCoverageModulationTableEnableNv => DynamicState::eCoverageModulationTableEnableNv,
                Self::eCoverageModulationTableNv => DynamicState::eCoverageModulationTableNv,
                Self::eShadingRateImageEnableNv => DynamicState::eShadingRateImageEnableNv,
                Self::eRepresentativeFragmentTestEnableNv => DynamicState::eRepresentativeFragmentTestEnableNv,
                Self::eCoverageReductionModeNv => DynamicState::eCoverageReductionModeNv,
                v => DynamicState::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawDynamicState {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawDescriptorUpdateTemplateType(pub i32);

    pub type RawDescriptorUpdateTemplateTypeKHR = RawDescriptorUpdateTemplateType;


    #[allow(non_upper_case_globals)]
    impl RawDescriptorUpdateTemplateType {
        pub const eDescriptorSet: Self = RawDescriptorUpdateTemplateType(0);
        pub const ePushDescriptorsKhr: Self = RawDescriptorUpdateTemplateType(1);
        pub const eDescriptorSetKhr: Self = Self::eDescriptorSet;

        pub fn normalise(self) -> DescriptorUpdateTemplateType {
            match self {
                Self::eDescriptorSet => DescriptorUpdateTemplateType::eDescriptorSet,
                Self::ePushDescriptorsKhr => DescriptorUpdateTemplateType::ePushDescriptorsKhr,
                v => DescriptorUpdateTemplateType::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawDescriptorUpdateTemplateType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawObjectType(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawObjectType {
        pub const eUnknown: Self = RawObjectType(0);
        pub const eInstance: Self = RawObjectType(1);
        pub const ePhysicalDevice: Self = RawObjectType(2);
        pub const eDevice: Self = RawObjectType(3);
        pub const eQueue: Self = RawObjectType(4);
        pub const eSemaphore: Self = RawObjectType(5);
        pub const eCommandBuffer: Self = RawObjectType(6);
        pub const eFence: Self = RawObjectType(7);
        pub const eDeviceMemory: Self = RawObjectType(8);
        pub const eBuffer: Self = RawObjectType(9);
        pub const eImage: Self = RawObjectType(10);
        pub const eEvent: Self = RawObjectType(11);
        pub const eQueryPool: Self = RawObjectType(12);
        pub const eBufferView: Self = RawObjectType(13);
        pub const eImageView: Self = RawObjectType(14);
        pub const eShaderModule: Self = RawObjectType(15);
        pub const ePipelineCache: Self = RawObjectType(16);
        pub const ePipelineLayout: Self = RawObjectType(17);
        pub const eRenderPass: Self = RawObjectType(18);
        pub const ePipeline: Self = RawObjectType(19);
        pub const eDescriptorSetLayout: Self = RawObjectType(20);
        pub const eSampler: Self = RawObjectType(21);
        pub const eDescriptorPool: Self = RawObjectType(22);
        pub const eDescriptorSet: Self = RawObjectType(23);
        pub const eFramebuffer: Self = RawObjectType(24);
        pub const eCommandPool: Self = RawObjectType(25);
        pub const eSurfaceKhr: Self = RawObjectType(1000000000);
        pub const eSwapchainKhr: Self = RawObjectType(1000001000);
        pub const eDisplayKhr: Self = RawObjectType(1000002000);
        pub const eDisplayModeKhr: Self = RawObjectType(1000002001);
        pub const eDebugReportCallbackExt: Self = RawObjectType(1000011000);
        pub const eVideoSessionKhr: Self = RawObjectType(1000023000);
        pub const eVideoSessionParametersKhr: Self = RawObjectType(1000023001);
        pub const eCuModuleNvx: Self = RawObjectType(1000029000);
        pub const eCuFunctionNvx: Self = RawObjectType(1000029001);
        pub const eDescriptorUpdateTemplate: Self = RawObjectType(1000085000);
        pub const eDebugUtilsMessengerExt: Self = RawObjectType(1000128000);
        pub const eAccelerationStructureKhr: Self = RawObjectType(1000150000);
        pub const eSamplerYcbcrConversion: Self = RawObjectType(1000156000);
        pub const eValidationCacheExt: Self = RawObjectType(1000160000);
        pub const eAccelerationStructureNv: Self = RawObjectType(1000165000);
        pub const ePerformanceConfigurationIntel: Self = RawObjectType(1000210000);
        pub const eDeferredOperationKhr: Self = RawObjectType(1000268000);
        pub const eIndirectCommandsLayoutNv: Self = RawObjectType(1000277000);
        pub const ePrivateDataSlot: Self = RawObjectType(1000295000);
        pub const eBufferCollectionFuchsia: Self = RawObjectType(1000366000);
        pub const eMicromapExt: Self = RawObjectType(1000396000);
        pub const eOpticalFlowSessionNv: Self = RawObjectType(1000464000);
        pub const eSemaphoreSciSyncPoolNv: Self = RawObjectType(1000489000);
        pub const eDescriptorUpdateTemplateKhr: Self = Self::eDescriptorUpdateTemplate;
        pub const ePrivateDataSlotExt: Self = Self::ePrivateDataSlot;
        pub const eSamplerYcbcrConversionKhr: Self = Self::eSamplerYcbcrConversion;

        pub fn normalise(self) -> ObjectType {
            match self {
                Self::eUnknown => ObjectType::eUnknown,
                Self::eInstance => ObjectType::eInstance,
                Self::ePhysicalDevice => ObjectType::ePhysicalDevice,
                Self::eDevice => ObjectType::eDevice,
                Self::eQueue => ObjectType::eQueue,
                Self::eSemaphore => ObjectType::eSemaphore,
                Self::eCommandBuffer => ObjectType::eCommandBuffer,
                Self::eFence => ObjectType::eFence,
                Self::eDeviceMemory => ObjectType::eDeviceMemory,
                Self::eBuffer => ObjectType::eBuffer,
                Self::eImage => ObjectType::eImage,
                Self::eEvent => ObjectType::eEvent,
                Self::eQueryPool => ObjectType::eQueryPool,
                Self::eBufferView => ObjectType::eBufferView,
                Self::eImageView => ObjectType::eImageView,
                Self::eShaderModule => ObjectType::eShaderModule,
                Self::ePipelineCache => ObjectType::ePipelineCache,
                Self::ePipelineLayout => ObjectType::ePipelineLayout,
                Self::eRenderPass => ObjectType::eRenderPass,
                Self::ePipeline => ObjectType::ePipeline,
                Self::eDescriptorSetLayout => ObjectType::eDescriptorSetLayout,
                Self::eSampler => ObjectType::eSampler,
                Self::eDescriptorPool => ObjectType::eDescriptorPool,
                Self::eDescriptorSet => ObjectType::eDescriptorSet,
                Self::eFramebuffer => ObjectType::eFramebuffer,
                Self::eCommandPool => ObjectType::eCommandPool,
                Self::eSurfaceKhr => ObjectType::eSurfaceKhr,
                Self::eSwapchainKhr => ObjectType::eSwapchainKhr,
                Self::eDisplayKhr => ObjectType::eDisplayKhr,
                Self::eDisplayModeKhr => ObjectType::eDisplayModeKhr,
                Self::eDebugReportCallbackExt => ObjectType::eDebugReportCallbackExt,
                Self::eVideoSessionKhr => ObjectType::eVideoSessionKhr,
                Self::eVideoSessionParametersKhr => ObjectType::eVideoSessionParametersKhr,
                Self::eCuModuleNvx => ObjectType::eCuModuleNvx,
                Self::eCuFunctionNvx => ObjectType::eCuFunctionNvx,
                Self::eDescriptorUpdateTemplate => ObjectType::eDescriptorUpdateTemplate,
                Self::eDebugUtilsMessengerExt => ObjectType::eDebugUtilsMessengerExt,
                Self::eAccelerationStructureKhr => ObjectType::eAccelerationStructureKhr,
                Self::eSamplerYcbcrConversion => ObjectType::eSamplerYcbcrConversion,
                Self::eValidationCacheExt => ObjectType::eValidationCacheExt,
                Self::eAccelerationStructureNv => ObjectType::eAccelerationStructureNv,
                Self::ePerformanceConfigurationIntel => ObjectType::ePerformanceConfigurationIntel,
                Self::eDeferredOperationKhr => ObjectType::eDeferredOperationKhr,
                Self::eIndirectCommandsLayoutNv => ObjectType::eIndirectCommandsLayoutNv,
                Self::ePrivateDataSlot => ObjectType::ePrivateDataSlot,
                Self::eBufferCollectionFuchsia => ObjectType::eBufferCollectionFuchsia,
                Self::eMicromapExt => ObjectType::eMicromapExt,
                Self::eOpticalFlowSessionNv => ObjectType::eOpticalFlowSessionNv,
                Self::eSemaphoreSciSyncPoolNv => ObjectType::eSemaphoreSciSyncPoolNv,
                v => ObjectType::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawObjectType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawRayTracingInvocationReorderModeNV(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawRayTracingInvocationReorderModeNV {
        pub const eNone: Self = RawRayTracingInvocationReorderModeNV(0);
        pub const eReorder: Self = RawRayTracingInvocationReorderModeNV(1);

        pub fn normalise(self) -> RayTracingInvocationReorderModeNV {
            match self {
                Self::eNone => RayTracingInvocationReorderModeNV::eNone,
                Self::eReorder => RayTracingInvocationReorderModeNV::eReorder,
                v => RayTracingInvocationReorderModeNV::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawRayTracingInvocationReorderModeNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawDirectDriverLoadingModeLUNARG(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawDirectDriverLoadingModeLUNARG {
        pub const eExclusive: Self = RawDirectDriverLoadingModeLUNARG(0);
        pub const eInclusive: Self = RawDirectDriverLoadingModeLUNARG(1);

        pub fn normalise(self) -> DirectDriverLoadingModeLUNARG {
            match self {
                Self::eExclusive => DirectDriverLoadingModeLUNARG::eExclusive,
                Self::eInclusive => DirectDriverLoadingModeLUNARG::eInclusive,
                v => DirectDriverLoadingModeLUNARG::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawDirectDriverLoadingModeLUNARG {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawSemaphoreType(pub i32);

    pub type RawSemaphoreTypeKHR = RawSemaphoreType;


    #[allow(non_upper_case_globals)]
    impl RawSemaphoreType {
        pub const eBinary: Self = RawSemaphoreType(0);
        pub const eTimeline: Self = RawSemaphoreType(1);
        pub const eBinaryKhr: Self = Self::eBinary;
        pub const eTimelineKhr: Self = Self::eTimeline;

        pub fn normalise(self) -> SemaphoreType {
            match self {
                Self::eBinary => SemaphoreType::eBinary,
                Self::eTimeline => SemaphoreType::eTimeline,
                v => SemaphoreType::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawSemaphoreType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawPresentModeKHR(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawPresentModeKHR {
        pub const eImmediate: Self = RawPresentModeKHR(0);
        pub const eMailbox: Self = RawPresentModeKHR(1);
        pub const eFifo: Self = RawPresentModeKHR(2);
        pub const eFifoRelaxed: Self = RawPresentModeKHR(3);
        pub const eSharedDemandRefresh: Self = RawPresentModeKHR(1000111000);
        pub const eSharedContinuousRefresh: Self = RawPresentModeKHR(1000111001);

        pub fn normalise(self) -> PresentModeKHR {
            match self {
                Self::eImmediate => PresentModeKHR::eImmediate,
                Self::eMailbox => PresentModeKHR::eMailbox,
                Self::eFifo => PresentModeKHR::eFifo,
                Self::eFifoRelaxed => PresentModeKHR::eFifoRelaxed,
                Self::eSharedDemandRefresh => PresentModeKHR::eSharedDemandRefresh,
                Self::eSharedContinuousRefresh => PresentModeKHR::eSharedContinuousRefresh,
                v => PresentModeKHR::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawPresentModeKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawColourSpaceKHR(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawColourSpaceKHR {
        pub const eSrgbNonlinear: Self = RawColourSpaceKHR(0);
        pub const eDisplayP3NonlinearExt: Self = RawColourSpaceKHR(1000104001);
        pub const eExtendedSrgbLinearExt: Self = RawColourSpaceKHR(1000104002);
        pub const eDisplayP3LinearExt: Self = RawColourSpaceKHR(1000104003);
        pub const eDciP3NonlinearExt: Self = RawColourSpaceKHR(1000104004);
        pub const eBt709LinearExt: Self = RawColourSpaceKHR(1000104005);
        pub const eBt709NonlinearExt: Self = RawColourSpaceKHR(1000104006);
        pub const eBt2020LinearExt: Self = RawColourSpaceKHR(1000104007);
        pub const eHdr10St2084Ext: Self = RawColourSpaceKHR(1000104008);
        pub const eDolbyvisionExt: Self = RawColourSpaceKHR(1000104009);
        pub const eHdr10HlgExt: Self = RawColourSpaceKHR(1000104010);
        pub const eAdobergbLinearExt: Self = RawColourSpaceKHR(1000104011);
        pub const eAdobergbNonlinearExt: Self = RawColourSpaceKHR(1000104012);
        pub const ePassThroughExt: Self = RawColourSpaceKHR(1000104013);
        pub const eExtendedSrgbNonlinearExt: Self = RawColourSpaceKHR(1000104014);
        pub const eDisplayNativeAmd: Self = RawColourSpaceKHR(1000213000);
        pub const eDciP3LinearExt: Self = Self::eDisplayP3LinearExt;
        pub const eColourspaceSrgbNonlinear: Self = Self::eSrgbNonlinear;

        pub fn normalise(self) -> ColourSpaceKHR {
            match self {
                Self::eSrgbNonlinear => ColourSpaceKHR::eSrgbNonlinear,
                Self::eDisplayP3NonlinearExt => ColourSpaceKHR::eDisplayP3NonlinearExt,
                Self::eExtendedSrgbLinearExt => ColourSpaceKHR::eExtendedSrgbLinearExt,
                Self::eDisplayP3LinearExt => ColourSpaceKHR::eDisplayP3LinearExt,
                Self::eDciP3NonlinearExt => ColourSpaceKHR::eDciP3NonlinearExt,
                Self::eBt709LinearExt => ColourSpaceKHR::eBt709LinearExt,
                Self::eBt709NonlinearExt => ColourSpaceKHR::eBt709NonlinearExt,
                Self::eBt2020LinearExt => ColourSpaceKHR::eBt2020LinearExt,
                Self::eHdr10St2084Ext => ColourSpaceKHR::eHdr10St2084Ext,
                Self::eDolbyvisionExt => ColourSpaceKHR::eDolbyvisionExt,
                Self::eHdr10HlgExt => ColourSpaceKHR::eHdr10HlgExt,
                Self::eAdobergbLinearExt => ColourSpaceKHR::eAdobergbLinearExt,
                Self::eAdobergbNonlinearExt => ColourSpaceKHR::eAdobergbNonlinearExt,
                Self::ePassThroughExt => ColourSpaceKHR::ePassThroughExt,
                Self::eExtendedSrgbNonlinearExt => ColourSpaceKHR::eExtendedSrgbNonlinearExt,
                Self::eDisplayNativeAmd => ColourSpaceKHR::eDisplayNativeAmd,
                v => ColourSpaceKHR::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawColourSpaceKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawTimeDomainEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawTimeDomainEXT {
        pub const eDevice: Self = RawTimeDomainEXT(0);
        pub const eClockMonotonic: Self = RawTimeDomainEXT(1);
        pub const eClockMonotonicRaw: Self = RawTimeDomainEXT(2);
        pub const eQueryPerformanceCounter: Self = RawTimeDomainEXT(3);

        pub fn normalise(self) -> TimeDomainEXT {
            match self {
                Self::eDevice => TimeDomainEXT::eDevice,
                Self::eClockMonotonic => TimeDomainEXT::eClockMonotonic,
                Self::eClockMonotonicRaw => TimeDomainEXT::eClockMonotonicRaw,
                Self::eQueryPerformanceCounter => TimeDomainEXT::eQueryPerformanceCounter,
                v => TimeDomainEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawTimeDomainEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawDebugReportObjectTypeEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawDebugReportObjectTypeEXT {
        pub const eUnknown: Self = RawDebugReportObjectTypeEXT(0);
        pub const eInstance: Self = RawDebugReportObjectTypeEXT(1);
        pub const ePhysicalDevice: Self = RawDebugReportObjectTypeEXT(2);
        pub const eDevice: Self = RawDebugReportObjectTypeEXT(3);
        pub const eQueue: Self = RawDebugReportObjectTypeEXT(4);
        pub const eSemaphore: Self = RawDebugReportObjectTypeEXT(5);
        pub const eCommandBuffer: Self = RawDebugReportObjectTypeEXT(6);
        pub const eFence: Self = RawDebugReportObjectTypeEXT(7);
        pub const eDeviceMemory: Self = RawDebugReportObjectTypeEXT(8);
        pub const eBuffer: Self = RawDebugReportObjectTypeEXT(9);
        pub const eImage: Self = RawDebugReportObjectTypeEXT(10);
        pub const eEvent: Self = RawDebugReportObjectTypeEXT(11);
        pub const eQueryPool: Self = RawDebugReportObjectTypeEXT(12);
        pub const eBufferView: Self = RawDebugReportObjectTypeEXT(13);
        pub const eImageView: Self = RawDebugReportObjectTypeEXT(14);
        pub const eShaderModule: Self = RawDebugReportObjectTypeEXT(15);
        pub const ePipelineCache: Self = RawDebugReportObjectTypeEXT(16);
        pub const ePipelineLayout: Self = RawDebugReportObjectTypeEXT(17);
        pub const eRenderPass: Self = RawDebugReportObjectTypeEXT(18);
        pub const ePipeline: Self = RawDebugReportObjectTypeEXT(19);
        pub const eDescriptorSetLayout: Self = RawDebugReportObjectTypeEXT(20);
        pub const eSampler: Self = RawDebugReportObjectTypeEXT(21);
        pub const eDescriptorPool: Self = RawDebugReportObjectTypeEXT(22);
        pub const eDescriptorSet: Self = RawDebugReportObjectTypeEXT(23);
        pub const eFramebuffer: Self = RawDebugReportObjectTypeEXT(24);
        pub const eCommandPool: Self = RawDebugReportObjectTypeEXT(25);
        pub const eSurfaceKhr: Self = RawDebugReportObjectTypeEXT(26);
        pub const eSwapchainKhr: Self = RawDebugReportObjectTypeEXT(27);
        pub const eDebugReportCallbackExt: Self = RawDebugReportObjectTypeEXT(28);
        pub const eDisplayKhr: Self = RawDebugReportObjectTypeEXT(29);
        pub const eDisplayModeKhr: Self = RawDebugReportObjectTypeEXT(30);
        pub const eValidationCacheExt: Self = RawDebugReportObjectTypeEXT(33);
        pub const eCuModuleNvx: Self = RawDebugReportObjectTypeEXT(1000029000);
        pub const eCuFunctionNvx: Self = RawDebugReportObjectTypeEXT(1000029001);
        pub const eDescriptorUpdateTemplate: Self = RawDebugReportObjectTypeEXT(1000085000);
        pub const eAccelerationStructureKhr: Self = RawDebugReportObjectTypeEXT(1000150000);
        pub const eSamplerYcbcrConversion: Self = RawDebugReportObjectTypeEXT(1000156000);
        pub const eAccelerationStructureNv: Self = RawDebugReportObjectTypeEXT(1000165000);
        pub const eBufferCollectionFuchsia: Self = RawDebugReportObjectTypeEXT(1000366000);
        pub const eDebugReport: Self = Self::eDebugReportCallbackExt;
        pub const eDescriptorUpdateTemplateKhr: Self = Self::eDescriptorUpdateTemplate;
        pub const eSamplerYcbcrConversionKhr: Self = Self::eSamplerYcbcrConversion;
        pub const eValidationCache: Self = Self::eValidationCacheExt;

        pub fn normalise(self) -> DebugReportObjectTypeEXT {
            match self {
                Self::eUnknown => DebugReportObjectTypeEXT::eUnknown,
                Self::eInstance => DebugReportObjectTypeEXT::eInstance,
                Self::ePhysicalDevice => DebugReportObjectTypeEXT::ePhysicalDevice,
                Self::eDevice => DebugReportObjectTypeEXT::eDevice,
                Self::eQueue => DebugReportObjectTypeEXT::eQueue,
                Self::eSemaphore => DebugReportObjectTypeEXT::eSemaphore,
                Self::eCommandBuffer => DebugReportObjectTypeEXT::eCommandBuffer,
                Self::eFence => DebugReportObjectTypeEXT::eFence,
                Self::eDeviceMemory => DebugReportObjectTypeEXT::eDeviceMemory,
                Self::eBuffer => DebugReportObjectTypeEXT::eBuffer,
                Self::eImage => DebugReportObjectTypeEXT::eImage,
                Self::eEvent => DebugReportObjectTypeEXT::eEvent,
                Self::eQueryPool => DebugReportObjectTypeEXT::eQueryPool,
                Self::eBufferView => DebugReportObjectTypeEXT::eBufferView,
                Self::eImageView => DebugReportObjectTypeEXT::eImageView,
                Self::eShaderModule => DebugReportObjectTypeEXT::eShaderModule,
                Self::ePipelineCache => DebugReportObjectTypeEXT::ePipelineCache,
                Self::ePipelineLayout => DebugReportObjectTypeEXT::ePipelineLayout,
                Self::eRenderPass => DebugReportObjectTypeEXT::eRenderPass,
                Self::ePipeline => DebugReportObjectTypeEXT::ePipeline,
                Self::eDescriptorSetLayout => DebugReportObjectTypeEXT::eDescriptorSetLayout,
                Self::eSampler => DebugReportObjectTypeEXT::eSampler,
                Self::eDescriptorPool => DebugReportObjectTypeEXT::eDescriptorPool,
                Self::eDescriptorSet => DebugReportObjectTypeEXT::eDescriptorSet,
                Self::eFramebuffer => DebugReportObjectTypeEXT::eFramebuffer,
                Self::eCommandPool => DebugReportObjectTypeEXT::eCommandPool,
                Self::eSurfaceKhr => DebugReportObjectTypeEXT::eSurfaceKhr,
                Self::eSwapchainKhr => DebugReportObjectTypeEXT::eSwapchainKhr,
                Self::eDebugReportCallbackExt => DebugReportObjectTypeEXT::eDebugReportCallbackExt,
                Self::eDisplayKhr => DebugReportObjectTypeEXT::eDisplayKhr,
                Self::eDisplayModeKhr => DebugReportObjectTypeEXT::eDisplayModeKhr,
                Self::eValidationCacheExt => DebugReportObjectTypeEXT::eValidationCacheExt,
                Self::eCuModuleNvx => DebugReportObjectTypeEXT::eCuModuleNvx,
                Self::eCuFunctionNvx => DebugReportObjectTypeEXT::eCuFunctionNvx,
                Self::eDescriptorUpdateTemplate => DebugReportObjectTypeEXT::eDescriptorUpdateTemplate,
                Self::eAccelerationStructureKhr => DebugReportObjectTypeEXT::eAccelerationStructureKhr,
                Self::eSamplerYcbcrConversion => DebugReportObjectTypeEXT::eSamplerYcbcrConversion,
                Self::eAccelerationStructureNv => DebugReportObjectTypeEXT::eAccelerationStructureNv,
                Self::eBufferCollectionFuchsia => DebugReportObjectTypeEXT::eBufferCollectionFuchsia,
                v => DebugReportObjectTypeEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawDebugReportObjectTypeEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawDeviceMemoryReportEventTypeEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawDeviceMemoryReportEventTypeEXT {
        pub const eAllocate: Self = RawDeviceMemoryReportEventTypeEXT(0);
        pub const eFree: Self = RawDeviceMemoryReportEventTypeEXT(1);
        pub const eImport: Self = RawDeviceMemoryReportEventTypeEXT(2);
        pub const eUnimport: Self = RawDeviceMemoryReportEventTypeEXT(3);
        pub const eAllocationFailed: Self = RawDeviceMemoryReportEventTypeEXT(4);

        pub fn normalise(self) -> DeviceMemoryReportEventTypeEXT {
            match self {
                Self::eAllocate => DeviceMemoryReportEventTypeEXT::eAllocate,
                Self::eFree => DeviceMemoryReportEventTypeEXT::eFree,
                Self::eImport => DeviceMemoryReportEventTypeEXT::eImport,
                Self::eUnimport => DeviceMemoryReportEventTypeEXT::eUnimport,
                Self::eAllocationFailed => DeviceMemoryReportEventTypeEXT::eAllocationFailed,
                v => DeviceMemoryReportEventTypeEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawDeviceMemoryReportEventTypeEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawRasterizationOrderAMD(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawRasterizationOrderAMD {
        pub const eStrict: Self = RawRasterizationOrderAMD(0);
        pub const eRelaxed: Self = RawRasterizationOrderAMD(1);

        pub fn normalise(self) -> RasterizationOrderAMD {
            match self {
                Self::eStrict => RasterizationOrderAMD::eStrict,
                Self::eRelaxed => RasterizationOrderAMD::eRelaxed,
                v => RasterizationOrderAMD::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawRasterizationOrderAMD {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawValidationCheckEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawValidationCheckEXT {
        pub const eAll: Self = RawValidationCheckEXT(0);
        pub const eShaders: Self = RawValidationCheckEXT(1);

        pub fn normalise(self) -> ValidationCheckEXT {
            match self {
                Self::eAll => ValidationCheckEXT::eAll,
                Self::eShaders => ValidationCheckEXT::eShaders,
                v => ValidationCheckEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawValidationCheckEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawValidationFeatureEnableEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawValidationFeatureEnableEXT {
        pub const eGpuAssisted: Self = RawValidationFeatureEnableEXT(0);
        pub const eGpuAssistedReserveBindingSlot: Self = RawValidationFeatureEnableEXT(1);
        pub const eBestPractices: Self = RawValidationFeatureEnableEXT(2);
        pub const eDebugPrintf: Self = RawValidationFeatureEnableEXT(3);
        pub const eSynchronizationValidation: Self = RawValidationFeatureEnableEXT(4);

        pub fn normalise(self) -> ValidationFeatureEnableEXT {
            match self {
                Self::eGpuAssisted => ValidationFeatureEnableEXT::eGpuAssisted,
                Self::eGpuAssistedReserveBindingSlot => ValidationFeatureEnableEXT::eGpuAssistedReserveBindingSlot,
                Self::eBestPractices => ValidationFeatureEnableEXT::eBestPractices,
                Self::eDebugPrintf => ValidationFeatureEnableEXT::eDebugPrintf,
                Self::eSynchronizationValidation => ValidationFeatureEnableEXT::eSynchronizationValidation,
                v => ValidationFeatureEnableEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawValidationFeatureEnableEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawValidationFeatureDisableEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawValidationFeatureDisableEXT {
        pub const eAll: Self = RawValidationFeatureDisableEXT(0);
        pub const eShaders: Self = RawValidationFeatureDisableEXT(1);
        pub const eThreadSafety: Self = RawValidationFeatureDisableEXT(2);
        pub const eApiParameters: Self = RawValidationFeatureDisableEXT(3);
        pub const eObjectLifetimes: Self = RawValidationFeatureDisableEXT(4);
        pub const eCoreChecks: Self = RawValidationFeatureDisableEXT(5);
        pub const eUniqueHandles: Self = RawValidationFeatureDisableEXT(6);
        pub const eShaderValidationCache: Self = RawValidationFeatureDisableEXT(7);

        pub fn normalise(self) -> ValidationFeatureDisableEXT {
            match self {
                Self::eAll => ValidationFeatureDisableEXT::eAll,
                Self::eShaders => ValidationFeatureDisableEXT::eShaders,
                Self::eThreadSafety => ValidationFeatureDisableEXT::eThreadSafety,
                Self::eApiParameters => ValidationFeatureDisableEXT::eApiParameters,
                Self::eObjectLifetimes => ValidationFeatureDisableEXT::eObjectLifetimes,
                Self::eCoreChecks => ValidationFeatureDisableEXT::eCoreChecks,
                Self::eUniqueHandles => ValidationFeatureDisableEXT::eUniqueHandles,
                Self::eShaderValidationCache => ValidationFeatureDisableEXT::eShaderValidationCache,
                v => ValidationFeatureDisableEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawValidationFeatureDisableEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawIndirectCommandsTokenTypeNV(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawIndirectCommandsTokenTypeNV {
        pub const eShaderGroup: Self = RawIndirectCommandsTokenTypeNV(0);
        pub const eStateFlags: Self = RawIndirectCommandsTokenTypeNV(1);
        pub const eIndexBuffer: Self = RawIndirectCommandsTokenTypeNV(2);
        pub const eVertexBuffer: Self = RawIndirectCommandsTokenTypeNV(3);
        pub const ePushConstant: Self = RawIndirectCommandsTokenTypeNV(4);
        pub const eDrawIndexed: Self = RawIndirectCommandsTokenTypeNV(5);
        pub const eDraw: Self = RawIndirectCommandsTokenTypeNV(6);
        pub const eDrawTasks: Self = RawIndirectCommandsTokenTypeNV(7);
        pub const eDrawMeshTasks: Self = RawIndirectCommandsTokenTypeNV(1000328000);

        pub fn normalise(self) -> IndirectCommandsTokenTypeNV {
            match self {
                Self::eShaderGroup => IndirectCommandsTokenTypeNV::eShaderGroup,
                Self::eStateFlags => IndirectCommandsTokenTypeNV::eStateFlags,
                Self::eIndexBuffer => IndirectCommandsTokenTypeNV::eIndexBuffer,
                Self::eVertexBuffer => IndirectCommandsTokenTypeNV::eVertexBuffer,
                Self::ePushConstant => IndirectCommandsTokenTypeNV::ePushConstant,
                Self::eDrawIndexed => IndirectCommandsTokenTypeNV::eDrawIndexed,
                Self::eDraw => IndirectCommandsTokenTypeNV::eDraw,
                Self::eDrawTasks => IndirectCommandsTokenTypeNV::eDrawTasks,
                Self::eDrawMeshTasks => IndirectCommandsTokenTypeNV::eDrawMeshTasks,
                v => IndirectCommandsTokenTypeNV::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawIndirectCommandsTokenTypeNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawDisplayPowerStateEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawDisplayPowerStateEXT {
        pub const eOff: Self = RawDisplayPowerStateEXT(0);
        pub const eSuspend: Self = RawDisplayPowerStateEXT(1);
        pub const eOn: Self = RawDisplayPowerStateEXT(2);

        pub fn normalise(self) -> DisplayPowerStateEXT {
            match self {
                Self::eOff => DisplayPowerStateEXT::eOff,
                Self::eSuspend => DisplayPowerStateEXT::eSuspend,
                Self::eOn => DisplayPowerStateEXT::eOn,
                v => DisplayPowerStateEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawDisplayPowerStateEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawDeviceEventTypeEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawDeviceEventTypeEXT {
        pub const eDisplayHotplug: Self = RawDeviceEventTypeEXT(0);

        pub fn normalise(self) -> DeviceEventTypeEXT {
            match self {
                Self::eDisplayHotplug => DeviceEventTypeEXT::eDisplayHotplug,
                v => DeviceEventTypeEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawDeviceEventTypeEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawDisplayEventTypeEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawDisplayEventTypeEXT {
        pub const eFirstPixelOut: Self = RawDisplayEventTypeEXT(0);

        pub fn normalise(self) -> DisplayEventTypeEXT {
            match self {
                Self::eFirstPixelOut => DisplayEventTypeEXT::eFirstPixelOut,
                v => DisplayEventTypeEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawDisplayEventTypeEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawViewportCoordinateSwizzleNV(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawViewportCoordinateSwizzleNV {
        pub const ePositiveX: Self = RawViewportCoordinateSwizzleNV(0);
        pub const eNegativeX: Self = RawViewportCoordinateSwizzleNV(1);
        pub const ePositiveY: Self = RawViewportCoordinateSwizzleNV(2);
        pub const eNegativeY: Self = RawViewportCoordinateSwizzleNV(3);
        pub const ePositiveZ: Self = RawViewportCoordinateSwizzleNV(4);
        pub const eNegativeZ: Self = RawViewportCoordinateSwizzleNV(5);
        pub const ePositiveW: Self = RawViewportCoordinateSwizzleNV(6);
        pub const eNegativeW: Self = RawViewportCoordinateSwizzleNV(7);

        pub fn normalise(self) -> ViewportCoordinateSwizzleNV {
            match self {
                Self::ePositiveX => ViewportCoordinateSwizzleNV::ePositiveX,
                Self::eNegativeX => ViewportCoordinateSwizzleNV::eNegativeX,
                Self::ePositiveY => ViewportCoordinateSwizzleNV::ePositiveY,
                Self::eNegativeY => ViewportCoordinateSwizzleNV::eNegativeY,
                Self::ePositiveZ => ViewportCoordinateSwizzleNV::ePositiveZ,
                Self::eNegativeZ => ViewportCoordinateSwizzleNV::eNegativeZ,
                Self::ePositiveW => ViewportCoordinateSwizzleNV::ePositiveW,
                Self::eNegativeW => ViewportCoordinateSwizzleNV::eNegativeW,
                v => ViewportCoordinateSwizzleNV::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawViewportCoordinateSwizzleNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawDiscardRectangleModeEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawDiscardRectangleModeEXT {
        pub const eInclusive: Self = RawDiscardRectangleModeEXT(0);
        pub const eExclusive: Self = RawDiscardRectangleModeEXT(1);

        pub fn normalise(self) -> DiscardRectangleModeEXT {
            match self {
                Self::eInclusive => DiscardRectangleModeEXT::eInclusive,
                Self::eExclusive => DiscardRectangleModeEXT::eExclusive,
                v => DiscardRectangleModeEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawDiscardRectangleModeEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawPointClippingBehavior(pub i32);

    pub type RawPointClippingBehaviorKHR = RawPointClippingBehavior;


    #[allow(non_upper_case_globals)]
    impl RawPointClippingBehavior {
        pub const eAllClipPlanes: Self = RawPointClippingBehavior(0);
        pub const eUserClipPlanesOnly: Self = RawPointClippingBehavior(1);
        pub const eAllClipPlanesKhr: Self = Self::eAllClipPlanes;
        pub const eUserClipPlanesOnlyKhr: Self = Self::eUserClipPlanesOnly;

        pub fn normalise(self) -> PointClippingBehavior {
            match self {
                Self::eAllClipPlanes => PointClippingBehavior::eAllClipPlanes,
                Self::eUserClipPlanesOnly => PointClippingBehavior::eUserClipPlanesOnly,
                v => PointClippingBehavior::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawPointClippingBehavior {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawSamplerReductionMode(pub i32);

    pub type RawSamplerReductionModeEXT = RawSamplerReductionMode;


    #[allow(non_upper_case_globals)]
    impl RawSamplerReductionMode {
        pub const eWeightedAverage: Self = RawSamplerReductionMode(0);
        pub const eMin: Self = RawSamplerReductionMode(1);
        pub const eMax: Self = RawSamplerReductionMode(2);
        pub const eMaxExt: Self = Self::eMax;
        pub const eMinExt: Self = Self::eMin;
        pub const eWeightedAverageExt: Self = Self::eWeightedAverage;

        pub fn normalise(self) -> SamplerReductionMode {
            match self {
                Self::eWeightedAverage => SamplerReductionMode::eWeightedAverage,
                Self::eMin => SamplerReductionMode::eMin,
                Self::eMax => SamplerReductionMode::eMax,
                v => SamplerReductionMode::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawSamplerReductionMode {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawTessellationDomainOrigin(pub i32);

    pub type RawTessellationDomainOriginKHR = RawTessellationDomainOrigin;


    #[allow(non_upper_case_globals)]
    impl RawTessellationDomainOrigin {
        pub const eUpperLeft: Self = RawTessellationDomainOrigin(0);
        pub const eLowerLeft: Self = RawTessellationDomainOrigin(1);
        pub const eLowerLeftKhr: Self = Self::eLowerLeft;
        pub const eUpperLeftKhr: Self = Self::eUpperLeft;

        pub fn normalise(self) -> TessellationDomainOrigin {
            match self {
                Self::eUpperLeft => TessellationDomainOrigin::eUpperLeft,
                Self::eLowerLeft => TessellationDomainOrigin::eLowerLeft,
                v => TessellationDomainOrigin::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawTessellationDomainOrigin {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawSamplerYcbcrModelConversion(pub i32);

    pub type RawSamplerYcbcrModelConversionKHR = RawSamplerYcbcrModelConversion;


    #[allow(non_upper_case_globals)]
    impl RawSamplerYcbcrModelConversion {
        pub const eRgbIdentity: Self = RawSamplerYcbcrModelConversion(0);
        pub const eYcbcrIdentity: Self = RawSamplerYcbcrModelConversion(1);
        pub const eYcbcr709: Self = RawSamplerYcbcrModelConversion(2);
        pub const eYcbcr601: Self = RawSamplerYcbcrModelConversion(3);
        pub const eYcbcr2020: Self = RawSamplerYcbcrModelConversion(4);
        pub const eRgbIdentityKhr: Self = Self::eRgbIdentity;
        pub const eYcbcr2020Khr: Self = Self::eYcbcr2020;
        pub const eYcbcr601Khr: Self = Self::eYcbcr601;
        pub const eYcbcr709Khr: Self = Self::eYcbcr709;
        pub const eYcbcrIdentityKhr: Self = Self::eYcbcrIdentity;

        pub fn normalise(self) -> SamplerYcbcrModelConversion {
            match self {
                Self::eRgbIdentity => SamplerYcbcrModelConversion::eRgbIdentity,
                Self::eYcbcrIdentity => SamplerYcbcrModelConversion::eYcbcrIdentity,
                Self::eYcbcr709 => SamplerYcbcrModelConversion::eYcbcr709,
                Self::eYcbcr601 => SamplerYcbcrModelConversion::eYcbcr601,
                Self::eYcbcr2020 => SamplerYcbcrModelConversion::eYcbcr2020,
                v => SamplerYcbcrModelConversion::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawSamplerYcbcrModelConversion {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawSamplerYcbcrRange(pub i32);

    pub type RawSamplerYcbcrRangeKHR = RawSamplerYcbcrRange;


    #[allow(non_upper_case_globals)]
    impl RawSamplerYcbcrRange {
        pub const eItuFull: Self = RawSamplerYcbcrRange(0);
        pub const eItuNarrow: Self = RawSamplerYcbcrRange(1);
        pub const eItuFullKhr: Self = Self::eItuFull;
        pub const eItuNarrowKhr: Self = Self::eItuNarrow;

        pub fn normalise(self) -> SamplerYcbcrRange {
            match self {
                Self::eItuFull => SamplerYcbcrRange::eItuFull,
                Self::eItuNarrow => SamplerYcbcrRange::eItuNarrow,
                v => SamplerYcbcrRange::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawSamplerYcbcrRange {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawChromaLocation(pub i32);

    pub type RawChromaLocationKHR = RawChromaLocation;


    #[allow(non_upper_case_globals)]
    impl RawChromaLocation {
        pub const eCositedEven: Self = RawChromaLocation(0);
        pub const eMidpoint: Self = RawChromaLocation(1);
        pub const eCositedEvenKhr: Self = Self::eCositedEven;
        pub const eMidpointKhr: Self = Self::eMidpoint;

        pub fn normalise(self) -> ChromaLocation {
            match self {
                Self::eCositedEven => ChromaLocation::eCositedEven,
                Self::eMidpoint => ChromaLocation::eMidpoint,
                v => ChromaLocation::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawChromaLocation {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawBlendOverlapEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawBlendOverlapEXT {
        pub const eUncorrelated: Self = RawBlendOverlapEXT(0);
        pub const eDisjoint: Self = RawBlendOverlapEXT(1);
        pub const eConjoint: Self = RawBlendOverlapEXT(2);

        pub fn normalise(self) -> BlendOverlapEXT {
            match self {
                Self::eUncorrelated => BlendOverlapEXT::eUncorrelated,
                Self::eDisjoint => BlendOverlapEXT::eDisjoint,
                Self::eConjoint => BlendOverlapEXT::eConjoint,
                v => BlendOverlapEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawBlendOverlapEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawCoverageModulationModeNV(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawCoverageModulationModeNV {
        pub const eNone: Self = RawCoverageModulationModeNV(0);
        pub const eRgb: Self = RawCoverageModulationModeNV(1);
        pub const eAlpha: Self = RawCoverageModulationModeNV(2);
        pub const eRgba: Self = RawCoverageModulationModeNV(3);

        pub fn normalise(self) -> CoverageModulationModeNV {
            match self {
                Self::eNone => CoverageModulationModeNV::eNone,
                Self::eRgb => CoverageModulationModeNV::eRgb,
                Self::eAlpha => CoverageModulationModeNV::eAlpha,
                Self::eRgba => CoverageModulationModeNV::eRgba,
                v => CoverageModulationModeNV::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawCoverageModulationModeNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawCoverageReductionModeNV(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawCoverageReductionModeNV {
        pub const eMerge: Self = RawCoverageReductionModeNV(0);
        pub const eTruncate: Self = RawCoverageReductionModeNV(1);

        pub fn normalise(self) -> CoverageReductionModeNV {
            match self {
                Self::eMerge => CoverageReductionModeNV::eMerge,
                Self::eTruncate => CoverageReductionModeNV::eTruncate,
                v => CoverageReductionModeNV::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawCoverageReductionModeNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawValidationCacheHeaderVersionEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawValidationCacheHeaderVersionEXT {
        pub const eOne: Self = RawValidationCacheHeaderVersionEXT(1);

        pub fn normalise(self) -> ValidationCacheHeaderVersionEXT {
            match self {
                Self::eOne => ValidationCacheHeaderVersionEXT::eOne,
                v => ValidationCacheHeaderVersionEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawValidationCacheHeaderVersionEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawShaderInfoTypeAMD(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawShaderInfoTypeAMD {
        pub const eStatistics: Self = RawShaderInfoTypeAMD(0);
        pub const eBinary: Self = RawShaderInfoTypeAMD(1);
        pub const eDisassembly: Self = RawShaderInfoTypeAMD(2);

        pub fn normalise(self) -> ShaderInfoTypeAMD {
            match self {
                Self::eStatistics => ShaderInfoTypeAMD::eStatistics,
                Self::eBinary => ShaderInfoTypeAMD::eBinary,
                Self::eDisassembly => ShaderInfoTypeAMD::eDisassembly,
                v => ShaderInfoTypeAMD::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawShaderInfoTypeAMD {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawQueueGlobalPriorityKHR(pub i32);

    pub type RawQueueGlobalPriorityEXT = RawQueueGlobalPriorityKHR;


    #[allow(non_upper_case_globals)]
    impl RawQueueGlobalPriorityKHR {
        pub const eLow: Self = RawQueueGlobalPriorityKHR(128);
        pub const eMedium: Self = RawQueueGlobalPriorityKHR(256);
        pub const eHigh: Self = RawQueueGlobalPriorityKHR(512);
        pub const eRealtime: Self = RawQueueGlobalPriorityKHR(1024);
        pub const eHighExt: Self = Self::eHigh;
        pub const eLowExt: Self = Self::eLow;
        pub const eMediumExt: Self = Self::eMedium;
        pub const eRealtimeExt: Self = Self::eRealtime;

        pub fn normalise(self) -> QueueGlobalPriorityKHR {
            match self {
                Self::eLow => QueueGlobalPriorityKHR::eLow,
                Self::eMedium => QueueGlobalPriorityKHR::eMedium,
                Self::eHigh => QueueGlobalPriorityKHR::eHigh,
                Self::eRealtime => QueueGlobalPriorityKHR::eRealtime,
                v => QueueGlobalPriorityKHR::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawQueueGlobalPriorityKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawConservativeRasterizationModeEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawConservativeRasterizationModeEXT {
        pub const eDisabled: Self = RawConservativeRasterizationModeEXT(0);
        pub const eOverestimate: Self = RawConservativeRasterizationModeEXT(1);
        pub const eUnderestimate: Self = RawConservativeRasterizationModeEXT(2);

        pub fn normalise(self) -> ConservativeRasterizationModeEXT {
            match self {
                Self::eDisabled => ConservativeRasterizationModeEXT::eDisabled,
                Self::eOverestimate => ConservativeRasterizationModeEXT::eOverestimate,
                Self::eUnderestimate => ConservativeRasterizationModeEXT::eUnderestimate,
                v => ConservativeRasterizationModeEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawConservativeRasterizationModeEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawVendorId(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawVendorId {
        pub const eViv: Self = RawVendorId(65537);
        pub const eVsi: Self = RawVendorId(65538);
        pub const eKazan: Self = RawVendorId(65539);
        pub const eCodeplay: Self = RawVendorId(65540);
        pub const eMesa: Self = RawVendorId(65541);
        pub const ePocl: Self = RawVendorId(65542);

        pub fn normalise(self) -> VendorId {
            match self {
                Self::eViv => VendorId::eViv,
                Self::eVsi => VendorId::eVsi,
                Self::eKazan => VendorId::eKazan,
                Self::eCodeplay => VendorId::eCodeplay,
                Self::eMesa => VendorId::eMesa,
                Self::ePocl => VendorId::ePocl,
                v => VendorId::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawVendorId {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawDriverId(pub i32);

    pub type RawDriverIdKHR = RawDriverId;


    #[allow(non_upper_case_globals)]
    impl RawDriverId {
        pub const eAmdProprietary: Self = RawDriverId(1);
        pub const eAmdOpenSource: Self = RawDriverId(2);
        pub const eMesaRadv: Self = RawDriverId(3);
        pub const eNvidiaProprietary: Self = RawDriverId(4);
        pub const eIntelProprietaryWindows: Self = RawDriverId(5);
        pub const eIntelOpenSourceMesa: Self = RawDriverId(6);
        pub const eImaginationProprietary: Self = RawDriverId(7);
        pub const eQualcommProprietary: Self = RawDriverId(8);
        pub const eArmProprietary: Self = RawDriverId(9);
        pub const eGoogleSwiftshader: Self = RawDriverId(10);
        pub const eGgpProprietary: Self = RawDriverId(11);
        pub const eBroadcomProprietary: Self = RawDriverId(12);
        pub const eMesaLlvmpipe: Self = RawDriverId(13);
        pub const eMoltenvk: Self = RawDriverId(14);
        pub const eCoreaviProprietary: Self = RawDriverId(15);
        pub const eJuiceProprietary: Self = RawDriverId(16);
        pub const eVerisiliconProprietary: Self = RawDriverId(17);
        pub const eMesaTurnip: Self = RawDriverId(18);
        pub const eMesaV3dv: Self = RawDriverId(19);
        pub const eMesaPanvk: Self = RawDriverId(20);
        pub const eSamsungProprietary: Self = RawDriverId(21);
        pub const eMesaVenus: Self = RawDriverId(22);
        pub const eMesaDozen: Self = RawDriverId(23);
        pub const eMesaNvk: Self = RawDriverId(24);
        pub const eImaginationOpenSourceMesa: Self = RawDriverId(25);
        pub const eAmdOpenSourceKhr: Self = Self::eAmdOpenSource;
        pub const eAmdProprietaryKhr: Self = Self::eAmdProprietary;
        pub const eArmProprietaryKhr: Self = Self::eArmProprietary;
        pub const eBroadcomProprietaryKhr: Self = Self::eBroadcomProprietary;
        pub const eGgpProprietaryKhr: Self = Self::eGgpProprietary;
        pub const eGoogleSwiftshaderKhr: Self = Self::eGoogleSwiftshader;
        pub const eImaginationProprietaryKhr: Self = Self::eImaginationProprietary;
        pub const eIntelOpenSourceMesaKhr: Self = Self::eIntelOpenSourceMesa;
        pub const eIntelProprietaryWindowsKhr: Self = Self::eIntelProprietaryWindows;
        pub const eMesaRadvKhr: Self = Self::eMesaRadv;
        pub const eNvidiaProprietaryKhr: Self = Self::eNvidiaProprietary;
        pub const eQualcommProprietaryKhr: Self = Self::eQualcommProprietary;

        pub fn normalise(self) -> DriverId {
            match self {
                Self::eAmdProprietary => DriverId::eAmdProprietary,
                Self::eAmdOpenSource => DriverId::eAmdOpenSource,
                Self::eMesaRadv => DriverId::eMesaRadv,
                Self::eNvidiaProprietary => DriverId::eNvidiaProprietary,
                Self::eIntelProprietaryWindows => DriverId::eIntelProprietaryWindows,
                Self::eIntelOpenSourceMesa => DriverId::eIntelOpenSourceMesa,
                Self::eImaginationProprietary => DriverId::eImaginationProprietary,
                Self::eQualcommProprietary => DriverId::eQualcommProprietary,
                Self::eArmProprietary => DriverId::eArmProprietary,
                Self::eGoogleSwiftshader => DriverId::eGoogleSwiftshader,
                Self::eGgpProprietary => DriverId::eGgpProprietary,
                Self::eBroadcomProprietary => DriverId::eBroadcomProprietary,
                Self::eMesaLlvmpipe => DriverId::eMesaLlvmpipe,
                Self::eMoltenvk => DriverId::eMoltenvk,
                Self::eCoreaviProprietary => DriverId::eCoreaviProprietary,
                Self::eJuiceProprietary => DriverId::eJuiceProprietary,
                Self::eVerisiliconProprietary => DriverId::eVerisiliconProprietary,
                Self::eMesaTurnip => DriverId::eMesaTurnip,
                Self::eMesaV3dv => DriverId::eMesaV3dv,
                Self::eMesaPanvk => DriverId::eMesaPanvk,
                Self::eSamsungProprietary => DriverId::eSamsungProprietary,
                Self::eMesaVenus => DriverId::eMesaVenus,
                Self::eMesaDozen => DriverId::eMesaDozen,
                Self::eMesaNvk => DriverId::eMesaNvk,
                Self::eImaginationOpenSourceMesa => DriverId::eImaginationOpenSourceMesa,
                v => DriverId::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawDriverId {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawShadingRatePaletteEntryNV(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawShadingRatePaletteEntryNV {
        pub const eNoInvocations: Self = RawShadingRatePaletteEntryNV(0);
        pub const e16InvocationsPerPixel: Self = RawShadingRatePaletteEntryNV(1);
        pub const e8InvocationsPerPixel: Self = RawShadingRatePaletteEntryNV(2);
        pub const e4InvocationsPerPixel: Self = RawShadingRatePaletteEntryNV(3);
        pub const e2InvocationsPerPixel: Self = RawShadingRatePaletteEntryNV(4);
        pub const e1InvocationPerPixel: Self = RawShadingRatePaletteEntryNV(5);
        pub const e1InvocationPer2X1Pixels: Self = RawShadingRatePaletteEntryNV(6);
        pub const e1InvocationPer1X2Pixels: Self = RawShadingRatePaletteEntryNV(7);
        pub const e1InvocationPer2X2Pixels: Self = RawShadingRatePaletteEntryNV(8);
        pub const e1InvocationPer4X2Pixels: Self = RawShadingRatePaletteEntryNV(9);
        pub const e1InvocationPer2X4Pixels: Self = RawShadingRatePaletteEntryNV(10);
        pub const e1InvocationPer4X4Pixels: Self = RawShadingRatePaletteEntryNV(11);

        pub fn normalise(self) -> ShadingRatePaletteEntryNV {
            match self {
                Self::eNoInvocations => ShadingRatePaletteEntryNV::eNoInvocations,
                Self::e16InvocationsPerPixel => ShadingRatePaletteEntryNV::e16InvocationsPerPixel,
                Self::e8InvocationsPerPixel => ShadingRatePaletteEntryNV::e8InvocationsPerPixel,
                Self::e4InvocationsPerPixel => ShadingRatePaletteEntryNV::e4InvocationsPerPixel,
                Self::e2InvocationsPerPixel => ShadingRatePaletteEntryNV::e2InvocationsPerPixel,
                Self::e1InvocationPerPixel => ShadingRatePaletteEntryNV::e1InvocationPerPixel,
                Self::e1InvocationPer2X1Pixels => ShadingRatePaletteEntryNV::e1InvocationPer2X1Pixels,
                Self::e1InvocationPer1X2Pixels => ShadingRatePaletteEntryNV::e1InvocationPer1X2Pixels,
                Self::e1InvocationPer2X2Pixels => ShadingRatePaletteEntryNV::e1InvocationPer2X2Pixels,
                Self::e1InvocationPer4X2Pixels => ShadingRatePaletteEntryNV::e1InvocationPer4X2Pixels,
                Self::e1InvocationPer2X4Pixels => ShadingRatePaletteEntryNV::e1InvocationPer2X4Pixels,
                Self::e1InvocationPer4X4Pixels => ShadingRatePaletteEntryNV::e1InvocationPer4X4Pixels,
                v => ShadingRatePaletteEntryNV::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawShadingRatePaletteEntryNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawCoarseSampleOrderTypeNV(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawCoarseSampleOrderTypeNV {
        pub const eDefault: Self = RawCoarseSampleOrderTypeNV(0);
        pub const eCustom: Self = RawCoarseSampleOrderTypeNV(1);
        pub const ePixelMajor: Self = RawCoarseSampleOrderTypeNV(2);
        pub const eSampleMajor: Self = RawCoarseSampleOrderTypeNV(3);

        pub fn normalise(self) -> CoarseSampleOrderTypeNV {
            match self {
                Self::eDefault => CoarseSampleOrderTypeNV::eDefault,
                Self::eCustom => CoarseSampleOrderTypeNV::eCustom,
                Self::ePixelMajor => CoarseSampleOrderTypeNV::ePixelMajor,
                Self::eSampleMajor => CoarseSampleOrderTypeNV::eSampleMajor,
                v => CoarseSampleOrderTypeNV::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawCoarseSampleOrderTypeNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawCopyAccelerationStructureModeKHR(pub i32);

    pub type RawCopyAccelerationStructureModeNV = RawCopyAccelerationStructureModeKHR;


    #[allow(non_upper_case_globals)]
    impl RawCopyAccelerationStructureModeKHR {
        pub const eClone: Self = RawCopyAccelerationStructureModeKHR(0);
        pub const eCompact: Self = RawCopyAccelerationStructureModeKHR(1);
        pub const eSerialize: Self = RawCopyAccelerationStructureModeKHR(2);
        pub const eDeserialize: Self = RawCopyAccelerationStructureModeKHR(3);
        pub const eCloneNv: Self = Self::eClone;
        pub const eCompactNv: Self = Self::eCompact;

        pub fn normalise(self) -> CopyAccelerationStructureModeKHR {
            match self {
                Self::eClone => CopyAccelerationStructureModeKHR::eClone,
                Self::eCompact => CopyAccelerationStructureModeKHR::eCompact,
                Self::eSerialize => CopyAccelerationStructureModeKHR::eSerialize,
                Self::eDeserialize => CopyAccelerationStructureModeKHR::eDeserialize,
                v => CopyAccelerationStructureModeKHR::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawCopyAccelerationStructureModeKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawBuildAccelerationStructureModeKHR(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawBuildAccelerationStructureModeKHR {
        pub const eBuild: Self = RawBuildAccelerationStructureModeKHR(0);
        pub const eUpdate: Self = RawBuildAccelerationStructureModeKHR(1);

        pub fn normalise(self) -> BuildAccelerationStructureModeKHR {
            match self {
                Self::eBuild => BuildAccelerationStructureModeKHR::eBuild,
                Self::eUpdate => BuildAccelerationStructureModeKHR::eUpdate,
                v => BuildAccelerationStructureModeKHR::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawBuildAccelerationStructureModeKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawAccelerationStructureTypeKHR(pub i32);

    pub type RawAccelerationStructureTypeNV = RawAccelerationStructureTypeKHR;


    #[allow(non_upper_case_globals)]
    impl RawAccelerationStructureTypeKHR {
        pub const eTopLevel: Self = RawAccelerationStructureTypeKHR(0);
        pub const eBottomLevel: Self = RawAccelerationStructureTypeKHR(1);
        pub const eGeneric: Self = RawAccelerationStructureTypeKHR(2);
        pub const eBottomLevelNv: Self = Self::eBottomLevel;
        pub const eTopLevelNv: Self = Self::eTopLevel;

        pub fn normalise(self) -> AccelerationStructureTypeKHR {
            match self {
                Self::eTopLevel => AccelerationStructureTypeKHR::eTopLevel,
                Self::eBottomLevel => AccelerationStructureTypeKHR::eBottomLevel,
                Self::eGeneric => AccelerationStructureTypeKHR::eGeneric,
                v => AccelerationStructureTypeKHR::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawAccelerationStructureTypeKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawGeometryTypeKHR(pub i32);

    pub type RawGeometryTypeNV = RawGeometryTypeKHR;


    #[allow(non_upper_case_globals)]
    impl RawGeometryTypeKHR {
        pub const eTriangles: Self = RawGeometryTypeKHR(0);
        pub const eAabbs: Self = RawGeometryTypeKHR(1);
        pub const eInstances: Self = RawGeometryTypeKHR(2);
        pub const eAabbsNv: Self = Self::eAabbs;
        pub const eTrianglesNv: Self = Self::eTriangles;

        pub fn normalise(self) -> GeometryTypeKHR {
            match self {
                Self::eTriangles => GeometryTypeKHR::eTriangles,
                Self::eAabbs => GeometryTypeKHR::eAabbs,
                Self::eInstances => GeometryTypeKHR::eInstances,
                v => GeometryTypeKHR::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawGeometryTypeKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawAccelerationStructureMemoryRequirementsTypeNV(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawAccelerationStructureMemoryRequirementsTypeNV {
        pub const eObject: Self = RawAccelerationStructureMemoryRequirementsTypeNV(0);
        pub const eBuildScratch: Self = RawAccelerationStructureMemoryRequirementsTypeNV(1);
        pub const eUpdateScratch: Self = RawAccelerationStructureMemoryRequirementsTypeNV(2);

        pub fn normalise(self) -> AccelerationStructureMemoryRequirementsTypeNV {
            match self {
                Self::eObject => AccelerationStructureMemoryRequirementsTypeNV::eObject,
                Self::eBuildScratch => AccelerationStructureMemoryRequirementsTypeNV::eBuildScratch,
                Self::eUpdateScratch => AccelerationStructureMemoryRequirementsTypeNV::eUpdateScratch,
                v => AccelerationStructureMemoryRequirementsTypeNV::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawAccelerationStructureMemoryRequirementsTypeNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawAccelerationStructureBuildTypeKHR(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawAccelerationStructureBuildTypeKHR {
        pub const eHost: Self = RawAccelerationStructureBuildTypeKHR(0);
        pub const eDevice: Self = RawAccelerationStructureBuildTypeKHR(1);
        pub const eHostOrDevice: Self = RawAccelerationStructureBuildTypeKHR(2);

        pub fn normalise(self) -> AccelerationStructureBuildTypeKHR {
            match self {
                Self::eHost => AccelerationStructureBuildTypeKHR::eHost,
                Self::eDevice => AccelerationStructureBuildTypeKHR::eDevice,
                Self::eHostOrDevice => AccelerationStructureBuildTypeKHR::eHostOrDevice,
                v => AccelerationStructureBuildTypeKHR::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawAccelerationStructureBuildTypeKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawRayTracingShaderGroupTypeKHR(pub i32);

    pub type RawRayTracingShaderGroupTypeNV = RawRayTracingShaderGroupTypeKHR;


    #[allow(non_upper_case_globals)]
    impl RawRayTracingShaderGroupTypeKHR {
        pub const eGeneral: Self = RawRayTracingShaderGroupTypeKHR(0);
        pub const eTrianglesHitGroup: Self = RawRayTracingShaderGroupTypeKHR(1);
        pub const eProceduralHitGroup: Self = RawRayTracingShaderGroupTypeKHR(2);
        pub const eGeneralNv: Self = Self::eGeneral;
        pub const eProceduralHitGroupNv: Self = Self::eProceduralHitGroup;
        pub const eTrianglesHitGroupNv: Self = Self::eTrianglesHitGroup;

        pub fn normalise(self) -> RayTracingShaderGroupTypeKHR {
            match self {
                Self::eGeneral => RayTracingShaderGroupTypeKHR::eGeneral,
                Self::eTrianglesHitGroup => RayTracingShaderGroupTypeKHR::eTrianglesHitGroup,
                Self::eProceduralHitGroup => RayTracingShaderGroupTypeKHR::eProceduralHitGroup,
                v => RayTracingShaderGroupTypeKHR::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawRayTracingShaderGroupTypeKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawAccelerationStructureCompatibilityKHR(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawAccelerationStructureCompatibilityKHR {
        pub const eCompatible: Self = RawAccelerationStructureCompatibilityKHR(0);
        pub const eIncompatible: Self = RawAccelerationStructureCompatibilityKHR(1);

        pub fn normalise(self) -> AccelerationStructureCompatibilityKHR {
            match self {
                Self::eCompatible => AccelerationStructureCompatibilityKHR::eCompatible,
                Self::eIncompatible => AccelerationStructureCompatibilityKHR::eIncompatible,
                v => AccelerationStructureCompatibilityKHR::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawAccelerationStructureCompatibilityKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawShaderGroupShaderKHR(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawShaderGroupShaderKHR {
        pub const eGeneral: Self = RawShaderGroupShaderKHR(0);
        pub const eClosestHit: Self = RawShaderGroupShaderKHR(1);
        pub const eAnyHit: Self = RawShaderGroupShaderKHR(2);
        pub const eIntersection: Self = RawShaderGroupShaderKHR(3);

        pub fn normalise(self) -> ShaderGroupShaderKHR {
            match self {
                Self::eGeneral => ShaderGroupShaderKHR::eGeneral,
                Self::eClosestHit => ShaderGroupShaderKHR::eClosestHit,
                Self::eAnyHit => ShaderGroupShaderKHR::eAnyHit,
                Self::eIntersection => ShaderGroupShaderKHR::eIntersection,
                v => ShaderGroupShaderKHR::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawShaderGroupShaderKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawMemoryOverallocationBehaviorAMD(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawMemoryOverallocationBehaviorAMD {
        pub const eDefault: Self = RawMemoryOverallocationBehaviorAMD(0);
        pub const eAllowed: Self = RawMemoryOverallocationBehaviorAMD(1);
        pub const eDisallowed: Self = RawMemoryOverallocationBehaviorAMD(2);

        pub fn normalise(self) -> MemoryOverallocationBehaviorAMD {
            match self {
                Self::eDefault => MemoryOverallocationBehaviorAMD::eDefault,
                Self::eAllowed => MemoryOverallocationBehaviorAMD::eAllowed,
                Self::eDisallowed => MemoryOverallocationBehaviorAMD::eDisallowed,
                v => MemoryOverallocationBehaviorAMD::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawMemoryOverallocationBehaviorAMD {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawScopeNV(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawScopeNV {
        pub const eDevice: Self = RawScopeNV(1);
        pub const eWorkgroup: Self = RawScopeNV(2);
        pub const eSubgroup: Self = RawScopeNV(3);
        pub const eQueueFamily: Self = RawScopeNV(5);

        pub fn normalise(self) -> ScopeNV {
            match self {
                Self::eDevice => ScopeNV::eDevice,
                Self::eWorkgroup => ScopeNV::eWorkgroup,
                Self::eSubgroup => ScopeNV::eSubgroup,
                Self::eQueueFamily => ScopeNV::eQueueFamily,
                v => ScopeNV::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawScopeNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawComponentTypeNV(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawComponentTypeNV {
        pub const eFloat16: Self = RawComponentTypeNV(0);
        pub const eFloat32: Self = RawComponentTypeNV(1);
        pub const eFloat64: Self = RawComponentTypeNV(2);
        pub const eSint8: Self = RawComponentTypeNV(3);
        pub const eSint16: Self = RawComponentTypeNV(4);
        pub const eSint32: Self = RawComponentTypeNV(5);
        pub const eSint64: Self = RawComponentTypeNV(6);
        pub const eUint8: Self = RawComponentTypeNV(7);
        pub const eUint16: Self = RawComponentTypeNV(8);
        pub const eUint32: Self = RawComponentTypeNV(9);
        pub const eUint64: Self = RawComponentTypeNV(10);

        pub fn normalise(self) -> ComponentTypeNV {
            match self {
                Self::eFloat16 => ComponentTypeNV::eFloat16,
                Self::eFloat32 => ComponentTypeNV::eFloat32,
                Self::eFloat64 => ComponentTypeNV::eFloat64,
                Self::eSint8 => ComponentTypeNV::eSint8,
                Self::eSint16 => ComponentTypeNV::eSint16,
                Self::eSint32 => ComponentTypeNV::eSint32,
                Self::eSint64 => ComponentTypeNV::eSint64,
                Self::eUint8 => ComponentTypeNV::eUint8,
                Self::eUint16 => ComponentTypeNV::eUint16,
                Self::eUint32 => ComponentTypeNV::eUint32,
                Self::eUint64 => ComponentTypeNV::eUint64,
                v => ComponentTypeNV::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawComponentTypeNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawFullScreenExclusiveEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawFullScreenExclusiveEXT {
        pub const eDefault: Self = RawFullScreenExclusiveEXT(0);
        pub const eAllowed: Self = RawFullScreenExclusiveEXT(1);
        pub const eDisallowed: Self = RawFullScreenExclusiveEXT(2);
        pub const eApplicationControlled: Self = RawFullScreenExclusiveEXT(3);

        pub fn normalise(self) -> FullScreenExclusiveEXT {
            match self {
                Self::eDefault => FullScreenExclusiveEXT::eDefault,
                Self::eAllowed => FullScreenExclusiveEXT::eAllowed,
                Self::eDisallowed => FullScreenExclusiveEXT::eDisallowed,
                Self::eApplicationControlled => FullScreenExclusiveEXT::eApplicationControlled,
                v => FullScreenExclusiveEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawFullScreenExclusiveEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawPerformanceCounterScopeKHR(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawPerformanceCounterScopeKHR {
        pub const eCommandBuffer: Self = RawPerformanceCounterScopeKHR(0);
        pub const eRenderPass: Self = RawPerformanceCounterScopeKHR(1);
        pub const eCommand: Self = RawPerformanceCounterScopeKHR(2);
        pub const eQueryScopeCommandBuffer: Self = Self::eCommandBuffer;
        pub const eQueryScopeCommand: Self = Self::eCommand;
        pub const eQueryScopeRenderPass: Self = Self::eRenderPass;

        pub fn normalise(self) -> PerformanceCounterScopeKHR {
            match self {
                Self::eCommandBuffer => PerformanceCounterScopeKHR::eCommandBuffer,
                Self::eRenderPass => PerformanceCounterScopeKHR::eRenderPass,
                Self::eCommand => PerformanceCounterScopeKHR::eCommand,
                v => PerformanceCounterScopeKHR::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawPerformanceCounterScopeKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawPerformanceCounterUnitKHR(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawPerformanceCounterUnitKHR {
        pub const eGeneric: Self = RawPerformanceCounterUnitKHR(0);
        pub const ePercentage: Self = RawPerformanceCounterUnitKHR(1);
        pub const eNanoseconds: Self = RawPerformanceCounterUnitKHR(2);
        pub const eBytes: Self = RawPerformanceCounterUnitKHR(3);
        pub const eBytesPerSecond: Self = RawPerformanceCounterUnitKHR(4);
        pub const eKelvin: Self = RawPerformanceCounterUnitKHR(5);
        pub const eWatts: Self = RawPerformanceCounterUnitKHR(6);
        pub const eVolts: Self = RawPerformanceCounterUnitKHR(7);
        pub const eAmps: Self = RawPerformanceCounterUnitKHR(8);
        pub const eHertz: Self = RawPerformanceCounterUnitKHR(9);
        pub const eCycles: Self = RawPerformanceCounterUnitKHR(10);

        pub fn normalise(self) -> PerformanceCounterUnitKHR {
            match self {
                Self::eGeneric => PerformanceCounterUnitKHR::eGeneric,
                Self::ePercentage => PerformanceCounterUnitKHR::ePercentage,
                Self::eNanoseconds => PerformanceCounterUnitKHR::eNanoseconds,
                Self::eBytes => PerformanceCounterUnitKHR::eBytes,
                Self::eBytesPerSecond => PerformanceCounterUnitKHR::eBytesPerSecond,
                Self::eKelvin => PerformanceCounterUnitKHR::eKelvin,
                Self::eWatts => PerformanceCounterUnitKHR::eWatts,
                Self::eVolts => PerformanceCounterUnitKHR::eVolts,
                Self::eAmps => PerformanceCounterUnitKHR::eAmps,
                Self::eHertz => PerformanceCounterUnitKHR::eHertz,
                Self::eCycles => PerformanceCounterUnitKHR::eCycles,
                v => PerformanceCounterUnitKHR::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawPerformanceCounterUnitKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawPerformanceCounterStorageKHR(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawPerformanceCounterStorageKHR {
        pub const eInt32: Self = RawPerformanceCounterStorageKHR(0);
        pub const eInt64: Self = RawPerformanceCounterStorageKHR(1);
        pub const eUint32: Self = RawPerformanceCounterStorageKHR(2);
        pub const eUint64: Self = RawPerformanceCounterStorageKHR(3);
        pub const eFloat32: Self = RawPerformanceCounterStorageKHR(4);
        pub const eFloat64: Self = RawPerformanceCounterStorageKHR(5);

        pub fn normalise(self) -> PerformanceCounterStorageKHR {
            match self {
                Self::eInt32 => PerformanceCounterStorageKHR::eInt32,
                Self::eInt64 => PerformanceCounterStorageKHR::eInt64,
                Self::eUint32 => PerformanceCounterStorageKHR::eUint32,
                Self::eUint64 => PerformanceCounterStorageKHR::eUint64,
                Self::eFloat32 => PerformanceCounterStorageKHR::eFloat32,
                Self::eFloat64 => PerformanceCounterStorageKHR::eFloat64,
                v => PerformanceCounterStorageKHR::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawPerformanceCounterStorageKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawPerformanceConfigurationTypeINTEL(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawPerformanceConfigurationTypeINTEL {
        pub const eCommandQueueMetricsDiscoveryActivated: Self = RawPerformanceConfigurationTypeINTEL(0);

        pub fn normalise(self) -> PerformanceConfigurationTypeINTEL {
            match self {
                Self::eCommandQueueMetricsDiscoveryActivated => PerformanceConfigurationTypeINTEL::eCommandQueueMetricsDiscoveryActivated,
                v => PerformanceConfigurationTypeINTEL::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawPerformanceConfigurationTypeINTEL {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawQueryPoolSamplingModeINTEL(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawQueryPoolSamplingModeINTEL {
        pub const eManual: Self = RawQueryPoolSamplingModeINTEL(0);

        pub fn normalise(self) -> QueryPoolSamplingModeINTEL {
            match self {
                Self::eManual => QueryPoolSamplingModeINTEL::eManual,
                v => QueryPoolSamplingModeINTEL::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawQueryPoolSamplingModeINTEL {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawPerformanceOverrideTypeINTEL(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawPerformanceOverrideTypeINTEL {
        pub const eNullHardware: Self = RawPerformanceOverrideTypeINTEL(0);
        pub const eFlushGpuCaches: Self = RawPerformanceOverrideTypeINTEL(1);

        pub fn normalise(self) -> PerformanceOverrideTypeINTEL {
            match self {
                Self::eNullHardware => PerformanceOverrideTypeINTEL::eNullHardware,
                Self::eFlushGpuCaches => PerformanceOverrideTypeINTEL::eFlushGpuCaches,
                v => PerformanceOverrideTypeINTEL::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawPerformanceOverrideTypeINTEL {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawPerformanceParameterTypeINTEL(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawPerformanceParameterTypeINTEL {
        pub const eHwCountersSupported: Self = RawPerformanceParameterTypeINTEL(0);
        pub const eStreamMarkerValidBits: Self = RawPerformanceParameterTypeINTEL(1);

        pub fn normalise(self) -> PerformanceParameterTypeINTEL {
            match self {
                Self::eHwCountersSupported => PerformanceParameterTypeINTEL::eHwCountersSupported,
                Self::eStreamMarkerValidBits => PerformanceParameterTypeINTEL::eStreamMarkerValidBits,
                v => PerformanceParameterTypeINTEL::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawPerformanceParameterTypeINTEL {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawPerformanceValueTypeINTEL(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawPerformanceValueTypeINTEL {
        pub const eUint32: Self = RawPerformanceValueTypeINTEL(0);
        pub const eUint64: Self = RawPerformanceValueTypeINTEL(1);
        pub const eFloat: Self = RawPerformanceValueTypeINTEL(2);
        pub const eBool: Self = RawPerformanceValueTypeINTEL(3);
        pub const eString: Self = RawPerformanceValueTypeINTEL(4);

        pub fn normalise(self) -> PerformanceValueTypeINTEL {
            match self {
                Self::eUint32 => PerformanceValueTypeINTEL::eUint32,
                Self::eUint64 => PerformanceValueTypeINTEL::eUint64,
                Self::eFloat => PerformanceValueTypeINTEL::eFloat,
                Self::eBool => PerformanceValueTypeINTEL::eBool,
                Self::eString => PerformanceValueTypeINTEL::eString,
                v => PerformanceValueTypeINTEL::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawPerformanceValueTypeINTEL {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawShaderFloatControlsIndependence(pub i32);

    pub type RawShaderFloatControlsIndependenceKHR = RawShaderFloatControlsIndependence;


    #[allow(non_upper_case_globals)]
    impl RawShaderFloatControlsIndependence {
        pub const e32BitOnly: Self = RawShaderFloatControlsIndependence(0);
        pub const eAll: Self = RawShaderFloatControlsIndependence(1);
        pub const eNone: Self = RawShaderFloatControlsIndependence(2);
        pub const e32BitOnlyKhr: Self = Self::e32BitOnly;
        pub const eAllKhr: Self = Self::eAll;
        pub const eNoneKhr: Self = Self::eNone;

        pub fn normalise(self) -> ShaderFloatControlsIndependence {
            match self {
                Self::e32BitOnly => ShaderFloatControlsIndependence::e32BitOnly,
                Self::eAll => ShaderFloatControlsIndependence::eAll,
                Self::eNone => ShaderFloatControlsIndependence::eNone,
                v => ShaderFloatControlsIndependence::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawShaderFloatControlsIndependence {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawPipelineExecutableStatisticFormatKHR(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawPipelineExecutableStatisticFormatKHR {
        pub const eBool32: Self = RawPipelineExecutableStatisticFormatKHR(0);
        pub const eInt64: Self = RawPipelineExecutableStatisticFormatKHR(1);
        pub const eUint64: Self = RawPipelineExecutableStatisticFormatKHR(2);
        pub const eFloat64: Self = RawPipelineExecutableStatisticFormatKHR(3);

        pub fn normalise(self) -> PipelineExecutableStatisticFormatKHR {
            match self {
                Self::eBool32 => PipelineExecutableStatisticFormatKHR::eBool32,
                Self::eInt64 => PipelineExecutableStatisticFormatKHR::eInt64,
                Self::eUint64 => PipelineExecutableStatisticFormatKHR::eUint64,
                Self::eFloat64 => PipelineExecutableStatisticFormatKHR::eFloat64,
                v => PipelineExecutableStatisticFormatKHR::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawPipelineExecutableStatisticFormatKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawLineRasterizationModeEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawLineRasterizationModeEXT {
        pub const eDefault: Self = RawLineRasterizationModeEXT(0);
        pub const eRectangular: Self = RawLineRasterizationModeEXT(1);
        pub const eBresenham: Self = RawLineRasterizationModeEXT(2);
        pub const eRectangularSmooth: Self = RawLineRasterizationModeEXT(3);

        pub fn normalise(self) -> LineRasterizationModeEXT {
            match self {
                Self::eDefault => LineRasterizationModeEXT::eDefault,
                Self::eRectangular => LineRasterizationModeEXT::eRectangular,
                Self::eBresenham => LineRasterizationModeEXT::eBresenham,
                Self::eRectangularSmooth => LineRasterizationModeEXT::eRectangularSmooth,
                v => LineRasterizationModeEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawLineRasterizationModeEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawFragmentShadingRateCombinerOpKHR(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawFragmentShadingRateCombinerOpKHR {
        pub const eKeep: Self = RawFragmentShadingRateCombinerOpKHR(0);
        pub const eReplace: Self = RawFragmentShadingRateCombinerOpKHR(1);
        pub const eMin: Self = RawFragmentShadingRateCombinerOpKHR(2);
        pub const eMax: Self = RawFragmentShadingRateCombinerOpKHR(3);
        pub const eMul: Self = RawFragmentShadingRateCombinerOpKHR(4);

        pub fn normalise(self) -> FragmentShadingRateCombinerOpKHR {
            match self {
                Self::eKeep => FragmentShadingRateCombinerOpKHR::eKeep,
                Self::eReplace => FragmentShadingRateCombinerOpKHR::eReplace,
                Self::eMin => FragmentShadingRateCombinerOpKHR::eMin,
                Self::eMax => FragmentShadingRateCombinerOpKHR::eMax,
                Self::eMul => FragmentShadingRateCombinerOpKHR::eMul,
                v => FragmentShadingRateCombinerOpKHR::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawFragmentShadingRateCombinerOpKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawFragmentShadingRateNV(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawFragmentShadingRateNV {
        pub const e1InvocationPerPixel: Self = RawFragmentShadingRateNV(0);
        pub const e1InvocationPer1X2Pixels: Self = RawFragmentShadingRateNV(1);
        pub const e1InvocationPer2X1Pixels: Self = RawFragmentShadingRateNV(4);
        pub const e1InvocationPer2X2Pixels: Self = RawFragmentShadingRateNV(5);
        pub const e1InvocationPer2X4Pixels: Self = RawFragmentShadingRateNV(6);
        pub const e1InvocationPer4X2Pixels: Self = RawFragmentShadingRateNV(9);
        pub const e1InvocationPer4X4Pixels: Self = RawFragmentShadingRateNV(10);
        pub const e2InvocationsPerPixel: Self = RawFragmentShadingRateNV(11);
        pub const e4InvocationsPerPixel: Self = RawFragmentShadingRateNV(12);
        pub const e8InvocationsPerPixel: Self = RawFragmentShadingRateNV(13);
        pub const e16InvocationsPerPixel: Self = RawFragmentShadingRateNV(14);
        pub const eNoInvocations: Self = RawFragmentShadingRateNV(15);

        pub fn normalise(self) -> FragmentShadingRateNV {
            match self {
                Self::e1InvocationPerPixel => FragmentShadingRateNV::e1InvocationPerPixel,
                Self::e1InvocationPer1X2Pixels => FragmentShadingRateNV::e1InvocationPer1X2Pixels,
                Self::e1InvocationPer2X1Pixels => FragmentShadingRateNV::e1InvocationPer2X1Pixels,
                Self::e1InvocationPer2X2Pixels => FragmentShadingRateNV::e1InvocationPer2X2Pixels,
                Self::e1InvocationPer2X4Pixels => FragmentShadingRateNV::e1InvocationPer2X4Pixels,
                Self::e1InvocationPer4X2Pixels => FragmentShadingRateNV::e1InvocationPer4X2Pixels,
                Self::e1InvocationPer4X4Pixels => FragmentShadingRateNV::e1InvocationPer4X4Pixels,
                Self::e2InvocationsPerPixel => FragmentShadingRateNV::e2InvocationsPerPixel,
                Self::e4InvocationsPerPixel => FragmentShadingRateNV::e4InvocationsPerPixel,
                Self::e8InvocationsPerPixel => FragmentShadingRateNV::e8InvocationsPerPixel,
                Self::e16InvocationsPerPixel => FragmentShadingRateNV::e16InvocationsPerPixel,
                Self::eNoInvocations => FragmentShadingRateNV::eNoInvocations,
                v => FragmentShadingRateNV::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawFragmentShadingRateNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawFragmentShadingRateTypeNV(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawFragmentShadingRateTypeNV {
        pub const eFragmentSize: Self = RawFragmentShadingRateTypeNV(0);
        pub const eEnums: Self = RawFragmentShadingRateTypeNV(1);

        pub fn normalise(self) -> FragmentShadingRateTypeNV {
            match self {
                Self::eFragmentSize => FragmentShadingRateTypeNV::eFragmentSize,
                Self::eEnums => FragmentShadingRateTypeNV::eEnums,
                v => FragmentShadingRateTypeNV::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawFragmentShadingRateTypeNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawSubpassMergeStatusEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawSubpassMergeStatusEXT {
        pub const eMerged: Self = RawSubpassMergeStatusEXT(0);
        pub const eDisallowed: Self = RawSubpassMergeStatusEXT(1);
        pub const eNotMergedSideEffects: Self = RawSubpassMergeStatusEXT(2);
        pub const eNotMergedSamplesMismatch: Self = RawSubpassMergeStatusEXT(3);
        pub const eNotMergedViewsMismatch: Self = RawSubpassMergeStatusEXT(4);
        pub const eNotMergedAliasing: Self = RawSubpassMergeStatusEXT(5);
        pub const eNotMergedDependencies: Self = RawSubpassMergeStatusEXT(6);
        pub const eNotMergedIncompatibleInputAttachment: Self = RawSubpassMergeStatusEXT(7);
        pub const eNotMergedTooManyAttachments: Self = RawSubpassMergeStatusEXT(8);
        pub const eNotMergedInsufficientStorage: Self = RawSubpassMergeStatusEXT(9);
        pub const eNotMergedDepthStencilCount: Self = RawSubpassMergeStatusEXT(10);
        pub const eNotMergedResolveAttachmentReuse: Self = RawSubpassMergeStatusEXT(11);
        pub const eNotMergedSingleSubpass: Self = RawSubpassMergeStatusEXT(12);
        pub const eNotMergedUnspecified: Self = RawSubpassMergeStatusEXT(13);

        pub fn normalise(self) -> SubpassMergeStatusEXT {
            match self {
                Self::eMerged => SubpassMergeStatusEXT::eMerged,
                Self::eDisallowed => SubpassMergeStatusEXT::eDisallowed,
                Self::eNotMergedSideEffects => SubpassMergeStatusEXT::eNotMergedSideEffects,
                Self::eNotMergedSamplesMismatch => SubpassMergeStatusEXT::eNotMergedSamplesMismatch,
                Self::eNotMergedViewsMismatch => SubpassMergeStatusEXT::eNotMergedViewsMismatch,
                Self::eNotMergedAliasing => SubpassMergeStatusEXT::eNotMergedAliasing,
                Self::eNotMergedDependencies => SubpassMergeStatusEXT::eNotMergedDependencies,
                Self::eNotMergedIncompatibleInputAttachment => SubpassMergeStatusEXT::eNotMergedIncompatibleInputAttachment,
                Self::eNotMergedTooManyAttachments => SubpassMergeStatusEXT::eNotMergedTooManyAttachments,
                Self::eNotMergedInsufficientStorage => SubpassMergeStatusEXT::eNotMergedInsufficientStorage,
                Self::eNotMergedDepthStencilCount => SubpassMergeStatusEXT::eNotMergedDepthStencilCount,
                Self::eNotMergedResolveAttachmentReuse => SubpassMergeStatusEXT::eNotMergedResolveAttachmentReuse,
                Self::eNotMergedSingleSubpass => SubpassMergeStatusEXT::eNotMergedSingleSubpass,
                Self::eNotMergedUnspecified => SubpassMergeStatusEXT::eNotMergedUnspecified,
                v => SubpassMergeStatusEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawSubpassMergeStatusEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawProvokingVertexModeEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawProvokingVertexModeEXT {
        pub const eFirstVertex: Self = RawProvokingVertexModeEXT(0);
        pub const eLastVertex: Self = RawProvokingVertexModeEXT(1);

        pub fn normalise(self) -> ProvokingVertexModeEXT {
            match self {
                Self::eFirstVertex => ProvokingVertexModeEXT::eFirstVertex,
                Self::eLastVertex => ProvokingVertexModeEXT::eLastVertex,
                v => ProvokingVertexModeEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawProvokingVertexModeEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawAccelerationStructureMotionInstanceTypeNV(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawAccelerationStructureMotionInstanceTypeNV {
        pub const eStatic: Self = RawAccelerationStructureMotionInstanceTypeNV(0);
        pub const eMatrixMotion: Self = RawAccelerationStructureMotionInstanceTypeNV(1);
        pub const eSrtMotion: Self = RawAccelerationStructureMotionInstanceTypeNV(2);

        pub fn normalise(self) -> AccelerationStructureMotionInstanceTypeNV {
            match self {
                Self::eStatic => AccelerationStructureMotionInstanceTypeNV::eStatic,
                Self::eMatrixMotion => AccelerationStructureMotionInstanceTypeNV::eMatrixMotion,
                Self::eSrtMotion => AccelerationStructureMotionInstanceTypeNV::eSrtMotion,
                v => AccelerationStructureMotionInstanceTypeNV::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawAccelerationStructureMotionInstanceTypeNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawDeviceAddressBindingTypeEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawDeviceAddressBindingTypeEXT {
        pub const eBind: Self = RawDeviceAddressBindingTypeEXT(0);
        pub const eUnbind: Self = RawDeviceAddressBindingTypeEXT(1);

        pub fn normalise(self) -> DeviceAddressBindingTypeEXT {
            match self {
                Self::eBind => DeviceAddressBindingTypeEXT::eBind,
                Self::eUnbind => DeviceAddressBindingTypeEXT::eUnbind,
                v => DeviceAddressBindingTypeEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawDeviceAddressBindingTypeEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawQueryResultStatusKHR(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawQueryResultStatusKHR {
        pub const eError: Self = RawQueryResultStatusKHR(-1);
        pub const eNotReady: Self = RawQueryResultStatusKHR(0);
        pub const eComplete: Self = RawQueryResultStatusKHR(1);

        pub fn normalise(self) -> QueryResultStatusKHR {
            match self {
                Self::eError => QueryResultStatusKHR::eError,
                Self::eNotReady => QueryResultStatusKHR::eNotReady,
                Self::eComplete => QueryResultStatusKHR::eComplete,
                v => QueryResultStatusKHR::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawQueryResultStatusKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawPipelineRobustnessBufferBehaviorEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawPipelineRobustnessBufferBehaviorEXT {
        pub const eDeviceDefault: Self = RawPipelineRobustnessBufferBehaviorEXT(0);
        pub const eDisabled: Self = RawPipelineRobustnessBufferBehaviorEXT(1);
        pub const eRobustBufferAccess: Self = RawPipelineRobustnessBufferBehaviorEXT(2);
        pub const eRobustBufferAccess2: Self = RawPipelineRobustnessBufferBehaviorEXT(3);

        pub fn normalise(self) -> PipelineRobustnessBufferBehaviorEXT {
            match self {
                Self::eDeviceDefault => PipelineRobustnessBufferBehaviorEXT::eDeviceDefault,
                Self::eDisabled => PipelineRobustnessBufferBehaviorEXT::eDisabled,
                Self::eRobustBufferAccess => PipelineRobustnessBufferBehaviorEXT::eRobustBufferAccess,
                Self::eRobustBufferAccess2 => PipelineRobustnessBufferBehaviorEXT::eRobustBufferAccess2,
                v => PipelineRobustnessBufferBehaviorEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawPipelineRobustnessBufferBehaviorEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawPipelineRobustnessImageBehaviorEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawPipelineRobustnessImageBehaviorEXT {
        pub const eDeviceDefault: Self = RawPipelineRobustnessImageBehaviorEXT(0);
        pub const eDisabled: Self = RawPipelineRobustnessImageBehaviorEXT(1);
        pub const eRobustImageAccess: Self = RawPipelineRobustnessImageBehaviorEXT(2);
        pub const eRobustImageAccess2: Self = RawPipelineRobustnessImageBehaviorEXT(3);

        pub fn normalise(self) -> PipelineRobustnessImageBehaviorEXT {
            match self {
                Self::eDeviceDefault => PipelineRobustnessImageBehaviorEXT::eDeviceDefault,
                Self::eDisabled => PipelineRobustnessImageBehaviorEXT::eDisabled,
                Self::eRobustImageAccess => PipelineRobustnessImageBehaviorEXT::eRobustImageAccess,
                Self::eRobustImageAccess2 => PipelineRobustnessImageBehaviorEXT::eRobustImageAccess2,
                v => PipelineRobustnessImageBehaviorEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawPipelineRobustnessImageBehaviorEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawOpticalFlowPerformanceLevelNV(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawOpticalFlowPerformanceLevelNV {
        pub const eUnknown: Self = RawOpticalFlowPerformanceLevelNV(0);
        pub const eSlow: Self = RawOpticalFlowPerformanceLevelNV(1);
        pub const eMedium: Self = RawOpticalFlowPerformanceLevelNV(2);
        pub const eFast: Self = RawOpticalFlowPerformanceLevelNV(3);

        pub fn normalise(self) -> OpticalFlowPerformanceLevelNV {
            match self {
                Self::eUnknown => OpticalFlowPerformanceLevelNV::eUnknown,
                Self::eSlow => OpticalFlowPerformanceLevelNV::eSlow,
                Self::eMedium => OpticalFlowPerformanceLevelNV::eMedium,
                Self::eFast => OpticalFlowPerformanceLevelNV::eFast,
                v => OpticalFlowPerformanceLevelNV::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawOpticalFlowPerformanceLevelNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawOpticalFlowSessionBindingPointNV(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawOpticalFlowSessionBindingPointNV {
        pub const eUnknown: Self = RawOpticalFlowSessionBindingPointNV(0);
        pub const eInput: Self = RawOpticalFlowSessionBindingPointNV(1);
        pub const eReference: Self = RawOpticalFlowSessionBindingPointNV(2);
        pub const eHint: Self = RawOpticalFlowSessionBindingPointNV(3);
        pub const eFlowVector: Self = RawOpticalFlowSessionBindingPointNV(4);
        pub const eBackwardFlowVector: Self = RawOpticalFlowSessionBindingPointNV(5);
        pub const eCost: Self = RawOpticalFlowSessionBindingPointNV(6);
        pub const eBackwardCost: Self = RawOpticalFlowSessionBindingPointNV(7);
        pub const eGlobalFlow: Self = RawOpticalFlowSessionBindingPointNV(8);

        pub fn normalise(self) -> OpticalFlowSessionBindingPointNV {
            match self {
                Self::eUnknown => OpticalFlowSessionBindingPointNV::eUnknown,
                Self::eInput => OpticalFlowSessionBindingPointNV::eInput,
                Self::eReference => OpticalFlowSessionBindingPointNV::eReference,
                Self::eHint => OpticalFlowSessionBindingPointNV::eHint,
                Self::eFlowVector => OpticalFlowSessionBindingPointNV::eFlowVector,
                Self::eBackwardFlowVector => OpticalFlowSessionBindingPointNV::eBackwardFlowVector,
                Self::eCost => OpticalFlowSessionBindingPointNV::eCost,
                Self::eBackwardCost => OpticalFlowSessionBindingPointNV::eBackwardCost,
                Self::eGlobalFlow => OpticalFlowSessionBindingPointNV::eGlobalFlow,
                v => OpticalFlowSessionBindingPointNV::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawOpticalFlowSessionBindingPointNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawMicromapTypeEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawMicromapTypeEXT {
        pub const eOpacityMicromap: Self = RawMicromapTypeEXT(0);

        pub fn normalise(self) -> MicromapTypeEXT {
            match self {
                Self::eOpacityMicromap => MicromapTypeEXT::eOpacityMicromap,
                v => MicromapTypeEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawMicromapTypeEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawCopyMicromapModeEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawCopyMicromapModeEXT {
        pub const eClone: Self = RawCopyMicromapModeEXT(0);
        pub const eSerialize: Self = RawCopyMicromapModeEXT(1);
        pub const eDeserialize: Self = RawCopyMicromapModeEXT(2);
        pub const eCompact: Self = RawCopyMicromapModeEXT(3);

        pub fn normalise(self) -> CopyMicromapModeEXT {
            match self {
                Self::eClone => CopyMicromapModeEXT::eClone,
                Self::eSerialize => CopyMicromapModeEXT::eSerialize,
                Self::eDeserialize => CopyMicromapModeEXT::eDeserialize,
                Self::eCompact => CopyMicromapModeEXT::eCompact,
                v => CopyMicromapModeEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawCopyMicromapModeEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawBuildMicromapModeEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawBuildMicromapModeEXT {
        pub const eBuild: Self = RawBuildMicromapModeEXT(0);

        pub fn normalise(self) -> BuildMicromapModeEXT {
            match self {
                Self::eBuild => BuildMicromapModeEXT::eBuild,
                v => BuildMicromapModeEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawBuildMicromapModeEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawOpacityMicromapFormatEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawOpacityMicromapFormatEXT {
        pub const e2State: Self = RawOpacityMicromapFormatEXT(1);
        pub const e4State: Self = RawOpacityMicromapFormatEXT(2);

        pub fn normalise(self) -> OpacityMicromapFormatEXT {
            match self {
                Self::e2State => OpacityMicromapFormatEXT::e2State,
                Self::e4State => OpacityMicromapFormatEXT::e4State,
                v => OpacityMicromapFormatEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawOpacityMicromapFormatEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawOpacityMicromapSpecialIndexEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawOpacityMicromapSpecialIndexEXT {
        pub const eFullyUnknownOpaque: Self = RawOpacityMicromapSpecialIndexEXT(-4);
        pub const eFullyUnknownTransparent: Self = RawOpacityMicromapSpecialIndexEXT(-3);
        pub const eFullyOpaque: Self = RawOpacityMicromapSpecialIndexEXT(-2);
        pub const eFullyTransparent: Self = RawOpacityMicromapSpecialIndexEXT(-1);

        pub fn normalise(self) -> OpacityMicromapSpecialIndexEXT {
            match self {
                Self::eFullyUnknownOpaque => OpacityMicromapSpecialIndexEXT::eFullyUnknownOpaque,
                Self::eFullyUnknownTransparent => OpacityMicromapSpecialIndexEXT::eFullyUnknownTransparent,
                Self::eFullyOpaque => OpacityMicromapSpecialIndexEXT::eFullyOpaque,
                Self::eFullyTransparent => OpacityMicromapSpecialIndexEXT::eFullyTransparent,
                v => OpacityMicromapSpecialIndexEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawOpacityMicromapSpecialIndexEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawDeviceFaultAddressTypeEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawDeviceFaultAddressTypeEXT {
        pub const eNone: Self = RawDeviceFaultAddressTypeEXT(0);
        pub const eReadInvalid: Self = RawDeviceFaultAddressTypeEXT(1);
        pub const eWriteInvalid: Self = RawDeviceFaultAddressTypeEXT(2);
        pub const eExecuteInvalid: Self = RawDeviceFaultAddressTypeEXT(3);
        pub const eInstructionPointerUnknown: Self = RawDeviceFaultAddressTypeEXT(4);
        pub const eInstructionPointerInvalid: Self = RawDeviceFaultAddressTypeEXT(5);
        pub const eInstructionPointerFault: Self = RawDeviceFaultAddressTypeEXT(6);

        pub fn normalise(self) -> DeviceFaultAddressTypeEXT {
            match self {
                Self::eNone => DeviceFaultAddressTypeEXT::eNone,
                Self::eReadInvalid => DeviceFaultAddressTypeEXT::eReadInvalid,
                Self::eWriteInvalid => DeviceFaultAddressTypeEXT::eWriteInvalid,
                Self::eExecuteInvalid => DeviceFaultAddressTypeEXT::eExecuteInvalid,
                Self::eInstructionPointerUnknown => DeviceFaultAddressTypeEXT::eInstructionPointerUnknown,
                Self::eInstructionPointerInvalid => DeviceFaultAddressTypeEXT::eInstructionPointerInvalid,
                Self::eInstructionPointerFault => DeviceFaultAddressTypeEXT::eInstructionPointerFault,
                v => DeviceFaultAddressTypeEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawDeviceFaultAddressTypeEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawDeviceFaultVendorBinaryHeaderVersionEXT(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawDeviceFaultVendorBinaryHeaderVersionEXT {
        pub const eOne: Self = RawDeviceFaultVendorBinaryHeaderVersionEXT(1);

        pub fn normalise(self) -> DeviceFaultVendorBinaryHeaderVersionEXT {
            match self {
                Self::eOne => DeviceFaultVendorBinaryHeaderVersionEXT::eOne,
                v => DeviceFaultVendorBinaryHeaderVersionEXT::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawDeviceFaultVendorBinaryHeaderVersionEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH264ChromaFormatIdc(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH264ChromaFormatIdc {
        pub const eMonochrome: Self = RawStdVideoH264ChromaFormatIdc(0);
        pub const e420: Self = RawStdVideoH264ChromaFormatIdc(1);
        pub const e422: Self = RawStdVideoH264ChromaFormatIdc(2);
        pub const e444: Self = RawStdVideoH264ChromaFormatIdc(3);
        pub const eInvalid: Self = RawStdVideoH264ChromaFormatIdc(2147483647);

        pub fn normalise(self) -> StdVideoH264ChromaFormatIdc {
            match self {
                Self::eMonochrome => StdVideoH264ChromaFormatIdc::eMonochrome,
                Self::e420 => StdVideoH264ChromaFormatIdc::e420,
                Self::e422 => StdVideoH264ChromaFormatIdc::e422,
                Self::e444 => StdVideoH264ChromaFormatIdc::e444,
                Self::eInvalid => StdVideoH264ChromaFormatIdc::eInvalid,
                v => StdVideoH264ChromaFormatIdc::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH264ChromaFormatIdc {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH264ProfileIdc(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH264ProfileIdc {
        pub const eBaseline: Self = RawStdVideoH264ProfileIdc(66);
        pub const eMain: Self = RawStdVideoH264ProfileIdc(77);
        pub const eHigh: Self = RawStdVideoH264ProfileIdc(100);
        pub const eHigh444Predictive: Self = RawStdVideoH264ProfileIdc(244);
        pub const eInvalid: Self = RawStdVideoH264ProfileIdc(2147483647);

        pub fn normalise(self) -> StdVideoH264ProfileIdc {
            match self {
                Self::eBaseline => StdVideoH264ProfileIdc::eBaseline,
                Self::eMain => StdVideoH264ProfileIdc::eMain,
                Self::eHigh => StdVideoH264ProfileIdc::eHigh,
                Self::eHigh444Predictive => StdVideoH264ProfileIdc::eHigh444Predictive,
                Self::eInvalid => StdVideoH264ProfileIdc::eInvalid,
                v => StdVideoH264ProfileIdc::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH264ProfileIdc {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH264LevelIdc(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH264LevelIdc {
        pub const e10: Self = RawStdVideoH264LevelIdc(0);
        pub const e11: Self = RawStdVideoH264LevelIdc(1);
        pub const e12: Self = RawStdVideoH264LevelIdc(2);
        pub const e13: Self = RawStdVideoH264LevelIdc(3);
        pub const e20: Self = RawStdVideoH264LevelIdc(4);
        pub const e21: Self = RawStdVideoH264LevelIdc(5);
        pub const e22: Self = RawStdVideoH264LevelIdc(6);
        pub const e30: Self = RawStdVideoH264LevelIdc(7);
        pub const e31: Self = RawStdVideoH264LevelIdc(8);
        pub const e32: Self = RawStdVideoH264LevelIdc(9);
        pub const e40: Self = RawStdVideoH264LevelIdc(10);
        pub const e41: Self = RawStdVideoH264LevelIdc(11);
        pub const e42: Self = RawStdVideoH264LevelIdc(12);
        pub const e50: Self = RawStdVideoH264LevelIdc(13);
        pub const e51: Self = RawStdVideoH264LevelIdc(14);
        pub const e52: Self = RawStdVideoH264LevelIdc(15);
        pub const e60: Self = RawStdVideoH264LevelIdc(16);
        pub const e61: Self = RawStdVideoH264LevelIdc(17);
        pub const e62: Self = RawStdVideoH264LevelIdc(18);
        pub const eInvalid: Self = RawStdVideoH264LevelIdc(2147483647);

        pub fn normalise(self) -> StdVideoH264LevelIdc {
            match self {
                Self::e10 => StdVideoH264LevelIdc::e10,
                Self::e11 => StdVideoH264LevelIdc::e11,
                Self::e12 => StdVideoH264LevelIdc::e12,
                Self::e13 => StdVideoH264LevelIdc::e13,
                Self::e20 => StdVideoH264LevelIdc::e20,
                Self::e21 => StdVideoH264LevelIdc::e21,
                Self::e22 => StdVideoH264LevelIdc::e22,
                Self::e30 => StdVideoH264LevelIdc::e30,
                Self::e31 => StdVideoH264LevelIdc::e31,
                Self::e32 => StdVideoH264LevelIdc::e32,
                Self::e40 => StdVideoH264LevelIdc::e40,
                Self::e41 => StdVideoH264LevelIdc::e41,
                Self::e42 => StdVideoH264LevelIdc::e42,
                Self::e50 => StdVideoH264LevelIdc::e50,
                Self::e51 => StdVideoH264LevelIdc::e51,
                Self::e52 => StdVideoH264LevelIdc::e52,
                Self::e60 => StdVideoH264LevelIdc::e60,
                Self::e61 => StdVideoH264LevelIdc::e61,
                Self::e62 => StdVideoH264LevelIdc::e62,
                Self::eInvalid => StdVideoH264LevelIdc::eInvalid,
                v => StdVideoH264LevelIdc::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH264LevelIdc {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH264PocType(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH264PocType {
        pub const e0: Self = RawStdVideoH264PocType(0);
        pub const e1: Self = RawStdVideoH264PocType(1);
        pub const e2: Self = RawStdVideoH264PocType(2);
        pub const eInvalid: Self = RawStdVideoH264PocType(2147483647);

        pub fn normalise(self) -> StdVideoH264PocType {
            match self {
                Self::e0 => StdVideoH264PocType::e0,
                Self::e1 => StdVideoH264PocType::e1,
                Self::e2 => StdVideoH264PocType::e2,
                Self::eInvalid => StdVideoH264PocType::eInvalid,
                v => StdVideoH264PocType::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH264PocType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH264AspectRatioIdc(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH264AspectRatioIdc {
        pub const eUnspecified: Self = RawStdVideoH264AspectRatioIdc(0);
        pub const eSquare: Self = RawStdVideoH264AspectRatioIdc(1);
        pub const e1211: Self = RawStdVideoH264AspectRatioIdc(2);
        pub const e1011: Self = RawStdVideoH264AspectRatioIdc(3);
        pub const e1611: Self = RawStdVideoH264AspectRatioIdc(4);
        pub const e4033: Self = RawStdVideoH264AspectRatioIdc(5);
        pub const e2411: Self = RawStdVideoH264AspectRatioIdc(6);
        pub const e2011: Self = RawStdVideoH264AspectRatioIdc(7);
        pub const e3211: Self = RawStdVideoH264AspectRatioIdc(8);
        pub const e8033: Self = RawStdVideoH264AspectRatioIdc(9);
        pub const e1811: Self = RawStdVideoH264AspectRatioIdc(10);
        pub const e1511: Self = RawStdVideoH264AspectRatioIdc(11);
        pub const e6433: Self = RawStdVideoH264AspectRatioIdc(12);
        pub const e16099: Self = RawStdVideoH264AspectRatioIdc(13);
        pub const e43: Self = RawStdVideoH264AspectRatioIdc(14);
        pub const e32: Self = RawStdVideoH264AspectRatioIdc(15);
        pub const e21: Self = RawStdVideoH264AspectRatioIdc(16);
        pub const eExtendedSar: Self = RawStdVideoH264AspectRatioIdc(255);
        pub const eInvalid: Self = RawStdVideoH264AspectRatioIdc(2147483647);

        pub fn normalise(self) -> StdVideoH264AspectRatioIdc {
            match self {
                Self::eUnspecified => StdVideoH264AspectRatioIdc::eUnspecified,
                Self::eSquare => StdVideoH264AspectRatioIdc::eSquare,
                Self::e1211 => StdVideoH264AspectRatioIdc::e1211,
                Self::e1011 => StdVideoH264AspectRatioIdc::e1011,
                Self::e1611 => StdVideoH264AspectRatioIdc::e1611,
                Self::e4033 => StdVideoH264AspectRatioIdc::e4033,
                Self::e2411 => StdVideoH264AspectRatioIdc::e2411,
                Self::e2011 => StdVideoH264AspectRatioIdc::e2011,
                Self::e3211 => StdVideoH264AspectRatioIdc::e3211,
                Self::e8033 => StdVideoH264AspectRatioIdc::e8033,
                Self::e1811 => StdVideoH264AspectRatioIdc::e1811,
                Self::e1511 => StdVideoH264AspectRatioIdc::e1511,
                Self::e6433 => StdVideoH264AspectRatioIdc::e6433,
                Self::e16099 => StdVideoH264AspectRatioIdc::e16099,
                Self::e43 => StdVideoH264AspectRatioIdc::e43,
                Self::e32 => StdVideoH264AspectRatioIdc::e32,
                Self::e21 => StdVideoH264AspectRatioIdc::e21,
                Self::eExtendedSar => StdVideoH264AspectRatioIdc::eExtendedSar,
                Self::eInvalid => StdVideoH264AspectRatioIdc::eInvalid,
                v => StdVideoH264AspectRatioIdc::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH264AspectRatioIdc {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH264WeightedBipredIdc(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH264WeightedBipredIdc {
        pub const eDefault: Self = RawStdVideoH264WeightedBipredIdc(0);
        pub const eExplicit: Self = RawStdVideoH264WeightedBipredIdc(1);
        pub const eImplicit: Self = RawStdVideoH264WeightedBipredIdc(2);
        pub const eInvalid: Self = RawStdVideoH264WeightedBipredIdc(2147483647);

        pub fn normalise(self) -> StdVideoH264WeightedBipredIdc {
            match self {
                Self::eDefault => StdVideoH264WeightedBipredIdc::eDefault,
                Self::eExplicit => StdVideoH264WeightedBipredIdc::eExplicit,
                Self::eImplicit => StdVideoH264WeightedBipredIdc::eImplicit,
                Self::eInvalid => StdVideoH264WeightedBipredIdc::eInvalid,
                v => StdVideoH264WeightedBipredIdc::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH264WeightedBipredIdc {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH264ModificationOfPicNumsIdc(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH264ModificationOfPicNumsIdc {
        pub const eShortTermSubtract: Self = RawStdVideoH264ModificationOfPicNumsIdc(0);
        pub const eShortTermAdd: Self = RawStdVideoH264ModificationOfPicNumsIdc(1);
        pub const eLongTerm: Self = RawStdVideoH264ModificationOfPicNumsIdc(2);
        pub const eEnd: Self = RawStdVideoH264ModificationOfPicNumsIdc(3);
        pub const eInvalid: Self = RawStdVideoH264ModificationOfPicNumsIdc(2147483647);

        pub fn normalise(self) -> StdVideoH264ModificationOfPicNumsIdc {
            match self {
                Self::eShortTermSubtract => StdVideoH264ModificationOfPicNumsIdc::eShortTermSubtract,
                Self::eShortTermAdd => StdVideoH264ModificationOfPicNumsIdc::eShortTermAdd,
                Self::eLongTerm => StdVideoH264ModificationOfPicNumsIdc::eLongTerm,
                Self::eEnd => StdVideoH264ModificationOfPicNumsIdc::eEnd,
                Self::eInvalid => StdVideoH264ModificationOfPicNumsIdc::eInvalid,
                v => StdVideoH264ModificationOfPicNumsIdc::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH264ModificationOfPicNumsIdc {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH264MemMgmtControlOp(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH264MemMgmtControlOp {
        pub const eEnd: Self = RawStdVideoH264MemMgmtControlOp(0);
        pub const eUnmarkShortTerm: Self = RawStdVideoH264MemMgmtControlOp(1);
        pub const eUnmarkLongTerm: Self = RawStdVideoH264MemMgmtControlOp(2);
        pub const eMarkLongTerm: Self = RawStdVideoH264MemMgmtControlOp(3);
        pub const eSetMaxLongTermIndex: Self = RawStdVideoH264MemMgmtControlOp(4);
        pub const eUnmarkAll: Self = RawStdVideoH264MemMgmtControlOp(5);
        pub const eMarkCurrentAsLongTerm: Self = RawStdVideoH264MemMgmtControlOp(6);
        pub const eInvalid: Self = RawStdVideoH264MemMgmtControlOp(2147483647);

        pub fn normalise(self) -> StdVideoH264MemMgmtControlOp {
            match self {
                Self::eEnd => StdVideoH264MemMgmtControlOp::eEnd,
                Self::eUnmarkShortTerm => StdVideoH264MemMgmtControlOp::eUnmarkShortTerm,
                Self::eUnmarkLongTerm => StdVideoH264MemMgmtControlOp::eUnmarkLongTerm,
                Self::eMarkLongTerm => StdVideoH264MemMgmtControlOp::eMarkLongTerm,
                Self::eSetMaxLongTermIndex => StdVideoH264MemMgmtControlOp::eSetMaxLongTermIndex,
                Self::eUnmarkAll => StdVideoH264MemMgmtControlOp::eUnmarkAll,
                Self::eMarkCurrentAsLongTerm => StdVideoH264MemMgmtControlOp::eMarkCurrentAsLongTerm,
                Self::eInvalid => StdVideoH264MemMgmtControlOp::eInvalid,
                v => StdVideoH264MemMgmtControlOp::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH264MemMgmtControlOp {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH264CabacInitIdc(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH264CabacInitIdc {
        pub const e0: Self = RawStdVideoH264CabacInitIdc(0);
        pub const e1: Self = RawStdVideoH264CabacInitIdc(1);
        pub const e2: Self = RawStdVideoH264CabacInitIdc(2);
        pub const eInvalid: Self = RawStdVideoH264CabacInitIdc(2147483647);

        pub fn normalise(self) -> StdVideoH264CabacInitIdc {
            match self {
                Self::e0 => StdVideoH264CabacInitIdc::e0,
                Self::e1 => StdVideoH264CabacInitIdc::e1,
                Self::e2 => StdVideoH264CabacInitIdc::e2,
                Self::eInvalid => StdVideoH264CabacInitIdc::eInvalid,
                v => StdVideoH264CabacInitIdc::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH264CabacInitIdc {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH264DisableDeblockingFilterIdc(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH264DisableDeblockingFilterIdc {
        pub const eDisabled: Self = RawStdVideoH264DisableDeblockingFilterIdc(0);
        pub const eEnabled: Self = RawStdVideoH264DisableDeblockingFilterIdc(1);
        pub const ePartial: Self = RawStdVideoH264DisableDeblockingFilterIdc(2);
        pub const eInvalid: Self = RawStdVideoH264DisableDeblockingFilterIdc(2147483647);

        pub fn normalise(self) -> StdVideoH264DisableDeblockingFilterIdc {
            match self {
                Self::eDisabled => StdVideoH264DisableDeblockingFilterIdc::eDisabled,
                Self::eEnabled => StdVideoH264DisableDeblockingFilterIdc::eEnabled,
                Self::ePartial => StdVideoH264DisableDeblockingFilterIdc::ePartial,
                Self::eInvalid => StdVideoH264DisableDeblockingFilterIdc::eInvalid,
                v => StdVideoH264DisableDeblockingFilterIdc::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH264DisableDeblockingFilterIdc {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH264SliceType(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH264SliceType {
        pub const eP: Self = RawStdVideoH264SliceType(0);
        pub const eB: Self = RawStdVideoH264SliceType(1);
        pub const eI: Self = RawStdVideoH264SliceType(2);
        pub const eInvalid: Self = RawStdVideoH264SliceType(2147483647);

        pub fn normalise(self) -> StdVideoH264SliceType {
            match self {
                Self::eP => StdVideoH264SliceType::eP,
                Self::eB => StdVideoH264SliceType::eB,
                Self::eI => StdVideoH264SliceType::eI,
                Self::eInvalid => StdVideoH264SliceType::eInvalid,
                v => StdVideoH264SliceType::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH264SliceType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH264PictureType(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH264PictureType {
        pub const eP: Self = RawStdVideoH264PictureType(0);
        pub const eB: Self = RawStdVideoH264PictureType(1);
        pub const eI: Self = RawStdVideoH264PictureType(2);
        pub const eIdr: Self = RawStdVideoH264PictureType(5);
        pub const eInvalid: Self = RawStdVideoH264PictureType(2147483647);

        pub fn normalise(self) -> StdVideoH264PictureType {
            match self {
                Self::eP => StdVideoH264PictureType::eP,
                Self::eB => StdVideoH264PictureType::eB,
                Self::eI => StdVideoH264PictureType::eI,
                Self::eIdr => StdVideoH264PictureType::eIdr,
                Self::eInvalid => StdVideoH264PictureType::eInvalid,
                v => StdVideoH264PictureType::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH264PictureType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH264NonVclNaluType(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH264NonVclNaluType {
        pub const eSps: Self = RawStdVideoH264NonVclNaluType(0);
        pub const ePps: Self = RawStdVideoH264NonVclNaluType(1);
        pub const eAud: Self = RawStdVideoH264NonVclNaluType(2);
        pub const ePrefix: Self = RawStdVideoH264NonVclNaluType(3);
        pub const eEndOfSequence: Self = RawStdVideoH264NonVclNaluType(4);
        pub const eEndOfStream: Self = RawStdVideoH264NonVclNaluType(5);
        pub const ePrecoded: Self = RawStdVideoH264NonVclNaluType(6);
        pub const eInvalid: Self = RawStdVideoH264NonVclNaluType(2147483647);

        pub fn normalise(self) -> StdVideoH264NonVclNaluType {
            match self {
                Self::eSps => StdVideoH264NonVclNaluType::eSps,
                Self::ePps => StdVideoH264NonVclNaluType::ePps,
                Self::eAud => StdVideoH264NonVclNaluType::eAud,
                Self::ePrefix => StdVideoH264NonVclNaluType::ePrefix,
                Self::eEndOfSequence => StdVideoH264NonVclNaluType::eEndOfSequence,
                Self::eEndOfStream => StdVideoH264NonVclNaluType::eEndOfStream,
                Self::ePrecoded => StdVideoH264NonVclNaluType::ePrecoded,
                Self::eInvalid => StdVideoH264NonVclNaluType::eInvalid,
                v => StdVideoH264NonVclNaluType::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH264NonVclNaluType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoDecodeH264FieldOrderCount(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoDecodeH264FieldOrderCount {
        pub const eTop: Self = RawStdVideoDecodeH264FieldOrderCount(0);
        pub const eBottom: Self = RawStdVideoDecodeH264FieldOrderCount(1);
        pub const eInvalid: Self = RawStdVideoDecodeH264FieldOrderCount(2147483647);

        pub fn normalise(self) -> StdVideoDecodeH264FieldOrderCount {
            match self {
                Self::eTop => StdVideoDecodeH264FieldOrderCount::eTop,
                Self::eBottom => StdVideoDecodeH264FieldOrderCount::eBottom,
                Self::eInvalid => StdVideoDecodeH264FieldOrderCount::eInvalid,
                v => StdVideoDecodeH264FieldOrderCount::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoDecodeH264FieldOrderCount {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH265ChromaFormatIdc(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH265ChromaFormatIdc {
        pub const eMonochrome: Self = RawStdVideoH265ChromaFormatIdc(0);
        pub const e420: Self = RawStdVideoH265ChromaFormatIdc(1);
        pub const e422: Self = RawStdVideoH265ChromaFormatIdc(2);
        pub const e444: Self = RawStdVideoH265ChromaFormatIdc(3);
        pub const eInvalid: Self = RawStdVideoH265ChromaFormatIdc(2147483647);

        pub fn normalise(self) -> StdVideoH265ChromaFormatIdc {
            match self {
                Self::eMonochrome => StdVideoH265ChromaFormatIdc::eMonochrome,
                Self::e420 => StdVideoH265ChromaFormatIdc::e420,
                Self::e422 => StdVideoH265ChromaFormatIdc::e422,
                Self::e444 => StdVideoH265ChromaFormatIdc::e444,
                Self::eInvalid => StdVideoH265ChromaFormatIdc::eInvalid,
                v => StdVideoH265ChromaFormatIdc::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH265ChromaFormatIdc {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH265ProfileIdc(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH265ProfileIdc {
        pub const eMain: Self = RawStdVideoH265ProfileIdc(1);
        pub const eMain10: Self = RawStdVideoH265ProfileIdc(2);
        pub const eMainStillPicture: Self = RawStdVideoH265ProfileIdc(3);
        pub const eFormatRangeExtensions: Self = RawStdVideoH265ProfileIdc(4);
        pub const eSccExtensions: Self = RawStdVideoH265ProfileIdc(9);
        pub const eInvalid: Self = RawStdVideoH265ProfileIdc(2147483647);

        pub fn normalise(self) -> StdVideoH265ProfileIdc {
            match self {
                Self::eMain => StdVideoH265ProfileIdc::eMain,
                Self::eMain10 => StdVideoH265ProfileIdc::eMain10,
                Self::eMainStillPicture => StdVideoH265ProfileIdc::eMainStillPicture,
                Self::eFormatRangeExtensions => StdVideoH265ProfileIdc::eFormatRangeExtensions,
                Self::eSccExtensions => StdVideoH265ProfileIdc::eSccExtensions,
                Self::eInvalid => StdVideoH265ProfileIdc::eInvalid,
                v => StdVideoH265ProfileIdc::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH265ProfileIdc {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH265LevelIdc(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH265LevelIdc {
        pub const e10: Self = RawStdVideoH265LevelIdc(0);
        pub const e20: Self = RawStdVideoH265LevelIdc(1);
        pub const e21: Self = RawStdVideoH265LevelIdc(2);
        pub const e30: Self = RawStdVideoH265LevelIdc(3);
        pub const e31: Self = RawStdVideoH265LevelIdc(4);
        pub const e40: Self = RawStdVideoH265LevelIdc(5);
        pub const e41: Self = RawStdVideoH265LevelIdc(6);
        pub const e50: Self = RawStdVideoH265LevelIdc(7);
        pub const e51: Self = RawStdVideoH265LevelIdc(8);
        pub const e52: Self = RawStdVideoH265LevelIdc(9);
        pub const e60: Self = RawStdVideoH265LevelIdc(10);
        pub const e61: Self = RawStdVideoH265LevelIdc(11);
        pub const e62: Self = RawStdVideoH265LevelIdc(12);
        pub const eInvalid: Self = RawStdVideoH265LevelIdc(2147483647);

        pub fn normalise(self) -> StdVideoH265LevelIdc {
            match self {
                Self::e10 => StdVideoH265LevelIdc::e10,
                Self::e20 => StdVideoH265LevelIdc::e20,
                Self::e21 => StdVideoH265LevelIdc::e21,
                Self::e30 => StdVideoH265LevelIdc::e30,
                Self::e31 => StdVideoH265LevelIdc::e31,
                Self::e40 => StdVideoH265LevelIdc::e40,
                Self::e41 => StdVideoH265LevelIdc::e41,
                Self::e50 => StdVideoH265LevelIdc::e50,
                Self::e51 => StdVideoH265LevelIdc::e51,
                Self::e52 => StdVideoH265LevelIdc::e52,
                Self::e60 => StdVideoH265LevelIdc::e60,
                Self::e61 => StdVideoH265LevelIdc::e61,
                Self::e62 => StdVideoH265LevelIdc::e62,
                Self::eInvalid => StdVideoH265LevelIdc::eInvalid,
                v => StdVideoH265LevelIdc::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH265LevelIdc {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH265SliceType(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH265SliceType {
        pub const eB: Self = RawStdVideoH265SliceType(0);
        pub const eP: Self = RawStdVideoH265SliceType(1);
        pub const eI: Self = RawStdVideoH265SliceType(2);
        pub const eInvalid: Self = RawStdVideoH265SliceType(2147483647);

        pub fn normalise(self) -> StdVideoH265SliceType {
            match self {
                Self::eB => StdVideoH265SliceType::eB,
                Self::eP => StdVideoH265SliceType::eP,
                Self::eI => StdVideoH265SliceType::eI,
                Self::eInvalid => StdVideoH265SliceType::eInvalid,
                v => StdVideoH265SliceType::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH265SliceType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH265PictureType(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH265PictureType {
        pub const eP: Self = RawStdVideoH265PictureType(0);
        pub const eB: Self = RawStdVideoH265PictureType(1);
        pub const eI: Self = RawStdVideoH265PictureType(2);
        pub const eIdr: Self = RawStdVideoH265PictureType(3);
        pub const eInvalid: Self = RawStdVideoH265PictureType(2147483647);

        pub fn normalise(self) -> StdVideoH265PictureType {
            match self {
                Self::eP => StdVideoH265PictureType::eP,
                Self::eB => StdVideoH265PictureType::eB,
                Self::eI => StdVideoH265PictureType::eI,
                Self::eIdr => StdVideoH265PictureType::eIdr,
                Self::eInvalid => StdVideoH265PictureType::eInvalid,
                v => StdVideoH265PictureType::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH265PictureType {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawStdVideoH265AspectRatioIdc(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawStdVideoH265AspectRatioIdc {
        pub const eUnspecified: Self = RawStdVideoH265AspectRatioIdc(0);
        pub const eSquare: Self = RawStdVideoH265AspectRatioIdc(1);
        pub const e1211: Self = RawStdVideoH265AspectRatioIdc(2);
        pub const e1011: Self = RawStdVideoH265AspectRatioIdc(3);
        pub const e1611: Self = RawStdVideoH265AspectRatioIdc(4);
        pub const e4033: Self = RawStdVideoH265AspectRatioIdc(5);
        pub const e2411: Self = RawStdVideoH265AspectRatioIdc(6);
        pub const e2011: Self = RawStdVideoH265AspectRatioIdc(7);
        pub const e3211: Self = RawStdVideoH265AspectRatioIdc(8);
        pub const e8033: Self = RawStdVideoH265AspectRatioIdc(9);
        pub const e1811: Self = RawStdVideoH265AspectRatioIdc(10);
        pub const e1511: Self = RawStdVideoH265AspectRatioIdc(11);
        pub const e6433: Self = RawStdVideoH265AspectRatioIdc(12);
        pub const e16099: Self = RawStdVideoH265AspectRatioIdc(13);
        pub const e43: Self = RawStdVideoH265AspectRatioIdc(14);
        pub const e32: Self = RawStdVideoH265AspectRatioIdc(15);
        pub const e21: Self = RawStdVideoH265AspectRatioIdc(16);
        pub const eExtendedSar: Self = RawStdVideoH265AspectRatioIdc(255);
        pub const eInvalid: Self = RawStdVideoH265AspectRatioIdc(2147483647);

        pub fn normalise(self) -> StdVideoH265AspectRatioIdc {
            match self {
                Self::eUnspecified => StdVideoH265AspectRatioIdc::eUnspecified,
                Self::eSquare => StdVideoH265AspectRatioIdc::eSquare,
                Self::e1211 => StdVideoH265AspectRatioIdc::e1211,
                Self::e1011 => StdVideoH265AspectRatioIdc::e1011,
                Self::e1611 => StdVideoH265AspectRatioIdc::e1611,
                Self::e4033 => StdVideoH265AspectRatioIdc::e4033,
                Self::e2411 => StdVideoH265AspectRatioIdc::e2411,
                Self::e2011 => StdVideoH265AspectRatioIdc::e2011,
                Self::e3211 => StdVideoH265AspectRatioIdc::e3211,
                Self::e8033 => StdVideoH265AspectRatioIdc::e8033,
                Self::e1811 => StdVideoH265AspectRatioIdc::e1811,
                Self::e1511 => StdVideoH265AspectRatioIdc::e1511,
                Self::e6433 => StdVideoH265AspectRatioIdc::e6433,
                Self::e16099 => StdVideoH265AspectRatioIdc::e16099,
                Self::e43 => StdVideoH265AspectRatioIdc::e43,
                Self::e32 => StdVideoH265AspectRatioIdc::e32,
                Self::e21 => StdVideoH265AspectRatioIdc::e21,
                Self::eExtendedSar => StdVideoH265AspectRatioIdc::eExtendedSar,
                Self::eInvalid => StdVideoH265AspectRatioIdc::eInvalid,
                v => StdVideoH265AspectRatioIdc::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawStdVideoH265AspectRatioIdc {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawSuccessCode(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawSuccessCode {
        pub const eSuccess: Self = RawSuccessCode(0);
        pub const eNotReady: Self = RawSuccessCode(1);
        pub const eTimeout: Self = RawSuccessCode(2);
        pub const eEventSet: Self = RawSuccessCode(3);
        pub const eEventReset: Self = RawSuccessCode(4);
        pub const eIncomplete: Self = RawSuccessCode(5);
        pub const eSuboptimalKhr: Self = RawSuccessCode(1000001003);
        pub const eThreadIdleKhr: Self = RawSuccessCode(1000268000);
        pub const eThreadDoneKhr: Self = RawSuccessCode(1000268001);
        pub const eOperationDeferredKhr: Self = RawSuccessCode(1000268002);
        pub const eOperationNotDeferredKhr: Self = RawSuccessCode(1000268003);
        pub const ePipelineCompileRequired: Self = RawSuccessCode(1000297000);

        pub fn normalise(self) -> SuccessCode {
            match self {
                Self::eSuccess => SuccessCode::eSuccess,
                Self::eNotReady => SuccessCode::eNotReady,
                Self::eTimeout => SuccessCode::eTimeout,
                Self::eEventSet => SuccessCode::eEventSet,
                Self::eEventReset => SuccessCode::eEventReset,
                Self::eIncomplete => SuccessCode::eIncomplete,
                Self::eSuboptimalKhr => SuccessCode::eSuboptimalKhr,
                Self::eThreadIdleKhr => SuccessCode::eThreadIdleKhr,
                Self::eThreadDoneKhr => SuccessCode::eThreadDoneKhr,
                Self::eOperationDeferredKhr => SuccessCode::eOperationDeferredKhr,
                Self::eOperationNotDeferredKhr => SuccessCode::eOperationNotDeferredKhr,
                Self::ePipelineCompileRequired => SuccessCode::ePipelineCompileRequired,
                v => SuccessCode::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawSuccessCode {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
    pub struct RawErrorCode(pub i32);

    #[allow(non_upper_case_globals)]
    impl RawErrorCode {
        pub const eErrorCompressionExhaustedExt: Self = RawErrorCode(-1000338000);
        pub const eErrorNoPipelineMatch: Self = RawErrorCode(-1000298001);
        pub const eErrorInvalidPipelineCacheData: Self = RawErrorCode(-1000298000);
        pub const eErrorInvalidOpaqueCaptureAddress: Self = RawErrorCode(-1000257000);
        pub const eErrorFullScreenExclusiveModeLostExt: Self = RawErrorCode(-1000255000);
        pub const eErrorNotPermittedKhr: Self = RawErrorCode(-1000174001);
        pub const eErrorFragmentation: Self = RawErrorCode(-1000161000);
        pub const eErrorInvalidDrmFormatModifierPlaneLayoutExt: Self = RawErrorCode(-1000158000);
        pub const eErrorInvalidExternalHandle: Self = RawErrorCode(-1000072003);
        pub const eErrorOutOfPoolMemory: Self = RawErrorCode(-1000069000);
        pub const eErrorVideoStdVersionNotSupportedKhr: Self = RawErrorCode(-1000023005);
        pub const eErrorVideoProfileCodecNotSupportedKhr: Self = RawErrorCode(-1000023004);
        pub const eErrorVideoProfileFormatNotSupportedKhr: Self = RawErrorCode(-1000023003);
        pub const eErrorVideoProfileOperationNotSupportedKhr: Self = RawErrorCode(-1000023002);
        pub const eErrorVideoPictureLayoutNotSupportedKhr: Self = RawErrorCode(-1000023001);
        pub const eErrorImageUsageNotSupportedKhr: Self = RawErrorCode(-1000023000);
        pub const eErrorInvalidShaderNv: Self = RawErrorCode(-1000012000);
        pub const eErrorValidationFailedExt: Self = RawErrorCode(-1000011001);
        pub const eErrorIncompatibleDisplayKhr: Self = RawErrorCode(-1000003001);
        pub const eErrorOutOfDateKhr: Self = RawErrorCode(-1000001004);
        pub const eErrorNativeWindowInUseKhr: Self = RawErrorCode(-1000000001);
        pub const eErrorSurfaceLostKhr: Self = RawErrorCode(-1000000000);
        pub const eErrorUnknown: Self = RawErrorCode(-13);
        pub const eErrorFragmentedPool: Self = RawErrorCode(-12);
        pub const eErrorFormatNotSupported: Self = RawErrorCode(-11);
        pub const eErrorTooManyObjects: Self = RawErrorCode(-10);
        pub const eErrorIncompatibleDriver: Self = RawErrorCode(-9);
        pub const eErrorFeatureNotPresent: Self = RawErrorCode(-8);
        pub const eErrorExtensionNotPresent: Self = RawErrorCode(-7);
        pub const eErrorLayerNotPresent: Self = RawErrorCode(-6);
        pub const eErrorMemoryMapFailed: Self = RawErrorCode(-5);
        pub const eErrorDeviceLost: Self = RawErrorCode(-4);
        pub const eErrorInitializationFailed: Self = RawErrorCode(-3);
        pub const eErrorOutOfDeviceMemory: Self = RawErrorCode(-2);
        pub const eErrorOutOfHostMemory: Self = RawErrorCode(-1);

        pub fn normalise(self) -> ErrorCode {
            match self {
                Self::eErrorCompressionExhaustedExt => ErrorCode::eErrorCompressionExhaustedExt,
                Self::eErrorNoPipelineMatch => ErrorCode::eErrorNoPipelineMatch,
                Self::eErrorInvalidPipelineCacheData => ErrorCode::eErrorInvalidPipelineCacheData,
                Self::eErrorInvalidOpaqueCaptureAddress => ErrorCode::eErrorInvalidOpaqueCaptureAddress,
                Self::eErrorFullScreenExclusiveModeLostExt => ErrorCode::eErrorFullScreenExclusiveModeLostExt,
                Self::eErrorNotPermittedKhr => ErrorCode::eErrorNotPermittedKhr,
                Self::eErrorFragmentation => ErrorCode::eErrorFragmentation,
                Self::eErrorInvalidDrmFormatModifierPlaneLayoutExt => ErrorCode::eErrorInvalidDrmFormatModifierPlaneLayoutExt,
                Self::eErrorInvalidExternalHandle => ErrorCode::eErrorInvalidExternalHandle,
                Self::eErrorOutOfPoolMemory => ErrorCode::eErrorOutOfPoolMemory,
                Self::eErrorVideoStdVersionNotSupportedKhr => ErrorCode::eErrorVideoStdVersionNotSupportedKhr,
                Self::eErrorVideoProfileCodecNotSupportedKhr => ErrorCode::eErrorVideoProfileCodecNotSupportedKhr,
                Self::eErrorVideoProfileFormatNotSupportedKhr => ErrorCode::eErrorVideoProfileFormatNotSupportedKhr,
                Self::eErrorVideoProfileOperationNotSupportedKhr => ErrorCode::eErrorVideoProfileOperationNotSupportedKhr,
                Self::eErrorVideoPictureLayoutNotSupportedKhr => ErrorCode::eErrorVideoPictureLayoutNotSupportedKhr,
                Self::eErrorImageUsageNotSupportedKhr => ErrorCode::eErrorImageUsageNotSupportedKhr,
                Self::eErrorInvalidShaderNv => ErrorCode::eErrorInvalidShaderNv,
                Self::eErrorValidationFailedExt => ErrorCode::eErrorValidationFailedExt,
                Self::eErrorIncompatibleDisplayKhr => ErrorCode::eErrorIncompatibleDisplayKhr,
                Self::eErrorOutOfDateKhr => ErrorCode::eErrorOutOfDateKhr,
                Self::eErrorNativeWindowInUseKhr => ErrorCode::eErrorNativeWindowInUseKhr,
                Self::eErrorSurfaceLostKhr => ErrorCode::eErrorSurfaceLostKhr,
                Self::eErrorUnknown => ErrorCode::eErrorUnknown,
                Self::eErrorFragmentedPool => ErrorCode::eErrorFragmentedPool,
                Self::eErrorFormatNotSupported => ErrorCode::eErrorFormatNotSupported,
                Self::eErrorTooManyObjects => ErrorCode::eErrorTooManyObjects,
                Self::eErrorIncompatibleDriver => ErrorCode::eErrorIncompatibleDriver,
                Self::eErrorFeatureNotPresent => ErrorCode::eErrorFeatureNotPresent,
                Self::eErrorExtensionNotPresent => ErrorCode::eErrorExtensionNotPresent,
                Self::eErrorLayerNotPresent => ErrorCode::eErrorLayerNotPresent,
                Self::eErrorMemoryMapFailed => ErrorCode::eErrorMemoryMapFailed,
                Self::eErrorDeviceLost => ErrorCode::eErrorDeviceLost,
                Self::eErrorInitializationFailed => ErrorCode::eErrorInitializationFailed,
                Self::eErrorOutOfDeviceMemory => ErrorCode::eErrorOutOfDeviceMemory,
                Self::eErrorOutOfHostMemory => ErrorCode::eErrorOutOfHostMemory,
                v => ErrorCode::eUnknownVariant(v.0),
            }
        }
    }

    impl core::fmt::Debug for RawErrorCode {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }

}

