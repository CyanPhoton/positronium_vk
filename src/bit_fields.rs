pub use super::*;
// Bit fields:
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum FramebufferCreateFlagBits {
    eImagelessBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl FramebufferCreateFlagBits {
    pub const eImagelessBitKhr: Self = Self::eImagelessBit;

    pub fn into_raw(self) -> RawFramebufferCreateFlagBits {
        match self {
            Self::eImagelessBit => RawFramebufferCreateFlagBits::eImagelessBit,
            Self::eUnknownBit(b) => RawFramebufferCreateFlagBits(b),
        }
    }
}

impl core::convert::From<FramebufferCreateFlagBits> for FramebufferCreateFlags {
    fn from(value: FramebufferCreateFlagBits) -> Self {
        match value {
            FramebufferCreateFlagBits::eImagelessBit => FramebufferCreateFlags::eImagelessBit,
            FramebufferCreateFlagBits::eUnknownBit(b) => FramebufferCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(FramebufferCreateFlagBits, FramebufferCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct FramebufferCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl FramebufferCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eImagelessBit: Self = Self(0b1);
    pub const eImagelessBitKhr: Self = Self::eImagelessBit;
}

bit_field_mask_derives!(FramebufferCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(FramebufferCreateFlagBits, FramebufferCreateFlags, u32, FramebufferCreateFlagBits::eImagelessBit);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct QueryPoolCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl QueryPoolCreateFlags {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(QueryPoolCreateFlags, u32);

fieldless_debug_derive!(QueryPoolCreateFlags);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum RenderPassCreateFlagBits {
    eTransformBitQcom,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl RenderPassCreateFlagBits {
    pub fn into_raw(self) -> RawRenderPassCreateFlagBits {
        match self {
            Self::eTransformBitQcom => RawRenderPassCreateFlagBits::eTransformBitQcom,
            Self::eUnknownBit(b) => RawRenderPassCreateFlagBits(b),
        }
    }
}

impl core::convert::From<RenderPassCreateFlagBits> for RenderPassCreateFlags {
    fn from(value: RenderPassCreateFlagBits) -> Self {
        match value {
            RenderPassCreateFlagBits::eTransformBitQcom => RenderPassCreateFlags::eTransformBitQcom,
            RenderPassCreateFlagBits::eUnknownBit(b) => RenderPassCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(RenderPassCreateFlagBits, RenderPassCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct RenderPassCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl RenderPassCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eTransformBitQcom: Self = Self(0b10);
}

bit_field_mask_derives!(RenderPassCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(RenderPassCreateFlagBits, RenderPassCreateFlags, u32, RenderPassCreateFlagBits::eTransformBitQcom);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SamplerCreateFlagBits {
    eSubsampledBitExt,
    eSubsampledCoarseReconstructionBitExt,
    eNonSeamlessCubeMapBitExt,
    eDescriptorBufferCaptureReplayBitExt,
    eImageProcessingBitQcom,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl SamplerCreateFlagBits {
    pub fn into_raw(self) -> RawSamplerCreateFlagBits {
        match self {
            Self::eSubsampledBitExt => RawSamplerCreateFlagBits::eSubsampledBitExt,
            Self::eSubsampledCoarseReconstructionBitExt => RawSamplerCreateFlagBits::eSubsampledCoarseReconstructionBitExt,
            Self::eNonSeamlessCubeMapBitExt => RawSamplerCreateFlagBits::eNonSeamlessCubeMapBitExt,
            Self::eDescriptorBufferCaptureReplayBitExt => RawSamplerCreateFlagBits::eDescriptorBufferCaptureReplayBitExt,
            Self::eImageProcessingBitQcom => RawSamplerCreateFlagBits::eImageProcessingBitQcom,
            Self::eUnknownBit(b) => RawSamplerCreateFlagBits(b),
        }
    }
}

impl core::convert::From<SamplerCreateFlagBits> for SamplerCreateFlags {
    fn from(value: SamplerCreateFlagBits) -> Self {
        match value {
            SamplerCreateFlagBits::eSubsampledBitExt => SamplerCreateFlags::eSubsampledBitExt,
            SamplerCreateFlagBits::eSubsampledCoarseReconstructionBitExt => SamplerCreateFlags::eSubsampledCoarseReconstructionBitExt,
            SamplerCreateFlagBits::eNonSeamlessCubeMapBitExt => SamplerCreateFlags::eNonSeamlessCubeMapBitExt,
            SamplerCreateFlagBits::eDescriptorBufferCaptureReplayBitExt => SamplerCreateFlags::eDescriptorBufferCaptureReplayBitExt,
            SamplerCreateFlagBits::eImageProcessingBitQcom => SamplerCreateFlags::eImageProcessingBitQcom,
            SamplerCreateFlagBits::eUnknownBit(b) => SamplerCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(SamplerCreateFlagBits, SamplerCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct SamplerCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl SamplerCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eSubsampledBitExt: Self = Self(0b1);
    pub const eSubsampledCoarseReconstructionBitExt: Self = Self(0b10);
    pub const eNonSeamlessCubeMapBitExt: Self = Self(0b100);
    pub const eDescriptorBufferCaptureReplayBitExt: Self = Self(0b1000);
    pub const eImageProcessingBitQcom: Self = Self(0b10000);
}

bit_field_mask_derives!(SamplerCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(SamplerCreateFlagBits, SamplerCreateFlags, u32, SamplerCreateFlagBits::eSubsampledBitExt, SamplerCreateFlagBits::eSubsampledCoarseReconstructionBitExt, SamplerCreateFlagBits::eNonSeamlessCubeMapBitExt, SamplerCreateFlagBits::eDescriptorBufferCaptureReplayBitExt, SamplerCreateFlagBits::eImageProcessingBitQcom);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PipelineLayoutCreateFlagBits {
    eIndependentSetsBitExt,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl PipelineLayoutCreateFlagBits {
    pub fn into_raw(self) -> RawPipelineLayoutCreateFlagBits {
        match self {
            Self::eIndependentSetsBitExt => RawPipelineLayoutCreateFlagBits::eIndependentSetsBitExt,
            Self::eUnknownBit(b) => RawPipelineLayoutCreateFlagBits(b),
        }
    }
}

impl core::convert::From<PipelineLayoutCreateFlagBits> for PipelineLayoutCreateFlags {
    fn from(value: PipelineLayoutCreateFlagBits) -> Self {
        match value {
            PipelineLayoutCreateFlagBits::eIndependentSetsBitExt => PipelineLayoutCreateFlags::eIndependentSetsBitExt,
            PipelineLayoutCreateFlagBits::eUnknownBit(b) => PipelineLayoutCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(PipelineLayoutCreateFlagBits, PipelineLayoutCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineLayoutCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineLayoutCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eIndependentSetsBitExt: Self = Self(0b10);
}

bit_field_mask_derives!(PipelineLayoutCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(PipelineLayoutCreateFlagBits, PipelineLayoutCreateFlags, u32, PipelineLayoutCreateFlagBits::eIndependentSetsBitExt);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PipelineCacheCreateFlagBits {
    eExternallySynchronizedBit,
    eUseApplicationStorageBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl PipelineCacheCreateFlagBits {
    pub const eExternallySynchronizedBitExt: Self = Self::eExternallySynchronizedBit;

    pub fn into_raw(self) -> RawPipelineCacheCreateFlagBits {
        match self {
            Self::eExternallySynchronizedBit => RawPipelineCacheCreateFlagBits::eExternallySynchronizedBit,
            Self::eUseApplicationStorageBit => RawPipelineCacheCreateFlagBits::eUseApplicationStorageBit,
            Self::eUnknownBit(b) => RawPipelineCacheCreateFlagBits(b),
        }
    }
}

impl core::convert::From<PipelineCacheCreateFlagBits> for PipelineCacheCreateFlags {
    fn from(value: PipelineCacheCreateFlagBits) -> Self {
        match value {
            PipelineCacheCreateFlagBits::eExternallySynchronizedBit => PipelineCacheCreateFlags::eExternallySynchronizedBit,
            PipelineCacheCreateFlagBits::eUseApplicationStorageBit => PipelineCacheCreateFlags::eUseApplicationStorageBit,
            PipelineCacheCreateFlagBits::eUnknownBit(b) => PipelineCacheCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(PipelineCacheCreateFlagBits, PipelineCacheCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineCacheCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineCacheCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eExternallySynchronizedBit: Self = Self(0b1);
    pub const eUseApplicationStorageBit: Self = Self(0b100);
    pub const eExternallySynchronizedBitExt: Self = Self::eExternallySynchronizedBit;
}

bit_field_mask_derives!(PipelineCacheCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(PipelineCacheCreateFlagBits, PipelineCacheCreateFlags, u32, PipelineCacheCreateFlagBits::eExternallySynchronizedBit, PipelineCacheCreateFlagBits::eUseApplicationStorageBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PipelineDepthStencilStateCreateFlagBits {
    eRasterizationOrderAttachmentDepthAccessBitExt,
    eRasterizationOrderAttachmentStencilAccessBitExt,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl PipelineDepthStencilStateCreateFlagBits {
    pub fn into_raw(self) -> RawPipelineDepthStencilStateCreateFlagBits {
        match self {
            Self::eRasterizationOrderAttachmentDepthAccessBitExt => RawPipelineDepthStencilStateCreateFlagBits::eRasterizationOrderAttachmentDepthAccessBitExt,
            Self::eRasterizationOrderAttachmentStencilAccessBitExt => RawPipelineDepthStencilStateCreateFlagBits::eRasterizationOrderAttachmentStencilAccessBitExt,
            Self::eUnknownBit(b) => RawPipelineDepthStencilStateCreateFlagBits(b),
        }
    }
}

impl core::convert::From<PipelineDepthStencilStateCreateFlagBits> for PipelineDepthStencilStateCreateFlags {
    fn from(value: PipelineDepthStencilStateCreateFlagBits) -> Self {
        match value {
            PipelineDepthStencilStateCreateFlagBits::eRasterizationOrderAttachmentDepthAccessBitExt => PipelineDepthStencilStateCreateFlags::eRasterizationOrderAttachmentDepthAccessBitExt,
            PipelineDepthStencilStateCreateFlagBits::eRasterizationOrderAttachmentStencilAccessBitExt => PipelineDepthStencilStateCreateFlags::eRasterizationOrderAttachmentStencilAccessBitExt,
            PipelineDepthStencilStateCreateFlagBits::eUnknownBit(b) => PipelineDepthStencilStateCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(PipelineDepthStencilStateCreateFlagBits, PipelineDepthStencilStateCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineDepthStencilStateCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineDepthStencilStateCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eRasterizationOrderAttachmentDepthAccessBitExt: Self = Self(0b1);
    pub const eRasterizationOrderAttachmentStencilAccessBitExt: Self = Self(0b10);
}

bit_field_mask_derives!(PipelineDepthStencilStateCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(PipelineDepthStencilStateCreateFlagBits, PipelineDepthStencilStateCreateFlags, u32, PipelineDepthStencilStateCreateFlagBits::eRasterizationOrderAttachmentDepthAccessBitExt, PipelineDepthStencilStateCreateFlagBits::eRasterizationOrderAttachmentStencilAccessBitExt);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineDynamicStateCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineDynamicStateCreateFlags {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(PipelineDynamicStateCreateFlags, u32);

fieldless_debug_derive!(PipelineDynamicStateCreateFlags);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PipelineColourBlendStateCreateFlagBits {
    eRasterizationOrderAttachmentAccessBitExt,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl PipelineColourBlendStateCreateFlagBits {
    pub fn into_raw(self) -> RawPipelineColourBlendStateCreateFlagBits {
        match self {
            Self::eRasterizationOrderAttachmentAccessBitExt => RawPipelineColourBlendStateCreateFlagBits::eRasterizationOrderAttachmentAccessBitExt,
            Self::eUnknownBit(b) => RawPipelineColourBlendStateCreateFlagBits(b),
        }
    }
}

impl core::convert::From<PipelineColourBlendStateCreateFlagBits> for PipelineColourBlendStateCreateFlags {
    fn from(value: PipelineColourBlendStateCreateFlagBits) -> Self {
        match value {
            PipelineColourBlendStateCreateFlagBits::eRasterizationOrderAttachmentAccessBitExt => PipelineColourBlendStateCreateFlags::eRasterizationOrderAttachmentAccessBitExt,
            PipelineColourBlendStateCreateFlagBits::eUnknownBit(b) => PipelineColourBlendStateCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(PipelineColourBlendStateCreateFlagBits, PipelineColourBlendStateCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineColourBlendStateCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineColourBlendStateCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eRasterizationOrderAttachmentAccessBitExt: Self = Self(0b1);
}

bit_field_mask_derives!(PipelineColourBlendStateCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(PipelineColourBlendStateCreateFlagBits, PipelineColourBlendStateCreateFlags, u32, PipelineColourBlendStateCreateFlagBits::eRasterizationOrderAttachmentAccessBitExt);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineMultisampleStateCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineMultisampleStateCreateFlags {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(PipelineMultisampleStateCreateFlags, u32);

fieldless_debug_derive!(PipelineMultisampleStateCreateFlags);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineRasterizationStateCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineRasterizationStateCreateFlags {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(PipelineRasterizationStateCreateFlags, u32);

fieldless_debug_derive!(PipelineRasterizationStateCreateFlags);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineViewportStateCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineViewportStateCreateFlags {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(PipelineViewportStateCreateFlags, u32);

fieldless_debug_derive!(PipelineViewportStateCreateFlags);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineTessellationStateCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineTessellationStateCreateFlags {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(PipelineTessellationStateCreateFlags, u32);

fieldless_debug_derive!(PipelineTessellationStateCreateFlags);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineInputAssemblyStateCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineInputAssemblyStateCreateFlags {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(PipelineInputAssemblyStateCreateFlags, u32);

fieldless_debug_derive!(PipelineInputAssemblyStateCreateFlags);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineVertexInputStateCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineVertexInputStateCreateFlags {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(PipelineVertexInputStateCreateFlags, u32);

fieldless_debug_derive!(PipelineVertexInputStateCreateFlags);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PipelineShaderStageCreateFlagBits {
    eAllowVaryingSubgroupSizeBit,
    eRequireFullSubgroupsBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl PipelineShaderStageCreateFlagBits {
    pub const eAllowVaryingSubgroupSizeBitExt: Self = Self::eAllowVaryingSubgroupSizeBit;
    pub const eRequireFullSubgroupsBitExt: Self = Self::eRequireFullSubgroupsBit;

    pub fn into_raw(self) -> RawPipelineShaderStageCreateFlagBits {
        match self {
            Self::eAllowVaryingSubgroupSizeBit => RawPipelineShaderStageCreateFlagBits::eAllowVaryingSubgroupSizeBit,
            Self::eRequireFullSubgroupsBit => RawPipelineShaderStageCreateFlagBits::eRequireFullSubgroupsBit,
            Self::eUnknownBit(b) => RawPipelineShaderStageCreateFlagBits(b),
        }
    }
}

impl core::convert::From<PipelineShaderStageCreateFlagBits> for PipelineShaderStageCreateFlags {
    fn from(value: PipelineShaderStageCreateFlagBits) -> Self {
        match value {
            PipelineShaderStageCreateFlagBits::eAllowVaryingSubgroupSizeBit => PipelineShaderStageCreateFlags::eAllowVaryingSubgroupSizeBit,
            PipelineShaderStageCreateFlagBits::eRequireFullSubgroupsBit => PipelineShaderStageCreateFlags::eRequireFullSubgroupsBit,
            PipelineShaderStageCreateFlagBits::eUnknownBit(b) => PipelineShaderStageCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(PipelineShaderStageCreateFlagBits, PipelineShaderStageCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineShaderStageCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineShaderStageCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eAllowVaryingSubgroupSizeBit: Self = Self(0b1);
    pub const eRequireFullSubgroupsBit: Self = Self(0b10);
    pub const eAllowVaryingSubgroupSizeBitExt: Self = Self::eAllowVaryingSubgroupSizeBit;
    pub const eRequireFullSubgroupsBitExt: Self = Self::eRequireFullSubgroupsBit;
}

bit_field_mask_derives!(PipelineShaderStageCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(PipelineShaderStageCreateFlagBits, PipelineShaderStageCreateFlags, u32, PipelineShaderStageCreateFlagBits::eAllowVaryingSubgroupSizeBit, PipelineShaderStageCreateFlagBits::eRequireFullSubgroupsBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DescriptorSetLayoutCreateFlagBits {
    ePushDescriptorBitKhr,
    eUpdateAfterBindPoolBit,
    eHostOnlyPoolBitExt,
    eDescriptorBufferBitExt,
    eEmbeddedImmutableSamplersBitExt,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl DescriptorSetLayoutCreateFlagBits {
    pub const eUpdateAfterBindPoolBitExt: Self = Self::eUpdateAfterBindPoolBit;

    pub fn into_raw(self) -> RawDescriptorSetLayoutCreateFlagBits {
        match self {
            Self::ePushDescriptorBitKhr => RawDescriptorSetLayoutCreateFlagBits::ePushDescriptorBitKhr,
            Self::eUpdateAfterBindPoolBit => RawDescriptorSetLayoutCreateFlagBits::eUpdateAfterBindPoolBit,
            Self::eHostOnlyPoolBitExt => RawDescriptorSetLayoutCreateFlagBits::eHostOnlyPoolBitExt,
            Self::eDescriptorBufferBitExt => RawDescriptorSetLayoutCreateFlagBits::eDescriptorBufferBitExt,
            Self::eEmbeddedImmutableSamplersBitExt => RawDescriptorSetLayoutCreateFlagBits::eEmbeddedImmutableSamplersBitExt,
            Self::eUnknownBit(b) => RawDescriptorSetLayoutCreateFlagBits(b),
        }
    }
}

impl core::convert::From<DescriptorSetLayoutCreateFlagBits> for DescriptorSetLayoutCreateFlags {
    fn from(value: DescriptorSetLayoutCreateFlagBits) -> Self {
        match value {
            DescriptorSetLayoutCreateFlagBits::ePushDescriptorBitKhr => DescriptorSetLayoutCreateFlags::ePushDescriptorBitKhr,
            DescriptorSetLayoutCreateFlagBits::eUpdateAfterBindPoolBit => DescriptorSetLayoutCreateFlags::eUpdateAfterBindPoolBit,
            DescriptorSetLayoutCreateFlagBits::eHostOnlyPoolBitExt => DescriptorSetLayoutCreateFlags::eHostOnlyPoolBitExt,
            DescriptorSetLayoutCreateFlagBits::eDescriptorBufferBitExt => DescriptorSetLayoutCreateFlags::eDescriptorBufferBitExt,
            DescriptorSetLayoutCreateFlagBits::eEmbeddedImmutableSamplersBitExt => DescriptorSetLayoutCreateFlags::eEmbeddedImmutableSamplersBitExt,
            DescriptorSetLayoutCreateFlagBits::eUnknownBit(b) => DescriptorSetLayoutCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(DescriptorSetLayoutCreateFlagBits, DescriptorSetLayoutCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DescriptorSetLayoutCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DescriptorSetLayoutCreateFlags {
    pub const eNone: Self = Self(0);
    pub const ePushDescriptorBitKhr: Self = Self(0b1);
    pub const eUpdateAfterBindPoolBit: Self = Self(0b10);
    pub const eHostOnlyPoolBitExt: Self = Self(0b100);
    pub const eDescriptorBufferBitExt: Self = Self(0b10000);
    pub const eEmbeddedImmutableSamplersBitExt: Self = Self(0b100000);
    pub const eUpdateAfterBindPoolBitExt: Self = Self::eUpdateAfterBindPoolBit;
}

bit_field_mask_derives!(DescriptorSetLayoutCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(DescriptorSetLayoutCreateFlagBits, DescriptorSetLayoutCreateFlags, u32, DescriptorSetLayoutCreateFlagBits::ePushDescriptorBitKhr, DescriptorSetLayoutCreateFlagBits::eUpdateAfterBindPoolBit, DescriptorSetLayoutCreateFlagBits::eHostOnlyPoolBitExt, DescriptorSetLayoutCreateFlagBits::eDescriptorBufferBitExt, DescriptorSetLayoutCreateFlagBits::eEmbeddedImmutableSamplersBitExt);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct BufferViewCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl BufferViewCreateFlags {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(BufferViewCreateFlags, u32);

fieldless_debug_derive!(BufferViewCreateFlags);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum InstanceCreateFlagBits {
    eEnumeratePortabilityBitKhr,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl InstanceCreateFlagBits {
    pub fn into_raw(self) -> RawInstanceCreateFlagBits {
        match self {
            Self::eEnumeratePortabilityBitKhr => RawInstanceCreateFlagBits::eEnumeratePortabilityBitKhr,
            Self::eUnknownBit(b) => RawInstanceCreateFlagBits(b),
        }
    }
}

impl core::convert::From<InstanceCreateFlagBits> for InstanceCreateFlags {
    fn from(value: InstanceCreateFlagBits) -> Self {
        match value {
            InstanceCreateFlagBits::eEnumeratePortabilityBitKhr => InstanceCreateFlags::eEnumeratePortabilityBitKhr,
            InstanceCreateFlagBits::eUnknownBit(b) => InstanceCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(InstanceCreateFlagBits, InstanceCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct InstanceCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl InstanceCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eEnumeratePortabilityBitKhr: Self = Self(0b1);
}

bit_field_mask_derives!(InstanceCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(InstanceCreateFlagBits, InstanceCreateFlags, u32, InstanceCreateFlagBits::eEnumeratePortabilityBitKhr);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DeviceCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DeviceCreateFlags {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(DeviceCreateFlags, u32);

fieldless_debug_derive!(DeviceCreateFlags);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DeviceQueueCreateFlagBits {
    eProtectedBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl DeviceQueueCreateFlagBits {
    pub fn into_raw(self) -> RawDeviceQueueCreateFlagBits {
        match self {
            Self::eProtectedBit => RawDeviceQueueCreateFlagBits::eProtectedBit,
            Self::eUnknownBit(b) => RawDeviceQueueCreateFlagBits(b),
        }
    }
}

impl core::convert::From<DeviceQueueCreateFlagBits> for DeviceQueueCreateFlags {
    fn from(value: DeviceQueueCreateFlagBits) -> Self {
        match value {
            DeviceQueueCreateFlagBits::eProtectedBit => DeviceQueueCreateFlags::eProtectedBit,
            DeviceQueueCreateFlagBits::eUnknownBit(b) => DeviceQueueCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(DeviceQueueCreateFlagBits, DeviceQueueCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DeviceQueueCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DeviceQueueCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eProtectedBit: Self = Self(0b1);
}

bit_field_mask_derives!(DeviceQueueCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(DeviceQueueCreateFlagBits, DeviceQueueCreateFlags, u32, DeviceQueueCreateFlagBits::eProtectedBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum QueueFlagBits {
    eGraphicsBit,
    eComputeBit,
    eTransferBit,
    eSparseBindingBit,
    eProtectedBit,
    eVideoDecodeBitKhr,
    eVideoEncodeBitKhr,
    eOpticalFlowBitNv,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl QueueFlagBits {
    pub fn into_raw(self) -> RawQueueFlagBits {
        match self {
            Self::eGraphicsBit => RawQueueFlagBits::eGraphicsBit,
            Self::eComputeBit => RawQueueFlagBits::eComputeBit,
            Self::eTransferBit => RawQueueFlagBits::eTransferBit,
            Self::eSparseBindingBit => RawQueueFlagBits::eSparseBindingBit,
            Self::eProtectedBit => RawQueueFlagBits::eProtectedBit,
            Self::eVideoDecodeBitKhr => RawQueueFlagBits::eVideoDecodeBitKhr,
            Self::eVideoEncodeBitKhr => RawQueueFlagBits::eVideoEncodeBitKhr,
            Self::eOpticalFlowBitNv => RawQueueFlagBits::eOpticalFlowBitNv,
            Self::eUnknownBit(b) => RawQueueFlagBits(b),
        }
    }
}

impl core::convert::From<QueueFlagBits> for QueueFlags {
    fn from(value: QueueFlagBits) -> Self {
        match value {
            QueueFlagBits::eGraphicsBit => QueueFlags::eGraphicsBit,
            QueueFlagBits::eComputeBit => QueueFlags::eComputeBit,
            QueueFlagBits::eTransferBit => QueueFlags::eTransferBit,
            QueueFlagBits::eSparseBindingBit => QueueFlags::eSparseBindingBit,
            QueueFlagBits::eProtectedBit => QueueFlags::eProtectedBit,
            QueueFlagBits::eVideoDecodeBitKhr => QueueFlags::eVideoDecodeBitKhr,
            QueueFlagBits::eVideoEncodeBitKhr => QueueFlags::eVideoEncodeBitKhr,
            QueueFlagBits::eOpticalFlowBitNv => QueueFlags::eOpticalFlowBitNv,
            QueueFlagBits::eUnknownBit(b) => QueueFlags(b),
        }
    }
}

bit_field_enum_derives!(QueueFlagBits, QueueFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct QueueFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl QueueFlags {
    pub const eNone: Self = Self(0);
    pub const eGraphicsBit: Self = Self(0b1);
    pub const eComputeBit: Self = Self(0b10);
    pub const eTransferBit: Self = Self(0b100);
    pub const eSparseBindingBit: Self = Self(0b1000);
    pub const eProtectedBit: Self = Self(0b10000);
    pub const eVideoDecodeBitKhr: Self = Self(0b100000);
    pub const eVideoEncodeBitKhr: Self = Self(0b1000000);
    pub const eOpticalFlowBitNv: Self = Self(0b100000000);
}

bit_field_mask_derives!(QueueFlags, u32);

bit_field_mask_with_enum_debug_derive!(QueueFlagBits, QueueFlags, u32, QueueFlagBits::eGraphicsBit, QueueFlagBits::eComputeBit, QueueFlagBits::eTransferBit, QueueFlagBits::eSparseBindingBit, QueueFlagBits::eProtectedBit, QueueFlagBits::eVideoDecodeBitKhr, QueueFlagBits::eVideoEncodeBitKhr, QueueFlagBits::eOpticalFlowBitNv);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum MemoryPropertyFlagBits {
    eDeviceLocalBit,
    eHostVisibleBit,
    eHostCoherentBit,
    eHostCachedBit,
    eLazilyAllocatedBit,
    eProtectedBit,
    eDeviceCoherentBitAmd,
    eDeviceUncachedBitAmd,
    eRdmaCapableBitNv,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl MemoryPropertyFlagBits {
    pub fn into_raw(self) -> RawMemoryPropertyFlagBits {
        match self {
            Self::eDeviceLocalBit => RawMemoryPropertyFlagBits::eDeviceLocalBit,
            Self::eHostVisibleBit => RawMemoryPropertyFlagBits::eHostVisibleBit,
            Self::eHostCoherentBit => RawMemoryPropertyFlagBits::eHostCoherentBit,
            Self::eHostCachedBit => RawMemoryPropertyFlagBits::eHostCachedBit,
            Self::eLazilyAllocatedBit => RawMemoryPropertyFlagBits::eLazilyAllocatedBit,
            Self::eProtectedBit => RawMemoryPropertyFlagBits::eProtectedBit,
            Self::eDeviceCoherentBitAmd => RawMemoryPropertyFlagBits::eDeviceCoherentBitAmd,
            Self::eDeviceUncachedBitAmd => RawMemoryPropertyFlagBits::eDeviceUncachedBitAmd,
            Self::eRdmaCapableBitNv => RawMemoryPropertyFlagBits::eRdmaCapableBitNv,
            Self::eUnknownBit(b) => RawMemoryPropertyFlagBits(b),
        }
    }
}

impl core::convert::From<MemoryPropertyFlagBits> for MemoryPropertyFlags {
    fn from(value: MemoryPropertyFlagBits) -> Self {
        match value {
            MemoryPropertyFlagBits::eDeviceLocalBit => MemoryPropertyFlags::eDeviceLocalBit,
            MemoryPropertyFlagBits::eHostVisibleBit => MemoryPropertyFlags::eHostVisibleBit,
            MemoryPropertyFlagBits::eHostCoherentBit => MemoryPropertyFlags::eHostCoherentBit,
            MemoryPropertyFlagBits::eHostCachedBit => MemoryPropertyFlags::eHostCachedBit,
            MemoryPropertyFlagBits::eLazilyAllocatedBit => MemoryPropertyFlags::eLazilyAllocatedBit,
            MemoryPropertyFlagBits::eProtectedBit => MemoryPropertyFlags::eProtectedBit,
            MemoryPropertyFlagBits::eDeviceCoherentBitAmd => MemoryPropertyFlags::eDeviceCoherentBitAmd,
            MemoryPropertyFlagBits::eDeviceUncachedBitAmd => MemoryPropertyFlags::eDeviceUncachedBitAmd,
            MemoryPropertyFlagBits::eRdmaCapableBitNv => MemoryPropertyFlags::eRdmaCapableBitNv,
            MemoryPropertyFlagBits::eUnknownBit(b) => MemoryPropertyFlags(b),
        }
    }
}

bit_field_enum_derives!(MemoryPropertyFlagBits, MemoryPropertyFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct MemoryPropertyFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl MemoryPropertyFlags {
    pub const eNone: Self = Self(0);
    pub const eDeviceLocalBit: Self = Self(0b1);
    pub const eHostVisibleBit: Self = Self(0b10);
    pub const eHostCoherentBit: Self = Self(0b100);
    pub const eHostCachedBit: Self = Self(0b1000);
    pub const eLazilyAllocatedBit: Self = Self(0b10000);
    pub const eProtectedBit: Self = Self(0b100000);
    pub const eDeviceCoherentBitAmd: Self = Self(0b1000000);
    pub const eDeviceUncachedBitAmd: Self = Self(0b10000000);
    pub const eRdmaCapableBitNv: Self = Self(0b100000000);
}

bit_field_mask_derives!(MemoryPropertyFlags, u32);

bit_field_mask_with_enum_debug_derive!(MemoryPropertyFlagBits, MemoryPropertyFlags, u32, MemoryPropertyFlagBits::eDeviceLocalBit, MemoryPropertyFlagBits::eHostVisibleBit, MemoryPropertyFlagBits::eHostCoherentBit, MemoryPropertyFlagBits::eHostCachedBit, MemoryPropertyFlagBits::eLazilyAllocatedBit, MemoryPropertyFlagBits::eProtectedBit, MemoryPropertyFlagBits::eDeviceCoherentBitAmd, MemoryPropertyFlagBits::eDeviceUncachedBitAmd, MemoryPropertyFlagBits::eRdmaCapableBitNv);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum MemoryHeapFlagBits {
    eDeviceLocalBit,
    eMultiInstanceBit,
    eSeuSafeBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl MemoryHeapFlagBits {
    pub const eMultiInstanceBitKhr: Self = Self::eMultiInstanceBit;

    pub fn into_raw(self) -> RawMemoryHeapFlagBits {
        match self {
            Self::eDeviceLocalBit => RawMemoryHeapFlagBits::eDeviceLocalBit,
            Self::eMultiInstanceBit => RawMemoryHeapFlagBits::eMultiInstanceBit,
            Self::eSeuSafeBit => RawMemoryHeapFlagBits::eSeuSafeBit,
            Self::eUnknownBit(b) => RawMemoryHeapFlagBits(b),
        }
    }
}

impl core::convert::From<MemoryHeapFlagBits> for MemoryHeapFlags {
    fn from(value: MemoryHeapFlagBits) -> Self {
        match value {
            MemoryHeapFlagBits::eDeviceLocalBit => MemoryHeapFlags::eDeviceLocalBit,
            MemoryHeapFlagBits::eMultiInstanceBit => MemoryHeapFlags::eMultiInstanceBit,
            MemoryHeapFlagBits::eSeuSafeBit => MemoryHeapFlags::eSeuSafeBit,
            MemoryHeapFlagBits::eUnknownBit(b) => MemoryHeapFlags(b),
        }
    }
}

bit_field_enum_derives!(MemoryHeapFlagBits, MemoryHeapFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct MemoryHeapFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl MemoryHeapFlags {
    pub const eNone: Self = Self(0);
    pub const eDeviceLocalBit: Self = Self(0b1);
    pub const eMultiInstanceBit: Self = Self(0b10);
    pub const eSeuSafeBit: Self = Self(0b100);
    pub const eMultiInstanceBitKhr: Self = Self::eMultiInstanceBit;
}

bit_field_mask_derives!(MemoryHeapFlags, u32);

bit_field_mask_with_enum_debug_derive!(MemoryHeapFlagBits, MemoryHeapFlags, u32, MemoryHeapFlagBits::eDeviceLocalBit, MemoryHeapFlagBits::eMultiInstanceBit, MemoryHeapFlagBits::eSeuSafeBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum AccessFlagBits {
    eNone,
    eIndirectCommandReadBit,
    eIndexReadBit,
    eVertexAttributeReadBit,
    eUniformReadBit,
    eInputAttachmentReadBit,
    eShaderReadBit,
    eShaderWriteBit,
    eColourAttachmentReadBit,
    eColourAttachmentWriteBit,
    eDepthStencilAttachmentReadBit,
    eDepthStencilAttachmentWriteBit,
    eTransferReadBit,
    eTransferWriteBit,
    eHostReadBit,
    eHostWriteBit,
    eMemoryReadBit,
    eMemoryWriteBit,
    eCommandPreprocessReadBitNv,
    eCommandPreprocessWriteBitNv,
    eColourAttachmentReadNoncoherentBitExt,
    eConditionalRenderingReadBitExt,
    eAccelerationStructureReadBitKhr,
    eAccelerationStructureWriteBitKhr,
    eFragmentShadingRateAttachmentReadBitKhr,
    eFragmentDensityMapReadBitExt,
    eTransformFeedbackWriteBitExt,
    eTransformFeedbackCounterReadBitExt,
    eTransformFeedbackCounterWriteBitExt,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl AccessFlagBits {
    pub const eNoneKhr: Self = Self::eNone;
    pub const eAccelerationStructureReadBitNv: Self = Self::eAccelerationStructureReadBitKhr;
    pub const eAccelerationStructureWriteBitNv: Self = Self::eAccelerationStructureWriteBitKhr;

    pub fn into_raw(self) -> RawAccessFlagBits {
        match self {
            Self::eNone => RawAccessFlagBits::eNone,
            Self::eIndirectCommandReadBit => RawAccessFlagBits::eIndirectCommandReadBit,
            Self::eIndexReadBit => RawAccessFlagBits::eIndexReadBit,
            Self::eVertexAttributeReadBit => RawAccessFlagBits::eVertexAttributeReadBit,
            Self::eUniformReadBit => RawAccessFlagBits::eUniformReadBit,
            Self::eInputAttachmentReadBit => RawAccessFlagBits::eInputAttachmentReadBit,
            Self::eShaderReadBit => RawAccessFlagBits::eShaderReadBit,
            Self::eShaderWriteBit => RawAccessFlagBits::eShaderWriteBit,
            Self::eColourAttachmentReadBit => RawAccessFlagBits::eColourAttachmentReadBit,
            Self::eColourAttachmentWriteBit => RawAccessFlagBits::eColourAttachmentWriteBit,
            Self::eDepthStencilAttachmentReadBit => RawAccessFlagBits::eDepthStencilAttachmentReadBit,
            Self::eDepthStencilAttachmentWriteBit => RawAccessFlagBits::eDepthStencilAttachmentWriteBit,
            Self::eTransferReadBit => RawAccessFlagBits::eTransferReadBit,
            Self::eTransferWriteBit => RawAccessFlagBits::eTransferWriteBit,
            Self::eHostReadBit => RawAccessFlagBits::eHostReadBit,
            Self::eHostWriteBit => RawAccessFlagBits::eHostWriteBit,
            Self::eMemoryReadBit => RawAccessFlagBits::eMemoryReadBit,
            Self::eMemoryWriteBit => RawAccessFlagBits::eMemoryWriteBit,
            Self::eCommandPreprocessReadBitNv => RawAccessFlagBits::eCommandPreprocessReadBitNv,
            Self::eCommandPreprocessWriteBitNv => RawAccessFlagBits::eCommandPreprocessWriteBitNv,
            Self::eColourAttachmentReadNoncoherentBitExt => RawAccessFlagBits::eColourAttachmentReadNoncoherentBitExt,
            Self::eConditionalRenderingReadBitExt => RawAccessFlagBits::eConditionalRenderingReadBitExt,
            Self::eAccelerationStructureReadBitKhr => RawAccessFlagBits::eAccelerationStructureReadBitKhr,
            Self::eAccelerationStructureWriteBitKhr => RawAccessFlagBits::eAccelerationStructureWriteBitKhr,
            Self::eFragmentShadingRateAttachmentReadBitKhr => RawAccessFlagBits::eFragmentShadingRateAttachmentReadBitKhr,
            Self::eFragmentDensityMapReadBitExt => RawAccessFlagBits::eFragmentDensityMapReadBitExt,
            Self::eTransformFeedbackWriteBitExt => RawAccessFlagBits::eTransformFeedbackWriteBitExt,
            Self::eTransformFeedbackCounterReadBitExt => RawAccessFlagBits::eTransformFeedbackCounterReadBitExt,
            Self::eTransformFeedbackCounterWriteBitExt => RawAccessFlagBits::eTransformFeedbackCounterWriteBitExt,
            Self::eUnknownBit(b) => RawAccessFlagBits(b),
        }
    }
}

impl core::convert::From<AccessFlagBits> for AccessFlags {
    fn from(value: AccessFlagBits) -> Self {
        match value {
            AccessFlagBits::eNone => AccessFlags::eNone,
            AccessFlagBits::eIndirectCommandReadBit => AccessFlags::eIndirectCommandReadBit,
            AccessFlagBits::eIndexReadBit => AccessFlags::eIndexReadBit,
            AccessFlagBits::eVertexAttributeReadBit => AccessFlags::eVertexAttributeReadBit,
            AccessFlagBits::eUniformReadBit => AccessFlags::eUniformReadBit,
            AccessFlagBits::eInputAttachmentReadBit => AccessFlags::eInputAttachmentReadBit,
            AccessFlagBits::eShaderReadBit => AccessFlags::eShaderReadBit,
            AccessFlagBits::eShaderWriteBit => AccessFlags::eShaderWriteBit,
            AccessFlagBits::eColourAttachmentReadBit => AccessFlags::eColourAttachmentReadBit,
            AccessFlagBits::eColourAttachmentWriteBit => AccessFlags::eColourAttachmentWriteBit,
            AccessFlagBits::eDepthStencilAttachmentReadBit => AccessFlags::eDepthStencilAttachmentReadBit,
            AccessFlagBits::eDepthStencilAttachmentWriteBit => AccessFlags::eDepthStencilAttachmentWriteBit,
            AccessFlagBits::eTransferReadBit => AccessFlags::eTransferReadBit,
            AccessFlagBits::eTransferWriteBit => AccessFlags::eTransferWriteBit,
            AccessFlagBits::eHostReadBit => AccessFlags::eHostReadBit,
            AccessFlagBits::eHostWriteBit => AccessFlags::eHostWriteBit,
            AccessFlagBits::eMemoryReadBit => AccessFlags::eMemoryReadBit,
            AccessFlagBits::eMemoryWriteBit => AccessFlags::eMemoryWriteBit,
            AccessFlagBits::eCommandPreprocessReadBitNv => AccessFlags::eCommandPreprocessReadBitNv,
            AccessFlagBits::eCommandPreprocessWriteBitNv => AccessFlags::eCommandPreprocessWriteBitNv,
            AccessFlagBits::eColourAttachmentReadNoncoherentBitExt => AccessFlags::eColourAttachmentReadNoncoherentBitExt,
            AccessFlagBits::eConditionalRenderingReadBitExt => AccessFlags::eConditionalRenderingReadBitExt,
            AccessFlagBits::eAccelerationStructureReadBitKhr => AccessFlags::eAccelerationStructureReadBitKhr,
            AccessFlagBits::eAccelerationStructureWriteBitKhr => AccessFlags::eAccelerationStructureWriteBitKhr,
            AccessFlagBits::eFragmentShadingRateAttachmentReadBitKhr => AccessFlags::eFragmentShadingRateAttachmentReadBitKhr,
            AccessFlagBits::eFragmentDensityMapReadBitExt => AccessFlags::eFragmentDensityMapReadBitExt,
            AccessFlagBits::eTransformFeedbackWriteBitExt => AccessFlags::eTransformFeedbackWriteBitExt,
            AccessFlagBits::eTransformFeedbackCounterReadBitExt => AccessFlags::eTransformFeedbackCounterReadBitExt,
            AccessFlagBits::eTransformFeedbackCounterWriteBitExt => AccessFlags::eTransformFeedbackCounterWriteBitExt,
            AccessFlagBits::eUnknownBit(b) => AccessFlags(b),
        }
    }
}

bit_field_enum_derives!(AccessFlagBits, AccessFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct AccessFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl AccessFlags {
    pub const eNone: Self = Self(0);
    pub const eIndirectCommandReadBit: Self = Self(0b1);
    pub const eIndexReadBit: Self = Self(0b10);
    pub const eVertexAttributeReadBit: Self = Self(0b100);
    pub const eUniformReadBit: Self = Self(0b1000);
    pub const eInputAttachmentReadBit: Self = Self(0b10000);
    pub const eShaderReadBit: Self = Self(0b100000);
    pub const eShaderWriteBit: Self = Self(0b1000000);
    pub const eColourAttachmentReadBit: Self = Self(0b10000000);
    pub const eColourAttachmentWriteBit: Self = Self(0b100000000);
    pub const eDepthStencilAttachmentReadBit: Self = Self(0b1000000000);
    pub const eDepthStencilAttachmentWriteBit: Self = Self(0b10000000000);
    pub const eTransferReadBit: Self = Self(0b100000000000);
    pub const eTransferWriteBit: Self = Self(0b1000000000000);
    pub const eHostReadBit: Self = Self(0b10000000000000);
    pub const eHostWriteBit: Self = Self(0b100000000000000);
    pub const eMemoryReadBit: Self = Self(0b1000000000000000);
    pub const eMemoryWriteBit: Self = Self(0b10000000000000000);
    pub const eCommandPreprocessReadBitNv: Self = Self(0b100000000000000000);
    pub const eCommandPreprocessWriteBitNv: Self = Self(0b1000000000000000000);
    pub const eColourAttachmentReadNoncoherentBitExt: Self = Self(0b10000000000000000000);
    pub const eConditionalRenderingReadBitExt: Self = Self(0b100000000000000000000);
    pub const eAccelerationStructureReadBitKhr: Self = Self(0b1000000000000000000000);
    pub const eAccelerationStructureWriteBitKhr: Self = Self(0b10000000000000000000000);
    pub const eFragmentShadingRateAttachmentReadBitKhr: Self = Self(0b100000000000000000000000);
    pub const eFragmentDensityMapReadBitExt: Self = Self(0b1000000000000000000000000);
    pub const eTransformFeedbackWriteBitExt: Self = Self(0b10000000000000000000000000);
    pub const eTransformFeedbackCounterReadBitExt: Self = Self(0b100000000000000000000000000);
    pub const eTransformFeedbackCounterWriteBitExt: Self = Self(0b1000000000000000000000000000);
    pub const eNoneKhr: Self = Self::eNone;
    pub const eAccelerationStructureReadBitNv: Self = Self::eAccelerationStructureReadBitKhr;
    pub const eAccelerationStructureWriteBitNv: Self = Self::eAccelerationStructureWriteBitKhr;
}

bit_field_mask_derives!(AccessFlags, u32);

bit_field_mask_with_enum_debug_derive!(AccessFlagBits, AccessFlags, u32, AccessFlagBits::eIndirectCommandReadBit, AccessFlagBits::eIndexReadBit, AccessFlagBits::eVertexAttributeReadBit, AccessFlagBits::eUniformReadBit, AccessFlagBits::eInputAttachmentReadBit, AccessFlagBits::eShaderReadBit, AccessFlagBits::eShaderWriteBit, AccessFlagBits::eColourAttachmentReadBit, AccessFlagBits::eColourAttachmentWriteBit, AccessFlagBits::eDepthStencilAttachmentReadBit, AccessFlagBits::eDepthStencilAttachmentWriteBit, AccessFlagBits::eTransferReadBit, AccessFlagBits::eTransferWriteBit, AccessFlagBits::eHostReadBit, AccessFlagBits::eHostWriteBit, AccessFlagBits::eMemoryReadBit, AccessFlagBits::eMemoryWriteBit, AccessFlagBits::eCommandPreprocessReadBitNv, AccessFlagBits::eCommandPreprocessWriteBitNv, AccessFlagBits::eColourAttachmentReadNoncoherentBitExt, AccessFlagBits::eConditionalRenderingReadBitExt, AccessFlagBits::eAccelerationStructureReadBitKhr, AccessFlagBits::eAccelerationStructureWriteBitKhr, AccessFlagBits::eFragmentShadingRateAttachmentReadBitKhr, AccessFlagBits::eFragmentDensityMapReadBitExt, AccessFlagBits::eTransformFeedbackWriteBitExt, AccessFlagBits::eTransformFeedbackCounterReadBitExt, AccessFlagBits::eTransformFeedbackCounterWriteBitExt => AccessFlagBits::eNone);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum BufferUsageFlagBits {
    eTransferSrcBit,
    eTransferDstBit,
    eUniformTexelBufferBit,
    eStorageTexelBufferBit,
    eUniformBufferBit,
    eStorageBufferBit,
    eIndexBufferBit,
    eVertexBufferBit,
    eIndirectBufferBit,
    eConditionalRenderingBitExt,
    eShaderBindingTableBitKhr,
    eTransformFeedbackBufferBitExt,
    eTransformFeedbackCounterBufferBitExt,
    eVideoDecodeSrcBitKhr,
    eVideoDecodeDstBitKhr,
    eVideoEncodeDstBitKhr,
    eVideoEncodeSrcBitKhr,
    eShaderDeviceAddressBit,
    eAccelerationStructureBuildInputReadOnlyBitKhr,
    eAccelerationStructureStorageBitKhr,
    eSamplerDescriptorBufferBitExt,
    eResourceDescriptorBufferBitExt,
    eMicromapBuildInputReadOnlyBitExt,
    eMicromapStorageBitExt,
    ePushDescriptorsDescriptorBufferBitExt,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl BufferUsageFlagBits {
    pub const eRayTracingBitNv: Self = Self::eShaderBindingTableBitKhr;
    pub const eShaderDeviceAddressBitExt: Self = Self::eShaderDeviceAddressBit;
    pub const eShaderDeviceAddressBitKhr: Self = Self::eShaderDeviceAddressBit;

    pub fn into_raw(self) -> RawBufferUsageFlagBits {
        match self {
            Self::eTransferSrcBit => RawBufferUsageFlagBits::eTransferSrcBit,
            Self::eTransferDstBit => RawBufferUsageFlagBits::eTransferDstBit,
            Self::eUniformTexelBufferBit => RawBufferUsageFlagBits::eUniformTexelBufferBit,
            Self::eStorageTexelBufferBit => RawBufferUsageFlagBits::eStorageTexelBufferBit,
            Self::eUniformBufferBit => RawBufferUsageFlagBits::eUniformBufferBit,
            Self::eStorageBufferBit => RawBufferUsageFlagBits::eStorageBufferBit,
            Self::eIndexBufferBit => RawBufferUsageFlagBits::eIndexBufferBit,
            Self::eVertexBufferBit => RawBufferUsageFlagBits::eVertexBufferBit,
            Self::eIndirectBufferBit => RawBufferUsageFlagBits::eIndirectBufferBit,
            Self::eConditionalRenderingBitExt => RawBufferUsageFlagBits::eConditionalRenderingBitExt,
            Self::eShaderBindingTableBitKhr => RawBufferUsageFlagBits::eShaderBindingTableBitKhr,
            Self::eTransformFeedbackBufferBitExt => RawBufferUsageFlagBits::eTransformFeedbackBufferBitExt,
            Self::eTransformFeedbackCounterBufferBitExt => RawBufferUsageFlagBits::eTransformFeedbackCounterBufferBitExt,
            Self::eVideoDecodeSrcBitKhr => RawBufferUsageFlagBits::eVideoDecodeSrcBitKhr,
            Self::eVideoDecodeDstBitKhr => RawBufferUsageFlagBits::eVideoDecodeDstBitKhr,
            Self::eVideoEncodeDstBitKhr => RawBufferUsageFlagBits::eVideoEncodeDstBitKhr,
            Self::eVideoEncodeSrcBitKhr => RawBufferUsageFlagBits::eVideoEncodeSrcBitKhr,
            Self::eShaderDeviceAddressBit => RawBufferUsageFlagBits::eShaderDeviceAddressBit,
            Self::eAccelerationStructureBuildInputReadOnlyBitKhr => RawBufferUsageFlagBits::eAccelerationStructureBuildInputReadOnlyBitKhr,
            Self::eAccelerationStructureStorageBitKhr => RawBufferUsageFlagBits::eAccelerationStructureStorageBitKhr,
            Self::eSamplerDescriptorBufferBitExt => RawBufferUsageFlagBits::eSamplerDescriptorBufferBitExt,
            Self::eResourceDescriptorBufferBitExt => RawBufferUsageFlagBits::eResourceDescriptorBufferBitExt,
            Self::eMicromapBuildInputReadOnlyBitExt => RawBufferUsageFlagBits::eMicromapBuildInputReadOnlyBitExt,
            Self::eMicromapStorageBitExt => RawBufferUsageFlagBits::eMicromapStorageBitExt,
            Self::ePushDescriptorsDescriptorBufferBitExt => RawBufferUsageFlagBits::ePushDescriptorsDescriptorBufferBitExt,
            Self::eUnknownBit(b) => RawBufferUsageFlagBits(b),
        }
    }
}

impl core::convert::From<BufferUsageFlagBits> for BufferUsageFlags {
    fn from(value: BufferUsageFlagBits) -> Self {
        match value {
            BufferUsageFlagBits::eTransferSrcBit => BufferUsageFlags::eTransferSrcBit,
            BufferUsageFlagBits::eTransferDstBit => BufferUsageFlags::eTransferDstBit,
            BufferUsageFlagBits::eUniformTexelBufferBit => BufferUsageFlags::eUniformTexelBufferBit,
            BufferUsageFlagBits::eStorageTexelBufferBit => BufferUsageFlags::eStorageTexelBufferBit,
            BufferUsageFlagBits::eUniformBufferBit => BufferUsageFlags::eUniformBufferBit,
            BufferUsageFlagBits::eStorageBufferBit => BufferUsageFlags::eStorageBufferBit,
            BufferUsageFlagBits::eIndexBufferBit => BufferUsageFlags::eIndexBufferBit,
            BufferUsageFlagBits::eVertexBufferBit => BufferUsageFlags::eVertexBufferBit,
            BufferUsageFlagBits::eIndirectBufferBit => BufferUsageFlags::eIndirectBufferBit,
            BufferUsageFlagBits::eConditionalRenderingBitExt => BufferUsageFlags::eConditionalRenderingBitExt,
            BufferUsageFlagBits::eShaderBindingTableBitKhr => BufferUsageFlags::eShaderBindingTableBitKhr,
            BufferUsageFlagBits::eTransformFeedbackBufferBitExt => BufferUsageFlags::eTransformFeedbackBufferBitExt,
            BufferUsageFlagBits::eTransformFeedbackCounterBufferBitExt => BufferUsageFlags::eTransformFeedbackCounterBufferBitExt,
            BufferUsageFlagBits::eVideoDecodeSrcBitKhr => BufferUsageFlags::eVideoDecodeSrcBitKhr,
            BufferUsageFlagBits::eVideoDecodeDstBitKhr => BufferUsageFlags::eVideoDecodeDstBitKhr,
            BufferUsageFlagBits::eVideoEncodeDstBitKhr => BufferUsageFlags::eVideoEncodeDstBitKhr,
            BufferUsageFlagBits::eVideoEncodeSrcBitKhr => BufferUsageFlags::eVideoEncodeSrcBitKhr,
            BufferUsageFlagBits::eShaderDeviceAddressBit => BufferUsageFlags::eShaderDeviceAddressBit,
            BufferUsageFlagBits::eAccelerationStructureBuildInputReadOnlyBitKhr => BufferUsageFlags::eAccelerationStructureBuildInputReadOnlyBitKhr,
            BufferUsageFlagBits::eAccelerationStructureStorageBitKhr => BufferUsageFlags::eAccelerationStructureStorageBitKhr,
            BufferUsageFlagBits::eSamplerDescriptorBufferBitExt => BufferUsageFlags::eSamplerDescriptorBufferBitExt,
            BufferUsageFlagBits::eResourceDescriptorBufferBitExt => BufferUsageFlags::eResourceDescriptorBufferBitExt,
            BufferUsageFlagBits::eMicromapBuildInputReadOnlyBitExt => BufferUsageFlags::eMicromapBuildInputReadOnlyBitExt,
            BufferUsageFlagBits::eMicromapStorageBitExt => BufferUsageFlags::eMicromapStorageBitExt,
            BufferUsageFlagBits::ePushDescriptorsDescriptorBufferBitExt => BufferUsageFlags::ePushDescriptorsDescriptorBufferBitExt,
            BufferUsageFlagBits::eUnknownBit(b) => BufferUsageFlags(b),
        }
    }
}

bit_field_enum_derives!(BufferUsageFlagBits, BufferUsageFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct BufferUsageFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl BufferUsageFlags {
    pub const eNone: Self = Self(0);
    pub const eTransferSrcBit: Self = Self(0b1);
    pub const eTransferDstBit: Self = Self(0b10);
    pub const eUniformTexelBufferBit: Self = Self(0b100);
    pub const eStorageTexelBufferBit: Self = Self(0b1000);
    pub const eUniformBufferBit: Self = Self(0b10000);
    pub const eStorageBufferBit: Self = Self(0b100000);
    pub const eIndexBufferBit: Self = Self(0b1000000);
    pub const eVertexBufferBit: Self = Self(0b10000000);
    pub const eIndirectBufferBit: Self = Self(0b100000000);
    pub const eConditionalRenderingBitExt: Self = Self(0b1000000000);
    pub const eShaderBindingTableBitKhr: Self = Self(0b10000000000);
    pub const eTransformFeedbackBufferBitExt: Self = Self(0b100000000000);
    pub const eTransformFeedbackCounterBufferBitExt: Self = Self(0b1000000000000);
    pub const eVideoDecodeSrcBitKhr: Self = Self(0b10000000000000);
    pub const eVideoDecodeDstBitKhr: Self = Self(0b100000000000000);
    pub const eVideoEncodeDstBitKhr: Self = Self(0b1000000000000000);
    pub const eVideoEncodeSrcBitKhr: Self = Self(0b10000000000000000);
    pub const eShaderDeviceAddressBit: Self = Self(0b100000000000000000);
    pub const eAccelerationStructureBuildInputReadOnlyBitKhr: Self = Self(0b10000000000000000000);
    pub const eAccelerationStructureStorageBitKhr: Self = Self(0b100000000000000000000);
    pub const eSamplerDescriptorBufferBitExt: Self = Self(0b1000000000000000000000);
    pub const eResourceDescriptorBufferBitExt: Self = Self(0b10000000000000000000000);
    pub const eMicromapBuildInputReadOnlyBitExt: Self = Self(0b100000000000000000000000);
    pub const eMicromapStorageBitExt: Self = Self(0b1000000000000000000000000);
    pub const ePushDescriptorsDescriptorBufferBitExt: Self = Self(0b100000000000000000000000000);
    pub const eRayTracingBitNv: Self = Self::eShaderBindingTableBitKhr;
    pub const eShaderDeviceAddressBitExt: Self = Self::eShaderDeviceAddressBit;
    pub const eShaderDeviceAddressBitKhr: Self = Self::eShaderDeviceAddressBit;
}

bit_field_mask_derives!(BufferUsageFlags, u32);

bit_field_mask_with_enum_debug_derive!(BufferUsageFlagBits, BufferUsageFlags, u32, BufferUsageFlagBits::eTransferSrcBit, BufferUsageFlagBits::eTransferDstBit, BufferUsageFlagBits::eUniformTexelBufferBit, BufferUsageFlagBits::eStorageTexelBufferBit, BufferUsageFlagBits::eUniformBufferBit, BufferUsageFlagBits::eStorageBufferBit, BufferUsageFlagBits::eIndexBufferBit, BufferUsageFlagBits::eVertexBufferBit, BufferUsageFlagBits::eIndirectBufferBit, BufferUsageFlagBits::eConditionalRenderingBitExt, BufferUsageFlagBits::eShaderBindingTableBitKhr, BufferUsageFlagBits::eTransformFeedbackBufferBitExt, BufferUsageFlagBits::eTransformFeedbackCounterBufferBitExt, BufferUsageFlagBits::eVideoDecodeSrcBitKhr, BufferUsageFlagBits::eVideoDecodeDstBitKhr, BufferUsageFlagBits::eVideoEncodeDstBitKhr, BufferUsageFlagBits::eVideoEncodeSrcBitKhr, BufferUsageFlagBits::eShaderDeviceAddressBit, BufferUsageFlagBits::eAccelerationStructureBuildInputReadOnlyBitKhr, BufferUsageFlagBits::eAccelerationStructureStorageBitKhr, BufferUsageFlagBits::eSamplerDescriptorBufferBitExt, BufferUsageFlagBits::eResourceDescriptorBufferBitExt, BufferUsageFlagBits::eMicromapBuildInputReadOnlyBitExt, BufferUsageFlagBits::eMicromapStorageBitExt, BufferUsageFlagBits::ePushDescriptorsDescriptorBufferBitExt);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum BufferCreateFlagBits {
    eSparseBindingBit,
    eSparseResidencyBit,
    eSparseAliasedBit,
    eProtectedBit,
    eDeviceAddressCaptureReplayBit,
    eDescriptorBufferCaptureReplayBitExt,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl BufferCreateFlagBits {
    pub const eDeviceAddressCaptureReplayBitExt: Self = Self::eDeviceAddressCaptureReplayBit;
    pub const eDeviceAddressCaptureReplayBitKhr: Self = Self::eDeviceAddressCaptureReplayBit;

    pub fn into_raw(self) -> RawBufferCreateFlagBits {
        match self {
            Self::eSparseBindingBit => RawBufferCreateFlagBits::eSparseBindingBit,
            Self::eSparseResidencyBit => RawBufferCreateFlagBits::eSparseResidencyBit,
            Self::eSparseAliasedBit => RawBufferCreateFlagBits::eSparseAliasedBit,
            Self::eProtectedBit => RawBufferCreateFlagBits::eProtectedBit,
            Self::eDeviceAddressCaptureReplayBit => RawBufferCreateFlagBits::eDeviceAddressCaptureReplayBit,
            Self::eDescriptorBufferCaptureReplayBitExt => RawBufferCreateFlagBits::eDescriptorBufferCaptureReplayBitExt,
            Self::eUnknownBit(b) => RawBufferCreateFlagBits(b),
        }
    }
}

impl core::convert::From<BufferCreateFlagBits> for BufferCreateFlags {
    fn from(value: BufferCreateFlagBits) -> Self {
        match value {
            BufferCreateFlagBits::eSparseBindingBit => BufferCreateFlags::eSparseBindingBit,
            BufferCreateFlagBits::eSparseResidencyBit => BufferCreateFlags::eSparseResidencyBit,
            BufferCreateFlagBits::eSparseAliasedBit => BufferCreateFlags::eSparseAliasedBit,
            BufferCreateFlagBits::eProtectedBit => BufferCreateFlags::eProtectedBit,
            BufferCreateFlagBits::eDeviceAddressCaptureReplayBit => BufferCreateFlags::eDeviceAddressCaptureReplayBit,
            BufferCreateFlagBits::eDescriptorBufferCaptureReplayBitExt => BufferCreateFlags::eDescriptorBufferCaptureReplayBitExt,
            BufferCreateFlagBits::eUnknownBit(b) => BufferCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(BufferCreateFlagBits, BufferCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct BufferCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl BufferCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eSparseBindingBit: Self = Self(0b1);
    pub const eSparseResidencyBit: Self = Self(0b10);
    pub const eSparseAliasedBit: Self = Self(0b100);
    pub const eProtectedBit: Self = Self(0b1000);
    pub const eDeviceAddressCaptureReplayBit: Self = Self(0b10000);
    pub const eDescriptorBufferCaptureReplayBitExt: Self = Self(0b100000);
    pub const eDeviceAddressCaptureReplayBitExt: Self = Self::eDeviceAddressCaptureReplayBit;
    pub const eDeviceAddressCaptureReplayBitKhr: Self = Self::eDeviceAddressCaptureReplayBit;
}

bit_field_mask_derives!(BufferCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(BufferCreateFlagBits, BufferCreateFlags, u32, BufferCreateFlagBits::eSparseBindingBit, BufferCreateFlagBits::eSparseResidencyBit, BufferCreateFlagBits::eSparseAliasedBit, BufferCreateFlagBits::eProtectedBit, BufferCreateFlagBits::eDeviceAddressCaptureReplayBit, BufferCreateFlagBits::eDescriptorBufferCaptureReplayBitExt);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ShaderStageFlagBits {
    eVertexBit,
    eTessellationControlBit,
    eTessellationEvaluationBit,
    eGeometryBit,
    eFragmentBit,
    eAllGraphics,
    eComputeBit,
    eTaskBitExt,
    eMeshBitExt,
    eRaygenBitKhr,
    eAnyHitBitKhr,
    eClosestHitBitKhr,
    eMissBitKhr,
    eIntersectionBitKhr,
    eCallableBitKhr,
    eSubpassShadingBitHuawei,
    eClusterCullingBitHuawei,
    eAll,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl ShaderStageFlagBits {
    pub const eRaygenBitNv: Self = Self::eRaygenBitKhr;
    pub const eAnyHitBitNv: Self = Self::eAnyHitBitKhr;
    pub const eClosestHitBitNv: Self = Self::eClosestHitBitKhr;
    pub const eMissBitNv: Self = Self::eMissBitKhr;
    pub const eIntersectionBitNv: Self = Self::eIntersectionBitKhr;
    pub const eCallableBitNv: Self = Self::eCallableBitKhr;

    pub fn into_raw(self) -> RawShaderStageFlagBits {
        match self {
            Self::eVertexBit => RawShaderStageFlagBits::eVertexBit,
            Self::eTessellationControlBit => RawShaderStageFlagBits::eTessellationControlBit,
            Self::eTessellationEvaluationBit => RawShaderStageFlagBits::eTessellationEvaluationBit,
            Self::eGeometryBit => RawShaderStageFlagBits::eGeometryBit,
            Self::eFragmentBit => RawShaderStageFlagBits::eFragmentBit,
            Self::eAllGraphics => RawShaderStageFlagBits::eAllGraphics,
            Self::eComputeBit => RawShaderStageFlagBits::eComputeBit,
            Self::eTaskBitExt => RawShaderStageFlagBits::eTaskBitExt,
            Self::eMeshBitExt => RawShaderStageFlagBits::eMeshBitExt,
            Self::eRaygenBitKhr => RawShaderStageFlagBits::eRaygenBitKhr,
            Self::eAnyHitBitKhr => RawShaderStageFlagBits::eAnyHitBitKhr,
            Self::eClosestHitBitKhr => RawShaderStageFlagBits::eClosestHitBitKhr,
            Self::eMissBitKhr => RawShaderStageFlagBits::eMissBitKhr,
            Self::eIntersectionBitKhr => RawShaderStageFlagBits::eIntersectionBitKhr,
            Self::eCallableBitKhr => RawShaderStageFlagBits::eCallableBitKhr,
            Self::eSubpassShadingBitHuawei => RawShaderStageFlagBits::eSubpassShadingBitHuawei,
            Self::eClusterCullingBitHuawei => RawShaderStageFlagBits::eClusterCullingBitHuawei,
            Self::eAll => RawShaderStageFlagBits::eAll,
            Self::eUnknownBit(b) => RawShaderStageFlagBits(b),
        }
    }
}

impl core::convert::From<ShaderStageFlagBits> for ShaderStageFlags {
    fn from(value: ShaderStageFlagBits) -> Self {
        match value {
            ShaderStageFlagBits::eVertexBit => ShaderStageFlags::eVertexBit,
            ShaderStageFlagBits::eTessellationControlBit => ShaderStageFlags::eTessellationControlBit,
            ShaderStageFlagBits::eTessellationEvaluationBit => ShaderStageFlags::eTessellationEvaluationBit,
            ShaderStageFlagBits::eGeometryBit => ShaderStageFlags::eGeometryBit,
            ShaderStageFlagBits::eFragmentBit => ShaderStageFlags::eFragmentBit,
            ShaderStageFlagBits::eAllGraphics => ShaderStageFlags::eAllGraphics,
            ShaderStageFlagBits::eComputeBit => ShaderStageFlags::eComputeBit,
            ShaderStageFlagBits::eTaskBitExt => ShaderStageFlags::eTaskBitExt,
            ShaderStageFlagBits::eMeshBitExt => ShaderStageFlags::eMeshBitExt,
            ShaderStageFlagBits::eRaygenBitKhr => ShaderStageFlags::eRaygenBitKhr,
            ShaderStageFlagBits::eAnyHitBitKhr => ShaderStageFlags::eAnyHitBitKhr,
            ShaderStageFlagBits::eClosestHitBitKhr => ShaderStageFlags::eClosestHitBitKhr,
            ShaderStageFlagBits::eMissBitKhr => ShaderStageFlags::eMissBitKhr,
            ShaderStageFlagBits::eIntersectionBitKhr => ShaderStageFlags::eIntersectionBitKhr,
            ShaderStageFlagBits::eCallableBitKhr => ShaderStageFlags::eCallableBitKhr,
            ShaderStageFlagBits::eSubpassShadingBitHuawei => ShaderStageFlags::eSubpassShadingBitHuawei,
            ShaderStageFlagBits::eClusterCullingBitHuawei => ShaderStageFlags::eClusterCullingBitHuawei,
            ShaderStageFlagBits::eAll => ShaderStageFlags::eAll,
            ShaderStageFlagBits::eUnknownBit(b) => ShaderStageFlags(b),
        }
    }
}

bit_field_enum_derives!(ShaderStageFlagBits, ShaderStageFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ShaderStageFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ShaderStageFlags {
    pub const eNone: Self = Self(0);
    pub const eVertexBit: Self = Self(0b1);
    pub const eTessellationControlBit: Self = Self(0b10);
    pub const eTessellationEvaluationBit: Self = Self(0b100);
    pub const eGeometryBit: Self = Self(0b1000);
    pub const eFragmentBit: Self = Self(0b10000);
    pub const eAllGraphics: Self = Self(0b11111);
    pub const eComputeBit: Self = Self(0b100000);
    pub const eTaskBitExt: Self = Self(0b1000000);
    pub const eMeshBitExt: Self = Self(0b10000000);
    pub const eRaygenBitKhr: Self = Self(0b100000000);
    pub const eAnyHitBitKhr: Self = Self(0b1000000000);
    pub const eClosestHitBitKhr: Self = Self(0b10000000000);
    pub const eMissBitKhr: Self = Self(0b100000000000);
    pub const eIntersectionBitKhr: Self = Self(0b1000000000000);
    pub const eCallableBitKhr: Self = Self(0b10000000000000);
    pub const eSubpassShadingBitHuawei: Self = Self(0b100000000000000);
    pub const eClusterCullingBitHuawei: Self = Self(0b10000000000000000000);
    pub const eAll: Self = Self(0b1111111111111111111111111111111);
    pub const eRaygenBitNv: Self = Self::eRaygenBitKhr;
    pub const eAnyHitBitNv: Self = Self::eAnyHitBitKhr;
    pub const eClosestHitBitNv: Self = Self::eClosestHitBitKhr;
    pub const eMissBitNv: Self = Self::eMissBitKhr;
    pub const eIntersectionBitNv: Self = Self::eIntersectionBitKhr;
    pub const eCallableBitNv: Self = Self::eCallableBitKhr;
}

bit_field_mask_derives!(ShaderStageFlags, u32);

bit_field_mask_with_enum_debug_derive!(ShaderStageFlagBits, ShaderStageFlags, u32, ShaderStageFlagBits::eVertexBit, ShaderStageFlagBits::eTessellationControlBit, ShaderStageFlagBits::eTessellationEvaluationBit, ShaderStageFlagBits::eGeometryBit, ShaderStageFlagBits::eFragmentBit, ShaderStageFlagBits::eAllGraphics, ShaderStageFlagBits::eComputeBit, ShaderStageFlagBits::eTaskBitExt, ShaderStageFlagBits::eMeshBitExt, ShaderStageFlagBits::eRaygenBitKhr, ShaderStageFlagBits::eAnyHitBitKhr, ShaderStageFlagBits::eClosestHitBitKhr, ShaderStageFlagBits::eMissBitKhr, ShaderStageFlagBits::eIntersectionBitKhr, ShaderStageFlagBits::eCallableBitKhr, ShaderStageFlagBits::eSubpassShadingBitHuawei, ShaderStageFlagBits::eClusterCullingBitHuawei, ShaderStageFlagBits::eAll);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ImageUsageFlagBits {
    eTransferSrcBit,
    eTransferDstBit,
    eSampledBit,
    eStorageBit,
    eColourAttachmentBit,
    eDepthStencilAttachmentBit,
    eTransientAttachmentBit,
    eInputAttachmentBit,
    eFragmentShadingRateAttachmentBitKhr,
    eFragmentDensityMapBitExt,
    eVideoDecodeDstBitKhr,
    eVideoDecodeSrcBitKhr,
    eVideoDecodeDpbBitKhr,
    eVideoEncodeDstBitKhr,
    eVideoEncodeSrcBitKhr,
    eVideoEncodeDpbBitKhr,
    eInvocationMaskBitHuawei,
    eAttachmentFeedbackLoopBitExt,
    eSampleWeightBitQcom,
    eSampleBlockMatchBitQcom,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl ImageUsageFlagBits {
    pub fn into_raw(self) -> RawImageUsageFlagBits {
        match self {
            Self::eTransferSrcBit => RawImageUsageFlagBits::eTransferSrcBit,
            Self::eTransferDstBit => RawImageUsageFlagBits::eTransferDstBit,
            Self::eSampledBit => RawImageUsageFlagBits::eSampledBit,
            Self::eStorageBit => RawImageUsageFlagBits::eStorageBit,
            Self::eColourAttachmentBit => RawImageUsageFlagBits::eColourAttachmentBit,
            Self::eDepthStencilAttachmentBit => RawImageUsageFlagBits::eDepthStencilAttachmentBit,
            Self::eTransientAttachmentBit => RawImageUsageFlagBits::eTransientAttachmentBit,
            Self::eInputAttachmentBit => RawImageUsageFlagBits::eInputAttachmentBit,
            Self::eFragmentShadingRateAttachmentBitKhr => RawImageUsageFlagBits::eFragmentShadingRateAttachmentBitKhr,
            Self::eFragmentDensityMapBitExt => RawImageUsageFlagBits::eFragmentDensityMapBitExt,
            Self::eVideoDecodeDstBitKhr => RawImageUsageFlagBits::eVideoDecodeDstBitKhr,
            Self::eVideoDecodeSrcBitKhr => RawImageUsageFlagBits::eVideoDecodeSrcBitKhr,
            Self::eVideoDecodeDpbBitKhr => RawImageUsageFlagBits::eVideoDecodeDpbBitKhr,
            Self::eVideoEncodeDstBitKhr => RawImageUsageFlagBits::eVideoEncodeDstBitKhr,
            Self::eVideoEncodeSrcBitKhr => RawImageUsageFlagBits::eVideoEncodeSrcBitKhr,
            Self::eVideoEncodeDpbBitKhr => RawImageUsageFlagBits::eVideoEncodeDpbBitKhr,
            Self::eInvocationMaskBitHuawei => RawImageUsageFlagBits::eInvocationMaskBitHuawei,
            Self::eAttachmentFeedbackLoopBitExt => RawImageUsageFlagBits::eAttachmentFeedbackLoopBitExt,
            Self::eSampleWeightBitQcom => RawImageUsageFlagBits::eSampleWeightBitQcom,
            Self::eSampleBlockMatchBitQcom => RawImageUsageFlagBits::eSampleBlockMatchBitQcom,
            Self::eUnknownBit(b) => RawImageUsageFlagBits(b),
        }
    }
}

impl core::convert::From<ImageUsageFlagBits> for ImageUsageFlags {
    fn from(value: ImageUsageFlagBits) -> Self {
        match value {
            ImageUsageFlagBits::eTransferSrcBit => ImageUsageFlags::eTransferSrcBit,
            ImageUsageFlagBits::eTransferDstBit => ImageUsageFlags::eTransferDstBit,
            ImageUsageFlagBits::eSampledBit => ImageUsageFlags::eSampledBit,
            ImageUsageFlagBits::eStorageBit => ImageUsageFlags::eStorageBit,
            ImageUsageFlagBits::eColourAttachmentBit => ImageUsageFlags::eColourAttachmentBit,
            ImageUsageFlagBits::eDepthStencilAttachmentBit => ImageUsageFlags::eDepthStencilAttachmentBit,
            ImageUsageFlagBits::eTransientAttachmentBit => ImageUsageFlags::eTransientAttachmentBit,
            ImageUsageFlagBits::eInputAttachmentBit => ImageUsageFlags::eInputAttachmentBit,
            ImageUsageFlagBits::eFragmentShadingRateAttachmentBitKhr => ImageUsageFlags::eFragmentShadingRateAttachmentBitKhr,
            ImageUsageFlagBits::eFragmentDensityMapBitExt => ImageUsageFlags::eFragmentDensityMapBitExt,
            ImageUsageFlagBits::eVideoDecodeDstBitKhr => ImageUsageFlags::eVideoDecodeDstBitKhr,
            ImageUsageFlagBits::eVideoDecodeSrcBitKhr => ImageUsageFlags::eVideoDecodeSrcBitKhr,
            ImageUsageFlagBits::eVideoDecodeDpbBitKhr => ImageUsageFlags::eVideoDecodeDpbBitKhr,
            ImageUsageFlagBits::eVideoEncodeDstBitKhr => ImageUsageFlags::eVideoEncodeDstBitKhr,
            ImageUsageFlagBits::eVideoEncodeSrcBitKhr => ImageUsageFlags::eVideoEncodeSrcBitKhr,
            ImageUsageFlagBits::eVideoEncodeDpbBitKhr => ImageUsageFlags::eVideoEncodeDpbBitKhr,
            ImageUsageFlagBits::eInvocationMaskBitHuawei => ImageUsageFlags::eInvocationMaskBitHuawei,
            ImageUsageFlagBits::eAttachmentFeedbackLoopBitExt => ImageUsageFlags::eAttachmentFeedbackLoopBitExt,
            ImageUsageFlagBits::eSampleWeightBitQcom => ImageUsageFlags::eSampleWeightBitQcom,
            ImageUsageFlagBits::eSampleBlockMatchBitQcom => ImageUsageFlags::eSampleBlockMatchBitQcom,
            ImageUsageFlagBits::eUnknownBit(b) => ImageUsageFlags(b),
        }
    }
}

bit_field_enum_derives!(ImageUsageFlagBits, ImageUsageFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ImageUsageFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ImageUsageFlags {
    pub const eNone: Self = Self(0);
    pub const eTransferSrcBit: Self = Self(0b1);
    pub const eTransferDstBit: Self = Self(0b10);
    pub const eSampledBit: Self = Self(0b100);
    pub const eStorageBit: Self = Self(0b1000);
    pub const eColourAttachmentBit: Self = Self(0b10000);
    pub const eDepthStencilAttachmentBit: Self = Self(0b100000);
    pub const eTransientAttachmentBit: Self = Self(0b1000000);
    pub const eInputAttachmentBit: Self = Self(0b10000000);
    pub const eFragmentShadingRateAttachmentBitKhr: Self = Self(0b100000000);
    pub const eFragmentDensityMapBitExt: Self = Self(0b1000000000);
    pub const eVideoDecodeDstBitKhr: Self = Self(0b10000000000);
    pub const eVideoDecodeSrcBitKhr: Self = Self(0b100000000000);
    pub const eVideoDecodeDpbBitKhr: Self = Self(0b1000000000000);
    pub const eVideoEncodeDstBitKhr: Self = Self(0b10000000000000);
    pub const eVideoEncodeSrcBitKhr: Self = Self(0b100000000000000);
    pub const eVideoEncodeDpbBitKhr: Self = Self(0b1000000000000000);
    pub const eInvocationMaskBitHuawei: Self = Self(0b1000000000000000000);
    pub const eAttachmentFeedbackLoopBitExt: Self = Self(0b10000000000000000000);
    pub const eSampleWeightBitQcom: Self = Self(0b100000000000000000000);
    pub const eSampleBlockMatchBitQcom: Self = Self(0b1000000000000000000000);
}

bit_field_mask_derives!(ImageUsageFlags, u32);

bit_field_mask_with_enum_debug_derive!(ImageUsageFlagBits, ImageUsageFlags, u32, ImageUsageFlagBits::eTransferSrcBit, ImageUsageFlagBits::eTransferDstBit, ImageUsageFlagBits::eSampledBit, ImageUsageFlagBits::eStorageBit, ImageUsageFlagBits::eColourAttachmentBit, ImageUsageFlagBits::eDepthStencilAttachmentBit, ImageUsageFlagBits::eTransientAttachmentBit, ImageUsageFlagBits::eInputAttachmentBit, ImageUsageFlagBits::eFragmentShadingRateAttachmentBitKhr, ImageUsageFlagBits::eFragmentDensityMapBitExt, ImageUsageFlagBits::eVideoDecodeDstBitKhr, ImageUsageFlagBits::eVideoDecodeSrcBitKhr, ImageUsageFlagBits::eVideoDecodeDpbBitKhr, ImageUsageFlagBits::eVideoEncodeDstBitKhr, ImageUsageFlagBits::eVideoEncodeSrcBitKhr, ImageUsageFlagBits::eVideoEncodeDpbBitKhr, ImageUsageFlagBits::eInvocationMaskBitHuawei, ImageUsageFlagBits::eAttachmentFeedbackLoopBitExt, ImageUsageFlagBits::eSampleWeightBitQcom, ImageUsageFlagBits::eSampleBlockMatchBitQcom);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ImageCreateFlagBits {
    eSparseBindingBit,
    eSparseResidencyBit,
    eSparseAliasedBit,
    eMutableFormatBit,
    eCubeCompatibleBit,
    e2dArrayCompatibleBit,
    eSplitInstanceBindRegionsBit,
    eBlockTexelViewCompatibleBit,
    eExtendedUsageBit,
    eDisjointBit,
    eAliasBit,
    eProtectedBit,
    eSampleLocationsCompatibleDepthBitExt,
    eCornerSampledBitNv,
    eSubsampledBitExt,
    eFragmentDensityMapOffsetBitQcom,
    eDescriptorBufferCaptureReplayBitExt,
    e2dViewCompatibleBitExt,
    eMultisampledRenderToSingleSampledBitExt,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl ImageCreateFlagBits {
    pub const e2dArrayCompatibleBitKhr: Self = Self::e2dArrayCompatibleBit;
    pub const eSplitInstanceBindRegionsBitKhr: Self = Self::eSplitInstanceBindRegionsBit;
    pub const eBlockTexelViewCompatibleBitKhr: Self = Self::eBlockTexelViewCompatibleBit;
    pub const eExtendedUsageBitKhr: Self = Self::eExtendedUsageBit;
    pub const eDisjointBitKhr: Self = Self::eDisjointBit;
    pub const eAliasBitKhr: Self = Self::eAliasBit;

    pub fn into_raw(self) -> RawImageCreateFlagBits {
        match self {
            Self::eSparseBindingBit => RawImageCreateFlagBits::eSparseBindingBit,
            Self::eSparseResidencyBit => RawImageCreateFlagBits::eSparseResidencyBit,
            Self::eSparseAliasedBit => RawImageCreateFlagBits::eSparseAliasedBit,
            Self::eMutableFormatBit => RawImageCreateFlagBits::eMutableFormatBit,
            Self::eCubeCompatibleBit => RawImageCreateFlagBits::eCubeCompatibleBit,
            Self::e2dArrayCompatibleBit => RawImageCreateFlagBits::e2dArrayCompatibleBit,
            Self::eSplitInstanceBindRegionsBit => RawImageCreateFlagBits::eSplitInstanceBindRegionsBit,
            Self::eBlockTexelViewCompatibleBit => RawImageCreateFlagBits::eBlockTexelViewCompatibleBit,
            Self::eExtendedUsageBit => RawImageCreateFlagBits::eExtendedUsageBit,
            Self::eDisjointBit => RawImageCreateFlagBits::eDisjointBit,
            Self::eAliasBit => RawImageCreateFlagBits::eAliasBit,
            Self::eProtectedBit => RawImageCreateFlagBits::eProtectedBit,
            Self::eSampleLocationsCompatibleDepthBitExt => RawImageCreateFlagBits::eSampleLocationsCompatibleDepthBitExt,
            Self::eCornerSampledBitNv => RawImageCreateFlagBits::eCornerSampledBitNv,
            Self::eSubsampledBitExt => RawImageCreateFlagBits::eSubsampledBitExt,
            Self::eFragmentDensityMapOffsetBitQcom => RawImageCreateFlagBits::eFragmentDensityMapOffsetBitQcom,
            Self::eDescriptorBufferCaptureReplayBitExt => RawImageCreateFlagBits::eDescriptorBufferCaptureReplayBitExt,
            Self::e2dViewCompatibleBitExt => RawImageCreateFlagBits::e2dViewCompatibleBitExt,
            Self::eMultisampledRenderToSingleSampledBitExt => RawImageCreateFlagBits::eMultisampledRenderToSingleSampledBitExt,
            Self::eUnknownBit(b) => RawImageCreateFlagBits(b),
        }
    }
}

impl core::convert::From<ImageCreateFlagBits> for ImageCreateFlags {
    fn from(value: ImageCreateFlagBits) -> Self {
        match value {
            ImageCreateFlagBits::eSparseBindingBit => ImageCreateFlags::eSparseBindingBit,
            ImageCreateFlagBits::eSparseResidencyBit => ImageCreateFlags::eSparseResidencyBit,
            ImageCreateFlagBits::eSparseAliasedBit => ImageCreateFlags::eSparseAliasedBit,
            ImageCreateFlagBits::eMutableFormatBit => ImageCreateFlags::eMutableFormatBit,
            ImageCreateFlagBits::eCubeCompatibleBit => ImageCreateFlags::eCubeCompatibleBit,
            ImageCreateFlagBits::e2dArrayCompatibleBit => ImageCreateFlags::e2dArrayCompatibleBit,
            ImageCreateFlagBits::eSplitInstanceBindRegionsBit => ImageCreateFlags::eSplitInstanceBindRegionsBit,
            ImageCreateFlagBits::eBlockTexelViewCompatibleBit => ImageCreateFlags::eBlockTexelViewCompatibleBit,
            ImageCreateFlagBits::eExtendedUsageBit => ImageCreateFlags::eExtendedUsageBit,
            ImageCreateFlagBits::eDisjointBit => ImageCreateFlags::eDisjointBit,
            ImageCreateFlagBits::eAliasBit => ImageCreateFlags::eAliasBit,
            ImageCreateFlagBits::eProtectedBit => ImageCreateFlags::eProtectedBit,
            ImageCreateFlagBits::eSampleLocationsCompatibleDepthBitExt => ImageCreateFlags::eSampleLocationsCompatibleDepthBitExt,
            ImageCreateFlagBits::eCornerSampledBitNv => ImageCreateFlags::eCornerSampledBitNv,
            ImageCreateFlagBits::eSubsampledBitExt => ImageCreateFlags::eSubsampledBitExt,
            ImageCreateFlagBits::eFragmentDensityMapOffsetBitQcom => ImageCreateFlags::eFragmentDensityMapOffsetBitQcom,
            ImageCreateFlagBits::eDescriptorBufferCaptureReplayBitExt => ImageCreateFlags::eDescriptorBufferCaptureReplayBitExt,
            ImageCreateFlagBits::e2dViewCompatibleBitExt => ImageCreateFlags::e2dViewCompatibleBitExt,
            ImageCreateFlagBits::eMultisampledRenderToSingleSampledBitExt => ImageCreateFlags::eMultisampledRenderToSingleSampledBitExt,
            ImageCreateFlagBits::eUnknownBit(b) => ImageCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(ImageCreateFlagBits, ImageCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ImageCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ImageCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eSparseBindingBit: Self = Self(0b1);
    pub const eSparseResidencyBit: Self = Self(0b10);
    pub const eSparseAliasedBit: Self = Self(0b100);
    pub const eMutableFormatBit: Self = Self(0b1000);
    pub const eCubeCompatibleBit: Self = Self(0b10000);
    pub const e2dArrayCompatibleBit: Self = Self(0b100000);
    pub const eSplitInstanceBindRegionsBit: Self = Self(0b1000000);
    pub const eBlockTexelViewCompatibleBit: Self = Self(0b10000000);
    pub const eExtendedUsageBit: Self = Self(0b100000000);
    pub const eDisjointBit: Self = Self(0b1000000000);
    pub const eAliasBit: Self = Self(0b10000000000);
    pub const eProtectedBit: Self = Self(0b100000000000);
    pub const eSampleLocationsCompatibleDepthBitExt: Self = Self(0b1000000000000);
    pub const eCornerSampledBitNv: Self = Self(0b10000000000000);
    pub const eSubsampledBitExt: Self = Self(0b100000000000000);
    pub const eFragmentDensityMapOffsetBitQcom: Self = Self(0b1000000000000000);
    pub const eDescriptorBufferCaptureReplayBitExt: Self = Self(0b10000000000000000);
    pub const e2dViewCompatibleBitExt: Self = Self(0b100000000000000000);
    pub const eMultisampledRenderToSingleSampledBitExt: Self = Self(0b1000000000000000000);
    pub const e2dArrayCompatibleBitKhr: Self = Self::e2dArrayCompatibleBit;
    pub const eSplitInstanceBindRegionsBitKhr: Self = Self::eSplitInstanceBindRegionsBit;
    pub const eBlockTexelViewCompatibleBitKhr: Self = Self::eBlockTexelViewCompatibleBit;
    pub const eExtendedUsageBitKhr: Self = Self::eExtendedUsageBit;
    pub const eDisjointBitKhr: Self = Self::eDisjointBit;
    pub const eAliasBitKhr: Self = Self::eAliasBit;
}

bit_field_mask_derives!(ImageCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(ImageCreateFlagBits, ImageCreateFlags, u32, ImageCreateFlagBits::eSparseBindingBit, ImageCreateFlagBits::eSparseResidencyBit, ImageCreateFlagBits::eSparseAliasedBit, ImageCreateFlagBits::eMutableFormatBit, ImageCreateFlagBits::eCubeCompatibleBit, ImageCreateFlagBits::e2dArrayCompatibleBit, ImageCreateFlagBits::eSplitInstanceBindRegionsBit, ImageCreateFlagBits::eBlockTexelViewCompatibleBit, ImageCreateFlagBits::eExtendedUsageBit, ImageCreateFlagBits::eDisjointBit, ImageCreateFlagBits::eAliasBit, ImageCreateFlagBits::eProtectedBit, ImageCreateFlagBits::eSampleLocationsCompatibleDepthBitExt, ImageCreateFlagBits::eCornerSampledBitNv, ImageCreateFlagBits::eSubsampledBitExt, ImageCreateFlagBits::eFragmentDensityMapOffsetBitQcom, ImageCreateFlagBits::eDescriptorBufferCaptureReplayBitExt, ImageCreateFlagBits::e2dViewCompatibleBitExt, ImageCreateFlagBits::eMultisampledRenderToSingleSampledBitExt);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ImageViewCreateFlagBits {
    eFragmentDensityMapDynamicBitExt,
    eFragmentDensityMapDeferredBitExt,
    eDescriptorBufferCaptureReplayBitExt,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl ImageViewCreateFlagBits {
    pub fn into_raw(self) -> RawImageViewCreateFlagBits {
        match self {
            Self::eFragmentDensityMapDynamicBitExt => RawImageViewCreateFlagBits::eFragmentDensityMapDynamicBitExt,
            Self::eFragmentDensityMapDeferredBitExt => RawImageViewCreateFlagBits::eFragmentDensityMapDeferredBitExt,
            Self::eDescriptorBufferCaptureReplayBitExt => RawImageViewCreateFlagBits::eDescriptorBufferCaptureReplayBitExt,
            Self::eUnknownBit(b) => RawImageViewCreateFlagBits(b),
        }
    }
}

impl core::convert::From<ImageViewCreateFlagBits> for ImageViewCreateFlags {
    fn from(value: ImageViewCreateFlagBits) -> Self {
        match value {
            ImageViewCreateFlagBits::eFragmentDensityMapDynamicBitExt => ImageViewCreateFlags::eFragmentDensityMapDynamicBitExt,
            ImageViewCreateFlagBits::eFragmentDensityMapDeferredBitExt => ImageViewCreateFlags::eFragmentDensityMapDeferredBitExt,
            ImageViewCreateFlagBits::eDescriptorBufferCaptureReplayBitExt => ImageViewCreateFlags::eDescriptorBufferCaptureReplayBitExt,
            ImageViewCreateFlagBits::eUnknownBit(b) => ImageViewCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(ImageViewCreateFlagBits, ImageViewCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ImageViewCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ImageViewCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eFragmentDensityMapDynamicBitExt: Self = Self(0b1);
    pub const eFragmentDensityMapDeferredBitExt: Self = Self(0b10);
    pub const eDescriptorBufferCaptureReplayBitExt: Self = Self(0b100);
}

bit_field_mask_derives!(ImageViewCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(ImageViewCreateFlagBits, ImageViewCreateFlags, u32, ImageViewCreateFlagBits::eFragmentDensityMapDynamicBitExt, ImageViewCreateFlagBits::eFragmentDensityMapDeferredBitExt, ImageViewCreateFlagBits::eDescriptorBufferCaptureReplayBitExt);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PipelineCreateFlagBits {
    eDisableOptimizationBit,
    eAllowDerivativesBit,
    eDerivativeBit,
    eViewIndexFromDeviceIndexBit,
    eDispatchBaseBit,
    eDeferCompileBitNv,
    eCaptureStatisticsBitKhr,
    eCaptureInternalRepresentationsBitKhr,
    eFailOnPipelineCompileRequiredBit,
    eEarlyReturnOnFailureBit,
    eLinkTimeOptimizationBitExt,
    eLibraryBitKhr,
    eRayTracingSkipTrianglesBitKhr,
    eRayTracingSkipAabbsBitKhr,
    eRayTracingNoNullAnyHitShadersBitKhr,
    eRayTracingNoNullClosestHitShadersBitKhr,
    eRayTracingNoNullMissShadersBitKhr,
    eRayTracingNoNullIntersectionShadersBitKhr,
    eIndirectBindableBitNv,
    eRayTracingShaderGroupHandleCaptureReplayBitKhr,
    eRayTracingAllowMotionBitNv,
    eRenderingFragmentShadingRateAttachmentBitKhr,
    eRenderingFragmentDensityMapAttachmentBitExt,
    eRetainLinkTimeOptimizationInfoBitExt,
    eRayTracingOpacityMicromapBitExt,
    eColourAttachmentFeedbackLoopBitExt,
    eDepthStencilAttachmentFeedbackLoopBitExt,
    eNoProtectedAccessBitExt,
    eDescriptorBufferBitExt,
    eProtectedAccessOnlyBitExt,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl PipelineCreateFlagBits {
    pub const eViewIndexFromDeviceIndexBitKhr: Self = Self::eViewIndexFromDeviceIndexBit;
    pub const eDispatchBase: Self = Self::eDispatchBaseBit;
    pub const eFailOnPipelineCompileRequiredBitExt: Self = Self::eFailOnPipelineCompileRequiredBit;
    pub const eEarlyReturnOnFailureBitExt: Self = Self::eEarlyReturnOnFailureBit;
    pub const eRasterizationStateCreateFragmentShadingRateAttachmentBitKhr: Self = Self::eRenderingFragmentShadingRateAttachmentBitKhr;
    pub const eRasterizationStateCreateFragmentDensityMapAttachmentBitExt: Self = Self::eRenderingFragmentDensityMapAttachmentBitExt;

    pub fn into_raw(self) -> RawPipelineCreateFlagBits {
        match self {
            Self::eDisableOptimizationBit => RawPipelineCreateFlagBits::eDisableOptimizationBit,
            Self::eAllowDerivativesBit => RawPipelineCreateFlagBits::eAllowDerivativesBit,
            Self::eDerivativeBit => RawPipelineCreateFlagBits::eDerivativeBit,
            Self::eViewIndexFromDeviceIndexBit => RawPipelineCreateFlagBits::eViewIndexFromDeviceIndexBit,
            Self::eDispatchBaseBit => RawPipelineCreateFlagBits::eDispatchBaseBit,
            Self::eDeferCompileBitNv => RawPipelineCreateFlagBits::eDeferCompileBitNv,
            Self::eCaptureStatisticsBitKhr => RawPipelineCreateFlagBits::eCaptureStatisticsBitKhr,
            Self::eCaptureInternalRepresentationsBitKhr => RawPipelineCreateFlagBits::eCaptureInternalRepresentationsBitKhr,
            Self::eFailOnPipelineCompileRequiredBit => RawPipelineCreateFlagBits::eFailOnPipelineCompileRequiredBit,
            Self::eEarlyReturnOnFailureBit => RawPipelineCreateFlagBits::eEarlyReturnOnFailureBit,
            Self::eLinkTimeOptimizationBitExt => RawPipelineCreateFlagBits::eLinkTimeOptimizationBitExt,
            Self::eLibraryBitKhr => RawPipelineCreateFlagBits::eLibraryBitKhr,
            Self::eRayTracingSkipTrianglesBitKhr => RawPipelineCreateFlagBits::eRayTracingSkipTrianglesBitKhr,
            Self::eRayTracingSkipAabbsBitKhr => RawPipelineCreateFlagBits::eRayTracingSkipAabbsBitKhr,
            Self::eRayTracingNoNullAnyHitShadersBitKhr => RawPipelineCreateFlagBits::eRayTracingNoNullAnyHitShadersBitKhr,
            Self::eRayTracingNoNullClosestHitShadersBitKhr => RawPipelineCreateFlagBits::eRayTracingNoNullClosestHitShadersBitKhr,
            Self::eRayTracingNoNullMissShadersBitKhr => RawPipelineCreateFlagBits::eRayTracingNoNullMissShadersBitKhr,
            Self::eRayTracingNoNullIntersectionShadersBitKhr => RawPipelineCreateFlagBits::eRayTracingNoNullIntersectionShadersBitKhr,
            Self::eIndirectBindableBitNv => RawPipelineCreateFlagBits::eIndirectBindableBitNv,
            Self::eRayTracingShaderGroupHandleCaptureReplayBitKhr => RawPipelineCreateFlagBits::eRayTracingShaderGroupHandleCaptureReplayBitKhr,
            Self::eRayTracingAllowMotionBitNv => RawPipelineCreateFlagBits::eRayTracingAllowMotionBitNv,
            Self::eRenderingFragmentShadingRateAttachmentBitKhr => RawPipelineCreateFlagBits::eRenderingFragmentShadingRateAttachmentBitKhr,
            Self::eRenderingFragmentDensityMapAttachmentBitExt => RawPipelineCreateFlagBits::eRenderingFragmentDensityMapAttachmentBitExt,
            Self::eRetainLinkTimeOptimizationInfoBitExt => RawPipelineCreateFlagBits::eRetainLinkTimeOptimizationInfoBitExt,
            Self::eRayTracingOpacityMicromapBitExt => RawPipelineCreateFlagBits::eRayTracingOpacityMicromapBitExt,
            Self::eColourAttachmentFeedbackLoopBitExt => RawPipelineCreateFlagBits::eColourAttachmentFeedbackLoopBitExt,
            Self::eDepthStencilAttachmentFeedbackLoopBitExt => RawPipelineCreateFlagBits::eDepthStencilAttachmentFeedbackLoopBitExt,
            Self::eNoProtectedAccessBitExt => RawPipelineCreateFlagBits::eNoProtectedAccessBitExt,
            Self::eDescriptorBufferBitExt => RawPipelineCreateFlagBits::eDescriptorBufferBitExt,
            Self::eProtectedAccessOnlyBitExt => RawPipelineCreateFlagBits::eProtectedAccessOnlyBitExt,
            Self::eUnknownBit(b) => RawPipelineCreateFlagBits(b),
        }
    }
}

impl core::convert::From<PipelineCreateFlagBits> for PipelineCreateFlags {
    fn from(value: PipelineCreateFlagBits) -> Self {
        match value {
            PipelineCreateFlagBits::eDisableOptimizationBit => PipelineCreateFlags::eDisableOptimizationBit,
            PipelineCreateFlagBits::eAllowDerivativesBit => PipelineCreateFlags::eAllowDerivativesBit,
            PipelineCreateFlagBits::eDerivativeBit => PipelineCreateFlags::eDerivativeBit,
            PipelineCreateFlagBits::eViewIndexFromDeviceIndexBit => PipelineCreateFlags::eViewIndexFromDeviceIndexBit,
            PipelineCreateFlagBits::eDispatchBaseBit => PipelineCreateFlags::eDispatchBaseBit,
            PipelineCreateFlagBits::eDeferCompileBitNv => PipelineCreateFlags::eDeferCompileBitNv,
            PipelineCreateFlagBits::eCaptureStatisticsBitKhr => PipelineCreateFlags::eCaptureStatisticsBitKhr,
            PipelineCreateFlagBits::eCaptureInternalRepresentationsBitKhr => PipelineCreateFlags::eCaptureInternalRepresentationsBitKhr,
            PipelineCreateFlagBits::eFailOnPipelineCompileRequiredBit => PipelineCreateFlags::eFailOnPipelineCompileRequiredBit,
            PipelineCreateFlagBits::eEarlyReturnOnFailureBit => PipelineCreateFlags::eEarlyReturnOnFailureBit,
            PipelineCreateFlagBits::eLinkTimeOptimizationBitExt => PipelineCreateFlags::eLinkTimeOptimizationBitExt,
            PipelineCreateFlagBits::eLibraryBitKhr => PipelineCreateFlags::eLibraryBitKhr,
            PipelineCreateFlagBits::eRayTracingSkipTrianglesBitKhr => PipelineCreateFlags::eRayTracingSkipTrianglesBitKhr,
            PipelineCreateFlagBits::eRayTracingSkipAabbsBitKhr => PipelineCreateFlags::eRayTracingSkipAabbsBitKhr,
            PipelineCreateFlagBits::eRayTracingNoNullAnyHitShadersBitKhr => PipelineCreateFlags::eRayTracingNoNullAnyHitShadersBitKhr,
            PipelineCreateFlagBits::eRayTracingNoNullClosestHitShadersBitKhr => PipelineCreateFlags::eRayTracingNoNullClosestHitShadersBitKhr,
            PipelineCreateFlagBits::eRayTracingNoNullMissShadersBitKhr => PipelineCreateFlags::eRayTracingNoNullMissShadersBitKhr,
            PipelineCreateFlagBits::eRayTracingNoNullIntersectionShadersBitKhr => PipelineCreateFlags::eRayTracingNoNullIntersectionShadersBitKhr,
            PipelineCreateFlagBits::eIndirectBindableBitNv => PipelineCreateFlags::eIndirectBindableBitNv,
            PipelineCreateFlagBits::eRayTracingShaderGroupHandleCaptureReplayBitKhr => PipelineCreateFlags::eRayTracingShaderGroupHandleCaptureReplayBitKhr,
            PipelineCreateFlagBits::eRayTracingAllowMotionBitNv => PipelineCreateFlags::eRayTracingAllowMotionBitNv,
            PipelineCreateFlagBits::eRenderingFragmentShadingRateAttachmentBitKhr => PipelineCreateFlags::eRenderingFragmentShadingRateAttachmentBitKhr,
            PipelineCreateFlagBits::eRenderingFragmentDensityMapAttachmentBitExt => PipelineCreateFlags::eRenderingFragmentDensityMapAttachmentBitExt,
            PipelineCreateFlagBits::eRetainLinkTimeOptimizationInfoBitExt => PipelineCreateFlags::eRetainLinkTimeOptimizationInfoBitExt,
            PipelineCreateFlagBits::eRayTracingOpacityMicromapBitExt => PipelineCreateFlags::eRayTracingOpacityMicromapBitExt,
            PipelineCreateFlagBits::eColourAttachmentFeedbackLoopBitExt => PipelineCreateFlags::eColourAttachmentFeedbackLoopBitExt,
            PipelineCreateFlagBits::eDepthStencilAttachmentFeedbackLoopBitExt => PipelineCreateFlags::eDepthStencilAttachmentFeedbackLoopBitExt,
            PipelineCreateFlagBits::eNoProtectedAccessBitExt => PipelineCreateFlags::eNoProtectedAccessBitExt,
            PipelineCreateFlagBits::eDescriptorBufferBitExt => PipelineCreateFlags::eDescriptorBufferBitExt,
            PipelineCreateFlagBits::eProtectedAccessOnlyBitExt => PipelineCreateFlags::eProtectedAccessOnlyBitExt,
            PipelineCreateFlagBits::eUnknownBit(b) => PipelineCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(PipelineCreateFlagBits, PipelineCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eDisableOptimizationBit: Self = Self(0b1);
    pub const eAllowDerivativesBit: Self = Self(0b10);
    pub const eDerivativeBit: Self = Self(0b100);
    pub const eViewIndexFromDeviceIndexBit: Self = Self(0b1000);
    pub const eDispatchBaseBit: Self = Self(0b10000);
    pub const eDeferCompileBitNv: Self = Self(0b100000);
    pub const eCaptureStatisticsBitKhr: Self = Self(0b1000000);
    pub const eCaptureInternalRepresentationsBitKhr: Self = Self(0b10000000);
    pub const eFailOnPipelineCompileRequiredBit: Self = Self(0b100000000);
    pub const eEarlyReturnOnFailureBit: Self = Self(0b1000000000);
    pub const eLinkTimeOptimizationBitExt: Self = Self(0b10000000000);
    pub const eLibraryBitKhr: Self = Self(0b100000000000);
    pub const eRayTracingSkipTrianglesBitKhr: Self = Self(0b1000000000000);
    pub const eRayTracingSkipAabbsBitKhr: Self = Self(0b10000000000000);
    pub const eRayTracingNoNullAnyHitShadersBitKhr: Self = Self(0b100000000000000);
    pub const eRayTracingNoNullClosestHitShadersBitKhr: Self = Self(0b1000000000000000);
    pub const eRayTracingNoNullMissShadersBitKhr: Self = Self(0b10000000000000000);
    pub const eRayTracingNoNullIntersectionShadersBitKhr: Self = Self(0b100000000000000000);
    pub const eIndirectBindableBitNv: Self = Self(0b1000000000000000000);
    pub const eRayTracingShaderGroupHandleCaptureReplayBitKhr: Self = Self(0b10000000000000000000);
    pub const eRayTracingAllowMotionBitNv: Self = Self(0b100000000000000000000);
    pub const eRenderingFragmentShadingRateAttachmentBitKhr: Self = Self(0b1000000000000000000000);
    pub const eRenderingFragmentDensityMapAttachmentBitExt: Self = Self(0b10000000000000000000000);
    pub const eRetainLinkTimeOptimizationInfoBitExt: Self = Self(0b100000000000000000000000);
    pub const eRayTracingOpacityMicromapBitExt: Self = Self(0b1000000000000000000000000);
    pub const eColourAttachmentFeedbackLoopBitExt: Self = Self(0b10000000000000000000000000);
    pub const eDepthStencilAttachmentFeedbackLoopBitExt: Self = Self(0b100000000000000000000000000);
    pub const eNoProtectedAccessBitExt: Self = Self(0b1000000000000000000000000000);
    pub const eDescriptorBufferBitExt: Self = Self(0b100000000000000000000000000000);
    pub const eProtectedAccessOnlyBitExt: Self = Self(0b1000000000000000000000000000000);
    pub const eViewIndexFromDeviceIndexBitKhr: Self = Self::eViewIndexFromDeviceIndexBit;
    pub const eDispatchBase: Self = Self::eDispatchBaseBit;
    pub const eFailOnPipelineCompileRequiredBitExt: Self = Self::eFailOnPipelineCompileRequiredBit;
    pub const eEarlyReturnOnFailureBitExt: Self = Self::eEarlyReturnOnFailureBit;
    pub const eRasterizationStateCreateFragmentShadingRateAttachmentBitKhr: Self = Self::eRenderingFragmentShadingRateAttachmentBitKhr;
    pub const eRasterizationStateCreateFragmentDensityMapAttachmentBitExt: Self = Self::eRenderingFragmentDensityMapAttachmentBitExt;
}

bit_field_mask_derives!(PipelineCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(PipelineCreateFlagBits, PipelineCreateFlags, u32, PipelineCreateFlagBits::eDisableOptimizationBit, PipelineCreateFlagBits::eAllowDerivativesBit, PipelineCreateFlagBits::eDerivativeBit, PipelineCreateFlagBits::eViewIndexFromDeviceIndexBit, PipelineCreateFlagBits::eDispatchBaseBit, PipelineCreateFlagBits::eDeferCompileBitNv, PipelineCreateFlagBits::eCaptureStatisticsBitKhr, PipelineCreateFlagBits::eCaptureInternalRepresentationsBitKhr, PipelineCreateFlagBits::eFailOnPipelineCompileRequiredBit, PipelineCreateFlagBits::eEarlyReturnOnFailureBit, PipelineCreateFlagBits::eLinkTimeOptimizationBitExt, PipelineCreateFlagBits::eLibraryBitKhr, PipelineCreateFlagBits::eRayTracingSkipTrianglesBitKhr, PipelineCreateFlagBits::eRayTracingSkipAabbsBitKhr, PipelineCreateFlagBits::eRayTracingNoNullAnyHitShadersBitKhr, PipelineCreateFlagBits::eRayTracingNoNullClosestHitShadersBitKhr, PipelineCreateFlagBits::eRayTracingNoNullMissShadersBitKhr, PipelineCreateFlagBits::eRayTracingNoNullIntersectionShadersBitKhr, PipelineCreateFlagBits::eIndirectBindableBitNv, PipelineCreateFlagBits::eRayTracingShaderGroupHandleCaptureReplayBitKhr, PipelineCreateFlagBits::eRayTracingAllowMotionBitNv, PipelineCreateFlagBits::eRenderingFragmentShadingRateAttachmentBitKhr, PipelineCreateFlagBits::eRenderingFragmentDensityMapAttachmentBitExt, PipelineCreateFlagBits::eRetainLinkTimeOptimizationInfoBitExt, PipelineCreateFlagBits::eRayTracingOpacityMicromapBitExt, PipelineCreateFlagBits::eColourAttachmentFeedbackLoopBitExt, PipelineCreateFlagBits::eDepthStencilAttachmentFeedbackLoopBitExt, PipelineCreateFlagBits::eNoProtectedAccessBitExt, PipelineCreateFlagBits::eDescriptorBufferBitExt, PipelineCreateFlagBits::eProtectedAccessOnlyBitExt);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ColourComponentFlagBits {
    eRBit,
    eGBit,
    eBBit,
    eABit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl ColourComponentFlagBits {
    pub fn into_raw(self) -> RawColourComponentFlagBits {
        match self {
            Self::eRBit => RawColourComponentFlagBits::eRBit,
            Self::eGBit => RawColourComponentFlagBits::eGBit,
            Self::eBBit => RawColourComponentFlagBits::eBBit,
            Self::eABit => RawColourComponentFlagBits::eABit,
            Self::eUnknownBit(b) => RawColourComponentFlagBits(b),
        }
    }
}

impl core::convert::From<ColourComponentFlagBits> for ColourComponentFlags {
    fn from(value: ColourComponentFlagBits) -> Self {
        match value {
            ColourComponentFlagBits::eRBit => ColourComponentFlags::eRBit,
            ColourComponentFlagBits::eGBit => ColourComponentFlags::eGBit,
            ColourComponentFlagBits::eBBit => ColourComponentFlags::eBBit,
            ColourComponentFlagBits::eABit => ColourComponentFlags::eABit,
            ColourComponentFlagBits::eUnknownBit(b) => ColourComponentFlags(b),
        }
    }
}

bit_field_enum_derives!(ColourComponentFlagBits, ColourComponentFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ColourComponentFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ColourComponentFlags {
    pub const eNone: Self = Self(0);
    pub const eRBit: Self = Self(0b1);
    pub const eGBit: Self = Self(0b10);
    pub const eBBit: Self = Self(0b100);
    pub const eABit: Self = Self(0b1000);
}

bit_field_mask_derives!(ColourComponentFlags, u32);

bit_field_mask_with_enum_debug_derive!(ColourComponentFlagBits, ColourComponentFlags, u32, ColourComponentFlagBits::eRBit, ColourComponentFlagBits::eGBit, ColourComponentFlagBits::eBBit, ColourComponentFlagBits::eABit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum FenceCreateFlagBits {
    eSignaledBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl FenceCreateFlagBits {
    pub fn into_raw(self) -> RawFenceCreateFlagBits {
        match self {
            Self::eSignaledBit => RawFenceCreateFlagBits::eSignaledBit,
            Self::eUnknownBit(b) => RawFenceCreateFlagBits(b),
        }
    }
}

impl core::convert::From<FenceCreateFlagBits> for FenceCreateFlags {
    fn from(value: FenceCreateFlagBits) -> Self {
        match value {
            FenceCreateFlagBits::eSignaledBit => FenceCreateFlags::eSignaledBit,
            FenceCreateFlagBits::eUnknownBit(b) => FenceCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(FenceCreateFlagBits, FenceCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct FenceCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl FenceCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eSignaledBit: Self = Self(0b1);
}

bit_field_mask_derives!(FenceCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(FenceCreateFlagBits, FenceCreateFlags, u32, FenceCreateFlagBits::eSignaledBit);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct SemaphoreCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl SemaphoreCreateFlags {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(SemaphoreCreateFlags, u32);

fieldless_debug_derive!(SemaphoreCreateFlags);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum FormatFeatureFlagBits {
    eSampledImageBit,
    eStorageImageBit,
    eStorageImageAtomicBit,
    eUniformTexelBufferBit,
    eStorageTexelBufferBit,
    eStorageTexelBufferAtomicBit,
    eVertexBufferBit,
    eColourAttachmentBit,
    eColourAttachmentBlendBit,
    eDepthStencilAttachmentBit,
    eBlitSrcBit,
    eBlitDstBit,
    eSampledImageFilterLinearBit,
    eSampledImageFilterCubicBitExt,
    eTransferSrcBit,
    eTransferDstBit,
    eSampledImageFilterMinmaxBit,
    eMidpointChromaSamplesBit,
    eSampledImageYcbcrConversionLinearFilterBit,
    eSampledImageYcbcrConversionSeparateReconstructionFilterBit,
    eSampledImageYcbcrConversionChromaReconstructionExplicitBit,
    eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit,
    eDisjointBit,
    eCositedChromaSamplesBit,
    eFragmentDensityMapBitExt,
    eVideoDecodeOutputBitKhr,
    eVideoDecodeDpbBitKhr,
    eVideoEncodeInputBitKhr,
    eVideoEncodeDpbBitKhr,
    eAccelerationStructureVertexBufferBitKhr,
    eFragmentShadingRateAttachmentBitKhr,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl FormatFeatureFlagBits {
    pub const eTransferSrcBitKhr: Self = Self::eTransferSrcBit;
    pub const eTransferDstBitKhr: Self = Self::eTransferDstBit;
    pub const eSampledImageFilterMinmaxBitExt: Self = Self::eSampledImageFilterMinmaxBit;
    pub const eMidpointChromaSamplesBitKhr: Self = Self::eMidpointChromaSamplesBit;
    pub const eSampledImageYcbcrConversionLinearFilterBitKhr: Self = Self::eSampledImageYcbcrConversionLinearFilterBit;
    pub const eSampledImageYcbcrConversionSeparateReconstructionFilterBitKhr: Self = Self::eSampledImageYcbcrConversionSeparateReconstructionFilterBit;
    pub const eSampledImageYcbcrConversionChromaReconstructionExplicitBitKhr: Self = Self::eSampledImageYcbcrConversionChromaReconstructionExplicitBit;
    pub const eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBitKhr: Self = Self::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit;
    pub const eDisjointBitKhr: Self = Self::eDisjointBit;
    pub const eCositedChromaSamplesBitKhr: Self = Self::eCositedChromaSamplesBit;

    pub fn into_raw(self) -> RawFormatFeatureFlagBits {
        match self {
            Self::eSampledImageBit => RawFormatFeatureFlagBits::eSampledImageBit,
            Self::eStorageImageBit => RawFormatFeatureFlagBits::eStorageImageBit,
            Self::eStorageImageAtomicBit => RawFormatFeatureFlagBits::eStorageImageAtomicBit,
            Self::eUniformTexelBufferBit => RawFormatFeatureFlagBits::eUniformTexelBufferBit,
            Self::eStorageTexelBufferBit => RawFormatFeatureFlagBits::eStorageTexelBufferBit,
            Self::eStorageTexelBufferAtomicBit => RawFormatFeatureFlagBits::eStorageTexelBufferAtomicBit,
            Self::eVertexBufferBit => RawFormatFeatureFlagBits::eVertexBufferBit,
            Self::eColourAttachmentBit => RawFormatFeatureFlagBits::eColourAttachmentBit,
            Self::eColourAttachmentBlendBit => RawFormatFeatureFlagBits::eColourAttachmentBlendBit,
            Self::eDepthStencilAttachmentBit => RawFormatFeatureFlagBits::eDepthStencilAttachmentBit,
            Self::eBlitSrcBit => RawFormatFeatureFlagBits::eBlitSrcBit,
            Self::eBlitDstBit => RawFormatFeatureFlagBits::eBlitDstBit,
            Self::eSampledImageFilterLinearBit => RawFormatFeatureFlagBits::eSampledImageFilterLinearBit,
            Self::eSampledImageFilterCubicBitExt => RawFormatFeatureFlagBits::eSampledImageFilterCubicBitExt,
            Self::eTransferSrcBit => RawFormatFeatureFlagBits::eTransferSrcBit,
            Self::eTransferDstBit => RawFormatFeatureFlagBits::eTransferDstBit,
            Self::eSampledImageFilterMinmaxBit => RawFormatFeatureFlagBits::eSampledImageFilterMinmaxBit,
            Self::eMidpointChromaSamplesBit => RawFormatFeatureFlagBits::eMidpointChromaSamplesBit,
            Self::eSampledImageYcbcrConversionLinearFilterBit => RawFormatFeatureFlagBits::eSampledImageYcbcrConversionLinearFilterBit,
            Self::eSampledImageYcbcrConversionSeparateReconstructionFilterBit => RawFormatFeatureFlagBits::eSampledImageYcbcrConversionSeparateReconstructionFilterBit,
            Self::eSampledImageYcbcrConversionChromaReconstructionExplicitBit => RawFormatFeatureFlagBits::eSampledImageYcbcrConversionChromaReconstructionExplicitBit,
            Self::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit => RawFormatFeatureFlagBits::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit,
            Self::eDisjointBit => RawFormatFeatureFlagBits::eDisjointBit,
            Self::eCositedChromaSamplesBit => RawFormatFeatureFlagBits::eCositedChromaSamplesBit,
            Self::eFragmentDensityMapBitExt => RawFormatFeatureFlagBits::eFragmentDensityMapBitExt,
            Self::eVideoDecodeOutputBitKhr => RawFormatFeatureFlagBits::eVideoDecodeOutputBitKhr,
            Self::eVideoDecodeDpbBitKhr => RawFormatFeatureFlagBits::eVideoDecodeDpbBitKhr,
            Self::eVideoEncodeInputBitKhr => RawFormatFeatureFlagBits::eVideoEncodeInputBitKhr,
            Self::eVideoEncodeDpbBitKhr => RawFormatFeatureFlagBits::eVideoEncodeDpbBitKhr,
            Self::eAccelerationStructureVertexBufferBitKhr => RawFormatFeatureFlagBits::eAccelerationStructureVertexBufferBitKhr,
            Self::eFragmentShadingRateAttachmentBitKhr => RawFormatFeatureFlagBits::eFragmentShadingRateAttachmentBitKhr,
            Self::eUnknownBit(b) => RawFormatFeatureFlagBits(b),
        }
    }
}

impl core::convert::From<FormatFeatureFlagBits> for FormatFeatureFlags {
    fn from(value: FormatFeatureFlagBits) -> Self {
        match value {
            FormatFeatureFlagBits::eSampledImageBit => FormatFeatureFlags::eSampledImageBit,
            FormatFeatureFlagBits::eStorageImageBit => FormatFeatureFlags::eStorageImageBit,
            FormatFeatureFlagBits::eStorageImageAtomicBit => FormatFeatureFlags::eStorageImageAtomicBit,
            FormatFeatureFlagBits::eUniformTexelBufferBit => FormatFeatureFlags::eUniformTexelBufferBit,
            FormatFeatureFlagBits::eStorageTexelBufferBit => FormatFeatureFlags::eStorageTexelBufferBit,
            FormatFeatureFlagBits::eStorageTexelBufferAtomicBit => FormatFeatureFlags::eStorageTexelBufferAtomicBit,
            FormatFeatureFlagBits::eVertexBufferBit => FormatFeatureFlags::eVertexBufferBit,
            FormatFeatureFlagBits::eColourAttachmentBit => FormatFeatureFlags::eColourAttachmentBit,
            FormatFeatureFlagBits::eColourAttachmentBlendBit => FormatFeatureFlags::eColourAttachmentBlendBit,
            FormatFeatureFlagBits::eDepthStencilAttachmentBit => FormatFeatureFlags::eDepthStencilAttachmentBit,
            FormatFeatureFlagBits::eBlitSrcBit => FormatFeatureFlags::eBlitSrcBit,
            FormatFeatureFlagBits::eBlitDstBit => FormatFeatureFlags::eBlitDstBit,
            FormatFeatureFlagBits::eSampledImageFilterLinearBit => FormatFeatureFlags::eSampledImageFilterLinearBit,
            FormatFeatureFlagBits::eSampledImageFilterCubicBitExt => FormatFeatureFlags::eSampledImageFilterCubicBitExt,
            FormatFeatureFlagBits::eTransferSrcBit => FormatFeatureFlags::eTransferSrcBit,
            FormatFeatureFlagBits::eTransferDstBit => FormatFeatureFlags::eTransferDstBit,
            FormatFeatureFlagBits::eSampledImageFilterMinmaxBit => FormatFeatureFlags::eSampledImageFilterMinmaxBit,
            FormatFeatureFlagBits::eMidpointChromaSamplesBit => FormatFeatureFlags::eMidpointChromaSamplesBit,
            FormatFeatureFlagBits::eSampledImageYcbcrConversionLinearFilterBit => FormatFeatureFlags::eSampledImageYcbcrConversionLinearFilterBit,
            FormatFeatureFlagBits::eSampledImageYcbcrConversionSeparateReconstructionFilterBit => FormatFeatureFlags::eSampledImageYcbcrConversionSeparateReconstructionFilterBit,
            FormatFeatureFlagBits::eSampledImageYcbcrConversionChromaReconstructionExplicitBit => FormatFeatureFlags::eSampledImageYcbcrConversionChromaReconstructionExplicitBit,
            FormatFeatureFlagBits::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit => FormatFeatureFlags::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit,
            FormatFeatureFlagBits::eDisjointBit => FormatFeatureFlags::eDisjointBit,
            FormatFeatureFlagBits::eCositedChromaSamplesBit => FormatFeatureFlags::eCositedChromaSamplesBit,
            FormatFeatureFlagBits::eFragmentDensityMapBitExt => FormatFeatureFlags::eFragmentDensityMapBitExt,
            FormatFeatureFlagBits::eVideoDecodeOutputBitKhr => FormatFeatureFlags::eVideoDecodeOutputBitKhr,
            FormatFeatureFlagBits::eVideoDecodeDpbBitKhr => FormatFeatureFlags::eVideoDecodeDpbBitKhr,
            FormatFeatureFlagBits::eVideoEncodeInputBitKhr => FormatFeatureFlags::eVideoEncodeInputBitKhr,
            FormatFeatureFlagBits::eVideoEncodeDpbBitKhr => FormatFeatureFlags::eVideoEncodeDpbBitKhr,
            FormatFeatureFlagBits::eAccelerationStructureVertexBufferBitKhr => FormatFeatureFlags::eAccelerationStructureVertexBufferBitKhr,
            FormatFeatureFlagBits::eFragmentShadingRateAttachmentBitKhr => FormatFeatureFlags::eFragmentShadingRateAttachmentBitKhr,
            FormatFeatureFlagBits::eUnknownBit(b) => FormatFeatureFlags(b),
        }
    }
}

bit_field_enum_derives!(FormatFeatureFlagBits, FormatFeatureFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct FormatFeatureFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl FormatFeatureFlags {
    pub const eNone: Self = Self(0);
    pub const eSampledImageBit: Self = Self(0b1);
    pub const eStorageImageBit: Self = Self(0b10);
    pub const eStorageImageAtomicBit: Self = Self(0b100);
    pub const eUniformTexelBufferBit: Self = Self(0b1000);
    pub const eStorageTexelBufferBit: Self = Self(0b10000);
    pub const eStorageTexelBufferAtomicBit: Self = Self(0b100000);
    pub const eVertexBufferBit: Self = Self(0b1000000);
    pub const eColourAttachmentBit: Self = Self(0b10000000);
    pub const eColourAttachmentBlendBit: Self = Self(0b100000000);
    pub const eDepthStencilAttachmentBit: Self = Self(0b1000000000);
    pub const eBlitSrcBit: Self = Self(0b10000000000);
    pub const eBlitDstBit: Self = Self(0b100000000000);
    pub const eSampledImageFilterLinearBit: Self = Self(0b1000000000000);
    pub const eSampledImageFilterCubicBitExt: Self = Self(0b10000000000000);
    pub const eTransferSrcBit: Self = Self(0b100000000000000);
    pub const eTransferDstBit: Self = Self(0b1000000000000000);
    pub const eSampledImageFilterMinmaxBit: Self = Self(0b10000000000000000);
    pub const eMidpointChromaSamplesBit: Self = Self(0b100000000000000000);
    pub const eSampledImageYcbcrConversionLinearFilterBit: Self = Self(0b1000000000000000000);
    pub const eSampledImageYcbcrConversionSeparateReconstructionFilterBit: Self = Self(0b10000000000000000000);
    pub const eSampledImageYcbcrConversionChromaReconstructionExplicitBit: Self = Self(0b100000000000000000000);
    pub const eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit: Self = Self(0b1000000000000000000000);
    pub const eDisjointBit: Self = Self(0b10000000000000000000000);
    pub const eCositedChromaSamplesBit: Self = Self(0b100000000000000000000000);
    pub const eFragmentDensityMapBitExt: Self = Self(0b1000000000000000000000000);
    pub const eVideoDecodeOutputBitKhr: Self = Self(0b10000000000000000000000000);
    pub const eVideoDecodeDpbBitKhr: Self = Self(0b100000000000000000000000000);
    pub const eVideoEncodeInputBitKhr: Self = Self(0b1000000000000000000000000000);
    pub const eVideoEncodeDpbBitKhr: Self = Self(0b10000000000000000000000000000);
    pub const eAccelerationStructureVertexBufferBitKhr: Self = Self(0b100000000000000000000000000000);
    pub const eFragmentShadingRateAttachmentBitKhr: Self = Self(0b1000000000000000000000000000000);
    pub const eTransferSrcBitKhr: Self = Self::eTransferSrcBit;
    pub const eTransferDstBitKhr: Self = Self::eTransferDstBit;
    pub const eSampledImageFilterMinmaxBitExt: Self = Self::eSampledImageFilterMinmaxBit;
    pub const eMidpointChromaSamplesBitKhr: Self = Self::eMidpointChromaSamplesBit;
    pub const eSampledImageYcbcrConversionLinearFilterBitKhr: Self = Self::eSampledImageYcbcrConversionLinearFilterBit;
    pub const eSampledImageYcbcrConversionSeparateReconstructionFilterBitKhr: Self = Self::eSampledImageYcbcrConversionSeparateReconstructionFilterBit;
    pub const eSampledImageYcbcrConversionChromaReconstructionExplicitBitKhr: Self = Self::eSampledImageYcbcrConversionChromaReconstructionExplicitBit;
    pub const eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBitKhr: Self = Self::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit;
    pub const eDisjointBitKhr: Self = Self::eDisjointBit;
    pub const eCositedChromaSamplesBitKhr: Self = Self::eCositedChromaSamplesBit;
}

bit_field_mask_derives!(FormatFeatureFlags, u32);

bit_field_mask_with_enum_debug_derive!(FormatFeatureFlagBits, FormatFeatureFlags, u32, FormatFeatureFlagBits::eSampledImageBit, FormatFeatureFlagBits::eStorageImageBit, FormatFeatureFlagBits::eStorageImageAtomicBit, FormatFeatureFlagBits::eUniformTexelBufferBit, FormatFeatureFlagBits::eStorageTexelBufferBit, FormatFeatureFlagBits::eStorageTexelBufferAtomicBit, FormatFeatureFlagBits::eVertexBufferBit, FormatFeatureFlagBits::eColourAttachmentBit, FormatFeatureFlagBits::eColourAttachmentBlendBit, FormatFeatureFlagBits::eDepthStencilAttachmentBit, FormatFeatureFlagBits::eBlitSrcBit, FormatFeatureFlagBits::eBlitDstBit, FormatFeatureFlagBits::eSampledImageFilterLinearBit, FormatFeatureFlagBits::eSampledImageFilterCubicBitExt, FormatFeatureFlagBits::eTransferSrcBit, FormatFeatureFlagBits::eTransferDstBit, FormatFeatureFlagBits::eSampledImageFilterMinmaxBit, FormatFeatureFlagBits::eMidpointChromaSamplesBit, FormatFeatureFlagBits::eSampledImageYcbcrConversionLinearFilterBit, FormatFeatureFlagBits::eSampledImageYcbcrConversionSeparateReconstructionFilterBit, FormatFeatureFlagBits::eSampledImageYcbcrConversionChromaReconstructionExplicitBit, FormatFeatureFlagBits::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit, FormatFeatureFlagBits::eDisjointBit, FormatFeatureFlagBits::eCositedChromaSamplesBit, FormatFeatureFlagBits::eFragmentDensityMapBitExt, FormatFeatureFlagBits::eVideoDecodeOutputBitKhr, FormatFeatureFlagBits::eVideoDecodeDpbBitKhr, FormatFeatureFlagBits::eVideoEncodeInputBitKhr, FormatFeatureFlagBits::eVideoEncodeDpbBitKhr, FormatFeatureFlagBits::eAccelerationStructureVertexBufferBitKhr, FormatFeatureFlagBits::eFragmentShadingRateAttachmentBitKhr);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum QueryControlFlagBits {
    ePreciseBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl QueryControlFlagBits {
    pub fn into_raw(self) -> RawQueryControlFlagBits {
        match self {
            Self::ePreciseBit => RawQueryControlFlagBits::ePreciseBit,
            Self::eUnknownBit(b) => RawQueryControlFlagBits(b),
        }
    }
}

impl core::convert::From<QueryControlFlagBits> for QueryControlFlags {
    fn from(value: QueryControlFlagBits) -> Self {
        match value {
            QueryControlFlagBits::ePreciseBit => QueryControlFlags::ePreciseBit,
            QueryControlFlagBits::eUnknownBit(b) => QueryControlFlags(b),
        }
    }
}

bit_field_enum_derives!(QueryControlFlagBits, QueryControlFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct QueryControlFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl QueryControlFlags {
    pub const eNone: Self = Self(0);
    pub const ePreciseBit: Self = Self(0b1);
}

bit_field_mask_derives!(QueryControlFlags, u32);

bit_field_mask_with_enum_debug_derive!(QueryControlFlagBits, QueryControlFlags, u32, QueryControlFlagBits::ePreciseBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum QueryResultFlagBits {
    e64Bit,
    eWaitBit,
    eWithAvailabilityBit,
    ePartialBit,
    eWithStatusBitKhr,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl QueryResultFlagBits {
    pub fn into_raw(self) -> RawQueryResultFlagBits {
        match self {
            Self::e64Bit => RawQueryResultFlagBits::e64Bit,
            Self::eWaitBit => RawQueryResultFlagBits::eWaitBit,
            Self::eWithAvailabilityBit => RawQueryResultFlagBits::eWithAvailabilityBit,
            Self::ePartialBit => RawQueryResultFlagBits::ePartialBit,
            Self::eWithStatusBitKhr => RawQueryResultFlagBits::eWithStatusBitKhr,
            Self::eUnknownBit(b) => RawQueryResultFlagBits(b),
        }
    }
}

impl core::convert::From<QueryResultFlagBits> for QueryResultFlags {
    fn from(value: QueryResultFlagBits) -> Self {
        match value {
            QueryResultFlagBits::e64Bit => QueryResultFlags::e64Bit,
            QueryResultFlagBits::eWaitBit => QueryResultFlags::eWaitBit,
            QueryResultFlagBits::eWithAvailabilityBit => QueryResultFlags::eWithAvailabilityBit,
            QueryResultFlagBits::ePartialBit => QueryResultFlags::ePartialBit,
            QueryResultFlagBits::eWithStatusBitKhr => QueryResultFlags::eWithStatusBitKhr,
            QueryResultFlagBits::eUnknownBit(b) => QueryResultFlags(b),
        }
    }
}

bit_field_enum_derives!(QueryResultFlagBits, QueryResultFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct QueryResultFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl QueryResultFlags {
    pub const eNone: Self = Self(0);
    pub const e64Bit: Self = Self(0b1);
    pub const eWaitBit: Self = Self(0b10);
    pub const eWithAvailabilityBit: Self = Self(0b100);
    pub const ePartialBit: Self = Self(0b1000);
    pub const eWithStatusBitKhr: Self = Self(0b10000);
}

bit_field_mask_derives!(QueryResultFlags, u32);

bit_field_mask_with_enum_debug_derive!(QueryResultFlagBits, QueryResultFlags, u32, QueryResultFlagBits::e64Bit, QueryResultFlagBits::eWaitBit, QueryResultFlagBits::eWithAvailabilityBit, QueryResultFlagBits::ePartialBit, QueryResultFlagBits::eWithStatusBitKhr);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ShaderModuleCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ShaderModuleCreateFlags {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(ShaderModuleCreateFlags, u32);

fieldless_debug_derive!(ShaderModuleCreateFlags);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum EventCreateFlagBits {
    eDeviceOnlyBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl EventCreateFlagBits {
    pub const eDeviceOnlyBitKhr: Self = Self::eDeviceOnlyBit;

    pub fn into_raw(self) -> RawEventCreateFlagBits {
        match self {
            Self::eDeviceOnlyBit => RawEventCreateFlagBits::eDeviceOnlyBit,
            Self::eUnknownBit(b) => RawEventCreateFlagBits(b),
        }
    }
}

impl core::convert::From<EventCreateFlagBits> for EventCreateFlags {
    fn from(value: EventCreateFlagBits) -> Self {
        match value {
            EventCreateFlagBits::eDeviceOnlyBit => EventCreateFlags::eDeviceOnlyBit,
            EventCreateFlagBits::eUnknownBit(b) => EventCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(EventCreateFlagBits, EventCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct EventCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl EventCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eDeviceOnlyBit: Self = Self(0b1);
    pub const eDeviceOnlyBitKhr: Self = Self::eDeviceOnlyBit;
}

bit_field_mask_derives!(EventCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(EventCreateFlagBits, EventCreateFlags, u32, EventCreateFlagBits::eDeviceOnlyBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum CommandPoolCreateFlagBits {
    eTransientBit,
    eResetCommandBufferBit,
    eProtectedBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl CommandPoolCreateFlagBits {
    pub fn into_raw(self) -> RawCommandPoolCreateFlagBits {
        match self {
            Self::eTransientBit => RawCommandPoolCreateFlagBits::eTransientBit,
            Self::eResetCommandBufferBit => RawCommandPoolCreateFlagBits::eResetCommandBufferBit,
            Self::eProtectedBit => RawCommandPoolCreateFlagBits::eProtectedBit,
            Self::eUnknownBit(b) => RawCommandPoolCreateFlagBits(b),
        }
    }
}

impl core::convert::From<CommandPoolCreateFlagBits> for CommandPoolCreateFlags {
    fn from(value: CommandPoolCreateFlagBits) -> Self {
        match value {
            CommandPoolCreateFlagBits::eTransientBit => CommandPoolCreateFlags::eTransientBit,
            CommandPoolCreateFlagBits::eResetCommandBufferBit => CommandPoolCreateFlags::eResetCommandBufferBit,
            CommandPoolCreateFlagBits::eProtectedBit => CommandPoolCreateFlags::eProtectedBit,
            CommandPoolCreateFlagBits::eUnknownBit(b) => CommandPoolCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(CommandPoolCreateFlagBits, CommandPoolCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct CommandPoolCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl CommandPoolCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eTransientBit: Self = Self(0b1);
    pub const eResetCommandBufferBit: Self = Self(0b10);
    pub const eProtectedBit: Self = Self(0b100);
}

bit_field_mask_derives!(CommandPoolCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(CommandPoolCreateFlagBits, CommandPoolCreateFlags, u32, CommandPoolCreateFlagBits::eTransientBit, CommandPoolCreateFlagBits::eResetCommandBufferBit, CommandPoolCreateFlagBits::eProtectedBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum CommandPoolResetFlagBits {
    eReleaseResourcesBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl CommandPoolResetFlagBits {
    pub fn into_raw(self) -> RawCommandPoolResetFlagBits {
        match self {
            Self::eReleaseResourcesBit => RawCommandPoolResetFlagBits::eReleaseResourcesBit,
            Self::eUnknownBit(b) => RawCommandPoolResetFlagBits(b),
        }
    }
}

impl core::convert::From<CommandPoolResetFlagBits> for CommandPoolResetFlags {
    fn from(value: CommandPoolResetFlagBits) -> Self {
        match value {
            CommandPoolResetFlagBits::eReleaseResourcesBit => CommandPoolResetFlags::eReleaseResourcesBit,
            CommandPoolResetFlagBits::eUnknownBit(b) => CommandPoolResetFlags(b),
        }
    }
}

bit_field_enum_derives!(CommandPoolResetFlagBits, CommandPoolResetFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct CommandPoolResetFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl CommandPoolResetFlags {
    pub const eNone: Self = Self(0);
    pub const eReleaseResourcesBit: Self = Self(0b1);
}

bit_field_mask_derives!(CommandPoolResetFlags, u32);

bit_field_mask_with_enum_debug_derive!(CommandPoolResetFlagBits, CommandPoolResetFlags, u32, CommandPoolResetFlagBits::eReleaseResourcesBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum CommandBufferResetFlagBits {
    eReleaseResourcesBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl CommandBufferResetFlagBits {
    pub fn into_raw(self) -> RawCommandBufferResetFlagBits {
        match self {
            Self::eReleaseResourcesBit => RawCommandBufferResetFlagBits::eReleaseResourcesBit,
            Self::eUnknownBit(b) => RawCommandBufferResetFlagBits(b),
        }
    }
}

impl core::convert::From<CommandBufferResetFlagBits> for CommandBufferResetFlags {
    fn from(value: CommandBufferResetFlagBits) -> Self {
        match value {
            CommandBufferResetFlagBits::eReleaseResourcesBit => CommandBufferResetFlags::eReleaseResourcesBit,
            CommandBufferResetFlagBits::eUnknownBit(b) => CommandBufferResetFlags(b),
        }
    }
}

bit_field_enum_derives!(CommandBufferResetFlagBits, CommandBufferResetFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct CommandBufferResetFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl CommandBufferResetFlags {
    pub const eNone: Self = Self(0);
    pub const eReleaseResourcesBit: Self = Self(0b1);
}

bit_field_mask_derives!(CommandBufferResetFlags, u32);

bit_field_mask_with_enum_debug_derive!(CommandBufferResetFlagBits, CommandBufferResetFlags, u32, CommandBufferResetFlagBits::eReleaseResourcesBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum CommandBufferUsageFlagBits {
    eOneTimeSubmitBit,
    eRenderPassContinueBit,
    eSimultaneousUseBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl CommandBufferUsageFlagBits {
    pub fn into_raw(self) -> RawCommandBufferUsageFlagBits {
        match self {
            Self::eOneTimeSubmitBit => RawCommandBufferUsageFlagBits::eOneTimeSubmitBit,
            Self::eRenderPassContinueBit => RawCommandBufferUsageFlagBits::eRenderPassContinueBit,
            Self::eSimultaneousUseBit => RawCommandBufferUsageFlagBits::eSimultaneousUseBit,
            Self::eUnknownBit(b) => RawCommandBufferUsageFlagBits(b),
        }
    }
}

impl core::convert::From<CommandBufferUsageFlagBits> for CommandBufferUsageFlags {
    fn from(value: CommandBufferUsageFlagBits) -> Self {
        match value {
            CommandBufferUsageFlagBits::eOneTimeSubmitBit => CommandBufferUsageFlags::eOneTimeSubmitBit,
            CommandBufferUsageFlagBits::eRenderPassContinueBit => CommandBufferUsageFlags::eRenderPassContinueBit,
            CommandBufferUsageFlagBits::eSimultaneousUseBit => CommandBufferUsageFlags::eSimultaneousUseBit,
            CommandBufferUsageFlagBits::eUnknownBit(b) => CommandBufferUsageFlags(b),
        }
    }
}

bit_field_enum_derives!(CommandBufferUsageFlagBits, CommandBufferUsageFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct CommandBufferUsageFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl CommandBufferUsageFlags {
    pub const eNone: Self = Self(0);
    pub const eOneTimeSubmitBit: Self = Self(0b1);
    pub const eRenderPassContinueBit: Self = Self(0b10);
    pub const eSimultaneousUseBit: Self = Self(0b100);
}

bit_field_mask_derives!(CommandBufferUsageFlags, u32);

bit_field_mask_with_enum_debug_derive!(CommandBufferUsageFlagBits, CommandBufferUsageFlags, u32, CommandBufferUsageFlagBits::eOneTimeSubmitBit, CommandBufferUsageFlagBits::eRenderPassContinueBit, CommandBufferUsageFlagBits::eSimultaneousUseBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum QueryPipelineStatisticFlagBits {
    eInputAssemblyVerticesBit,
    eInputAssemblyPrimitivesBit,
    eVertexShaderInvocationsBit,
    eGeometryShaderInvocationsBit,
    eGeometryShaderPrimitivesBit,
    eClippingInvocationsBit,
    eClippingPrimitivesBit,
    eFragmentShaderInvocationsBit,
    eTessellationControlShaderPatchesBit,
    eTessellationEvaluationShaderInvocationsBit,
    eComputeShaderInvocationsBit,
    eTaskShaderInvocationsBitExt,
    eMeshShaderInvocationsBitExt,
    eClusterCullingShaderInvocationsBitHuawei,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl QueryPipelineStatisticFlagBits {
    pub fn into_raw(self) -> RawQueryPipelineStatisticFlagBits {
        match self {
            Self::eInputAssemblyVerticesBit => RawQueryPipelineStatisticFlagBits::eInputAssemblyVerticesBit,
            Self::eInputAssemblyPrimitivesBit => RawQueryPipelineStatisticFlagBits::eInputAssemblyPrimitivesBit,
            Self::eVertexShaderInvocationsBit => RawQueryPipelineStatisticFlagBits::eVertexShaderInvocationsBit,
            Self::eGeometryShaderInvocationsBit => RawQueryPipelineStatisticFlagBits::eGeometryShaderInvocationsBit,
            Self::eGeometryShaderPrimitivesBit => RawQueryPipelineStatisticFlagBits::eGeometryShaderPrimitivesBit,
            Self::eClippingInvocationsBit => RawQueryPipelineStatisticFlagBits::eClippingInvocationsBit,
            Self::eClippingPrimitivesBit => RawQueryPipelineStatisticFlagBits::eClippingPrimitivesBit,
            Self::eFragmentShaderInvocationsBit => RawQueryPipelineStatisticFlagBits::eFragmentShaderInvocationsBit,
            Self::eTessellationControlShaderPatchesBit => RawQueryPipelineStatisticFlagBits::eTessellationControlShaderPatchesBit,
            Self::eTessellationEvaluationShaderInvocationsBit => RawQueryPipelineStatisticFlagBits::eTessellationEvaluationShaderInvocationsBit,
            Self::eComputeShaderInvocationsBit => RawQueryPipelineStatisticFlagBits::eComputeShaderInvocationsBit,
            Self::eTaskShaderInvocationsBitExt => RawQueryPipelineStatisticFlagBits::eTaskShaderInvocationsBitExt,
            Self::eMeshShaderInvocationsBitExt => RawQueryPipelineStatisticFlagBits::eMeshShaderInvocationsBitExt,
            Self::eClusterCullingShaderInvocationsBitHuawei => RawQueryPipelineStatisticFlagBits::eClusterCullingShaderInvocationsBitHuawei,
            Self::eUnknownBit(b) => RawQueryPipelineStatisticFlagBits(b),
        }
    }
}

impl core::convert::From<QueryPipelineStatisticFlagBits> for QueryPipelineStatisticFlags {
    fn from(value: QueryPipelineStatisticFlagBits) -> Self {
        match value {
            QueryPipelineStatisticFlagBits::eInputAssemblyVerticesBit => QueryPipelineStatisticFlags::eInputAssemblyVerticesBit,
            QueryPipelineStatisticFlagBits::eInputAssemblyPrimitivesBit => QueryPipelineStatisticFlags::eInputAssemblyPrimitivesBit,
            QueryPipelineStatisticFlagBits::eVertexShaderInvocationsBit => QueryPipelineStatisticFlags::eVertexShaderInvocationsBit,
            QueryPipelineStatisticFlagBits::eGeometryShaderInvocationsBit => QueryPipelineStatisticFlags::eGeometryShaderInvocationsBit,
            QueryPipelineStatisticFlagBits::eGeometryShaderPrimitivesBit => QueryPipelineStatisticFlags::eGeometryShaderPrimitivesBit,
            QueryPipelineStatisticFlagBits::eClippingInvocationsBit => QueryPipelineStatisticFlags::eClippingInvocationsBit,
            QueryPipelineStatisticFlagBits::eClippingPrimitivesBit => QueryPipelineStatisticFlags::eClippingPrimitivesBit,
            QueryPipelineStatisticFlagBits::eFragmentShaderInvocationsBit => QueryPipelineStatisticFlags::eFragmentShaderInvocationsBit,
            QueryPipelineStatisticFlagBits::eTessellationControlShaderPatchesBit => QueryPipelineStatisticFlags::eTessellationControlShaderPatchesBit,
            QueryPipelineStatisticFlagBits::eTessellationEvaluationShaderInvocationsBit => QueryPipelineStatisticFlags::eTessellationEvaluationShaderInvocationsBit,
            QueryPipelineStatisticFlagBits::eComputeShaderInvocationsBit => QueryPipelineStatisticFlags::eComputeShaderInvocationsBit,
            QueryPipelineStatisticFlagBits::eTaskShaderInvocationsBitExt => QueryPipelineStatisticFlags::eTaskShaderInvocationsBitExt,
            QueryPipelineStatisticFlagBits::eMeshShaderInvocationsBitExt => QueryPipelineStatisticFlags::eMeshShaderInvocationsBitExt,
            QueryPipelineStatisticFlagBits::eClusterCullingShaderInvocationsBitHuawei => QueryPipelineStatisticFlags::eClusterCullingShaderInvocationsBitHuawei,
            QueryPipelineStatisticFlagBits::eUnknownBit(b) => QueryPipelineStatisticFlags(b),
        }
    }
}

bit_field_enum_derives!(QueryPipelineStatisticFlagBits, QueryPipelineStatisticFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct QueryPipelineStatisticFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl QueryPipelineStatisticFlags {
    pub const eNone: Self = Self(0);
    pub const eInputAssemblyVerticesBit: Self = Self(0b1);
    pub const eInputAssemblyPrimitivesBit: Self = Self(0b10);
    pub const eVertexShaderInvocationsBit: Self = Self(0b100);
    pub const eGeometryShaderInvocationsBit: Self = Self(0b1000);
    pub const eGeometryShaderPrimitivesBit: Self = Self(0b10000);
    pub const eClippingInvocationsBit: Self = Self(0b100000);
    pub const eClippingPrimitivesBit: Self = Self(0b1000000);
    pub const eFragmentShaderInvocationsBit: Self = Self(0b10000000);
    pub const eTessellationControlShaderPatchesBit: Self = Self(0b100000000);
    pub const eTessellationEvaluationShaderInvocationsBit: Self = Self(0b1000000000);
    pub const eComputeShaderInvocationsBit: Self = Self(0b10000000000);
    pub const eTaskShaderInvocationsBitExt: Self = Self(0b100000000000);
    pub const eMeshShaderInvocationsBitExt: Self = Self(0b1000000000000);
    pub const eClusterCullingShaderInvocationsBitHuawei: Self = Self(0b10000000000000);
}

bit_field_mask_derives!(QueryPipelineStatisticFlags, u32);

bit_field_mask_with_enum_debug_derive!(QueryPipelineStatisticFlagBits, QueryPipelineStatisticFlags, u32, QueryPipelineStatisticFlagBits::eInputAssemblyVerticesBit, QueryPipelineStatisticFlagBits::eInputAssemblyPrimitivesBit, QueryPipelineStatisticFlagBits::eVertexShaderInvocationsBit, QueryPipelineStatisticFlagBits::eGeometryShaderInvocationsBit, QueryPipelineStatisticFlagBits::eGeometryShaderPrimitivesBit, QueryPipelineStatisticFlagBits::eClippingInvocationsBit, QueryPipelineStatisticFlagBits::eClippingPrimitivesBit, QueryPipelineStatisticFlagBits::eFragmentShaderInvocationsBit, QueryPipelineStatisticFlagBits::eTessellationControlShaderPatchesBit, QueryPipelineStatisticFlagBits::eTessellationEvaluationShaderInvocationsBit, QueryPipelineStatisticFlagBits::eComputeShaderInvocationsBit, QueryPipelineStatisticFlagBits::eTaskShaderInvocationsBitExt, QueryPipelineStatisticFlagBits::eMeshShaderInvocationsBitExt, QueryPipelineStatisticFlagBits::eClusterCullingShaderInvocationsBitHuawei);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct MemoryMapFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl MemoryMapFlags {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(MemoryMapFlags, u32);

fieldless_debug_derive!(MemoryMapFlags);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ImageAspectFlagBits {
    eNone,
    eColourBit,
    eDepthBit,
    eStencilBit,
    eMetadataBit,
    ePlane0Bit,
    ePlane1Bit,
    ePlane2Bit,
    eMemoryPlane0BitExt,
    eMemoryPlane1BitExt,
    eMemoryPlane2BitExt,
    eMemoryPlane3BitExt,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl ImageAspectFlagBits {
    pub const eNoneKhr: Self = Self::eNone;
    pub const ePlane0BitKhr: Self = Self::ePlane0Bit;
    pub const ePlane1BitKhr: Self = Self::ePlane1Bit;
    pub const ePlane2BitKhr: Self = Self::ePlane2Bit;

    pub fn into_raw(self) -> RawImageAspectFlagBits {
        match self {
            Self::eNone => RawImageAspectFlagBits::eNone,
            Self::eColourBit => RawImageAspectFlagBits::eColourBit,
            Self::eDepthBit => RawImageAspectFlagBits::eDepthBit,
            Self::eStencilBit => RawImageAspectFlagBits::eStencilBit,
            Self::eMetadataBit => RawImageAspectFlagBits::eMetadataBit,
            Self::ePlane0Bit => RawImageAspectFlagBits::ePlane0Bit,
            Self::ePlane1Bit => RawImageAspectFlagBits::ePlane1Bit,
            Self::ePlane2Bit => RawImageAspectFlagBits::ePlane2Bit,
            Self::eMemoryPlane0BitExt => RawImageAspectFlagBits::eMemoryPlane0BitExt,
            Self::eMemoryPlane1BitExt => RawImageAspectFlagBits::eMemoryPlane1BitExt,
            Self::eMemoryPlane2BitExt => RawImageAspectFlagBits::eMemoryPlane2BitExt,
            Self::eMemoryPlane3BitExt => RawImageAspectFlagBits::eMemoryPlane3BitExt,
            Self::eUnknownBit(b) => RawImageAspectFlagBits(b),
        }
    }
}

impl core::convert::From<ImageAspectFlagBits> for ImageAspectFlags {
    fn from(value: ImageAspectFlagBits) -> Self {
        match value {
            ImageAspectFlagBits::eNone => ImageAspectFlags::eNone,
            ImageAspectFlagBits::eColourBit => ImageAspectFlags::eColourBit,
            ImageAspectFlagBits::eDepthBit => ImageAspectFlags::eDepthBit,
            ImageAspectFlagBits::eStencilBit => ImageAspectFlags::eStencilBit,
            ImageAspectFlagBits::eMetadataBit => ImageAspectFlags::eMetadataBit,
            ImageAspectFlagBits::ePlane0Bit => ImageAspectFlags::ePlane0Bit,
            ImageAspectFlagBits::ePlane1Bit => ImageAspectFlags::ePlane1Bit,
            ImageAspectFlagBits::ePlane2Bit => ImageAspectFlags::ePlane2Bit,
            ImageAspectFlagBits::eMemoryPlane0BitExt => ImageAspectFlags::eMemoryPlane0BitExt,
            ImageAspectFlagBits::eMemoryPlane1BitExt => ImageAspectFlags::eMemoryPlane1BitExt,
            ImageAspectFlagBits::eMemoryPlane2BitExt => ImageAspectFlags::eMemoryPlane2BitExt,
            ImageAspectFlagBits::eMemoryPlane3BitExt => ImageAspectFlags::eMemoryPlane3BitExt,
            ImageAspectFlagBits::eUnknownBit(b) => ImageAspectFlags(b),
        }
    }
}

bit_field_enum_derives!(ImageAspectFlagBits, ImageAspectFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ImageAspectFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ImageAspectFlags {
    pub const eNone: Self = Self(0);
    pub const eColourBit: Self = Self(0b1);
    pub const eDepthBit: Self = Self(0b10);
    pub const eStencilBit: Self = Self(0b100);
    pub const eMetadataBit: Self = Self(0b1000);
    pub const ePlane0Bit: Self = Self(0b10000);
    pub const ePlane1Bit: Self = Self(0b100000);
    pub const ePlane2Bit: Self = Self(0b1000000);
    pub const eMemoryPlane0BitExt: Self = Self(0b10000000);
    pub const eMemoryPlane1BitExt: Self = Self(0b100000000);
    pub const eMemoryPlane2BitExt: Self = Self(0b1000000000);
    pub const eMemoryPlane3BitExt: Self = Self(0b10000000000);
    pub const eNoneKhr: Self = Self::eNone;
    pub const ePlane0BitKhr: Self = Self::ePlane0Bit;
    pub const ePlane1BitKhr: Self = Self::ePlane1Bit;
    pub const ePlane2BitKhr: Self = Self::ePlane2Bit;
}

bit_field_mask_derives!(ImageAspectFlags, u32);

bit_field_mask_with_enum_debug_derive!(ImageAspectFlagBits, ImageAspectFlags, u32, ImageAspectFlagBits::eColourBit, ImageAspectFlagBits::eDepthBit, ImageAspectFlagBits::eStencilBit, ImageAspectFlagBits::eMetadataBit, ImageAspectFlagBits::ePlane0Bit, ImageAspectFlagBits::ePlane1Bit, ImageAspectFlagBits::ePlane2Bit, ImageAspectFlagBits::eMemoryPlane0BitExt, ImageAspectFlagBits::eMemoryPlane1BitExt, ImageAspectFlagBits::eMemoryPlane2BitExt, ImageAspectFlagBits::eMemoryPlane3BitExt => ImageAspectFlagBits::eNone);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SparseMemoryBindFlagBits {
    eMetadataBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl SparseMemoryBindFlagBits {
    pub fn into_raw(self) -> RawSparseMemoryBindFlagBits {
        match self {
            Self::eMetadataBit => RawSparseMemoryBindFlagBits::eMetadataBit,
            Self::eUnknownBit(b) => RawSparseMemoryBindFlagBits(b),
        }
    }
}

impl core::convert::From<SparseMemoryBindFlagBits> for SparseMemoryBindFlags {
    fn from(value: SparseMemoryBindFlagBits) -> Self {
        match value {
            SparseMemoryBindFlagBits::eMetadataBit => SparseMemoryBindFlags::eMetadataBit,
            SparseMemoryBindFlagBits::eUnknownBit(b) => SparseMemoryBindFlags(b),
        }
    }
}

bit_field_enum_derives!(SparseMemoryBindFlagBits, SparseMemoryBindFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct SparseMemoryBindFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl SparseMemoryBindFlags {
    pub const eNone: Self = Self(0);
    pub const eMetadataBit: Self = Self(0b1);
}

bit_field_mask_derives!(SparseMemoryBindFlags, u32);

bit_field_mask_with_enum_debug_derive!(SparseMemoryBindFlagBits, SparseMemoryBindFlags, u32, SparseMemoryBindFlagBits::eMetadataBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SparseImageFormatFlagBits {
    eSingleMiptailBit,
    eAlignedMipSizeBit,
    eNonstandardBlockSizeBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl SparseImageFormatFlagBits {
    pub fn into_raw(self) -> RawSparseImageFormatFlagBits {
        match self {
            Self::eSingleMiptailBit => RawSparseImageFormatFlagBits::eSingleMiptailBit,
            Self::eAlignedMipSizeBit => RawSparseImageFormatFlagBits::eAlignedMipSizeBit,
            Self::eNonstandardBlockSizeBit => RawSparseImageFormatFlagBits::eNonstandardBlockSizeBit,
            Self::eUnknownBit(b) => RawSparseImageFormatFlagBits(b),
        }
    }
}

impl core::convert::From<SparseImageFormatFlagBits> for SparseImageFormatFlags {
    fn from(value: SparseImageFormatFlagBits) -> Self {
        match value {
            SparseImageFormatFlagBits::eSingleMiptailBit => SparseImageFormatFlags::eSingleMiptailBit,
            SparseImageFormatFlagBits::eAlignedMipSizeBit => SparseImageFormatFlags::eAlignedMipSizeBit,
            SparseImageFormatFlagBits::eNonstandardBlockSizeBit => SparseImageFormatFlags::eNonstandardBlockSizeBit,
            SparseImageFormatFlagBits::eUnknownBit(b) => SparseImageFormatFlags(b),
        }
    }
}

bit_field_enum_derives!(SparseImageFormatFlagBits, SparseImageFormatFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct SparseImageFormatFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl SparseImageFormatFlags {
    pub const eNone: Self = Self(0);
    pub const eSingleMiptailBit: Self = Self(0b1);
    pub const eAlignedMipSizeBit: Self = Self(0b10);
    pub const eNonstandardBlockSizeBit: Self = Self(0b100);
}

bit_field_mask_derives!(SparseImageFormatFlags, u32);

bit_field_mask_with_enum_debug_derive!(SparseImageFormatFlagBits, SparseImageFormatFlags, u32, SparseImageFormatFlagBits::eSingleMiptailBit, SparseImageFormatFlagBits::eAlignedMipSizeBit, SparseImageFormatFlagBits::eNonstandardBlockSizeBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SubpassDescriptionFlagBits {
    ePerViewAttributesBitNvx,
    ePerViewPositionXOnlyBitNvx,
    eFragmentRegionBitQcom,
    eShaderResolveBitQcom,
    eRasterizationOrderAttachmentColourAccessBitExt,
    eRasterizationOrderAttachmentDepthAccessBitExt,
    eRasterizationOrderAttachmentStencilAccessBitExt,
    eEnableLegacyDitheringBitExt,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl SubpassDescriptionFlagBits {
    pub fn into_raw(self) -> RawSubpassDescriptionFlagBits {
        match self {
            Self::ePerViewAttributesBitNvx => RawSubpassDescriptionFlagBits::ePerViewAttributesBitNvx,
            Self::ePerViewPositionXOnlyBitNvx => RawSubpassDescriptionFlagBits::ePerViewPositionXOnlyBitNvx,
            Self::eFragmentRegionBitQcom => RawSubpassDescriptionFlagBits::eFragmentRegionBitQcom,
            Self::eShaderResolveBitQcom => RawSubpassDescriptionFlagBits::eShaderResolveBitQcom,
            Self::eRasterizationOrderAttachmentColourAccessBitExt => RawSubpassDescriptionFlagBits::eRasterizationOrderAttachmentColourAccessBitExt,
            Self::eRasterizationOrderAttachmentDepthAccessBitExt => RawSubpassDescriptionFlagBits::eRasterizationOrderAttachmentDepthAccessBitExt,
            Self::eRasterizationOrderAttachmentStencilAccessBitExt => RawSubpassDescriptionFlagBits::eRasterizationOrderAttachmentStencilAccessBitExt,
            Self::eEnableLegacyDitheringBitExt => RawSubpassDescriptionFlagBits::eEnableLegacyDitheringBitExt,
            Self::eUnknownBit(b) => RawSubpassDescriptionFlagBits(b),
        }
    }
}

impl core::convert::From<SubpassDescriptionFlagBits> for SubpassDescriptionFlags {
    fn from(value: SubpassDescriptionFlagBits) -> Self {
        match value {
            SubpassDescriptionFlagBits::ePerViewAttributesBitNvx => SubpassDescriptionFlags::ePerViewAttributesBitNvx,
            SubpassDescriptionFlagBits::ePerViewPositionXOnlyBitNvx => SubpassDescriptionFlags::ePerViewPositionXOnlyBitNvx,
            SubpassDescriptionFlagBits::eFragmentRegionBitQcom => SubpassDescriptionFlags::eFragmentRegionBitQcom,
            SubpassDescriptionFlagBits::eShaderResolveBitQcom => SubpassDescriptionFlags::eShaderResolveBitQcom,
            SubpassDescriptionFlagBits::eRasterizationOrderAttachmentColourAccessBitExt => SubpassDescriptionFlags::eRasterizationOrderAttachmentColourAccessBitExt,
            SubpassDescriptionFlagBits::eRasterizationOrderAttachmentDepthAccessBitExt => SubpassDescriptionFlags::eRasterizationOrderAttachmentDepthAccessBitExt,
            SubpassDescriptionFlagBits::eRasterizationOrderAttachmentStencilAccessBitExt => SubpassDescriptionFlags::eRasterizationOrderAttachmentStencilAccessBitExt,
            SubpassDescriptionFlagBits::eEnableLegacyDitheringBitExt => SubpassDescriptionFlags::eEnableLegacyDitheringBitExt,
            SubpassDescriptionFlagBits::eUnknownBit(b) => SubpassDescriptionFlags(b),
        }
    }
}

bit_field_enum_derives!(SubpassDescriptionFlagBits, SubpassDescriptionFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct SubpassDescriptionFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl SubpassDescriptionFlags {
    pub const eNone: Self = Self(0);
    pub const ePerViewAttributesBitNvx: Self = Self(0b1);
    pub const ePerViewPositionXOnlyBitNvx: Self = Self(0b10);
    pub const eFragmentRegionBitQcom: Self = Self(0b100);
    pub const eShaderResolveBitQcom: Self = Self(0b1000);
    pub const eRasterizationOrderAttachmentColourAccessBitExt: Self = Self(0b10000);
    pub const eRasterizationOrderAttachmentDepthAccessBitExt: Self = Self(0b100000);
    pub const eRasterizationOrderAttachmentStencilAccessBitExt: Self = Self(0b1000000);
    pub const eEnableLegacyDitheringBitExt: Self = Self(0b10000000);
}

bit_field_mask_derives!(SubpassDescriptionFlags, u32);

bit_field_mask_with_enum_debug_derive!(SubpassDescriptionFlagBits, SubpassDescriptionFlags, u32, SubpassDescriptionFlagBits::ePerViewAttributesBitNvx, SubpassDescriptionFlagBits::ePerViewPositionXOnlyBitNvx, SubpassDescriptionFlagBits::eFragmentRegionBitQcom, SubpassDescriptionFlagBits::eShaderResolveBitQcom, SubpassDescriptionFlagBits::eRasterizationOrderAttachmentColourAccessBitExt, SubpassDescriptionFlagBits::eRasterizationOrderAttachmentDepthAccessBitExt, SubpassDescriptionFlagBits::eRasterizationOrderAttachmentStencilAccessBitExt, SubpassDescriptionFlagBits::eEnableLegacyDitheringBitExt);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PipelineStageFlagBits {
    eNone,
    eTopOfPipeBit,
    eDrawIndirectBit,
    eVertexInputBit,
    eVertexShaderBit,
    eTessellationControlShaderBit,
    eTessellationEvaluationShaderBit,
    eGeometryShaderBit,
    eFragmentShaderBit,
    eEarlyFragmentTestsBit,
    eLateFragmentTestsBit,
    eColourAttachmentOutputBit,
    eComputeShaderBit,
    eTransferBit,
    eBottomOfPipeBit,
    eHostBit,
    eAllGraphicsBit,
    eAllCommandsBit,
    eCommandPreprocessBitNv,
    eConditionalRenderingBitExt,
    eTaskShaderBitExt,
    eMeshShaderBitExt,
    eRayTracingShaderBitKhr,
    eFragmentShadingRateAttachmentBitKhr,
    eFragmentDensityProcessBitExt,
    eTransformFeedbackBitExt,
    eAccelerationStructureBuildBitKhr,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl PipelineStageFlagBits {
    pub const eNoneKhr: Self = Self::eNone;
    pub const eRayTracingShaderBitNv: Self = Self::eRayTracingShaderBitKhr;
    pub const eAccelerationStructureBuildBitNv: Self = Self::eAccelerationStructureBuildBitKhr;

    pub fn into_raw(self) -> RawPipelineStageFlagBits {
        match self {
            Self::eNone => RawPipelineStageFlagBits::eNone,
            Self::eTopOfPipeBit => RawPipelineStageFlagBits::eTopOfPipeBit,
            Self::eDrawIndirectBit => RawPipelineStageFlagBits::eDrawIndirectBit,
            Self::eVertexInputBit => RawPipelineStageFlagBits::eVertexInputBit,
            Self::eVertexShaderBit => RawPipelineStageFlagBits::eVertexShaderBit,
            Self::eTessellationControlShaderBit => RawPipelineStageFlagBits::eTessellationControlShaderBit,
            Self::eTessellationEvaluationShaderBit => RawPipelineStageFlagBits::eTessellationEvaluationShaderBit,
            Self::eGeometryShaderBit => RawPipelineStageFlagBits::eGeometryShaderBit,
            Self::eFragmentShaderBit => RawPipelineStageFlagBits::eFragmentShaderBit,
            Self::eEarlyFragmentTestsBit => RawPipelineStageFlagBits::eEarlyFragmentTestsBit,
            Self::eLateFragmentTestsBit => RawPipelineStageFlagBits::eLateFragmentTestsBit,
            Self::eColourAttachmentOutputBit => RawPipelineStageFlagBits::eColourAttachmentOutputBit,
            Self::eComputeShaderBit => RawPipelineStageFlagBits::eComputeShaderBit,
            Self::eTransferBit => RawPipelineStageFlagBits::eTransferBit,
            Self::eBottomOfPipeBit => RawPipelineStageFlagBits::eBottomOfPipeBit,
            Self::eHostBit => RawPipelineStageFlagBits::eHostBit,
            Self::eAllGraphicsBit => RawPipelineStageFlagBits::eAllGraphicsBit,
            Self::eAllCommandsBit => RawPipelineStageFlagBits::eAllCommandsBit,
            Self::eCommandPreprocessBitNv => RawPipelineStageFlagBits::eCommandPreprocessBitNv,
            Self::eConditionalRenderingBitExt => RawPipelineStageFlagBits::eConditionalRenderingBitExt,
            Self::eTaskShaderBitExt => RawPipelineStageFlagBits::eTaskShaderBitExt,
            Self::eMeshShaderBitExt => RawPipelineStageFlagBits::eMeshShaderBitExt,
            Self::eRayTracingShaderBitKhr => RawPipelineStageFlagBits::eRayTracingShaderBitKhr,
            Self::eFragmentShadingRateAttachmentBitKhr => RawPipelineStageFlagBits::eFragmentShadingRateAttachmentBitKhr,
            Self::eFragmentDensityProcessBitExt => RawPipelineStageFlagBits::eFragmentDensityProcessBitExt,
            Self::eTransformFeedbackBitExt => RawPipelineStageFlagBits::eTransformFeedbackBitExt,
            Self::eAccelerationStructureBuildBitKhr => RawPipelineStageFlagBits::eAccelerationStructureBuildBitKhr,
            Self::eUnknownBit(b) => RawPipelineStageFlagBits(b),
        }
    }
}

impl core::convert::From<PipelineStageFlagBits> for PipelineStageFlags {
    fn from(value: PipelineStageFlagBits) -> Self {
        match value {
            PipelineStageFlagBits::eNone => PipelineStageFlags::eNone,
            PipelineStageFlagBits::eTopOfPipeBit => PipelineStageFlags::eTopOfPipeBit,
            PipelineStageFlagBits::eDrawIndirectBit => PipelineStageFlags::eDrawIndirectBit,
            PipelineStageFlagBits::eVertexInputBit => PipelineStageFlags::eVertexInputBit,
            PipelineStageFlagBits::eVertexShaderBit => PipelineStageFlags::eVertexShaderBit,
            PipelineStageFlagBits::eTessellationControlShaderBit => PipelineStageFlags::eTessellationControlShaderBit,
            PipelineStageFlagBits::eTessellationEvaluationShaderBit => PipelineStageFlags::eTessellationEvaluationShaderBit,
            PipelineStageFlagBits::eGeometryShaderBit => PipelineStageFlags::eGeometryShaderBit,
            PipelineStageFlagBits::eFragmentShaderBit => PipelineStageFlags::eFragmentShaderBit,
            PipelineStageFlagBits::eEarlyFragmentTestsBit => PipelineStageFlags::eEarlyFragmentTestsBit,
            PipelineStageFlagBits::eLateFragmentTestsBit => PipelineStageFlags::eLateFragmentTestsBit,
            PipelineStageFlagBits::eColourAttachmentOutputBit => PipelineStageFlags::eColourAttachmentOutputBit,
            PipelineStageFlagBits::eComputeShaderBit => PipelineStageFlags::eComputeShaderBit,
            PipelineStageFlagBits::eTransferBit => PipelineStageFlags::eTransferBit,
            PipelineStageFlagBits::eBottomOfPipeBit => PipelineStageFlags::eBottomOfPipeBit,
            PipelineStageFlagBits::eHostBit => PipelineStageFlags::eHostBit,
            PipelineStageFlagBits::eAllGraphicsBit => PipelineStageFlags::eAllGraphicsBit,
            PipelineStageFlagBits::eAllCommandsBit => PipelineStageFlags::eAllCommandsBit,
            PipelineStageFlagBits::eCommandPreprocessBitNv => PipelineStageFlags::eCommandPreprocessBitNv,
            PipelineStageFlagBits::eConditionalRenderingBitExt => PipelineStageFlags::eConditionalRenderingBitExt,
            PipelineStageFlagBits::eTaskShaderBitExt => PipelineStageFlags::eTaskShaderBitExt,
            PipelineStageFlagBits::eMeshShaderBitExt => PipelineStageFlags::eMeshShaderBitExt,
            PipelineStageFlagBits::eRayTracingShaderBitKhr => PipelineStageFlags::eRayTracingShaderBitKhr,
            PipelineStageFlagBits::eFragmentShadingRateAttachmentBitKhr => PipelineStageFlags::eFragmentShadingRateAttachmentBitKhr,
            PipelineStageFlagBits::eFragmentDensityProcessBitExt => PipelineStageFlags::eFragmentDensityProcessBitExt,
            PipelineStageFlagBits::eTransformFeedbackBitExt => PipelineStageFlags::eTransformFeedbackBitExt,
            PipelineStageFlagBits::eAccelerationStructureBuildBitKhr => PipelineStageFlags::eAccelerationStructureBuildBitKhr,
            PipelineStageFlagBits::eUnknownBit(b) => PipelineStageFlags(b),
        }
    }
}

bit_field_enum_derives!(PipelineStageFlagBits, PipelineStageFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineStageFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineStageFlags {
    pub const eNone: Self = Self(0);
    pub const eTopOfPipeBit: Self = Self(0b1);
    pub const eDrawIndirectBit: Self = Self(0b10);
    pub const eVertexInputBit: Self = Self(0b100);
    pub const eVertexShaderBit: Self = Self(0b1000);
    pub const eTessellationControlShaderBit: Self = Self(0b10000);
    pub const eTessellationEvaluationShaderBit: Self = Self(0b100000);
    pub const eGeometryShaderBit: Self = Self(0b1000000);
    pub const eFragmentShaderBit: Self = Self(0b10000000);
    pub const eEarlyFragmentTestsBit: Self = Self(0b100000000);
    pub const eLateFragmentTestsBit: Self = Self(0b1000000000);
    pub const eColourAttachmentOutputBit: Self = Self(0b10000000000);
    pub const eComputeShaderBit: Self = Self(0b100000000000);
    pub const eTransferBit: Self = Self(0b1000000000000);
    pub const eBottomOfPipeBit: Self = Self(0b10000000000000);
    pub const eHostBit: Self = Self(0b100000000000000);
    pub const eAllGraphicsBit: Self = Self(0b1000000000000000);
    pub const eAllCommandsBit: Self = Self(0b10000000000000000);
    pub const eCommandPreprocessBitNv: Self = Self(0b100000000000000000);
    pub const eConditionalRenderingBitExt: Self = Self(0b1000000000000000000);
    pub const eTaskShaderBitExt: Self = Self(0b10000000000000000000);
    pub const eMeshShaderBitExt: Self = Self(0b100000000000000000000);
    pub const eRayTracingShaderBitKhr: Self = Self(0b1000000000000000000000);
    pub const eFragmentShadingRateAttachmentBitKhr: Self = Self(0b10000000000000000000000);
    pub const eFragmentDensityProcessBitExt: Self = Self(0b100000000000000000000000);
    pub const eTransformFeedbackBitExt: Self = Self(0b1000000000000000000000000);
    pub const eAccelerationStructureBuildBitKhr: Self = Self(0b10000000000000000000000000);
    pub const eNoneKhr: Self = Self::eNone;
    pub const eRayTracingShaderBitNv: Self = Self::eRayTracingShaderBitKhr;
    pub const eAccelerationStructureBuildBitNv: Self = Self::eAccelerationStructureBuildBitKhr;
}

bit_field_mask_derives!(PipelineStageFlags, u32);

bit_field_mask_with_enum_debug_derive!(PipelineStageFlagBits, PipelineStageFlags, u32, PipelineStageFlagBits::eTopOfPipeBit, PipelineStageFlagBits::eDrawIndirectBit, PipelineStageFlagBits::eVertexInputBit, PipelineStageFlagBits::eVertexShaderBit, PipelineStageFlagBits::eTessellationControlShaderBit, PipelineStageFlagBits::eTessellationEvaluationShaderBit, PipelineStageFlagBits::eGeometryShaderBit, PipelineStageFlagBits::eFragmentShaderBit, PipelineStageFlagBits::eEarlyFragmentTestsBit, PipelineStageFlagBits::eLateFragmentTestsBit, PipelineStageFlagBits::eColourAttachmentOutputBit, PipelineStageFlagBits::eComputeShaderBit, PipelineStageFlagBits::eTransferBit, PipelineStageFlagBits::eBottomOfPipeBit, PipelineStageFlagBits::eHostBit, PipelineStageFlagBits::eAllGraphicsBit, PipelineStageFlagBits::eAllCommandsBit, PipelineStageFlagBits::eCommandPreprocessBitNv, PipelineStageFlagBits::eConditionalRenderingBitExt, PipelineStageFlagBits::eTaskShaderBitExt, PipelineStageFlagBits::eMeshShaderBitExt, PipelineStageFlagBits::eRayTracingShaderBitKhr, PipelineStageFlagBits::eFragmentShadingRateAttachmentBitKhr, PipelineStageFlagBits::eFragmentDensityProcessBitExt, PipelineStageFlagBits::eTransformFeedbackBitExt, PipelineStageFlagBits::eAccelerationStructureBuildBitKhr => PipelineStageFlagBits::eNone);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SampleCountFlagBits {
    e1Bit,
    e2Bit,
    e4Bit,
    e8Bit,
    e16Bit,
    e32Bit,
    e64Bit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl SampleCountFlagBits {
    pub fn into_raw(self) -> RawSampleCountFlagBits {
        match self {
            Self::e1Bit => RawSampleCountFlagBits::e1Bit,
            Self::e2Bit => RawSampleCountFlagBits::e2Bit,
            Self::e4Bit => RawSampleCountFlagBits::e4Bit,
            Self::e8Bit => RawSampleCountFlagBits::e8Bit,
            Self::e16Bit => RawSampleCountFlagBits::e16Bit,
            Self::e32Bit => RawSampleCountFlagBits::e32Bit,
            Self::e64Bit => RawSampleCountFlagBits::e64Bit,
            Self::eUnknownBit(b) => RawSampleCountFlagBits(b),
        }
    }
}

impl core::convert::From<SampleCountFlagBits> for SampleCountFlags {
    fn from(value: SampleCountFlagBits) -> Self {
        match value {
            SampleCountFlagBits::e1Bit => SampleCountFlags::e1Bit,
            SampleCountFlagBits::e2Bit => SampleCountFlags::e2Bit,
            SampleCountFlagBits::e4Bit => SampleCountFlags::e4Bit,
            SampleCountFlagBits::e8Bit => SampleCountFlags::e8Bit,
            SampleCountFlagBits::e16Bit => SampleCountFlags::e16Bit,
            SampleCountFlagBits::e32Bit => SampleCountFlags::e32Bit,
            SampleCountFlagBits::e64Bit => SampleCountFlags::e64Bit,
            SampleCountFlagBits::eUnknownBit(b) => SampleCountFlags(b),
        }
    }
}

bit_field_enum_derives!(SampleCountFlagBits, SampleCountFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct SampleCountFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl SampleCountFlags {
    pub const eNone: Self = Self(0);
    pub const e1Bit: Self = Self(0b1);
    pub const e2Bit: Self = Self(0b10);
    pub const e4Bit: Self = Self(0b100);
    pub const e8Bit: Self = Self(0b1000);
    pub const e16Bit: Self = Self(0b10000);
    pub const e32Bit: Self = Self(0b100000);
    pub const e64Bit: Self = Self(0b1000000);
}

bit_field_mask_derives!(SampleCountFlags, u32);

bit_field_mask_with_enum_debug_derive!(SampleCountFlagBits, SampleCountFlags, u32, SampleCountFlagBits::e1Bit, SampleCountFlagBits::e2Bit, SampleCountFlagBits::e4Bit, SampleCountFlagBits::e8Bit, SampleCountFlagBits::e16Bit, SampleCountFlagBits::e32Bit, SampleCountFlagBits::e64Bit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum AttachmentDescriptionFlagBits {
    eMayAliasBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl AttachmentDescriptionFlagBits {
    pub fn into_raw(self) -> RawAttachmentDescriptionFlagBits {
        match self {
            Self::eMayAliasBit => RawAttachmentDescriptionFlagBits::eMayAliasBit,
            Self::eUnknownBit(b) => RawAttachmentDescriptionFlagBits(b),
        }
    }
}

impl core::convert::From<AttachmentDescriptionFlagBits> for AttachmentDescriptionFlags {
    fn from(value: AttachmentDescriptionFlagBits) -> Self {
        match value {
            AttachmentDescriptionFlagBits::eMayAliasBit => AttachmentDescriptionFlags::eMayAliasBit,
            AttachmentDescriptionFlagBits::eUnknownBit(b) => AttachmentDescriptionFlags(b),
        }
    }
}

bit_field_enum_derives!(AttachmentDescriptionFlagBits, AttachmentDescriptionFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct AttachmentDescriptionFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl AttachmentDescriptionFlags {
    pub const eNone: Self = Self(0);
    pub const eMayAliasBit: Self = Self(0b1);
}

bit_field_mask_derives!(AttachmentDescriptionFlags, u32);

bit_field_mask_with_enum_debug_derive!(AttachmentDescriptionFlagBits, AttachmentDescriptionFlags, u32, AttachmentDescriptionFlagBits::eMayAliasBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum StencilFaceFlagBits {
    eFrontBit,
    eBackBit,
    eFrontAndBack,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl StencilFaceFlagBits {
    pub const eFrontAndBack: Self = Self::eFrontAndBack;

    pub fn into_raw(self) -> RawStencilFaceFlagBits {
        match self {
            Self::eFrontBit => RawStencilFaceFlagBits::eFrontBit,
            Self::eBackBit => RawStencilFaceFlagBits::eBackBit,
            Self::eFrontAndBack => RawStencilFaceFlagBits::eFrontAndBack,
            Self::eUnknownBit(b) => RawStencilFaceFlagBits(b),
        }
    }
}

impl core::convert::From<StencilFaceFlagBits> for StencilFaceFlags {
    fn from(value: StencilFaceFlagBits) -> Self {
        match value {
            StencilFaceFlagBits::eFrontBit => StencilFaceFlags::eFrontBit,
            StencilFaceFlagBits::eBackBit => StencilFaceFlags::eBackBit,
            StencilFaceFlagBits::eFrontAndBack => StencilFaceFlags::eFrontAndBack,
            StencilFaceFlagBits::eUnknownBit(b) => StencilFaceFlags(b),
        }
    }
}

bit_field_enum_derives!(StencilFaceFlagBits, StencilFaceFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct StencilFaceFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl StencilFaceFlags {
    pub const eNone: Self = Self(0);
    pub const eFrontBit: Self = Self(0b1);
    pub const eBackBit: Self = Self(0b10);
    pub const eFrontAndBack: Self = Self(0b11);
}

bit_field_mask_derives!(StencilFaceFlags, u32);

bit_field_mask_with_enum_debug_derive!(StencilFaceFlagBits, StencilFaceFlags, u32, StencilFaceFlagBits::eFrontBit, StencilFaceFlagBits::eBackBit, StencilFaceFlagBits::eFrontAndBack);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum CullModeFlagBits {
    eNone,
    eFrontBit,
    eBackBit,
    eFrontAndBack,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl CullModeFlagBits {
    pub fn into_raw(self) -> RawCullModeFlagBits {
        match self {
            Self::eNone => RawCullModeFlagBits::eNone,
            Self::eFrontBit => RawCullModeFlagBits::eFrontBit,
            Self::eBackBit => RawCullModeFlagBits::eBackBit,
            Self::eFrontAndBack => RawCullModeFlagBits::eFrontAndBack,
            Self::eUnknownBit(b) => RawCullModeFlagBits(b),
        }
    }
}

impl core::convert::From<CullModeFlagBits> for CullModeFlags {
    fn from(value: CullModeFlagBits) -> Self {
        match value {
            CullModeFlagBits::eNone => CullModeFlags::eNone,
            CullModeFlagBits::eFrontBit => CullModeFlags::eFrontBit,
            CullModeFlagBits::eBackBit => CullModeFlags::eBackBit,
            CullModeFlagBits::eFrontAndBack => CullModeFlags::eFrontAndBack,
            CullModeFlagBits::eUnknownBit(b) => CullModeFlags(b),
        }
    }
}

bit_field_enum_derives!(CullModeFlagBits, CullModeFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct CullModeFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl CullModeFlags {
    pub const eNone: Self = Self(0);
    pub const eFrontBit: Self = Self(0b1);
    pub const eBackBit: Self = Self(0b10);
    pub const eFrontAndBack: Self = Self(0b11);
}

bit_field_mask_derives!(CullModeFlags, u32);

bit_field_mask_with_enum_debug_derive!(CullModeFlagBits, CullModeFlags, u32, CullModeFlagBits::eFrontBit, CullModeFlagBits::eBackBit, CullModeFlagBits::eFrontAndBack => CullModeFlagBits::eNone);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DescriptorPoolCreateFlagBits {
    eFreeDescriptorSetBit,
    eUpdateAfterBindBit,
    eHostOnlyBitExt,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl DescriptorPoolCreateFlagBits {
    pub const eUpdateAfterBindBitExt: Self = Self::eUpdateAfterBindBit;

    pub fn into_raw(self) -> RawDescriptorPoolCreateFlagBits {
        match self {
            Self::eFreeDescriptorSetBit => RawDescriptorPoolCreateFlagBits::eFreeDescriptorSetBit,
            Self::eUpdateAfterBindBit => RawDescriptorPoolCreateFlagBits::eUpdateAfterBindBit,
            Self::eHostOnlyBitExt => RawDescriptorPoolCreateFlagBits::eHostOnlyBitExt,
            Self::eUnknownBit(b) => RawDescriptorPoolCreateFlagBits(b),
        }
    }
}

impl core::convert::From<DescriptorPoolCreateFlagBits> for DescriptorPoolCreateFlags {
    fn from(value: DescriptorPoolCreateFlagBits) -> Self {
        match value {
            DescriptorPoolCreateFlagBits::eFreeDescriptorSetBit => DescriptorPoolCreateFlags::eFreeDescriptorSetBit,
            DescriptorPoolCreateFlagBits::eUpdateAfterBindBit => DescriptorPoolCreateFlags::eUpdateAfterBindBit,
            DescriptorPoolCreateFlagBits::eHostOnlyBitExt => DescriptorPoolCreateFlags::eHostOnlyBitExt,
            DescriptorPoolCreateFlagBits::eUnknownBit(b) => DescriptorPoolCreateFlags(b),
        }
    }
}

bit_field_enum_derives!(DescriptorPoolCreateFlagBits, DescriptorPoolCreateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DescriptorPoolCreateFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DescriptorPoolCreateFlags {
    pub const eNone: Self = Self(0);
    pub const eFreeDescriptorSetBit: Self = Self(0b1);
    pub const eUpdateAfterBindBit: Self = Self(0b10);
    pub const eHostOnlyBitExt: Self = Self(0b100);
    pub const eUpdateAfterBindBitExt: Self = Self::eUpdateAfterBindBit;
}

bit_field_mask_derives!(DescriptorPoolCreateFlags, u32);

bit_field_mask_with_enum_debug_derive!(DescriptorPoolCreateFlagBits, DescriptorPoolCreateFlags, u32, DescriptorPoolCreateFlagBits::eFreeDescriptorSetBit, DescriptorPoolCreateFlagBits::eUpdateAfterBindBit, DescriptorPoolCreateFlagBits::eHostOnlyBitExt);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DescriptorPoolResetFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DescriptorPoolResetFlags {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(DescriptorPoolResetFlags, u32);

fieldless_debug_derive!(DescriptorPoolResetFlags);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DependencyFlagBits {
    eByRegionBit,
    eViewLocalBit,
    eDeviceGroupBit,
    eFeedbackLoopBitExt,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl DependencyFlagBits {
    pub const eViewLocalBitKhr: Self = Self::eViewLocalBit;
    pub const eDeviceGroupBitKhr: Self = Self::eDeviceGroupBit;

    pub fn into_raw(self) -> RawDependencyFlagBits {
        match self {
            Self::eByRegionBit => RawDependencyFlagBits::eByRegionBit,
            Self::eViewLocalBit => RawDependencyFlagBits::eViewLocalBit,
            Self::eDeviceGroupBit => RawDependencyFlagBits::eDeviceGroupBit,
            Self::eFeedbackLoopBitExt => RawDependencyFlagBits::eFeedbackLoopBitExt,
            Self::eUnknownBit(b) => RawDependencyFlagBits(b),
        }
    }
}

impl core::convert::From<DependencyFlagBits> for DependencyFlags {
    fn from(value: DependencyFlagBits) -> Self {
        match value {
            DependencyFlagBits::eByRegionBit => DependencyFlags::eByRegionBit,
            DependencyFlagBits::eViewLocalBit => DependencyFlags::eViewLocalBit,
            DependencyFlagBits::eDeviceGroupBit => DependencyFlags::eDeviceGroupBit,
            DependencyFlagBits::eFeedbackLoopBitExt => DependencyFlags::eFeedbackLoopBitExt,
            DependencyFlagBits::eUnknownBit(b) => DependencyFlags(b),
        }
    }
}

bit_field_enum_derives!(DependencyFlagBits, DependencyFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DependencyFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DependencyFlags {
    pub const eNone: Self = Self(0);
    pub const eByRegionBit: Self = Self(0b1);
    pub const eViewLocalBit: Self = Self(0b10);
    pub const eDeviceGroupBit: Self = Self(0b100);
    pub const eFeedbackLoopBitExt: Self = Self(0b1000);
    pub const eViewLocalBitKhr: Self = Self::eViewLocalBit;
    pub const eDeviceGroupBitKhr: Self = Self::eDeviceGroupBit;
}

bit_field_mask_derives!(DependencyFlags, u32);

bit_field_mask_with_enum_debug_derive!(DependencyFlagBits, DependencyFlags, u32, DependencyFlagBits::eByRegionBit, DependencyFlagBits::eViewLocalBit, DependencyFlagBits::eDeviceGroupBit, DependencyFlagBits::eFeedbackLoopBitExt);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SubgroupFeatureFlagBits {
    eBasicBit,
    eVoteBit,
    eArithmeticBit,
    eBallotBit,
    eShuffleBit,
    eShuffleRelativeBit,
    eClusteredBit,
    eQuadBit,
    ePartitionedBitNv,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl SubgroupFeatureFlagBits {
    pub fn into_raw(self) -> RawSubgroupFeatureFlagBits {
        match self {
            Self::eBasicBit => RawSubgroupFeatureFlagBits::eBasicBit,
            Self::eVoteBit => RawSubgroupFeatureFlagBits::eVoteBit,
            Self::eArithmeticBit => RawSubgroupFeatureFlagBits::eArithmeticBit,
            Self::eBallotBit => RawSubgroupFeatureFlagBits::eBallotBit,
            Self::eShuffleBit => RawSubgroupFeatureFlagBits::eShuffleBit,
            Self::eShuffleRelativeBit => RawSubgroupFeatureFlagBits::eShuffleRelativeBit,
            Self::eClusteredBit => RawSubgroupFeatureFlagBits::eClusteredBit,
            Self::eQuadBit => RawSubgroupFeatureFlagBits::eQuadBit,
            Self::ePartitionedBitNv => RawSubgroupFeatureFlagBits::ePartitionedBitNv,
            Self::eUnknownBit(b) => RawSubgroupFeatureFlagBits(b),
        }
    }
}

impl core::convert::From<SubgroupFeatureFlagBits> for SubgroupFeatureFlags {
    fn from(value: SubgroupFeatureFlagBits) -> Self {
        match value {
            SubgroupFeatureFlagBits::eBasicBit => SubgroupFeatureFlags::eBasicBit,
            SubgroupFeatureFlagBits::eVoteBit => SubgroupFeatureFlags::eVoteBit,
            SubgroupFeatureFlagBits::eArithmeticBit => SubgroupFeatureFlags::eArithmeticBit,
            SubgroupFeatureFlagBits::eBallotBit => SubgroupFeatureFlags::eBallotBit,
            SubgroupFeatureFlagBits::eShuffleBit => SubgroupFeatureFlags::eShuffleBit,
            SubgroupFeatureFlagBits::eShuffleRelativeBit => SubgroupFeatureFlags::eShuffleRelativeBit,
            SubgroupFeatureFlagBits::eClusteredBit => SubgroupFeatureFlags::eClusteredBit,
            SubgroupFeatureFlagBits::eQuadBit => SubgroupFeatureFlags::eQuadBit,
            SubgroupFeatureFlagBits::ePartitionedBitNv => SubgroupFeatureFlags::ePartitionedBitNv,
            SubgroupFeatureFlagBits::eUnknownBit(b) => SubgroupFeatureFlags(b),
        }
    }
}

bit_field_enum_derives!(SubgroupFeatureFlagBits, SubgroupFeatureFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct SubgroupFeatureFlags(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl SubgroupFeatureFlags {
    pub const eNone: Self = Self(0);
    pub const eBasicBit: Self = Self(0b1);
    pub const eVoteBit: Self = Self(0b10);
    pub const eArithmeticBit: Self = Self(0b100);
    pub const eBallotBit: Self = Self(0b1000);
    pub const eShuffleBit: Self = Self(0b10000);
    pub const eShuffleRelativeBit: Self = Self(0b100000);
    pub const eClusteredBit: Self = Self(0b1000000);
    pub const eQuadBit: Self = Self(0b10000000);
    pub const ePartitionedBitNv: Self = Self(0b100000000);
}

bit_field_mask_derives!(SubgroupFeatureFlags, u32);

bit_field_mask_with_enum_debug_derive!(SubgroupFeatureFlagBits, SubgroupFeatureFlags, u32, SubgroupFeatureFlagBits::eBasicBit, SubgroupFeatureFlagBits::eVoteBit, SubgroupFeatureFlagBits::eArithmeticBit, SubgroupFeatureFlagBits::eBallotBit, SubgroupFeatureFlagBits::eShuffleBit, SubgroupFeatureFlagBits::eShuffleRelativeBit, SubgroupFeatureFlagBits::eClusteredBit, SubgroupFeatureFlagBits::eQuadBit, SubgroupFeatureFlagBits::ePartitionedBitNv);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum IndirectCommandsLayoutUsageFlagBitsNV {
    eExplicitPreprocessBit,
    eIndexedSequencesBit,
    eUnorderedSequencesBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl IndirectCommandsLayoutUsageFlagBitsNV {
    pub fn into_raw(self) -> RawIndirectCommandsLayoutUsageFlagBitsNV {
        match self {
            Self::eExplicitPreprocessBit => RawIndirectCommandsLayoutUsageFlagBitsNV::eExplicitPreprocessBit,
            Self::eIndexedSequencesBit => RawIndirectCommandsLayoutUsageFlagBitsNV::eIndexedSequencesBit,
            Self::eUnorderedSequencesBit => RawIndirectCommandsLayoutUsageFlagBitsNV::eUnorderedSequencesBit,
            Self::eUnknownBit(b) => RawIndirectCommandsLayoutUsageFlagBitsNV(b),
        }
    }
}

impl core::convert::From<IndirectCommandsLayoutUsageFlagBitsNV> for IndirectCommandsLayoutUsageFlagsNV {
    fn from(value: IndirectCommandsLayoutUsageFlagBitsNV) -> Self {
        match value {
            IndirectCommandsLayoutUsageFlagBitsNV::eExplicitPreprocessBit => IndirectCommandsLayoutUsageFlagsNV::eExplicitPreprocessBit,
            IndirectCommandsLayoutUsageFlagBitsNV::eIndexedSequencesBit => IndirectCommandsLayoutUsageFlagsNV::eIndexedSequencesBit,
            IndirectCommandsLayoutUsageFlagBitsNV::eUnorderedSequencesBit => IndirectCommandsLayoutUsageFlagsNV::eUnorderedSequencesBit,
            IndirectCommandsLayoutUsageFlagBitsNV::eUnknownBit(b) => IndirectCommandsLayoutUsageFlagsNV(b),
        }
    }
}

bit_field_enum_derives!(IndirectCommandsLayoutUsageFlagBitsNV, IndirectCommandsLayoutUsageFlagsNV, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct IndirectCommandsLayoutUsageFlagsNV(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl IndirectCommandsLayoutUsageFlagsNV {
    pub const eNone: Self = Self(0);
    pub const eExplicitPreprocessBit: Self = Self(0b1);
    pub const eIndexedSequencesBit: Self = Self(0b10);
    pub const eUnorderedSequencesBit: Self = Self(0b100);
}

bit_field_mask_derives!(IndirectCommandsLayoutUsageFlagsNV, u32);

bit_field_mask_with_enum_debug_derive!(IndirectCommandsLayoutUsageFlagBitsNV, IndirectCommandsLayoutUsageFlagsNV, u32, IndirectCommandsLayoutUsageFlagBitsNV::eExplicitPreprocessBit, IndirectCommandsLayoutUsageFlagBitsNV::eIndexedSequencesBit, IndirectCommandsLayoutUsageFlagBitsNV::eUnorderedSequencesBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum IndirectStateFlagBitsNV {
    eFrontfaceBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl IndirectStateFlagBitsNV {
    pub fn into_raw(self) -> RawIndirectStateFlagBitsNV {
        match self {
            Self::eFrontfaceBit => RawIndirectStateFlagBitsNV::eFrontfaceBit,
            Self::eUnknownBit(b) => RawIndirectStateFlagBitsNV(b),
        }
    }
}

impl core::convert::From<IndirectStateFlagBitsNV> for IndirectStateFlagsNV {
    fn from(value: IndirectStateFlagBitsNV) -> Self {
        match value {
            IndirectStateFlagBitsNV::eFrontfaceBit => IndirectStateFlagsNV::eFrontfaceBit,
            IndirectStateFlagBitsNV::eUnknownBit(b) => IndirectStateFlagsNV(b),
        }
    }
}

bit_field_enum_derives!(IndirectStateFlagBitsNV, IndirectStateFlagsNV, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct IndirectStateFlagsNV(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl IndirectStateFlagsNV {
    pub const eNone: Self = Self(0);
    pub const eFrontfaceBit: Self = Self(0b1);
}

bit_field_mask_derives!(IndirectStateFlagsNV, u32);

bit_field_mask_with_enum_debug_derive!(IndirectStateFlagBitsNV, IndirectStateFlagsNV, u32, IndirectStateFlagBitsNV::eFrontfaceBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum GeometryFlagBitsKHR {
    eOpaqueBit,
    eNoDuplicateAnyHitInvocationBit,
    eUnknownBit(u32),
}

pub type GeometryFlagBitsNV = GeometryFlagBitsKHR;

#[allow(non_upper_case_globals)]
impl GeometryFlagBitsKHR {
    pub const eOpaqueBitNv: Self = Self::eOpaqueBit;
    pub const eNoDuplicateAnyHitInvocationBitNv: Self = Self::eNoDuplicateAnyHitInvocationBit;

    pub fn into_raw(self) -> RawGeometryFlagBitsKHR {
        match self {
            Self::eOpaqueBit => RawGeometryFlagBitsKHR::eOpaqueBit,
            Self::eNoDuplicateAnyHitInvocationBit => RawGeometryFlagBitsKHR::eNoDuplicateAnyHitInvocationBit,
            Self::eUnknownBit(b) => RawGeometryFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<GeometryFlagBitsKHR> for GeometryFlagsKHR {
    fn from(value: GeometryFlagBitsKHR) -> Self {
        match value {
            GeometryFlagBitsKHR::eOpaqueBit => GeometryFlagsKHR::eOpaqueBit,
            GeometryFlagBitsKHR::eNoDuplicateAnyHitInvocationBit => GeometryFlagsKHR::eNoDuplicateAnyHitInvocationBit,
            GeometryFlagBitsKHR::eUnknownBit(b) => GeometryFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(GeometryFlagBitsKHR, GeometryFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct GeometryFlagsKHR(pub(crate) u32);

pub type GeometryFlagsNV = GeometryFlagsKHR;

#[allow(non_upper_case_globals)]
impl GeometryFlagsKHR {
    pub const eNone: Self = Self(0);
    pub const eOpaqueBit: Self = Self(0b1);
    pub const eNoDuplicateAnyHitInvocationBit: Self = Self(0b10);
    pub const eOpaqueBitNv: Self = Self::eOpaqueBit;
    pub const eNoDuplicateAnyHitInvocationBitNv: Self = Self::eNoDuplicateAnyHitInvocationBit;
}

bit_field_mask_derives!(GeometryFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(GeometryFlagBitsKHR, GeometryFlagsKHR, u32, GeometryFlagBitsKHR::eOpaqueBit, GeometryFlagBitsKHR::eNoDuplicateAnyHitInvocationBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum GeometryInstanceFlagBitsKHR {
    eTriangleFacingCullDisableBit,
    eTriangleFlipFacingBit,
    eForceOpaqueBit,
    eForceNoOpaqueBit,
    eForceOpacityMicromap2StateExt,
    eDisableOpacityMicromapsExt,
    eUnknownBit(u32),
}

pub type GeometryInstanceFlagBitsNV = GeometryInstanceFlagBitsKHR;

#[allow(non_upper_case_globals)]
impl GeometryInstanceFlagBitsKHR {
    pub const eTriangleCullDisableBitNv: Self = Self::eTriangleFacingCullDisableBit;
    pub const eTriangleFrontCounterclockwiseBit: Self = Self::eTriangleFlipFacingBit;
    pub const eForceOpaqueBitNv: Self = Self::eForceOpaqueBit;
    pub const eForceNoOpaqueBitNv: Self = Self::eForceNoOpaqueBit;

    pub fn into_raw(self) -> RawGeometryInstanceFlagBitsKHR {
        match self {
            Self::eTriangleFacingCullDisableBit => RawGeometryInstanceFlagBitsKHR::eTriangleFacingCullDisableBit,
            Self::eTriangleFlipFacingBit => RawGeometryInstanceFlagBitsKHR::eTriangleFlipFacingBit,
            Self::eForceOpaqueBit => RawGeometryInstanceFlagBitsKHR::eForceOpaqueBit,
            Self::eForceNoOpaqueBit => RawGeometryInstanceFlagBitsKHR::eForceNoOpaqueBit,
            Self::eForceOpacityMicromap2StateExt => RawGeometryInstanceFlagBitsKHR::eForceOpacityMicromap2StateExt,
            Self::eDisableOpacityMicromapsExt => RawGeometryInstanceFlagBitsKHR::eDisableOpacityMicromapsExt,
            Self::eUnknownBit(b) => RawGeometryInstanceFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<GeometryInstanceFlagBitsKHR> for GeometryInstanceFlagsKHR {
    fn from(value: GeometryInstanceFlagBitsKHR) -> Self {
        match value {
            GeometryInstanceFlagBitsKHR::eTriangleFacingCullDisableBit => GeometryInstanceFlagsKHR::eTriangleFacingCullDisableBit,
            GeometryInstanceFlagBitsKHR::eTriangleFlipFacingBit => GeometryInstanceFlagsKHR::eTriangleFlipFacingBit,
            GeometryInstanceFlagBitsKHR::eForceOpaqueBit => GeometryInstanceFlagsKHR::eForceOpaqueBit,
            GeometryInstanceFlagBitsKHR::eForceNoOpaqueBit => GeometryInstanceFlagsKHR::eForceNoOpaqueBit,
            GeometryInstanceFlagBitsKHR::eForceOpacityMicromap2StateExt => GeometryInstanceFlagsKHR::eForceOpacityMicromap2StateExt,
            GeometryInstanceFlagBitsKHR::eDisableOpacityMicromapsExt => GeometryInstanceFlagsKHR::eDisableOpacityMicromapsExt,
            GeometryInstanceFlagBitsKHR::eUnknownBit(b) => GeometryInstanceFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(GeometryInstanceFlagBitsKHR, GeometryInstanceFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct GeometryInstanceFlagsKHR(pub(crate) u32);

use c2rust_bitfields::FieldType;

impl FieldType for GeometryInstanceFlagsKHR {
    const IS_SIGNED: bool = false;

    fn get_bit(&self, bit: usize) -> bool {
        self.0.get_bit(bit)
    }

    fn set_field(&self, field: &mut [u8], bit_range: (usize, usize)) {
        self.0.set_field(field, bit_range)
    }

    fn get_field(field: &[u8], bit_range: (usize, usize)) -> Self {
        GeometryInstanceFlagsKHR(u32::get_field(field, bit_range))
    }
}

pub type GeometryInstanceFlagsNV = GeometryInstanceFlagsKHR;

#[allow(non_upper_case_globals)]
impl GeometryInstanceFlagsKHR {
    pub const eNone: Self = Self(0);
    pub const eTriangleFacingCullDisableBit: Self = Self(0b1);
    pub const eTriangleFlipFacingBit: Self = Self(0b10);
    pub const eForceOpaqueBit: Self = Self(0b100);
    pub const eForceNoOpaqueBit: Self = Self(0b1000);
    pub const eForceOpacityMicromap2StateExt: Self = Self(0b10000);
    pub const eDisableOpacityMicromapsExt: Self = Self(0b100000);
    pub const eTriangleCullDisableBitNv: Self = Self::eTriangleFacingCullDisableBit;
    pub const eTriangleFrontCounterclockwiseBit: Self = Self::eTriangleFlipFacingBit;
    pub const eForceOpaqueBitNv: Self = Self::eForceOpaqueBit;
    pub const eForceNoOpaqueBitNv: Self = Self::eForceNoOpaqueBit;
}

bit_field_mask_derives!(GeometryInstanceFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(GeometryInstanceFlagBitsKHR, GeometryInstanceFlagsKHR, u32, GeometryInstanceFlagBitsKHR::eTriangleFacingCullDisableBit, GeometryInstanceFlagBitsKHR::eTriangleFlipFacingBit, GeometryInstanceFlagBitsKHR::eForceOpaqueBit, GeometryInstanceFlagBitsKHR::eForceNoOpaqueBit, GeometryInstanceFlagBitsKHR::eForceOpacityMicromap2StateExt, GeometryInstanceFlagBitsKHR::eDisableOpacityMicromapsExt);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum BuildAccelerationStructureFlagBitsKHR {
    eAllowUpdateBit,
    eAllowCompactionBit,
    ePreferFastTraceBit,
    ePreferFastBuildBit,
    eLowMemoryBit,
    eMotionBitNv,
    eAllowOpacityMicromapUpdateExt,
    eAllowDisableOpacityMicromapsExt,
    eAllowOpacityMicromapDataUpdateExt,
    eUnknownBit(u32),
}

pub type BuildAccelerationStructureFlagBitsNV = BuildAccelerationStructureFlagBitsKHR;

#[allow(non_upper_case_globals)]
impl BuildAccelerationStructureFlagBitsKHR {
    pub const eAllowUpdateBitNv: Self = Self::eAllowUpdateBit;
    pub const eAllowCompactionBitNv: Self = Self::eAllowCompactionBit;
    pub const ePreferFastTraceBitNv: Self = Self::ePreferFastTraceBit;
    pub const ePreferFastBuildBitNv: Self = Self::ePreferFastBuildBit;
    pub const eLowMemoryBitNv: Self = Self::eLowMemoryBit;

    pub fn into_raw(self) -> RawBuildAccelerationStructureFlagBitsKHR {
        match self {
            Self::eAllowUpdateBit => RawBuildAccelerationStructureFlagBitsKHR::eAllowUpdateBit,
            Self::eAllowCompactionBit => RawBuildAccelerationStructureFlagBitsKHR::eAllowCompactionBit,
            Self::ePreferFastTraceBit => RawBuildAccelerationStructureFlagBitsKHR::ePreferFastTraceBit,
            Self::ePreferFastBuildBit => RawBuildAccelerationStructureFlagBitsKHR::ePreferFastBuildBit,
            Self::eLowMemoryBit => RawBuildAccelerationStructureFlagBitsKHR::eLowMemoryBit,
            Self::eMotionBitNv => RawBuildAccelerationStructureFlagBitsKHR::eMotionBitNv,
            Self::eAllowOpacityMicromapUpdateExt => RawBuildAccelerationStructureFlagBitsKHR::eAllowOpacityMicromapUpdateExt,
            Self::eAllowDisableOpacityMicromapsExt => RawBuildAccelerationStructureFlagBitsKHR::eAllowDisableOpacityMicromapsExt,
            Self::eAllowOpacityMicromapDataUpdateExt => RawBuildAccelerationStructureFlagBitsKHR::eAllowOpacityMicromapDataUpdateExt,
            Self::eUnknownBit(b) => RawBuildAccelerationStructureFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<BuildAccelerationStructureFlagBitsKHR> for BuildAccelerationStructureFlagsKHR {
    fn from(value: BuildAccelerationStructureFlagBitsKHR) -> Self {
        match value {
            BuildAccelerationStructureFlagBitsKHR::eAllowUpdateBit => BuildAccelerationStructureFlagsKHR::eAllowUpdateBit,
            BuildAccelerationStructureFlagBitsKHR::eAllowCompactionBit => BuildAccelerationStructureFlagsKHR::eAllowCompactionBit,
            BuildAccelerationStructureFlagBitsKHR::ePreferFastTraceBit => BuildAccelerationStructureFlagsKHR::ePreferFastTraceBit,
            BuildAccelerationStructureFlagBitsKHR::ePreferFastBuildBit => BuildAccelerationStructureFlagsKHR::ePreferFastBuildBit,
            BuildAccelerationStructureFlagBitsKHR::eLowMemoryBit => BuildAccelerationStructureFlagsKHR::eLowMemoryBit,
            BuildAccelerationStructureFlagBitsKHR::eMotionBitNv => BuildAccelerationStructureFlagsKHR::eMotionBitNv,
            BuildAccelerationStructureFlagBitsKHR::eAllowOpacityMicromapUpdateExt => BuildAccelerationStructureFlagsKHR::eAllowOpacityMicromapUpdateExt,
            BuildAccelerationStructureFlagBitsKHR::eAllowDisableOpacityMicromapsExt => BuildAccelerationStructureFlagsKHR::eAllowDisableOpacityMicromapsExt,
            BuildAccelerationStructureFlagBitsKHR::eAllowOpacityMicromapDataUpdateExt => BuildAccelerationStructureFlagsKHR::eAllowOpacityMicromapDataUpdateExt,
            BuildAccelerationStructureFlagBitsKHR::eUnknownBit(b) => BuildAccelerationStructureFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(BuildAccelerationStructureFlagBitsKHR, BuildAccelerationStructureFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct BuildAccelerationStructureFlagsKHR(pub(crate) u32);

pub type BuildAccelerationStructureFlagsNV = BuildAccelerationStructureFlagsKHR;

#[allow(non_upper_case_globals)]
impl BuildAccelerationStructureFlagsKHR {
    pub const eNone: Self = Self(0);
    pub const eAllowUpdateBit: Self = Self(0b1);
    pub const eAllowCompactionBit: Self = Self(0b10);
    pub const ePreferFastTraceBit: Self = Self(0b100);
    pub const ePreferFastBuildBit: Self = Self(0b1000);
    pub const eLowMemoryBit: Self = Self(0b10000);
    pub const eMotionBitNv: Self = Self(0b100000);
    pub const eAllowOpacityMicromapUpdateExt: Self = Self(0b1000000);
    pub const eAllowDisableOpacityMicromapsExt: Self = Self(0b10000000);
    pub const eAllowOpacityMicromapDataUpdateExt: Self = Self(0b100000000);
    pub const eAllowUpdateBitNv: Self = Self::eAllowUpdateBit;
    pub const eAllowCompactionBitNv: Self = Self::eAllowCompactionBit;
    pub const ePreferFastTraceBitNv: Self = Self::ePreferFastTraceBit;
    pub const ePreferFastBuildBitNv: Self = Self::ePreferFastBuildBit;
    pub const eLowMemoryBitNv: Self = Self::eLowMemoryBit;
}

bit_field_mask_derives!(BuildAccelerationStructureFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(BuildAccelerationStructureFlagBitsKHR, BuildAccelerationStructureFlagsKHR, u32, BuildAccelerationStructureFlagBitsKHR::eAllowUpdateBit, BuildAccelerationStructureFlagBitsKHR::eAllowCompactionBit, BuildAccelerationStructureFlagBitsKHR::ePreferFastTraceBit, BuildAccelerationStructureFlagBitsKHR::ePreferFastBuildBit, BuildAccelerationStructureFlagBitsKHR::eLowMemoryBit, BuildAccelerationStructureFlagBitsKHR::eMotionBitNv, BuildAccelerationStructureFlagBitsKHR::eAllowOpacityMicromapUpdateExt, BuildAccelerationStructureFlagBitsKHR::eAllowDisableOpacityMicromapsExt, BuildAccelerationStructureFlagBitsKHR::eAllowOpacityMicromapDataUpdateExt);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PrivateDataSlotCreateFlags(pub(crate) u32);

pub type PrivateDataSlotCreateFlagsEXT = PrivateDataSlotCreateFlags;

#[allow(non_upper_case_globals)]
impl PrivateDataSlotCreateFlags {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(PrivateDataSlotCreateFlags, u32);

fieldless_debug_derive!(PrivateDataSlotCreateFlags);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum AccelerationStructureCreateFlagBitsKHR {
    eDeviceAddressCaptureReplayBit,
    eMotionBitNv,
    eDescriptorBufferCaptureReplayBitExt,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl AccelerationStructureCreateFlagBitsKHR {
    pub fn into_raw(self) -> RawAccelerationStructureCreateFlagBitsKHR {
        match self {
            Self::eDeviceAddressCaptureReplayBit => RawAccelerationStructureCreateFlagBitsKHR::eDeviceAddressCaptureReplayBit,
            Self::eMotionBitNv => RawAccelerationStructureCreateFlagBitsKHR::eMotionBitNv,
            Self::eDescriptorBufferCaptureReplayBitExt => RawAccelerationStructureCreateFlagBitsKHR::eDescriptorBufferCaptureReplayBitExt,
            Self::eUnknownBit(b) => RawAccelerationStructureCreateFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<AccelerationStructureCreateFlagBitsKHR> for AccelerationStructureCreateFlagsKHR {
    fn from(value: AccelerationStructureCreateFlagBitsKHR) -> Self {
        match value {
            AccelerationStructureCreateFlagBitsKHR::eDeviceAddressCaptureReplayBit => AccelerationStructureCreateFlagsKHR::eDeviceAddressCaptureReplayBit,
            AccelerationStructureCreateFlagBitsKHR::eMotionBitNv => AccelerationStructureCreateFlagsKHR::eMotionBitNv,
            AccelerationStructureCreateFlagBitsKHR::eDescriptorBufferCaptureReplayBitExt => AccelerationStructureCreateFlagsKHR::eDescriptorBufferCaptureReplayBitExt,
            AccelerationStructureCreateFlagBitsKHR::eUnknownBit(b) => AccelerationStructureCreateFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(AccelerationStructureCreateFlagBitsKHR, AccelerationStructureCreateFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct AccelerationStructureCreateFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl AccelerationStructureCreateFlagsKHR {
    pub const eNone: Self = Self(0);
    pub const eDeviceAddressCaptureReplayBit: Self = Self(0b1);
    pub const eMotionBitNv: Self = Self(0b100);
    pub const eDescriptorBufferCaptureReplayBitExt: Self = Self(0b1000);
}

bit_field_mask_derives!(AccelerationStructureCreateFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(AccelerationStructureCreateFlagBitsKHR, AccelerationStructureCreateFlagsKHR, u32, AccelerationStructureCreateFlagBitsKHR::eDeviceAddressCaptureReplayBit, AccelerationStructureCreateFlagBitsKHR::eMotionBitNv, AccelerationStructureCreateFlagBitsKHR::eDescriptorBufferCaptureReplayBitExt);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DescriptorUpdateTemplateCreateFlags(pub(crate) u32);

pub type DescriptorUpdateTemplateCreateFlagsKHR = DescriptorUpdateTemplateCreateFlags;

#[allow(non_upper_case_globals)]
impl DescriptorUpdateTemplateCreateFlags {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(DescriptorUpdateTemplateCreateFlags, u32);

fieldless_debug_derive!(DescriptorUpdateTemplateCreateFlags);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PipelineCreationFeedbackFlagBits {
    eValidBit,
    eApplicationPipelineCacheHitBit,
    eBasePipelineAccelerationBit,
    eUnknownBit(u32),
}

pub type PipelineCreationFeedbackFlagBitsEXT = PipelineCreationFeedbackFlagBits;

#[allow(non_upper_case_globals)]
impl PipelineCreationFeedbackFlagBits {
    pub const eValidBitExt: Self = Self::eValidBit;
    pub const eApplicationPipelineCacheHitBitExt: Self = Self::eApplicationPipelineCacheHitBit;
    pub const eBasePipelineAccelerationBitExt: Self = Self::eBasePipelineAccelerationBit;

    pub fn into_raw(self) -> RawPipelineCreationFeedbackFlagBits {
        match self {
            Self::eValidBit => RawPipelineCreationFeedbackFlagBits::eValidBit,
            Self::eApplicationPipelineCacheHitBit => RawPipelineCreationFeedbackFlagBits::eApplicationPipelineCacheHitBit,
            Self::eBasePipelineAccelerationBit => RawPipelineCreationFeedbackFlagBits::eBasePipelineAccelerationBit,
            Self::eUnknownBit(b) => RawPipelineCreationFeedbackFlagBits(b),
        }
    }
}

impl core::convert::From<PipelineCreationFeedbackFlagBits> for PipelineCreationFeedbackFlags {
    fn from(value: PipelineCreationFeedbackFlagBits) -> Self {
        match value {
            PipelineCreationFeedbackFlagBits::eValidBit => PipelineCreationFeedbackFlags::eValidBit,
            PipelineCreationFeedbackFlagBits::eApplicationPipelineCacheHitBit => PipelineCreationFeedbackFlags::eApplicationPipelineCacheHitBit,
            PipelineCreationFeedbackFlagBits::eBasePipelineAccelerationBit => PipelineCreationFeedbackFlags::eBasePipelineAccelerationBit,
            PipelineCreationFeedbackFlagBits::eUnknownBit(b) => PipelineCreationFeedbackFlags(b),
        }
    }
}

bit_field_enum_derives!(PipelineCreationFeedbackFlagBits, PipelineCreationFeedbackFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineCreationFeedbackFlags(pub(crate) u32);

pub type PipelineCreationFeedbackFlagsEXT = PipelineCreationFeedbackFlags;

#[allow(non_upper_case_globals)]
impl PipelineCreationFeedbackFlags {
    pub const eNone: Self = Self(0);
    pub const eValidBit: Self = Self(0b1);
    pub const eApplicationPipelineCacheHitBit: Self = Self(0b10);
    pub const eBasePipelineAccelerationBit: Self = Self(0b100);
    pub const eValidBitExt: Self = Self::eValidBit;
    pub const eApplicationPipelineCacheHitBitExt: Self = Self::eApplicationPipelineCacheHitBit;
    pub const eBasePipelineAccelerationBitExt: Self = Self::eBasePipelineAccelerationBit;
}

bit_field_mask_derives!(PipelineCreationFeedbackFlags, u32);

bit_field_mask_with_enum_debug_derive!(PipelineCreationFeedbackFlagBits, PipelineCreationFeedbackFlags, u32, PipelineCreationFeedbackFlagBits::eValidBit, PipelineCreationFeedbackFlagBits::eApplicationPipelineCacheHitBit, PipelineCreationFeedbackFlagBits::eBasePipelineAccelerationBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PerformanceCounterDescriptionFlagBitsKHR {
    ePerformanceImpactingBit,
    eConcurrentlyImpactedBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl PerformanceCounterDescriptionFlagBitsKHR {
    pub const ePerformanceImpacting: Self = Self::ePerformanceImpactingBit;
    pub const eConcurrentlyImpacted: Self = Self::eConcurrentlyImpactedBit;

    pub fn into_raw(self) -> RawPerformanceCounterDescriptionFlagBitsKHR {
        match self {
            Self::ePerformanceImpactingBit => RawPerformanceCounterDescriptionFlagBitsKHR::ePerformanceImpactingBit,
            Self::eConcurrentlyImpactedBit => RawPerformanceCounterDescriptionFlagBitsKHR::eConcurrentlyImpactedBit,
            Self::eUnknownBit(b) => RawPerformanceCounterDescriptionFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<PerformanceCounterDescriptionFlagBitsKHR> for PerformanceCounterDescriptionFlagsKHR {
    fn from(value: PerformanceCounterDescriptionFlagBitsKHR) -> Self {
        match value {
            PerformanceCounterDescriptionFlagBitsKHR::ePerformanceImpactingBit => PerformanceCounterDescriptionFlagsKHR::ePerformanceImpactingBit,
            PerformanceCounterDescriptionFlagBitsKHR::eConcurrentlyImpactedBit => PerformanceCounterDescriptionFlagsKHR::eConcurrentlyImpactedBit,
            PerformanceCounterDescriptionFlagBitsKHR::eUnknownBit(b) => PerformanceCounterDescriptionFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(PerformanceCounterDescriptionFlagBitsKHR, PerformanceCounterDescriptionFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PerformanceCounterDescriptionFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PerformanceCounterDescriptionFlagsKHR {
    pub const eNone: Self = Self(0);
    pub const ePerformanceImpactingBit: Self = Self(0b1);
    pub const eConcurrentlyImpactedBit: Self = Self(0b10);
    pub const ePerformanceImpacting: Self = Self::ePerformanceImpactingBit;
    pub const eConcurrentlyImpacted: Self = Self::eConcurrentlyImpactedBit;
}

bit_field_mask_derives!(PerformanceCounterDescriptionFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(PerformanceCounterDescriptionFlagBitsKHR, PerformanceCounterDescriptionFlagsKHR, u32, PerformanceCounterDescriptionFlagBitsKHR::ePerformanceImpactingBit, PerformanceCounterDescriptionFlagBitsKHR::eConcurrentlyImpactedBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum AcquireProfilingLockFlagBitsKHR {
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl AcquireProfilingLockFlagBitsKHR {
    pub fn into_raw(self) -> RawAcquireProfilingLockFlagBitsKHR {
        match self {
            Self::eUnknownBit(b) => RawAcquireProfilingLockFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<AcquireProfilingLockFlagBitsKHR> for AcquireProfilingLockFlagsKHR {
    fn from(value: AcquireProfilingLockFlagBitsKHR) -> Self {
        match value {
            AcquireProfilingLockFlagBitsKHR::eUnknownBit(b) => AcquireProfilingLockFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(AcquireProfilingLockFlagBitsKHR, AcquireProfilingLockFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct AcquireProfilingLockFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl AcquireProfilingLockFlagsKHR {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(AcquireProfilingLockFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(AcquireProfilingLockFlagBitsKHR, AcquireProfilingLockFlagsKHR, u32, );

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SemaphoreWaitFlagBits {
    eAnyBit,
    eUnknownBit(u32),
}

pub type SemaphoreWaitFlagBitsKHR = SemaphoreWaitFlagBits;

#[allow(non_upper_case_globals)]
impl SemaphoreWaitFlagBits {
    pub const eAnyBitKhr: Self = Self::eAnyBit;

    pub fn into_raw(self) -> RawSemaphoreWaitFlagBits {
        match self {
            Self::eAnyBit => RawSemaphoreWaitFlagBits::eAnyBit,
            Self::eUnknownBit(b) => RawSemaphoreWaitFlagBits(b),
        }
    }
}

impl core::convert::From<SemaphoreWaitFlagBits> for SemaphoreWaitFlags {
    fn from(value: SemaphoreWaitFlagBits) -> Self {
        match value {
            SemaphoreWaitFlagBits::eAnyBit => SemaphoreWaitFlags::eAnyBit,
            SemaphoreWaitFlagBits::eUnknownBit(b) => SemaphoreWaitFlags(b),
        }
    }
}

bit_field_enum_derives!(SemaphoreWaitFlagBits, SemaphoreWaitFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct SemaphoreWaitFlags(pub(crate) u32);

pub type SemaphoreWaitFlagsKHR = SemaphoreWaitFlags;

#[allow(non_upper_case_globals)]
impl SemaphoreWaitFlags {
    pub const eNone: Self = Self(0);
    pub const eAnyBit: Self = Self(0b1);
    pub const eAnyBitKhr: Self = Self::eAnyBit;
}

bit_field_mask_derives!(SemaphoreWaitFlags, u32);

bit_field_mask_with_enum_debug_derive!(SemaphoreWaitFlagBits, SemaphoreWaitFlags, u32, SemaphoreWaitFlagBits::eAnyBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PipelineCompilerControlFlagBitsAMD {
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl PipelineCompilerControlFlagBitsAMD {
    pub fn into_raw(self) -> RawPipelineCompilerControlFlagBitsAMD {
        match self {
            Self::eUnknownBit(b) => RawPipelineCompilerControlFlagBitsAMD(b),
        }
    }
}

impl core::convert::From<PipelineCompilerControlFlagBitsAMD> for PipelineCompilerControlFlagsAMD {
    fn from(value: PipelineCompilerControlFlagBitsAMD) -> Self {
        match value {
            PipelineCompilerControlFlagBitsAMD::eUnknownBit(b) => PipelineCompilerControlFlagsAMD(b),
        }
    }
}

bit_field_enum_derives!(PipelineCompilerControlFlagBitsAMD, PipelineCompilerControlFlagsAMD, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineCompilerControlFlagsAMD(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineCompilerControlFlagsAMD {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(PipelineCompilerControlFlagsAMD, u32);

bit_field_mask_with_enum_debug_derive!(PipelineCompilerControlFlagBitsAMD, PipelineCompilerControlFlagsAMD, u32, );

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ShaderCorePropertiesFlagBitsAMD {
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl ShaderCorePropertiesFlagBitsAMD {
    pub fn into_raw(self) -> RawShaderCorePropertiesFlagBitsAMD {
        match self {
            Self::eUnknownBit(b) => RawShaderCorePropertiesFlagBitsAMD(b),
        }
    }
}

impl core::convert::From<ShaderCorePropertiesFlagBitsAMD> for ShaderCorePropertiesFlagsAMD {
    fn from(value: ShaderCorePropertiesFlagBitsAMD) -> Self {
        match value {
            ShaderCorePropertiesFlagBitsAMD::eUnknownBit(b) => ShaderCorePropertiesFlagsAMD(b),
        }
    }
}

bit_field_enum_derives!(ShaderCorePropertiesFlagBitsAMD, ShaderCorePropertiesFlagsAMD, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ShaderCorePropertiesFlagsAMD(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ShaderCorePropertiesFlagsAMD {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(ShaderCorePropertiesFlagsAMD, u32);

bit_field_mask_with_enum_debug_derive!(ShaderCorePropertiesFlagBitsAMD, ShaderCorePropertiesFlagsAMD, u32, );

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DeviceDiagnosticsConfigFlagBitsNV {
    eEnableShaderDebugInfoBit,
    eEnableResourceTrackingBit,
    eEnableAutomaticCheckpointsBit,
    eEnableShaderErrorReportingBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl DeviceDiagnosticsConfigFlagBitsNV {
    pub fn into_raw(self) -> RawDeviceDiagnosticsConfigFlagBitsNV {
        match self {
            Self::eEnableShaderDebugInfoBit => RawDeviceDiagnosticsConfigFlagBitsNV::eEnableShaderDebugInfoBit,
            Self::eEnableResourceTrackingBit => RawDeviceDiagnosticsConfigFlagBitsNV::eEnableResourceTrackingBit,
            Self::eEnableAutomaticCheckpointsBit => RawDeviceDiagnosticsConfigFlagBitsNV::eEnableAutomaticCheckpointsBit,
            Self::eEnableShaderErrorReportingBit => RawDeviceDiagnosticsConfigFlagBitsNV::eEnableShaderErrorReportingBit,
            Self::eUnknownBit(b) => RawDeviceDiagnosticsConfigFlagBitsNV(b),
        }
    }
}

impl core::convert::From<DeviceDiagnosticsConfigFlagBitsNV> for DeviceDiagnosticsConfigFlagsNV {
    fn from(value: DeviceDiagnosticsConfigFlagBitsNV) -> Self {
        match value {
            DeviceDiagnosticsConfigFlagBitsNV::eEnableShaderDebugInfoBit => DeviceDiagnosticsConfigFlagsNV::eEnableShaderDebugInfoBit,
            DeviceDiagnosticsConfigFlagBitsNV::eEnableResourceTrackingBit => DeviceDiagnosticsConfigFlagsNV::eEnableResourceTrackingBit,
            DeviceDiagnosticsConfigFlagBitsNV::eEnableAutomaticCheckpointsBit => DeviceDiagnosticsConfigFlagsNV::eEnableAutomaticCheckpointsBit,
            DeviceDiagnosticsConfigFlagBitsNV::eEnableShaderErrorReportingBit => DeviceDiagnosticsConfigFlagsNV::eEnableShaderErrorReportingBit,
            DeviceDiagnosticsConfigFlagBitsNV::eUnknownBit(b) => DeviceDiagnosticsConfigFlagsNV(b),
        }
    }
}

bit_field_enum_derives!(DeviceDiagnosticsConfigFlagBitsNV, DeviceDiagnosticsConfigFlagsNV, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DeviceDiagnosticsConfigFlagsNV(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DeviceDiagnosticsConfigFlagsNV {
    pub const eNone: Self = Self(0);
    pub const eEnableShaderDebugInfoBit: Self = Self(0b1);
    pub const eEnableResourceTrackingBit: Self = Self(0b10);
    pub const eEnableAutomaticCheckpointsBit: Self = Self(0b100);
    pub const eEnableShaderErrorReportingBit: Self = Self(0b1000);
}

bit_field_mask_derives!(DeviceDiagnosticsConfigFlagsNV, u32);

bit_field_mask_with_enum_debug_derive!(DeviceDiagnosticsConfigFlagBitsNV, DeviceDiagnosticsConfigFlagsNV, u32, DeviceDiagnosticsConfigFlagBitsNV::eEnableShaderDebugInfoBit, DeviceDiagnosticsConfigFlagBitsNV::eEnableResourceTrackingBit, DeviceDiagnosticsConfigFlagBitsNV::eEnableAutomaticCheckpointsBit, DeviceDiagnosticsConfigFlagBitsNV::eEnableShaderErrorReportingBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum AccessFlagBits2 {
    eNone,
    eIndirectCommandReadBit,
    eIndexReadBit,
    eVertexAttributeReadBit,
    eUniformReadBit,
    eInputAttachmentReadBit,
    eShaderReadBit,
    eShaderWriteBit,
    eColourAttachmentReadBit,
    eColourAttachmentWriteBit,
    eDepthStencilAttachmentReadBit,
    eDepthStencilAttachmentWriteBit,
    eTransferReadBit,
    eTransferWriteBit,
    eHostReadBit,
    eHostWriteBit,
    eMemoryReadBit,
    eMemoryWriteBit,
    eCommandPreprocessReadBitNv,
    eCommandPreprocessWriteBitNv,
    eColourAttachmentReadNoncoherentBitExt,
    eConditionalRenderingReadBitExt,
    eAccelerationStructureReadBitKhr,
    eAccelerationStructureWriteBitKhr,
    eFragmentShadingRateAttachmentReadBitKhr,
    eFragmentDensityMapReadBitExt,
    eTransformFeedbackWriteBitExt,
    eTransformFeedbackCounterReadBitExt,
    eTransformFeedbackCounterWriteBitExt,
    eShaderSampledReadBit,
    eShaderStorageReadBit,
    eShaderStorageWriteBit,
    eVideoDecodeReadBitKhr,
    eVideoDecodeWriteBitKhr,
    eVideoEncodeReadBitKhr,
    eVideoEncodeWriteBitKhr,
    eInvocationMaskReadBitHuawei,
    eShaderBindingTableReadBitKhr,
    eDescriptorBufferReadBitExt,
    eOpticalFlowReadBitNv,
    eOpticalFlowWriteBitNv,
    eMicromapReadBitExt,
    eMicromapWriteBitExt,
    eUnknownBit(u64),
}

pub type AccessFlagBits2KHR = AccessFlagBits2;

#[allow(non_upper_case_globals)]
impl AccessFlagBits2 {
    pub const eNoneKhr: Self = Self::eNone;
    pub const eIndirectCommandReadBitKhr: Self = Self::eIndirectCommandReadBit;
    pub const eIndexReadBitKhr: Self = Self::eIndexReadBit;
    pub const eVertexAttributeReadBitKhr: Self = Self::eVertexAttributeReadBit;
    pub const eUniformReadBitKhr: Self = Self::eUniformReadBit;
    pub const eInputAttachmentReadBitKhr: Self = Self::eInputAttachmentReadBit;
    pub const eShaderReadBitKhr: Self = Self::eShaderReadBit;
    pub const eShaderWriteBitKhr: Self = Self::eShaderWriteBit;
    pub const eColourAttachmentReadBitKhr: Self = Self::eColourAttachmentReadBit;
    pub const eColourAttachmentWriteBitKhr: Self = Self::eColourAttachmentWriteBit;
    pub const eDepthStencilAttachmentReadBitKhr: Self = Self::eDepthStencilAttachmentReadBit;
    pub const eDepthStencilAttachmentWriteBitKhr: Self = Self::eDepthStencilAttachmentWriteBit;
    pub const eTransferReadBitKhr: Self = Self::eTransferReadBit;
    pub const eTransferWriteBitKhr: Self = Self::eTransferWriteBit;
    pub const eHostReadBitKhr: Self = Self::eHostReadBit;
    pub const eHostWriteBitKhr: Self = Self::eHostWriteBit;
    pub const eMemoryReadBitKhr: Self = Self::eMemoryReadBit;
    pub const eMemoryWriteBitKhr: Self = Self::eMemoryWriteBit;
    pub const eAccelerationStructureReadBitNv: Self = Self::eAccelerationStructureReadBitKhr;
    pub const eAccelerationStructureWriteBitNv: Self = Self::eAccelerationStructureWriteBitKhr;
    pub const eShadingRateImageReadBitNv: Self = Self::eFragmentShadingRateAttachmentReadBitKhr;
    pub const eShaderSampledReadBitKhr: Self = Self::eShaderSampledReadBit;
    pub const eShaderStorageReadBitKhr: Self = Self::eShaderStorageReadBit;
    pub const eShaderStorageWriteBitKhr: Self = Self::eShaderStorageWriteBit;

    pub fn into_raw(self) -> RawAccessFlagBits2 {
        match self {
            Self::eNone => RawAccessFlagBits2::eNone,
            Self::eIndirectCommandReadBit => RawAccessFlagBits2::eIndirectCommandReadBit,
            Self::eIndexReadBit => RawAccessFlagBits2::eIndexReadBit,
            Self::eVertexAttributeReadBit => RawAccessFlagBits2::eVertexAttributeReadBit,
            Self::eUniformReadBit => RawAccessFlagBits2::eUniformReadBit,
            Self::eInputAttachmentReadBit => RawAccessFlagBits2::eInputAttachmentReadBit,
            Self::eShaderReadBit => RawAccessFlagBits2::eShaderReadBit,
            Self::eShaderWriteBit => RawAccessFlagBits2::eShaderWriteBit,
            Self::eColourAttachmentReadBit => RawAccessFlagBits2::eColourAttachmentReadBit,
            Self::eColourAttachmentWriteBit => RawAccessFlagBits2::eColourAttachmentWriteBit,
            Self::eDepthStencilAttachmentReadBit => RawAccessFlagBits2::eDepthStencilAttachmentReadBit,
            Self::eDepthStencilAttachmentWriteBit => RawAccessFlagBits2::eDepthStencilAttachmentWriteBit,
            Self::eTransferReadBit => RawAccessFlagBits2::eTransferReadBit,
            Self::eTransferWriteBit => RawAccessFlagBits2::eTransferWriteBit,
            Self::eHostReadBit => RawAccessFlagBits2::eHostReadBit,
            Self::eHostWriteBit => RawAccessFlagBits2::eHostWriteBit,
            Self::eMemoryReadBit => RawAccessFlagBits2::eMemoryReadBit,
            Self::eMemoryWriteBit => RawAccessFlagBits2::eMemoryWriteBit,
            Self::eCommandPreprocessReadBitNv => RawAccessFlagBits2::eCommandPreprocessReadBitNv,
            Self::eCommandPreprocessWriteBitNv => RawAccessFlagBits2::eCommandPreprocessWriteBitNv,
            Self::eColourAttachmentReadNoncoherentBitExt => RawAccessFlagBits2::eColourAttachmentReadNoncoherentBitExt,
            Self::eConditionalRenderingReadBitExt => RawAccessFlagBits2::eConditionalRenderingReadBitExt,
            Self::eAccelerationStructureReadBitKhr => RawAccessFlagBits2::eAccelerationStructureReadBitKhr,
            Self::eAccelerationStructureWriteBitKhr => RawAccessFlagBits2::eAccelerationStructureWriteBitKhr,
            Self::eFragmentShadingRateAttachmentReadBitKhr => RawAccessFlagBits2::eFragmentShadingRateAttachmentReadBitKhr,
            Self::eFragmentDensityMapReadBitExt => RawAccessFlagBits2::eFragmentDensityMapReadBitExt,
            Self::eTransformFeedbackWriteBitExt => RawAccessFlagBits2::eTransformFeedbackWriteBitExt,
            Self::eTransformFeedbackCounterReadBitExt => RawAccessFlagBits2::eTransformFeedbackCounterReadBitExt,
            Self::eTransformFeedbackCounterWriteBitExt => RawAccessFlagBits2::eTransformFeedbackCounterWriteBitExt,
            Self::eShaderSampledReadBit => RawAccessFlagBits2::eShaderSampledReadBit,
            Self::eShaderStorageReadBit => RawAccessFlagBits2::eShaderStorageReadBit,
            Self::eShaderStorageWriteBit => RawAccessFlagBits2::eShaderStorageWriteBit,
            Self::eVideoDecodeReadBitKhr => RawAccessFlagBits2::eVideoDecodeReadBitKhr,
            Self::eVideoDecodeWriteBitKhr => RawAccessFlagBits2::eVideoDecodeWriteBitKhr,
            Self::eVideoEncodeReadBitKhr => RawAccessFlagBits2::eVideoEncodeReadBitKhr,
            Self::eVideoEncodeWriteBitKhr => RawAccessFlagBits2::eVideoEncodeWriteBitKhr,
            Self::eInvocationMaskReadBitHuawei => RawAccessFlagBits2::eInvocationMaskReadBitHuawei,
            Self::eShaderBindingTableReadBitKhr => RawAccessFlagBits2::eShaderBindingTableReadBitKhr,
            Self::eDescriptorBufferReadBitExt => RawAccessFlagBits2::eDescriptorBufferReadBitExt,
            Self::eOpticalFlowReadBitNv => RawAccessFlagBits2::eOpticalFlowReadBitNv,
            Self::eOpticalFlowWriteBitNv => RawAccessFlagBits2::eOpticalFlowWriteBitNv,
            Self::eMicromapReadBitExt => RawAccessFlagBits2::eMicromapReadBitExt,
            Self::eMicromapWriteBitExt => RawAccessFlagBits2::eMicromapWriteBitExt,
            Self::eUnknownBit(b) => RawAccessFlagBits2(b),
        }
    }
}

impl core::convert::From<AccessFlagBits2> for AccessFlags2 {
    fn from(value: AccessFlagBits2) -> Self {
        match value {
            AccessFlagBits2::eNone => AccessFlags2::eNone,
            AccessFlagBits2::eIndirectCommandReadBit => AccessFlags2::eIndirectCommandReadBit,
            AccessFlagBits2::eIndexReadBit => AccessFlags2::eIndexReadBit,
            AccessFlagBits2::eVertexAttributeReadBit => AccessFlags2::eVertexAttributeReadBit,
            AccessFlagBits2::eUniformReadBit => AccessFlags2::eUniformReadBit,
            AccessFlagBits2::eInputAttachmentReadBit => AccessFlags2::eInputAttachmentReadBit,
            AccessFlagBits2::eShaderReadBit => AccessFlags2::eShaderReadBit,
            AccessFlagBits2::eShaderWriteBit => AccessFlags2::eShaderWriteBit,
            AccessFlagBits2::eColourAttachmentReadBit => AccessFlags2::eColourAttachmentReadBit,
            AccessFlagBits2::eColourAttachmentWriteBit => AccessFlags2::eColourAttachmentWriteBit,
            AccessFlagBits2::eDepthStencilAttachmentReadBit => AccessFlags2::eDepthStencilAttachmentReadBit,
            AccessFlagBits2::eDepthStencilAttachmentWriteBit => AccessFlags2::eDepthStencilAttachmentWriteBit,
            AccessFlagBits2::eTransferReadBit => AccessFlags2::eTransferReadBit,
            AccessFlagBits2::eTransferWriteBit => AccessFlags2::eTransferWriteBit,
            AccessFlagBits2::eHostReadBit => AccessFlags2::eHostReadBit,
            AccessFlagBits2::eHostWriteBit => AccessFlags2::eHostWriteBit,
            AccessFlagBits2::eMemoryReadBit => AccessFlags2::eMemoryReadBit,
            AccessFlagBits2::eMemoryWriteBit => AccessFlags2::eMemoryWriteBit,
            AccessFlagBits2::eCommandPreprocessReadBitNv => AccessFlags2::eCommandPreprocessReadBitNv,
            AccessFlagBits2::eCommandPreprocessWriteBitNv => AccessFlags2::eCommandPreprocessWriteBitNv,
            AccessFlagBits2::eColourAttachmentReadNoncoherentBitExt => AccessFlags2::eColourAttachmentReadNoncoherentBitExt,
            AccessFlagBits2::eConditionalRenderingReadBitExt => AccessFlags2::eConditionalRenderingReadBitExt,
            AccessFlagBits2::eAccelerationStructureReadBitKhr => AccessFlags2::eAccelerationStructureReadBitKhr,
            AccessFlagBits2::eAccelerationStructureWriteBitKhr => AccessFlags2::eAccelerationStructureWriteBitKhr,
            AccessFlagBits2::eFragmentShadingRateAttachmentReadBitKhr => AccessFlags2::eFragmentShadingRateAttachmentReadBitKhr,
            AccessFlagBits2::eFragmentDensityMapReadBitExt => AccessFlags2::eFragmentDensityMapReadBitExt,
            AccessFlagBits2::eTransformFeedbackWriteBitExt => AccessFlags2::eTransformFeedbackWriteBitExt,
            AccessFlagBits2::eTransformFeedbackCounterReadBitExt => AccessFlags2::eTransformFeedbackCounterReadBitExt,
            AccessFlagBits2::eTransformFeedbackCounterWriteBitExt => AccessFlags2::eTransformFeedbackCounterWriteBitExt,
            AccessFlagBits2::eShaderSampledReadBit => AccessFlags2::eShaderSampledReadBit,
            AccessFlagBits2::eShaderStorageReadBit => AccessFlags2::eShaderStorageReadBit,
            AccessFlagBits2::eShaderStorageWriteBit => AccessFlags2::eShaderStorageWriteBit,
            AccessFlagBits2::eVideoDecodeReadBitKhr => AccessFlags2::eVideoDecodeReadBitKhr,
            AccessFlagBits2::eVideoDecodeWriteBitKhr => AccessFlags2::eVideoDecodeWriteBitKhr,
            AccessFlagBits2::eVideoEncodeReadBitKhr => AccessFlags2::eVideoEncodeReadBitKhr,
            AccessFlagBits2::eVideoEncodeWriteBitKhr => AccessFlags2::eVideoEncodeWriteBitKhr,
            AccessFlagBits2::eInvocationMaskReadBitHuawei => AccessFlags2::eInvocationMaskReadBitHuawei,
            AccessFlagBits2::eShaderBindingTableReadBitKhr => AccessFlags2::eShaderBindingTableReadBitKhr,
            AccessFlagBits2::eDescriptorBufferReadBitExt => AccessFlags2::eDescriptorBufferReadBitExt,
            AccessFlagBits2::eOpticalFlowReadBitNv => AccessFlags2::eOpticalFlowReadBitNv,
            AccessFlagBits2::eOpticalFlowWriteBitNv => AccessFlags2::eOpticalFlowWriteBitNv,
            AccessFlagBits2::eMicromapReadBitExt => AccessFlags2::eMicromapReadBitExt,
            AccessFlagBits2::eMicromapWriteBitExt => AccessFlags2::eMicromapWriteBitExt,
            AccessFlagBits2::eUnknownBit(b) => AccessFlags2(b),
        }
    }
}

bit_field_enum_derives!(AccessFlagBits2, AccessFlags2, u64);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct AccessFlags2(pub(crate) u64);

pub type AccessFlags2KHR = AccessFlags2;

#[allow(non_upper_case_globals)]
impl AccessFlags2 {
    pub const eNone: Self = Self(0);
    pub const eIndirectCommandReadBit: Self = Self(0b1);
    pub const eIndexReadBit: Self = Self(0b10);
    pub const eVertexAttributeReadBit: Self = Self(0b100);
    pub const eUniformReadBit: Self = Self(0b1000);
    pub const eInputAttachmentReadBit: Self = Self(0b10000);
    pub const eShaderReadBit: Self = Self(0b100000);
    pub const eShaderWriteBit: Self = Self(0b1000000);
    pub const eColourAttachmentReadBit: Self = Self(0b10000000);
    pub const eColourAttachmentWriteBit: Self = Self(0b100000000);
    pub const eDepthStencilAttachmentReadBit: Self = Self(0b1000000000);
    pub const eDepthStencilAttachmentWriteBit: Self = Self(0b10000000000);
    pub const eTransferReadBit: Self = Self(0b100000000000);
    pub const eTransferWriteBit: Self = Self(0b1000000000000);
    pub const eHostReadBit: Self = Self(0b10000000000000);
    pub const eHostWriteBit: Self = Self(0b100000000000000);
    pub const eMemoryReadBit: Self = Self(0b1000000000000000);
    pub const eMemoryWriteBit: Self = Self(0b10000000000000000);
    pub const eCommandPreprocessReadBitNv: Self = Self(0b100000000000000000);
    pub const eCommandPreprocessWriteBitNv: Self = Self(0b1000000000000000000);
    pub const eColourAttachmentReadNoncoherentBitExt: Self = Self(0b10000000000000000000);
    pub const eConditionalRenderingReadBitExt: Self = Self(0b100000000000000000000);
    pub const eAccelerationStructureReadBitKhr: Self = Self(0b1000000000000000000000);
    pub const eAccelerationStructureWriteBitKhr: Self = Self(0b10000000000000000000000);
    pub const eFragmentShadingRateAttachmentReadBitKhr: Self = Self(0b100000000000000000000000);
    pub const eFragmentDensityMapReadBitExt: Self = Self(0b1000000000000000000000000);
    pub const eTransformFeedbackWriteBitExt: Self = Self(0b10000000000000000000000000);
    pub const eTransformFeedbackCounterReadBitExt: Self = Self(0b100000000000000000000000000);
    pub const eTransformFeedbackCounterWriteBitExt: Self = Self(0b1000000000000000000000000000);
    pub const eShaderSampledReadBit: Self = Self(0b100000000000000000000000000000000);
    pub const eShaderStorageReadBit: Self = Self(0b1000000000000000000000000000000000);
    pub const eShaderStorageWriteBit: Self = Self(0b10000000000000000000000000000000000);
    pub const eVideoDecodeReadBitKhr: Self = Self(0b100000000000000000000000000000000000);
    pub const eVideoDecodeWriteBitKhr: Self = Self(0b1000000000000000000000000000000000000);
    pub const eVideoEncodeReadBitKhr: Self = Self(0b10000000000000000000000000000000000000);
    pub const eVideoEncodeWriteBitKhr: Self = Self(0b100000000000000000000000000000000000000);
    pub const eInvocationMaskReadBitHuawei: Self = Self(0b1000000000000000000000000000000000000000);
    pub const eShaderBindingTableReadBitKhr: Self = Self(0b10000000000000000000000000000000000000000);
    pub const eDescriptorBufferReadBitExt: Self = Self(0b100000000000000000000000000000000000000000);
    pub const eOpticalFlowReadBitNv: Self = Self(0b1000000000000000000000000000000000000000000);
    pub const eOpticalFlowWriteBitNv: Self = Self(0b10000000000000000000000000000000000000000000);
    pub const eMicromapReadBitExt: Self = Self(0b100000000000000000000000000000000000000000000);
    pub const eMicromapWriteBitExt: Self = Self(0b1000000000000000000000000000000000000000000000);
    pub const eNoneKhr: Self = Self::eNone;
    pub const eIndirectCommandReadBitKhr: Self = Self::eIndirectCommandReadBit;
    pub const eIndexReadBitKhr: Self = Self::eIndexReadBit;
    pub const eVertexAttributeReadBitKhr: Self = Self::eVertexAttributeReadBit;
    pub const eUniformReadBitKhr: Self = Self::eUniformReadBit;
    pub const eInputAttachmentReadBitKhr: Self = Self::eInputAttachmentReadBit;
    pub const eShaderReadBitKhr: Self = Self::eShaderReadBit;
    pub const eShaderWriteBitKhr: Self = Self::eShaderWriteBit;
    pub const eColourAttachmentReadBitKhr: Self = Self::eColourAttachmentReadBit;
    pub const eColourAttachmentWriteBitKhr: Self = Self::eColourAttachmentWriteBit;
    pub const eDepthStencilAttachmentReadBitKhr: Self = Self::eDepthStencilAttachmentReadBit;
    pub const eDepthStencilAttachmentWriteBitKhr: Self = Self::eDepthStencilAttachmentWriteBit;
    pub const eTransferReadBitKhr: Self = Self::eTransferReadBit;
    pub const eTransferWriteBitKhr: Self = Self::eTransferWriteBit;
    pub const eHostReadBitKhr: Self = Self::eHostReadBit;
    pub const eHostWriteBitKhr: Self = Self::eHostWriteBit;
    pub const eMemoryReadBitKhr: Self = Self::eMemoryReadBit;
    pub const eMemoryWriteBitKhr: Self = Self::eMemoryWriteBit;
    pub const eAccelerationStructureReadBitNv: Self = Self::eAccelerationStructureReadBitKhr;
    pub const eAccelerationStructureWriteBitNv: Self = Self::eAccelerationStructureWriteBitKhr;
    pub const eShadingRateImageReadBitNv: Self = Self::eFragmentShadingRateAttachmentReadBitKhr;
    pub const eShaderSampledReadBitKhr: Self = Self::eShaderSampledReadBit;
    pub const eShaderStorageReadBitKhr: Self = Self::eShaderStorageReadBit;
    pub const eShaderStorageWriteBitKhr: Self = Self::eShaderStorageWriteBit;
}

bit_field_mask_derives!(AccessFlags2, u64);

bit_field_mask_with_enum_debug_derive!(AccessFlagBits2, AccessFlags2, u64, AccessFlagBits2::eIndirectCommandReadBit, AccessFlagBits2::eIndexReadBit, AccessFlagBits2::eVertexAttributeReadBit, AccessFlagBits2::eUniformReadBit, AccessFlagBits2::eInputAttachmentReadBit, AccessFlagBits2::eShaderReadBit, AccessFlagBits2::eShaderWriteBit, AccessFlagBits2::eColourAttachmentReadBit, AccessFlagBits2::eColourAttachmentWriteBit, AccessFlagBits2::eDepthStencilAttachmentReadBit, AccessFlagBits2::eDepthStencilAttachmentWriteBit, AccessFlagBits2::eTransferReadBit, AccessFlagBits2::eTransferWriteBit, AccessFlagBits2::eHostReadBit, AccessFlagBits2::eHostWriteBit, AccessFlagBits2::eMemoryReadBit, AccessFlagBits2::eMemoryWriteBit, AccessFlagBits2::eCommandPreprocessReadBitNv, AccessFlagBits2::eCommandPreprocessWriteBitNv, AccessFlagBits2::eColourAttachmentReadNoncoherentBitExt, AccessFlagBits2::eConditionalRenderingReadBitExt, AccessFlagBits2::eAccelerationStructureReadBitKhr, AccessFlagBits2::eAccelerationStructureWriteBitKhr, AccessFlagBits2::eFragmentShadingRateAttachmentReadBitKhr, AccessFlagBits2::eFragmentDensityMapReadBitExt, AccessFlagBits2::eTransformFeedbackWriteBitExt, AccessFlagBits2::eTransformFeedbackCounterReadBitExt, AccessFlagBits2::eTransformFeedbackCounterWriteBitExt, AccessFlagBits2::eShaderSampledReadBit, AccessFlagBits2::eShaderStorageReadBit, AccessFlagBits2::eShaderStorageWriteBit, AccessFlagBits2::eVideoDecodeReadBitKhr, AccessFlagBits2::eVideoDecodeWriteBitKhr, AccessFlagBits2::eVideoEncodeReadBitKhr, AccessFlagBits2::eVideoEncodeWriteBitKhr, AccessFlagBits2::eInvocationMaskReadBitHuawei, AccessFlagBits2::eShaderBindingTableReadBitKhr, AccessFlagBits2::eDescriptorBufferReadBitExt, AccessFlagBits2::eOpticalFlowReadBitNv, AccessFlagBits2::eOpticalFlowWriteBitNv, AccessFlagBits2::eMicromapReadBitExt, AccessFlagBits2::eMicromapWriteBitExt => AccessFlagBits2::eNone);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PipelineStageFlagBits2 {
    eNone,
    eTopOfPipeBit,
    eDrawIndirectBit,
    eVertexInputBit,
    eVertexShaderBit,
    eTessellationControlShaderBit,
    eTessellationEvaluationShaderBit,
    eGeometryShaderBit,
    eFragmentShaderBit,
    eEarlyFragmentTestsBit,
    eLateFragmentTestsBit,
    eColourAttachmentOutputBit,
    eComputeShaderBit,
    eAllTransferBit,
    eBottomOfPipeBit,
    eHostBit,
    eAllGraphicsBit,
    eAllCommandsBit,
    eCommandPreprocessBitNv,
    eConditionalRenderingBitExt,
    eTaskShaderBitExt,
    eMeshShaderBitExt,
    eRayTracingShaderBitKhr,
    eFragmentShadingRateAttachmentBitKhr,
    eFragmentDensityProcessBitExt,
    eTransformFeedbackBitExt,
    eAccelerationStructureBuildBitKhr,
    eVideoDecodeBitKhr,
    eVideoEncodeBitKhr,
    eAccelerationStructureCopyBitKhr,
    eOpticalFlowBitNv,
    eMicromapBuildBitExt,
    eCopyBit,
    eResolveBit,
    eBlitBit,
    eClearBit,
    eIndexInputBit,
    eVertexAttributeInputBit,
    ePreRasterizationShadersBit,
    eSubpassShadingBitHuawei,
    eInvocationMaskBitHuawei,
    eClusterCullingShaderBitHuawei,
    eUnknownBit(u64),
}

pub type PipelineStageFlagBits2KHR = PipelineStageFlagBits2;

#[allow(non_upper_case_globals)]
impl PipelineStageFlagBits2 {
    pub const eNoneKhr: Self = Self::eNone;
    pub const eTopOfPipeBitKhr: Self = Self::eTopOfPipeBit;
    pub const eDrawIndirectBitKhr: Self = Self::eDrawIndirectBit;
    pub const eVertexInputBitKhr: Self = Self::eVertexInputBit;
    pub const eVertexShaderBitKhr: Self = Self::eVertexShaderBit;
    pub const eTessellationControlShaderBitKhr: Self = Self::eTessellationControlShaderBit;
    pub const eTessellationEvaluationShaderBitKhr: Self = Self::eTessellationEvaluationShaderBit;
    pub const eGeometryShaderBitKhr: Self = Self::eGeometryShaderBit;
    pub const eFragmentShaderBitKhr: Self = Self::eFragmentShaderBit;
    pub const eEarlyFragmentTestsBitKhr: Self = Self::eEarlyFragmentTestsBit;
    pub const eLateFragmentTestsBitKhr: Self = Self::eLateFragmentTestsBit;
    pub const eColourAttachmentOutputBitKhr: Self = Self::eColourAttachmentOutputBit;
    pub const eComputeShaderBitKhr: Self = Self::eComputeShaderBit;
    pub const eAllTransferBitKhr: Self = Self::eAllTransferBit;
    pub const eTransferBitKhr: Self = Self::eAllTransferBit;
    pub const eBottomOfPipeBitKhr: Self = Self::eBottomOfPipeBit;
    pub const eHostBitKhr: Self = Self::eHostBit;
    pub const eAllGraphicsBitKhr: Self = Self::eAllGraphicsBit;
    pub const eAllCommandsBitKhr: Self = Self::eAllCommandsBit;
    pub const eRayTracingShaderBitNv: Self = Self::eRayTracingShaderBitKhr;
    pub const eShadingRateImageBitNv: Self = Self::eFragmentShadingRateAttachmentBitKhr;
    pub const eAccelerationStructureBuildBitNv: Self = Self::eAccelerationStructureBuildBitKhr;
    pub const eCopyBitKhr: Self = Self::eCopyBit;
    pub const eResolveBitKhr: Self = Self::eResolveBit;
    pub const eBlitBitKhr: Self = Self::eBlitBit;
    pub const eClearBitKhr: Self = Self::eClearBit;
    pub const eIndexInputBitKhr: Self = Self::eIndexInputBit;
    pub const eVertexAttributeInputBitKhr: Self = Self::eVertexAttributeInputBit;
    pub const ePreRasterizationShadersBitKhr: Self = Self::ePreRasterizationShadersBit;

    pub fn into_raw(self) -> RawPipelineStageFlagBits2 {
        match self {
            Self::eNone => RawPipelineStageFlagBits2::eNone,
            Self::eTopOfPipeBit => RawPipelineStageFlagBits2::eTopOfPipeBit,
            Self::eDrawIndirectBit => RawPipelineStageFlagBits2::eDrawIndirectBit,
            Self::eVertexInputBit => RawPipelineStageFlagBits2::eVertexInputBit,
            Self::eVertexShaderBit => RawPipelineStageFlagBits2::eVertexShaderBit,
            Self::eTessellationControlShaderBit => RawPipelineStageFlagBits2::eTessellationControlShaderBit,
            Self::eTessellationEvaluationShaderBit => RawPipelineStageFlagBits2::eTessellationEvaluationShaderBit,
            Self::eGeometryShaderBit => RawPipelineStageFlagBits2::eGeometryShaderBit,
            Self::eFragmentShaderBit => RawPipelineStageFlagBits2::eFragmentShaderBit,
            Self::eEarlyFragmentTestsBit => RawPipelineStageFlagBits2::eEarlyFragmentTestsBit,
            Self::eLateFragmentTestsBit => RawPipelineStageFlagBits2::eLateFragmentTestsBit,
            Self::eColourAttachmentOutputBit => RawPipelineStageFlagBits2::eColourAttachmentOutputBit,
            Self::eComputeShaderBit => RawPipelineStageFlagBits2::eComputeShaderBit,
            Self::eAllTransferBit => RawPipelineStageFlagBits2::eAllTransferBit,
            Self::eBottomOfPipeBit => RawPipelineStageFlagBits2::eBottomOfPipeBit,
            Self::eHostBit => RawPipelineStageFlagBits2::eHostBit,
            Self::eAllGraphicsBit => RawPipelineStageFlagBits2::eAllGraphicsBit,
            Self::eAllCommandsBit => RawPipelineStageFlagBits2::eAllCommandsBit,
            Self::eCommandPreprocessBitNv => RawPipelineStageFlagBits2::eCommandPreprocessBitNv,
            Self::eConditionalRenderingBitExt => RawPipelineStageFlagBits2::eConditionalRenderingBitExt,
            Self::eTaskShaderBitExt => RawPipelineStageFlagBits2::eTaskShaderBitExt,
            Self::eMeshShaderBitExt => RawPipelineStageFlagBits2::eMeshShaderBitExt,
            Self::eRayTracingShaderBitKhr => RawPipelineStageFlagBits2::eRayTracingShaderBitKhr,
            Self::eFragmentShadingRateAttachmentBitKhr => RawPipelineStageFlagBits2::eFragmentShadingRateAttachmentBitKhr,
            Self::eFragmentDensityProcessBitExt => RawPipelineStageFlagBits2::eFragmentDensityProcessBitExt,
            Self::eTransformFeedbackBitExt => RawPipelineStageFlagBits2::eTransformFeedbackBitExt,
            Self::eAccelerationStructureBuildBitKhr => RawPipelineStageFlagBits2::eAccelerationStructureBuildBitKhr,
            Self::eVideoDecodeBitKhr => RawPipelineStageFlagBits2::eVideoDecodeBitKhr,
            Self::eVideoEncodeBitKhr => RawPipelineStageFlagBits2::eVideoEncodeBitKhr,
            Self::eAccelerationStructureCopyBitKhr => RawPipelineStageFlagBits2::eAccelerationStructureCopyBitKhr,
            Self::eOpticalFlowBitNv => RawPipelineStageFlagBits2::eOpticalFlowBitNv,
            Self::eMicromapBuildBitExt => RawPipelineStageFlagBits2::eMicromapBuildBitExt,
            Self::eCopyBit => RawPipelineStageFlagBits2::eCopyBit,
            Self::eResolveBit => RawPipelineStageFlagBits2::eResolveBit,
            Self::eBlitBit => RawPipelineStageFlagBits2::eBlitBit,
            Self::eClearBit => RawPipelineStageFlagBits2::eClearBit,
            Self::eIndexInputBit => RawPipelineStageFlagBits2::eIndexInputBit,
            Self::eVertexAttributeInputBit => RawPipelineStageFlagBits2::eVertexAttributeInputBit,
            Self::ePreRasterizationShadersBit => RawPipelineStageFlagBits2::ePreRasterizationShadersBit,
            Self::eSubpassShadingBitHuawei => RawPipelineStageFlagBits2::eSubpassShadingBitHuawei,
            Self::eInvocationMaskBitHuawei => RawPipelineStageFlagBits2::eInvocationMaskBitHuawei,
            Self::eClusterCullingShaderBitHuawei => RawPipelineStageFlagBits2::eClusterCullingShaderBitHuawei,
            Self::eUnknownBit(b) => RawPipelineStageFlagBits2(b),
        }
    }
}

impl core::convert::From<PipelineStageFlagBits2> for PipelineStageFlags2 {
    fn from(value: PipelineStageFlagBits2) -> Self {
        match value {
            PipelineStageFlagBits2::eNone => PipelineStageFlags2::eNone,
            PipelineStageFlagBits2::eTopOfPipeBit => PipelineStageFlags2::eTopOfPipeBit,
            PipelineStageFlagBits2::eDrawIndirectBit => PipelineStageFlags2::eDrawIndirectBit,
            PipelineStageFlagBits2::eVertexInputBit => PipelineStageFlags2::eVertexInputBit,
            PipelineStageFlagBits2::eVertexShaderBit => PipelineStageFlags2::eVertexShaderBit,
            PipelineStageFlagBits2::eTessellationControlShaderBit => PipelineStageFlags2::eTessellationControlShaderBit,
            PipelineStageFlagBits2::eTessellationEvaluationShaderBit => PipelineStageFlags2::eTessellationEvaluationShaderBit,
            PipelineStageFlagBits2::eGeometryShaderBit => PipelineStageFlags2::eGeometryShaderBit,
            PipelineStageFlagBits2::eFragmentShaderBit => PipelineStageFlags2::eFragmentShaderBit,
            PipelineStageFlagBits2::eEarlyFragmentTestsBit => PipelineStageFlags2::eEarlyFragmentTestsBit,
            PipelineStageFlagBits2::eLateFragmentTestsBit => PipelineStageFlags2::eLateFragmentTestsBit,
            PipelineStageFlagBits2::eColourAttachmentOutputBit => PipelineStageFlags2::eColourAttachmentOutputBit,
            PipelineStageFlagBits2::eComputeShaderBit => PipelineStageFlags2::eComputeShaderBit,
            PipelineStageFlagBits2::eAllTransferBit => PipelineStageFlags2::eAllTransferBit,
            PipelineStageFlagBits2::eBottomOfPipeBit => PipelineStageFlags2::eBottomOfPipeBit,
            PipelineStageFlagBits2::eHostBit => PipelineStageFlags2::eHostBit,
            PipelineStageFlagBits2::eAllGraphicsBit => PipelineStageFlags2::eAllGraphicsBit,
            PipelineStageFlagBits2::eAllCommandsBit => PipelineStageFlags2::eAllCommandsBit,
            PipelineStageFlagBits2::eCommandPreprocessBitNv => PipelineStageFlags2::eCommandPreprocessBitNv,
            PipelineStageFlagBits2::eConditionalRenderingBitExt => PipelineStageFlags2::eConditionalRenderingBitExt,
            PipelineStageFlagBits2::eTaskShaderBitExt => PipelineStageFlags2::eTaskShaderBitExt,
            PipelineStageFlagBits2::eMeshShaderBitExt => PipelineStageFlags2::eMeshShaderBitExt,
            PipelineStageFlagBits2::eRayTracingShaderBitKhr => PipelineStageFlags2::eRayTracingShaderBitKhr,
            PipelineStageFlagBits2::eFragmentShadingRateAttachmentBitKhr => PipelineStageFlags2::eFragmentShadingRateAttachmentBitKhr,
            PipelineStageFlagBits2::eFragmentDensityProcessBitExt => PipelineStageFlags2::eFragmentDensityProcessBitExt,
            PipelineStageFlagBits2::eTransformFeedbackBitExt => PipelineStageFlags2::eTransformFeedbackBitExt,
            PipelineStageFlagBits2::eAccelerationStructureBuildBitKhr => PipelineStageFlags2::eAccelerationStructureBuildBitKhr,
            PipelineStageFlagBits2::eVideoDecodeBitKhr => PipelineStageFlags2::eVideoDecodeBitKhr,
            PipelineStageFlagBits2::eVideoEncodeBitKhr => PipelineStageFlags2::eVideoEncodeBitKhr,
            PipelineStageFlagBits2::eAccelerationStructureCopyBitKhr => PipelineStageFlags2::eAccelerationStructureCopyBitKhr,
            PipelineStageFlagBits2::eOpticalFlowBitNv => PipelineStageFlags2::eOpticalFlowBitNv,
            PipelineStageFlagBits2::eMicromapBuildBitExt => PipelineStageFlags2::eMicromapBuildBitExt,
            PipelineStageFlagBits2::eCopyBit => PipelineStageFlags2::eCopyBit,
            PipelineStageFlagBits2::eResolveBit => PipelineStageFlags2::eResolveBit,
            PipelineStageFlagBits2::eBlitBit => PipelineStageFlags2::eBlitBit,
            PipelineStageFlagBits2::eClearBit => PipelineStageFlags2::eClearBit,
            PipelineStageFlagBits2::eIndexInputBit => PipelineStageFlags2::eIndexInputBit,
            PipelineStageFlagBits2::eVertexAttributeInputBit => PipelineStageFlags2::eVertexAttributeInputBit,
            PipelineStageFlagBits2::ePreRasterizationShadersBit => PipelineStageFlags2::ePreRasterizationShadersBit,
            PipelineStageFlagBits2::eSubpassShadingBitHuawei => PipelineStageFlags2::eSubpassShadingBitHuawei,
            PipelineStageFlagBits2::eInvocationMaskBitHuawei => PipelineStageFlags2::eInvocationMaskBitHuawei,
            PipelineStageFlagBits2::eClusterCullingShaderBitHuawei => PipelineStageFlags2::eClusterCullingShaderBitHuawei,
            PipelineStageFlagBits2::eUnknownBit(b) => PipelineStageFlags2(b),
        }
    }
}

bit_field_enum_derives!(PipelineStageFlagBits2, PipelineStageFlags2, u64);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineStageFlags2(pub(crate) u64);

pub type PipelineStageFlags2KHR = PipelineStageFlags2;

#[allow(non_upper_case_globals)]
impl PipelineStageFlags2 {
    pub const eNone: Self = Self(0);
    pub const eTopOfPipeBit: Self = Self(0b1);
    pub const eDrawIndirectBit: Self = Self(0b10);
    pub const eVertexInputBit: Self = Self(0b100);
    pub const eVertexShaderBit: Self = Self(0b1000);
    pub const eTessellationControlShaderBit: Self = Self(0b10000);
    pub const eTessellationEvaluationShaderBit: Self = Self(0b100000);
    pub const eGeometryShaderBit: Self = Self(0b1000000);
    pub const eFragmentShaderBit: Self = Self(0b10000000);
    pub const eEarlyFragmentTestsBit: Self = Self(0b100000000);
    pub const eLateFragmentTestsBit: Self = Self(0b1000000000);
    pub const eColourAttachmentOutputBit: Self = Self(0b10000000000);
    pub const eComputeShaderBit: Self = Self(0b100000000000);
    pub const eAllTransferBit: Self = Self(0b1000000000000);
    pub const eBottomOfPipeBit: Self = Self(0b10000000000000);
    pub const eHostBit: Self = Self(0b100000000000000);
    pub const eAllGraphicsBit: Self = Self(0b1000000000000000);
    pub const eAllCommandsBit: Self = Self(0b10000000000000000);
    pub const eCommandPreprocessBitNv: Self = Self(0b100000000000000000);
    pub const eConditionalRenderingBitExt: Self = Self(0b1000000000000000000);
    pub const eTaskShaderBitExt: Self = Self(0b10000000000000000000);
    pub const eMeshShaderBitExt: Self = Self(0b100000000000000000000);
    pub const eRayTracingShaderBitKhr: Self = Self(0b1000000000000000000000);
    pub const eFragmentShadingRateAttachmentBitKhr: Self = Self(0b10000000000000000000000);
    pub const eFragmentDensityProcessBitExt: Self = Self(0b100000000000000000000000);
    pub const eTransformFeedbackBitExt: Self = Self(0b1000000000000000000000000);
    pub const eAccelerationStructureBuildBitKhr: Self = Self(0b10000000000000000000000000);
    pub const eVideoDecodeBitKhr: Self = Self(0b100000000000000000000000000);
    pub const eVideoEncodeBitKhr: Self = Self(0b1000000000000000000000000000);
    pub const eAccelerationStructureCopyBitKhr: Self = Self(0b10000000000000000000000000000);
    pub const eOpticalFlowBitNv: Self = Self(0b100000000000000000000000000000);
    pub const eMicromapBuildBitExt: Self = Self(0b1000000000000000000000000000000);
    pub const eCopyBit: Self = Self(0b100000000000000000000000000000000);
    pub const eResolveBit: Self = Self(0b1000000000000000000000000000000000);
    pub const eBlitBit: Self = Self(0b10000000000000000000000000000000000);
    pub const eClearBit: Self = Self(0b100000000000000000000000000000000000);
    pub const eIndexInputBit: Self = Self(0b1000000000000000000000000000000000000);
    pub const eVertexAttributeInputBit: Self = Self(0b10000000000000000000000000000000000000);
    pub const ePreRasterizationShadersBit: Self = Self(0b100000000000000000000000000000000000000);
    pub const eSubpassShadingBitHuawei: Self = Self(0b1000000000000000000000000000000000000000);
    pub const eInvocationMaskBitHuawei: Self = Self(0b10000000000000000000000000000000000000000);
    pub const eClusterCullingShaderBitHuawei: Self = Self(0b100000000000000000000000000000000000000000);
    pub const eNoneKhr: Self = Self::eNone;
    pub const eTopOfPipeBitKhr: Self = Self::eTopOfPipeBit;
    pub const eDrawIndirectBitKhr: Self = Self::eDrawIndirectBit;
    pub const eVertexInputBitKhr: Self = Self::eVertexInputBit;
    pub const eVertexShaderBitKhr: Self = Self::eVertexShaderBit;
    pub const eTessellationControlShaderBitKhr: Self = Self::eTessellationControlShaderBit;
    pub const eTessellationEvaluationShaderBitKhr: Self = Self::eTessellationEvaluationShaderBit;
    pub const eGeometryShaderBitKhr: Self = Self::eGeometryShaderBit;
    pub const eFragmentShaderBitKhr: Self = Self::eFragmentShaderBit;
    pub const eEarlyFragmentTestsBitKhr: Self = Self::eEarlyFragmentTestsBit;
    pub const eLateFragmentTestsBitKhr: Self = Self::eLateFragmentTestsBit;
    pub const eColourAttachmentOutputBitKhr: Self = Self::eColourAttachmentOutputBit;
    pub const eComputeShaderBitKhr: Self = Self::eComputeShaderBit;
    pub const eAllTransferBitKhr: Self = Self::eAllTransferBit;
    pub const eTransferBitKhr: Self = Self::eAllTransferBit;
    pub const eBottomOfPipeBitKhr: Self = Self::eBottomOfPipeBit;
    pub const eHostBitKhr: Self = Self::eHostBit;
    pub const eAllGraphicsBitKhr: Self = Self::eAllGraphicsBit;
    pub const eAllCommandsBitKhr: Self = Self::eAllCommandsBit;
    pub const eRayTracingShaderBitNv: Self = Self::eRayTracingShaderBitKhr;
    pub const eShadingRateImageBitNv: Self = Self::eFragmentShadingRateAttachmentBitKhr;
    pub const eAccelerationStructureBuildBitNv: Self = Self::eAccelerationStructureBuildBitKhr;
    pub const eCopyBitKhr: Self = Self::eCopyBit;
    pub const eResolveBitKhr: Self = Self::eResolveBit;
    pub const eBlitBitKhr: Self = Self::eBlitBit;
    pub const eClearBitKhr: Self = Self::eClearBit;
    pub const eIndexInputBitKhr: Self = Self::eIndexInputBit;
    pub const eVertexAttributeInputBitKhr: Self = Self::eVertexAttributeInputBit;
    pub const ePreRasterizationShadersBitKhr: Self = Self::ePreRasterizationShadersBit;
}

bit_field_mask_derives!(PipelineStageFlags2, u64);

bit_field_mask_with_enum_debug_derive!(PipelineStageFlagBits2, PipelineStageFlags2, u64, PipelineStageFlagBits2::eTopOfPipeBit, PipelineStageFlagBits2::eDrawIndirectBit, PipelineStageFlagBits2::eVertexInputBit, PipelineStageFlagBits2::eVertexShaderBit, PipelineStageFlagBits2::eTessellationControlShaderBit, PipelineStageFlagBits2::eTessellationEvaluationShaderBit, PipelineStageFlagBits2::eGeometryShaderBit, PipelineStageFlagBits2::eFragmentShaderBit, PipelineStageFlagBits2::eEarlyFragmentTestsBit, PipelineStageFlagBits2::eLateFragmentTestsBit, PipelineStageFlagBits2::eColourAttachmentOutputBit, PipelineStageFlagBits2::eComputeShaderBit, PipelineStageFlagBits2::eAllTransferBit, PipelineStageFlagBits2::eBottomOfPipeBit, PipelineStageFlagBits2::eHostBit, PipelineStageFlagBits2::eAllGraphicsBit, PipelineStageFlagBits2::eAllCommandsBit, PipelineStageFlagBits2::eCommandPreprocessBitNv, PipelineStageFlagBits2::eConditionalRenderingBitExt, PipelineStageFlagBits2::eTaskShaderBitExt, PipelineStageFlagBits2::eMeshShaderBitExt, PipelineStageFlagBits2::eRayTracingShaderBitKhr, PipelineStageFlagBits2::eFragmentShadingRateAttachmentBitKhr, PipelineStageFlagBits2::eFragmentDensityProcessBitExt, PipelineStageFlagBits2::eTransformFeedbackBitExt, PipelineStageFlagBits2::eAccelerationStructureBuildBitKhr, PipelineStageFlagBits2::eVideoDecodeBitKhr, PipelineStageFlagBits2::eVideoEncodeBitKhr, PipelineStageFlagBits2::eAccelerationStructureCopyBitKhr, PipelineStageFlagBits2::eOpticalFlowBitNv, PipelineStageFlagBits2::eMicromapBuildBitExt, PipelineStageFlagBits2::eCopyBit, PipelineStageFlagBits2::eResolveBit, PipelineStageFlagBits2::eBlitBit, PipelineStageFlagBits2::eClearBit, PipelineStageFlagBits2::eIndexInputBit, PipelineStageFlagBits2::eVertexAttributeInputBit, PipelineStageFlagBits2::ePreRasterizationShadersBit, PipelineStageFlagBits2::eSubpassShadingBitHuawei, PipelineStageFlagBits2::eInvocationMaskBitHuawei, PipelineStageFlagBits2::eClusterCullingShaderBitHuawei => PipelineStageFlagBits2::eNone);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct AccelerationStructureMotionInfoFlagsNV(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl AccelerationStructureMotionInfoFlagsNV {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(AccelerationStructureMotionInfoFlagsNV, u32);

fieldless_debug_derive!(AccelerationStructureMotionInfoFlagsNV);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct AccelerationStructureMotionInstanceFlagsNV(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl AccelerationStructureMotionInstanceFlagsNV {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(AccelerationStructureMotionInstanceFlagsNV, u32);

fieldless_debug_derive!(AccelerationStructureMotionInstanceFlagsNV);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum FormatFeatureFlagBits2 {
    eSampledImageBit,
    eStorageImageBit,
    eStorageImageAtomicBit,
    eUniformTexelBufferBit,
    eStorageTexelBufferBit,
    eStorageTexelBufferAtomicBit,
    eVertexBufferBit,
    eColourAttachmentBit,
    eColourAttachmentBlendBit,
    eDepthStencilAttachmentBit,
    eBlitSrcBit,
    eBlitDstBit,
    eSampledImageFilterLinearBit,
    eSampledImageFilterCubicBit,
    eTransferSrcBit,
    eTransferDstBit,
    eSampledImageFilterMinmaxBit,
    eMidpointChromaSamplesBit,
    eSampledImageYcbcrConversionLinearFilterBit,
    eSampledImageYcbcrConversionSeparateReconstructionFilterBit,
    eSampledImageYcbcrConversionChromaReconstructionExplicitBit,
    eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit,
    eDisjointBit,
    eCositedChromaSamplesBit,
    eFragmentDensityMapBitExt,
    eVideoDecodeOutputBitKhr,
    eVideoDecodeDpbBitKhr,
    eVideoEncodeInputBitKhr,
    eVideoEncodeDpbBitKhr,
    eAccelerationStructureVertexBufferBitKhr,
    eFragmentShadingRateAttachmentBitKhr,
    eStorageReadWithoutFormatBit,
    eStorageWriteWithoutFormatBit,
    eSampledImageDepthComparisonBit,
    eWeightImageBitQcom,
    eWeightSampledImageBitQcom,
    eBlockMatchingBitQcom,
    eBoxFilterSampledBitQcom,
    eLinearColourAttachmentBitNv,
    eOpticalFlowImageBitNv,
    eOpticalFlowVectorBitNv,
    eOpticalFlowCostBitNv,
    eUnknownBit(u64),
}

pub type FormatFeatureFlagBits2KHR = FormatFeatureFlagBits2;

#[allow(non_upper_case_globals)]
impl FormatFeatureFlagBits2 {
    pub const eSampledImageBitKhr: Self = Self::eSampledImageBit;
    pub const eStorageImageBitKhr: Self = Self::eStorageImageBit;
    pub const eStorageImageAtomicBitKhr: Self = Self::eStorageImageAtomicBit;
    pub const eUniformTexelBufferBitKhr: Self = Self::eUniformTexelBufferBit;
    pub const eStorageTexelBufferBitKhr: Self = Self::eStorageTexelBufferBit;
    pub const eStorageTexelBufferAtomicBitKhr: Self = Self::eStorageTexelBufferAtomicBit;
    pub const eVertexBufferBitKhr: Self = Self::eVertexBufferBit;
    pub const eColourAttachmentBitKhr: Self = Self::eColourAttachmentBit;
    pub const eColourAttachmentBlendBitKhr: Self = Self::eColourAttachmentBlendBit;
    pub const eDepthStencilAttachmentBitKhr: Self = Self::eDepthStencilAttachmentBit;
    pub const eBlitSrcBitKhr: Self = Self::eBlitSrcBit;
    pub const eBlitDstBitKhr: Self = Self::eBlitDstBit;
    pub const eSampledImageFilterLinearBitKhr: Self = Self::eSampledImageFilterLinearBit;
    pub const eSampledImageFilterCubicBitExt: Self = Self::eSampledImageFilterCubicBit;
    pub const eTransferSrcBitKhr: Self = Self::eTransferSrcBit;
    pub const eTransferDstBitKhr: Self = Self::eTransferDstBit;
    pub const eSampledImageFilterMinmaxBitKhr: Self = Self::eSampledImageFilterMinmaxBit;
    pub const eMidpointChromaSamplesBitKhr: Self = Self::eMidpointChromaSamplesBit;
    pub const eSampledImageYcbcrConversionLinearFilterBitKhr: Self = Self::eSampledImageYcbcrConversionLinearFilterBit;
    pub const eSampledImageYcbcrConversionSeparateReconstructionFilterBitKhr: Self = Self::eSampledImageYcbcrConversionSeparateReconstructionFilterBit;
    pub const eSampledImageYcbcrConversionChromaReconstructionExplicitBitKhr: Self = Self::eSampledImageYcbcrConversionChromaReconstructionExplicitBit;
    pub const eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBitKhr: Self = Self::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit;
    pub const eDisjointBitKhr: Self = Self::eDisjointBit;
    pub const eCositedChromaSamplesBitKhr: Self = Self::eCositedChromaSamplesBit;
    pub const eStorageReadWithoutFormatBitKhr: Self = Self::eStorageReadWithoutFormatBit;
    pub const eStorageWriteWithoutFormatBitKhr: Self = Self::eStorageWriteWithoutFormatBit;
    pub const eSampledImageDepthComparisonBitKhr: Self = Self::eSampledImageDepthComparisonBit;

    pub fn into_raw(self) -> RawFormatFeatureFlagBits2 {
        match self {
            Self::eSampledImageBit => RawFormatFeatureFlagBits2::eSampledImageBit,
            Self::eStorageImageBit => RawFormatFeatureFlagBits2::eStorageImageBit,
            Self::eStorageImageAtomicBit => RawFormatFeatureFlagBits2::eStorageImageAtomicBit,
            Self::eUniformTexelBufferBit => RawFormatFeatureFlagBits2::eUniformTexelBufferBit,
            Self::eStorageTexelBufferBit => RawFormatFeatureFlagBits2::eStorageTexelBufferBit,
            Self::eStorageTexelBufferAtomicBit => RawFormatFeatureFlagBits2::eStorageTexelBufferAtomicBit,
            Self::eVertexBufferBit => RawFormatFeatureFlagBits2::eVertexBufferBit,
            Self::eColourAttachmentBit => RawFormatFeatureFlagBits2::eColourAttachmentBit,
            Self::eColourAttachmentBlendBit => RawFormatFeatureFlagBits2::eColourAttachmentBlendBit,
            Self::eDepthStencilAttachmentBit => RawFormatFeatureFlagBits2::eDepthStencilAttachmentBit,
            Self::eBlitSrcBit => RawFormatFeatureFlagBits2::eBlitSrcBit,
            Self::eBlitDstBit => RawFormatFeatureFlagBits2::eBlitDstBit,
            Self::eSampledImageFilterLinearBit => RawFormatFeatureFlagBits2::eSampledImageFilterLinearBit,
            Self::eSampledImageFilterCubicBit => RawFormatFeatureFlagBits2::eSampledImageFilterCubicBit,
            Self::eTransferSrcBit => RawFormatFeatureFlagBits2::eTransferSrcBit,
            Self::eTransferDstBit => RawFormatFeatureFlagBits2::eTransferDstBit,
            Self::eSampledImageFilterMinmaxBit => RawFormatFeatureFlagBits2::eSampledImageFilterMinmaxBit,
            Self::eMidpointChromaSamplesBit => RawFormatFeatureFlagBits2::eMidpointChromaSamplesBit,
            Self::eSampledImageYcbcrConversionLinearFilterBit => RawFormatFeatureFlagBits2::eSampledImageYcbcrConversionLinearFilterBit,
            Self::eSampledImageYcbcrConversionSeparateReconstructionFilterBit => RawFormatFeatureFlagBits2::eSampledImageYcbcrConversionSeparateReconstructionFilterBit,
            Self::eSampledImageYcbcrConversionChromaReconstructionExplicitBit => RawFormatFeatureFlagBits2::eSampledImageYcbcrConversionChromaReconstructionExplicitBit,
            Self::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit => RawFormatFeatureFlagBits2::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit,
            Self::eDisjointBit => RawFormatFeatureFlagBits2::eDisjointBit,
            Self::eCositedChromaSamplesBit => RawFormatFeatureFlagBits2::eCositedChromaSamplesBit,
            Self::eFragmentDensityMapBitExt => RawFormatFeatureFlagBits2::eFragmentDensityMapBitExt,
            Self::eVideoDecodeOutputBitKhr => RawFormatFeatureFlagBits2::eVideoDecodeOutputBitKhr,
            Self::eVideoDecodeDpbBitKhr => RawFormatFeatureFlagBits2::eVideoDecodeDpbBitKhr,
            Self::eVideoEncodeInputBitKhr => RawFormatFeatureFlagBits2::eVideoEncodeInputBitKhr,
            Self::eVideoEncodeDpbBitKhr => RawFormatFeatureFlagBits2::eVideoEncodeDpbBitKhr,
            Self::eAccelerationStructureVertexBufferBitKhr => RawFormatFeatureFlagBits2::eAccelerationStructureVertexBufferBitKhr,
            Self::eFragmentShadingRateAttachmentBitKhr => RawFormatFeatureFlagBits2::eFragmentShadingRateAttachmentBitKhr,
            Self::eStorageReadWithoutFormatBit => RawFormatFeatureFlagBits2::eStorageReadWithoutFormatBit,
            Self::eStorageWriteWithoutFormatBit => RawFormatFeatureFlagBits2::eStorageWriteWithoutFormatBit,
            Self::eSampledImageDepthComparisonBit => RawFormatFeatureFlagBits2::eSampledImageDepthComparisonBit,
            Self::eWeightImageBitQcom => RawFormatFeatureFlagBits2::eWeightImageBitQcom,
            Self::eWeightSampledImageBitQcom => RawFormatFeatureFlagBits2::eWeightSampledImageBitQcom,
            Self::eBlockMatchingBitQcom => RawFormatFeatureFlagBits2::eBlockMatchingBitQcom,
            Self::eBoxFilterSampledBitQcom => RawFormatFeatureFlagBits2::eBoxFilterSampledBitQcom,
            Self::eLinearColourAttachmentBitNv => RawFormatFeatureFlagBits2::eLinearColourAttachmentBitNv,
            Self::eOpticalFlowImageBitNv => RawFormatFeatureFlagBits2::eOpticalFlowImageBitNv,
            Self::eOpticalFlowVectorBitNv => RawFormatFeatureFlagBits2::eOpticalFlowVectorBitNv,
            Self::eOpticalFlowCostBitNv => RawFormatFeatureFlagBits2::eOpticalFlowCostBitNv,
            Self::eUnknownBit(b) => RawFormatFeatureFlagBits2(b),
        }
    }
}

impl core::convert::From<FormatFeatureFlagBits2> for FormatFeatureFlags2 {
    fn from(value: FormatFeatureFlagBits2) -> Self {
        match value {
            FormatFeatureFlagBits2::eSampledImageBit => FormatFeatureFlags2::eSampledImageBit,
            FormatFeatureFlagBits2::eStorageImageBit => FormatFeatureFlags2::eStorageImageBit,
            FormatFeatureFlagBits2::eStorageImageAtomicBit => FormatFeatureFlags2::eStorageImageAtomicBit,
            FormatFeatureFlagBits2::eUniformTexelBufferBit => FormatFeatureFlags2::eUniformTexelBufferBit,
            FormatFeatureFlagBits2::eStorageTexelBufferBit => FormatFeatureFlags2::eStorageTexelBufferBit,
            FormatFeatureFlagBits2::eStorageTexelBufferAtomicBit => FormatFeatureFlags2::eStorageTexelBufferAtomicBit,
            FormatFeatureFlagBits2::eVertexBufferBit => FormatFeatureFlags2::eVertexBufferBit,
            FormatFeatureFlagBits2::eColourAttachmentBit => FormatFeatureFlags2::eColourAttachmentBit,
            FormatFeatureFlagBits2::eColourAttachmentBlendBit => FormatFeatureFlags2::eColourAttachmentBlendBit,
            FormatFeatureFlagBits2::eDepthStencilAttachmentBit => FormatFeatureFlags2::eDepthStencilAttachmentBit,
            FormatFeatureFlagBits2::eBlitSrcBit => FormatFeatureFlags2::eBlitSrcBit,
            FormatFeatureFlagBits2::eBlitDstBit => FormatFeatureFlags2::eBlitDstBit,
            FormatFeatureFlagBits2::eSampledImageFilterLinearBit => FormatFeatureFlags2::eSampledImageFilterLinearBit,
            FormatFeatureFlagBits2::eSampledImageFilterCubicBit => FormatFeatureFlags2::eSampledImageFilterCubicBit,
            FormatFeatureFlagBits2::eTransferSrcBit => FormatFeatureFlags2::eTransferSrcBit,
            FormatFeatureFlagBits2::eTransferDstBit => FormatFeatureFlags2::eTransferDstBit,
            FormatFeatureFlagBits2::eSampledImageFilterMinmaxBit => FormatFeatureFlags2::eSampledImageFilterMinmaxBit,
            FormatFeatureFlagBits2::eMidpointChromaSamplesBit => FormatFeatureFlags2::eMidpointChromaSamplesBit,
            FormatFeatureFlagBits2::eSampledImageYcbcrConversionLinearFilterBit => FormatFeatureFlags2::eSampledImageYcbcrConversionLinearFilterBit,
            FormatFeatureFlagBits2::eSampledImageYcbcrConversionSeparateReconstructionFilterBit => FormatFeatureFlags2::eSampledImageYcbcrConversionSeparateReconstructionFilterBit,
            FormatFeatureFlagBits2::eSampledImageYcbcrConversionChromaReconstructionExplicitBit => FormatFeatureFlags2::eSampledImageYcbcrConversionChromaReconstructionExplicitBit,
            FormatFeatureFlagBits2::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit => FormatFeatureFlags2::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit,
            FormatFeatureFlagBits2::eDisjointBit => FormatFeatureFlags2::eDisjointBit,
            FormatFeatureFlagBits2::eCositedChromaSamplesBit => FormatFeatureFlags2::eCositedChromaSamplesBit,
            FormatFeatureFlagBits2::eFragmentDensityMapBitExt => FormatFeatureFlags2::eFragmentDensityMapBitExt,
            FormatFeatureFlagBits2::eVideoDecodeOutputBitKhr => FormatFeatureFlags2::eVideoDecodeOutputBitKhr,
            FormatFeatureFlagBits2::eVideoDecodeDpbBitKhr => FormatFeatureFlags2::eVideoDecodeDpbBitKhr,
            FormatFeatureFlagBits2::eVideoEncodeInputBitKhr => FormatFeatureFlags2::eVideoEncodeInputBitKhr,
            FormatFeatureFlagBits2::eVideoEncodeDpbBitKhr => FormatFeatureFlags2::eVideoEncodeDpbBitKhr,
            FormatFeatureFlagBits2::eAccelerationStructureVertexBufferBitKhr => FormatFeatureFlags2::eAccelerationStructureVertexBufferBitKhr,
            FormatFeatureFlagBits2::eFragmentShadingRateAttachmentBitKhr => FormatFeatureFlags2::eFragmentShadingRateAttachmentBitKhr,
            FormatFeatureFlagBits2::eStorageReadWithoutFormatBit => FormatFeatureFlags2::eStorageReadWithoutFormatBit,
            FormatFeatureFlagBits2::eStorageWriteWithoutFormatBit => FormatFeatureFlags2::eStorageWriteWithoutFormatBit,
            FormatFeatureFlagBits2::eSampledImageDepthComparisonBit => FormatFeatureFlags2::eSampledImageDepthComparisonBit,
            FormatFeatureFlagBits2::eWeightImageBitQcom => FormatFeatureFlags2::eWeightImageBitQcom,
            FormatFeatureFlagBits2::eWeightSampledImageBitQcom => FormatFeatureFlags2::eWeightSampledImageBitQcom,
            FormatFeatureFlagBits2::eBlockMatchingBitQcom => FormatFeatureFlags2::eBlockMatchingBitQcom,
            FormatFeatureFlagBits2::eBoxFilterSampledBitQcom => FormatFeatureFlags2::eBoxFilterSampledBitQcom,
            FormatFeatureFlagBits2::eLinearColourAttachmentBitNv => FormatFeatureFlags2::eLinearColourAttachmentBitNv,
            FormatFeatureFlagBits2::eOpticalFlowImageBitNv => FormatFeatureFlags2::eOpticalFlowImageBitNv,
            FormatFeatureFlagBits2::eOpticalFlowVectorBitNv => FormatFeatureFlags2::eOpticalFlowVectorBitNv,
            FormatFeatureFlagBits2::eOpticalFlowCostBitNv => FormatFeatureFlags2::eOpticalFlowCostBitNv,
            FormatFeatureFlagBits2::eUnknownBit(b) => FormatFeatureFlags2(b),
        }
    }
}

bit_field_enum_derives!(FormatFeatureFlagBits2, FormatFeatureFlags2, u64);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct FormatFeatureFlags2(pub(crate) u64);

pub type FormatFeatureFlags2KHR = FormatFeatureFlags2;

#[allow(non_upper_case_globals)]
impl FormatFeatureFlags2 {
    pub const eNone: Self = Self(0);
    pub const eSampledImageBit: Self = Self(0b1);
    pub const eStorageImageBit: Self = Self(0b10);
    pub const eStorageImageAtomicBit: Self = Self(0b100);
    pub const eUniformTexelBufferBit: Self = Self(0b1000);
    pub const eStorageTexelBufferBit: Self = Self(0b10000);
    pub const eStorageTexelBufferAtomicBit: Self = Self(0b100000);
    pub const eVertexBufferBit: Self = Self(0b1000000);
    pub const eColourAttachmentBit: Self = Self(0b10000000);
    pub const eColourAttachmentBlendBit: Self = Self(0b100000000);
    pub const eDepthStencilAttachmentBit: Self = Self(0b1000000000);
    pub const eBlitSrcBit: Self = Self(0b10000000000);
    pub const eBlitDstBit: Self = Self(0b100000000000);
    pub const eSampledImageFilterLinearBit: Self = Self(0b1000000000000);
    pub const eSampledImageFilterCubicBit: Self = Self(0b10000000000000);
    pub const eTransferSrcBit: Self = Self(0b100000000000000);
    pub const eTransferDstBit: Self = Self(0b1000000000000000);
    pub const eSampledImageFilterMinmaxBit: Self = Self(0b10000000000000000);
    pub const eMidpointChromaSamplesBit: Self = Self(0b100000000000000000);
    pub const eSampledImageYcbcrConversionLinearFilterBit: Self = Self(0b1000000000000000000);
    pub const eSampledImageYcbcrConversionSeparateReconstructionFilterBit: Self = Self(0b10000000000000000000);
    pub const eSampledImageYcbcrConversionChromaReconstructionExplicitBit: Self = Self(0b100000000000000000000);
    pub const eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit: Self = Self(0b1000000000000000000000);
    pub const eDisjointBit: Self = Self(0b10000000000000000000000);
    pub const eCositedChromaSamplesBit: Self = Self(0b100000000000000000000000);
    pub const eFragmentDensityMapBitExt: Self = Self(0b1000000000000000000000000);
    pub const eVideoDecodeOutputBitKhr: Self = Self(0b10000000000000000000000000);
    pub const eVideoDecodeDpbBitKhr: Self = Self(0b100000000000000000000000000);
    pub const eVideoEncodeInputBitKhr: Self = Self(0b1000000000000000000000000000);
    pub const eVideoEncodeDpbBitKhr: Self = Self(0b10000000000000000000000000000);
    pub const eAccelerationStructureVertexBufferBitKhr: Self = Self(0b100000000000000000000000000000);
    pub const eFragmentShadingRateAttachmentBitKhr: Self = Self(0b1000000000000000000000000000000);
    pub const eStorageReadWithoutFormatBit: Self = Self(0b10000000000000000000000000000000);
    pub const eStorageWriteWithoutFormatBit: Self = Self(0b100000000000000000000000000000000);
    pub const eSampledImageDepthComparisonBit: Self = Self(0b1000000000000000000000000000000000);
    pub const eWeightImageBitQcom: Self = Self(0b10000000000000000000000000000000000);
    pub const eWeightSampledImageBitQcom: Self = Self(0b100000000000000000000000000000000000);
    pub const eBlockMatchingBitQcom: Self = Self(0b1000000000000000000000000000000000000);
    pub const eBoxFilterSampledBitQcom: Self = Self(0b10000000000000000000000000000000000000);
    pub const eLinearColourAttachmentBitNv: Self = Self(0b100000000000000000000000000000000000000);
    pub const eOpticalFlowImageBitNv: Self = Self(0b10000000000000000000000000000000000000000);
    pub const eOpticalFlowVectorBitNv: Self = Self(0b100000000000000000000000000000000000000000);
    pub const eOpticalFlowCostBitNv: Self = Self(0b1000000000000000000000000000000000000000000);
    pub const eSampledImageBitKhr: Self = Self::eSampledImageBit;
    pub const eStorageImageBitKhr: Self = Self::eStorageImageBit;
    pub const eStorageImageAtomicBitKhr: Self = Self::eStorageImageAtomicBit;
    pub const eUniformTexelBufferBitKhr: Self = Self::eUniformTexelBufferBit;
    pub const eStorageTexelBufferBitKhr: Self = Self::eStorageTexelBufferBit;
    pub const eStorageTexelBufferAtomicBitKhr: Self = Self::eStorageTexelBufferAtomicBit;
    pub const eVertexBufferBitKhr: Self = Self::eVertexBufferBit;
    pub const eColourAttachmentBitKhr: Self = Self::eColourAttachmentBit;
    pub const eColourAttachmentBlendBitKhr: Self = Self::eColourAttachmentBlendBit;
    pub const eDepthStencilAttachmentBitKhr: Self = Self::eDepthStencilAttachmentBit;
    pub const eBlitSrcBitKhr: Self = Self::eBlitSrcBit;
    pub const eBlitDstBitKhr: Self = Self::eBlitDstBit;
    pub const eSampledImageFilterLinearBitKhr: Self = Self::eSampledImageFilterLinearBit;
    pub const eSampledImageFilterCubicBitExt: Self = Self::eSampledImageFilterCubicBit;
    pub const eTransferSrcBitKhr: Self = Self::eTransferSrcBit;
    pub const eTransferDstBitKhr: Self = Self::eTransferDstBit;
    pub const eSampledImageFilterMinmaxBitKhr: Self = Self::eSampledImageFilterMinmaxBit;
    pub const eMidpointChromaSamplesBitKhr: Self = Self::eMidpointChromaSamplesBit;
    pub const eSampledImageYcbcrConversionLinearFilterBitKhr: Self = Self::eSampledImageYcbcrConversionLinearFilterBit;
    pub const eSampledImageYcbcrConversionSeparateReconstructionFilterBitKhr: Self = Self::eSampledImageYcbcrConversionSeparateReconstructionFilterBit;
    pub const eSampledImageYcbcrConversionChromaReconstructionExplicitBitKhr: Self = Self::eSampledImageYcbcrConversionChromaReconstructionExplicitBit;
    pub const eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBitKhr: Self = Self::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit;
    pub const eDisjointBitKhr: Self = Self::eDisjointBit;
    pub const eCositedChromaSamplesBitKhr: Self = Self::eCositedChromaSamplesBit;
    pub const eStorageReadWithoutFormatBitKhr: Self = Self::eStorageReadWithoutFormatBit;
    pub const eStorageWriteWithoutFormatBitKhr: Self = Self::eStorageWriteWithoutFormatBit;
    pub const eSampledImageDepthComparisonBitKhr: Self = Self::eSampledImageDepthComparisonBit;
}

bit_field_mask_derives!(FormatFeatureFlags2, u64);

bit_field_mask_with_enum_debug_derive!(FormatFeatureFlagBits2, FormatFeatureFlags2, u64, FormatFeatureFlagBits2::eSampledImageBit, FormatFeatureFlagBits2::eStorageImageBit, FormatFeatureFlagBits2::eStorageImageAtomicBit, FormatFeatureFlagBits2::eUniformTexelBufferBit, FormatFeatureFlagBits2::eStorageTexelBufferBit, FormatFeatureFlagBits2::eStorageTexelBufferAtomicBit, FormatFeatureFlagBits2::eVertexBufferBit, FormatFeatureFlagBits2::eColourAttachmentBit, FormatFeatureFlagBits2::eColourAttachmentBlendBit, FormatFeatureFlagBits2::eDepthStencilAttachmentBit, FormatFeatureFlagBits2::eBlitSrcBit, FormatFeatureFlagBits2::eBlitDstBit, FormatFeatureFlagBits2::eSampledImageFilterLinearBit, FormatFeatureFlagBits2::eSampledImageFilterCubicBit, FormatFeatureFlagBits2::eTransferSrcBit, FormatFeatureFlagBits2::eTransferDstBit, FormatFeatureFlagBits2::eSampledImageFilterMinmaxBit, FormatFeatureFlagBits2::eMidpointChromaSamplesBit, FormatFeatureFlagBits2::eSampledImageYcbcrConversionLinearFilterBit, FormatFeatureFlagBits2::eSampledImageYcbcrConversionSeparateReconstructionFilterBit, FormatFeatureFlagBits2::eSampledImageYcbcrConversionChromaReconstructionExplicitBit, FormatFeatureFlagBits2::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit, FormatFeatureFlagBits2::eDisjointBit, FormatFeatureFlagBits2::eCositedChromaSamplesBit, FormatFeatureFlagBits2::eFragmentDensityMapBitExt, FormatFeatureFlagBits2::eVideoDecodeOutputBitKhr, FormatFeatureFlagBits2::eVideoDecodeDpbBitKhr, FormatFeatureFlagBits2::eVideoEncodeInputBitKhr, FormatFeatureFlagBits2::eVideoEncodeDpbBitKhr, FormatFeatureFlagBits2::eAccelerationStructureVertexBufferBitKhr, FormatFeatureFlagBits2::eFragmentShadingRateAttachmentBitKhr, FormatFeatureFlagBits2::eStorageReadWithoutFormatBit, FormatFeatureFlagBits2::eStorageWriteWithoutFormatBit, FormatFeatureFlagBits2::eSampledImageDepthComparisonBit, FormatFeatureFlagBits2::eWeightImageBitQcom, FormatFeatureFlagBits2::eWeightSampledImageBitQcom, FormatFeatureFlagBits2::eBlockMatchingBitQcom, FormatFeatureFlagBits2::eBoxFilterSampledBitQcom, FormatFeatureFlagBits2::eLinearColourAttachmentBitNv, FormatFeatureFlagBits2::eOpticalFlowImageBitNv, FormatFeatureFlagBits2::eOpticalFlowVectorBitNv, FormatFeatureFlagBits2::eOpticalFlowCostBitNv);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum RenderingFlagBits {
    eContentsSecondaryCommandBuffersBit,
    eSuspendingBit,
    eResumingBit,
    eEnableLegacyDitheringBitExt,
    eUnknownBit(u32),
}

pub type RenderingFlagBitsKHR = RenderingFlagBits;

#[allow(non_upper_case_globals)]
impl RenderingFlagBits {
    pub const eContentsSecondaryCommandBuffersBitKhr: Self = Self::eContentsSecondaryCommandBuffersBit;
    pub const eSuspendingBitKhr: Self = Self::eSuspendingBit;
    pub const eResumingBitKhr: Self = Self::eResumingBit;

    pub fn into_raw(self) -> RawRenderingFlagBits {
        match self {
            Self::eContentsSecondaryCommandBuffersBit => RawRenderingFlagBits::eContentsSecondaryCommandBuffersBit,
            Self::eSuspendingBit => RawRenderingFlagBits::eSuspendingBit,
            Self::eResumingBit => RawRenderingFlagBits::eResumingBit,
            Self::eEnableLegacyDitheringBitExt => RawRenderingFlagBits::eEnableLegacyDitheringBitExt,
            Self::eUnknownBit(b) => RawRenderingFlagBits(b),
        }
    }
}

impl core::convert::From<RenderingFlagBits> for RenderingFlags {
    fn from(value: RenderingFlagBits) -> Self {
        match value {
            RenderingFlagBits::eContentsSecondaryCommandBuffersBit => RenderingFlags::eContentsSecondaryCommandBuffersBit,
            RenderingFlagBits::eSuspendingBit => RenderingFlags::eSuspendingBit,
            RenderingFlagBits::eResumingBit => RenderingFlags::eResumingBit,
            RenderingFlagBits::eEnableLegacyDitheringBitExt => RenderingFlags::eEnableLegacyDitheringBitExt,
            RenderingFlagBits::eUnknownBit(b) => RenderingFlags(b),
        }
    }
}

bit_field_enum_derives!(RenderingFlagBits, RenderingFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct RenderingFlags(pub(crate) u32);

pub type RenderingFlagsKHR = RenderingFlags;

#[allow(non_upper_case_globals)]
impl RenderingFlags {
    pub const eNone: Self = Self(0);
    pub const eContentsSecondaryCommandBuffersBit: Self = Self(0b1);
    pub const eSuspendingBit: Self = Self(0b10);
    pub const eResumingBit: Self = Self(0b100);
    pub const eEnableLegacyDitheringBitExt: Self = Self(0b1000);
    pub const eContentsSecondaryCommandBuffersBitKhr: Self = Self::eContentsSecondaryCommandBuffersBit;
    pub const eSuspendingBitKhr: Self = Self::eSuspendingBit;
    pub const eResumingBitKhr: Self = Self::eResumingBit;
}

bit_field_mask_derives!(RenderingFlags, u32);

bit_field_mask_with_enum_debug_derive!(RenderingFlagBits, RenderingFlags, u32, RenderingFlagBits::eContentsSecondaryCommandBuffersBit, RenderingFlagBits::eSuspendingBit, RenderingFlagBits::eResumingBit, RenderingFlagBits::eEnableLegacyDitheringBitExt);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum MemoryDecompressionMethodFlagBitsNV {
    eGdeflate10Bit,
    eUnknownBit(u64),
}

#[allow(non_upper_case_globals)]
impl MemoryDecompressionMethodFlagBitsNV {
    pub fn into_raw(self) -> RawMemoryDecompressionMethodFlagBitsNV {
        match self {
            Self::eGdeflate10Bit => RawMemoryDecompressionMethodFlagBitsNV::eGdeflate10Bit,
            Self::eUnknownBit(b) => RawMemoryDecompressionMethodFlagBitsNV(b),
        }
    }
}

impl core::convert::From<MemoryDecompressionMethodFlagBitsNV> for MemoryDecompressionMethodFlagsNV {
    fn from(value: MemoryDecompressionMethodFlagBitsNV) -> Self {
        match value {
            MemoryDecompressionMethodFlagBitsNV::eGdeflate10Bit => MemoryDecompressionMethodFlagsNV::eGdeflate10Bit,
            MemoryDecompressionMethodFlagBitsNV::eUnknownBit(b) => MemoryDecompressionMethodFlagsNV(b),
        }
    }
}

bit_field_enum_derives!(MemoryDecompressionMethodFlagBitsNV, MemoryDecompressionMethodFlagsNV, u64);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct MemoryDecompressionMethodFlagsNV(pub(crate) u64);

#[allow(non_upper_case_globals)]
impl MemoryDecompressionMethodFlagsNV {
    pub const eNone: Self = Self(0);
    pub const eGdeflate10Bit: Self = Self(0b1);
}

bit_field_mask_derives!(MemoryDecompressionMethodFlagsNV, u64);

bit_field_mask_with_enum_debug_derive!(MemoryDecompressionMethodFlagBitsNV, MemoryDecompressionMethodFlagsNV, u64, MemoryDecompressionMethodFlagBitsNV::eGdeflate10Bit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum BuildMicromapFlagBitsEXT {
    ePreferFastTraceBit,
    ePreferFastBuildBit,
    eAllowCompactionBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl BuildMicromapFlagBitsEXT {
    pub fn into_raw(self) -> RawBuildMicromapFlagBitsEXT {
        match self {
            Self::ePreferFastTraceBit => RawBuildMicromapFlagBitsEXT::ePreferFastTraceBit,
            Self::ePreferFastBuildBit => RawBuildMicromapFlagBitsEXT::ePreferFastBuildBit,
            Self::eAllowCompactionBit => RawBuildMicromapFlagBitsEXT::eAllowCompactionBit,
            Self::eUnknownBit(b) => RawBuildMicromapFlagBitsEXT(b),
        }
    }
}

impl core::convert::From<BuildMicromapFlagBitsEXT> for BuildMicromapFlagsEXT {
    fn from(value: BuildMicromapFlagBitsEXT) -> Self {
        match value {
            BuildMicromapFlagBitsEXT::ePreferFastTraceBit => BuildMicromapFlagsEXT::ePreferFastTraceBit,
            BuildMicromapFlagBitsEXT::ePreferFastBuildBit => BuildMicromapFlagsEXT::ePreferFastBuildBit,
            BuildMicromapFlagBitsEXT::eAllowCompactionBit => BuildMicromapFlagsEXT::eAllowCompactionBit,
            BuildMicromapFlagBitsEXT::eUnknownBit(b) => BuildMicromapFlagsEXT(b),
        }
    }
}

bit_field_enum_derives!(BuildMicromapFlagBitsEXT, BuildMicromapFlagsEXT, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct BuildMicromapFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl BuildMicromapFlagsEXT {
    pub const eNone: Self = Self(0);
    pub const ePreferFastTraceBit: Self = Self(0b1);
    pub const ePreferFastBuildBit: Self = Self(0b10);
    pub const eAllowCompactionBit: Self = Self(0b100);
}

bit_field_mask_derives!(BuildMicromapFlagsEXT, u32);

bit_field_mask_with_enum_debug_derive!(BuildMicromapFlagBitsEXT, BuildMicromapFlagsEXT, u32, BuildMicromapFlagBitsEXT::ePreferFastTraceBit, BuildMicromapFlagBitsEXT::ePreferFastBuildBit, BuildMicromapFlagBitsEXT::eAllowCompactionBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum MicromapCreateFlagBitsEXT {
    eDeviceAddressCaptureReplayBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl MicromapCreateFlagBitsEXT {
    pub fn into_raw(self) -> RawMicromapCreateFlagBitsEXT {
        match self {
            Self::eDeviceAddressCaptureReplayBit => RawMicromapCreateFlagBitsEXT::eDeviceAddressCaptureReplayBit,
            Self::eUnknownBit(b) => RawMicromapCreateFlagBitsEXT(b),
        }
    }
}

impl core::convert::From<MicromapCreateFlagBitsEXT> for MicromapCreateFlagsEXT {
    fn from(value: MicromapCreateFlagBitsEXT) -> Self {
        match value {
            MicromapCreateFlagBitsEXT::eDeviceAddressCaptureReplayBit => MicromapCreateFlagsEXT::eDeviceAddressCaptureReplayBit,
            MicromapCreateFlagBitsEXT::eUnknownBit(b) => MicromapCreateFlagsEXT(b),
        }
    }
}

bit_field_enum_derives!(MicromapCreateFlagBitsEXT, MicromapCreateFlagsEXT, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct MicromapCreateFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl MicromapCreateFlagsEXT {
    pub const eNone: Self = Self(0);
    pub const eDeviceAddressCaptureReplayBit: Self = Self(0b1);
}

bit_field_mask_derives!(MicromapCreateFlagsEXT, u32);

bit_field_mask_with_enum_debug_derive!(MicromapCreateFlagBitsEXT, MicromapCreateFlagsEXT, u32, MicromapCreateFlagBitsEXT::eDeviceAddressCaptureReplayBit);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DirectDriverLoadingFlagsLUNARG(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DirectDriverLoadingFlagsLUNARG {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(DirectDriverLoadingFlagsLUNARG, u32);

fieldless_debug_derive!(DirectDriverLoadingFlagsLUNARG);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum CompositeAlphaFlagBitsKHR {
    eOpaqueBit,
    ePreMultipliedBit,
    ePostMultipliedBit,
    eInheritBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl CompositeAlphaFlagBitsKHR {
    pub fn into_raw(self) -> RawCompositeAlphaFlagBitsKHR {
        match self {
            Self::eOpaqueBit => RawCompositeAlphaFlagBitsKHR::eOpaqueBit,
            Self::ePreMultipliedBit => RawCompositeAlphaFlagBitsKHR::ePreMultipliedBit,
            Self::ePostMultipliedBit => RawCompositeAlphaFlagBitsKHR::ePostMultipliedBit,
            Self::eInheritBit => RawCompositeAlphaFlagBitsKHR::eInheritBit,
            Self::eUnknownBit(b) => RawCompositeAlphaFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<CompositeAlphaFlagBitsKHR> for CompositeAlphaFlagsKHR {
    fn from(value: CompositeAlphaFlagBitsKHR) -> Self {
        match value {
            CompositeAlphaFlagBitsKHR::eOpaqueBit => CompositeAlphaFlagsKHR::eOpaqueBit,
            CompositeAlphaFlagBitsKHR::ePreMultipliedBit => CompositeAlphaFlagsKHR::ePreMultipliedBit,
            CompositeAlphaFlagBitsKHR::ePostMultipliedBit => CompositeAlphaFlagsKHR::ePostMultipliedBit,
            CompositeAlphaFlagBitsKHR::eInheritBit => CompositeAlphaFlagsKHR::eInheritBit,
            CompositeAlphaFlagBitsKHR::eUnknownBit(b) => CompositeAlphaFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(CompositeAlphaFlagBitsKHR, CompositeAlphaFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct CompositeAlphaFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl CompositeAlphaFlagsKHR {
    pub const eNone: Self = Self(0);
    pub const eOpaqueBit: Self = Self(0b1);
    pub const ePreMultipliedBit: Self = Self(0b10);
    pub const ePostMultipliedBit: Self = Self(0b100);
    pub const eInheritBit: Self = Self(0b1000);
}

bit_field_mask_derives!(CompositeAlphaFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(CompositeAlphaFlagBitsKHR, CompositeAlphaFlagsKHR, u32, CompositeAlphaFlagBitsKHR::eOpaqueBit, CompositeAlphaFlagBitsKHR::ePreMultipliedBit, CompositeAlphaFlagBitsKHR::ePostMultipliedBit, CompositeAlphaFlagBitsKHR::eInheritBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DisplayPlaneAlphaFlagBitsKHR {
    eOpaqueBit,
    eGlobalBit,
    ePerPixelBit,
    ePerPixelPremultipliedBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl DisplayPlaneAlphaFlagBitsKHR {
    pub fn into_raw(self) -> RawDisplayPlaneAlphaFlagBitsKHR {
        match self {
            Self::eOpaqueBit => RawDisplayPlaneAlphaFlagBitsKHR::eOpaqueBit,
            Self::eGlobalBit => RawDisplayPlaneAlphaFlagBitsKHR::eGlobalBit,
            Self::ePerPixelBit => RawDisplayPlaneAlphaFlagBitsKHR::ePerPixelBit,
            Self::ePerPixelPremultipliedBit => RawDisplayPlaneAlphaFlagBitsKHR::ePerPixelPremultipliedBit,
            Self::eUnknownBit(b) => RawDisplayPlaneAlphaFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<DisplayPlaneAlphaFlagBitsKHR> for DisplayPlaneAlphaFlagsKHR {
    fn from(value: DisplayPlaneAlphaFlagBitsKHR) -> Self {
        match value {
            DisplayPlaneAlphaFlagBitsKHR::eOpaqueBit => DisplayPlaneAlphaFlagsKHR::eOpaqueBit,
            DisplayPlaneAlphaFlagBitsKHR::eGlobalBit => DisplayPlaneAlphaFlagsKHR::eGlobalBit,
            DisplayPlaneAlphaFlagBitsKHR::ePerPixelBit => DisplayPlaneAlphaFlagsKHR::ePerPixelBit,
            DisplayPlaneAlphaFlagBitsKHR::ePerPixelPremultipliedBit => DisplayPlaneAlphaFlagsKHR::ePerPixelPremultipliedBit,
            DisplayPlaneAlphaFlagBitsKHR::eUnknownBit(b) => DisplayPlaneAlphaFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(DisplayPlaneAlphaFlagBitsKHR, DisplayPlaneAlphaFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DisplayPlaneAlphaFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DisplayPlaneAlphaFlagsKHR {
    pub const eNone: Self = Self(0);
    pub const eOpaqueBit: Self = Self(0b1);
    pub const eGlobalBit: Self = Self(0b10);
    pub const ePerPixelBit: Self = Self(0b100);
    pub const ePerPixelPremultipliedBit: Self = Self(0b1000);
}

bit_field_mask_derives!(DisplayPlaneAlphaFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(DisplayPlaneAlphaFlagBitsKHR, DisplayPlaneAlphaFlagsKHR, u32, DisplayPlaneAlphaFlagBitsKHR::eOpaqueBit, DisplayPlaneAlphaFlagBitsKHR::eGlobalBit, DisplayPlaneAlphaFlagBitsKHR::ePerPixelBit, DisplayPlaneAlphaFlagBitsKHR::ePerPixelPremultipliedBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SurfaceTransformFlagBitsKHR {
    eIdentityBit,
    eRotate90Bit,
    eRotate180Bit,
    eRotate270Bit,
    eHorizontalMirrorBit,
    eHorizontalMirrorRotate90Bit,
    eHorizontalMirrorRotate180Bit,
    eHorizontalMirrorRotate270Bit,
    eInheritBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl SurfaceTransformFlagBitsKHR {
    pub fn into_raw(self) -> RawSurfaceTransformFlagBitsKHR {
        match self {
            Self::eIdentityBit => RawSurfaceTransformFlagBitsKHR::eIdentityBit,
            Self::eRotate90Bit => RawSurfaceTransformFlagBitsKHR::eRotate90Bit,
            Self::eRotate180Bit => RawSurfaceTransformFlagBitsKHR::eRotate180Bit,
            Self::eRotate270Bit => RawSurfaceTransformFlagBitsKHR::eRotate270Bit,
            Self::eHorizontalMirrorBit => RawSurfaceTransformFlagBitsKHR::eHorizontalMirrorBit,
            Self::eHorizontalMirrorRotate90Bit => RawSurfaceTransformFlagBitsKHR::eHorizontalMirrorRotate90Bit,
            Self::eHorizontalMirrorRotate180Bit => RawSurfaceTransformFlagBitsKHR::eHorizontalMirrorRotate180Bit,
            Self::eHorizontalMirrorRotate270Bit => RawSurfaceTransformFlagBitsKHR::eHorizontalMirrorRotate270Bit,
            Self::eInheritBit => RawSurfaceTransformFlagBitsKHR::eInheritBit,
            Self::eUnknownBit(b) => RawSurfaceTransformFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<SurfaceTransformFlagBitsKHR> for SurfaceTransformFlagsKHR {
    fn from(value: SurfaceTransformFlagBitsKHR) -> Self {
        match value {
            SurfaceTransformFlagBitsKHR::eIdentityBit => SurfaceTransformFlagsKHR::eIdentityBit,
            SurfaceTransformFlagBitsKHR::eRotate90Bit => SurfaceTransformFlagsKHR::eRotate90Bit,
            SurfaceTransformFlagBitsKHR::eRotate180Bit => SurfaceTransformFlagsKHR::eRotate180Bit,
            SurfaceTransformFlagBitsKHR::eRotate270Bit => SurfaceTransformFlagsKHR::eRotate270Bit,
            SurfaceTransformFlagBitsKHR::eHorizontalMirrorBit => SurfaceTransformFlagsKHR::eHorizontalMirrorBit,
            SurfaceTransformFlagBitsKHR::eHorizontalMirrorRotate90Bit => SurfaceTransformFlagsKHR::eHorizontalMirrorRotate90Bit,
            SurfaceTransformFlagBitsKHR::eHorizontalMirrorRotate180Bit => SurfaceTransformFlagsKHR::eHorizontalMirrorRotate180Bit,
            SurfaceTransformFlagBitsKHR::eHorizontalMirrorRotate270Bit => SurfaceTransformFlagsKHR::eHorizontalMirrorRotate270Bit,
            SurfaceTransformFlagBitsKHR::eInheritBit => SurfaceTransformFlagsKHR::eInheritBit,
            SurfaceTransformFlagBitsKHR::eUnknownBit(b) => SurfaceTransformFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(SurfaceTransformFlagBitsKHR, SurfaceTransformFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct SurfaceTransformFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl SurfaceTransformFlagsKHR {
    pub const eNone: Self = Self(0);
    pub const eIdentityBit: Self = Self(0b1);
    pub const eRotate90Bit: Self = Self(0b10);
    pub const eRotate180Bit: Self = Self(0b100);
    pub const eRotate270Bit: Self = Self(0b1000);
    pub const eHorizontalMirrorBit: Self = Self(0b10000);
    pub const eHorizontalMirrorRotate90Bit: Self = Self(0b100000);
    pub const eHorizontalMirrorRotate180Bit: Self = Self(0b1000000);
    pub const eHorizontalMirrorRotate270Bit: Self = Self(0b10000000);
    pub const eInheritBit: Self = Self(0b100000000);
}

bit_field_mask_derives!(SurfaceTransformFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(SurfaceTransformFlagBitsKHR, SurfaceTransformFlagsKHR, u32, SurfaceTransformFlagBitsKHR::eIdentityBit, SurfaceTransformFlagBitsKHR::eRotate90Bit, SurfaceTransformFlagBitsKHR::eRotate180Bit, SurfaceTransformFlagBitsKHR::eRotate270Bit, SurfaceTransformFlagBitsKHR::eHorizontalMirrorBit, SurfaceTransformFlagBitsKHR::eHorizontalMirrorRotate90Bit, SurfaceTransformFlagBitsKHR::eHorizontalMirrorRotate180Bit, SurfaceTransformFlagBitsKHR::eHorizontalMirrorRotate270Bit, SurfaceTransformFlagBitsKHR::eInheritBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SwapchainCreateFlagBitsKHR {
    eSplitInstanceBindRegionsBit,
    eProtectedBit,
    eMutableFormatBit,
    eDeferredMemoryAllocationBitExt,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl SwapchainCreateFlagBitsKHR {
    pub fn into_raw(self) -> RawSwapchainCreateFlagBitsKHR {
        match self {
            Self::eSplitInstanceBindRegionsBit => RawSwapchainCreateFlagBitsKHR::eSplitInstanceBindRegionsBit,
            Self::eProtectedBit => RawSwapchainCreateFlagBitsKHR::eProtectedBit,
            Self::eMutableFormatBit => RawSwapchainCreateFlagBitsKHR::eMutableFormatBit,
            Self::eDeferredMemoryAllocationBitExt => RawSwapchainCreateFlagBitsKHR::eDeferredMemoryAllocationBitExt,
            Self::eUnknownBit(b) => RawSwapchainCreateFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<SwapchainCreateFlagBitsKHR> for SwapchainCreateFlagsKHR {
    fn from(value: SwapchainCreateFlagBitsKHR) -> Self {
        match value {
            SwapchainCreateFlagBitsKHR::eSplitInstanceBindRegionsBit => SwapchainCreateFlagsKHR::eSplitInstanceBindRegionsBit,
            SwapchainCreateFlagBitsKHR::eProtectedBit => SwapchainCreateFlagsKHR::eProtectedBit,
            SwapchainCreateFlagBitsKHR::eMutableFormatBit => SwapchainCreateFlagsKHR::eMutableFormatBit,
            SwapchainCreateFlagBitsKHR::eDeferredMemoryAllocationBitExt => SwapchainCreateFlagsKHR::eDeferredMemoryAllocationBitExt,
            SwapchainCreateFlagBitsKHR::eUnknownBit(b) => SwapchainCreateFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(SwapchainCreateFlagBitsKHR, SwapchainCreateFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct SwapchainCreateFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl SwapchainCreateFlagsKHR {
    pub const eNone: Self = Self(0);
    pub const eSplitInstanceBindRegionsBit: Self = Self(0b1);
    pub const eProtectedBit: Self = Self(0b10);
    pub const eMutableFormatBit: Self = Self(0b100);
    pub const eDeferredMemoryAllocationBitExt: Self = Self(0b1000);
}

bit_field_mask_derives!(SwapchainCreateFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(SwapchainCreateFlagBitsKHR, SwapchainCreateFlagsKHR, u32, SwapchainCreateFlagBitsKHR::eSplitInstanceBindRegionsBit, SwapchainCreateFlagBitsKHR::eProtectedBit, SwapchainCreateFlagBitsKHR::eMutableFormatBit, SwapchainCreateFlagBitsKHR::eDeferredMemoryAllocationBitExt);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DisplayModeCreateFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DisplayModeCreateFlagsKHR {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(DisplayModeCreateFlagsKHR, u32);

fieldless_debug_derive!(DisplayModeCreateFlagsKHR);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DisplaySurfaceCreateFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DisplaySurfaceCreateFlagsKHR {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(DisplaySurfaceCreateFlagsKHR, u32);

fieldless_debug_derive!(DisplaySurfaceCreateFlagsKHR);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct AndroidSurfaceCreateFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl AndroidSurfaceCreateFlagsKHR {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(AndroidSurfaceCreateFlagsKHR, u32);

fieldless_debug_derive!(AndroidSurfaceCreateFlagsKHR);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ViSurfaceCreateFlagsNN(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ViSurfaceCreateFlagsNN {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(ViSurfaceCreateFlagsNN, u32);

fieldless_debug_derive!(ViSurfaceCreateFlagsNN);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct WaylandSurfaceCreateFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl WaylandSurfaceCreateFlagsKHR {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(WaylandSurfaceCreateFlagsKHR, u32);

fieldless_debug_derive!(WaylandSurfaceCreateFlagsKHR);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct Win32SurfaceCreateFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl Win32SurfaceCreateFlagsKHR {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(Win32SurfaceCreateFlagsKHR, u32);

fieldless_debug_derive!(Win32SurfaceCreateFlagsKHR);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct XlibSurfaceCreateFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl XlibSurfaceCreateFlagsKHR {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(XlibSurfaceCreateFlagsKHR, u32);

fieldless_debug_derive!(XlibSurfaceCreateFlagsKHR);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct XcbSurfaceCreateFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl XcbSurfaceCreateFlagsKHR {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(XcbSurfaceCreateFlagsKHR, u32);

fieldless_debug_derive!(XcbSurfaceCreateFlagsKHR);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DirectFBSurfaceCreateFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DirectFBSurfaceCreateFlagsEXT {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(DirectFBSurfaceCreateFlagsEXT, u32);

fieldless_debug_derive!(DirectFBSurfaceCreateFlagsEXT);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct IOSSurfaceCreateFlagsMVK(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl IOSSurfaceCreateFlagsMVK {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(IOSSurfaceCreateFlagsMVK, u32);

fieldless_debug_derive!(IOSSurfaceCreateFlagsMVK);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct MacOSSurfaceCreateFlagsMVK(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl MacOSSurfaceCreateFlagsMVK {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(MacOSSurfaceCreateFlagsMVK, u32);

fieldless_debug_derive!(MacOSSurfaceCreateFlagsMVK);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct MetalSurfaceCreateFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl MetalSurfaceCreateFlagsEXT {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(MetalSurfaceCreateFlagsEXT, u32);

fieldless_debug_derive!(MetalSurfaceCreateFlagsEXT);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ImagePipeSurfaceCreateFlagsFUCHSIA(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ImagePipeSurfaceCreateFlagsFUCHSIA {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(ImagePipeSurfaceCreateFlagsFUCHSIA, u32);

fieldless_debug_derive!(ImagePipeSurfaceCreateFlagsFUCHSIA);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct StreamDescriptorSurfaceCreateFlagsGGP(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl StreamDescriptorSurfaceCreateFlagsGGP {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(StreamDescriptorSurfaceCreateFlagsGGP, u32);

fieldless_debug_derive!(StreamDescriptorSurfaceCreateFlagsGGP);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct HeadlessSurfaceCreateFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl HeadlessSurfaceCreateFlagsEXT {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(HeadlessSurfaceCreateFlagsEXT, u32);

fieldless_debug_derive!(HeadlessSurfaceCreateFlagsEXT);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ScreenSurfaceCreateFlagsQNX(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ScreenSurfaceCreateFlagsQNX {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(ScreenSurfaceCreateFlagsQNX, u32);

fieldless_debug_derive!(ScreenSurfaceCreateFlagsQNX);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PeerMemoryFeatureFlagBits {
    eCopySrcBit,
    eCopyDstBit,
    eGenericSrcBit,
    eGenericDstBit,
    eUnknownBit(u32),
}

pub type PeerMemoryFeatureFlagBitsKHR = PeerMemoryFeatureFlagBits;

#[allow(non_upper_case_globals)]
impl PeerMemoryFeatureFlagBits {
    pub const eCopySrcBitKhr: Self = Self::eCopySrcBit;
    pub const eCopyDstBitKhr: Self = Self::eCopyDstBit;
    pub const eGenericSrcBitKhr: Self = Self::eGenericSrcBit;
    pub const eGenericDstBitKhr: Self = Self::eGenericDstBit;

    pub fn into_raw(self) -> RawPeerMemoryFeatureFlagBits {
        match self {
            Self::eCopySrcBit => RawPeerMemoryFeatureFlagBits::eCopySrcBit,
            Self::eCopyDstBit => RawPeerMemoryFeatureFlagBits::eCopyDstBit,
            Self::eGenericSrcBit => RawPeerMemoryFeatureFlagBits::eGenericSrcBit,
            Self::eGenericDstBit => RawPeerMemoryFeatureFlagBits::eGenericDstBit,
            Self::eUnknownBit(b) => RawPeerMemoryFeatureFlagBits(b),
        }
    }
}

impl core::convert::From<PeerMemoryFeatureFlagBits> for PeerMemoryFeatureFlags {
    fn from(value: PeerMemoryFeatureFlagBits) -> Self {
        match value {
            PeerMemoryFeatureFlagBits::eCopySrcBit => PeerMemoryFeatureFlags::eCopySrcBit,
            PeerMemoryFeatureFlagBits::eCopyDstBit => PeerMemoryFeatureFlags::eCopyDstBit,
            PeerMemoryFeatureFlagBits::eGenericSrcBit => PeerMemoryFeatureFlags::eGenericSrcBit,
            PeerMemoryFeatureFlagBits::eGenericDstBit => PeerMemoryFeatureFlags::eGenericDstBit,
            PeerMemoryFeatureFlagBits::eUnknownBit(b) => PeerMemoryFeatureFlags(b),
        }
    }
}

bit_field_enum_derives!(PeerMemoryFeatureFlagBits, PeerMemoryFeatureFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PeerMemoryFeatureFlags(pub(crate) u32);

pub type PeerMemoryFeatureFlagsKHR = PeerMemoryFeatureFlags;

#[allow(non_upper_case_globals)]
impl PeerMemoryFeatureFlags {
    pub const eNone: Self = Self(0);
    pub const eCopySrcBit: Self = Self(0b1);
    pub const eCopyDstBit: Self = Self(0b10);
    pub const eGenericSrcBit: Self = Self(0b100);
    pub const eGenericDstBit: Self = Self(0b1000);
    pub const eCopySrcBitKhr: Self = Self::eCopySrcBit;
    pub const eCopyDstBitKhr: Self = Self::eCopyDstBit;
    pub const eGenericSrcBitKhr: Self = Self::eGenericSrcBit;
    pub const eGenericDstBitKhr: Self = Self::eGenericDstBit;
}

bit_field_mask_derives!(PeerMemoryFeatureFlags, u32);

bit_field_mask_with_enum_debug_derive!(PeerMemoryFeatureFlagBits, PeerMemoryFeatureFlags, u32, PeerMemoryFeatureFlagBits::eCopySrcBit, PeerMemoryFeatureFlagBits::eCopyDstBit, PeerMemoryFeatureFlagBits::eGenericSrcBit, PeerMemoryFeatureFlagBits::eGenericDstBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum MemoryAllocateFlagBits {
    eDeviceMaskBit,
    eDeviceAddressBit,
    eDeviceAddressCaptureReplayBit,
    eUnknownBit(u32),
}

pub type MemoryAllocateFlagBitsKHR = MemoryAllocateFlagBits;

#[allow(non_upper_case_globals)]
impl MemoryAllocateFlagBits {
    pub const eDeviceMaskBitKhr: Self = Self::eDeviceMaskBit;
    pub const eDeviceAddressBitKhr: Self = Self::eDeviceAddressBit;
    pub const eDeviceAddressCaptureReplayBitKhr: Self = Self::eDeviceAddressCaptureReplayBit;

    pub fn into_raw(self) -> RawMemoryAllocateFlagBits {
        match self {
            Self::eDeviceMaskBit => RawMemoryAllocateFlagBits::eDeviceMaskBit,
            Self::eDeviceAddressBit => RawMemoryAllocateFlagBits::eDeviceAddressBit,
            Self::eDeviceAddressCaptureReplayBit => RawMemoryAllocateFlagBits::eDeviceAddressCaptureReplayBit,
            Self::eUnknownBit(b) => RawMemoryAllocateFlagBits(b),
        }
    }
}

impl core::convert::From<MemoryAllocateFlagBits> for MemoryAllocateFlags {
    fn from(value: MemoryAllocateFlagBits) -> Self {
        match value {
            MemoryAllocateFlagBits::eDeviceMaskBit => MemoryAllocateFlags::eDeviceMaskBit,
            MemoryAllocateFlagBits::eDeviceAddressBit => MemoryAllocateFlags::eDeviceAddressBit,
            MemoryAllocateFlagBits::eDeviceAddressCaptureReplayBit => MemoryAllocateFlags::eDeviceAddressCaptureReplayBit,
            MemoryAllocateFlagBits::eUnknownBit(b) => MemoryAllocateFlags(b),
        }
    }
}

bit_field_enum_derives!(MemoryAllocateFlagBits, MemoryAllocateFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct MemoryAllocateFlags(pub(crate) u32);

pub type MemoryAllocateFlagsKHR = MemoryAllocateFlags;

#[allow(non_upper_case_globals)]
impl MemoryAllocateFlags {
    pub const eNone: Self = Self(0);
    pub const eDeviceMaskBit: Self = Self(0b1);
    pub const eDeviceAddressBit: Self = Self(0b10);
    pub const eDeviceAddressCaptureReplayBit: Self = Self(0b100);
    pub const eDeviceMaskBitKhr: Self = Self::eDeviceMaskBit;
    pub const eDeviceAddressBitKhr: Self = Self::eDeviceAddressBit;
    pub const eDeviceAddressCaptureReplayBitKhr: Self = Self::eDeviceAddressCaptureReplayBit;
}

bit_field_mask_derives!(MemoryAllocateFlags, u32);

bit_field_mask_with_enum_debug_derive!(MemoryAllocateFlagBits, MemoryAllocateFlags, u32, MemoryAllocateFlagBits::eDeviceMaskBit, MemoryAllocateFlagBits::eDeviceAddressBit, MemoryAllocateFlagBits::eDeviceAddressCaptureReplayBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DeviceGroupPresentModeFlagBitsKHR {
    eLocalBit,
    eRemoteBit,
    eSumBit,
    eLocalMultiDeviceBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl DeviceGroupPresentModeFlagBitsKHR {
    pub fn into_raw(self) -> RawDeviceGroupPresentModeFlagBitsKHR {
        match self {
            Self::eLocalBit => RawDeviceGroupPresentModeFlagBitsKHR::eLocalBit,
            Self::eRemoteBit => RawDeviceGroupPresentModeFlagBitsKHR::eRemoteBit,
            Self::eSumBit => RawDeviceGroupPresentModeFlagBitsKHR::eSumBit,
            Self::eLocalMultiDeviceBit => RawDeviceGroupPresentModeFlagBitsKHR::eLocalMultiDeviceBit,
            Self::eUnknownBit(b) => RawDeviceGroupPresentModeFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<DeviceGroupPresentModeFlagBitsKHR> for DeviceGroupPresentModeFlagsKHR {
    fn from(value: DeviceGroupPresentModeFlagBitsKHR) -> Self {
        match value {
            DeviceGroupPresentModeFlagBitsKHR::eLocalBit => DeviceGroupPresentModeFlagsKHR::eLocalBit,
            DeviceGroupPresentModeFlagBitsKHR::eRemoteBit => DeviceGroupPresentModeFlagsKHR::eRemoteBit,
            DeviceGroupPresentModeFlagBitsKHR::eSumBit => DeviceGroupPresentModeFlagsKHR::eSumBit,
            DeviceGroupPresentModeFlagBitsKHR::eLocalMultiDeviceBit => DeviceGroupPresentModeFlagsKHR::eLocalMultiDeviceBit,
            DeviceGroupPresentModeFlagBitsKHR::eUnknownBit(b) => DeviceGroupPresentModeFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(DeviceGroupPresentModeFlagBitsKHR, DeviceGroupPresentModeFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DeviceGroupPresentModeFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DeviceGroupPresentModeFlagsKHR {
    pub const eNone: Self = Self(0);
    pub const eLocalBit: Self = Self(0b1);
    pub const eRemoteBit: Self = Self(0b10);
    pub const eSumBit: Self = Self(0b100);
    pub const eLocalMultiDeviceBit: Self = Self(0b1000);
}

bit_field_mask_derives!(DeviceGroupPresentModeFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(DeviceGroupPresentModeFlagBitsKHR, DeviceGroupPresentModeFlagsKHR, u32, DeviceGroupPresentModeFlagBitsKHR::eLocalBit, DeviceGroupPresentModeFlagBitsKHR::eRemoteBit, DeviceGroupPresentModeFlagBitsKHR::eSumBit, DeviceGroupPresentModeFlagBitsKHR::eLocalMultiDeviceBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DebugReportFlagBitsEXT {
    eInformationBit,
    eWarningBit,
    ePerformanceWarningBit,
    eErrorBit,
    eDebugBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl DebugReportFlagBitsEXT {
    pub fn into_raw(self) -> RawDebugReportFlagBitsEXT {
        match self {
            Self::eInformationBit => RawDebugReportFlagBitsEXT::eInformationBit,
            Self::eWarningBit => RawDebugReportFlagBitsEXT::eWarningBit,
            Self::ePerformanceWarningBit => RawDebugReportFlagBitsEXT::ePerformanceWarningBit,
            Self::eErrorBit => RawDebugReportFlagBitsEXT::eErrorBit,
            Self::eDebugBit => RawDebugReportFlagBitsEXT::eDebugBit,
            Self::eUnknownBit(b) => RawDebugReportFlagBitsEXT(b),
        }
    }
}

impl core::convert::From<DebugReportFlagBitsEXT> for DebugReportFlagsEXT {
    fn from(value: DebugReportFlagBitsEXT) -> Self {
        match value {
            DebugReportFlagBitsEXT::eInformationBit => DebugReportFlagsEXT::eInformationBit,
            DebugReportFlagBitsEXT::eWarningBit => DebugReportFlagsEXT::eWarningBit,
            DebugReportFlagBitsEXT::ePerformanceWarningBit => DebugReportFlagsEXT::ePerformanceWarningBit,
            DebugReportFlagBitsEXT::eErrorBit => DebugReportFlagsEXT::eErrorBit,
            DebugReportFlagBitsEXT::eDebugBit => DebugReportFlagsEXT::eDebugBit,
            DebugReportFlagBitsEXT::eUnknownBit(b) => DebugReportFlagsEXT(b),
        }
    }
}

bit_field_enum_derives!(DebugReportFlagBitsEXT, DebugReportFlagsEXT, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DebugReportFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DebugReportFlagsEXT {
    pub const eNone: Self = Self(0);
    pub const eInformationBit: Self = Self(0b1);
    pub const eWarningBit: Self = Self(0b10);
    pub const ePerformanceWarningBit: Self = Self(0b100);
    pub const eErrorBit: Self = Self(0b1000);
    pub const eDebugBit: Self = Self(0b10000);
}

bit_field_mask_derives!(DebugReportFlagsEXT, u32);

bit_field_mask_with_enum_debug_derive!(DebugReportFlagBitsEXT, DebugReportFlagsEXT, u32, DebugReportFlagBitsEXT::eInformationBit, DebugReportFlagBitsEXT::eWarningBit, DebugReportFlagBitsEXT::ePerformanceWarningBit, DebugReportFlagBitsEXT::eErrorBit, DebugReportFlagBitsEXT::eDebugBit);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct CommandPoolTrimFlags(pub(crate) u32);

pub type CommandPoolTrimFlagsKHR = CommandPoolTrimFlags;

#[allow(non_upper_case_globals)]
impl CommandPoolTrimFlags {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(CommandPoolTrimFlags, u32);

fieldless_debug_derive!(CommandPoolTrimFlags);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ExternalMemoryHandleTypeFlagBitsNV {
    eOpaqueWin32Bit,
    eOpaqueWin32KmtBit,
    eD3d11ImageBit,
    eD3d11ImageKmtBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl ExternalMemoryHandleTypeFlagBitsNV {
    pub fn into_raw(self) -> RawExternalMemoryHandleTypeFlagBitsNV {
        match self {
            Self::eOpaqueWin32Bit => RawExternalMemoryHandleTypeFlagBitsNV::eOpaqueWin32Bit,
            Self::eOpaqueWin32KmtBit => RawExternalMemoryHandleTypeFlagBitsNV::eOpaqueWin32KmtBit,
            Self::eD3d11ImageBit => RawExternalMemoryHandleTypeFlagBitsNV::eD3d11ImageBit,
            Self::eD3d11ImageKmtBit => RawExternalMemoryHandleTypeFlagBitsNV::eD3d11ImageKmtBit,
            Self::eUnknownBit(b) => RawExternalMemoryHandleTypeFlagBitsNV(b),
        }
    }
}

impl core::convert::From<ExternalMemoryHandleTypeFlagBitsNV> for ExternalMemoryHandleTypeFlagsNV {
    fn from(value: ExternalMemoryHandleTypeFlagBitsNV) -> Self {
        match value {
            ExternalMemoryHandleTypeFlagBitsNV::eOpaqueWin32Bit => ExternalMemoryHandleTypeFlagsNV::eOpaqueWin32Bit,
            ExternalMemoryHandleTypeFlagBitsNV::eOpaqueWin32KmtBit => ExternalMemoryHandleTypeFlagsNV::eOpaqueWin32KmtBit,
            ExternalMemoryHandleTypeFlagBitsNV::eD3d11ImageBit => ExternalMemoryHandleTypeFlagsNV::eD3d11ImageBit,
            ExternalMemoryHandleTypeFlagBitsNV::eD3d11ImageKmtBit => ExternalMemoryHandleTypeFlagsNV::eD3d11ImageKmtBit,
            ExternalMemoryHandleTypeFlagBitsNV::eUnknownBit(b) => ExternalMemoryHandleTypeFlagsNV(b),
        }
    }
}

bit_field_enum_derives!(ExternalMemoryHandleTypeFlagBitsNV, ExternalMemoryHandleTypeFlagsNV, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ExternalMemoryHandleTypeFlagsNV(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ExternalMemoryHandleTypeFlagsNV {
    pub const eNone: Self = Self(0);
    pub const eOpaqueWin32Bit: Self = Self(0b1);
    pub const eOpaqueWin32KmtBit: Self = Self(0b10);
    pub const eD3d11ImageBit: Self = Self(0b100);
    pub const eD3d11ImageKmtBit: Self = Self(0b1000);
}

bit_field_mask_derives!(ExternalMemoryHandleTypeFlagsNV, u32);

bit_field_mask_with_enum_debug_derive!(ExternalMemoryHandleTypeFlagBitsNV, ExternalMemoryHandleTypeFlagsNV, u32, ExternalMemoryHandleTypeFlagBitsNV::eOpaqueWin32Bit, ExternalMemoryHandleTypeFlagBitsNV::eOpaqueWin32KmtBit, ExternalMemoryHandleTypeFlagBitsNV::eD3d11ImageBit, ExternalMemoryHandleTypeFlagBitsNV::eD3d11ImageKmtBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ExternalMemoryFeatureFlagBitsNV {
    eDedicatedOnlyBit,
    eExportableBit,
    eImportableBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl ExternalMemoryFeatureFlagBitsNV {
    pub fn into_raw(self) -> RawExternalMemoryFeatureFlagBitsNV {
        match self {
            Self::eDedicatedOnlyBit => RawExternalMemoryFeatureFlagBitsNV::eDedicatedOnlyBit,
            Self::eExportableBit => RawExternalMemoryFeatureFlagBitsNV::eExportableBit,
            Self::eImportableBit => RawExternalMemoryFeatureFlagBitsNV::eImportableBit,
            Self::eUnknownBit(b) => RawExternalMemoryFeatureFlagBitsNV(b),
        }
    }
}

impl core::convert::From<ExternalMemoryFeatureFlagBitsNV> for ExternalMemoryFeatureFlagsNV {
    fn from(value: ExternalMemoryFeatureFlagBitsNV) -> Self {
        match value {
            ExternalMemoryFeatureFlagBitsNV::eDedicatedOnlyBit => ExternalMemoryFeatureFlagsNV::eDedicatedOnlyBit,
            ExternalMemoryFeatureFlagBitsNV::eExportableBit => ExternalMemoryFeatureFlagsNV::eExportableBit,
            ExternalMemoryFeatureFlagBitsNV::eImportableBit => ExternalMemoryFeatureFlagsNV::eImportableBit,
            ExternalMemoryFeatureFlagBitsNV::eUnknownBit(b) => ExternalMemoryFeatureFlagsNV(b),
        }
    }
}

bit_field_enum_derives!(ExternalMemoryFeatureFlagBitsNV, ExternalMemoryFeatureFlagsNV, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ExternalMemoryFeatureFlagsNV(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ExternalMemoryFeatureFlagsNV {
    pub const eNone: Self = Self(0);
    pub const eDedicatedOnlyBit: Self = Self(0b1);
    pub const eExportableBit: Self = Self(0b10);
    pub const eImportableBit: Self = Self(0b100);
}

bit_field_mask_derives!(ExternalMemoryFeatureFlagsNV, u32);

bit_field_mask_with_enum_debug_derive!(ExternalMemoryFeatureFlagBitsNV, ExternalMemoryFeatureFlagsNV, u32, ExternalMemoryFeatureFlagBitsNV::eDedicatedOnlyBit, ExternalMemoryFeatureFlagBitsNV::eExportableBit, ExternalMemoryFeatureFlagBitsNV::eImportableBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ExternalMemoryHandleTypeFlagBits {
    eOpaqueFdBit,
    eOpaqueWin32Bit,
    eOpaqueWin32KmtBit,
    eD3d11TextureBit,
    eD3d11TextureKmtBit,
    eD3d12HeapBit,
    eD3d12ResourceBit,
    eHostAllocationBitExt,
    eHostMappedForeignMemoryBitExt,
    eDmaBufBitExt,
    eAndroidHardwareBufferBitAndroid,
    eZirconVmoBitFuchsia,
    eRdmaAddressBitNv,
    eSciBufBitNv,
    eUnknownBit(u32),
}

pub type ExternalMemoryHandleTypeFlagBitsKHR = ExternalMemoryHandleTypeFlagBits;

#[allow(non_upper_case_globals)]
impl ExternalMemoryHandleTypeFlagBits {
    pub const eOpaqueFdBitKhr: Self = Self::eOpaqueFdBit;
    pub const eOpaqueWin32BitKhr: Self = Self::eOpaqueWin32Bit;
    pub const eOpaqueWin32KmtBitKhr: Self = Self::eOpaqueWin32KmtBit;
    pub const eD3d11TextureBitKhr: Self = Self::eD3d11TextureBit;
    pub const eD3d11TextureKmtBitKhr: Self = Self::eD3d11TextureKmtBit;
    pub const eD3d12HeapBitKhr: Self = Self::eD3d12HeapBit;
    pub const eD3d12ResourceBitKhr: Self = Self::eD3d12ResourceBit;

    pub fn into_raw(self) -> RawExternalMemoryHandleTypeFlagBits {
        match self {
            Self::eOpaqueFdBit => RawExternalMemoryHandleTypeFlagBits::eOpaqueFdBit,
            Self::eOpaqueWin32Bit => RawExternalMemoryHandleTypeFlagBits::eOpaqueWin32Bit,
            Self::eOpaqueWin32KmtBit => RawExternalMemoryHandleTypeFlagBits::eOpaqueWin32KmtBit,
            Self::eD3d11TextureBit => RawExternalMemoryHandleTypeFlagBits::eD3d11TextureBit,
            Self::eD3d11TextureKmtBit => RawExternalMemoryHandleTypeFlagBits::eD3d11TextureKmtBit,
            Self::eD3d12HeapBit => RawExternalMemoryHandleTypeFlagBits::eD3d12HeapBit,
            Self::eD3d12ResourceBit => RawExternalMemoryHandleTypeFlagBits::eD3d12ResourceBit,
            Self::eHostAllocationBitExt => RawExternalMemoryHandleTypeFlagBits::eHostAllocationBitExt,
            Self::eHostMappedForeignMemoryBitExt => RawExternalMemoryHandleTypeFlagBits::eHostMappedForeignMemoryBitExt,
            Self::eDmaBufBitExt => RawExternalMemoryHandleTypeFlagBits::eDmaBufBitExt,
            Self::eAndroidHardwareBufferBitAndroid => RawExternalMemoryHandleTypeFlagBits::eAndroidHardwareBufferBitAndroid,
            Self::eZirconVmoBitFuchsia => RawExternalMemoryHandleTypeFlagBits::eZirconVmoBitFuchsia,
            Self::eRdmaAddressBitNv => RawExternalMemoryHandleTypeFlagBits::eRdmaAddressBitNv,
            Self::eSciBufBitNv => RawExternalMemoryHandleTypeFlagBits::eSciBufBitNv,
            Self::eUnknownBit(b) => RawExternalMemoryHandleTypeFlagBits(b),
        }
    }
}

impl core::convert::From<ExternalMemoryHandleTypeFlagBits> for ExternalMemoryHandleTypeFlags {
    fn from(value: ExternalMemoryHandleTypeFlagBits) -> Self {
        match value {
            ExternalMemoryHandleTypeFlagBits::eOpaqueFdBit => ExternalMemoryHandleTypeFlags::eOpaqueFdBit,
            ExternalMemoryHandleTypeFlagBits::eOpaqueWin32Bit => ExternalMemoryHandleTypeFlags::eOpaqueWin32Bit,
            ExternalMemoryHandleTypeFlagBits::eOpaqueWin32KmtBit => ExternalMemoryHandleTypeFlags::eOpaqueWin32KmtBit,
            ExternalMemoryHandleTypeFlagBits::eD3d11TextureBit => ExternalMemoryHandleTypeFlags::eD3d11TextureBit,
            ExternalMemoryHandleTypeFlagBits::eD3d11TextureKmtBit => ExternalMemoryHandleTypeFlags::eD3d11TextureKmtBit,
            ExternalMemoryHandleTypeFlagBits::eD3d12HeapBit => ExternalMemoryHandleTypeFlags::eD3d12HeapBit,
            ExternalMemoryHandleTypeFlagBits::eD3d12ResourceBit => ExternalMemoryHandleTypeFlags::eD3d12ResourceBit,
            ExternalMemoryHandleTypeFlagBits::eHostAllocationBitExt => ExternalMemoryHandleTypeFlags::eHostAllocationBitExt,
            ExternalMemoryHandleTypeFlagBits::eHostMappedForeignMemoryBitExt => ExternalMemoryHandleTypeFlags::eHostMappedForeignMemoryBitExt,
            ExternalMemoryHandleTypeFlagBits::eDmaBufBitExt => ExternalMemoryHandleTypeFlags::eDmaBufBitExt,
            ExternalMemoryHandleTypeFlagBits::eAndroidHardwareBufferBitAndroid => ExternalMemoryHandleTypeFlags::eAndroidHardwareBufferBitAndroid,
            ExternalMemoryHandleTypeFlagBits::eZirconVmoBitFuchsia => ExternalMemoryHandleTypeFlags::eZirconVmoBitFuchsia,
            ExternalMemoryHandleTypeFlagBits::eRdmaAddressBitNv => ExternalMemoryHandleTypeFlags::eRdmaAddressBitNv,
            ExternalMemoryHandleTypeFlagBits::eSciBufBitNv => ExternalMemoryHandleTypeFlags::eSciBufBitNv,
            ExternalMemoryHandleTypeFlagBits::eUnknownBit(b) => ExternalMemoryHandleTypeFlags(b),
        }
    }
}

bit_field_enum_derives!(ExternalMemoryHandleTypeFlagBits, ExternalMemoryHandleTypeFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ExternalMemoryHandleTypeFlags(pub(crate) u32);

pub type ExternalMemoryHandleTypeFlagsKHR = ExternalMemoryHandleTypeFlags;

#[allow(non_upper_case_globals)]
impl ExternalMemoryHandleTypeFlags {
    pub const eNone: Self = Self(0);
    pub const eOpaqueFdBit: Self = Self(0b1);
    pub const eOpaqueWin32Bit: Self = Self(0b10);
    pub const eOpaqueWin32KmtBit: Self = Self(0b100);
    pub const eD3d11TextureBit: Self = Self(0b1000);
    pub const eD3d11TextureKmtBit: Self = Self(0b10000);
    pub const eD3d12HeapBit: Self = Self(0b100000);
    pub const eD3d12ResourceBit: Self = Self(0b1000000);
    pub const eHostAllocationBitExt: Self = Self(0b10000000);
    pub const eHostMappedForeignMemoryBitExt: Self = Self(0b100000000);
    pub const eDmaBufBitExt: Self = Self(0b1000000000);
    pub const eAndroidHardwareBufferBitAndroid: Self = Self(0b10000000000);
    pub const eZirconVmoBitFuchsia: Self = Self(0b100000000000);
    pub const eRdmaAddressBitNv: Self = Self(0b1000000000000);
    pub const eSciBufBitNv: Self = Self(0b10000000000000);
    pub const eOpaqueFdBitKhr: Self = Self::eOpaqueFdBit;
    pub const eOpaqueWin32BitKhr: Self = Self::eOpaqueWin32Bit;
    pub const eOpaqueWin32KmtBitKhr: Self = Self::eOpaqueWin32KmtBit;
    pub const eD3d11TextureBitKhr: Self = Self::eD3d11TextureBit;
    pub const eD3d11TextureKmtBitKhr: Self = Self::eD3d11TextureKmtBit;
    pub const eD3d12HeapBitKhr: Self = Self::eD3d12HeapBit;
    pub const eD3d12ResourceBitKhr: Self = Self::eD3d12ResourceBit;
}

bit_field_mask_derives!(ExternalMemoryHandleTypeFlags, u32);

bit_field_mask_with_enum_debug_derive!(ExternalMemoryHandleTypeFlagBits, ExternalMemoryHandleTypeFlags, u32, ExternalMemoryHandleTypeFlagBits::eOpaqueFdBit, ExternalMemoryHandleTypeFlagBits::eOpaqueWin32Bit, ExternalMemoryHandleTypeFlagBits::eOpaqueWin32KmtBit, ExternalMemoryHandleTypeFlagBits::eD3d11TextureBit, ExternalMemoryHandleTypeFlagBits::eD3d11TextureKmtBit, ExternalMemoryHandleTypeFlagBits::eD3d12HeapBit, ExternalMemoryHandleTypeFlagBits::eD3d12ResourceBit, ExternalMemoryHandleTypeFlagBits::eHostAllocationBitExt, ExternalMemoryHandleTypeFlagBits::eHostMappedForeignMemoryBitExt, ExternalMemoryHandleTypeFlagBits::eDmaBufBitExt, ExternalMemoryHandleTypeFlagBits::eAndroidHardwareBufferBitAndroid, ExternalMemoryHandleTypeFlagBits::eZirconVmoBitFuchsia, ExternalMemoryHandleTypeFlagBits::eRdmaAddressBitNv, ExternalMemoryHandleTypeFlagBits::eSciBufBitNv);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ExternalMemoryFeatureFlagBits {
    eDedicatedOnlyBit,
    eExportableBit,
    eImportableBit,
    eUnknownBit(u32),
}

pub type ExternalMemoryFeatureFlagBitsKHR = ExternalMemoryFeatureFlagBits;

#[allow(non_upper_case_globals)]
impl ExternalMemoryFeatureFlagBits {
    pub const eDedicatedOnlyBitKhr: Self = Self::eDedicatedOnlyBit;
    pub const eExportableBitKhr: Self = Self::eExportableBit;
    pub const eImportableBitKhr: Self = Self::eImportableBit;

    pub fn into_raw(self) -> RawExternalMemoryFeatureFlagBits {
        match self {
            Self::eDedicatedOnlyBit => RawExternalMemoryFeatureFlagBits::eDedicatedOnlyBit,
            Self::eExportableBit => RawExternalMemoryFeatureFlagBits::eExportableBit,
            Self::eImportableBit => RawExternalMemoryFeatureFlagBits::eImportableBit,
            Self::eUnknownBit(b) => RawExternalMemoryFeatureFlagBits(b),
        }
    }
}

impl core::convert::From<ExternalMemoryFeatureFlagBits> for ExternalMemoryFeatureFlags {
    fn from(value: ExternalMemoryFeatureFlagBits) -> Self {
        match value {
            ExternalMemoryFeatureFlagBits::eDedicatedOnlyBit => ExternalMemoryFeatureFlags::eDedicatedOnlyBit,
            ExternalMemoryFeatureFlagBits::eExportableBit => ExternalMemoryFeatureFlags::eExportableBit,
            ExternalMemoryFeatureFlagBits::eImportableBit => ExternalMemoryFeatureFlags::eImportableBit,
            ExternalMemoryFeatureFlagBits::eUnknownBit(b) => ExternalMemoryFeatureFlags(b),
        }
    }
}

bit_field_enum_derives!(ExternalMemoryFeatureFlagBits, ExternalMemoryFeatureFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ExternalMemoryFeatureFlags(pub(crate) u32);

pub type ExternalMemoryFeatureFlagsKHR = ExternalMemoryFeatureFlags;

#[allow(non_upper_case_globals)]
impl ExternalMemoryFeatureFlags {
    pub const eNone: Self = Self(0);
    pub const eDedicatedOnlyBit: Self = Self(0b1);
    pub const eExportableBit: Self = Self(0b10);
    pub const eImportableBit: Self = Self(0b100);
    pub const eDedicatedOnlyBitKhr: Self = Self::eDedicatedOnlyBit;
    pub const eExportableBitKhr: Self = Self::eExportableBit;
    pub const eImportableBitKhr: Self = Self::eImportableBit;
}

bit_field_mask_derives!(ExternalMemoryFeatureFlags, u32);

bit_field_mask_with_enum_debug_derive!(ExternalMemoryFeatureFlagBits, ExternalMemoryFeatureFlags, u32, ExternalMemoryFeatureFlagBits::eDedicatedOnlyBit, ExternalMemoryFeatureFlagBits::eExportableBit, ExternalMemoryFeatureFlagBits::eImportableBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ExternalSemaphoreHandleTypeFlagBits {
    eOpaqueFdBit,
    eOpaqueWin32Bit,
    eOpaqueWin32KmtBit,
    eD3d12FenceBit,
    eSyncFdBit,
    eSciSyncObjBitNv,
    eZirconEventBitFuchsia,
    eUnknownBit(u32),
}

pub type ExternalSemaphoreHandleTypeFlagBitsKHR = ExternalSemaphoreHandleTypeFlagBits;

#[allow(non_upper_case_globals)]
impl ExternalSemaphoreHandleTypeFlagBits {
    pub const eOpaqueFdBitKhr: Self = Self::eOpaqueFdBit;
    pub const eOpaqueWin32BitKhr: Self = Self::eOpaqueWin32Bit;
    pub const eOpaqueWin32KmtBitKhr: Self = Self::eOpaqueWin32KmtBit;
    pub const eD3d11FenceBit: Self = Self::eD3d12FenceBit;
    pub const eD3d12FenceBitKhr: Self = Self::eD3d12FenceBit;
    pub const eSyncFdBitKhr: Self = Self::eSyncFdBit;

    pub fn into_raw(self) -> RawExternalSemaphoreHandleTypeFlagBits {
        match self {
            Self::eOpaqueFdBit => RawExternalSemaphoreHandleTypeFlagBits::eOpaqueFdBit,
            Self::eOpaqueWin32Bit => RawExternalSemaphoreHandleTypeFlagBits::eOpaqueWin32Bit,
            Self::eOpaqueWin32KmtBit => RawExternalSemaphoreHandleTypeFlagBits::eOpaqueWin32KmtBit,
            Self::eD3d12FenceBit => RawExternalSemaphoreHandleTypeFlagBits::eD3d12FenceBit,
            Self::eSyncFdBit => RawExternalSemaphoreHandleTypeFlagBits::eSyncFdBit,
            Self::eSciSyncObjBitNv => RawExternalSemaphoreHandleTypeFlagBits::eSciSyncObjBitNv,
            Self::eZirconEventBitFuchsia => RawExternalSemaphoreHandleTypeFlagBits::eZirconEventBitFuchsia,
            Self::eUnknownBit(b) => RawExternalSemaphoreHandleTypeFlagBits(b),
        }
    }
}

impl core::convert::From<ExternalSemaphoreHandleTypeFlagBits> for ExternalSemaphoreHandleTypeFlags {
    fn from(value: ExternalSemaphoreHandleTypeFlagBits) -> Self {
        match value {
            ExternalSemaphoreHandleTypeFlagBits::eOpaqueFdBit => ExternalSemaphoreHandleTypeFlags::eOpaqueFdBit,
            ExternalSemaphoreHandleTypeFlagBits::eOpaqueWin32Bit => ExternalSemaphoreHandleTypeFlags::eOpaqueWin32Bit,
            ExternalSemaphoreHandleTypeFlagBits::eOpaqueWin32KmtBit => ExternalSemaphoreHandleTypeFlags::eOpaqueWin32KmtBit,
            ExternalSemaphoreHandleTypeFlagBits::eD3d12FenceBit => ExternalSemaphoreHandleTypeFlags::eD3d12FenceBit,
            ExternalSemaphoreHandleTypeFlagBits::eSyncFdBit => ExternalSemaphoreHandleTypeFlags::eSyncFdBit,
            ExternalSemaphoreHandleTypeFlagBits::eSciSyncObjBitNv => ExternalSemaphoreHandleTypeFlags::eSciSyncObjBitNv,
            ExternalSemaphoreHandleTypeFlagBits::eZirconEventBitFuchsia => ExternalSemaphoreHandleTypeFlags::eZirconEventBitFuchsia,
            ExternalSemaphoreHandleTypeFlagBits::eUnknownBit(b) => ExternalSemaphoreHandleTypeFlags(b),
        }
    }
}

bit_field_enum_derives!(ExternalSemaphoreHandleTypeFlagBits, ExternalSemaphoreHandleTypeFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ExternalSemaphoreHandleTypeFlags(pub(crate) u32);

pub type ExternalSemaphoreHandleTypeFlagsKHR = ExternalSemaphoreHandleTypeFlags;

#[allow(non_upper_case_globals)]
impl ExternalSemaphoreHandleTypeFlags {
    pub const eNone: Self = Self(0);
    pub const eOpaqueFdBit: Self = Self(0b1);
    pub const eOpaqueWin32Bit: Self = Self(0b10);
    pub const eOpaqueWin32KmtBit: Self = Self(0b100);
    pub const eD3d12FenceBit: Self = Self(0b1000);
    pub const eSyncFdBit: Self = Self(0b10000);
    pub const eSciSyncObjBitNv: Self = Self(0b100000);
    pub const eZirconEventBitFuchsia: Self = Self(0b10000000);
    pub const eOpaqueFdBitKhr: Self = Self::eOpaqueFdBit;
    pub const eOpaqueWin32BitKhr: Self = Self::eOpaqueWin32Bit;
    pub const eOpaqueWin32KmtBitKhr: Self = Self::eOpaqueWin32KmtBit;
    pub const eD3d11FenceBit: Self = Self::eD3d12FenceBit;
    pub const eD3d12FenceBitKhr: Self = Self::eD3d12FenceBit;
    pub const eSyncFdBitKhr: Self = Self::eSyncFdBit;
}

bit_field_mask_derives!(ExternalSemaphoreHandleTypeFlags, u32);

bit_field_mask_with_enum_debug_derive!(ExternalSemaphoreHandleTypeFlagBits, ExternalSemaphoreHandleTypeFlags, u32, ExternalSemaphoreHandleTypeFlagBits::eOpaqueFdBit, ExternalSemaphoreHandleTypeFlagBits::eOpaqueWin32Bit, ExternalSemaphoreHandleTypeFlagBits::eOpaqueWin32KmtBit, ExternalSemaphoreHandleTypeFlagBits::eD3d12FenceBit, ExternalSemaphoreHandleTypeFlagBits::eSyncFdBit, ExternalSemaphoreHandleTypeFlagBits::eSciSyncObjBitNv, ExternalSemaphoreHandleTypeFlagBits::eZirconEventBitFuchsia);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ExternalSemaphoreFeatureFlagBits {
    eExportableBit,
    eImportableBit,
    eUnknownBit(u32),
}

pub type ExternalSemaphoreFeatureFlagBitsKHR = ExternalSemaphoreFeatureFlagBits;

#[allow(non_upper_case_globals)]
impl ExternalSemaphoreFeatureFlagBits {
    pub const eExportableBitKhr: Self = Self::eExportableBit;
    pub const eImportableBitKhr: Self = Self::eImportableBit;

    pub fn into_raw(self) -> RawExternalSemaphoreFeatureFlagBits {
        match self {
            Self::eExportableBit => RawExternalSemaphoreFeatureFlagBits::eExportableBit,
            Self::eImportableBit => RawExternalSemaphoreFeatureFlagBits::eImportableBit,
            Self::eUnknownBit(b) => RawExternalSemaphoreFeatureFlagBits(b),
        }
    }
}

impl core::convert::From<ExternalSemaphoreFeatureFlagBits> for ExternalSemaphoreFeatureFlags {
    fn from(value: ExternalSemaphoreFeatureFlagBits) -> Self {
        match value {
            ExternalSemaphoreFeatureFlagBits::eExportableBit => ExternalSemaphoreFeatureFlags::eExportableBit,
            ExternalSemaphoreFeatureFlagBits::eImportableBit => ExternalSemaphoreFeatureFlags::eImportableBit,
            ExternalSemaphoreFeatureFlagBits::eUnknownBit(b) => ExternalSemaphoreFeatureFlags(b),
        }
    }
}

bit_field_enum_derives!(ExternalSemaphoreFeatureFlagBits, ExternalSemaphoreFeatureFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ExternalSemaphoreFeatureFlags(pub(crate) u32);

pub type ExternalSemaphoreFeatureFlagsKHR = ExternalSemaphoreFeatureFlags;

#[allow(non_upper_case_globals)]
impl ExternalSemaphoreFeatureFlags {
    pub const eNone: Self = Self(0);
    pub const eExportableBit: Self = Self(0b1);
    pub const eImportableBit: Self = Self(0b10);
    pub const eExportableBitKhr: Self = Self::eExportableBit;
    pub const eImportableBitKhr: Self = Self::eImportableBit;
}

bit_field_mask_derives!(ExternalSemaphoreFeatureFlags, u32);

bit_field_mask_with_enum_debug_derive!(ExternalSemaphoreFeatureFlagBits, ExternalSemaphoreFeatureFlags, u32, ExternalSemaphoreFeatureFlagBits::eExportableBit, ExternalSemaphoreFeatureFlagBits::eImportableBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SemaphoreImportFlagBits {
    eTemporaryBit,
    eUnknownBit(u32),
}

pub type SemaphoreImportFlagBitsKHR = SemaphoreImportFlagBits;

#[allow(non_upper_case_globals)]
impl SemaphoreImportFlagBits {
    pub const eTemporaryBitKhr: Self = Self::eTemporaryBit;

    pub fn into_raw(self) -> RawSemaphoreImportFlagBits {
        match self {
            Self::eTemporaryBit => RawSemaphoreImportFlagBits::eTemporaryBit,
            Self::eUnknownBit(b) => RawSemaphoreImportFlagBits(b),
        }
    }
}

impl core::convert::From<SemaphoreImportFlagBits> for SemaphoreImportFlags {
    fn from(value: SemaphoreImportFlagBits) -> Self {
        match value {
            SemaphoreImportFlagBits::eTemporaryBit => SemaphoreImportFlags::eTemporaryBit,
            SemaphoreImportFlagBits::eUnknownBit(b) => SemaphoreImportFlags(b),
        }
    }
}

bit_field_enum_derives!(SemaphoreImportFlagBits, SemaphoreImportFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct SemaphoreImportFlags(pub(crate) u32);

pub type SemaphoreImportFlagsKHR = SemaphoreImportFlags;

#[allow(non_upper_case_globals)]
impl SemaphoreImportFlags {
    pub const eNone: Self = Self(0);
    pub const eTemporaryBit: Self = Self(0b1);
    pub const eTemporaryBitKhr: Self = Self::eTemporaryBit;
}

bit_field_mask_derives!(SemaphoreImportFlags, u32);

bit_field_mask_with_enum_debug_derive!(SemaphoreImportFlagBits, SemaphoreImportFlags, u32, SemaphoreImportFlagBits::eTemporaryBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ExternalFenceHandleTypeFlagBits {
    eOpaqueFdBit,
    eOpaqueWin32Bit,
    eOpaqueWin32KmtBit,
    eSyncFdBit,
    eSciSyncObjBitNv,
    eSciSyncFenceBitNv,
    eUnknownBit(u32),
}

pub type ExternalFenceHandleTypeFlagBitsKHR = ExternalFenceHandleTypeFlagBits;

#[allow(non_upper_case_globals)]
impl ExternalFenceHandleTypeFlagBits {
    pub const eOpaqueFdBitKhr: Self = Self::eOpaqueFdBit;
    pub const eOpaqueWin32BitKhr: Self = Self::eOpaqueWin32Bit;
    pub const eOpaqueWin32KmtBitKhr: Self = Self::eOpaqueWin32KmtBit;
    pub const eSyncFdBitKhr: Self = Self::eSyncFdBit;

    pub fn into_raw(self) -> RawExternalFenceHandleTypeFlagBits {
        match self {
            Self::eOpaqueFdBit => RawExternalFenceHandleTypeFlagBits::eOpaqueFdBit,
            Self::eOpaqueWin32Bit => RawExternalFenceHandleTypeFlagBits::eOpaqueWin32Bit,
            Self::eOpaqueWin32KmtBit => RawExternalFenceHandleTypeFlagBits::eOpaqueWin32KmtBit,
            Self::eSyncFdBit => RawExternalFenceHandleTypeFlagBits::eSyncFdBit,
            Self::eSciSyncObjBitNv => RawExternalFenceHandleTypeFlagBits::eSciSyncObjBitNv,
            Self::eSciSyncFenceBitNv => RawExternalFenceHandleTypeFlagBits::eSciSyncFenceBitNv,
            Self::eUnknownBit(b) => RawExternalFenceHandleTypeFlagBits(b),
        }
    }
}

impl core::convert::From<ExternalFenceHandleTypeFlagBits> for ExternalFenceHandleTypeFlags {
    fn from(value: ExternalFenceHandleTypeFlagBits) -> Self {
        match value {
            ExternalFenceHandleTypeFlagBits::eOpaqueFdBit => ExternalFenceHandleTypeFlags::eOpaqueFdBit,
            ExternalFenceHandleTypeFlagBits::eOpaqueWin32Bit => ExternalFenceHandleTypeFlags::eOpaqueWin32Bit,
            ExternalFenceHandleTypeFlagBits::eOpaqueWin32KmtBit => ExternalFenceHandleTypeFlags::eOpaqueWin32KmtBit,
            ExternalFenceHandleTypeFlagBits::eSyncFdBit => ExternalFenceHandleTypeFlags::eSyncFdBit,
            ExternalFenceHandleTypeFlagBits::eSciSyncObjBitNv => ExternalFenceHandleTypeFlags::eSciSyncObjBitNv,
            ExternalFenceHandleTypeFlagBits::eSciSyncFenceBitNv => ExternalFenceHandleTypeFlags::eSciSyncFenceBitNv,
            ExternalFenceHandleTypeFlagBits::eUnknownBit(b) => ExternalFenceHandleTypeFlags(b),
        }
    }
}

bit_field_enum_derives!(ExternalFenceHandleTypeFlagBits, ExternalFenceHandleTypeFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ExternalFenceHandleTypeFlags(pub(crate) u32);

pub type ExternalFenceHandleTypeFlagsKHR = ExternalFenceHandleTypeFlags;

#[allow(non_upper_case_globals)]
impl ExternalFenceHandleTypeFlags {
    pub const eNone: Self = Self(0);
    pub const eOpaqueFdBit: Self = Self(0b1);
    pub const eOpaqueWin32Bit: Self = Self(0b10);
    pub const eOpaqueWin32KmtBit: Self = Self(0b100);
    pub const eSyncFdBit: Self = Self(0b1000);
    pub const eSciSyncObjBitNv: Self = Self(0b10000);
    pub const eSciSyncFenceBitNv: Self = Self(0b100000);
    pub const eOpaqueFdBitKhr: Self = Self::eOpaqueFdBit;
    pub const eOpaqueWin32BitKhr: Self = Self::eOpaqueWin32Bit;
    pub const eOpaqueWin32KmtBitKhr: Self = Self::eOpaqueWin32KmtBit;
    pub const eSyncFdBitKhr: Self = Self::eSyncFdBit;
}

bit_field_mask_derives!(ExternalFenceHandleTypeFlags, u32);

bit_field_mask_with_enum_debug_derive!(ExternalFenceHandleTypeFlagBits, ExternalFenceHandleTypeFlags, u32, ExternalFenceHandleTypeFlagBits::eOpaqueFdBit, ExternalFenceHandleTypeFlagBits::eOpaqueWin32Bit, ExternalFenceHandleTypeFlagBits::eOpaqueWin32KmtBit, ExternalFenceHandleTypeFlagBits::eSyncFdBit, ExternalFenceHandleTypeFlagBits::eSciSyncObjBitNv, ExternalFenceHandleTypeFlagBits::eSciSyncFenceBitNv);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ExternalFenceFeatureFlagBits {
    eExportableBit,
    eImportableBit,
    eUnknownBit(u32),
}

pub type ExternalFenceFeatureFlagBitsKHR = ExternalFenceFeatureFlagBits;

#[allow(non_upper_case_globals)]
impl ExternalFenceFeatureFlagBits {
    pub const eExportableBitKhr: Self = Self::eExportableBit;
    pub const eImportableBitKhr: Self = Self::eImportableBit;

    pub fn into_raw(self) -> RawExternalFenceFeatureFlagBits {
        match self {
            Self::eExportableBit => RawExternalFenceFeatureFlagBits::eExportableBit,
            Self::eImportableBit => RawExternalFenceFeatureFlagBits::eImportableBit,
            Self::eUnknownBit(b) => RawExternalFenceFeatureFlagBits(b),
        }
    }
}

impl core::convert::From<ExternalFenceFeatureFlagBits> for ExternalFenceFeatureFlags {
    fn from(value: ExternalFenceFeatureFlagBits) -> Self {
        match value {
            ExternalFenceFeatureFlagBits::eExportableBit => ExternalFenceFeatureFlags::eExportableBit,
            ExternalFenceFeatureFlagBits::eImportableBit => ExternalFenceFeatureFlags::eImportableBit,
            ExternalFenceFeatureFlagBits::eUnknownBit(b) => ExternalFenceFeatureFlags(b),
        }
    }
}

bit_field_enum_derives!(ExternalFenceFeatureFlagBits, ExternalFenceFeatureFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ExternalFenceFeatureFlags(pub(crate) u32);

pub type ExternalFenceFeatureFlagsKHR = ExternalFenceFeatureFlags;

#[allow(non_upper_case_globals)]
impl ExternalFenceFeatureFlags {
    pub const eNone: Self = Self(0);
    pub const eExportableBit: Self = Self(0b1);
    pub const eImportableBit: Self = Self(0b10);
    pub const eExportableBitKhr: Self = Self::eExportableBit;
    pub const eImportableBitKhr: Self = Self::eImportableBit;
}

bit_field_mask_derives!(ExternalFenceFeatureFlags, u32);

bit_field_mask_with_enum_debug_derive!(ExternalFenceFeatureFlagBits, ExternalFenceFeatureFlags, u32, ExternalFenceFeatureFlagBits::eExportableBit, ExternalFenceFeatureFlagBits::eImportableBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum FenceImportFlagBits {
    eTemporaryBit,
    eUnknownBit(u32),
}

pub type FenceImportFlagBitsKHR = FenceImportFlagBits;

#[allow(non_upper_case_globals)]
impl FenceImportFlagBits {
    pub const eTemporaryBitKhr: Self = Self::eTemporaryBit;

    pub fn into_raw(self) -> RawFenceImportFlagBits {
        match self {
            Self::eTemporaryBit => RawFenceImportFlagBits::eTemporaryBit,
            Self::eUnknownBit(b) => RawFenceImportFlagBits(b),
        }
    }
}

impl core::convert::From<FenceImportFlagBits> for FenceImportFlags {
    fn from(value: FenceImportFlagBits) -> Self {
        match value {
            FenceImportFlagBits::eTemporaryBit => FenceImportFlags::eTemporaryBit,
            FenceImportFlagBits::eUnknownBit(b) => FenceImportFlags(b),
        }
    }
}

bit_field_enum_derives!(FenceImportFlagBits, FenceImportFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct FenceImportFlags(pub(crate) u32);

pub type FenceImportFlagsKHR = FenceImportFlags;

#[allow(non_upper_case_globals)]
impl FenceImportFlags {
    pub const eNone: Self = Self(0);
    pub const eTemporaryBit: Self = Self(0b1);
    pub const eTemporaryBitKhr: Self = Self::eTemporaryBit;
}

bit_field_mask_derives!(FenceImportFlags, u32);

bit_field_mask_with_enum_debug_derive!(FenceImportFlagBits, FenceImportFlags, u32, FenceImportFlagBits::eTemporaryBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SurfaceCounterFlagBitsEXT {
    eVblankBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl SurfaceCounterFlagBitsEXT {
    pub const eVblank: Self = Self::eVblankBit;

    pub fn into_raw(self) -> RawSurfaceCounterFlagBitsEXT {
        match self {
            Self::eVblankBit => RawSurfaceCounterFlagBitsEXT::eVblankBit,
            Self::eUnknownBit(b) => RawSurfaceCounterFlagBitsEXT(b),
        }
    }
}

impl core::convert::From<SurfaceCounterFlagBitsEXT> for SurfaceCounterFlagsEXT {
    fn from(value: SurfaceCounterFlagBitsEXT) -> Self {
        match value {
            SurfaceCounterFlagBitsEXT::eVblankBit => SurfaceCounterFlagsEXT::eVblankBit,
            SurfaceCounterFlagBitsEXT::eUnknownBit(b) => SurfaceCounterFlagsEXT(b),
        }
    }
}

bit_field_enum_derives!(SurfaceCounterFlagBitsEXT, SurfaceCounterFlagsEXT, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct SurfaceCounterFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl SurfaceCounterFlagsEXT {
    pub const eNone: Self = Self(0);
    pub const eVblankBit: Self = Self(0b1);
    pub const eVblank: Self = Self::eVblankBit;
}

bit_field_mask_derives!(SurfaceCounterFlagsEXT, u32);

bit_field_mask_with_enum_debug_derive!(SurfaceCounterFlagBitsEXT, SurfaceCounterFlagsEXT, u32, SurfaceCounterFlagBitsEXT::eVblankBit);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineViewportSwizzleStateCreateFlagsNV(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineViewportSwizzleStateCreateFlagsNV {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(PipelineViewportSwizzleStateCreateFlagsNV, u32);

fieldless_debug_derive!(PipelineViewportSwizzleStateCreateFlagsNV);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineDiscardRectangleStateCreateFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineDiscardRectangleStateCreateFlagsEXT {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(PipelineDiscardRectangleStateCreateFlagsEXT, u32);

fieldless_debug_derive!(PipelineDiscardRectangleStateCreateFlagsEXT);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineCoverageToColourStateCreateFlagsNV(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineCoverageToColourStateCreateFlagsNV {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(PipelineCoverageToColourStateCreateFlagsNV, u32);

fieldless_debug_derive!(PipelineCoverageToColourStateCreateFlagsNV);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineCoverageModulationStateCreateFlagsNV(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineCoverageModulationStateCreateFlagsNV {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(PipelineCoverageModulationStateCreateFlagsNV, u32);

fieldless_debug_derive!(PipelineCoverageModulationStateCreateFlagsNV);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineCoverageReductionStateCreateFlagsNV(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineCoverageReductionStateCreateFlagsNV {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(PipelineCoverageReductionStateCreateFlagsNV, u32);

fieldless_debug_derive!(PipelineCoverageReductionStateCreateFlagsNV);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ValidationCacheCreateFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ValidationCacheCreateFlagsEXT {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(ValidationCacheCreateFlagsEXT, u32);

fieldless_debug_derive!(ValidationCacheCreateFlagsEXT);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DebugUtilsMessageSeverityFlagBitsEXT {
    eVerboseBit,
    eInfoBit,
    eWarningBit,
    eErrorBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl DebugUtilsMessageSeverityFlagBitsEXT {
    pub fn into_raw(self) -> RawDebugUtilsMessageSeverityFlagBitsEXT {
        match self {
            Self::eVerboseBit => RawDebugUtilsMessageSeverityFlagBitsEXT::eVerboseBit,
            Self::eInfoBit => RawDebugUtilsMessageSeverityFlagBitsEXT::eInfoBit,
            Self::eWarningBit => RawDebugUtilsMessageSeverityFlagBitsEXT::eWarningBit,
            Self::eErrorBit => RawDebugUtilsMessageSeverityFlagBitsEXT::eErrorBit,
            Self::eUnknownBit(b) => RawDebugUtilsMessageSeverityFlagBitsEXT(b),
        }
    }
}

impl core::convert::From<DebugUtilsMessageSeverityFlagBitsEXT> for DebugUtilsMessageSeverityFlagsEXT {
    fn from(value: DebugUtilsMessageSeverityFlagBitsEXT) -> Self {
        match value {
            DebugUtilsMessageSeverityFlagBitsEXT::eVerboseBit => DebugUtilsMessageSeverityFlagsEXT::eVerboseBit,
            DebugUtilsMessageSeverityFlagBitsEXT::eInfoBit => DebugUtilsMessageSeverityFlagsEXT::eInfoBit,
            DebugUtilsMessageSeverityFlagBitsEXT::eWarningBit => DebugUtilsMessageSeverityFlagsEXT::eWarningBit,
            DebugUtilsMessageSeverityFlagBitsEXT::eErrorBit => DebugUtilsMessageSeverityFlagsEXT::eErrorBit,
            DebugUtilsMessageSeverityFlagBitsEXT::eUnknownBit(b) => DebugUtilsMessageSeverityFlagsEXT(b),
        }
    }
}

bit_field_enum_derives!(DebugUtilsMessageSeverityFlagBitsEXT, DebugUtilsMessageSeverityFlagsEXT, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DebugUtilsMessageSeverityFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DebugUtilsMessageSeverityFlagsEXT {
    pub const eNone: Self = Self(0);
    pub const eVerboseBit: Self = Self(0b1);
    pub const eInfoBit: Self = Self(0b10000);
    pub const eWarningBit: Self = Self(0b100000000);
    pub const eErrorBit: Self = Self(0b1000000000000);
}

bit_field_mask_derives!(DebugUtilsMessageSeverityFlagsEXT, u32);

bit_field_mask_with_enum_debug_derive!(DebugUtilsMessageSeverityFlagBitsEXT, DebugUtilsMessageSeverityFlagsEXT, u32, DebugUtilsMessageSeverityFlagBitsEXT::eVerboseBit, DebugUtilsMessageSeverityFlagBitsEXT::eInfoBit, DebugUtilsMessageSeverityFlagBitsEXT::eWarningBit, DebugUtilsMessageSeverityFlagBitsEXT::eErrorBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DebugUtilsMessageTypeFlagBitsEXT {
    eGeneralBit,
    eValidationBit,
    ePerformanceBit,
    eDeviceAddressBindingBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl DebugUtilsMessageTypeFlagBitsEXT {
    pub fn into_raw(self) -> RawDebugUtilsMessageTypeFlagBitsEXT {
        match self {
            Self::eGeneralBit => RawDebugUtilsMessageTypeFlagBitsEXT::eGeneralBit,
            Self::eValidationBit => RawDebugUtilsMessageTypeFlagBitsEXT::eValidationBit,
            Self::ePerformanceBit => RawDebugUtilsMessageTypeFlagBitsEXT::ePerformanceBit,
            Self::eDeviceAddressBindingBit => RawDebugUtilsMessageTypeFlagBitsEXT::eDeviceAddressBindingBit,
            Self::eUnknownBit(b) => RawDebugUtilsMessageTypeFlagBitsEXT(b),
        }
    }
}

impl core::convert::From<DebugUtilsMessageTypeFlagBitsEXT> for DebugUtilsMessageTypeFlagsEXT {
    fn from(value: DebugUtilsMessageTypeFlagBitsEXT) -> Self {
        match value {
            DebugUtilsMessageTypeFlagBitsEXT::eGeneralBit => DebugUtilsMessageTypeFlagsEXT::eGeneralBit,
            DebugUtilsMessageTypeFlagBitsEXT::eValidationBit => DebugUtilsMessageTypeFlagsEXT::eValidationBit,
            DebugUtilsMessageTypeFlagBitsEXT::ePerformanceBit => DebugUtilsMessageTypeFlagsEXT::ePerformanceBit,
            DebugUtilsMessageTypeFlagBitsEXT::eDeviceAddressBindingBit => DebugUtilsMessageTypeFlagsEXT::eDeviceAddressBindingBit,
            DebugUtilsMessageTypeFlagBitsEXT::eUnknownBit(b) => DebugUtilsMessageTypeFlagsEXT(b),
        }
    }
}

bit_field_enum_derives!(DebugUtilsMessageTypeFlagBitsEXT, DebugUtilsMessageTypeFlagsEXT, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DebugUtilsMessageTypeFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DebugUtilsMessageTypeFlagsEXT {
    pub const eNone: Self = Self(0);
    pub const eGeneralBit: Self = Self(0b1);
    pub const eValidationBit: Self = Self(0b10);
    pub const ePerformanceBit: Self = Self(0b100);
    pub const eDeviceAddressBindingBit: Self = Self(0b1000);
}

bit_field_mask_derives!(DebugUtilsMessageTypeFlagsEXT, u32);

bit_field_mask_with_enum_debug_derive!(DebugUtilsMessageTypeFlagBitsEXT, DebugUtilsMessageTypeFlagsEXT, u32, DebugUtilsMessageTypeFlagBitsEXT::eGeneralBit, DebugUtilsMessageTypeFlagBitsEXT::eValidationBit, DebugUtilsMessageTypeFlagBitsEXT::ePerformanceBit, DebugUtilsMessageTypeFlagBitsEXT::eDeviceAddressBindingBit);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DebugUtilsMessengerCreateFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DebugUtilsMessengerCreateFlagsEXT {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(DebugUtilsMessengerCreateFlagsEXT, u32);

fieldless_debug_derive!(DebugUtilsMessengerCreateFlagsEXT);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DebugUtilsMessengerCallbackDataFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DebugUtilsMessengerCallbackDataFlagsEXT {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(DebugUtilsMessengerCallbackDataFlagsEXT, u32);

fieldless_debug_derive!(DebugUtilsMessengerCallbackDataFlagsEXT);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DeviceMemoryReportFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DeviceMemoryReportFlagsEXT {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(DeviceMemoryReportFlagsEXT, u32);

fieldless_debug_derive!(DeviceMemoryReportFlagsEXT);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineRasterizationConservativeStateCreateFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineRasterizationConservativeStateCreateFlagsEXT {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(PipelineRasterizationConservativeStateCreateFlagsEXT, u32);

fieldless_debug_derive!(PipelineRasterizationConservativeStateCreateFlagsEXT);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DescriptorBindingFlagBits {
    eUpdateAfterBindBit,
    eUpdateUnusedWhilePendingBit,
    ePartiallyBoundBit,
    eVariableDescriptorCountBit,
    eUnknownBit(u32),
}

pub type DescriptorBindingFlagBitsEXT = DescriptorBindingFlagBits;

#[allow(non_upper_case_globals)]
impl DescriptorBindingFlagBits {
    pub const eUpdateAfterBindBitExt: Self = Self::eUpdateAfterBindBit;
    pub const eUpdateUnusedWhilePendingBitExt: Self = Self::eUpdateUnusedWhilePendingBit;
    pub const ePartiallyBoundBitExt: Self = Self::ePartiallyBoundBit;
    pub const eVariableDescriptorCountBitExt: Self = Self::eVariableDescriptorCountBit;

    pub fn into_raw(self) -> RawDescriptorBindingFlagBits {
        match self {
            Self::eUpdateAfterBindBit => RawDescriptorBindingFlagBits::eUpdateAfterBindBit,
            Self::eUpdateUnusedWhilePendingBit => RawDescriptorBindingFlagBits::eUpdateUnusedWhilePendingBit,
            Self::ePartiallyBoundBit => RawDescriptorBindingFlagBits::ePartiallyBoundBit,
            Self::eVariableDescriptorCountBit => RawDescriptorBindingFlagBits::eVariableDescriptorCountBit,
            Self::eUnknownBit(b) => RawDescriptorBindingFlagBits(b),
        }
    }
}

impl core::convert::From<DescriptorBindingFlagBits> for DescriptorBindingFlags {
    fn from(value: DescriptorBindingFlagBits) -> Self {
        match value {
            DescriptorBindingFlagBits::eUpdateAfterBindBit => DescriptorBindingFlags::eUpdateAfterBindBit,
            DescriptorBindingFlagBits::eUpdateUnusedWhilePendingBit => DescriptorBindingFlags::eUpdateUnusedWhilePendingBit,
            DescriptorBindingFlagBits::ePartiallyBoundBit => DescriptorBindingFlags::ePartiallyBoundBit,
            DescriptorBindingFlagBits::eVariableDescriptorCountBit => DescriptorBindingFlags::eVariableDescriptorCountBit,
            DescriptorBindingFlagBits::eUnknownBit(b) => DescriptorBindingFlags(b),
        }
    }
}

bit_field_enum_derives!(DescriptorBindingFlagBits, DescriptorBindingFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DescriptorBindingFlags(pub(crate) u32);

pub type DescriptorBindingFlagsEXT = DescriptorBindingFlags;

#[allow(non_upper_case_globals)]
impl DescriptorBindingFlags {
    pub const eNone: Self = Self(0);
    pub const eUpdateAfterBindBit: Self = Self(0b1);
    pub const eUpdateUnusedWhilePendingBit: Self = Self(0b10);
    pub const ePartiallyBoundBit: Self = Self(0b100);
    pub const eVariableDescriptorCountBit: Self = Self(0b1000);
    pub const eUpdateAfterBindBitExt: Self = Self::eUpdateAfterBindBit;
    pub const eUpdateUnusedWhilePendingBitExt: Self = Self::eUpdateUnusedWhilePendingBit;
    pub const ePartiallyBoundBitExt: Self = Self::ePartiallyBoundBit;
    pub const eVariableDescriptorCountBitExt: Self = Self::eVariableDescriptorCountBit;
}

bit_field_mask_derives!(DescriptorBindingFlags, u32);

bit_field_mask_with_enum_debug_derive!(DescriptorBindingFlagBits, DescriptorBindingFlags, u32, DescriptorBindingFlagBits::eUpdateAfterBindBit, DescriptorBindingFlagBits::eUpdateUnusedWhilePendingBit, DescriptorBindingFlagBits::ePartiallyBoundBit, DescriptorBindingFlagBits::eVariableDescriptorCountBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ConditionalRenderingFlagBitsEXT {
    eInvertedBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl ConditionalRenderingFlagBitsEXT {
    pub fn into_raw(self) -> RawConditionalRenderingFlagBitsEXT {
        match self {
            Self::eInvertedBit => RawConditionalRenderingFlagBitsEXT::eInvertedBit,
            Self::eUnknownBit(b) => RawConditionalRenderingFlagBitsEXT(b),
        }
    }
}

impl core::convert::From<ConditionalRenderingFlagBitsEXT> for ConditionalRenderingFlagsEXT {
    fn from(value: ConditionalRenderingFlagBitsEXT) -> Self {
        match value {
            ConditionalRenderingFlagBitsEXT::eInvertedBit => ConditionalRenderingFlagsEXT::eInvertedBit,
            ConditionalRenderingFlagBitsEXT::eUnknownBit(b) => ConditionalRenderingFlagsEXT(b),
        }
    }
}

bit_field_enum_derives!(ConditionalRenderingFlagBitsEXT, ConditionalRenderingFlagsEXT, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ConditionalRenderingFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ConditionalRenderingFlagsEXT {
    pub const eNone: Self = Self(0);
    pub const eInvertedBit: Self = Self(0b1);
}

bit_field_mask_derives!(ConditionalRenderingFlagsEXT, u32);

bit_field_mask_with_enum_debug_derive!(ConditionalRenderingFlagBitsEXT, ConditionalRenderingFlagsEXT, u32, ConditionalRenderingFlagBitsEXT::eInvertedBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ResolveModeFlagBits {
    eNone,
    eSampleZeroBit,
    eAverageBit,
    eMinBit,
    eMaxBit,
    eUnknownBit(u32),
}

pub type ResolveModeFlagBitsKHR = ResolveModeFlagBits;

#[allow(non_upper_case_globals)]
impl ResolveModeFlagBits {
    pub const eNoneKhr: Self = Self::eNone;
    pub const eSampleZeroBitKhr: Self = Self::eSampleZeroBit;
    pub const eAverageBitKhr: Self = Self::eAverageBit;
    pub const eMinBitKhr: Self = Self::eMinBit;
    pub const eMaxBitKhr: Self = Self::eMaxBit;

    pub fn into_raw(self) -> RawResolveModeFlagBits {
        match self {
            Self::eNone => RawResolveModeFlagBits::eNone,
            Self::eSampleZeroBit => RawResolveModeFlagBits::eSampleZeroBit,
            Self::eAverageBit => RawResolveModeFlagBits::eAverageBit,
            Self::eMinBit => RawResolveModeFlagBits::eMinBit,
            Self::eMaxBit => RawResolveModeFlagBits::eMaxBit,
            Self::eUnknownBit(b) => RawResolveModeFlagBits(b),
        }
    }
}

impl core::convert::From<ResolveModeFlagBits> for ResolveModeFlags {
    fn from(value: ResolveModeFlagBits) -> Self {
        match value {
            ResolveModeFlagBits::eNone => ResolveModeFlags::eNone,
            ResolveModeFlagBits::eSampleZeroBit => ResolveModeFlags::eSampleZeroBit,
            ResolveModeFlagBits::eAverageBit => ResolveModeFlags::eAverageBit,
            ResolveModeFlagBits::eMinBit => ResolveModeFlags::eMinBit,
            ResolveModeFlagBits::eMaxBit => ResolveModeFlags::eMaxBit,
            ResolveModeFlagBits::eUnknownBit(b) => ResolveModeFlags(b),
        }
    }
}

bit_field_enum_derives!(ResolveModeFlagBits, ResolveModeFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ResolveModeFlags(pub(crate) u32);

pub type ResolveModeFlagsKHR = ResolveModeFlags;

#[allow(non_upper_case_globals)]
impl ResolveModeFlags {
    pub const eNone: Self = Self(0);
    pub const eSampleZeroBit: Self = Self(0b1);
    pub const eAverageBit: Self = Self(0b10);
    pub const eMinBit: Self = Self(0b100);
    pub const eMaxBit: Self = Self(0b1000);
    pub const eNoneKhr: Self = Self::eNone;
    pub const eSampleZeroBitKhr: Self = Self::eSampleZeroBit;
    pub const eAverageBitKhr: Self = Self::eAverageBit;
    pub const eMinBitKhr: Self = Self::eMinBit;
    pub const eMaxBitKhr: Self = Self::eMaxBit;
}

bit_field_mask_derives!(ResolveModeFlags, u32);

bit_field_mask_with_enum_debug_derive!(ResolveModeFlagBits, ResolveModeFlags, u32, ResolveModeFlagBits::eSampleZeroBit, ResolveModeFlagBits::eAverageBit, ResolveModeFlagBits::eMinBit, ResolveModeFlagBits::eMaxBit => ResolveModeFlagBits::eNone);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineRasterizationStateStreamCreateFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineRasterizationStateStreamCreateFlagsEXT {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(PipelineRasterizationStateStreamCreateFlagsEXT, u32);

fieldless_debug_derive!(PipelineRasterizationStateStreamCreateFlagsEXT);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PipelineRasterizationDepthClipStateCreateFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PipelineRasterizationDepthClipStateCreateFlagsEXT {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(PipelineRasterizationDepthClipStateCreateFlagsEXT, u32);

fieldless_debug_derive!(PipelineRasterizationDepthClipStateCreateFlagsEXT);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ToolPurposeFlagBits {
    eValidationBit,
    eProfilingBit,
    eTracingBit,
    eAdditionalFeaturesBit,
    eModifyingFeaturesBit,
    eDebugReportingBitExt,
    eDebugMarkersBitExt,
    eUnknownBit(u32),
}

pub type ToolPurposeFlagBitsEXT = ToolPurposeFlagBits;

#[allow(non_upper_case_globals)]
impl ToolPurposeFlagBits {
    pub const eValidationBitExt: Self = Self::eValidationBit;
    pub const eProfilingBitExt: Self = Self::eProfilingBit;
    pub const eTracingBitExt: Self = Self::eTracingBit;
    pub const eAdditionalFeaturesBitExt: Self = Self::eAdditionalFeaturesBit;
    pub const eModifyingFeaturesBitExt: Self = Self::eModifyingFeaturesBit;

    pub fn into_raw(self) -> RawToolPurposeFlagBits {
        match self {
            Self::eValidationBit => RawToolPurposeFlagBits::eValidationBit,
            Self::eProfilingBit => RawToolPurposeFlagBits::eProfilingBit,
            Self::eTracingBit => RawToolPurposeFlagBits::eTracingBit,
            Self::eAdditionalFeaturesBit => RawToolPurposeFlagBits::eAdditionalFeaturesBit,
            Self::eModifyingFeaturesBit => RawToolPurposeFlagBits::eModifyingFeaturesBit,
            Self::eDebugReportingBitExt => RawToolPurposeFlagBits::eDebugReportingBitExt,
            Self::eDebugMarkersBitExt => RawToolPurposeFlagBits::eDebugMarkersBitExt,
            Self::eUnknownBit(b) => RawToolPurposeFlagBits(b),
        }
    }
}

impl core::convert::From<ToolPurposeFlagBits> for ToolPurposeFlags {
    fn from(value: ToolPurposeFlagBits) -> Self {
        match value {
            ToolPurposeFlagBits::eValidationBit => ToolPurposeFlags::eValidationBit,
            ToolPurposeFlagBits::eProfilingBit => ToolPurposeFlags::eProfilingBit,
            ToolPurposeFlagBits::eTracingBit => ToolPurposeFlags::eTracingBit,
            ToolPurposeFlagBits::eAdditionalFeaturesBit => ToolPurposeFlags::eAdditionalFeaturesBit,
            ToolPurposeFlagBits::eModifyingFeaturesBit => ToolPurposeFlags::eModifyingFeaturesBit,
            ToolPurposeFlagBits::eDebugReportingBitExt => ToolPurposeFlags::eDebugReportingBitExt,
            ToolPurposeFlagBits::eDebugMarkersBitExt => ToolPurposeFlags::eDebugMarkersBitExt,
            ToolPurposeFlagBits::eUnknownBit(b) => ToolPurposeFlags(b),
        }
    }
}

bit_field_enum_derives!(ToolPurposeFlagBits, ToolPurposeFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ToolPurposeFlags(pub(crate) u32);

pub type ToolPurposeFlagsEXT = ToolPurposeFlags;

#[allow(non_upper_case_globals)]
impl ToolPurposeFlags {
    pub const eNone: Self = Self(0);
    pub const eValidationBit: Self = Self(0b1);
    pub const eProfilingBit: Self = Self(0b10);
    pub const eTracingBit: Self = Self(0b100);
    pub const eAdditionalFeaturesBit: Self = Self(0b1000);
    pub const eModifyingFeaturesBit: Self = Self(0b10000);
    pub const eDebugReportingBitExt: Self = Self(0b100000);
    pub const eDebugMarkersBitExt: Self = Self(0b1000000);
    pub const eValidationBitExt: Self = Self::eValidationBit;
    pub const eProfilingBitExt: Self = Self::eProfilingBit;
    pub const eTracingBitExt: Self = Self::eTracingBit;
    pub const eAdditionalFeaturesBitExt: Self = Self::eAdditionalFeaturesBit;
    pub const eModifyingFeaturesBitExt: Self = Self::eModifyingFeaturesBit;
}

bit_field_mask_derives!(ToolPurposeFlags, u32);

bit_field_mask_with_enum_debug_derive!(ToolPurposeFlagBits, ToolPurposeFlags, u32, ToolPurposeFlagBits::eValidationBit, ToolPurposeFlagBits::eProfilingBit, ToolPurposeFlagBits::eTracingBit, ToolPurposeFlagBits::eAdditionalFeaturesBit, ToolPurposeFlagBits::eModifyingFeaturesBit, ToolPurposeFlagBits::eDebugReportingBitExt, ToolPurposeFlagBits::eDebugMarkersBitExt);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum SubmitFlagBits {
    eProtectedBit,
    eUnknownBit(u32),
}

pub type SubmitFlagBitsKHR = SubmitFlagBits;

#[allow(non_upper_case_globals)]
impl SubmitFlagBits {
    pub const eProtectedBitKhr: Self = Self::eProtectedBit;

    pub fn into_raw(self) -> RawSubmitFlagBits {
        match self {
            Self::eProtectedBit => RawSubmitFlagBits::eProtectedBit,
            Self::eUnknownBit(b) => RawSubmitFlagBits(b),
        }
    }
}

impl core::convert::From<SubmitFlagBits> for SubmitFlags {
    fn from(value: SubmitFlagBits) -> Self {
        match value {
            SubmitFlagBits::eProtectedBit => SubmitFlags::eProtectedBit,
            SubmitFlagBits::eUnknownBit(b) => SubmitFlags(b),
        }
    }
}

bit_field_enum_derives!(SubmitFlagBits, SubmitFlags, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct SubmitFlags(pub(crate) u32);

pub type SubmitFlagsKHR = SubmitFlags;

#[allow(non_upper_case_globals)]
impl SubmitFlags {
    pub const eNone: Self = Self(0);
    pub const eProtectedBit: Self = Self(0b1);
    pub const eProtectedBitKhr: Self = Self::eProtectedBit;
}

bit_field_mask_derives!(SubmitFlags, u32);

bit_field_mask_with_enum_debug_derive!(SubmitFlagBits, SubmitFlags, u32, SubmitFlagBits::eProtectedBit);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ImageFormatConstraintsFlagsFUCHSIA(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ImageFormatConstraintsFlagsFUCHSIA {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(ImageFormatConstraintsFlagsFUCHSIA, u32);

fieldless_debug_derive!(ImageFormatConstraintsFlagsFUCHSIA);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ImageConstraintsInfoFlagBitsFUCHSIA {
    eCpuReadRarely,
    eCpuReadOften,
    eCpuWriteRarely,
    eCpuWriteOften,
    eProtectedOptional,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl ImageConstraintsInfoFlagBitsFUCHSIA {
    pub fn into_raw(self) -> RawImageConstraintsInfoFlagBitsFUCHSIA {
        match self {
            Self::eCpuReadRarely => RawImageConstraintsInfoFlagBitsFUCHSIA::eCpuReadRarely,
            Self::eCpuReadOften => RawImageConstraintsInfoFlagBitsFUCHSIA::eCpuReadOften,
            Self::eCpuWriteRarely => RawImageConstraintsInfoFlagBitsFUCHSIA::eCpuWriteRarely,
            Self::eCpuWriteOften => RawImageConstraintsInfoFlagBitsFUCHSIA::eCpuWriteOften,
            Self::eProtectedOptional => RawImageConstraintsInfoFlagBitsFUCHSIA::eProtectedOptional,
            Self::eUnknownBit(b) => RawImageConstraintsInfoFlagBitsFUCHSIA(b),
        }
    }
}

impl core::convert::From<ImageConstraintsInfoFlagBitsFUCHSIA> for ImageConstraintsInfoFlagsFUCHSIA {
    fn from(value: ImageConstraintsInfoFlagBitsFUCHSIA) -> Self {
        match value {
            ImageConstraintsInfoFlagBitsFUCHSIA::eCpuReadRarely => ImageConstraintsInfoFlagsFUCHSIA::eCpuReadRarely,
            ImageConstraintsInfoFlagBitsFUCHSIA::eCpuReadOften => ImageConstraintsInfoFlagsFUCHSIA::eCpuReadOften,
            ImageConstraintsInfoFlagBitsFUCHSIA::eCpuWriteRarely => ImageConstraintsInfoFlagsFUCHSIA::eCpuWriteRarely,
            ImageConstraintsInfoFlagBitsFUCHSIA::eCpuWriteOften => ImageConstraintsInfoFlagsFUCHSIA::eCpuWriteOften,
            ImageConstraintsInfoFlagBitsFUCHSIA::eProtectedOptional => ImageConstraintsInfoFlagsFUCHSIA::eProtectedOptional,
            ImageConstraintsInfoFlagBitsFUCHSIA::eUnknownBit(b) => ImageConstraintsInfoFlagsFUCHSIA(b),
        }
    }
}

bit_field_enum_derives!(ImageConstraintsInfoFlagBitsFUCHSIA, ImageConstraintsInfoFlagsFUCHSIA, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ImageConstraintsInfoFlagsFUCHSIA(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ImageConstraintsInfoFlagsFUCHSIA {
    pub const eNone: Self = Self(0);
    pub const eCpuReadRarely: Self = Self(0b1);
    pub const eCpuReadOften: Self = Self(0b10);
    pub const eCpuWriteRarely: Self = Self(0b100);
    pub const eCpuWriteOften: Self = Self(0b1000);
    pub const eProtectedOptional: Self = Self(0b10000);
}

bit_field_mask_derives!(ImageConstraintsInfoFlagsFUCHSIA, u32);

bit_field_mask_with_enum_debug_derive!(ImageConstraintsInfoFlagBitsFUCHSIA, ImageConstraintsInfoFlagsFUCHSIA, u32, ImageConstraintsInfoFlagBitsFUCHSIA::eCpuReadRarely, ImageConstraintsInfoFlagBitsFUCHSIA::eCpuReadOften, ImageConstraintsInfoFlagBitsFUCHSIA::eCpuWriteRarely, ImageConstraintsInfoFlagBitsFUCHSIA::eCpuWriteOften, ImageConstraintsInfoFlagBitsFUCHSIA::eProtectedOptional);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum GraphicsPipelineLibraryFlagBitsEXT {
    eVertexInputInterfaceBit,
    ePreRasterizationShadersBit,
    eFragmentShaderBit,
    eFragmentOutputInterfaceBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl GraphicsPipelineLibraryFlagBitsEXT {
    pub fn into_raw(self) -> RawGraphicsPipelineLibraryFlagBitsEXT {
        match self {
            Self::eVertexInputInterfaceBit => RawGraphicsPipelineLibraryFlagBitsEXT::eVertexInputInterfaceBit,
            Self::ePreRasterizationShadersBit => RawGraphicsPipelineLibraryFlagBitsEXT::ePreRasterizationShadersBit,
            Self::eFragmentShaderBit => RawGraphicsPipelineLibraryFlagBitsEXT::eFragmentShaderBit,
            Self::eFragmentOutputInterfaceBit => RawGraphicsPipelineLibraryFlagBitsEXT::eFragmentOutputInterfaceBit,
            Self::eUnknownBit(b) => RawGraphicsPipelineLibraryFlagBitsEXT(b),
        }
    }
}

impl core::convert::From<GraphicsPipelineLibraryFlagBitsEXT> for GraphicsPipelineLibraryFlagsEXT {
    fn from(value: GraphicsPipelineLibraryFlagBitsEXT) -> Self {
        match value {
            GraphicsPipelineLibraryFlagBitsEXT::eVertexInputInterfaceBit => GraphicsPipelineLibraryFlagsEXT::eVertexInputInterfaceBit,
            GraphicsPipelineLibraryFlagBitsEXT::ePreRasterizationShadersBit => GraphicsPipelineLibraryFlagsEXT::ePreRasterizationShadersBit,
            GraphicsPipelineLibraryFlagBitsEXT::eFragmentShaderBit => GraphicsPipelineLibraryFlagsEXT::eFragmentShaderBit,
            GraphicsPipelineLibraryFlagBitsEXT::eFragmentOutputInterfaceBit => GraphicsPipelineLibraryFlagsEXT::eFragmentOutputInterfaceBit,
            GraphicsPipelineLibraryFlagBitsEXT::eUnknownBit(b) => GraphicsPipelineLibraryFlagsEXT(b),
        }
    }
}

bit_field_enum_derives!(GraphicsPipelineLibraryFlagBitsEXT, GraphicsPipelineLibraryFlagsEXT, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct GraphicsPipelineLibraryFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl GraphicsPipelineLibraryFlagsEXT {
    pub const eNone: Self = Self(0);
    pub const eVertexInputInterfaceBit: Self = Self(0b1);
    pub const ePreRasterizationShadersBit: Self = Self(0b10);
    pub const eFragmentShaderBit: Self = Self(0b100);
    pub const eFragmentOutputInterfaceBit: Self = Self(0b1000);
}

bit_field_mask_derives!(GraphicsPipelineLibraryFlagsEXT, u32);

bit_field_mask_with_enum_debug_derive!(GraphicsPipelineLibraryFlagBitsEXT, GraphicsPipelineLibraryFlagsEXT, u32, GraphicsPipelineLibraryFlagBitsEXT::eVertexInputInterfaceBit, GraphicsPipelineLibraryFlagBitsEXT::ePreRasterizationShadersBit, GraphicsPipelineLibraryFlagBitsEXT::eFragmentShaderBit, GraphicsPipelineLibraryFlagBitsEXT::eFragmentOutputInterfaceBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ImageCompressionFlagBitsEXT {
    eDefault,
    eFixedRateDefault,
    eFixedRateExplicit,
    eDisabled,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl ImageCompressionFlagBitsEXT {
    pub fn into_raw(self) -> RawImageCompressionFlagBitsEXT {
        match self {
            Self::eDefault => RawImageCompressionFlagBitsEXT::eDefault,
            Self::eFixedRateDefault => RawImageCompressionFlagBitsEXT::eFixedRateDefault,
            Self::eFixedRateExplicit => RawImageCompressionFlagBitsEXT::eFixedRateExplicit,
            Self::eDisabled => RawImageCompressionFlagBitsEXT::eDisabled,
            Self::eUnknownBit(b) => RawImageCompressionFlagBitsEXT(b),
        }
    }
}

impl core::convert::From<ImageCompressionFlagBitsEXT> for ImageCompressionFlagsEXT {
    fn from(value: ImageCompressionFlagBitsEXT) -> Self {
        match value {
            ImageCompressionFlagBitsEXT::eDefault => ImageCompressionFlagsEXT::eDefault,
            ImageCompressionFlagBitsEXT::eFixedRateDefault => ImageCompressionFlagsEXT::eFixedRateDefault,
            ImageCompressionFlagBitsEXT::eFixedRateExplicit => ImageCompressionFlagsEXT::eFixedRateExplicit,
            ImageCompressionFlagBitsEXT::eDisabled => ImageCompressionFlagsEXT::eDisabled,
            ImageCompressionFlagBitsEXT::eUnknownBit(b) => ImageCompressionFlagsEXT(b),
        }
    }
}

bit_field_enum_derives!(ImageCompressionFlagBitsEXT, ImageCompressionFlagsEXT, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ImageCompressionFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ImageCompressionFlagsEXT {
    pub const eDefault: Self = Self(0);
    pub const eFixedRateDefault: Self = Self(0b1);
    pub const eFixedRateExplicit: Self = Self(0b10);
    pub const eDisabled: Self = Self(0b100);
}

bit_field_mask_derives!(ImageCompressionFlagsEXT, u32);

bit_field_mask_with_enum_debug_derive!(ImageCompressionFlagBitsEXT, ImageCompressionFlagsEXT, u32, ImageCompressionFlagBitsEXT::eFixedRateDefault, ImageCompressionFlagBitsEXT::eFixedRateExplicit, ImageCompressionFlagBitsEXT::eDisabled => ImageCompressionFlagBitsEXT::eDefault);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ImageCompressionFixedRateFlagBitsEXT {
    eNone,
    e1bpcBit,
    e2bpcBit,
    e3bpcBit,
    e4bpcBit,
    e5bpcBit,
    e6bpcBit,
    e7bpcBit,
    e8bpcBit,
    e9bpcBit,
    e10bpcBit,
    e11bpcBit,
    e12bpcBit,
    e13bpcBit,
    e14bpcBit,
    e15bpcBit,
    e16bpcBit,
    e17bpcBit,
    e18bpcBit,
    e19bpcBit,
    e20bpcBit,
    e21bpcBit,
    e22bpcBit,
    e23bpcBit,
    e24bpcBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl ImageCompressionFixedRateFlagBitsEXT {
    pub fn into_raw(self) -> RawImageCompressionFixedRateFlagBitsEXT {
        match self {
            Self::eNone => RawImageCompressionFixedRateFlagBitsEXT::eNone,
            Self::e1bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e1bpcBit,
            Self::e2bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e2bpcBit,
            Self::e3bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e3bpcBit,
            Self::e4bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e4bpcBit,
            Self::e5bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e5bpcBit,
            Self::e6bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e6bpcBit,
            Self::e7bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e7bpcBit,
            Self::e8bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e8bpcBit,
            Self::e9bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e9bpcBit,
            Self::e10bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e10bpcBit,
            Self::e11bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e11bpcBit,
            Self::e12bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e12bpcBit,
            Self::e13bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e13bpcBit,
            Self::e14bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e14bpcBit,
            Self::e15bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e15bpcBit,
            Self::e16bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e16bpcBit,
            Self::e17bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e17bpcBit,
            Self::e18bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e18bpcBit,
            Self::e19bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e19bpcBit,
            Self::e20bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e20bpcBit,
            Self::e21bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e21bpcBit,
            Self::e22bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e22bpcBit,
            Self::e23bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e23bpcBit,
            Self::e24bpcBit => RawImageCompressionFixedRateFlagBitsEXT::e24bpcBit,
            Self::eUnknownBit(b) => RawImageCompressionFixedRateFlagBitsEXT(b),
        }
    }
}

impl core::convert::From<ImageCompressionFixedRateFlagBitsEXT> for ImageCompressionFixedRateFlagsEXT {
    fn from(value: ImageCompressionFixedRateFlagBitsEXT) -> Self {
        match value {
            ImageCompressionFixedRateFlagBitsEXT::eNone => ImageCompressionFixedRateFlagsEXT::eNone,
            ImageCompressionFixedRateFlagBitsEXT::e1bpcBit => ImageCompressionFixedRateFlagsEXT::e1bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e2bpcBit => ImageCompressionFixedRateFlagsEXT::e2bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e3bpcBit => ImageCompressionFixedRateFlagsEXT::e3bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e4bpcBit => ImageCompressionFixedRateFlagsEXT::e4bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e5bpcBit => ImageCompressionFixedRateFlagsEXT::e5bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e6bpcBit => ImageCompressionFixedRateFlagsEXT::e6bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e7bpcBit => ImageCompressionFixedRateFlagsEXT::e7bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e8bpcBit => ImageCompressionFixedRateFlagsEXT::e8bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e9bpcBit => ImageCompressionFixedRateFlagsEXT::e9bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e10bpcBit => ImageCompressionFixedRateFlagsEXT::e10bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e11bpcBit => ImageCompressionFixedRateFlagsEXT::e11bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e12bpcBit => ImageCompressionFixedRateFlagsEXT::e12bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e13bpcBit => ImageCompressionFixedRateFlagsEXT::e13bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e14bpcBit => ImageCompressionFixedRateFlagsEXT::e14bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e15bpcBit => ImageCompressionFixedRateFlagsEXT::e15bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e16bpcBit => ImageCompressionFixedRateFlagsEXT::e16bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e17bpcBit => ImageCompressionFixedRateFlagsEXT::e17bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e18bpcBit => ImageCompressionFixedRateFlagsEXT::e18bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e19bpcBit => ImageCompressionFixedRateFlagsEXT::e19bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e20bpcBit => ImageCompressionFixedRateFlagsEXT::e20bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e21bpcBit => ImageCompressionFixedRateFlagsEXT::e21bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e22bpcBit => ImageCompressionFixedRateFlagsEXT::e22bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e23bpcBit => ImageCompressionFixedRateFlagsEXT::e23bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::e24bpcBit => ImageCompressionFixedRateFlagsEXT::e24bpcBit,
            ImageCompressionFixedRateFlagBitsEXT::eUnknownBit(b) => ImageCompressionFixedRateFlagsEXT(b),
        }
    }
}

bit_field_enum_derives!(ImageCompressionFixedRateFlagBitsEXT, ImageCompressionFixedRateFlagsEXT, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ImageCompressionFixedRateFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ImageCompressionFixedRateFlagsEXT {
    pub const eNone: Self = Self(0);
    pub const e1bpcBit: Self = Self(0b1);
    pub const e2bpcBit: Self = Self(0b10);
    pub const e3bpcBit: Self = Self(0b100);
    pub const e4bpcBit: Self = Self(0b1000);
    pub const e5bpcBit: Self = Self(0b10000);
    pub const e6bpcBit: Self = Self(0b100000);
    pub const e7bpcBit: Self = Self(0b1000000);
    pub const e8bpcBit: Self = Self(0b10000000);
    pub const e9bpcBit: Self = Self(0b100000000);
    pub const e10bpcBit: Self = Self(0b1000000000);
    pub const e11bpcBit: Self = Self(0b10000000000);
    pub const e12bpcBit: Self = Self(0b100000000000);
    pub const e13bpcBit: Self = Self(0b1000000000000);
    pub const e14bpcBit: Self = Self(0b10000000000000);
    pub const e15bpcBit: Self = Self(0b100000000000000);
    pub const e16bpcBit: Self = Self(0b1000000000000000);
    pub const e17bpcBit: Self = Self(0b10000000000000000);
    pub const e18bpcBit: Self = Self(0b100000000000000000);
    pub const e19bpcBit: Self = Self(0b1000000000000000000);
    pub const e20bpcBit: Self = Self(0b10000000000000000000);
    pub const e21bpcBit: Self = Self(0b100000000000000000000);
    pub const e22bpcBit: Self = Self(0b1000000000000000000000);
    pub const e23bpcBit: Self = Self(0b10000000000000000000000);
    pub const e24bpcBit: Self = Self(0b100000000000000000000000);
}

bit_field_mask_derives!(ImageCompressionFixedRateFlagsEXT, u32);

bit_field_mask_with_enum_debug_derive!(ImageCompressionFixedRateFlagBitsEXT, ImageCompressionFixedRateFlagsEXT, u32, ImageCompressionFixedRateFlagBitsEXT::e1bpcBit, ImageCompressionFixedRateFlagBitsEXT::e2bpcBit, ImageCompressionFixedRateFlagBitsEXT::e3bpcBit, ImageCompressionFixedRateFlagBitsEXT::e4bpcBit, ImageCompressionFixedRateFlagBitsEXT::e5bpcBit, ImageCompressionFixedRateFlagBitsEXT::e6bpcBit, ImageCompressionFixedRateFlagBitsEXT::e7bpcBit, ImageCompressionFixedRateFlagBitsEXT::e8bpcBit, ImageCompressionFixedRateFlagBitsEXT::e9bpcBit, ImageCompressionFixedRateFlagBitsEXT::e10bpcBit, ImageCompressionFixedRateFlagBitsEXT::e11bpcBit, ImageCompressionFixedRateFlagBitsEXT::e12bpcBit, ImageCompressionFixedRateFlagBitsEXT::e13bpcBit, ImageCompressionFixedRateFlagBitsEXT::e14bpcBit, ImageCompressionFixedRateFlagBitsEXT::e15bpcBit, ImageCompressionFixedRateFlagBitsEXT::e16bpcBit, ImageCompressionFixedRateFlagBitsEXT::e17bpcBit, ImageCompressionFixedRateFlagBitsEXT::e18bpcBit, ImageCompressionFixedRateFlagBitsEXT::e19bpcBit, ImageCompressionFixedRateFlagBitsEXT::e20bpcBit, ImageCompressionFixedRateFlagBitsEXT::e21bpcBit, ImageCompressionFixedRateFlagBitsEXT::e22bpcBit, ImageCompressionFixedRateFlagBitsEXT::e23bpcBit, ImageCompressionFixedRateFlagBitsEXT::e24bpcBit => ImageCompressionFixedRateFlagBitsEXT::eNone);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum ExportMetalObjectTypeFlagBitsEXT {
    eMetalDeviceBit,
    eMetalCommandQueueBit,
    eMetalBufferBit,
    eMetalTextureBit,
    eMetalIosurfaceBit,
    eMetalSharedEventBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl ExportMetalObjectTypeFlagBitsEXT {
    pub fn into_raw(self) -> RawExportMetalObjectTypeFlagBitsEXT {
        match self {
            Self::eMetalDeviceBit => RawExportMetalObjectTypeFlagBitsEXT::eMetalDeviceBit,
            Self::eMetalCommandQueueBit => RawExportMetalObjectTypeFlagBitsEXT::eMetalCommandQueueBit,
            Self::eMetalBufferBit => RawExportMetalObjectTypeFlagBitsEXT::eMetalBufferBit,
            Self::eMetalTextureBit => RawExportMetalObjectTypeFlagBitsEXT::eMetalTextureBit,
            Self::eMetalIosurfaceBit => RawExportMetalObjectTypeFlagBitsEXT::eMetalIosurfaceBit,
            Self::eMetalSharedEventBit => RawExportMetalObjectTypeFlagBitsEXT::eMetalSharedEventBit,
            Self::eUnknownBit(b) => RawExportMetalObjectTypeFlagBitsEXT(b),
        }
    }
}

impl core::convert::From<ExportMetalObjectTypeFlagBitsEXT> for ExportMetalObjectTypeFlagsEXT {
    fn from(value: ExportMetalObjectTypeFlagBitsEXT) -> Self {
        match value {
            ExportMetalObjectTypeFlagBitsEXT::eMetalDeviceBit => ExportMetalObjectTypeFlagsEXT::eMetalDeviceBit,
            ExportMetalObjectTypeFlagBitsEXT::eMetalCommandQueueBit => ExportMetalObjectTypeFlagsEXT::eMetalCommandQueueBit,
            ExportMetalObjectTypeFlagBitsEXT::eMetalBufferBit => ExportMetalObjectTypeFlagsEXT::eMetalBufferBit,
            ExportMetalObjectTypeFlagBitsEXT::eMetalTextureBit => ExportMetalObjectTypeFlagsEXT::eMetalTextureBit,
            ExportMetalObjectTypeFlagBitsEXT::eMetalIosurfaceBit => ExportMetalObjectTypeFlagsEXT::eMetalIosurfaceBit,
            ExportMetalObjectTypeFlagBitsEXT::eMetalSharedEventBit => ExportMetalObjectTypeFlagsEXT::eMetalSharedEventBit,
            ExportMetalObjectTypeFlagBitsEXT::eUnknownBit(b) => ExportMetalObjectTypeFlagsEXT(b),
        }
    }
}

bit_field_enum_derives!(ExportMetalObjectTypeFlagBitsEXT, ExportMetalObjectTypeFlagsEXT, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct ExportMetalObjectTypeFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl ExportMetalObjectTypeFlagsEXT {
    pub const eNone: Self = Self(0);
    pub const eMetalDeviceBit: Self = Self(0b1);
    pub const eMetalCommandQueueBit: Self = Self(0b10);
    pub const eMetalBufferBit: Self = Self(0b100);
    pub const eMetalTextureBit: Self = Self(0b1000);
    pub const eMetalIosurfaceBit: Self = Self(0b10000);
    pub const eMetalSharedEventBit: Self = Self(0b100000);
}

bit_field_mask_derives!(ExportMetalObjectTypeFlagsEXT, u32);

bit_field_mask_with_enum_debug_derive!(ExportMetalObjectTypeFlagBitsEXT, ExportMetalObjectTypeFlagsEXT, u32, ExportMetalObjectTypeFlagBitsEXT::eMetalDeviceBit, ExportMetalObjectTypeFlagBitsEXT::eMetalCommandQueueBit, ExportMetalObjectTypeFlagBitsEXT::eMetalBufferBit, ExportMetalObjectTypeFlagBitsEXT::eMetalTextureBit, ExportMetalObjectTypeFlagBitsEXT::eMetalIosurfaceBit, ExportMetalObjectTypeFlagBitsEXT::eMetalSharedEventBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum DeviceAddressBindingFlagBitsEXT {
    eInternalObjectBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl DeviceAddressBindingFlagBitsEXT {
    pub fn into_raw(self) -> RawDeviceAddressBindingFlagBitsEXT {
        match self {
            Self::eInternalObjectBit => RawDeviceAddressBindingFlagBitsEXT::eInternalObjectBit,
            Self::eUnknownBit(b) => RawDeviceAddressBindingFlagBitsEXT(b),
        }
    }
}

impl core::convert::From<DeviceAddressBindingFlagBitsEXT> for DeviceAddressBindingFlagsEXT {
    fn from(value: DeviceAddressBindingFlagBitsEXT) -> Self {
        match value {
            DeviceAddressBindingFlagBitsEXT::eInternalObjectBit => DeviceAddressBindingFlagsEXT::eInternalObjectBit,
            DeviceAddressBindingFlagBitsEXT::eUnknownBit(b) => DeviceAddressBindingFlagsEXT(b),
        }
    }
}

bit_field_enum_derives!(DeviceAddressBindingFlagBitsEXT, DeviceAddressBindingFlagsEXT, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct DeviceAddressBindingFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl DeviceAddressBindingFlagsEXT {
    pub const eNone: Self = Self(0);
    pub const eInternalObjectBit: Self = Self(0b1);
}

bit_field_mask_derives!(DeviceAddressBindingFlagsEXT, u32);

bit_field_mask_with_enum_debug_derive!(DeviceAddressBindingFlagBitsEXT, DeviceAddressBindingFlagsEXT, u32, DeviceAddressBindingFlagBitsEXT::eInternalObjectBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum OpticalFlowGridSizeFlagBitsNV {
    eUnknown,
    e1x1Bit,
    e2x2Bit,
    e4x4Bit,
    e8x8Bit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl OpticalFlowGridSizeFlagBitsNV {
    pub fn into_raw(self) -> RawOpticalFlowGridSizeFlagBitsNV {
        match self {
            Self::eUnknown => RawOpticalFlowGridSizeFlagBitsNV::eUnknown,
            Self::e1x1Bit => RawOpticalFlowGridSizeFlagBitsNV::e1x1Bit,
            Self::e2x2Bit => RawOpticalFlowGridSizeFlagBitsNV::e2x2Bit,
            Self::e4x4Bit => RawOpticalFlowGridSizeFlagBitsNV::e4x4Bit,
            Self::e8x8Bit => RawOpticalFlowGridSizeFlagBitsNV::e8x8Bit,
            Self::eUnknownBit(b) => RawOpticalFlowGridSizeFlagBitsNV(b),
        }
    }
}

impl core::convert::From<OpticalFlowGridSizeFlagBitsNV> for OpticalFlowGridSizeFlagsNV {
    fn from(value: OpticalFlowGridSizeFlagBitsNV) -> Self {
        match value {
            OpticalFlowGridSizeFlagBitsNV::eUnknown => OpticalFlowGridSizeFlagsNV::eUnknown,
            OpticalFlowGridSizeFlagBitsNV::e1x1Bit => OpticalFlowGridSizeFlagsNV::e1x1Bit,
            OpticalFlowGridSizeFlagBitsNV::e2x2Bit => OpticalFlowGridSizeFlagsNV::e2x2Bit,
            OpticalFlowGridSizeFlagBitsNV::e4x4Bit => OpticalFlowGridSizeFlagsNV::e4x4Bit,
            OpticalFlowGridSizeFlagBitsNV::e8x8Bit => OpticalFlowGridSizeFlagsNV::e8x8Bit,
            OpticalFlowGridSizeFlagBitsNV::eUnknownBit(b) => OpticalFlowGridSizeFlagsNV(b),
        }
    }
}

bit_field_enum_derives!(OpticalFlowGridSizeFlagBitsNV, OpticalFlowGridSizeFlagsNV, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct OpticalFlowGridSizeFlagsNV(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl OpticalFlowGridSizeFlagsNV {
    pub const eUnknown: Self = Self(0);
    pub const e1x1Bit: Self = Self(0b1);
    pub const e2x2Bit: Self = Self(0b10);
    pub const e4x4Bit: Self = Self(0b100);
    pub const e8x8Bit: Self = Self(0b1000);
}

bit_field_mask_derives!(OpticalFlowGridSizeFlagsNV, u32);

bit_field_mask_with_enum_debug_derive!(OpticalFlowGridSizeFlagBitsNV, OpticalFlowGridSizeFlagsNV, u32, OpticalFlowGridSizeFlagBitsNV::e1x1Bit, OpticalFlowGridSizeFlagBitsNV::e2x2Bit, OpticalFlowGridSizeFlagBitsNV::e4x4Bit, OpticalFlowGridSizeFlagBitsNV::e8x8Bit => OpticalFlowGridSizeFlagBitsNV::eUnknown);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum OpticalFlowUsageFlagBitsNV {
    eUnknown,
    eInputBit,
    eOutputBit,
    eHintBit,
    eCostBit,
    eGlobalFlowBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl OpticalFlowUsageFlagBitsNV {
    pub fn into_raw(self) -> RawOpticalFlowUsageFlagBitsNV {
        match self {
            Self::eUnknown => RawOpticalFlowUsageFlagBitsNV::eUnknown,
            Self::eInputBit => RawOpticalFlowUsageFlagBitsNV::eInputBit,
            Self::eOutputBit => RawOpticalFlowUsageFlagBitsNV::eOutputBit,
            Self::eHintBit => RawOpticalFlowUsageFlagBitsNV::eHintBit,
            Self::eCostBit => RawOpticalFlowUsageFlagBitsNV::eCostBit,
            Self::eGlobalFlowBit => RawOpticalFlowUsageFlagBitsNV::eGlobalFlowBit,
            Self::eUnknownBit(b) => RawOpticalFlowUsageFlagBitsNV(b),
        }
    }
}

impl core::convert::From<OpticalFlowUsageFlagBitsNV> for OpticalFlowUsageFlagsNV {
    fn from(value: OpticalFlowUsageFlagBitsNV) -> Self {
        match value {
            OpticalFlowUsageFlagBitsNV::eUnknown => OpticalFlowUsageFlagsNV::eUnknown,
            OpticalFlowUsageFlagBitsNV::eInputBit => OpticalFlowUsageFlagsNV::eInputBit,
            OpticalFlowUsageFlagBitsNV::eOutputBit => OpticalFlowUsageFlagsNV::eOutputBit,
            OpticalFlowUsageFlagBitsNV::eHintBit => OpticalFlowUsageFlagsNV::eHintBit,
            OpticalFlowUsageFlagBitsNV::eCostBit => OpticalFlowUsageFlagsNV::eCostBit,
            OpticalFlowUsageFlagBitsNV::eGlobalFlowBit => OpticalFlowUsageFlagsNV::eGlobalFlowBit,
            OpticalFlowUsageFlagBitsNV::eUnknownBit(b) => OpticalFlowUsageFlagsNV(b),
        }
    }
}

bit_field_enum_derives!(OpticalFlowUsageFlagBitsNV, OpticalFlowUsageFlagsNV, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct OpticalFlowUsageFlagsNV(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl OpticalFlowUsageFlagsNV {
    pub const eUnknown: Self = Self(0);
    pub const eInputBit: Self = Self(0b1);
    pub const eOutputBit: Self = Self(0b10);
    pub const eHintBit: Self = Self(0b100);
    pub const eCostBit: Self = Self(0b1000);
    pub const eGlobalFlowBit: Self = Self(0b10000);
}

bit_field_mask_derives!(OpticalFlowUsageFlagsNV, u32);

bit_field_mask_with_enum_debug_derive!(OpticalFlowUsageFlagBitsNV, OpticalFlowUsageFlagsNV, u32, OpticalFlowUsageFlagBitsNV::eInputBit, OpticalFlowUsageFlagBitsNV::eOutputBit, OpticalFlowUsageFlagBitsNV::eHintBit, OpticalFlowUsageFlagBitsNV::eCostBit, OpticalFlowUsageFlagBitsNV::eGlobalFlowBit => OpticalFlowUsageFlagBitsNV::eUnknown);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum OpticalFlowSessionCreateFlagBitsNV {
    eEnableHintBit,
    eEnableCostBit,
    eEnableGlobalFlowBit,
    eAllowRegionsBit,
    eBothDirectionsBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl OpticalFlowSessionCreateFlagBitsNV {
    pub fn into_raw(self) -> RawOpticalFlowSessionCreateFlagBitsNV {
        match self {
            Self::eEnableHintBit => RawOpticalFlowSessionCreateFlagBitsNV::eEnableHintBit,
            Self::eEnableCostBit => RawOpticalFlowSessionCreateFlagBitsNV::eEnableCostBit,
            Self::eEnableGlobalFlowBit => RawOpticalFlowSessionCreateFlagBitsNV::eEnableGlobalFlowBit,
            Self::eAllowRegionsBit => RawOpticalFlowSessionCreateFlagBitsNV::eAllowRegionsBit,
            Self::eBothDirectionsBit => RawOpticalFlowSessionCreateFlagBitsNV::eBothDirectionsBit,
            Self::eUnknownBit(b) => RawOpticalFlowSessionCreateFlagBitsNV(b),
        }
    }
}

impl core::convert::From<OpticalFlowSessionCreateFlagBitsNV> for OpticalFlowSessionCreateFlagsNV {
    fn from(value: OpticalFlowSessionCreateFlagBitsNV) -> Self {
        match value {
            OpticalFlowSessionCreateFlagBitsNV::eEnableHintBit => OpticalFlowSessionCreateFlagsNV::eEnableHintBit,
            OpticalFlowSessionCreateFlagBitsNV::eEnableCostBit => OpticalFlowSessionCreateFlagsNV::eEnableCostBit,
            OpticalFlowSessionCreateFlagBitsNV::eEnableGlobalFlowBit => OpticalFlowSessionCreateFlagsNV::eEnableGlobalFlowBit,
            OpticalFlowSessionCreateFlagBitsNV::eAllowRegionsBit => OpticalFlowSessionCreateFlagsNV::eAllowRegionsBit,
            OpticalFlowSessionCreateFlagBitsNV::eBothDirectionsBit => OpticalFlowSessionCreateFlagsNV::eBothDirectionsBit,
            OpticalFlowSessionCreateFlagBitsNV::eUnknownBit(b) => OpticalFlowSessionCreateFlagsNV(b),
        }
    }
}

bit_field_enum_derives!(OpticalFlowSessionCreateFlagBitsNV, OpticalFlowSessionCreateFlagsNV, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct OpticalFlowSessionCreateFlagsNV(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl OpticalFlowSessionCreateFlagsNV {
    pub const eNone: Self = Self(0);
    pub const eEnableHintBit: Self = Self(0b1);
    pub const eEnableCostBit: Self = Self(0b10);
    pub const eEnableGlobalFlowBit: Self = Self(0b100);
    pub const eAllowRegionsBit: Self = Self(0b1000);
    pub const eBothDirectionsBit: Self = Self(0b10000);
}

bit_field_mask_derives!(OpticalFlowSessionCreateFlagsNV, u32);

bit_field_mask_with_enum_debug_derive!(OpticalFlowSessionCreateFlagBitsNV, OpticalFlowSessionCreateFlagsNV, u32, OpticalFlowSessionCreateFlagBitsNV::eEnableHintBit, OpticalFlowSessionCreateFlagBitsNV::eEnableCostBit, OpticalFlowSessionCreateFlagBitsNV::eEnableGlobalFlowBit, OpticalFlowSessionCreateFlagBitsNV::eAllowRegionsBit, OpticalFlowSessionCreateFlagBitsNV::eBothDirectionsBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum OpticalFlowExecuteFlagBitsNV {
    eDisableTemporalHintsBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl OpticalFlowExecuteFlagBitsNV {
    pub fn into_raw(self) -> RawOpticalFlowExecuteFlagBitsNV {
        match self {
            Self::eDisableTemporalHintsBit => RawOpticalFlowExecuteFlagBitsNV::eDisableTemporalHintsBit,
            Self::eUnknownBit(b) => RawOpticalFlowExecuteFlagBitsNV(b),
        }
    }
}

impl core::convert::From<OpticalFlowExecuteFlagBitsNV> for OpticalFlowExecuteFlagsNV {
    fn from(value: OpticalFlowExecuteFlagBitsNV) -> Self {
        match value {
            OpticalFlowExecuteFlagBitsNV::eDisableTemporalHintsBit => OpticalFlowExecuteFlagsNV::eDisableTemporalHintsBit,
            OpticalFlowExecuteFlagBitsNV::eUnknownBit(b) => OpticalFlowExecuteFlagsNV(b),
        }
    }
}

bit_field_enum_derives!(OpticalFlowExecuteFlagBitsNV, OpticalFlowExecuteFlagsNV, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct OpticalFlowExecuteFlagsNV(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl OpticalFlowExecuteFlagsNV {
    pub const eNone: Self = Self(0);
    pub const eDisableTemporalHintsBit: Self = Self(0b1);
}

bit_field_mask_derives!(OpticalFlowExecuteFlagsNV, u32);

bit_field_mask_with_enum_debug_derive!(OpticalFlowExecuteFlagBitsNV, OpticalFlowExecuteFlagsNV, u32, OpticalFlowExecuteFlagBitsNV::eDisableTemporalHintsBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PresentScalingFlagBitsEXT {
    eOneToOneBit,
    eAspectRatioStretchBit,
    eStretchBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl PresentScalingFlagBitsEXT {
    pub fn into_raw(self) -> RawPresentScalingFlagBitsEXT {
        match self {
            Self::eOneToOneBit => RawPresentScalingFlagBitsEXT::eOneToOneBit,
            Self::eAspectRatioStretchBit => RawPresentScalingFlagBitsEXT::eAspectRatioStretchBit,
            Self::eStretchBit => RawPresentScalingFlagBitsEXT::eStretchBit,
            Self::eUnknownBit(b) => RawPresentScalingFlagBitsEXT(b),
        }
    }
}

impl core::convert::From<PresentScalingFlagBitsEXT> for PresentScalingFlagsEXT {
    fn from(value: PresentScalingFlagBitsEXT) -> Self {
        match value {
            PresentScalingFlagBitsEXT::eOneToOneBit => PresentScalingFlagsEXT::eOneToOneBit,
            PresentScalingFlagBitsEXT::eAspectRatioStretchBit => PresentScalingFlagsEXT::eAspectRatioStretchBit,
            PresentScalingFlagBitsEXT::eStretchBit => PresentScalingFlagsEXT::eStretchBit,
            PresentScalingFlagBitsEXT::eUnknownBit(b) => PresentScalingFlagsEXT(b),
        }
    }
}

bit_field_enum_derives!(PresentScalingFlagBitsEXT, PresentScalingFlagsEXT, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PresentScalingFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PresentScalingFlagsEXT {
    pub const eNone: Self = Self(0);
    pub const eOneToOneBit: Self = Self(0b1);
    pub const eAspectRatioStretchBit: Self = Self(0b10);
    pub const eStretchBit: Self = Self(0b100);
}

bit_field_mask_derives!(PresentScalingFlagsEXT, u32);

bit_field_mask_with_enum_debug_derive!(PresentScalingFlagBitsEXT, PresentScalingFlagsEXT, u32, PresentScalingFlagBitsEXT::eOneToOneBit, PresentScalingFlagBitsEXT::eAspectRatioStretchBit, PresentScalingFlagBitsEXT::eStretchBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum PresentGravityFlagBitsEXT {
    eMinBit,
    eMaxBit,
    eCenteredBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl PresentGravityFlagBitsEXT {
    pub fn into_raw(self) -> RawPresentGravityFlagBitsEXT {
        match self {
            Self::eMinBit => RawPresentGravityFlagBitsEXT::eMinBit,
            Self::eMaxBit => RawPresentGravityFlagBitsEXT::eMaxBit,
            Self::eCenteredBit => RawPresentGravityFlagBitsEXT::eCenteredBit,
            Self::eUnknownBit(b) => RawPresentGravityFlagBitsEXT(b),
        }
    }
}

impl core::convert::From<PresentGravityFlagBitsEXT> for PresentGravityFlagsEXT {
    fn from(value: PresentGravityFlagBitsEXT) -> Self {
        match value {
            PresentGravityFlagBitsEXT::eMinBit => PresentGravityFlagsEXT::eMinBit,
            PresentGravityFlagBitsEXT::eMaxBit => PresentGravityFlagsEXT::eMaxBit,
            PresentGravityFlagBitsEXT::eCenteredBit => PresentGravityFlagsEXT::eCenteredBit,
            PresentGravityFlagBitsEXT::eUnknownBit(b) => PresentGravityFlagsEXT(b),
        }
    }
}

bit_field_enum_derives!(PresentGravityFlagBitsEXT, PresentGravityFlagsEXT, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct PresentGravityFlagsEXT(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl PresentGravityFlagsEXT {
    pub const eNone: Self = Self(0);
    pub const eMinBit: Self = Self(0b1);
    pub const eMaxBit: Self = Self(0b10);
    pub const eCenteredBit: Self = Self(0b100);
}

bit_field_mask_derives!(PresentGravityFlagsEXT, u32);

bit_field_mask_with_enum_debug_derive!(PresentGravityFlagBitsEXT, PresentGravityFlagsEXT, u32, PresentGravityFlagBitsEXT::eMinBit, PresentGravityFlagBitsEXT::eMaxBit, PresentGravityFlagBitsEXT::eCenteredBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum VideoCodecOperationFlagBitsKHR {
    eNone,
    eDecodeH264Bit,
    eDecodeH265Bit,
    eEncodeH264BitExt,
    eEncodeH265BitExt,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl VideoCodecOperationFlagBitsKHR {
    pub fn into_raw(self) -> RawVideoCodecOperationFlagBitsKHR {
        match self {
            Self::eNone => RawVideoCodecOperationFlagBitsKHR::eNone,
            Self::eDecodeH264Bit => RawVideoCodecOperationFlagBitsKHR::eDecodeH264Bit,
            Self::eDecodeH265Bit => RawVideoCodecOperationFlagBitsKHR::eDecodeH265Bit,
            Self::eEncodeH264BitExt => RawVideoCodecOperationFlagBitsKHR::eEncodeH264BitExt,
            Self::eEncodeH265BitExt => RawVideoCodecOperationFlagBitsKHR::eEncodeH265BitExt,
            Self::eUnknownBit(b) => RawVideoCodecOperationFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<VideoCodecOperationFlagBitsKHR> for VideoCodecOperationFlagsKHR {
    fn from(value: VideoCodecOperationFlagBitsKHR) -> Self {
        match value {
            VideoCodecOperationFlagBitsKHR::eNone => VideoCodecOperationFlagsKHR::eNone,
            VideoCodecOperationFlagBitsKHR::eDecodeH264Bit => VideoCodecOperationFlagsKHR::eDecodeH264Bit,
            VideoCodecOperationFlagBitsKHR::eDecodeH265Bit => VideoCodecOperationFlagsKHR::eDecodeH265Bit,
            VideoCodecOperationFlagBitsKHR::eEncodeH264BitExt => VideoCodecOperationFlagsKHR::eEncodeH264BitExt,
            VideoCodecOperationFlagBitsKHR::eEncodeH265BitExt => VideoCodecOperationFlagsKHR::eEncodeH265BitExt,
            VideoCodecOperationFlagBitsKHR::eUnknownBit(b) => VideoCodecOperationFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(VideoCodecOperationFlagBitsKHR, VideoCodecOperationFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct VideoCodecOperationFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl VideoCodecOperationFlagsKHR {
    pub const eNone: Self = Self(0);
    pub const eDecodeH264Bit: Self = Self(0b1);
    pub const eDecodeH265Bit: Self = Self(0b10);
    pub const eEncodeH264BitExt: Self = Self(0b10000000000000000);
    pub const eEncodeH265BitExt: Self = Self(0b100000000000000000);
}

bit_field_mask_derives!(VideoCodecOperationFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(VideoCodecOperationFlagBitsKHR, VideoCodecOperationFlagsKHR, u32, VideoCodecOperationFlagBitsKHR::eDecodeH264Bit, VideoCodecOperationFlagBitsKHR::eDecodeH265Bit, VideoCodecOperationFlagBitsKHR::eEncodeH264BitExt, VideoCodecOperationFlagBitsKHR::eEncodeH265BitExt => VideoCodecOperationFlagBitsKHR::eNone);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum VideoCapabilityFlagBitsKHR {
    eProtectedContentBit,
    eSeparateReferenceImagesBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl VideoCapabilityFlagBitsKHR {
    pub fn into_raw(self) -> RawVideoCapabilityFlagBitsKHR {
        match self {
            Self::eProtectedContentBit => RawVideoCapabilityFlagBitsKHR::eProtectedContentBit,
            Self::eSeparateReferenceImagesBit => RawVideoCapabilityFlagBitsKHR::eSeparateReferenceImagesBit,
            Self::eUnknownBit(b) => RawVideoCapabilityFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<VideoCapabilityFlagBitsKHR> for VideoCapabilityFlagsKHR {
    fn from(value: VideoCapabilityFlagBitsKHR) -> Self {
        match value {
            VideoCapabilityFlagBitsKHR::eProtectedContentBit => VideoCapabilityFlagsKHR::eProtectedContentBit,
            VideoCapabilityFlagBitsKHR::eSeparateReferenceImagesBit => VideoCapabilityFlagsKHR::eSeparateReferenceImagesBit,
            VideoCapabilityFlagBitsKHR::eUnknownBit(b) => VideoCapabilityFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(VideoCapabilityFlagBitsKHR, VideoCapabilityFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct VideoCapabilityFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl VideoCapabilityFlagsKHR {
    pub const eNone: Self = Self(0);
    pub const eProtectedContentBit: Self = Self(0b1);
    pub const eSeparateReferenceImagesBit: Self = Self(0b10);
}

bit_field_mask_derives!(VideoCapabilityFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(VideoCapabilityFlagBitsKHR, VideoCapabilityFlagsKHR, u32, VideoCapabilityFlagBitsKHR::eProtectedContentBit, VideoCapabilityFlagBitsKHR::eSeparateReferenceImagesBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum VideoSessionCreateFlagBitsKHR {
    eProtectedContentBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl VideoSessionCreateFlagBitsKHR {
    pub fn into_raw(self) -> RawVideoSessionCreateFlagBitsKHR {
        match self {
            Self::eProtectedContentBit => RawVideoSessionCreateFlagBitsKHR::eProtectedContentBit,
            Self::eUnknownBit(b) => RawVideoSessionCreateFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<VideoSessionCreateFlagBitsKHR> for VideoSessionCreateFlagsKHR {
    fn from(value: VideoSessionCreateFlagBitsKHR) -> Self {
        match value {
            VideoSessionCreateFlagBitsKHR::eProtectedContentBit => VideoSessionCreateFlagsKHR::eProtectedContentBit,
            VideoSessionCreateFlagBitsKHR::eUnknownBit(b) => VideoSessionCreateFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(VideoSessionCreateFlagBitsKHR, VideoSessionCreateFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct VideoSessionCreateFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl VideoSessionCreateFlagsKHR {
    pub const eNone: Self = Self(0);
    pub const eProtectedContentBit: Self = Self(0b1);
}

bit_field_mask_derives!(VideoSessionCreateFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(VideoSessionCreateFlagBitsKHR, VideoSessionCreateFlagsKHR, u32, VideoSessionCreateFlagBitsKHR::eProtectedContentBit);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct VideoSessionParametersCreateFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl VideoSessionParametersCreateFlagsKHR {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(VideoSessionParametersCreateFlagsKHR, u32);

fieldless_debug_derive!(VideoSessionParametersCreateFlagsKHR);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct VideoBeginCodingFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl VideoBeginCodingFlagsKHR {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(VideoBeginCodingFlagsKHR, u32);

fieldless_debug_derive!(VideoBeginCodingFlagsKHR);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct VideoEndCodingFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl VideoEndCodingFlagsKHR {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(VideoEndCodingFlagsKHR, u32);

fieldless_debug_derive!(VideoEndCodingFlagsKHR);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum VideoCodingControlFlagBitsKHR {
    eResetBit,
    eEncodeRateControlBit,
    eEncodeRateControlLayerBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl VideoCodingControlFlagBitsKHR {
    pub fn into_raw(self) -> RawVideoCodingControlFlagBitsKHR {
        match self {
            Self::eResetBit => RawVideoCodingControlFlagBitsKHR::eResetBit,
            Self::eEncodeRateControlBit => RawVideoCodingControlFlagBitsKHR::eEncodeRateControlBit,
            Self::eEncodeRateControlLayerBit => RawVideoCodingControlFlagBitsKHR::eEncodeRateControlLayerBit,
            Self::eUnknownBit(b) => RawVideoCodingControlFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<VideoCodingControlFlagBitsKHR> for VideoCodingControlFlagsKHR {
    fn from(value: VideoCodingControlFlagBitsKHR) -> Self {
        match value {
            VideoCodingControlFlagBitsKHR::eResetBit => VideoCodingControlFlagsKHR::eResetBit,
            VideoCodingControlFlagBitsKHR::eEncodeRateControlBit => VideoCodingControlFlagsKHR::eEncodeRateControlBit,
            VideoCodingControlFlagBitsKHR::eEncodeRateControlLayerBit => VideoCodingControlFlagsKHR::eEncodeRateControlLayerBit,
            VideoCodingControlFlagBitsKHR::eUnknownBit(b) => VideoCodingControlFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(VideoCodingControlFlagBitsKHR, VideoCodingControlFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct VideoCodingControlFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl VideoCodingControlFlagsKHR {
    pub const eNone: Self = Self(0);
    pub const eResetBit: Self = Self(0b1);
    pub const eEncodeRateControlBit: Self = Self(0b10);
    pub const eEncodeRateControlLayerBit: Self = Self(0b100);
}

bit_field_mask_derives!(VideoCodingControlFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(VideoCodingControlFlagBitsKHR, VideoCodingControlFlagsKHR, u32, VideoCodingControlFlagBitsKHR::eResetBit, VideoCodingControlFlagBitsKHR::eEncodeRateControlBit, VideoCodingControlFlagBitsKHR::eEncodeRateControlLayerBit);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum VideoDecodeUsageFlagBitsKHR {
    eDefault,
    eTranscodingBit,
    eOfflineBit,
    eStreamingBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl VideoDecodeUsageFlagBitsKHR {
    pub fn into_raw(self) -> RawVideoDecodeUsageFlagBitsKHR {
        match self {
            Self::eDefault => RawVideoDecodeUsageFlagBitsKHR::eDefault,
            Self::eTranscodingBit => RawVideoDecodeUsageFlagBitsKHR::eTranscodingBit,
            Self::eOfflineBit => RawVideoDecodeUsageFlagBitsKHR::eOfflineBit,
            Self::eStreamingBit => RawVideoDecodeUsageFlagBitsKHR::eStreamingBit,
            Self::eUnknownBit(b) => RawVideoDecodeUsageFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<VideoDecodeUsageFlagBitsKHR> for VideoDecodeUsageFlagsKHR {
    fn from(value: VideoDecodeUsageFlagBitsKHR) -> Self {
        match value {
            VideoDecodeUsageFlagBitsKHR::eDefault => VideoDecodeUsageFlagsKHR::eDefault,
            VideoDecodeUsageFlagBitsKHR::eTranscodingBit => VideoDecodeUsageFlagsKHR::eTranscodingBit,
            VideoDecodeUsageFlagBitsKHR::eOfflineBit => VideoDecodeUsageFlagsKHR::eOfflineBit,
            VideoDecodeUsageFlagBitsKHR::eStreamingBit => VideoDecodeUsageFlagsKHR::eStreamingBit,
            VideoDecodeUsageFlagBitsKHR::eUnknownBit(b) => VideoDecodeUsageFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(VideoDecodeUsageFlagBitsKHR, VideoDecodeUsageFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct VideoDecodeUsageFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl VideoDecodeUsageFlagsKHR {
    pub const eDefault: Self = Self(0);
    pub const eTranscodingBit: Self = Self(0b1);
    pub const eOfflineBit: Self = Self(0b10);
    pub const eStreamingBit: Self = Self(0b100);
}

bit_field_mask_derives!(VideoDecodeUsageFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(VideoDecodeUsageFlagBitsKHR, VideoDecodeUsageFlagsKHR, u32, VideoDecodeUsageFlagBitsKHR::eTranscodingBit, VideoDecodeUsageFlagBitsKHR::eOfflineBit, VideoDecodeUsageFlagBitsKHR::eStreamingBit => VideoDecodeUsageFlagBitsKHR::eDefault);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum VideoDecodeCapabilityFlagBitsKHR {
    eDpbAndOutputCoincideBit,
    eDpbAndOutputDistinctBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl VideoDecodeCapabilityFlagBitsKHR {
    pub fn into_raw(self) -> RawVideoDecodeCapabilityFlagBitsKHR {
        match self {
            Self::eDpbAndOutputCoincideBit => RawVideoDecodeCapabilityFlagBitsKHR::eDpbAndOutputCoincideBit,
            Self::eDpbAndOutputDistinctBit => RawVideoDecodeCapabilityFlagBitsKHR::eDpbAndOutputDistinctBit,
            Self::eUnknownBit(b) => RawVideoDecodeCapabilityFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<VideoDecodeCapabilityFlagBitsKHR> for VideoDecodeCapabilityFlagsKHR {
    fn from(value: VideoDecodeCapabilityFlagBitsKHR) -> Self {
        match value {
            VideoDecodeCapabilityFlagBitsKHR::eDpbAndOutputCoincideBit => VideoDecodeCapabilityFlagsKHR::eDpbAndOutputCoincideBit,
            VideoDecodeCapabilityFlagBitsKHR::eDpbAndOutputDistinctBit => VideoDecodeCapabilityFlagsKHR::eDpbAndOutputDistinctBit,
            VideoDecodeCapabilityFlagBitsKHR::eUnknownBit(b) => VideoDecodeCapabilityFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(VideoDecodeCapabilityFlagBitsKHR, VideoDecodeCapabilityFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct VideoDecodeCapabilityFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl VideoDecodeCapabilityFlagsKHR {
    pub const eNone: Self = Self(0);
    pub const eDpbAndOutputCoincideBit: Self = Self(0b1);
    pub const eDpbAndOutputDistinctBit: Self = Self(0b10);
}

bit_field_mask_derives!(VideoDecodeCapabilityFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(VideoDecodeCapabilityFlagBitsKHR, VideoDecodeCapabilityFlagsKHR, u32, VideoDecodeCapabilityFlagBitsKHR::eDpbAndOutputCoincideBit, VideoDecodeCapabilityFlagBitsKHR::eDpbAndOutputDistinctBit);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct VideoDecodeFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl VideoDecodeFlagsKHR {
    pub const eNone: Self = Self(0);
}

bit_field_mask_derives!(VideoDecodeFlagsKHR, u32);

fieldless_debug_derive!(VideoDecodeFlagsKHR);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum VideoDecodeH264PictureLayoutFlagBitsKHR {
    eProgressive,
    eInterlacedInterleavedLinesBit,
    eInterlacedSeparatePlanesBit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl VideoDecodeH264PictureLayoutFlagBitsKHR {
    pub fn into_raw(self) -> RawVideoDecodeH264PictureLayoutFlagBitsKHR {
        match self {
            Self::eProgressive => RawVideoDecodeH264PictureLayoutFlagBitsKHR::eProgressive,
            Self::eInterlacedInterleavedLinesBit => RawVideoDecodeH264PictureLayoutFlagBitsKHR::eInterlacedInterleavedLinesBit,
            Self::eInterlacedSeparatePlanesBit => RawVideoDecodeH264PictureLayoutFlagBitsKHR::eInterlacedSeparatePlanesBit,
            Self::eUnknownBit(b) => RawVideoDecodeH264PictureLayoutFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<VideoDecodeH264PictureLayoutFlagBitsKHR> for VideoDecodeH264PictureLayoutFlagsKHR {
    fn from(value: VideoDecodeH264PictureLayoutFlagBitsKHR) -> Self {
        match value {
            VideoDecodeH264PictureLayoutFlagBitsKHR::eProgressive => VideoDecodeH264PictureLayoutFlagsKHR::eProgressive,
            VideoDecodeH264PictureLayoutFlagBitsKHR::eInterlacedInterleavedLinesBit => VideoDecodeH264PictureLayoutFlagsKHR::eInterlacedInterleavedLinesBit,
            VideoDecodeH264PictureLayoutFlagBitsKHR::eInterlacedSeparatePlanesBit => VideoDecodeH264PictureLayoutFlagsKHR::eInterlacedSeparatePlanesBit,
            VideoDecodeH264PictureLayoutFlagBitsKHR::eUnknownBit(b) => VideoDecodeH264PictureLayoutFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(VideoDecodeH264PictureLayoutFlagBitsKHR, VideoDecodeH264PictureLayoutFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct VideoDecodeH264PictureLayoutFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl VideoDecodeH264PictureLayoutFlagsKHR {
    pub const eProgressive: Self = Self(0);
    pub const eInterlacedInterleavedLinesBit: Self = Self(0b1);
    pub const eInterlacedSeparatePlanesBit: Self = Self(0b10);
}

bit_field_mask_derives!(VideoDecodeH264PictureLayoutFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(VideoDecodeH264PictureLayoutFlagBitsKHR, VideoDecodeH264PictureLayoutFlagsKHR, u32, VideoDecodeH264PictureLayoutFlagBitsKHR::eInterlacedInterleavedLinesBit, VideoDecodeH264PictureLayoutFlagBitsKHR::eInterlacedSeparatePlanesBit => VideoDecodeH264PictureLayoutFlagBitsKHR::eProgressive);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum VideoChromaSubsamplingFlagBitsKHR {
    eInvalid,
    eMonochromeBit,
    e420Bit,
    e422Bit,
    e444Bit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl VideoChromaSubsamplingFlagBitsKHR {
    pub fn into_raw(self) -> RawVideoChromaSubsamplingFlagBitsKHR {
        match self {
            Self::eInvalid => RawVideoChromaSubsamplingFlagBitsKHR::eInvalid,
            Self::eMonochromeBit => RawVideoChromaSubsamplingFlagBitsKHR::eMonochromeBit,
            Self::e420Bit => RawVideoChromaSubsamplingFlagBitsKHR::e420Bit,
            Self::e422Bit => RawVideoChromaSubsamplingFlagBitsKHR::e422Bit,
            Self::e444Bit => RawVideoChromaSubsamplingFlagBitsKHR::e444Bit,
            Self::eUnknownBit(b) => RawVideoChromaSubsamplingFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<VideoChromaSubsamplingFlagBitsKHR> for VideoChromaSubsamplingFlagsKHR {
    fn from(value: VideoChromaSubsamplingFlagBitsKHR) -> Self {
        match value {
            VideoChromaSubsamplingFlagBitsKHR::eInvalid => VideoChromaSubsamplingFlagsKHR::eInvalid,
            VideoChromaSubsamplingFlagBitsKHR::eMonochromeBit => VideoChromaSubsamplingFlagsKHR::eMonochromeBit,
            VideoChromaSubsamplingFlagBitsKHR::e420Bit => VideoChromaSubsamplingFlagsKHR::e420Bit,
            VideoChromaSubsamplingFlagBitsKHR::e422Bit => VideoChromaSubsamplingFlagsKHR::e422Bit,
            VideoChromaSubsamplingFlagBitsKHR::e444Bit => VideoChromaSubsamplingFlagsKHR::e444Bit,
            VideoChromaSubsamplingFlagBitsKHR::eUnknownBit(b) => VideoChromaSubsamplingFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(VideoChromaSubsamplingFlagBitsKHR, VideoChromaSubsamplingFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct VideoChromaSubsamplingFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl VideoChromaSubsamplingFlagsKHR {
    pub const eInvalid: Self = Self(0);
    pub const eMonochromeBit: Self = Self(0b1);
    pub const e420Bit: Self = Self(0b10);
    pub const e422Bit: Self = Self(0b100);
    pub const e444Bit: Self = Self(0b1000);
}

bit_field_mask_derives!(VideoChromaSubsamplingFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(VideoChromaSubsamplingFlagBitsKHR, VideoChromaSubsamplingFlagsKHR, u32, VideoChromaSubsamplingFlagBitsKHR::eMonochromeBit, VideoChromaSubsamplingFlagBitsKHR::e420Bit, VideoChromaSubsamplingFlagBitsKHR::e422Bit, VideoChromaSubsamplingFlagBitsKHR::e444Bit => VideoChromaSubsamplingFlagBitsKHR::eInvalid);

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[allow(non_camel_case_types)]
#[non_exhaustive]
pub enum VideoComponentBitDepthFlagBitsKHR {
    eInvalid,
    e8Bit,
    e10Bit,
    e12Bit,
    eUnknownBit(u32),
}

#[allow(non_upper_case_globals)]
impl VideoComponentBitDepthFlagBitsKHR {
    pub fn into_raw(self) -> RawVideoComponentBitDepthFlagBitsKHR {
        match self {
            Self::eInvalid => RawVideoComponentBitDepthFlagBitsKHR::eInvalid,
            Self::e8Bit => RawVideoComponentBitDepthFlagBitsKHR::e8Bit,
            Self::e10Bit => RawVideoComponentBitDepthFlagBitsKHR::e10Bit,
            Self::e12Bit => RawVideoComponentBitDepthFlagBitsKHR::e12Bit,
            Self::eUnknownBit(b) => RawVideoComponentBitDepthFlagBitsKHR(b),
        }
    }
}

impl core::convert::From<VideoComponentBitDepthFlagBitsKHR> for VideoComponentBitDepthFlagsKHR {
    fn from(value: VideoComponentBitDepthFlagBitsKHR) -> Self {
        match value {
            VideoComponentBitDepthFlagBitsKHR::eInvalid => VideoComponentBitDepthFlagsKHR::eInvalid,
            VideoComponentBitDepthFlagBitsKHR::e8Bit => VideoComponentBitDepthFlagsKHR::e8Bit,
            VideoComponentBitDepthFlagBitsKHR::e10Bit => VideoComponentBitDepthFlagsKHR::e10Bit,
            VideoComponentBitDepthFlagBitsKHR::e12Bit => VideoComponentBitDepthFlagsKHR::e12Bit,
            VideoComponentBitDepthFlagBitsKHR::eUnknownBit(b) => VideoComponentBitDepthFlagsKHR(b),
        }
    }
}

bit_field_enum_derives!(VideoComponentBitDepthFlagBitsKHR, VideoComponentBitDepthFlagsKHR, u32);

#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct VideoComponentBitDepthFlagsKHR(pub(crate) u32);

#[allow(non_upper_case_globals)]
impl VideoComponentBitDepthFlagsKHR {
    pub const eInvalid: Self = Self(0);
    pub const e8Bit: Self = Self(0b1);
    pub const e10Bit: Self = Self(0b100);
    pub const e12Bit: Self = Self(0b10000);
}

bit_field_mask_derives!(VideoComponentBitDepthFlagsKHR, u32);

bit_field_mask_with_enum_debug_derive!(VideoComponentBitDepthFlagBitsKHR, VideoComponentBitDepthFlagsKHR, u32, VideoComponentBitDepthFlagBitsKHR::e8Bit, VideoComponentBitDepthFlagBitsKHR::e10Bit, VideoComponentBitDepthFlagBitsKHR::e12Bit => VideoComponentBitDepthFlagBitsKHR::eInvalid);


pub(crate) use raw_bit_fields::*;
pub mod raw_bit_fields {
    use super::*;
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawFramebufferCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawFramebufferCreateFlagBits {
        pub const eImagelessBit: Self = Self(0b1);
        pub const eImagelessBitKhr: Self = Self::eImagelessBit;

        pub fn normalise(self) -> FramebufferCreateFlagBits {
            match self {
                Self::eImagelessBit => FramebufferCreateFlagBits::eImagelessBit,
                RawFramebufferCreateFlagBits(b) => FramebufferCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<FramebufferCreateFlagBits> {
            match self {
                RawFramebufferCreateFlagBits(0) => None,
                Self::eImagelessBit => Some(FramebufferCreateFlagBits::eImagelessBit),
                RawFramebufferCreateFlagBits(b) => Some(FramebufferCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawFramebufferCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawRenderPassCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawRenderPassCreateFlagBits {
        pub const eTransformBitQcom: Self = Self(0b10);

        pub fn normalise(self) -> RenderPassCreateFlagBits {
            match self {
                Self::eTransformBitQcom => RenderPassCreateFlagBits::eTransformBitQcom,
                RawRenderPassCreateFlagBits(b) => RenderPassCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<RenderPassCreateFlagBits> {
            match self {
                RawRenderPassCreateFlagBits(0) => None,
                Self::eTransformBitQcom => Some(RenderPassCreateFlagBits::eTransformBitQcom),
                RawRenderPassCreateFlagBits(b) => Some(RenderPassCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawRenderPassCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawSamplerCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawSamplerCreateFlagBits {
        pub const eSubsampledBitExt: Self = Self(0b1);
        pub const eSubsampledCoarseReconstructionBitExt: Self = Self(0b10);
        pub const eNonSeamlessCubeMapBitExt: Self = Self(0b100);
        pub const eDescriptorBufferCaptureReplayBitExt: Self = Self(0b1000);
        pub const eImageProcessingBitQcom: Self = Self(0b10000);

        pub fn normalise(self) -> SamplerCreateFlagBits {
            match self {
                Self::eSubsampledBitExt => SamplerCreateFlagBits::eSubsampledBitExt,
                Self::eSubsampledCoarseReconstructionBitExt => SamplerCreateFlagBits::eSubsampledCoarseReconstructionBitExt,
                Self::eNonSeamlessCubeMapBitExt => SamplerCreateFlagBits::eNonSeamlessCubeMapBitExt,
                Self::eDescriptorBufferCaptureReplayBitExt => SamplerCreateFlagBits::eDescriptorBufferCaptureReplayBitExt,
                Self::eImageProcessingBitQcom => SamplerCreateFlagBits::eImageProcessingBitQcom,
                RawSamplerCreateFlagBits(b) => SamplerCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<SamplerCreateFlagBits> {
            match self {
                RawSamplerCreateFlagBits(0) => None,
                Self::eSubsampledBitExt => Some(SamplerCreateFlagBits::eSubsampledBitExt),
                Self::eSubsampledCoarseReconstructionBitExt => Some(SamplerCreateFlagBits::eSubsampledCoarseReconstructionBitExt),
                Self::eNonSeamlessCubeMapBitExt => Some(SamplerCreateFlagBits::eNonSeamlessCubeMapBitExt),
                Self::eDescriptorBufferCaptureReplayBitExt => Some(SamplerCreateFlagBits::eDescriptorBufferCaptureReplayBitExt),
                Self::eImageProcessingBitQcom => Some(SamplerCreateFlagBits::eImageProcessingBitQcom),
                RawSamplerCreateFlagBits(b) => Some(SamplerCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawSamplerCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawPipelineLayoutCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawPipelineLayoutCreateFlagBits {
        pub const eIndependentSetsBitExt: Self = Self(0b10);

        pub fn normalise(self) -> PipelineLayoutCreateFlagBits {
            match self {
                Self::eIndependentSetsBitExt => PipelineLayoutCreateFlagBits::eIndependentSetsBitExt,
                RawPipelineLayoutCreateFlagBits(b) => PipelineLayoutCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<PipelineLayoutCreateFlagBits> {
            match self {
                RawPipelineLayoutCreateFlagBits(0) => None,
                Self::eIndependentSetsBitExt => Some(PipelineLayoutCreateFlagBits::eIndependentSetsBitExt),
                RawPipelineLayoutCreateFlagBits(b) => Some(PipelineLayoutCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawPipelineLayoutCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawPipelineCacheCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawPipelineCacheCreateFlagBits {
        pub const eExternallySynchronizedBit: Self = Self(0b1);
        pub const eUseApplicationStorageBit: Self = Self(0b100);
        pub const eExternallySynchronizedBitExt: Self = Self::eExternallySynchronizedBit;

        pub fn normalise(self) -> PipelineCacheCreateFlagBits {
            match self {
                Self::eExternallySynchronizedBit => PipelineCacheCreateFlagBits::eExternallySynchronizedBit,
                Self::eUseApplicationStorageBit => PipelineCacheCreateFlagBits::eUseApplicationStorageBit,
                RawPipelineCacheCreateFlagBits(b) => PipelineCacheCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<PipelineCacheCreateFlagBits> {
            match self {
                RawPipelineCacheCreateFlagBits(0) => None,
                Self::eExternallySynchronizedBit => Some(PipelineCacheCreateFlagBits::eExternallySynchronizedBit),
                Self::eUseApplicationStorageBit => Some(PipelineCacheCreateFlagBits::eUseApplicationStorageBit),
                RawPipelineCacheCreateFlagBits(b) => Some(PipelineCacheCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawPipelineCacheCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawPipelineDepthStencilStateCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawPipelineDepthStencilStateCreateFlagBits {
        pub const eRasterizationOrderAttachmentDepthAccessBitExt: Self = Self(0b1);
        pub const eRasterizationOrderAttachmentStencilAccessBitExt: Self = Self(0b10);

        pub fn normalise(self) -> PipelineDepthStencilStateCreateFlagBits {
            match self {
                Self::eRasterizationOrderAttachmentDepthAccessBitExt => PipelineDepthStencilStateCreateFlagBits::eRasterizationOrderAttachmentDepthAccessBitExt,
                Self::eRasterizationOrderAttachmentStencilAccessBitExt => PipelineDepthStencilStateCreateFlagBits::eRasterizationOrderAttachmentStencilAccessBitExt,
                RawPipelineDepthStencilStateCreateFlagBits(b) => PipelineDepthStencilStateCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<PipelineDepthStencilStateCreateFlagBits> {
            match self {
                RawPipelineDepthStencilStateCreateFlagBits(0) => None,
                Self::eRasterizationOrderAttachmentDepthAccessBitExt => Some(PipelineDepthStencilStateCreateFlagBits::eRasterizationOrderAttachmentDepthAccessBitExt),
                Self::eRasterizationOrderAttachmentStencilAccessBitExt => Some(PipelineDepthStencilStateCreateFlagBits::eRasterizationOrderAttachmentStencilAccessBitExt),
                RawPipelineDepthStencilStateCreateFlagBits(b) => Some(PipelineDepthStencilStateCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawPipelineDepthStencilStateCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawPipelineColourBlendStateCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawPipelineColourBlendStateCreateFlagBits {
        pub const eRasterizationOrderAttachmentAccessBitExt: Self = Self(0b1);

        pub fn normalise(self) -> PipelineColourBlendStateCreateFlagBits {
            match self {
                Self::eRasterizationOrderAttachmentAccessBitExt => PipelineColourBlendStateCreateFlagBits::eRasterizationOrderAttachmentAccessBitExt,
                RawPipelineColourBlendStateCreateFlagBits(b) => PipelineColourBlendStateCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<PipelineColourBlendStateCreateFlagBits> {
            match self {
                RawPipelineColourBlendStateCreateFlagBits(0) => None,
                Self::eRasterizationOrderAttachmentAccessBitExt => Some(PipelineColourBlendStateCreateFlagBits::eRasterizationOrderAttachmentAccessBitExt),
                RawPipelineColourBlendStateCreateFlagBits(b) => Some(PipelineColourBlendStateCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawPipelineColourBlendStateCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawPipelineShaderStageCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawPipelineShaderStageCreateFlagBits {
        pub const eAllowVaryingSubgroupSizeBit: Self = Self(0b1);
        pub const eRequireFullSubgroupsBit: Self = Self(0b10);
        pub const eAllowVaryingSubgroupSizeBitExt: Self = Self::eAllowVaryingSubgroupSizeBit;
        pub const eRequireFullSubgroupsBitExt: Self = Self::eRequireFullSubgroupsBit;

        pub fn normalise(self) -> PipelineShaderStageCreateFlagBits {
            match self {
                Self::eAllowVaryingSubgroupSizeBit => PipelineShaderStageCreateFlagBits::eAllowVaryingSubgroupSizeBit,
                Self::eRequireFullSubgroupsBit => PipelineShaderStageCreateFlagBits::eRequireFullSubgroupsBit,
                RawPipelineShaderStageCreateFlagBits(b) => PipelineShaderStageCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<PipelineShaderStageCreateFlagBits> {
            match self {
                RawPipelineShaderStageCreateFlagBits(0) => None,
                Self::eAllowVaryingSubgroupSizeBit => Some(PipelineShaderStageCreateFlagBits::eAllowVaryingSubgroupSizeBit),
                Self::eRequireFullSubgroupsBit => Some(PipelineShaderStageCreateFlagBits::eRequireFullSubgroupsBit),
                RawPipelineShaderStageCreateFlagBits(b) => Some(PipelineShaderStageCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawPipelineShaderStageCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawDescriptorSetLayoutCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawDescriptorSetLayoutCreateFlagBits {
        pub const ePushDescriptorBitKhr: Self = Self(0b1);
        pub const eUpdateAfterBindPoolBit: Self = Self(0b10);
        pub const eHostOnlyPoolBitExt: Self = Self(0b100);
        pub const eDescriptorBufferBitExt: Self = Self(0b10000);
        pub const eEmbeddedImmutableSamplersBitExt: Self = Self(0b100000);
        pub const eUpdateAfterBindPoolBitExt: Self = Self::eUpdateAfterBindPoolBit;

        pub fn normalise(self) -> DescriptorSetLayoutCreateFlagBits {
            match self {
                Self::ePushDescriptorBitKhr => DescriptorSetLayoutCreateFlagBits::ePushDescriptorBitKhr,
                Self::eUpdateAfterBindPoolBit => DescriptorSetLayoutCreateFlagBits::eUpdateAfterBindPoolBit,
                Self::eHostOnlyPoolBitExt => DescriptorSetLayoutCreateFlagBits::eHostOnlyPoolBitExt,
                Self::eDescriptorBufferBitExt => DescriptorSetLayoutCreateFlagBits::eDescriptorBufferBitExt,
                Self::eEmbeddedImmutableSamplersBitExt => DescriptorSetLayoutCreateFlagBits::eEmbeddedImmutableSamplersBitExt,
                RawDescriptorSetLayoutCreateFlagBits(b) => DescriptorSetLayoutCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<DescriptorSetLayoutCreateFlagBits> {
            match self {
                RawDescriptorSetLayoutCreateFlagBits(0) => None,
                Self::ePushDescriptorBitKhr => Some(DescriptorSetLayoutCreateFlagBits::ePushDescriptorBitKhr),
                Self::eUpdateAfterBindPoolBit => Some(DescriptorSetLayoutCreateFlagBits::eUpdateAfterBindPoolBit),
                Self::eHostOnlyPoolBitExt => Some(DescriptorSetLayoutCreateFlagBits::eHostOnlyPoolBitExt),
                Self::eDescriptorBufferBitExt => Some(DescriptorSetLayoutCreateFlagBits::eDescriptorBufferBitExt),
                Self::eEmbeddedImmutableSamplersBitExt => Some(DescriptorSetLayoutCreateFlagBits::eEmbeddedImmutableSamplersBitExt),
                RawDescriptorSetLayoutCreateFlagBits(b) => Some(DescriptorSetLayoutCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawDescriptorSetLayoutCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawInstanceCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawInstanceCreateFlagBits {
        pub const eEnumeratePortabilityBitKhr: Self = Self(0b1);

        pub fn normalise(self) -> InstanceCreateFlagBits {
            match self {
                Self::eEnumeratePortabilityBitKhr => InstanceCreateFlagBits::eEnumeratePortabilityBitKhr,
                RawInstanceCreateFlagBits(b) => InstanceCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<InstanceCreateFlagBits> {
            match self {
                RawInstanceCreateFlagBits(0) => None,
                Self::eEnumeratePortabilityBitKhr => Some(InstanceCreateFlagBits::eEnumeratePortabilityBitKhr),
                RawInstanceCreateFlagBits(b) => Some(InstanceCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawInstanceCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawDeviceQueueCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawDeviceQueueCreateFlagBits {
        pub const eProtectedBit: Self = Self(0b1);

        pub fn normalise(self) -> DeviceQueueCreateFlagBits {
            match self {
                Self::eProtectedBit => DeviceQueueCreateFlagBits::eProtectedBit,
                RawDeviceQueueCreateFlagBits(b) => DeviceQueueCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<DeviceQueueCreateFlagBits> {
            match self {
                RawDeviceQueueCreateFlagBits(0) => None,
                Self::eProtectedBit => Some(DeviceQueueCreateFlagBits::eProtectedBit),
                RawDeviceQueueCreateFlagBits(b) => Some(DeviceQueueCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawDeviceQueueCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawQueueFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawQueueFlagBits {
        pub const eGraphicsBit: Self = Self(0b1);
        pub const eComputeBit: Self = Self(0b10);
        pub const eTransferBit: Self = Self(0b100);
        pub const eSparseBindingBit: Self = Self(0b1000);
        pub const eProtectedBit: Self = Self(0b10000);
        pub const eVideoDecodeBitKhr: Self = Self(0b100000);
        pub const eVideoEncodeBitKhr: Self = Self(0b1000000);
        pub const eOpticalFlowBitNv: Self = Self(0b100000000);

        pub fn normalise(self) -> QueueFlagBits {
            match self {
                Self::eGraphicsBit => QueueFlagBits::eGraphicsBit,
                Self::eComputeBit => QueueFlagBits::eComputeBit,
                Self::eTransferBit => QueueFlagBits::eTransferBit,
                Self::eSparseBindingBit => QueueFlagBits::eSparseBindingBit,
                Self::eProtectedBit => QueueFlagBits::eProtectedBit,
                Self::eVideoDecodeBitKhr => QueueFlagBits::eVideoDecodeBitKhr,
                Self::eVideoEncodeBitKhr => QueueFlagBits::eVideoEncodeBitKhr,
                Self::eOpticalFlowBitNv => QueueFlagBits::eOpticalFlowBitNv,
                RawQueueFlagBits(b) => QueueFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<QueueFlagBits> {
            match self {
                RawQueueFlagBits(0) => None,
                Self::eGraphicsBit => Some(QueueFlagBits::eGraphicsBit),
                Self::eComputeBit => Some(QueueFlagBits::eComputeBit),
                Self::eTransferBit => Some(QueueFlagBits::eTransferBit),
                Self::eSparseBindingBit => Some(QueueFlagBits::eSparseBindingBit),
                Self::eProtectedBit => Some(QueueFlagBits::eProtectedBit),
                Self::eVideoDecodeBitKhr => Some(QueueFlagBits::eVideoDecodeBitKhr),
                Self::eVideoEncodeBitKhr => Some(QueueFlagBits::eVideoEncodeBitKhr),
                Self::eOpticalFlowBitNv => Some(QueueFlagBits::eOpticalFlowBitNv),
                RawQueueFlagBits(b) => Some(QueueFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawQueueFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawMemoryPropertyFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawMemoryPropertyFlagBits {
        pub const eDeviceLocalBit: Self = Self(0b1);
        pub const eHostVisibleBit: Self = Self(0b10);
        pub const eHostCoherentBit: Self = Self(0b100);
        pub const eHostCachedBit: Self = Self(0b1000);
        pub const eLazilyAllocatedBit: Self = Self(0b10000);
        pub const eProtectedBit: Self = Self(0b100000);
        pub const eDeviceCoherentBitAmd: Self = Self(0b1000000);
        pub const eDeviceUncachedBitAmd: Self = Self(0b10000000);
        pub const eRdmaCapableBitNv: Self = Self(0b100000000);

        pub fn normalise(self) -> MemoryPropertyFlagBits {
            match self {
                Self::eDeviceLocalBit => MemoryPropertyFlagBits::eDeviceLocalBit,
                Self::eHostVisibleBit => MemoryPropertyFlagBits::eHostVisibleBit,
                Self::eHostCoherentBit => MemoryPropertyFlagBits::eHostCoherentBit,
                Self::eHostCachedBit => MemoryPropertyFlagBits::eHostCachedBit,
                Self::eLazilyAllocatedBit => MemoryPropertyFlagBits::eLazilyAllocatedBit,
                Self::eProtectedBit => MemoryPropertyFlagBits::eProtectedBit,
                Self::eDeviceCoherentBitAmd => MemoryPropertyFlagBits::eDeviceCoherentBitAmd,
                Self::eDeviceUncachedBitAmd => MemoryPropertyFlagBits::eDeviceUncachedBitAmd,
                Self::eRdmaCapableBitNv => MemoryPropertyFlagBits::eRdmaCapableBitNv,
                RawMemoryPropertyFlagBits(b) => MemoryPropertyFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<MemoryPropertyFlagBits> {
            match self {
                RawMemoryPropertyFlagBits(0) => None,
                Self::eDeviceLocalBit => Some(MemoryPropertyFlagBits::eDeviceLocalBit),
                Self::eHostVisibleBit => Some(MemoryPropertyFlagBits::eHostVisibleBit),
                Self::eHostCoherentBit => Some(MemoryPropertyFlagBits::eHostCoherentBit),
                Self::eHostCachedBit => Some(MemoryPropertyFlagBits::eHostCachedBit),
                Self::eLazilyAllocatedBit => Some(MemoryPropertyFlagBits::eLazilyAllocatedBit),
                Self::eProtectedBit => Some(MemoryPropertyFlagBits::eProtectedBit),
                Self::eDeviceCoherentBitAmd => Some(MemoryPropertyFlagBits::eDeviceCoherentBitAmd),
                Self::eDeviceUncachedBitAmd => Some(MemoryPropertyFlagBits::eDeviceUncachedBitAmd),
                Self::eRdmaCapableBitNv => Some(MemoryPropertyFlagBits::eRdmaCapableBitNv),
                RawMemoryPropertyFlagBits(b) => Some(MemoryPropertyFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawMemoryPropertyFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawMemoryHeapFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawMemoryHeapFlagBits {
        pub const eDeviceLocalBit: Self = Self(0b1);
        pub const eMultiInstanceBit: Self = Self(0b10);
        pub const eSeuSafeBit: Self = Self(0b100);
        pub const eMultiInstanceBitKhr: Self = Self::eMultiInstanceBit;

        pub fn normalise(self) -> MemoryHeapFlagBits {
            match self {
                Self::eDeviceLocalBit => MemoryHeapFlagBits::eDeviceLocalBit,
                Self::eMultiInstanceBit => MemoryHeapFlagBits::eMultiInstanceBit,
                Self::eSeuSafeBit => MemoryHeapFlagBits::eSeuSafeBit,
                RawMemoryHeapFlagBits(b) => MemoryHeapFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<MemoryHeapFlagBits> {
            match self {
                RawMemoryHeapFlagBits(0) => None,
                Self::eDeviceLocalBit => Some(MemoryHeapFlagBits::eDeviceLocalBit),
                Self::eMultiInstanceBit => Some(MemoryHeapFlagBits::eMultiInstanceBit),
                Self::eSeuSafeBit => Some(MemoryHeapFlagBits::eSeuSafeBit),
                RawMemoryHeapFlagBits(b) => Some(MemoryHeapFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawMemoryHeapFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawAccessFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawAccessFlagBits {
        pub const eNone: Self = Self(0);
        pub const eIndirectCommandReadBit: Self = Self(0b1);
        pub const eIndexReadBit: Self = Self(0b10);
        pub const eVertexAttributeReadBit: Self = Self(0b100);
        pub const eUniformReadBit: Self = Self(0b1000);
        pub const eInputAttachmentReadBit: Self = Self(0b10000);
        pub const eShaderReadBit: Self = Self(0b100000);
        pub const eShaderWriteBit: Self = Self(0b1000000);
        pub const eColourAttachmentReadBit: Self = Self(0b10000000);
        pub const eColourAttachmentWriteBit: Self = Self(0b100000000);
        pub const eDepthStencilAttachmentReadBit: Self = Self(0b1000000000);
        pub const eDepthStencilAttachmentWriteBit: Self = Self(0b10000000000);
        pub const eTransferReadBit: Self = Self(0b100000000000);
        pub const eTransferWriteBit: Self = Self(0b1000000000000);
        pub const eHostReadBit: Self = Self(0b10000000000000);
        pub const eHostWriteBit: Self = Self(0b100000000000000);
        pub const eMemoryReadBit: Self = Self(0b1000000000000000);
        pub const eMemoryWriteBit: Self = Self(0b10000000000000000);
        pub const eCommandPreprocessReadBitNv: Self = Self(0b100000000000000000);
        pub const eCommandPreprocessWriteBitNv: Self = Self(0b1000000000000000000);
        pub const eColourAttachmentReadNoncoherentBitExt: Self = Self(0b10000000000000000000);
        pub const eConditionalRenderingReadBitExt: Self = Self(0b100000000000000000000);
        pub const eAccelerationStructureReadBitKhr: Self = Self(0b1000000000000000000000);
        pub const eAccelerationStructureWriteBitKhr: Self = Self(0b10000000000000000000000);
        pub const eFragmentShadingRateAttachmentReadBitKhr: Self = Self(0b100000000000000000000000);
        pub const eFragmentDensityMapReadBitExt: Self = Self(0b1000000000000000000000000);
        pub const eTransformFeedbackWriteBitExt: Self = Self(0b10000000000000000000000000);
        pub const eTransformFeedbackCounterReadBitExt: Self = Self(0b100000000000000000000000000);
        pub const eTransformFeedbackCounterWriteBitExt: Self = Self(0b1000000000000000000000000000);
        pub const eNoneKhr: Self = Self::eNone;
        pub const eAccelerationStructureReadBitNv: Self = Self::eAccelerationStructureReadBitKhr;
        pub const eAccelerationStructureWriteBitNv: Self = Self::eAccelerationStructureWriteBitKhr;

        pub fn normalise(self) -> AccessFlagBits {
            match self {
                Self::eNone => AccessFlagBits::eNone,
                Self::eIndirectCommandReadBit => AccessFlagBits::eIndirectCommandReadBit,
                Self::eIndexReadBit => AccessFlagBits::eIndexReadBit,
                Self::eVertexAttributeReadBit => AccessFlagBits::eVertexAttributeReadBit,
                Self::eUniformReadBit => AccessFlagBits::eUniformReadBit,
                Self::eInputAttachmentReadBit => AccessFlagBits::eInputAttachmentReadBit,
                Self::eShaderReadBit => AccessFlagBits::eShaderReadBit,
                Self::eShaderWriteBit => AccessFlagBits::eShaderWriteBit,
                Self::eColourAttachmentReadBit => AccessFlagBits::eColourAttachmentReadBit,
                Self::eColourAttachmentWriteBit => AccessFlagBits::eColourAttachmentWriteBit,
                Self::eDepthStencilAttachmentReadBit => AccessFlagBits::eDepthStencilAttachmentReadBit,
                Self::eDepthStencilAttachmentWriteBit => AccessFlagBits::eDepthStencilAttachmentWriteBit,
                Self::eTransferReadBit => AccessFlagBits::eTransferReadBit,
                Self::eTransferWriteBit => AccessFlagBits::eTransferWriteBit,
                Self::eHostReadBit => AccessFlagBits::eHostReadBit,
                Self::eHostWriteBit => AccessFlagBits::eHostWriteBit,
                Self::eMemoryReadBit => AccessFlagBits::eMemoryReadBit,
                Self::eMemoryWriteBit => AccessFlagBits::eMemoryWriteBit,
                Self::eCommandPreprocessReadBitNv => AccessFlagBits::eCommandPreprocessReadBitNv,
                Self::eCommandPreprocessWriteBitNv => AccessFlagBits::eCommandPreprocessWriteBitNv,
                Self::eColourAttachmentReadNoncoherentBitExt => AccessFlagBits::eColourAttachmentReadNoncoherentBitExt,
                Self::eConditionalRenderingReadBitExt => AccessFlagBits::eConditionalRenderingReadBitExt,
                Self::eAccelerationStructureReadBitKhr => AccessFlagBits::eAccelerationStructureReadBitKhr,
                Self::eAccelerationStructureWriteBitKhr => AccessFlagBits::eAccelerationStructureWriteBitKhr,
                Self::eFragmentShadingRateAttachmentReadBitKhr => AccessFlagBits::eFragmentShadingRateAttachmentReadBitKhr,
                Self::eFragmentDensityMapReadBitExt => AccessFlagBits::eFragmentDensityMapReadBitExt,
                Self::eTransformFeedbackWriteBitExt => AccessFlagBits::eTransformFeedbackWriteBitExt,
                Self::eTransformFeedbackCounterReadBitExt => AccessFlagBits::eTransformFeedbackCounterReadBitExt,
                Self::eTransformFeedbackCounterWriteBitExt => AccessFlagBits::eTransformFeedbackCounterWriteBitExt,
                RawAccessFlagBits(b) => AccessFlagBits::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawAccessFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawBufferUsageFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawBufferUsageFlagBits {
        pub const eTransferSrcBit: Self = Self(0b1);
        pub const eTransferDstBit: Self = Self(0b10);
        pub const eUniformTexelBufferBit: Self = Self(0b100);
        pub const eStorageTexelBufferBit: Self = Self(0b1000);
        pub const eUniformBufferBit: Self = Self(0b10000);
        pub const eStorageBufferBit: Self = Self(0b100000);
        pub const eIndexBufferBit: Self = Self(0b1000000);
        pub const eVertexBufferBit: Self = Self(0b10000000);
        pub const eIndirectBufferBit: Self = Self(0b100000000);
        pub const eConditionalRenderingBitExt: Self = Self(0b1000000000);
        pub const eShaderBindingTableBitKhr: Self = Self(0b10000000000);
        pub const eTransformFeedbackBufferBitExt: Self = Self(0b100000000000);
        pub const eTransformFeedbackCounterBufferBitExt: Self = Self(0b1000000000000);
        pub const eVideoDecodeSrcBitKhr: Self = Self(0b10000000000000);
        pub const eVideoDecodeDstBitKhr: Self = Self(0b100000000000000);
        pub const eVideoEncodeDstBitKhr: Self = Self(0b1000000000000000);
        pub const eVideoEncodeSrcBitKhr: Self = Self(0b10000000000000000);
        pub const eShaderDeviceAddressBit: Self = Self(0b100000000000000000);
        pub const eAccelerationStructureBuildInputReadOnlyBitKhr: Self = Self(0b10000000000000000000);
        pub const eAccelerationStructureStorageBitKhr: Self = Self(0b100000000000000000000);
        pub const eSamplerDescriptorBufferBitExt: Self = Self(0b1000000000000000000000);
        pub const eResourceDescriptorBufferBitExt: Self = Self(0b10000000000000000000000);
        pub const eMicromapBuildInputReadOnlyBitExt: Self = Self(0b100000000000000000000000);
        pub const eMicromapStorageBitExt: Self = Self(0b1000000000000000000000000);
        pub const ePushDescriptorsDescriptorBufferBitExt: Self = Self(0b100000000000000000000000000);
        pub const eRayTracingBitNv: Self = Self::eShaderBindingTableBitKhr;
        pub const eShaderDeviceAddressBitExt: Self = Self::eShaderDeviceAddressBit;
        pub const eShaderDeviceAddressBitKhr: Self = Self::eShaderDeviceAddressBit;

        pub fn normalise(self) -> BufferUsageFlagBits {
            match self {
                Self::eTransferSrcBit => BufferUsageFlagBits::eTransferSrcBit,
                Self::eTransferDstBit => BufferUsageFlagBits::eTransferDstBit,
                Self::eUniformTexelBufferBit => BufferUsageFlagBits::eUniformTexelBufferBit,
                Self::eStorageTexelBufferBit => BufferUsageFlagBits::eStorageTexelBufferBit,
                Self::eUniformBufferBit => BufferUsageFlagBits::eUniformBufferBit,
                Self::eStorageBufferBit => BufferUsageFlagBits::eStorageBufferBit,
                Self::eIndexBufferBit => BufferUsageFlagBits::eIndexBufferBit,
                Self::eVertexBufferBit => BufferUsageFlagBits::eVertexBufferBit,
                Self::eIndirectBufferBit => BufferUsageFlagBits::eIndirectBufferBit,
                Self::eConditionalRenderingBitExt => BufferUsageFlagBits::eConditionalRenderingBitExt,
                Self::eShaderBindingTableBitKhr => BufferUsageFlagBits::eShaderBindingTableBitKhr,
                Self::eTransformFeedbackBufferBitExt => BufferUsageFlagBits::eTransformFeedbackBufferBitExt,
                Self::eTransformFeedbackCounterBufferBitExt => BufferUsageFlagBits::eTransformFeedbackCounterBufferBitExt,
                Self::eVideoDecodeSrcBitKhr => BufferUsageFlagBits::eVideoDecodeSrcBitKhr,
                Self::eVideoDecodeDstBitKhr => BufferUsageFlagBits::eVideoDecodeDstBitKhr,
                Self::eVideoEncodeDstBitKhr => BufferUsageFlagBits::eVideoEncodeDstBitKhr,
                Self::eVideoEncodeSrcBitKhr => BufferUsageFlagBits::eVideoEncodeSrcBitKhr,
                Self::eShaderDeviceAddressBit => BufferUsageFlagBits::eShaderDeviceAddressBit,
                Self::eAccelerationStructureBuildInputReadOnlyBitKhr => BufferUsageFlagBits::eAccelerationStructureBuildInputReadOnlyBitKhr,
                Self::eAccelerationStructureStorageBitKhr => BufferUsageFlagBits::eAccelerationStructureStorageBitKhr,
                Self::eSamplerDescriptorBufferBitExt => BufferUsageFlagBits::eSamplerDescriptorBufferBitExt,
                Self::eResourceDescriptorBufferBitExt => BufferUsageFlagBits::eResourceDescriptorBufferBitExt,
                Self::eMicromapBuildInputReadOnlyBitExt => BufferUsageFlagBits::eMicromapBuildInputReadOnlyBitExt,
                Self::eMicromapStorageBitExt => BufferUsageFlagBits::eMicromapStorageBitExt,
                Self::ePushDescriptorsDescriptorBufferBitExt => BufferUsageFlagBits::ePushDescriptorsDescriptorBufferBitExt,
                RawBufferUsageFlagBits(b) => BufferUsageFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<BufferUsageFlagBits> {
            match self {
                RawBufferUsageFlagBits(0) => None,
                Self::eTransferSrcBit => Some(BufferUsageFlagBits::eTransferSrcBit),
                Self::eTransferDstBit => Some(BufferUsageFlagBits::eTransferDstBit),
                Self::eUniformTexelBufferBit => Some(BufferUsageFlagBits::eUniformTexelBufferBit),
                Self::eStorageTexelBufferBit => Some(BufferUsageFlagBits::eStorageTexelBufferBit),
                Self::eUniformBufferBit => Some(BufferUsageFlagBits::eUniformBufferBit),
                Self::eStorageBufferBit => Some(BufferUsageFlagBits::eStorageBufferBit),
                Self::eIndexBufferBit => Some(BufferUsageFlagBits::eIndexBufferBit),
                Self::eVertexBufferBit => Some(BufferUsageFlagBits::eVertexBufferBit),
                Self::eIndirectBufferBit => Some(BufferUsageFlagBits::eIndirectBufferBit),
                Self::eConditionalRenderingBitExt => Some(BufferUsageFlagBits::eConditionalRenderingBitExt),
                Self::eShaderBindingTableBitKhr => Some(BufferUsageFlagBits::eShaderBindingTableBitKhr),
                Self::eTransformFeedbackBufferBitExt => Some(BufferUsageFlagBits::eTransformFeedbackBufferBitExt),
                Self::eTransformFeedbackCounterBufferBitExt => Some(BufferUsageFlagBits::eTransformFeedbackCounterBufferBitExt),
                Self::eVideoDecodeSrcBitKhr => Some(BufferUsageFlagBits::eVideoDecodeSrcBitKhr),
                Self::eVideoDecodeDstBitKhr => Some(BufferUsageFlagBits::eVideoDecodeDstBitKhr),
                Self::eVideoEncodeDstBitKhr => Some(BufferUsageFlagBits::eVideoEncodeDstBitKhr),
                Self::eVideoEncodeSrcBitKhr => Some(BufferUsageFlagBits::eVideoEncodeSrcBitKhr),
                Self::eShaderDeviceAddressBit => Some(BufferUsageFlagBits::eShaderDeviceAddressBit),
                Self::eAccelerationStructureBuildInputReadOnlyBitKhr => Some(BufferUsageFlagBits::eAccelerationStructureBuildInputReadOnlyBitKhr),
                Self::eAccelerationStructureStorageBitKhr => Some(BufferUsageFlagBits::eAccelerationStructureStorageBitKhr),
                Self::eSamplerDescriptorBufferBitExt => Some(BufferUsageFlagBits::eSamplerDescriptorBufferBitExt),
                Self::eResourceDescriptorBufferBitExt => Some(BufferUsageFlagBits::eResourceDescriptorBufferBitExt),
                Self::eMicromapBuildInputReadOnlyBitExt => Some(BufferUsageFlagBits::eMicromapBuildInputReadOnlyBitExt),
                Self::eMicromapStorageBitExt => Some(BufferUsageFlagBits::eMicromapStorageBitExt),
                Self::ePushDescriptorsDescriptorBufferBitExt => Some(BufferUsageFlagBits::ePushDescriptorsDescriptorBufferBitExt),
                RawBufferUsageFlagBits(b) => Some(BufferUsageFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawBufferUsageFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawBufferCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawBufferCreateFlagBits {
        pub const eSparseBindingBit: Self = Self(0b1);
        pub const eSparseResidencyBit: Self = Self(0b10);
        pub const eSparseAliasedBit: Self = Self(0b100);
        pub const eProtectedBit: Self = Self(0b1000);
        pub const eDeviceAddressCaptureReplayBit: Self = Self(0b10000);
        pub const eDescriptorBufferCaptureReplayBitExt: Self = Self(0b100000);
        pub const eDeviceAddressCaptureReplayBitExt: Self = Self::eDeviceAddressCaptureReplayBit;
        pub const eDeviceAddressCaptureReplayBitKhr: Self = Self::eDeviceAddressCaptureReplayBit;

        pub fn normalise(self) -> BufferCreateFlagBits {
            match self {
                Self::eSparseBindingBit => BufferCreateFlagBits::eSparseBindingBit,
                Self::eSparseResidencyBit => BufferCreateFlagBits::eSparseResidencyBit,
                Self::eSparseAliasedBit => BufferCreateFlagBits::eSparseAliasedBit,
                Self::eProtectedBit => BufferCreateFlagBits::eProtectedBit,
                Self::eDeviceAddressCaptureReplayBit => BufferCreateFlagBits::eDeviceAddressCaptureReplayBit,
                Self::eDescriptorBufferCaptureReplayBitExt => BufferCreateFlagBits::eDescriptorBufferCaptureReplayBitExt,
                RawBufferCreateFlagBits(b) => BufferCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<BufferCreateFlagBits> {
            match self {
                RawBufferCreateFlagBits(0) => None,
                Self::eSparseBindingBit => Some(BufferCreateFlagBits::eSparseBindingBit),
                Self::eSparseResidencyBit => Some(BufferCreateFlagBits::eSparseResidencyBit),
                Self::eSparseAliasedBit => Some(BufferCreateFlagBits::eSparseAliasedBit),
                Self::eProtectedBit => Some(BufferCreateFlagBits::eProtectedBit),
                Self::eDeviceAddressCaptureReplayBit => Some(BufferCreateFlagBits::eDeviceAddressCaptureReplayBit),
                Self::eDescriptorBufferCaptureReplayBitExt => Some(BufferCreateFlagBits::eDescriptorBufferCaptureReplayBitExt),
                RawBufferCreateFlagBits(b) => Some(BufferCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawBufferCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawShaderStageFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawShaderStageFlagBits {
        pub const eVertexBit: Self = Self(0b1);
        pub const eTessellationControlBit: Self = Self(0b10);
        pub const eTessellationEvaluationBit: Self = Self(0b100);
        pub const eGeometryBit: Self = Self(0b1000);
        pub const eFragmentBit: Self = Self(0b10000);
        pub const eAllGraphics: Self = Self(0b11111);
        pub const eComputeBit: Self = Self(0b100000);
        pub const eTaskBitExt: Self = Self(0b1000000);
        pub const eMeshBitExt: Self = Self(0b10000000);
        pub const eRaygenBitKhr: Self = Self(0b100000000);
        pub const eAnyHitBitKhr: Self = Self(0b1000000000);
        pub const eClosestHitBitKhr: Self = Self(0b10000000000);
        pub const eMissBitKhr: Self = Self(0b100000000000);
        pub const eIntersectionBitKhr: Self = Self(0b1000000000000);
        pub const eCallableBitKhr: Self = Self(0b10000000000000);
        pub const eSubpassShadingBitHuawei: Self = Self(0b100000000000000);
        pub const eClusterCullingBitHuawei: Self = Self(0b10000000000000000000);
        pub const eAll: Self = Self(0b1111111111111111111111111111111);
        pub const eRaygenBitNv: Self = Self::eRaygenBitKhr;
        pub const eAnyHitBitNv: Self = Self::eAnyHitBitKhr;
        pub const eClosestHitBitNv: Self = Self::eClosestHitBitKhr;
        pub const eMissBitNv: Self = Self::eMissBitKhr;
        pub const eIntersectionBitNv: Self = Self::eIntersectionBitKhr;
        pub const eCallableBitNv: Self = Self::eCallableBitKhr;

        pub fn normalise(self) -> ShaderStageFlagBits {
            match self {
                Self::eVertexBit => ShaderStageFlagBits::eVertexBit,
                Self::eTessellationControlBit => ShaderStageFlagBits::eTessellationControlBit,
                Self::eTessellationEvaluationBit => ShaderStageFlagBits::eTessellationEvaluationBit,
                Self::eGeometryBit => ShaderStageFlagBits::eGeometryBit,
                Self::eFragmentBit => ShaderStageFlagBits::eFragmentBit,
                Self::eAllGraphics => ShaderStageFlagBits::eAllGraphics,
                Self::eComputeBit => ShaderStageFlagBits::eComputeBit,
                Self::eTaskBitExt => ShaderStageFlagBits::eTaskBitExt,
                Self::eMeshBitExt => ShaderStageFlagBits::eMeshBitExt,
                Self::eRaygenBitKhr => ShaderStageFlagBits::eRaygenBitKhr,
                Self::eAnyHitBitKhr => ShaderStageFlagBits::eAnyHitBitKhr,
                Self::eClosestHitBitKhr => ShaderStageFlagBits::eClosestHitBitKhr,
                Self::eMissBitKhr => ShaderStageFlagBits::eMissBitKhr,
                Self::eIntersectionBitKhr => ShaderStageFlagBits::eIntersectionBitKhr,
                Self::eCallableBitKhr => ShaderStageFlagBits::eCallableBitKhr,
                Self::eSubpassShadingBitHuawei => ShaderStageFlagBits::eSubpassShadingBitHuawei,
                Self::eClusterCullingBitHuawei => ShaderStageFlagBits::eClusterCullingBitHuawei,
                Self::eAll => ShaderStageFlagBits::eAll,
                RawShaderStageFlagBits(b) => ShaderStageFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<ShaderStageFlagBits> {
            match self {
                RawShaderStageFlagBits(0) => None,
                Self::eVertexBit => Some(ShaderStageFlagBits::eVertexBit),
                Self::eTessellationControlBit => Some(ShaderStageFlagBits::eTessellationControlBit),
                Self::eTessellationEvaluationBit => Some(ShaderStageFlagBits::eTessellationEvaluationBit),
                Self::eGeometryBit => Some(ShaderStageFlagBits::eGeometryBit),
                Self::eFragmentBit => Some(ShaderStageFlagBits::eFragmentBit),
                Self::eAllGraphics => Some(ShaderStageFlagBits::eAllGraphics),
                Self::eComputeBit => Some(ShaderStageFlagBits::eComputeBit),
                Self::eTaskBitExt => Some(ShaderStageFlagBits::eTaskBitExt),
                Self::eMeshBitExt => Some(ShaderStageFlagBits::eMeshBitExt),
                Self::eRaygenBitKhr => Some(ShaderStageFlagBits::eRaygenBitKhr),
                Self::eAnyHitBitKhr => Some(ShaderStageFlagBits::eAnyHitBitKhr),
                Self::eClosestHitBitKhr => Some(ShaderStageFlagBits::eClosestHitBitKhr),
                Self::eMissBitKhr => Some(ShaderStageFlagBits::eMissBitKhr),
                Self::eIntersectionBitKhr => Some(ShaderStageFlagBits::eIntersectionBitKhr),
                Self::eCallableBitKhr => Some(ShaderStageFlagBits::eCallableBitKhr),
                Self::eSubpassShadingBitHuawei => Some(ShaderStageFlagBits::eSubpassShadingBitHuawei),
                Self::eClusterCullingBitHuawei => Some(ShaderStageFlagBits::eClusterCullingBitHuawei),
                Self::eAll => Some(ShaderStageFlagBits::eAll),
                RawShaderStageFlagBits(b) => Some(ShaderStageFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawShaderStageFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawImageUsageFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawImageUsageFlagBits {
        pub const eTransferSrcBit: Self = Self(0b1);
        pub const eTransferDstBit: Self = Self(0b10);
        pub const eSampledBit: Self = Self(0b100);
        pub const eStorageBit: Self = Self(0b1000);
        pub const eColourAttachmentBit: Self = Self(0b10000);
        pub const eDepthStencilAttachmentBit: Self = Self(0b100000);
        pub const eTransientAttachmentBit: Self = Self(0b1000000);
        pub const eInputAttachmentBit: Self = Self(0b10000000);
        pub const eFragmentShadingRateAttachmentBitKhr: Self = Self(0b100000000);
        pub const eFragmentDensityMapBitExt: Self = Self(0b1000000000);
        pub const eVideoDecodeDstBitKhr: Self = Self(0b10000000000);
        pub const eVideoDecodeSrcBitKhr: Self = Self(0b100000000000);
        pub const eVideoDecodeDpbBitKhr: Self = Self(0b1000000000000);
        pub const eVideoEncodeDstBitKhr: Self = Self(0b10000000000000);
        pub const eVideoEncodeSrcBitKhr: Self = Self(0b100000000000000);
        pub const eVideoEncodeDpbBitKhr: Self = Self(0b1000000000000000);
        pub const eInvocationMaskBitHuawei: Self = Self(0b1000000000000000000);
        pub const eAttachmentFeedbackLoopBitExt: Self = Self(0b10000000000000000000);
        pub const eSampleWeightBitQcom: Self = Self(0b100000000000000000000);
        pub const eSampleBlockMatchBitQcom: Self = Self(0b1000000000000000000000);

        pub fn normalise(self) -> ImageUsageFlagBits {
            match self {
                Self::eTransferSrcBit => ImageUsageFlagBits::eTransferSrcBit,
                Self::eTransferDstBit => ImageUsageFlagBits::eTransferDstBit,
                Self::eSampledBit => ImageUsageFlagBits::eSampledBit,
                Self::eStorageBit => ImageUsageFlagBits::eStorageBit,
                Self::eColourAttachmentBit => ImageUsageFlagBits::eColourAttachmentBit,
                Self::eDepthStencilAttachmentBit => ImageUsageFlagBits::eDepthStencilAttachmentBit,
                Self::eTransientAttachmentBit => ImageUsageFlagBits::eTransientAttachmentBit,
                Self::eInputAttachmentBit => ImageUsageFlagBits::eInputAttachmentBit,
                Self::eFragmentShadingRateAttachmentBitKhr => ImageUsageFlagBits::eFragmentShadingRateAttachmentBitKhr,
                Self::eFragmentDensityMapBitExt => ImageUsageFlagBits::eFragmentDensityMapBitExt,
                Self::eVideoDecodeDstBitKhr => ImageUsageFlagBits::eVideoDecodeDstBitKhr,
                Self::eVideoDecodeSrcBitKhr => ImageUsageFlagBits::eVideoDecodeSrcBitKhr,
                Self::eVideoDecodeDpbBitKhr => ImageUsageFlagBits::eVideoDecodeDpbBitKhr,
                Self::eVideoEncodeDstBitKhr => ImageUsageFlagBits::eVideoEncodeDstBitKhr,
                Self::eVideoEncodeSrcBitKhr => ImageUsageFlagBits::eVideoEncodeSrcBitKhr,
                Self::eVideoEncodeDpbBitKhr => ImageUsageFlagBits::eVideoEncodeDpbBitKhr,
                Self::eInvocationMaskBitHuawei => ImageUsageFlagBits::eInvocationMaskBitHuawei,
                Self::eAttachmentFeedbackLoopBitExt => ImageUsageFlagBits::eAttachmentFeedbackLoopBitExt,
                Self::eSampleWeightBitQcom => ImageUsageFlagBits::eSampleWeightBitQcom,
                Self::eSampleBlockMatchBitQcom => ImageUsageFlagBits::eSampleBlockMatchBitQcom,
                RawImageUsageFlagBits(b) => ImageUsageFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<ImageUsageFlagBits> {
            match self {
                RawImageUsageFlagBits(0) => None,
                Self::eTransferSrcBit => Some(ImageUsageFlagBits::eTransferSrcBit),
                Self::eTransferDstBit => Some(ImageUsageFlagBits::eTransferDstBit),
                Self::eSampledBit => Some(ImageUsageFlagBits::eSampledBit),
                Self::eStorageBit => Some(ImageUsageFlagBits::eStorageBit),
                Self::eColourAttachmentBit => Some(ImageUsageFlagBits::eColourAttachmentBit),
                Self::eDepthStencilAttachmentBit => Some(ImageUsageFlagBits::eDepthStencilAttachmentBit),
                Self::eTransientAttachmentBit => Some(ImageUsageFlagBits::eTransientAttachmentBit),
                Self::eInputAttachmentBit => Some(ImageUsageFlagBits::eInputAttachmentBit),
                Self::eFragmentShadingRateAttachmentBitKhr => Some(ImageUsageFlagBits::eFragmentShadingRateAttachmentBitKhr),
                Self::eFragmentDensityMapBitExt => Some(ImageUsageFlagBits::eFragmentDensityMapBitExt),
                Self::eVideoDecodeDstBitKhr => Some(ImageUsageFlagBits::eVideoDecodeDstBitKhr),
                Self::eVideoDecodeSrcBitKhr => Some(ImageUsageFlagBits::eVideoDecodeSrcBitKhr),
                Self::eVideoDecodeDpbBitKhr => Some(ImageUsageFlagBits::eVideoDecodeDpbBitKhr),
                Self::eVideoEncodeDstBitKhr => Some(ImageUsageFlagBits::eVideoEncodeDstBitKhr),
                Self::eVideoEncodeSrcBitKhr => Some(ImageUsageFlagBits::eVideoEncodeSrcBitKhr),
                Self::eVideoEncodeDpbBitKhr => Some(ImageUsageFlagBits::eVideoEncodeDpbBitKhr),
                Self::eInvocationMaskBitHuawei => Some(ImageUsageFlagBits::eInvocationMaskBitHuawei),
                Self::eAttachmentFeedbackLoopBitExt => Some(ImageUsageFlagBits::eAttachmentFeedbackLoopBitExt),
                Self::eSampleWeightBitQcom => Some(ImageUsageFlagBits::eSampleWeightBitQcom),
                Self::eSampleBlockMatchBitQcom => Some(ImageUsageFlagBits::eSampleBlockMatchBitQcom),
                RawImageUsageFlagBits(b) => Some(ImageUsageFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawImageUsageFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawImageCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawImageCreateFlagBits {
        pub const eSparseBindingBit: Self = Self(0b1);
        pub const eSparseResidencyBit: Self = Self(0b10);
        pub const eSparseAliasedBit: Self = Self(0b100);
        pub const eMutableFormatBit: Self = Self(0b1000);
        pub const eCubeCompatibleBit: Self = Self(0b10000);
        pub const e2dArrayCompatibleBit: Self = Self(0b100000);
        pub const eSplitInstanceBindRegionsBit: Self = Self(0b1000000);
        pub const eBlockTexelViewCompatibleBit: Self = Self(0b10000000);
        pub const eExtendedUsageBit: Self = Self(0b100000000);
        pub const eDisjointBit: Self = Self(0b1000000000);
        pub const eAliasBit: Self = Self(0b10000000000);
        pub const eProtectedBit: Self = Self(0b100000000000);
        pub const eSampleLocationsCompatibleDepthBitExt: Self = Self(0b1000000000000);
        pub const eCornerSampledBitNv: Self = Self(0b10000000000000);
        pub const eSubsampledBitExt: Self = Self(0b100000000000000);
        pub const eFragmentDensityMapOffsetBitQcom: Self = Self(0b1000000000000000);
        pub const eDescriptorBufferCaptureReplayBitExt: Self = Self(0b10000000000000000);
        pub const e2dViewCompatibleBitExt: Self = Self(0b100000000000000000);
        pub const eMultisampledRenderToSingleSampledBitExt: Self = Self(0b1000000000000000000);
        pub const e2dArrayCompatibleBitKhr: Self = Self::e2dArrayCompatibleBit;
        pub const eSplitInstanceBindRegionsBitKhr: Self = Self::eSplitInstanceBindRegionsBit;
        pub const eBlockTexelViewCompatibleBitKhr: Self = Self::eBlockTexelViewCompatibleBit;
        pub const eExtendedUsageBitKhr: Self = Self::eExtendedUsageBit;
        pub const eDisjointBitKhr: Self = Self::eDisjointBit;
        pub const eAliasBitKhr: Self = Self::eAliasBit;

        pub fn normalise(self) -> ImageCreateFlagBits {
            match self {
                Self::eSparseBindingBit => ImageCreateFlagBits::eSparseBindingBit,
                Self::eSparseResidencyBit => ImageCreateFlagBits::eSparseResidencyBit,
                Self::eSparseAliasedBit => ImageCreateFlagBits::eSparseAliasedBit,
                Self::eMutableFormatBit => ImageCreateFlagBits::eMutableFormatBit,
                Self::eCubeCompatibleBit => ImageCreateFlagBits::eCubeCompatibleBit,
                Self::e2dArrayCompatibleBit => ImageCreateFlagBits::e2dArrayCompatibleBit,
                Self::eSplitInstanceBindRegionsBit => ImageCreateFlagBits::eSplitInstanceBindRegionsBit,
                Self::eBlockTexelViewCompatibleBit => ImageCreateFlagBits::eBlockTexelViewCompatibleBit,
                Self::eExtendedUsageBit => ImageCreateFlagBits::eExtendedUsageBit,
                Self::eDisjointBit => ImageCreateFlagBits::eDisjointBit,
                Self::eAliasBit => ImageCreateFlagBits::eAliasBit,
                Self::eProtectedBit => ImageCreateFlagBits::eProtectedBit,
                Self::eSampleLocationsCompatibleDepthBitExt => ImageCreateFlagBits::eSampleLocationsCompatibleDepthBitExt,
                Self::eCornerSampledBitNv => ImageCreateFlagBits::eCornerSampledBitNv,
                Self::eSubsampledBitExt => ImageCreateFlagBits::eSubsampledBitExt,
                Self::eFragmentDensityMapOffsetBitQcom => ImageCreateFlagBits::eFragmentDensityMapOffsetBitQcom,
                Self::eDescriptorBufferCaptureReplayBitExt => ImageCreateFlagBits::eDescriptorBufferCaptureReplayBitExt,
                Self::e2dViewCompatibleBitExt => ImageCreateFlagBits::e2dViewCompatibleBitExt,
                Self::eMultisampledRenderToSingleSampledBitExt => ImageCreateFlagBits::eMultisampledRenderToSingleSampledBitExt,
                RawImageCreateFlagBits(b) => ImageCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<ImageCreateFlagBits> {
            match self {
                RawImageCreateFlagBits(0) => None,
                Self::eSparseBindingBit => Some(ImageCreateFlagBits::eSparseBindingBit),
                Self::eSparseResidencyBit => Some(ImageCreateFlagBits::eSparseResidencyBit),
                Self::eSparseAliasedBit => Some(ImageCreateFlagBits::eSparseAliasedBit),
                Self::eMutableFormatBit => Some(ImageCreateFlagBits::eMutableFormatBit),
                Self::eCubeCompatibleBit => Some(ImageCreateFlagBits::eCubeCompatibleBit),
                Self::e2dArrayCompatibleBit => Some(ImageCreateFlagBits::e2dArrayCompatibleBit),
                Self::eSplitInstanceBindRegionsBit => Some(ImageCreateFlagBits::eSplitInstanceBindRegionsBit),
                Self::eBlockTexelViewCompatibleBit => Some(ImageCreateFlagBits::eBlockTexelViewCompatibleBit),
                Self::eExtendedUsageBit => Some(ImageCreateFlagBits::eExtendedUsageBit),
                Self::eDisjointBit => Some(ImageCreateFlagBits::eDisjointBit),
                Self::eAliasBit => Some(ImageCreateFlagBits::eAliasBit),
                Self::eProtectedBit => Some(ImageCreateFlagBits::eProtectedBit),
                Self::eSampleLocationsCompatibleDepthBitExt => Some(ImageCreateFlagBits::eSampleLocationsCompatibleDepthBitExt),
                Self::eCornerSampledBitNv => Some(ImageCreateFlagBits::eCornerSampledBitNv),
                Self::eSubsampledBitExt => Some(ImageCreateFlagBits::eSubsampledBitExt),
                Self::eFragmentDensityMapOffsetBitQcom => Some(ImageCreateFlagBits::eFragmentDensityMapOffsetBitQcom),
                Self::eDescriptorBufferCaptureReplayBitExt => Some(ImageCreateFlagBits::eDescriptorBufferCaptureReplayBitExt),
                Self::e2dViewCompatibleBitExt => Some(ImageCreateFlagBits::e2dViewCompatibleBitExt),
                Self::eMultisampledRenderToSingleSampledBitExt => Some(ImageCreateFlagBits::eMultisampledRenderToSingleSampledBitExt),
                RawImageCreateFlagBits(b) => Some(ImageCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawImageCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawImageViewCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawImageViewCreateFlagBits {
        pub const eFragmentDensityMapDynamicBitExt: Self = Self(0b1);
        pub const eFragmentDensityMapDeferredBitExt: Self = Self(0b10);
        pub const eDescriptorBufferCaptureReplayBitExt: Self = Self(0b100);

        pub fn normalise(self) -> ImageViewCreateFlagBits {
            match self {
                Self::eFragmentDensityMapDynamicBitExt => ImageViewCreateFlagBits::eFragmentDensityMapDynamicBitExt,
                Self::eFragmentDensityMapDeferredBitExt => ImageViewCreateFlagBits::eFragmentDensityMapDeferredBitExt,
                Self::eDescriptorBufferCaptureReplayBitExt => ImageViewCreateFlagBits::eDescriptorBufferCaptureReplayBitExt,
                RawImageViewCreateFlagBits(b) => ImageViewCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<ImageViewCreateFlagBits> {
            match self {
                RawImageViewCreateFlagBits(0) => None,
                Self::eFragmentDensityMapDynamicBitExt => Some(ImageViewCreateFlagBits::eFragmentDensityMapDynamicBitExt),
                Self::eFragmentDensityMapDeferredBitExt => Some(ImageViewCreateFlagBits::eFragmentDensityMapDeferredBitExt),
                Self::eDescriptorBufferCaptureReplayBitExt => Some(ImageViewCreateFlagBits::eDescriptorBufferCaptureReplayBitExt),
                RawImageViewCreateFlagBits(b) => Some(ImageViewCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawImageViewCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawPipelineCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawPipelineCreateFlagBits {
        pub const eDisableOptimizationBit: Self = Self(0b1);
        pub const eAllowDerivativesBit: Self = Self(0b10);
        pub const eDerivativeBit: Self = Self(0b100);
        pub const eViewIndexFromDeviceIndexBit: Self = Self(0b1000);
        pub const eDispatchBaseBit: Self = Self(0b10000);
        pub const eDeferCompileBitNv: Self = Self(0b100000);
        pub const eCaptureStatisticsBitKhr: Self = Self(0b1000000);
        pub const eCaptureInternalRepresentationsBitKhr: Self = Self(0b10000000);
        pub const eFailOnPipelineCompileRequiredBit: Self = Self(0b100000000);
        pub const eEarlyReturnOnFailureBit: Self = Self(0b1000000000);
        pub const eLinkTimeOptimizationBitExt: Self = Self(0b10000000000);
        pub const eLibraryBitKhr: Self = Self(0b100000000000);
        pub const eRayTracingSkipTrianglesBitKhr: Self = Self(0b1000000000000);
        pub const eRayTracingSkipAabbsBitKhr: Self = Self(0b10000000000000);
        pub const eRayTracingNoNullAnyHitShadersBitKhr: Self = Self(0b100000000000000);
        pub const eRayTracingNoNullClosestHitShadersBitKhr: Self = Self(0b1000000000000000);
        pub const eRayTracingNoNullMissShadersBitKhr: Self = Self(0b10000000000000000);
        pub const eRayTracingNoNullIntersectionShadersBitKhr: Self = Self(0b100000000000000000);
        pub const eIndirectBindableBitNv: Self = Self(0b1000000000000000000);
        pub const eRayTracingShaderGroupHandleCaptureReplayBitKhr: Self = Self(0b10000000000000000000);
        pub const eRayTracingAllowMotionBitNv: Self = Self(0b100000000000000000000);
        pub const eRenderingFragmentShadingRateAttachmentBitKhr: Self = Self(0b1000000000000000000000);
        pub const eRenderingFragmentDensityMapAttachmentBitExt: Self = Self(0b10000000000000000000000);
        pub const eRetainLinkTimeOptimizationInfoBitExt: Self = Self(0b100000000000000000000000);
        pub const eRayTracingOpacityMicromapBitExt: Self = Self(0b1000000000000000000000000);
        pub const eColourAttachmentFeedbackLoopBitExt: Self = Self(0b10000000000000000000000000);
        pub const eDepthStencilAttachmentFeedbackLoopBitExt: Self = Self(0b100000000000000000000000000);
        pub const eNoProtectedAccessBitExt: Self = Self(0b1000000000000000000000000000);
        pub const eDescriptorBufferBitExt: Self = Self(0b100000000000000000000000000000);
        pub const eProtectedAccessOnlyBitExt: Self = Self(0b1000000000000000000000000000000);
        pub const eViewIndexFromDeviceIndexBitKhr: Self = Self::eViewIndexFromDeviceIndexBit;
        pub const eDispatchBase: Self = Self::eDispatchBaseBit;
        pub const eFailOnPipelineCompileRequiredBitExt: Self = Self::eFailOnPipelineCompileRequiredBit;
        pub const eEarlyReturnOnFailureBitExt: Self = Self::eEarlyReturnOnFailureBit;
        pub const eRasterizationStateCreateFragmentShadingRateAttachmentBitKhr: Self = Self::eRenderingFragmentShadingRateAttachmentBitKhr;
        pub const eRasterizationStateCreateFragmentDensityMapAttachmentBitExt: Self = Self::eRenderingFragmentDensityMapAttachmentBitExt;

        pub fn normalise(self) -> PipelineCreateFlagBits {
            match self {
                Self::eDisableOptimizationBit => PipelineCreateFlagBits::eDisableOptimizationBit,
                Self::eAllowDerivativesBit => PipelineCreateFlagBits::eAllowDerivativesBit,
                Self::eDerivativeBit => PipelineCreateFlagBits::eDerivativeBit,
                Self::eViewIndexFromDeviceIndexBit => PipelineCreateFlagBits::eViewIndexFromDeviceIndexBit,
                Self::eDispatchBaseBit => PipelineCreateFlagBits::eDispatchBaseBit,
                Self::eDeferCompileBitNv => PipelineCreateFlagBits::eDeferCompileBitNv,
                Self::eCaptureStatisticsBitKhr => PipelineCreateFlagBits::eCaptureStatisticsBitKhr,
                Self::eCaptureInternalRepresentationsBitKhr => PipelineCreateFlagBits::eCaptureInternalRepresentationsBitKhr,
                Self::eFailOnPipelineCompileRequiredBit => PipelineCreateFlagBits::eFailOnPipelineCompileRequiredBit,
                Self::eEarlyReturnOnFailureBit => PipelineCreateFlagBits::eEarlyReturnOnFailureBit,
                Self::eLinkTimeOptimizationBitExt => PipelineCreateFlagBits::eLinkTimeOptimizationBitExt,
                Self::eLibraryBitKhr => PipelineCreateFlagBits::eLibraryBitKhr,
                Self::eRayTracingSkipTrianglesBitKhr => PipelineCreateFlagBits::eRayTracingSkipTrianglesBitKhr,
                Self::eRayTracingSkipAabbsBitKhr => PipelineCreateFlagBits::eRayTracingSkipAabbsBitKhr,
                Self::eRayTracingNoNullAnyHitShadersBitKhr => PipelineCreateFlagBits::eRayTracingNoNullAnyHitShadersBitKhr,
                Self::eRayTracingNoNullClosestHitShadersBitKhr => PipelineCreateFlagBits::eRayTracingNoNullClosestHitShadersBitKhr,
                Self::eRayTracingNoNullMissShadersBitKhr => PipelineCreateFlagBits::eRayTracingNoNullMissShadersBitKhr,
                Self::eRayTracingNoNullIntersectionShadersBitKhr => PipelineCreateFlagBits::eRayTracingNoNullIntersectionShadersBitKhr,
                Self::eIndirectBindableBitNv => PipelineCreateFlagBits::eIndirectBindableBitNv,
                Self::eRayTracingShaderGroupHandleCaptureReplayBitKhr => PipelineCreateFlagBits::eRayTracingShaderGroupHandleCaptureReplayBitKhr,
                Self::eRayTracingAllowMotionBitNv => PipelineCreateFlagBits::eRayTracingAllowMotionBitNv,
                Self::eRenderingFragmentShadingRateAttachmentBitKhr => PipelineCreateFlagBits::eRenderingFragmentShadingRateAttachmentBitKhr,
                Self::eRenderingFragmentDensityMapAttachmentBitExt => PipelineCreateFlagBits::eRenderingFragmentDensityMapAttachmentBitExt,
                Self::eRetainLinkTimeOptimizationInfoBitExt => PipelineCreateFlagBits::eRetainLinkTimeOptimizationInfoBitExt,
                Self::eRayTracingOpacityMicromapBitExt => PipelineCreateFlagBits::eRayTracingOpacityMicromapBitExt,
                Self::eColourAttachmentFeedbackLoopBitExt => PipelineCreateFlagBits::eColourAttachmentFeedbackLoopBitExt,
                Self::eDepthStencilAttachmentFeedbackLoopBitExt => PipelineCreateFlagBits::eDepthStencilAttachmentFeedbackLoopBitExt,
                Self::eNoProtectedAccessBitExt => PipelineCreateFlagBits::eNoProtectedAccessBitExt,
                Self::eDescriptorBufferBitExt => PipelineCreateFlagBits::eDescriptorBufferBitExt,
                Self::eProtectedAccessOnlyBitExt => PipelineCreateFlagBits::eProtectedAccessOnlyBitExt,
                RawPipelineCreateFlagBits(b) => PipelineCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<PipelineCreateFlagBits> {
            match self {
                RawPipelineCreateFlagBits(0) => None,
                Self::eDisableOptimizationBit => Some(PipelineCreateFlagBits::eDisableOptimizationBit),
                Self::eAllowDerivativesBit => Some(PipelineCreateFlagBits::eAllowDerivativesBit),
                Self::eDerivativeBit => Some(PipelineCreateFlagBits::eDerivativeBit),
                Self::eViewIndexFromDeviceIndexBit => Some(PipelineCreateFlagBits::eViewIndexFromDeviceIndexBit),
                Self::eDispatchBaseBit => Some(PipelineCreateFlagBits::eDispatchBaseBit),
                Self::eDeferCompileBitNv => Some(PipelineCreateFlagBits::eDeferCompileBitNv),
                Self::eCaptureStatisticsBitKhr => Some(PipelineCreateFlagBits::eCaptureStatisticsBitKhr),
                Self::eCaptureInternalRepresentationsBitKhr => Some(PipelineCreateFlagBits::eCaptureInternalRepresentationsBitKhr),
                Self::eFailOnPipelineCompileRequiredBit => Some(PipelineCreateFlagBits::eFailOnPipelineCompileRequiredBit),
                Self::eEarlyReturnOnFailureBit => Some(PipelineCreateFlagBits::eEarlyReturnOnFailureBit),
                Self::eLinkTimeOptimizationBitExt => Some(PipelineCreateFlagBits::eLinkTimeOptimizationBitExt),
                Self::eLibraryBitKhr => Some(PipelineCreateFlagBits::eLibraryBitKhr),
                Self::eRayTracingSkipTrianglesBitKhr => Some(PipelineCreateFlagBits::eRayTracingSkipTrianglesBitKhr),
                Self::eRayTracingSkipAabbsBitKhr => Some(PipelineCreateFlagBits::eRayTracingSkipAabbsBitKhr),
                Self::eRayTracingNoNullAnyHitShadersBitKhr => Some(PipelineCreateFlagBits::eRayTracingNoNullAnyHitShadersBitKhr),
                Self::eRayTracingNoNullClosestHitShadersBitKhr => Some(PipelineCreateFlagBits::eRayTracingNoNullClosestHitShadersBitKhr),
                Self::eRayTracingNoNullMissShadersBitKhr => Some(PipelineCreateFlagBits::eRayTracingNoNullMissShadersBitKhr),
                Self::eRayTracingNoNullIntersectionShadersBitKhr => Some(PipelineCreateFlagBits::eRayTracingNoNullIntersectionShadersBitKhr),
                Self::eIndirectBindableBitNv => Some(PipelineCreateFlagBits::eIndirectBindableBitNv),
                Self::eRayTracingShaderGroupHandleCaptureReplayBitKhr => Some(PipelineCreateFlagBits::eRayTracingShaderGroupHandleCaptureReplayBitKhr),
                Self::eRayTracingAllowMotionBitNv => Some(PipelineCreateFlagBits::eRayTracingAllowMotionBitNv),
                Self::eRenderingFragmentShadingRateAttachmentBitKhr => Some(PipelineCreateFlagBits::eRenderingFragmentShadingRateAttachmentBitKhr),
                Self::eRenderingFragmentDensityMapAttachmentBitExt => Some(PipelineCreateFlagBits::eRenderingFragmentDensityMapAttachmentBitExt),
                Self::eRetainLinkTimeOptimizationInfoBitExt => Some(PipelineCreateFlagBits::eRetainLinkTimeOptimizationInfoBitExt),
                Self::eRayTracingOpacityMicromapBitExt => Some(PipelineCreateFlagBits::eRayTracingOpacityMicromapBitExt),
                Self::eColourAttachmentFeedbackLoopBitExt => Some(PipelineCreateFlagBits::eColourAttachmentFeedbackLoopBitExt),
                Self::eDepthStencilAttachmentFeedbackLoopBitExt => Some(PipelineCreateFlagBits::eDepthStencilAttachmentFeedbackLoopBitExt),
                Self::eNoProtectedAccessBitExt => Some(PipelineCreateFlagBits::eNoProtectedAccessBitExt),
                Self::eDescriptorBufferBitExt => Some(PipelineCreateFlagBits::eDescriptorBufferBitExt),
                Self::eProtectedAccessOnlyBitExt => Some(PipelineCreateFlagBits::eProtectedAccessOnlyBitExt),
                RawPipelineCreateFlagBits(b) => Some(PipelineCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawPipelineCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawColourComponentFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawColourComponentFlagBits {
        pub const eRBit: Self = Self(0b1);
        pub const eGBit: Self = Self(0b10);
        pub const eBBit: Self = Self(0b100);
        pub const eABit: Self = Self(0b1000);

        pub fn normalise(self) -> ColourComponentFlagBits {
            match self {
                Self::eRBit => ColourComponentFlagBits::eRBit,
                Self::eGBit => ColourComponentFlagBits::eGBit,
                Self::eBBit => ColourComponentFlagBits::eBBit,
                Self::eABit => ColourComponentFlagBits::eABit,
                RawColourComponentFlagBits(b) => ColourComponentFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<ColourComponentFlagBits> {
            match self {
                RawColourComponentFlagBits(0) => None,
                Self::eRBit => Some(ColourComponentFlagBits::eRBit),
                Self::eGBit => Some(ColourComponentFlagBits::eGBit),
                Self::eBBit => Some(ColourComponentFlagBits::eBBit),
                Self::eABit => Some(ColourComponentFlagBits::eABit),
                RawColourComponentFlagBits(b) => Some(ColourComponentFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawColourComponentFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawFenceCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawFenceCreateFlagBits {
        pub const eSignaledBit: Self = Self(0b1);

        pub fn normalise(self) -> FenceCreateFlagBits {
            match self {
                Self::eSignaledBit => FenceCreateFlagBits::eSignaledBit,
                RawFenceCreateFlagBits(b) => FenceCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<FenceCreateFlagBits> {
            match self {
                RawFenceCreateFlagBits(0) => None,
                Self::eSignaledBit => Some(FenceCreateFlagBits::eSignaledBit),
                RawFenceCreateFlagBits(b) => Some(FenceCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawFenceCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawFormatFeatureFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawFormatFeatureFlagBits {
        pub const eSampledImageBit: Self = Self(0b1);
        pub const eStorageImageBit: Self = Self(0b10);
        pub const eStorageImageAtomicBit: Self = Self(0b100);
        pub const eUniformTexelBufferBit: Self = Self(0b1000);
        pub const eStorageTexelBufferBit: Self = Self(0b10000);
        pub const eStorageTexelBufferAtomicBit: Self = Self(0b100000);
        pub const eVertexBufferBit: Self = Self(0b1000000);
        pub const eColourAttachmentBit: Self = Self(0b10000000);
        pub const eColourAttachmentBlendBit: Self = Self(0b100000000);
        pub const eDepthStencilAttachmentBit: Self = Self(0b1000000000);
        pub const eBlitSrcBit: Self = Self(0b10000000000);
        pub const eBlitDstBit: Self = Self(0b100000000000);
        pub const eSampledImageFilterLinearBit: Self = Self(0b1000000000000);
        pub const eSampledImageFilterCubicBitExt: Self = Self(0b10000000000000);
        pub const eTransferSrcBit: Self = Self(0b100000000000000);
        pub const eTransferDstBit: Self = Self(0b1000000000000000);
        pub const eSampledImageFilterMinmaxBit: Self = Self(0b10000000000000000);
        pub const eMidpointChromaSamplesBit: Self = Self(0b100000000000000000);
        pub const eSampledImageYcbcrConversionLinearFilterBit: Self = Self(0b1000000000000000000);
        pub const eSampledImageYcbcrConversionSeparateReconstructionFilterBit: Self = Self(0b10000000000000000000);
        pub const eSampledImageYcbcrConversionChromaReconstructionExplicitBit: Self = Self(0b100000000000000000000);
        pub const eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit: Self = Self(0b1000000000000000000000);
        pub const eDisjointBit: Self = Self(0b10000000000000000000000);
        pub const eCositedChromaSamplesBit: Self = Self(0b100000000000000000000000);
        pub const eFragmentDensityMapBitExt: Self = Self(0b1000000000000000000000000);
        pub const eVideoDecodeOutputBitKhr: Self = Self(0b10000000000000000000000000);
        pub const eVideoDecodeDpbBitKhr: Self = Self(0b100000000000000000000000000);
        pub const eVideoEncodeInputBitKhr: Self = Self(0b1000000000000000000000000000);
        pub const eVideoEncodeDpbBitKhr: Self = Self(0b10000000000000000000000000000);
        pub const eAccelerationStructureVertexBufferBitKhr: Self = Self(0b100000000000000000000000000000);
        pub const eFragmentShadingRateAttachmentBitKhr: Self = Self(0b1000000000000000000000000000000);
        pub const eTransferSrcBitKhr: Self = Self::eTransferSrcBit;
        pub const eTransferDstBitKhr: Self = Self::eTransferDstBit;
        pub const eSampledImageFilterMinmaxBitExt: Self = Self::eSampledImageFilterMinmaxBit;
        pub const eMidpointChromaSamplesBitKhr: Self = Self::eMidpointChromaSamplesBit;
        pub const eSampledImageYcbcrConversionLinearFilterBitKhr: Self = Self::eSampledImageYcbcrConversionLinearFilterBit;
        pub const eSampledImageYcbcrConversionSeparateReconstructionFilterBitKhr: Self = Self::eSampledImageYcbcrConversionSeparateReconstructionFilterBit;
        pub const eSampledImageYcbcrConversionChromaReconstructionExplicitBitKhr: Self = Self::eSampledImageYcbcrConversionChromaReconstructionExplicitBit;
        pub const eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBitKhr: Self = Self::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit;
        pub const eDisjointBitKhr: Self = Self::eDisjointBit;
        pub const eCositedChromaSamplesBitKhr: Self = Self::eCositedChromaSamplesBit;

        pub fn normalise(self) -> FormatFeatureFlagBits {
            match self {
                Self::eSampledImageBit => FormatFeatureFlagBits::eSampledImageBit,
                Self::eStorageImageBit => FormatFeatureFlagBits::eStorageImageBit,
                Self::eStorageImageAtomicBit => FormatFeatureFlagBits::eStorageImageAtomicBit,
                Self::eUniformTexelBufferBit => FormatFeatureFlagBits::eUniformTexelBufferBit,
                Self::eStorageTexelBufferBit => FormatFeatureFlagBits::eStorageTexelBufferBit,
                Self::eStorageTexelBufferAtomicBit => FormatFeatureFlagBits::eStorageTexelBufferAtomicBit,
                Self::eVertexBufferBit => FormatFeatureFlagBits::eVertexBufferBit,
                Self::eColourAttachmentBit => FormatFeatureFlagBits::eColourAttachmentBit,
                Self::eColourAttachmentBlendBit => FormatFeatureFlagBits::eColourAttachmentBlendBit,
                Self::eDepthStencilAttachmentBit => FormatFeatureFlagBits::eDepthStencilAttachmentBit,
                Self::eBlitSrcBit => FormatFeatureFlagBits::eBlitSrcBit,
                Self::eBlitDstBit => FormatFeatureFlagBits::eBlitDstBit,
                Self::eSampledImageFilterLinearBit => FormatFeatureFlagBits::eSampledImageFilterLinearBit,
                Self::eSampledImageFilterCubicBitExt => FormatFeatureFlagBits::eSampledImageFilterCubicBitExt,
                Self::eTransferSrcBit => FormatFeatureFlagBits::eTransferSrcBit,
                Self::eTransferDstBit => FormatFeatureFlagBits::eTransferDstBit,
                Self::eSampledImageFilterMinmaxBit => FormatFeatureFlagBits::eSampledImageFilterMinmaxBit,
                Self::eMidpointChromaSamplesBit => FormatFeatureFlagBits::eMidpointChromaSamplesBit,
                Self::eSampledImageYcbcrConversionLinearFilterBit => FormatFeatureFlagBits::eSampledImageYcbcrConversionLinearFilterBit,
                Self::eSampledImageYcbcrConversionSeparateReconstructionFilterBit => FormatFeatureFlagBits::eSampledImageYcbcrConversionSeparateReconstructionFilterBit,
                Self::eSampledImageYcbcrConversionChromaReconstructionExplicitBit => FormatFeatureFlagBits::eSampledImageYcbcrConversionChromaReconstructionExplicitBit,
                Self::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit => FormatFeatureFlagBits::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit,
                Self::eDisjointBit => FormatFeatureFlagBits::eDisjointBit,
                Self::eCositedChromaSamplesBit => FormatFeatureFlagBits::eCositedChromaSamplesBit,
                Self::eFragmentDensityMapBitExt => FormatFeatureFlagBits::eFragmentDensityMapBitExt,
                Self::eVideoDecodeOutputBitKhr => FormatFeatureFlagBits::eVideoDecodeOutputBitKhr,
                Self::eVideoDecodeDpbBitKhr => FormatFeatureFlagBits::eVideoDecodeDpbBitKhr,
                Self::eVideoEncodeInputBitKhr => FormatFeatureFlagBits::eVideoEncodeInputBitKhr,
                Self::eVideoEncodeDpbBitKhr => FormatFeatureFlagBits::eVideoEncodeDpbBitKhr,
                Self::eAccelerationStructureVertexBufferBitKhr => FormatFeatureFlagBits::eAccelerationStructureVertexBufferBitKhr,
                Self::eFragmentShadingRateAttachmentBitKhr => FormatFeatureFlagBits::eFragmentShadingRateAttachmentBitKhr,
                RawFormatFeatureFlagBits(b) => FormatFeatureFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<FormatFeatureFlagBits> {
            match self {
                RawFormatFeatureFlagBits(0) => None,
                Self::eSampledImageBit => Some(FormatFeatureFlagBits::eSampledImageBit),
                Self::eStorageImageBit => Some(FormatFeatureFlagBits::eStorageImageBit),
                Self::eStorageImageAtomicBit => Some(FormatFeatureFlagBits::eStorageImageAtomicBit),
                Self::eUniformTexelBufferBit => Some(FormatFeatureFlagBits::eUniformTexelBufferBit),
                Self::eStorageTexelBufferBit => Some(FormatFeatureFlagBits::eStorageTexelBufferBit),
                Self::eStorageTexelBufferAtomicBit => Some(FormatFeatureFlagBits::eStorageTexelBufferAtomicBit),
                Self::eVertexBufferBit => Some(FormatFeatureFlagBits::eVertexBufferBit),
                Self::eColourAttachmentBit => Some(FormatFeatureFlagBits::eColourAttachmentBit),
                Self::eColourAttachmentBlendBit => Some(FormatFeatureFlagBits::eColourAttachmentBlendBit),
                Self::eDepthStencilAttachmentBit => Some(FormatFeatureFlagBits::eDepthStencilAttachmentBit),
                Self::eBlitSrcBit => Some(FormatFeatureFlagBits::eBlitSrcBit),
                Self::eBlitDstBit => Some(FormatFeatureFlagBits::eBlitDstBit),
                Self::eSampledImageFilterLinearBit => Some(FormatFeatureFlagBits::eSampledImageFilterLinearBit),
                Self::eSampledImageFilterCubicBitExt => Some(FormatFeatureFlagBits::eSampledImageFilterCubicBitExt),
                Self::eTransferSrcBit => Some(FormatFeatureFlagBits::eTransferSrcBit),
                Self::eTransferDstBit => Some(FormatFeatureFlagBits::eTransferDstBit),
                Self::eSampledImageFilterMinmaxBit => Some(FormatFeatureFlagBits::eSampledImageFilterMinmaxBit),
                Self::eMidpointChromaSamplesBit => Some(FormatFeatureFlagBits::eMidpointChromaSamplesBit),
                Self::eSampledImageYcbcrConversionLinearFilterBit => Some(FormatFeatureFlagBits::eSampledImageYcbcrConversionLinearFilterBit),
                Self::eSampledImageYcbcrConversionSeparateReconstructionFilterBit => Some(FormatFeatureFlagBits::eSampledImageYcbcrConversionSeparateReconstructionFilterBit),
                Self::eSampledImageYcbcrConversionChromaReconstructionExplicitBit => Some(FormatFeatureFlagBits::eSampledImageYcbcrConversionChromaReconstructionExplicitBit),
                Self::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit => Some(FormatFeatureFlagBits::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit),
                Self::eDisjointBit => Some(FormatFeatureFlagBits::eDisjointBit),
                Self::eCositedChromaSamplesBit => Some(FormatFeatureFlagBits::eCositedChromaSamplesBit),
                Self::eFragmentDensityMapBitExt => Some(FormatFeatureFlagBits::eFragmentDensityMapBitExt),
                Self::eVideoDecodeOutputBitKhr => Some(FormatFeatureFlagBits::eVideoDecodeOutputBitKhr),
                Self::eVideoDecodeDpbBitKhr => Some(FormatFeatureFlagBits::eVideoDecodeDpbBitKhr),
                Self::eVideoEncodeInputBitKhr => Some(FormatFeatureFlagBits::eVideoEncodeInputBitKhr),
                Self::eVideoEncodeDpbBitKhr => Some(FormatFeatureFlagBits::eVideoEncodeDpbBitKhr),
                Self::eAccelerationStructureVertexBufferBitKhr => Some(FormatFeatureFlagBits::eAccelerationStructureVertexBufferBitKhr),
                Self::eFragmentShadingRateAttachmentBitKhr => Some(FormatFeatureFlagBits::eFragmentShadingRateAttachmentBitKhr),
                RawFormatFeatureFlagBits(b) => Some(FormatFeatureFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawFormatFeatureFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawQueryControlFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawQueryControlFlagBits {
        pub const ePreciseBit: Self = Self(0b1);

        pub fn normalise(self) -> QueryControlFlagBits {
            match self {
                Self::ePreciseBit => QueryControlFlagBits::ePreciseBit,
                RawQueryControlFlagBits(b) => QueryControlFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<QueryControlFlagBits> {
            match self {
                RawQueryControlFlagBits(0) => None,
                Self::ePreciseBit => Some(QueryControlFlagBits::ePreciseBit),
                RawQueryControlFlagBits(b) => Some(QueryControlFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawQueryControlFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawQueryResultFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawQueryResultFlagBits {
        pub const e64Bit: Self = Self(0b1);
        pub const eWaitBit: Self = Self(0b10);
        pub const eWithAvailabilityBit: Self = Self(0b100);
        pub const ePartialBit: Self = Self(0b1000);
        pub const eWithStatusBitKhr: Self = Self(0b10000);

        pub fn normalise(self) -> QueryResultFlagBits {
            match self {
                Self::e64Bit => QueryResultFlagBits::e64Bit,
                Self::eWaitBit => QueryResultFlagBits::eWaitBit,
                Self::eWithAvailabilityBit => QueryResultFlagBits::eWithAvailabilityBit,
                Self::ePartialBit => QueryResultFlagBits::ePartialBit,
                Self::eWithStatusBitKhr => QueryResultFlagBits::eWithStatusBitKhr,
                RawQueryResultFlagBits(b) => QueryResultFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<QueryResultFlagBits> {
            match self {
                RawQueryResultFlagBits(0) => None,
                Self::e64Bit => Some(QueryResultFlagBits::e64Bit),
                Self::eWaitBit => Some(QueryResultFlagBits::eWaitBit),
                Self::eWithAvailabilityBit => Some(QueryResultFlagBits::eWithAvailabilityBit),
                Self::ePartialBit => Some(QueryResultFlagBits::ePartialBit),
                Self::eWithStatusBitKhr => Some(QueryResultFlagBits::eWithStatusBitKhr),
                RawQueryResultFlagBits(b) => Some(QueryResultFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawQueryResultFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawEventCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawEventCreateFlagBits {
        pub const eDeviceOnlyBit: Self = Self(0b1);
        pub const eDeviceOnlyBitKhr: Self = Self::eDeviceOnlyBit;

        pub fn normalise(self) -> EventCreateFlagBits {
            match self {
                Self::eDeviceOnlyBit => EventCreateFlagBits::eDeviceOnlyBit,
                RawEventCreateFlagBits(b) => EventCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<EventCreateFlagBits> {
            match self {
                RawEventCreateFlagBits(0) => None,
                Self::eDeviceOnlyBit => Some(EventCreateFlagBits::eDeviceOnlyBit),
                RawEventCreateFlagBits(b) => Some(EventCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawEventCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawCommandPoolCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawCommandPoolCreateFlagBits {
        pub const eTransientBit: Self = Self(0b1);
        pub const eResetCommandBufferBit: Self = Self(0b10);
        pub const eProtectedBit: Self = Self(0b100);

        pub fn normalise(self) -> CommandPoolCreateFlagBits {
            match self {
                Self::eTransientBit => CommandPoolCreateFlagBits::eTransientBit,
                Self::eResetCommandBufferBit => CommandPoolCreateFlagBits::eResetCommandBufferBit,
                Self::eProtectedBit => CommandPoolCreateFlagBits::eProtectedBit,
                RawCommandPoolCreateFlagBits(b) => CommandPoolCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<CommandPoolCreateFlagBits> {
            match self {
                RawCommandPoolCreateFlagBits(0) => None,
                Self::eTransientBit => Some(CommandPoolCreateFlagBits::eTransientBit),
                Self::eResetCommandBufferBit => Some(CommandPoolCreateFlagBits::eResetCommandBufferBit),
                Self::eProtectedBit => Some(CommandPoolCreateFlagBits::eProtectedBit),
                RawCommandPoolCreateFlagBits(b) => Some(CommandPoolCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawCommandPoolCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawCommandPoolResetFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawCommandPoolResetFlagBits {
        pub const eReleaseResourcesBit: Self = Self(0b1);

        pub fn normalise(self) -> CommandPoolResetFlagBits {
            match self {
                Self::eReleaseResourcesBit => CommandPoolResetFlagBits::eReleaseResourcesBit,
                RawCommandPoolResetFlagBits(b) => CommandPoolResetFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<CommandPoolResetFlagBits> {
            match self {
                RawCommandPoolResetFlagBits(0) => None,
                Self::eReleaseResourcesBit => Some(CommandPoolResetFlagBits::eReleaseResourcesBit),
                RawCommandPoolResetFlagBits(b) => Some(CommandPoolResetFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawCommandPoolResetFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawCommandBufferResetFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawCommandBufferResetFlagBits {
        pub const eReleaseResourcesBit: Self = Self(0b1);

        pub fn normalise(self) -> CommandBufferResetFlagBits {
            match self {
                Self::eReleaseResourcesBit => CommandBufferResetFlagBits::eReleaseResourcesBit,
                RawCommandBufferResetFlagBits(b) => CommandBufferResetFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<CommandBufferResetFlagBits> {
            match self {
                RawCommandBufferResetFlagBits(0) => None,
                Self::eReleaseResourcesBit => Some(CommandBufferResetFlagBits::eReleaseResourcesBit),
                RawCommandBufferResetFlagBits(b) => Some(CommandBufferResetFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawCommandBufferResetFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawCommandBufferUsageFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawCommandBufferUsageFlagBits {
        pub const eOneTimeSubmitBit: Self = Self(0b1);
        pub const eRenderPassContinueBit: Self = Self(0b10);
        pub const eSimultaneousUseBit: Self = Self(0b100);

        pub fn normalise(self) -> CommandBufferUsageFlagBits {
            match self {
                Self::eOneTimeSubmitBit => CommandBufferUsageFlagBits::eOneTimeSubmitBit,
                Self::eRenderPassContinueBit => CommandBufferUsageFlagBits::eRenderPassContinueBit,
                Self::eSimultaneousUseBit => CommandBufferUsageFlagBits::eSimultaneousUseBit,
                RawCommandBufferUsageFlagBits(b) => CommandBufferUsageFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<CommandBufferUsageFlagBits> {
            match self {
                RawCommandBufferUsageFlagBits(0) => None,
                Self::eOneTimeSubmitBit => Some(CommandBufferUsageFlagBits::eOneTimeSubmitBit),
                Self::eRenderPassContinueBit => Some(CommandBufferUsageFlagBits::eRenderPassContinueBit),
                Self::eSimultaneousUseBit => Some(CommandBufferUsageFlagBits::eSimultaneousUseBit),
                RawCommandBufferUsageFlagBits(b) => Some(CommandBufferUsageFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawCommandBufferUsageFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawQueryPipelineStatisticFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawQueryPipelineStatisticFlagBits {
        pub const eInputAssemblyVerticesBit: Self = Self(0b1);
        pub const eInputAssemblyPrimitivesBit: Self = Self(0b10);
        pub const eVertexShaderInvocationsBit: Self = Self(0b100);
        pub const eGeometryShaderInvocationsBit: Self = Self(0b1000);
        pub const eGeometryShaderPrimitivesBit: Self = Self(0b10000);
        pub const eClippingInvocationsBit: Self = Self(0b100000);
        pub const eClippingPrimitivesBit: Self = Self(0b1000000);
        pub const eFragmentShaderInvocationsBit: Self = Self(0b10000000);
        pub const eTessellationControlShaderPatchesBit: Self = Self(0b100000000);
        pub const eTessellationEvaluationShaderInvocationsBit: Self = Self(0b1000000000);
        pub const eComputeShaderInvocationsBit: Self = Self(0b10000000000);
        pub const eTaskShaderInvocationsBitExt: Self = Self(0b100000000000);
        pub const eMeshShaderInvocationsBitExt: Self = Self(0b1000000000000);
        pub const eClusterCullingShaderInvocationsBitHuawei: Self = Self(0b10000000000000);

        pub fn normalise(self) -> QueryPipelineStatisticFlagBits {
            match self {
                Self::eInputAssemblyVerticesBit => QueryPipelineStatisticFlagBits::eInputAssemblyVerticesBit,
                Self::eInputAssemblyPrimitivesBit => QueryPipelineStatisticFlagBits::eInputAssemblyPrimitivesBit,
                Self::eVertexShaderInvocationsBit => QueryPipelineStatisticFlagBits::eVertexShaderInvocationsBit,
                Self::eGeometryShaderInvocationsBit => QueryPipelineStatisticFlagBits::eGeometryShaderInvocationsBit,
                Self::eGeometryShaderPrimitivesBit => QueryPipelineStatisticFlagBits::eGeometryShaderPrimitivesBit,
                Self::eClippingInvocationsBit => QueryPipelineStatisticFlagBits::eClippingInvocationsBit,
                Self::eClippingPrimitivesBit => QueryPipelineStatisticFlagBits::eClippingPrimitivesBit,
                Self::eFragmentShaderInvocationsBit => QueryPipelineStatisticFlagBits::eFragmentShaderInvocationsBit,
                Self::eTessellationControlShaderPatchesBit => QueryPipelineStatisticFlagBits::eTessellationControlShaderPatchesBit,
                Self::eTessellationEvaluationShaderInvocationsBit => QueryPipelineStatisticFlagBits::eTessellationEvaluationShaderInvocationsBit,
                Self::eComputeShaderInvocationsBit => QueryPipelineStatisticFlagBits::eComputeShaderInvocationsBit,
                Self::eTaskShaderInvocationsBitExt => QueryPipelineStatisticFlagBits::eTaskShaderInvocationsBitExt,
                Self::eMeshShaderInvocationsBitExt => QueryPipelineStatisticFlagBits::eMeshShaderInvocationsBitExt,
                Self::eClusterCullingShaderInvocationsBitHuawei => QueryPipelineStatisticFlagBits::eClusterCullingShaderInvocationsBitHuawei,
                RawQueryPipelineStatisticFlagBits(b) => QueryPipelineStatisticFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<QueryPipelineStatisticFlagBits> {
            match self {
                RawQueryPipelineStatisticFlagBits(0) => None,
                Self::eInputAssemblyVerticesBit => Some(QueryPipelineStatisticFlagBits::eInputAssemblyVerticesBit),
                Self::eInputAssemblyPrimitivesBit => Some(QueryPipelineStatisticFlagBits::eInputAssemblyPrimitivesBit),
                Self::eVertexShaderInvocationsBit => Some(QueryPipelineStatisticFlagBits::eVertexShaderInvocationsBit),
                Self::eGeometryShaderInvocationsBit => Some(QueryPipelineStatisticFlagBits::eGeometryShaderInvocationsBit),
                Self::eGeometryShaderPrimitivesBit => Some(QueryPipelineStatisticFlagBits::eGeometryShaderPrimitivesBit),
                Self::eClippingInvocationsBit => Some(QueryPipelineStatisticFlagBits::eClippingInvocationsBit),
                Self::eClippingPrimitivesBit => Some(QueryPipelineStatisticFlagBits::eClippingPrimitivesBit),
                Self::eFragmentShaderInvocationsBit => Some(QueryPipelineStatisticFlagBits::eFragmentShaderInvocationsBit),
                Self::eTessellationControlShaderPatchesBit => Some(QueryPipelineStatisticFlagBits::eTessellationControlShaderPatchesBit),
                Self::eTessellationEvaluationShaderInvocationsBit => Some(QueryPipelineStatisticFlagBits::eTessellationEvaluationShaderInvocationsBit),
                Self::eComputeShaderInvocationsBit => Some(QueryPipelineStatisticFlagBits::eComputeShaderInvocationsBit),
                Self::eTaskShaderInvocationsBitExt => Some(QueryPipelineStatisticFlagBits::eTaskShaderInvocationsBitExt),
                Self::eMeshShaderInvocationsBitExt => Some(QueryPipelineStatisticFlagBits::eMeshShaderInvocationsBitExt),
                Self::eClusterCullingShaderInvocationsBitHuawei => Some(QueryPipelineStatisticFlagBits::eClusterCullingShaderInvocationsBitHuawei),
                RawQueryPipelineStatisticFlagBits(b) => Some(QueryPipelineStatisticFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawQueryPipelineStatisticFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawImageAspectFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawImageAspectFlagBits {
        pub const eNone: Self = Self(0);
        pub const eColourBit: Self = Self(0b1);
        pub const eDepthBit: Self = Self(0b10);
        pub const eStencilBit: Self = Self(0b100);
        pub const eMetadataBit: Self = Self(0b1000);
        pub const ePlane0Bit: Self = Self(0b10000);
        pub const ePlane1Bit: Self = Self(0b100000);
        pub const ePlane2Bit: Self = Self(0b1000000);
        pub const eMemoryPlane0BitExt: Self = Self(0b10000000);
        pub const eMemoryPlane1BitExt: Self = Self(0b100000000);
        pub const eMemoryPlane2BitExt: Self = Self(0b1000000000);
        pub const eMemoryPlane3BitExt: Self = Self(0b10000000000);
        pub const eNoneKhr: Self = Self::eNone;
        pub const ePlane0BitKhr: Self = Self::ePlane0Bit;
        pub const ePlane1BitKhr: Self = Self::ePlane1Bit;
        pub const ePlane2BitKhr: Self = Self::ePlane2Bit;

        pub fn normalise(self) -> ImageAspectFlagBits {
            match self {
                Self::eNone => ImageAspectFlagBits::eNone,
                Self::eColourBit => ImageAspectFlagBits::eColourBit,
                Self::eDepthBit => ImageAspectFlagBits::eDepthBit,
                Self::eStencilBit => ImageAspectFlagBits::eStencilBit,
                Self::eMetadataBit => ImageAspectFlagBits::eMetadataBit,
                Self::ePlane0Bit => ImageAspectFlagBits::ePlane0Bit,
                Self::ePlane1Bit => ImageAspectFlagBits::ePlane1Bit,
                Self::ePlane2Bit => ImageAspectFlagBits::ePlane2Bit,
                Self::eMemoryPlane0BitExt => ImageAspectFlagBits::eMemoryPlane0BitExt,
                Self::eMemoryPlane1BitExt => ImageAspectFlagBits::eMemoryPlane1BitExt,
                Self::eMemoryPlane2BitExt => ImageAspectFlagBits::eMemoryPlane2BitExt,
                Self::eMemoryPlane3BitExt => ImageAspectFlagBits::eMemoryPlane3BitExt,
                RawImageAspectFlagBits(b) => ImageAspectFlagBits::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawImageAspectFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawSparseMemoryBindFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawSparseMemoryBindFlagBits {
        pub const eMetadataBit: Self = Self(0b1);

        pub fn normalise(self) -> SparseMemoryBindFlagBits {
            match self {
                Self::eMetadataBit => SparseMemoryBindFlagBits::eMetadataBit,
                RawSparseMemoryBindFlagBits(b) => SparseMemoryBindFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<SparseMemoryBindFlagBits> {
            match self {
                RawSparseMemoryBindFlagBits(0) => None,
                Self::eMetadataBit => Some(SparseMemoryBindFlagBits::eMetadataBit),
                RawSparseMemoryBindFlagBits(b) => Some(SparseMemoryBindFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawSparseMemoryBindFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawSparseImageFormatFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawSparseImageFormatFlagBits {
        pub const eSingleMiptailBit: Self = Self(0b1);
        pub const eAlignedMipSizeBit: Self = Self(0b10);
        pub const eNonstandardBlockSizeBit: Self = Self(0b100);

        pub fn normalise(self) -> SparseImageFormatFlagBits {
            match self {
                Self::eSingleMiptailBit => SparseImageFormatFlagBits::eSingleMiptailBit,
                Self::eAlignedMipSizeBit => SparseImageFormatFlagBits::eAlignedMipSizeBit,
                Self::eNonstandardBlockSizeBit => SparseImageFormatFlagBits::eNonstandardBlockSizeBit,
                RawSparseImageFormatFlagBits(b) => SparseImageFormatFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<SparseImageFormatFlagBits> {
            match self {
                RawSparseImageFormatFlagBits(0) => None,
                Self::eSingleMiptailBit => Some(SparseImageFormatFlagBits::eSingleMiptailBit),
                Self::eAlignedMipSizeBit => Some(SparseImageFormatFlagBits::eAlignedMipSizeBit),
                Self::eNonstandardBlockSizeBit => Some(SparseImageFormatFlagBits::eNonstandardBlockSizeBit),
                RawSparseImageFormatFlagBits(b) => Some(SparseImageFormatFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawSparseImageFormatFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawSubpassDescriptionFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawSubpassDescriptionFlagBits {
        pub const ePerViewAttributesBitNvx: Self = Self(0b1);
        pub const ePerViewPositionXOnlyBitNvx: Self = Self(0b10);
        pub const eFragmentRegionBitQcom: Self = Self(0b100);
        pub const eShaderResolveBitQcom: Self = Self(0b1000);
        pub const eRasterizationOrderAttachmentColourAccessBitExt: Self = Self(0b10000);
        pub const eRasterizationOrderAttachmentDepthAccessBitExt: Self = Self(0b100000);
        pub const eRasterizationOrderAttachmentStencilAccessBitExt: Self = Self(0b1000000);
        pub const eEnableLegacyDitheringBitExt: Self = Self(0b10000000);

        pub fn normalise(self) -> SubpassDescriptionFlagBits {
            match self {
                Self::ePerViewAttributesBitNvx => SubpassDescriptionFlagBits::ePerViewAttributesBitNvx,
                Self::ePerViewPositionXOnlyBitNvx => SubpassDescriptionFlagBits::ePerViewPositionXOnlyBitNvx,
                Self::eFragmentRegionBitQcom => SubpassDescriptionFlagBits::eFragmentRegionBitQcom,
                Self::eShaderResolveBitQcom => SubpassDescriptionFlagBits::eShaderResolveBitQcom,
                Self::eRasterizationOrderAttachmentColourAccessBitExt => SubpassDescriptionFlagBits::eRasterizationOrderAttachmentColourAccessBitExt,
                Self::eRasterizationOrderAttachmentDepthAccessBitExt => SubpassDescriptionFlagBits::eRasterizationOrderAttachmentDepthAccessBitExt,
                Self::eRasterizationOrderAttachmentStencilAccessBitExt => SubpassDescriptionFlagBits::eRasterizationOrderAttachmentStencilAccessBitExt,
                Self::eEnableLegacyDitheringBitExt => SubpassDescriptionFlagBits::eEnableLegacyDitheringBitExt,
                RawSubpassDescriptionFlagBits(b) => SubpassDescriptionFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<SubpassDescriptionFlagBits> {
            match self {
                RawSubpassDescriptionFlagBits(0) => None,
                Self::ePerViewAttributesBitNvx => Some(SubpassDescriptionFlagBits::ePerViewAttributesBitNvx),
                Self::ePerViewPositionXOnlyBitNvx => Some(SubpassDescriptionFlagBits::ePerViewPositionXOnlyBitNvx),
                Self::eFragmentRegionBitQcom => Some(SubpassDescriptionFlagBits::eFragmentRegionBitQcom),
                Self::eShaderResolveBitQcom => Some(SubpassDescriptionFlagBits::eShaderResolveBitQcom),
                Self::eRasterizationOrderAttachmentColourAccessBitExt => Some(SubpassDescriptionFlagBits::eRasterizationOrderAttachmentColourAccessBitExt),
                Self::eRasterizationOrderAttachmentDepthAccessBitExt => Some(SubpassDescriptionFlagBits::eRasterizationOrderAttachmentDepthAccessBitExt),
                Self::eRasterizationOrderAttachmentStencilAccessBitExt => Some(SubpassDescriptionFlagBits::eRasterizationOrderAttachmentStencilAccessBitExt),
                Self::eEnableLegacyDitheringBitExt => Some(SubpassDescriptionFlagBits::eEnableLegacyDitheringBitExt),
                RawSubpassDescriptionFlagBits(b) => Some(SubpassDescriptionFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawSubpassDescriptionFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawPipelineStageFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawPipelineStageFlagBits {
        pub const eNone: Self = Self(0);
        pub const eTopOfPipeBit: Self = Self(0b1);
        pub const eDrawIndirectBit: Self = Self(0b10);
        pub const eVertexInputBit: Self = Self(0b100);
        pub const eVertexShaderBit: Self = Self(0b1000);
        pub const eTessellationControlShaderBit: Self = Self(0b10000);
        pub const eTessellationEvaluationShaderBit: Self = Self(0b100000);
        pub const eGeometryShaderBit: Self = Self(0b1000000);
        pub const eFragmentShaderBit: Self = Self(0b10000000);
        pub const eEarlyFragmentTestsBit: Self = Self(0b100000000);
        pub const eLateFragmentTestsBit: Self = Self(0b1000000000);
        pub const eColourAttachmentOutputBit: Self = Self(0b10000000000);
        pub const eComputeShaderBit: Self = Self(0b100000000000);
        pub const eTransferBit: Self = Self(0b1000000000000);
        pub const eBottomOfPipeBit: Self = Self(0b10000000000000);
        pub const eHostBit: Self = Self(0b100000000000000);
        pub const eAllGraphicsBit: Self = Self(0b1000000000000000);
        pub const eAllCommandsBit: Self = Self(0b10000000000000000);
        pub const eCommandPreprocessBitNv: Self = Self(0b100000000000000000);
        pub const eConditionalRenderingBitExt: Self = Self(0b1000000000000000000);
        pub const eTaskShaderBitExt: Self = Self(0b10000000000000000000);
        pub const eMeshShaderBitExt: Self = Self(0b100000000000000000000);
        pub const eRayTracingShaderBitKhr: Self = Self(0b1000000000000000000000);
        pub const eFragmentShadingRateAttachmentBitKhr: Self = Self(0b10000000000000000000000);
        pub const eFragmentDensityProcessBitExt: Self = Self(0b100000000000000000000000);
        pub const eTransformFeedbackBitExt: Self = Self(0b1000000000000000000000000);
        pub const eAccelerationStructureBuildBitKhr: Self = Self(0b10000000000000000000000000);
        pub const eNoneKhr: Self = Self::eNone;
        pub const eRayTracingShaderBitNv: Self = Self::eRayTracingShaderBitKhr;
        pub const eAccelerationStructureBuildBitNv: Self = Self::eAccelerationStructureBuildBitKhr;

        pub fn normalise(self) -> PipelineStageFlagBits {
            match self {
                Self::eNone => PipelineStageFlagBits::eNone,
                Self::eTopOfPipeBit => PipelineStageFlagBits::eTopOfPipeBit,
                Self::eDrawIndirectBit => PipelineStageFlagBits::eDrawIndirectBit,
                Self::eVertexInputBit => PipelineStageFlagBits::eVertexInputBit,
                Self::eVertexShaderBit => PipelineStageFlagBits::eVertexShaderBit,
                Self::eTessellationControlShaderBit => PipelineStageFlagBits::eTessellationControlShaderBit,
                Self::eTessellationEvaluationShaderBit => PipelineStageFlagBits::eTessellationEvaluationShaderBit,
                Self::eGeometryShaderBit => PipelineStageFlagBits::eGeometryShaderBit,
                Self::eFragmentShaderBit => PipelineStageFlagBits::eFragmentShaderBit,
                Self::eEarlyFragmentTestsBit => PipelineStageFlagBits::eEarlyFragmentTestsBit,
                Self::eLateFragmentTestsBit => PipelineStageFlagBits::eLateFragmentTestsBit,
                Self::eColourAttachmentOutputBit => PipelineStageFlagBits::eColourAttachmentOutputBit,
                Self::eComputeShaderBit => PipelineStageFlagBits::eComputeShaderBit,
                Self::eTransferBit => PipelineStageFlagBits::eTransferBit,
                Self::eBottomOfPipeBit => PipelineStageFlagBits::eBottomOfPipeBit,
                Self::eHostBit => PipelineStageFlagBits::eHostBit,
                Self::eAllGraphicsBit => PipelineStageFlagBits::eAllGraphicsBit,
                Self::eAllCommandsBit => PipelineStageFlagBits::eAllCommandsBit,
                Self::eCommandPreprocessBitNv => PipelineStageFlagBits::eCommandPreprocessBitNv,
                Self::eConditionalRenderingBitExt => PipelineStageFlagBits::eConditionalRenderingBitExt,
                Self::eTaskShaderBitExt => PipelineStageFlagBits::eTaskShaderBitExt,
                Self::eMeshShaderBitExt => PipelineStageFlagBits::eMeshShaderBitExt,
                Self::eRayTracingShaderBitKhr => PipelineStageFlagBits::eRayTracingShaderBitKhr,
                Self::eFragmentShadingRateAttachmentBitKhr => PipelineStageFlagBits::eFragmentShadingRateAttachmentBitKhr,
                Self::eFragmentDensityProcessBitExt => PipelineStageFlagBits::eFragmentDensityProcessBitExt,
                Self::eTransformFeedbackBitExt => PipelineStageFlagBits::eTransformFeedbackBitExt,
                Self::eAccelerationStructureBuildBitKhr => PipelineStageFlagBits::eAccelerationStructureBuildBitKhr,
                RawPipelineStageFlagBits(b) => PipelineStageFlagBits::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawPipelineStageFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawSampleCountFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawSampleCountFlagBits {
        pub const e1Bit: Self = Self(0b1);
        pub const e2Bit: Self = Self(0b10);
        pub const e4Bit: Self = Self(0b100);
        pub const e8Bit: Self = Self(0b1000);
        pub const e16Bit: Self = Self(0b10000);
        pub const e32Bit: Self = Self(0b100000);
        pub const e64Bit: Self = Self(0b1000000);

        pub fn normalise(self) -> SampleCountFlagBits {
            match self {
                Self::e1Bit => SampleCountFlagBits::e1Bit,
                Self::e2Bit => SampleCountFlagBits::e2Bit,
                Self::e4Bit => SampleCountFlagBits::e4Bit,
                Self::e8Bit => SampleCountFlagBits::e8Bit,
                Self::e16Bit => SampleCountFlagBits::e16Bit,
                Self::e32Bit => SampleCountFlagBits::e32Bit,
                Self::e64Bit => SampleCountFlagBits::e64Bit,
                RawSampleCountFlagBits(b) => SampleCountFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<SampleCountFlagBits> {
            match self {
                RawSampleCountFlagBits(0) => None,
                Self::e1Bit => Some(SampleCountFlagBits::e1Bit),
                Self::e2Bit => Some(SampleCountFlagBits::e2Bit),
                Self::e4Bit => Some(SampleCountFlagBits::e4Bit),
                Self::e8Bit => Some(SampleCountFlagBits::e8Bit),
                Self::e16Bit => Some(SampleCountFlagBits::e16Bit),
                Self::e32Bit => Some(SampleCountFlagBits::e32Bit),
                Self::e64Bit => Some(SampleCountFlagBits::e64Bit),
                RawSampleCountFlagBits(b) => Some(SampleCountFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawSampleCountFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawAttachmentDescriptionFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawAttachmentDescriptionFlagBits {
        pub const eMayAliasBit: Self = Self(0b1);

        pub fn normalise(self) -> AttachmentDescriptionFlagBits {
            match self {
                Self::eMayAliasBit => AttachmentDescriptionFlagBits::eMayAliasBit,
                RawAttachmentDescriptionFlagBits(b) => AttachmentDescriptionFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<AttachmentDescriptionFlagBits> {
            match self {
                RawAttachmentDescriptionFlagBits(0) => None,
                Self::eMayAliasBit => Some(AttachmentDescriptionFlagBits::eMayAliasBit),
                RawAttachmentDescriptionFlagBits(b) => Some(AttachmentDescriptionFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawAttachmentDescriptionFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawStencilFaceFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawStencilFaceFlagBits {
        pub const eFrontBit: Self = Self(0b1);
        pub const eBackBit: Self = Self(0b10);
        pub const eFrontAndBack: Self = Self(0b11);

        pub fn normalise(self) -> StencilFaceFlagBits {
            match self {
                Self::eFrontBit => StencilFaceFlagBits::eFrontBit,
                Self::eBackBit => StencilFaceFlagBits::eBackBit,
                Self::eFrontAndBack => StencilFaceFlagBits::eFrontAndBack,
                RawStencilFaceFlagBits(b) => StencilFaceFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<StencilFaceFlagBits> {
            match self {
                RawStencilFaceFlagBits(0) => None,
                Self::eFrontBit => Some(StencilFaceFlagBits::eFrontBit),
                Self::eBackBit => Some(StencilFaceFlagBits::eBackBit),
                Self::eFrontAndBack => Some(StencilFaceFlagBits::eFrontAndBack),
                RawStencilFaceFlagBits(b) => Some(StencilFaceFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawStencilFaceFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawCullModeFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawCullModeFlagBits {
        pub const eNone: Self = Self(0);
        pub const eFrontBit: Self = Self(0b1);
        pub const eBackBit: Self = Self(0b10);
        pub const eFrontAndBack: Self = Self(0b11);

        pub fn normalise(self) -> CullModeFlagBits {
            match self {
                Self::eNone => CullModeFlagBits::eNone,
                Self::eFrontBit => CullModeFlagBits::eFrontBit,
                Self::eBackBit => CullModeFlagBits::eBackBit,
                Self::eFrontAndBack => CullModeFlagBits::eFrontAndBack,
                RawCullModeFlagBits(b) => CullModeFlagBits::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawCullModeFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawDescriptorPoolCreateFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawDescriptorPoolCreateFlagBits {
        pub const eFreeDescriptorSetBit: Self = Self(0b1);
        pub const eUpdateAfterBindBit: Self = Self(0b10);
        pub const eHostOnlyBitExt: Self = Self(0b100);
        pub const eUpdateAfterBindBitExt: Self = Self::eUpdateAfterBindBit;

        pub fn normalise(self) -> DescriptorPoolCreateFlagBits {
            match self {
                Self::eFreeDescriptorSetBit => DescriptorPoolCreateFlagBits::eFreeDescriptorSetBit,
                Self::eUpdateAfterBindBit => DescriptorPoolCreateFlagBits::eUpdateAfterBindBit,
                Self::eHostOnlyBitExt => DescriptorPoolCreateFlagBits::eHostOnlyBitExt,
                RawDescriptorPoolCreateFlagBits(b) => DescriptorPoolCreateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<DescriptorPoolCreateFlagBits> {
            match self {
                RawDescriptorPoolCreateFlagBits(0) => None,
                Self::eFreeDescriptorSetBit => Some(DescriptorPoolCreateFlagBits::eFreeDescriptorSetBit),
                Self::eUpdateAfterBindBit => Some(DescriptorPoolCreateFlagBits::eUpdateAfterBindBit),
                Self::eHostOnlyBitExt => Some(DescriptorPoolCreateFlagBits::eHostOnlyBitExt),
                RawDescriptorPoolCreateFlagBits(b) => Some(DescriptorPoolCreateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawDescriptorPoolCreateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawDependencyFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawDependencyFlagBits {
        pub const eByRegionBit: Self = Self(0b1);
        pub const eViewLocalBit: Self = Self(0b10);
        pub const eDeviceGroupBit: Self = Self(0b100);
        pub const eFeedbackLoopBitExt: Self = Self(0b1000);
        pub const eViewLocalBitKhr: Self = Self::eViewLocalBit;
        pub const eDeviceGroupBitKhr: Self = Self::eDeviceGroupBit;

        pub fn normalise(self) -> DependencyFlagBits {
            match self {
                Self::eByRegionBit => DependencyFlagBits::eByRegionBit,
                Self::eViewLocalBit => DependencyFlagBits::eViewLocalBit,
                Self::eDeviceGroupBit => DependencyFlagBits::eDeviceGroupBit,
                Self::eFeedbackLoopBitExt => DependencyFlagBits::eFeedbackLoopBitExt,
                RawDependencyFlagBits(b) => DependencyFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<DependencyFlagBits> {
            match self {
                RawDependencyFlagBits(0) => None,
                Self::eByRegionBit => Some(DependencyFlagBits::eByRegionBit),
                Self::eViewLocalBit => Some(DependencyFlagBits::eViewLocalBit),
                Self::eDeviceGroupBit => Some(DependencyFlagBits::eDeviceGroupBit),
                Self::eFeedbackLoopBitExt => Some(DependencyFlagBits::eFeedbackLoopBitExt),
                RawDependencyFlagBits(b) => Some(DependencyFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawDependencyFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawSubgroupFeatureFlagBits(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawSubgroupFeatureFlagBits {
        pub const eBasicBit: Self = Self(0b1);
        pub const eVoteBit: Self = Self(0b10);
        pub const eArithmeticBit: Self = Self(0b100);
        pub const eBallotBit: Self = Self(0b1000);
        pub const eShuffleBit: Self = Self(0b10000);
        pub const eShuffleRelativeBit: Self = Self(0b100000);
        pub const eClusteredBit: Self = Self(0b1000000);
        pub const eQuadBit: Self = Self(0b10000000);
        pub const ePartitionedBitNv: Self = Self(0b100000000);

        pub fn normalise(self) -> SubgroupFeatureFlagBits {
            match self {
                Self::eBasicBit => SubgroupFeatureFlagBits::eBasicBit,
                Self::eVoteBit => SubgroupFeatureFlagBits::eVoteBit,
                Self::eArithmeticBit => SubgroupFeatureFlagBits::eArithmeticBit,
                Self::eBallotBit => SubgroupFeatureFlagBits::eBallotBit,
                Self::eShuffleBit => SubgroupFeatureFlagBits::eShuffleBit,
                Self::eShuffleRelativeBit => SubgroupFeatureFlagBits::eShuffleRelativeBit,
                Self::eClusteredBit => SubgroupFeatureFlagBits::eClusteredBit,
                Self::eQuadBit => SubgroupFeatureFlagBits::eQuadBit,
                Self::ePartitionedBitNv => SubgroupFeatureFlagBits::ePartitionedBitNv,
                RawSubgroupFeatureFlagBits(b) => SubgroupFeatureFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<SubgroupFeatureFlagBits> {
            match self {
                RawSubgroupFeatureFlagBits(0) => None,
                Self::eBasicBit => Some(SubgroupFeatureFlagBits::eBasicBit),
                Self::eVoteBit => Some(SubgroupFeatureFlagBits::eVoteBit),
                Self::eArithmeticBit => Some(SubgroupFeatureFlagBits::eArithmeticBit),
                Self::eBallotBit => Some(SubgroupFeatureFlagBits::eBallotBit),
                Self::eShuffleBit => Some(SubgroupFeatureFlagBits::eShuffleBit),
                Self::eShuffleRelativeBit => Some(SubgroupFeatureFlagBits::eShuffleRelativeBit),
                Self::eClusteredBit => Some(SubgroupFeatureFlagBits::eClusteredBit),
                Self::eQuadBit => Some(SubgroupFeatureFlagBits::eQuadBit),
                Self::ePartitionedBitNv => Some(SubgroupFeatureFlagBits::ePartitionedBitNv),
                RawSubgroupFeatureFlagBits(b) => Some(SubgroupFeatureFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawSubgroupFeatureFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawIndirectCommandsLayoutUsageFlagBitsNV(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawIndirectCommandsLayoutUsageFlagBitsNV {
        pub const eExplicitPreprocessBit: Self = Self(0b1);
        pub const eIndexedSequencesBit: Self = Self(0b10);
        pub const eUnorderedSequencesBit: Self = Self(0b100);

        pub fn normalise(self) -> IndirectCommandsLayoutUsageFlagBitsNV {
            match self {
                Self::eExplicitPreprocessBit => IndirectCommandsLayoutUsageFlagBitsNV::eExplicitPreprocessBit,
                Self::eIndexedSequencesBit => IndirectCommandsLayoutUsageFlagBitsNV::eIndexedSequencesBit,
                Self::eUnorderedSequencesBit => IndirectCommandsLayoutUsageFlagBitsNV::eUnorderedSequencesBit,
                RawIndirectCommandsLayoutUsageFlagBitsNV(b) => IndirectCommandsLayoutUsageFlagBitsNV::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<IndirectCommandsLayoutUsageFlagBitsNV> {
            match self {
                RawIndirectCommandsLayoutUsageFlagBitsNV(0) => None,
                Self::eExplicitPreprocessBit => Some(IndirectCommandsLayoutUsageFlagBitsNV::eExplicitPreprocessBit),
                Self::eIndexedSequencesBit => Some(IndirectCommandsLayoutUsageFlagBitsNV::eIndexedSequencesBit),
                Self::eUnorderedSequencesBit => Some(IndirectCommandsLayoutUsageFlagBitsNV::eUnorderedSequencesBit),
                RawIndirectCommandsLayoutUsageFlagBitsNV(b) => Some(IndirectCommandsLayoutUsageFlagBitsNV::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawIndirectCommandsLayoutUsageFlagBitsNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawIndirectStateFlagBitsNV(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawIndirectStateFlagBitsNV {
        pub const eFrontfaceBit: Self = Self(0b1);

        pub fn normalise(self) -> IndirectStateFlagBitsNV {
            match self {
                Self::eFrontfaceBit => IndirectStateFlagBitsNV::eFrontfaceBit,
                RawIndirectStateFlagBitsNV(b) => IndirectStateFlagBitsNV::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<IndirectStateFlagBitsNV> {
            match self {
                RawIndirectStateFlagBitsNV(0) => None,
                Self::eFrontfaceBit => Some(IndirectStateFlagBitsNV::eFrontfaceBit),
                RawIndirectStateFlagBitsNV(b) => Some(IndirectStateFlagBitsNV::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawIndirectStateFlagBitsNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawGeometryFlagBitsKHR(pub u32);

    pub type RawGeometryFlagBitsNV = RawGeometryFlagBitsKHR;

    #[allow(non_upper_case_globals)]
    impl RawGeometryFlagBitsKHR {
        pub const eOpaqueBit: Self = Self(0b1);
        pub const eNoDuplicateAnyHitInvocationBit: Self = Self(0b10);
        pub const eOpaqueBitNv: Self = Self::eOpaqueBit;
        pub const eNoDuplicateAnyHitInvocationBitNv: Self = Self::eNoDuplicateAnyHitInvocationBit;

        pub fn normalise(self) -> GeometryFlagBitsKHR {
            match self {
                Self::eOpaqueBit => GeometryFlagBitsKHR::eOpaqueBit,
                Self::eNoDuplicateAnyHitInvocationBit => GeometryFlagBitsKHR::eNoDuplicateAnyHitInvocationBit,
                RawGeometryFlagBitsKHR(b) => GeometryFlagBitsKHR::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<GeometryFlagBitsKHR> {
            match self {
                RawGeometryFlagBitsKHR(0) => None,
                Self::eOpaqueBit => Some(GeometryFlagBitsKHR::eOpaqueBit),
                Self::eNoDuplicateAnyHitInvocationBit => Some(GeometryFlagBitsKHR::eNoDuplicateAnyHitInvocationBit),
                RawGeometryFlagBitsKHR(b) => Some(GeometryFlagBitsKHR::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawGeometryFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawGeometryInstanceFlagBitsKHR(pub u32);

    pub type RawGeometryInstanceFlagBitsNV = RawGeometryInstanceFlagBitsKHR;

    #[allow(non_upper_case_globals)]
    impl RawGeometryInstanceFlagBitsKHR {
        pub const eTriangleFacingCullDisableBit: Self = Self(0b1);
        pub const eTriangleFlipFacingBit: Self = Self(0b10);
        pub const eForceOpaqueBit: Self = Self(0b100);
        pub const eForceNoOpaqueBit: Self = Self(0b1000);
        pub const eForceOpacityMicromap2StateExt: Self = Self(0b10000);
        pub const eDisableOpacityMicromapsExt: Self = Self(0b100000);
        pub const eTriangleCullDisableBitNv: Self = Self::eTriangleFacingCullDisableBit;
        pub const eTriangleFrontCounterclockwiseBit: Self = Self::eTriangleFlipFacingBit;
        pub const eForceOpaqueBitNv: Self = Self::eForceOpaqueBit;
        pub const eForceNoOpaqueBitNv: Self = Self::eForceNoOpaqueBit;

        pub fn normalise(self) -> GeometryInstanceFlagBitsKHR {
            match self {
                Self::eTriangleFacingCullDisableBit => GeometryInstanceFlagBitsKHR::eTriangleFacingCullDisableBit,
                Self::eTriangleFlipFacingBit => GeometryInstanceFlagBitsKHR::eTriangleFlipFacingBit,
                Self::eForceOpaqueBit => GeometryInstanceFlagBitsKHR::eForceOpaqueBit,
                Self::eForceNoOpaqueBit => GeometryInstanceFlagBitsKHR::eForceNoOpaqueBit,
                Self::eForceOpacityMicromap2StateExt => GeometryInstanceFlagBitsKHR::eForceOpacityMicromap2StateExt,
                Self::eDisableOpacityMicromapsExt => GeometryInstanceFlagBitsKHR::eDisableOpacityMicromapsExt,
                RawGeometryInstanceFlagBitsKHR(b) => GeometryInstanceFlagBitsKHR::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<GeometryInstanceFlagBitsKHR> {
            match self {
                RawGeometryInstanceFlagBitsKHR(0) => None,
                Self::eTriangleFacingCullDisableBit => Some(GeometryInstanceFlagBitsKHR::eTriangleFacingCullDisableBit),
                Self::eTriangleFlipFacingBit => Some(GeometryInstanceFlagBitsKHR::eTriangleFlipFacingBit),
                Self::eForceOpaqueBit => Some(GeometryInstanceFlagBitsKHR::eForceOpaqueBit),
                Self::eForceNoOpaqueBit => Some(GeometryInstanceFlagBitsKHR::eForceNoOpaqueBit),
                Self::eForceOpacityMicromap2StateExt => Some(GeometryInstanceFlagBitsKHR::eForceOpacityMicromap2StateExt),
                Self::eDisableOpacityMicromapsExt => Some(GeometryInstanceFlagBitsKHR::eDisableOpacityMicromapsExt),
                RawGeometryInstanceFlagBitsKHR(b) => Some(GeometryInstanceFlagBitsKHR::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawGeometryInstanceFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawBuildAccelerationStructureFlagBitsKHR(pub u32);

    pub type RawBuildAccelerationStructureFlagBitsNV = RawBuildAccelerationStructureFlagBitsKHR;

    #[allow(non_upper_case_globals)]
    impl RawBuildAccelerationStructureFlagBitsKHR {
        pub const eAllowUpdateBit: Self = Self(0b1);
        pub const eAllowCompactionBit: Self = Self(0b10);
        pub const ePreferFastTraceBit: Self = Self(0b100);
        pub const ePreferFastBuildBit: Self = Self(0b1000);
        pub const eLowMemoryBit: Self = Self(0b10000);
        pub const eMotionBitNv: Self = Self(0b100000);
        pub const eAllowOpacityMicromapUpdateExt: Self = Self(0b1000000);
        pub const eAllowDisableOpacityMicromapsExt: Self = Self(0b10000000);
        pub const eAllowOpacityMicromapDataUpdateExt: Self = Self(0b100000000);
        pub const eAllowUpdateBitNv: Self = Self::eAllowUpdateBit;
        pub const eAllowCompactionBitNv: Self = Self::eAllowCompactionBit;
        pub const ePreferFastTraceBitNv: Self = Self::ePreferFastTraceBit;
        pub const ePreferFastBuildBitNv: Self = Self::ePreferFastBuildBit;
        pub const eLowMemoryBitNv: Self = Self::eLowMemoryBit;

        pub fn normalise(self) -> BuildAccelerationStructureFlagBitsKHR {
            match self {
                Self::eAllowUpdateBit => BuildAccelerationStructureFlagBitsKHR::eAllowUpdateBit,
                Self::eAllowCompactionBit => BuildAccelerationStructureFlagBitsKHR::eAllowCompactionBit,
                Self::ePreferFastTraceBit => BuildAccelerationStructureFlagBitsKHR::ePreferFastTraceBit,
                Self::ePreferFastBuildBit => BuildAccelerationStructureFlagBitsKHR::ePreferFastBuildBit,
                Self::eLowMemoryBit => BuildAccelerationStructureFlagBitsKHR::eLowMemoryBit,
                Self::eMotionBitNv => BuildAccelerationStructureFlagBitsKHR::eMotionBitNv,
                Self::eAllowOpacityMicromapUpdateExt => BuildAccelerationStructureFlagBitsKHR::eAllowOpacityMicromapUpdateExt,
                Self::eAllowDisableOpacityMicromapsExt => BuildAccelerationStructureFlagBitsKHR::eAllowDisableOpacityMicromapsExt,
                Self::eAllowOpacityMicromapDataUpdateExt => BuildAccelerationStructureFlagBitsKHR::eAllowOpacityMicromapDataUpdateExt,
                RawBuildAccelerationStructureFlagBitsKHR(b) => BuildAccelerationStructureFlagBitsKHR::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<BuildAccelerationStructureFlagBitsKHR> {
            match self {
                RawBuildAccelerationStructureFlagBitsKHR(0) => None,
                Self::eAllowUpdateBit => Some(BuildAccelerationStructureFlagBitsKHR::eAllowUpdateBit),
                Self::eAllowCompactionBit => Some(BuildAccelerationStructureFlagBitsKHR::eAllowCompactionBit),
                Self::ePreferFastTraceBit => Some(BuildAccelerationStructureFlagBitsKHR::ePreferFastTraceBit),
                Self::ePreferFastBuildBit => Some(BuildAccelerationStructureFlagBitsKHR::ePreferFastBuildBit),
                Self::eLowMemoryBit => Some(BuildAccelerationStructureFlagBitsKHR::eLowMemoryBit),
                Self::eMotionBitNv => Some(BuildAccelerationStructureFlagBitsKHR::eMotionBitNv),
                Self::eAllowOpacityMicromapUpdateExt => Some(BuildAccelerationStructureFlagBitsKHR::eAllowOpacityMicromapUpdateExt),
                Self::eAllowDisableOpacityMicromapsExt => Some(BuildAccelerationStructureFlagBitsKHR::eAllowDisableOpacityMicromapsExt),
                Self::eAllowOpacityMicromapDataUpdateExt => Some(BuildAccelerationStructureFlagBitsKHR::eAllowOpacityMicromapDataUpdateExt),
                RawBuildAccelerationStructureFlagBitsKHR(b) => Some(BuildAccelerationStructureFlagBitsKHR::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawBuildAccelerationStructureFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawAccelerationStructureCreateFlagBitsKHR(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawAccelerationStructureCreateFlagBitsKHR {
        pub const eDeviceAddressCaptureReplayBit: Self = Self(0b1);
        pub const eMotionBitNv: Self = Self(0b100);
        pub const eDescriptorBufferCaptureReplayBitExt: Self = Self(0b1000);

        pub fn normalise(self) -> AccelerationStructureCreateFlagBitsKHR {
            match self {
                Self::eDeviceAddressCaptureReplayBit => AccelerationStructureCreateFlagBitsKHR::eDeviceAddressCaptureReplayBit,
                Self::eMotionBitNv => AccelerationStructureCreateFlagBitsKHR::eMotionBitNv,
                Self::eDescriptorBufferCaptureReplayBitExt => AccelerationStructureCreateFlagBitsKHR::eDescriptorBufferCaptureReplayBitExt,
                RawAccelerationStructureCreateFlagBitsKHR(b) => AccelerationStructureCreateFlagBitsKHR::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<AccelerationStructureCreateFlagBitsKHR> {
            match self {
                RawAccelerationStructureCreateFlagBitsKHR(0) => None,
                Self::eDeviceAddressCaptureReplayBit => Some(AccelerationStructureCreateFlagBitsKHR::eDeviceAddressCaptureReplayBit),
                Self::eMotionBitNv => Some(AccelerationStructureCreateFlagBitsKHR::eMotionBitNv),
                Self::eDescriptorBufferCaptureReplayBitExt => Some(AccelerationStructureCreateFlagBitsKHR::eDescriptorBufferCaptureReplayBitExt),
                RawAccelerationStructureCreateFlagBitsKHR(b) => Some(AccelerationStructureCreateFlagBitsKHR::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawAccelerationStructureCreateFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawPipelineCreationFeedbackFlagBits(pub u32);

    pub type RawPipelineCreationFeedbackFlagBitsEXT = RawPipelineCreationFeedbackFlagBits;

    #[allow(non_upper_case_globals)]
    impl RawPipelineCreationFeedbackFlagBits {
        pub const eValidBit: Self = Self(0b1);
        pub const eApplicationPipelineCacheHitBit: Self = Self(0b10);
        pub const eBasePipelineAccelerationBit: Self = Self(0b100);
        pub const eValidBitExt: Self = Self::eValidBit;
        pub const eApplicationPipelineCacheHitBitExt: Self = Self::eApplicationPipelineCacheHitBit;
        pub const eBasePipelineAccelerationBitExt: Self = Self::eBasePipelineAccelerationBit;

        pub fn normalise(self) -> PipelineCreationFeedbackFlagBits {
            match self {
                Self::eValidBit => PipelineCreationFeedbackFlagBits::eValidBit,
                Self::eApplicationPipelineCacheHitBit => PipelineCreationFeedbackFlagBits::eApplicationPipelineCacheHitBit,
                Self::eBasePipelineAccelerationBit => PipelineCreationFeedbackFlagBits::eBasePipelineAccelerationBit,
                RawPipelineCreationFeedbackFlagBits(b) => PipelineCreationFeedbackFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<PipelineCreationFeedbackFlagBits> {
            match self {
                RawPipelineCreationFeedbackFlagBits(0) => None,
                Self::eValidBit => Some(PipelineCreationFeedbackFlagBits::eValidBit),
                Self::eApplicationPipelineCacheHitBit => Some(PipelineCreationFeedbackFlagBits::eApplicationPipelineCacheHitBit),
                Self::eBasePipelineAccelerationBit => Some(PipelineCreationFeedbackFlagBits::eBasePipelineAccelerationBit),
                RawPipelineCreationFeedbackFlagBits(b) => Some(PipelineCreationFeedbackFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawPipelineCreationFeedbackFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawPerformanceCounterDescriptionFlagBitsKHR(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawPerformanceCounterDescriptionFlagBitsKHR {
        pub const ePerformanceImpactingBit: Self = Self(0b1);
        pub const eConcurrentlyImpactedBit: Self = Self(0b10);
        pub const ePerformanceImpacting: Self = Self::ePerformanceImpactingBit;
        pub const eConcurrentlyImpacted: Self = Self::eConcurrentlyImpactedBit;

        pub fn normalise(self) -> PerformanceCounterDescriptionFlagBitsKHR {
            match self {
                Self::ePerformanceImpactingBit => PerformanceCounterDescriptionFlagBitsKHR::ePerformanceImpactingBit,
                Self::eConcurrentlyImpactedBit => PerformanceCounterDescriptionFlagBitsKHR::eConcurrentlyImpactedBit,
                RawPerformanceCounterDescriptionFlagBitsKHR(b) => PerformanceCounterDescriptionFlagBitsKHR::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<PerformanceCounterDescriptionFlagBitsKHR> {
            match self {
                RawPerformanceCounterDescriptionFlagBitsKHR(0) => None,
                Self::ePerformanceImpactingBit => Some(PerformanceCounterDescriptionFlagBitsKHR::ePerformanceImpactingBit),
                Self::eConcurrentlyImpactedBit => Some(PerformanceCounterDescriptionFlagBitsKHR::eConcurrentlyImpactedBit),
                RawPerformanceCounterDescriptionFlagBitsKHR(b) => Some(PerformanceCounterDescriptionFlagBitsKHR::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawPerformanceCounterDescriptionFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawAcquireProfilingLockFlagBitsKHR(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawAcquireProfilingLockFlagBitsKHR {
        pub fn normalise(self) -> AcquireProfilingLockFlagBitsKHR {
            match self {
                RawAcquireProfilingLockFlagBitsKHR(b) => AcquireProfilingLockFlagBitsKHR::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawAcquireProfilingLockFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawSemaphoreWaitFlagBits(pub u32);

    pub type RawSemaphoreWaitFlagBitsKHR = RawSemaphoreWaitFlagBits;

    #[allow(non_upper_case_globals)]
    impl RawSemaphoreWaitFlagBits {
        pub const eAnyBit: Self = Self(0b1);
        pub const eAnyBitKhr: Self = Self::eAnyBit;

        pub fn normalise(self) -> SemaphoreWaitFlagBits {
            match self {
                Self::eAnyBit => SemaphoreWaitFlagBits::eAnyBit,
                RawSemaphoreWaitFlagBits(b) => SemaphoreWaitFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<SemaphoreWaitFlagBits> {
            match self {
                RawSemaphoreWaitFlagBits(0) => None,
                Self::eAnyBit => Some(SemaphoreWaitFlagBits::eAnyBit),
                RawSemaphoreWaitFlagBits(b) => Some(SemaphoreWaitFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawSemaphoreWaitFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawPipelineCompilerControlFlagBitsAMD(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawPipelineCompilerControlFlagBitsAMD {
        pub fn normalise(self) -> PipelineCompilerControlFlagBitsAMD {
            match self {
                RawPipelineCompilerControlFlagBitsAMD(b) => PipelineCompilerControlFlagBitsAMD::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawPipelineCompilerControlFlagBitsAMD {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawShaderCorePropertiesFlagBitsAMD(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawShaderCorePropertiesFlagBitsAMD {
        pub fn normalise(self) -> ShaderCorePropertiesFlagBitsAMD {
            match self {
                RawShaderCorePropertiesFlagBitsAMD(b) => ShaderCorePropertiesFlagBitsAMD::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawShaderCorePropertiesFlagBitsAMD {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawDeviceDiagnosticsConfigFlagBitsNV(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawDeviceDiagnosticsConfigFlagBitsNV {
        pub const eEnableShaderDebugInfoBit: Self = Self(0b1);
        pub const eEnableResourceTrackingBit: Self = Self(0b10);
        pub const eEnableAutomaticCheckpointsBit: Self = Self(0b100);
        pub const eEnableShaderErrorReportingBit: Self = Self(0b1000);

        pub fn normalise(self) -> DeviceDiagnosticsConfigFlagBitsNV {
            match self {
                Self::eEnableShaderDebugInfoBit => DeviceDiagnosticsConfigFlagBitsNV::eEnableShaderDebugInfoBit,
                Self::eEnableResourceTrackingBit => DeviceDiagnosticsConfigFlagBitsNV::eEnableResourceTrackingBit,
                Self::eEnableAutomaticCheckpointsBit => DeviceDiagnosticsConfigFlagBitsNV::eEnableAutomaticCheckpointsBit,
                Self::eEnableShaderErrorReportingBit => DeviceDiagnosticsConfigFlagBitsNV::eEnableShaderErrorReportingBit,
                RawDeviceDiagnosticsConfigFlagBitsNV(b) => DeviceDiagnosticsConfigFlagBitsNV::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<DeviceDiagnosticsConfigFlagBitsNV> {
            match self {
                RawDeviceDiagnosticsConfigFlagBitsNV(0) => None,
                Self::eEnableShaderDebugInfoBit => Some(DeviceDiagnosticsConfigFlagBitsNV::eEnableShaderDebugInfoBit),
                Self::eEnableResourceTrackingBit => Some(DeviceDiagnosticsConfigFlagBitsNV::eEnableResourceTrackingBit),
                Self::eEnableAutomaticCheckpointsBit => Some(DeviceDiagnosticsConfigFlagBitsNV::eEnableAutomaticCheckpointsBit),
                Self::eEnableShaderErrorReportingBit => Some(DeviceDiagnosticsConfigFlagBitsNV::eEnableShaderErrorReportingBit),
                RawDeviceDiagnosticsConfigFlagBitsNV(b) => Some(DeviceDiagnosticsConfigFlagBitsNV::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawDeviceDiagnosticsConfigFlagBitsNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawAccessFlagBits2(pub u64);

    pub type RawAccessFlagBits2KHR = RawAccessFlagBits2;

    #[allow(non_upper_case_globals)]
    impl RawAccessFlagBits2 {
        pub const eNone: Self = Self(0);
        pub const eIndirectCommandReadBit: Self = Self(0b1);
        pub const eIndexReadBit: Self = Self(0b10);
        pub const eVertexAttributeReadBit: Self = Self(0b100);
        pub const eUniformReadBit: Self = Self(0b1000);
        pub const eInputAttachmentReadBit: Self = Self(0b10000);
        pub const eShaderReadBit: Self = Self(0b100000);
        pub const eShaderWriteBit: Self = Self(0b1000000);
        pub const eColourAttachmentReadBit: Self = Self(0b10000000);
        pub const eColourAttachmentWriteBit: Self = Self(0b100000000);
        pub const eDepthStencilAttachmentReadBit: Self = Self(0b1000000000);
        pub const eDepthStencilAttachmentWriteBit: Self = Self(0b10000000000);
        pub const eTransferReadBit: Self = Self(0b100000000000);
        pub const eTransferWriteBit: Self = Self(0b1000000000000);
        pub const eHostReadBit: Self = Self(0b10000000000000);
        pub const eHostWriteBit: Self = Self(0b100000000000000);
        pub const eMemoryReadBit: Self = Self(0b1000000000000000);
        pub const eMemoryWriteBit: Self = Self(0b10000000000000000);
        pub const eCommandPreprocessReadBitNv: Self = Self(0b100000000000000000);
        pub const eCommandPreprocessWriteBitNv: Self = Self(0b1000000000000000000);
        pub const eColourAttachmentReadNoncoherentBitExt: Self = Self(0b10000000000000000000);
        pub const eConditionalRenderingReadBitExt: Self = Self(0b100000000000000000000);
        pub const eAccelerationStructureReadBitKhr: Self = Self(0b1000000000000000000000);
        pub const eAccelerationStructureWriteBitKhr: Self = Self(0b10000000000000000000000);
        pub const eFragmentShadingRateAttachmentReadBitKhr: Self = Self(0b100000000000000000000000);
        pub const eFragmentDensityMapReadBitExt: Self = Self(0b1000000000000000000000000);
        pub const eTransformFeedbackWriteBitExt: Self = Self(0b10000000000000000000000000);
        pub const eTransformFeedbackCounterReadBitExt: Self = Self(0b100000000000000000000000000);
        pub const eTransformFeedbackCounterWriteBitExt: Self = Self(0b1000000000000000000000000000);
        pub const eShaderSampledReadBit: Self = Self(0b100000000000000000000000000000000);
        pub const eShaderStorageReadBit: Self = Self(0b1000000000000000000000000000000000);
        pub const eShaderStorageWriteBit: Self = Self(0b10000000000000000000000000000000000);
        pub const eVideoDecodeReadBitKhr: Self = Self(0b100000000000000000000000000000000000);
        pub const eVideoDecodeWriteBitKhr: Self = Self(0b1000000000000000000000000000000000000);
        pub const eVideoEncodeReadBitKhr: Self = Self(0b10000000000000000000000000000000000000);
        pub const eVideoEncodeWriteBitKhr: Self = Self(0b100000000000000000000000000000000000000);
        pub const eInvocationMaskReadBitHuawei: Self = Self(0b1000000000000000000000000000000000000000);
        pub const eShaderBindingTableReadBitKhr: Self = Self(0b10000000000000000000000000000000000000000);
        pub const eDescriptorBufferReadBitExt: Self = Self(0b100000000000000000000000000000000000000000);
        pub const eOpticalFlowReadBitNv: Self = Self(0b1000000000000000000000000000000000000000000);
        pub const eOpticalFlowWriteBitNv: Self = Self(0b10000000000000000000000000000000000000000000);
        pub const eMicromapReadBitExt: Self = Self(0b100000000000000000000000000000000000000000000);
        pub const eMicromapWriteBitExt: Self = Self(0b1000000000000000000000000000000000000000000000);
        pub const eNoneKhr: Self = Self::eNone;
        pub const eIndirectCommandReadBitKhr: Self = Self::eIndirectCommandReadBit;
        pub const eIndexReadBitKhr: Self = Self::eIndexReadBit;
        pub const eVertexAttributeReadBitKhr: Self = Self::eVertexAttributeReadBit;
        pub const eUniformReadBitKhr: Self = Self::eUniformReadBit;
        pub const eInputAttachmentReadBitKhr: Self = Self::eInputAttachmentReadBit;
        pub const eShaderReadBitKhr: Self = Self::eShaderReadBit;
        pub const eShaderWriteBitKhr: Self = Self::eShaderWriteBit;
        pub const eColourAttachmentReadBitKhr: Self = Self::eColourAttachmentReadBit;
        pub const eColourAttachmentWriteBitKhr: Self = Self::eColourAttachmentWriteBit;
        pub const eDepthStencilAttachmentReadBitKhr: Self = Self::eDepthStencilAttachmentReadBit;
        pub const eDepthStencilAttachmentWriteBitKhr: Self = Self::eDepthStencilAttachmentWriteBit;
        pub const eTransferReadBitKhr: Self = Self::eTransferReadBit;
        pub const eTransferWriteBitKhr: Self = Self::eTransferWriteBit;
        pub const eHostReadBitKhr: Self = Self::eHostReadBit;
        pub const eHostWriteBitKhr: Self = Self::eHostWriteBit;
        pub const eMemoryReadBitKhr: Self = Self::eMemoryReadBit;
        pub const eMemoryWriteBitKhr: Self = Self::eMemoryWriteBit;
        pub const eAccelerationStructureReadBitNv: Self = Self::eAccelerationStructureReadBitKhr;
        pub const eAccelerationStructureWriteBitNv: Self = Self::eAccelerationStructureWriteBitKhr;
        pub const eShadingRateImageReadBitNv: Self = Self::eFragmentShadingRateAttachmentReadBitKhr;
        pub const eShaderSampledReadBitKhr: Self = Self::eShaderSampledReadBit;
        pub const eShaderStorageReadBitKhr: Self = Self::eShaderStorageReadBit;
        pub const eShaderStorageWriteBitKhr: Self = Self::eShaderStorageWriteBit;

        pub fn normalise(self) -> AccessFlagBits2 {
            match self {
                Self::eNone => AccessFlagBits2::eNone,
                Self::eIndirectCommandReadBit => AccessFlagBits2::eIndirectCommandReadBit,
                Self::eIndexReadBit => AccessFlagBits2::eIndexReadBit,
                Self::eVertexAttributeReadBit => AccessFlagBits2::eVertexAttributeReadBit,
                Self::eUniformReadBit => AccessFlagBits2::eUniformReadBit,
                Self::eInputAttachmentReadBit => AccessFlagBits2::eInputAttachmentReadBit,
                Self::eShaderReadBit => AccessFlagBits2::eShaderReadBit,
                Self::eShaderWriteBit => AccessFlagBits2::eShaderWriteBit,
                Self::eColourAttachmentReadBit => AccessFlagBits2::eColourAttachmentReadBit,
                Self::eColourAttachmentWriteBit => AccessFlagBits2::eColourAttachmentWriteBit,
                Self::eDepthStencilAttachmentReadBit => AccessFlagBits2::eDepthStencilAttachmentReadBit,
                Self::eDepthStencilAttachmentWriteBit => AccessFlagBits2::eDepthStencilAttachmentWriteBit,
                Self::eTransferReadBit => AccessFlagBits2::eTransferReadBit,
                Self::eTransferWriteBit => AccessFlagBits2::eTransferWriteBit,
                Self::eHostReadBit => AccessFlagBits2::eHostReadBit,
                Self::eHostWriteBit => AccessFlagBits2::eHostWriteBit,
                Self::eMemoryReadBit => AccessFlagBits2::eMemoryReadBit,
                Self::eMemoryWriteBit => AccessFlagBits2::eMemoryWriteBit,
                Self::eCommandPreprocessReadBitNv => AccessFlagBits2::eCommandPreprocessReadBitNv,
                Self::eCommandPreprocessWriteBitNv => AccessFlagBits2::eCommandPreprocessWriteBitNv,
                Self::eColourAttachmentReadNoncoherentBitExt => AccessFlagBits2::eColourAttachmentReadNoncoherentBitExt,
                Self::eConditionalRenderingReadBitExt => AccessFlagBits2::eConditionalRenderingReadBitExt,
                Self::eAccelerationStructureReadBitKhr => AccessFlagBits2::eAccelerationStructureReadBitKhr,
                Self::eAccelerationStructureWriteBitKhr => AccessFlagBits2::eAccelerationStructureWriteBitKhr,
                Self::eFragmentShadingRateAttachmentReadBitKhr => AccessFlagBits2::eFragmentShadingRateAttachmentReadBitKhr,
                Self::eFragmentDensityMapReadBitExt => AccessFlagBits2::eFragmentDensityMapReadBitExt,
                Self::eTransformFeedbackWriteBitExt => AccessFlagBits2::eTransformFeedbackWriteBitExt,
                Self::eTransformFeedbackCounterReadBitExt => AccessFlagBits2::eTransformFeedbackCounterReadBitExt,
                Self::eTransformFeedbackCounterWriteBitExt => AccessFlagBits2::eTransformFeedbackCounterWriteBitExt,
                Self::eShaderSampledReadBit => AccessFlagBits2::eShaderSampledReadBit,
                Self::eShaderStorageReadBit => AccessFlagBits2::eShaderStorageReadBit,
                Self::eShaderStorageWriteBit => AccessFlagBits2::eShaderStorageWriteBit,
                Self::eVideoDecodeReadBitKhr => AccessFlagBits2::eVideoDecodeReadBitKhr,
                Self::eVideoDecodeWriteBitKhr => AccessFlagBits2::eVideoDecodeWriteBitKhr,
                Self::eVideoEncodeReadBitKhr => AccessFlagBits2::eVideoEncodeReadBitKhr,
                Self::eVideoEncodeWriteBitKhr => AccessFlagBits2::eVideoEncodeWriteBitKhr,
                Self::eInvocationMaskReadBitHuawei => AccessFlagBits2::eInvocationMaskReadBitHuawei,
                Self::eShaderBindingTableReadBitKhr => AccessFlagBits2::eShaderBindingTableReadBitKhr,
                Self::eDescriptorBufferReadBitExt => AccessFlagBits2::eDescriptorBufferReadBitExt,
                Self::eOpticalFlowReadBitNv => AccessFlagBits2::eOpticalFlowReadBitNv,
                Self::eOpticalFlowWriteBitNv => AccessFlagBits2::eOpticalFlowWriteBitNv,
                Self::eMicromapReadBitExt => AccessFlagBits2::eMicromapReadBitExt,
                Self::eMicromapWriteBitExt => AccessFlagBits2::eMicromapWriteBitExt,
                RawAccessFlagBits2(b) => AccessFlagBits2::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawAccessFlagBits2 {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawPipelineStageFlagBits2(pub u64);

    pub type RawPipelineStageFlagBits2KHR = RawPipelineStageFlagBits2;

    #[allow(non_upper_case_globals)]
    impl RawPipelineStageFlagBits2 {
        pub const eNone: Self = Self(0);
        pub const eTopOfPipeBit: Self = Self(0b1);
        pub const eDrawIndirectBit: Self = Self(0b10);
        pub const eVertexInputBit: Self = Self(0b100);
        pub const eVertexShaderBit: Self = Self(0b1000);
        pub const eTessellationControlShaderBit: Self = Self(0b10000);
        pub const eTessellationEvaluationShaderBit: Self = Self(0b100000);
        pub const eGeometryShaderBit: Self = Self(0b1000000);
        pub const eFragmentShaderBit: Self = Self(0b10000000);
        pub const eEarlyFragmentTestsBit: Self = Self(0b100000000);
        pub const eLateFragmentTestsBit: Self = Self(0b1000000000);
        pub const eColourAttachmentOutputBit: Self = Self(0b10000000000);
        pub const eComputeShaderBit: Self = Self(0b100000000000);
        pub const eAllTransferBit: Self = Self(0b1000000000000);
        pub const eBottomOfPipeBit: Self = Self(0b10000000000000);
        pub const eHostBit: Self = Self(0b100000000000000);
        pub const eAllGraphicsBit: Self = Self(0b1000000000000000);
        pub const eAllCommandsBit: Self = Self(0b10000000000000000);
        pub const eCommandPreprocessBitNv: Self = Self(0b100000000000000000);
        pub const eConditionalRenderingBitExt: Self = Self(0b1000000000000000000);
        pub const eTaskShaderBitExt: Self = Self(0b10000000000000000000);
        pub const eMeshShaderBitExt: Self = Self(0b100000000000000000000);
        pub const eRayTracingShaderBitKhr: Self = Self(0b1000000000000000000000);
        pub const eFragmentShadingRateAttachmentBitKhr: Self = Self(0b10000000000000000000000);
        pub const eFragmentDensityProcessBitExt: Self = Self(0b100000000000000000000000);
        pub const eTransformFeedbackBitExt: Self = Self(0b1000000000000000000000000);
        pub const eAccelerationStructureBuildBitKhr: Self = Self(0b10000000000000000000000000);
        pub const eVideoDecodeBitKhr: Self = Self(0b100000000000000000000000000);
        pub const eVideoEncodeBitKhr: Self = Self(0b1000000000000000000000000000);
        pub const eAccelerationStructureCopyBitKhr: Self = Self(0b10000000000000000000000000000);
        pub const eOpticalFlowBitNv: Self = Self(0b100000000000000000000000000000);
        pub const eMicromapBuildBitExt: Self = Self(0b1000000000000000000000000000000);
        pub const eCopyBit: Self = Self(0b100000000000000000000000000000000);
        pub const eResolveBit: Self = Self(0b1000000000000000000000000000000000);
        pub const eBlitBit: Self = Self(0b10000000000000000000000000000000000);
        pub const eClearBit: Self = Self(0b100000000000000000000000000000000000);
        pub const eIndexInputBit: Self = Self(0b1000000000000000000000000000000000000);
        pub const eVertexAttributeInputBit: Self = Self(0b10000000000000000000000000000000000000);
        pub const ePreRasterizationShadersBit: Self = Self(0b100000000000000000000000000000000000000);
        pub const eSubpassShadingBitHuawei: Self = Self(0b1000000000000000000000000000000000000000);
        pub const eInvocationMaskBitHuawei: Self = Self(0b10000000000000000000000000000000000000000);
        pub const eClusterCullingShaderBitHuawei: Self = Self(0b100000000000000000000000000000000000000000);
        pub const eNoneKhr: Self = Self::eNone;
        pub const eTopOfPipeBitKhr: Self = Self::eTopOfPipeBit;
        pub const eDrawIndirectBitKhr: Self = Self::eDrawIndirectBit;
        pub const eVertexInputBitKhr: Self = Self::eVertexInputBit;
        pub const eVertexShaderBitKhr: Self = Self::eVertexShaderBit;
        pub const eTessellationControlShaderBitKhr: Self = Self::eTessellationControlShaderBit;
        pub const eTessellationEvaluationShaderBitKhr: Self = Self::eTessellationEvaluationShaderBit;
        pub const eGeometryShaderBitKhr: Self = Self::eGeometryShaderBit;
        pub const eFragmentShaderBitKhr: Self = Self::eFragmentShaderBit;
        pub const eEarlyFragmentTestsBitKhr: Self = Self::eEarlyFragmentTestsBit;
        pub const eLateFragmentTestsBitKhr: Self = Self::eLateFragmentTestsBit;
        pub const eColourAttachmentOutputBitKhr: Self = Self::eColourAttachmentOutputBit;
        pub const eComputeShaderBitKhr: Self = Self::eComputeShaderBit;
        pub const eAllTransferBitKhr: Self = Self::eAllTransferBit;
        pub const eTransferBitKhr: Self = Self::eAllTransferBit;
        pub const eBottomOfPipeBitKhr: Self = Self::eBottomOfPipeBit;
        pub const eHostBitKhr: Self = Self::eHostBit;
        pub const eAllGraphicsBitKhr: Self = Self::eAllGraphicsBit;
        pub const eAllCommandsBitKhr: Self = Self::eAllCommandsBit;
        pub const eRayTracingShaderBitNv: Self = Self::eRayTracingShaderBitKhr;
        pub const eShadingRateImageBitNv: Self = Self::eFragmentShadingRateAttachmentBitKhr;
        pub const eAccelerationStructureBuildBitNv: Self = Self::eAccelerationStructureBuildBitKhr;
        pub const eCopyBitKhr: Self = Self::eCopyBit;
        pub const eResolveBitKhr: Self = Self::eResolveBit;
        pub const eBlitBitKhr: Self = Self::eBlitBit;
        pub const eClearBitKhr: Self = Self::eClearBit;
        pub const eIndexInputBitKhr: Self = Self::eIndexInputBit;
        pub const eVertexAttributeInputBitKhr: Self = Self::eVertexAttributeInputBit;
        pub const ePreRasterizationShadersBitKhr: Self = Self::ePreRasterizationShadersBit;

        pub fn normalise(self) -> PipelineStageFlagBits2 {
            match self {
                Self::eNone => PipelineStageFlagBits2::eNone,
                Self::eTopOfPipeBit => PipelineStageFlagBits2::eTopOfPipeBit,
                Self::eDrawIndirectBit => PipelineStageFlagBits2::eDrawIndirectBit,
                Self::eVertexInputBit => PipelineStageFlagBits2::eVertexInputBit,
                Self::eVertexShaderBit => PipelineStageFlagBits2::eVertexShaderBit,
                Self::eTessellationControlShaderBit => PipelineStageFlagBits2::eTessellationControlShaderBit,
                Self::eTessellationEvaluationShaderBit => PipelineStageFlagBits2::eTessellationEvaluationShaderBit,
                Self::eGeometryShaderBit => PipelineStageFlagBits2::eGeometryShaderBit,
                Self::eFragmentShaderBit => PipelineStageFlagBits2::eFragmentShaderBit,
                Self::eEarlyFragmentTestsBit => PipelineStageFlagBits2::eEarlyFragmentTestsBit,
                Self::eLateFragmentTestsBit => PipelineStageFlagBits2::eLateFragmentTestsBit,
                Self::eColourAttachmentOutputBit => PipelineStageFlagBits2::eColourAttachmentOutputBit,
                Self::eComputeShaderBit => PipelineStageFlagBits2::eComputeShaderBit,
                Self::eAllTransferBit => PipelineStageFlagBits2::eAllTransferBit,
                Self::eBottomOfPipeBit => PipelineStageFlagBits2::eBottomOfPipeBit,
                Self::eHostBit => PipelineStageFlagBits2::eHostBit,
                Self::eAllGraphicsBit => PipelineStageFlagBits2::eAllGraphicsBit,
                Self::eAllCommandsBit => PipelineStageFlagBits2::eAllCommandsBit,
                Self::eCommandPreprocessBitNv => PipelineStageFlagBits2::eCommandPreprocessBitNv,
                Self::eConditionalRenderingBitExt => PipelineStageFlagBits2::eConditionalRenderingBitExt,
                Self::eTaskShaderBitExt => PipelineStageFlagBits2::eTaskShaderBitExt,
                Self::eMeshShaderBitExt => PipelineStageFlagBits2::eMeshShaderBitExt,
                Self::eRayTracingShaderBitKhr => PipelineStageFlagBits2::eRayTracingShaderBitKhr,
                Self::eFragmentShadingRateAttachmentBitKhr => PipelineStageFlagBits2::eFragmentShadingRateAttachmentBitKhr,
                Self::eFragmentDensityProcessBitExt => PipelineStageFlagBits2::eFragmentDensityProcessBitExt,
                Self::eTransformFeedbackBitExt => PipelineStageFlagBits2::eTransformFeedbackBitExt,
                Self::eAccelerationStructureBuildBitKhr => PipelineStageFlagBits2::eAccelerationStructureBuildBitKhr,
                Self::eVideoDecodeBitKhr => PipelineStageFlagBits2::eVideoDecodeBitKhr,
                Self::eVideoEncodeBitKhr => PipelineStageFlagBits2::eVideoEncodeBitKhr,
                Self::eAccelerationStructureCopyBitKhr => PipelineStageFlagBits2::eAccelerationStructureCopyBitKhr,
                Self::eOpticalFlowBitNv => PipelineStageFlagBits2::eOpticalFlowBitNv,
                Self::eMicromapBuildBitExt => PipelineStageFlagBits2::eMicromapBuildBitExt,
                Self::eCopyBit => PipelineStageFlagBits2::eCopyBit,
                Self::eResolveBit => PipelineStageFlagBits2::eResolveBit,
                Self::eBlitBit => PipelineStageFlagBits2::eBlitBit,
                Self::eClearBit => PipelineStageFlagBits2::eClearBit,
                Self::eIndexInputBit => PipelineStageFlagBits2::eIndexInputBit,
                Self::eVertexAttributeInputBit => PipelineStageFlagBits2::eVertexAttributeInputBit,
                Self::ePreRasterizationShadersBit => PipelineStageFlagBits2::ePreRasterizationShadersBit,
                Self::eSubpassShadingBitHuawei => PipelineStageFlagBits2::eSubpassShadingBitHuawei,
                Self::eInvocationMaskBitHuawei => PipelineStageFlagBits2::eInvocationMaskBitHuawei,
                Self::eClusterCullingShaderBitHuawei => PipelineStageFlagBits2::eClusterCullingShaderBitHuawei,
                RawPipelineStageFlagBits2(b) => PipelineStageFlagBits2::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawPipelineStageFlagBits2 {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawFormatFeatureFlagBits2(pub u64);

    pub type RawFormatFeatureFlagBits2KHR = RawFormatFeatureFlagBits2;

    #[allow(non_upper_case_globals)]
    impl RawFormatFeatureFlagBits2 {
        pub const eSampledImageBit: Self = Self(0b1);
        pub const eStorageImageBit: Self = Self(0b10);
        pub const eStorageImageAtomicBit: Self = Self(0b100);
        pub const eUniformTexelBufferBit: Self = Self(0b1000);
        pub const eStorageTexelBufferBit: Self = Self(0b10000);
        pub const eStorageTexelBufferAtomicBit: Self = Self(0b100000);
        pub const eVertexBufferBit: Self = Self(0b1000000);
        pub const eColourAttachmentBit: Self = Self(0b10000000);
        pub const eColourAttachmentBlendBit: Self = Self(0b100000000);
        pub const eDepthStencilAttachmentBit: Self = Self(0b1000000000);
        pub const eBlitSrcBit: Self = Self(0b10000000000);
        pub const eBlitDstBit: Self = Self(0b100000000000);
        pub const eSampledImageFilterLinearBit: Self = Self(0b1000000000000);
        pub const eSampledImageFilterCubicBit: Self = Self(0b10000000000000);
        pub const eTransferSrcBit: Self = Self(0b100000000000000);
        pub const eTransferDstBit: Self = Self(0b1000000000000000);
        pub const eSampledImageFilterMinmaxBit: Self = Self(0b10000000000000000);
        pub const eMidpointChromaSamplesBit: Self = Self(0b100000000000000000);
        pub const eSampledImageYcbcrConversionLinearFilterBit: Self = Self(0b1000000000000000000);
        pub const eSampledImageYcbcrConversionSeparateReconstructionFilterBit: Self = Self(0b10000000000000000000);
        pub const eSampledImageYcbcrConversionChromaReconstructionExplicitBit: Self = Self(0b100000000000000000000);
        pub const eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit: Self = Self(0b1000000000000000000000);
        pub const eDisjointBit: Self = Self(0b10000000000000000000000);
        pub const eCositedChromaSamplesBit: Self = Self(0b100000000000000000000000);
        pub const eFragmentDensityMapBitExt: Self = Self(0b1000000000000000000000000);
        pub const eVideoDecodeOutputBitKhr: Self = Self(0b10000000000000000000000000);
        pub const eVideoDecodeDpbBitKhr: Self = Self(0b100000000000000000000000000);
        pub const eVideoEncodeInputBitKhr: Self = Self(0b1000000000000000000000000000);
        pub const eVideoEncodeDpbBitKhr: Self = Self(0b10000000000000000000000000000);
        pub const eAccelerationStructureVertexBufferBitKhr: Self = Self(0b100000000000000000000000000000);
        pub const eFragmentShadingRateAttachmentBitKhr: Self = Self(0b1000000000000000000000000000000);
        pub const eStorageReadWithoutFormatBit: Self = Self(0b10000000000000000000000000000000);
        pub const eStorageWriteWithoutFormatBit: Self = Self(0b100000000000000000000000000000000);
        pub const eSampledImageDepthComparisonBit: Self = Self(0b1000000000000000000000000000000000);
        pub const eWeightImageBitQcom: Self = Self(0b10000000000000000000000000000000000);
        pub const eWeightSampledImageBitQcom: Self = Self(0b100000000000000000000000000000000000);
        pub const eBlockMatchingBitQcom: Self = Self(0b1000000000000000000000000000000000000);
        pub const eBoxFilterSampledBitQcom: Self = Self(0b10000000000000000000000000000000000000);
        pub const eLinearColourAttachmentBitNv: Self = Self(0b100000000000000000000000000000000000000);
        pub const eOpticalFlowImageBitNv: Self = Self(0b10000000000000000000000000000000000000000);
        pub const eOpticalFlowVectorBitNv: Self = Self(0b100000000000000000000000000000000000000000);
        pub const eOpticalFlowCostBitNv: Self = Self(0b1000000000000000000000000000000000000000000);
        pub const eSampledImageBitKhr: Self = Self::eSampledImageBit;
        pub const eStorageImageBitKhr: Self = Self::eStorageImageBit;
        pub const eStorageImageAtomicBitKhr: Self = Self::eStorageImageAtomicBit;
        pub const eUniformTexelBufferBitKhr: Self = Self::eUniformTexelBufferBit;
        pub const eStorageTexelBufferBitKhr: Self = Self::eStorageTexelBufferBit;
        pub const eStorageTexelBufferAtomicBitKhr: Self = Self::eStorageTexelBufferAtomicBit;
        pub const eVertexBufferBitKhr: Self = Self::eVertexBufferBit;
        pub const eColourAttachmentBitKhr: Self = Self::eColourAttachmentBit;
        pub const eColourAttachmentBlendBitKhr: Self = Self::eColourAttachmentBlendBit;
        pub const eDepthStencilAttachmentBitKhr: Self = Self::eDepthStencilAttachmentBit;
        pub const eBlitSrcBitKhr: Self = Self::eBlitSrcBit;
        pub const eBlitDstBitKhr: Self = Self::eBlitDstBit;
        pub const eSampledImageFilterLinearBitKhr: Self = Self::eSampledImageFilterLinearBit;
        pub const eSampledImageFilterCubicBitExt: Self = Self::eSampledImageFilterCubicBit;
        pub const eTransferSrcBitKhr: Self = Self::eTransferSrcBit;
        pub const eTransferDstBitKhr: Self = Self::eTransferDstBit;
        pub const eSampledImageFilterMinmaxBitKhr: Self = Self::eSampledImageFilterMinmaxBit;
        pub const eMidpointChromaSamplesBitKhr: Self = Self::eMidpointChromaSamplesBit;
        pub const eSampledImageYcbcrConversionLinearFilterBitKhr: Self = Self::eSampledImageYcbcrConversionLinearFilterBit;
        pub const eSampledImageYcbcrConversionSeparateReconstructionFilterBitKhr: Self = Self::eSampledImageYcbcrConversionSeparateReconstructionFilterBit;
        pub const eSampledImageYcbcrConversionChromaReconstructionExplicitBitKhr: Self = Self::eSampledImageYcbcrConversionChromaReconstructionExplicitBit;
        pub const eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBitKhr: Self = Self::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit;
        pub const eDisjointBitKhr: Self = Self::eDisjointBit;
        pub const eCositedChromaSamplesBitKhr: Self = Self::eCositedChromaSamplesBit;
        pub const eStorageReadWithoutFormatBitKhr: Self = Self::eStorageReadWithoutFormatBit;
        pub const eStorageWriteWithoutFormatBitKhr: Self = Self::eStorageWriteWithoutFormatBit;
        pub const eSampledImageDepthComparisonBitKhr: Self = Self::eSampledImageDepthComparisonBit;

        pub fn normalise(self) -> FormatFeatureFlagBits2 {
            match self {
                Self::eSampledImageBit => FormatFeatureFlagBits2::eSampledImageBit,
                Self::eStorageImageBit => FormatFeatureFlagBits2::eStorageImageBit,
                Self::eStorageImageAtomicBit => FormatFeatureFlagBits2::eStorageImageAtomicBit,
                Self::eUniformTexelBufferBit => FormatFeatureFlagBits2::eUniformTexelBufferBit,
                Self::eStorageTexelBufferBit => FormatFeatureFlagBits2::eStorageTexelBufferBit,
                Self::eStorageTexelBufferAtomicBit => FormatFeatureFlagBits2::eStorageTexelBufferAtomicBit,
                Self::eVertexBufferBit => FormatFeatureFlagBits2::eVertexBufferBit,
                Self::eColourAttachmentBit => FormatFeatureFlagBits2::eColourAttachmentBit,
                Self::eColourAttachmentBlendBit => FormatFeatureFlagBits2::eColourAttachmentBlendBit,
                Self::eDepthStencilAttachmentBit => FormatFeatureFlagBits2::eDepthStencilAttachmentBit,
                Self::eBlitSrcBit => FormatFeatureFlagBits2::eBlitSrcBit,
                Self::eBlitDstBit => FormatFeatureFlagBits2::eBlitDstBit,
                Self::eSampledImageFilterLinearBit => FormatFeatureFlagBits2::eSampledImageFilterLinearBit,
                Self::eSampledImageFilterCubicBit => FormatFeatureFlagBits2::eSampledImageFilterCubicBit,
                Self::eTransferSrcBit => FormatFeatureFlagBits2::eTransferSrcBit,
                Self::eTransferDstBit => FormatFeatureFlagBits2::eTransferDstBit,
                Self::eSampledImageFilterMinmaxBit => FormatFeatureFlagBits2::eSampledImageFilterMinmaxBit,
                Self::eMidpointChromaSamplesBit => FormatFeatureFlagBits2::eMidpointChromaSamplesBit,
                Self::eSampledImageYcbcrConversionLinearFilterBit => FormatFeatureFlagBits2::eSampledImageYcbcrConversionLinearFilterBit,
                Self::eSampledImageYcbcrConversionSeparateReconstructionFilterBit => FormatFeatureFlagBits2::eSampledImageYcbcrConversionSeparateReconstructionFilterBit,
                Self::eSampledImageYcbcrConversionChromaReconstructionExplicitBit => FormatFeatureFlagBits2::eSampledImageYcbcrConversionChromaReconstructionExplicitBit,
                Self::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit => FormatFeatureFlagBits2::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit,
                Self::eDisjointBit => FormatFeatureFlagBits2::eDisjointBit,
                Self::eCositedChromaSamplesBit => FormatFeatureFlagBits2::eCositedChromaSamplesBit,
                Self::eFragmentDensityMapBitExt => FormatFeatureFlagBits2::eFragmentDensityMapBitExt,
                Self::eVideoDecodeOutputBitKhr => FormatFeatureFlagBits2::eVideoDecodeOutputBitKhr,
                Self::eVideoDecodeDpbBitKhr => FormatFeatureFlagBits2::eVideoDecodeDpbBitKhr,
                Self::eVideoEncodeInputBitKhr => FormatFeatureFlagBits2::eVideoEncodeInputBitKhr,
                Self::eVideoEncodeDpbBitKhr => FormatFeatureFlagBits2::eVideoEncodeDpbBitKhr,
                Self::eAccelerationStructureVertexBufferBitKhr => FormatFeatureFlagBits2::eAccelerationStructureVertexBufferBitKhr,
                Self::eFragmentShadingRateAttachmentBitKhr => FormatFeatureFlagBits2::eFragmentShadingRateAttachmentBitKhr,
                Self::eStorageReadWithoutFormatBit => FormatFeatureFlagBits2::eStorageReadWithoutFormatBit,
                Self::eStorageWriteWithoutFormatBit => FormatFeatureFlagBits2::eStorageWriteWithoutFormatBit,
                Self::eSampledImageDepthComparisonBit => FormatFeatureFlagBits2::eSampledImageDepthComparisonBit,
                Self::eWeightImageBitQcom => FormatFeatureFlagBits2::eWeightImageBitQcom,
                Self::eWeightSampledImageBitQcom => FormatFeatureFlagBits2::eWeightSampledImageBitQcom,
                Self::eBlockMatchingBitQcom => FormatFeatureFlagBits2::eBlockMatchingBitQcom,
                Self::eBoxFilterSampledBitQcom => FormatFeatureFlagBits2::eBoxFilterSampledBitQcom,
                Self::eLinearColourAttachmentBitNv => FormatFeatureFlagBits2::eLinearColourAttachmentBitNv,
                Self::eOpticalFlowImageBitNv => FormatFeatureFlagBits2::eOpticalFlowImageBitNv,
                Self::eOpticalFlowVectorBitNv => FormatFeatureFlagBits2::eOpticalFlowVectorBitNv,
                Self::eOpticalFlowCostBitNv => FormatFeatureFlagBits2::eOpticalFlowCostBitNv,
                RawFormatFeatureFlagBits2(b) => FormatFeatureFlagBits2::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<FormatFeatureFlagBits2> {
            match self {
                RawFormatFeatureFlagBits2(0) => None,
                Self::eSampledImageBit => Some(FormatFeatureFlagBits2::eSampledImageBit),
                Self::eStorageImageBit => Some(FormatFeatureFlagBits2::eStorageImageBit),
                Self::eStorageImageAtomicBit => Some(FormatFeatureFlagBits2::eStorageImageAtomicBit),
                Self::eUniformTexelBufferBit => Some(FormatFeatureFlagBits2::eUniformTexelBufferBit),
                Self::eStorageTexelBufferBit => Some(FormatFeatureFlagBits2::eStorageTexelBufferBit),
                Self::eStorageTexelBufferAtomicBit => Some(FormatFeatureFlagBits2::eStorageTexelBufferAtomicBit),
                Self::eVertexBufferBit => Some(FormatFeatureFlagBits2::eVertexBufferBit),
                Self::eColourAttachmentBit => Some(FormatFeatureFlagBits2::eColourAttachmentBit),
                Self::eColourAttachmentBlendBit => Some(FormatFeatureFlagBits2::eColourAttachmentBlendBit),
                Self::eDepthStencilAttachmentBit => Some(FormatFeatureFlagBits2::eDepthStencilAttachmentBit),
                Self::eBlitSrcBit => Some(FormatFeatureFlagBits2::eBlitSrcBit),
                Self::eBlitDstBit => Some(FormatFeatureFlagBits2::eBlitDstBit),
                Self::eSampledImageFilterLinearBit => Some(FormatFeatureFlagBits2::eSampledImageFilterLinearBit),
                Self::eSampledImageFilterCubicBit => Some(FormatFeatureFlagBits2::eSampledImageFilterCubicBit),
                Self::eTransferSrcBit => Some(FormatFeatureFlagBits2::eTransferSrcBit),
                Self::eTransferDstBit => Some(FormatFeatureFlagBits2::eTransferDstBit),
                Self::eSampledImageFilterMinmaxBit => Some(FormatFeatureFlagBits2::eSampledImageFilterMinmaxBit),
                Self::eMidpointChromaSamplesBit => Some(FormatFeatureFlagBits2::eMidpointChromaSamplesBit),
                Self::eSampledImageYcbcrConversionLinearFilterBit => Some(FormatFeatureFlagBits2::eSampledImageYcbcrConversionLinearFilterBit),
                Self::eSampledImageYcbcrConversionSeparateReconstructionFilterBit => Some(FormatFeatureFlagBits2::eSampledImageYcbcrConversionSeparateReconstructionFilterBit),
                Self::eSampledImageYcbcrConversionChromaReconstructionExplicitBit => Some(FormatFeatureFlagBits2::eSampledImageYcbcrConversionChromaReconstructionExplicitBit),
                Self::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit => Some(FormatFeatureFlagBits2::eSampledImageYcbcrConversionChromaReconstructionExplicitForceableBit),
                Self::eDisjointBit => Some(FormatFeatureFlagBits2::eDisjointBit),
                Self::eCositedChromaSamplesBit => Some(FormatFeatureFlagBits2::eCositedChromaSamplesBit),
                Self::eFragmentDensityMapBitExt => Some(FormatFeatureFlagBits2::eFragmentDensityMapBitExt),
                Self::eVideoDecodeOutputBitKhr => Some(FormatFeatureFlagBits2::eVideoDecodeOutputBitKhr),
                Self::eVideoDecodeDpbBitKhr => Some(FormatFeatureFlagBits2::eVideoDecodeDpbBitKhr),
                Self::eVideoEncodeInputBitKhr => Some(FormatFeatureFlagBits2::eVideoEncodeInputBitKhr),
                Self::eVideoEncodeDpbBitKhr => Some(FormatFeatureFlagBits2::eVideoEncodeDpbBitKhr),
                Self::eAccelerationStructureVertexBufferBitKhr => Some(FormatFeatureFlagBits2::eAccelerationStructureVertexBufferBitKhr),
                Self::eFragmentShadingRateAttachmentBitKhr => Some(FormatFeatureFlagBits2::eFragmentShadingRateAttachmentBitKhr),
                Self::eStorageReadWithoutFormatBit => Some(FormatFeatureFlagBits2::eStorageReadWithoutFormatBit),
                Self::eStorageWriteWithoutFormatBit => Some(FormatFeatureFlagBits2::eStorageWriteWithoutFormatBit),
                Self::eSampledImageDepthComparisonBit => Some(FormatFeatureFlagBits2::eSampledImageDepthComparisonBit),
                Self::eWeightImageBitQcom => Some(FormatFeatureFlagBits2::eWeightImageBitQcom),
                Self::eWeightSampledImageBitQcom => Some(FormatFeatureFlagBits2::eWeightSampledImageBitQcom),
                Self::eBlockMatchingBitQcom => Some(FormatFeatureFlagBits2::eBlockMatchingBitQcom),
                Self::eBoxFilterSampledBitQcom => Some(FormatFeatureFlagBits2::eBoxFilterSampledBitQcom),
                Self::eLinearColourAttachmentBitNv => Some(FormatFeatureFlagBits2::eLinearColourAttachmentBitNv),
                Self::eOpticalFlowImageBitNv => Some(FormatFeatureFlagBits2::eOpticalFlowImageBitNv),
                Self::eOpticalFlowVectorBitNv => Some(FormatFeatureFlagBits2::eOpticalFlowVectorBitNv),
                Self::eOpticalFlowCostBitNv => Some(FormatFeatureFlagBits2::eOpticalFlowCostBitNv),
                RawFormatFeatureFlagBits2(b) => Some(FormatFeatureFlagBits2::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawFormatFeatureFlagBits2 {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawRenderingFlagBits(pub u32);

    pub type RawRenderingFlagBitsKHR = RawRenderingFlagBits;

    #[allow(non_upper_case_globals)]
    impl RawRenderingFlagBits {
        pub const eContentsSecondaryCommandBuffersBit: Self = Self(0b1);
        pub const eSuspendingBit: Self = Self(0b10);
        pub const eResumingBit: Self = Self(0b100);
        pub const eEnableLegacyDitheringBitExt: Self = Self(0b1000);
        pub const eContentsSecondaryCommandBuffersBitKhr: Self = Self::eContentsSecondaryCommandBuffersBit;
        pub const eSuspendingBitKhr: Self = Self::eSuspendingBit;
        pub const eResumingBitKhr: Self = Self::eResumingBit;

        pub fn normalise(self) -> RenderingFlagBits {
            match self {
                Self::eContentsSecondaryCommandBuffersBit => RenderingFlagBits::eContentsSecondaryCommandBuffersBit,
                Self::eSuspendingBit => RenderingFlagBits::eSuspendingBit,
                Self::eResumingBit => RenderingFlagBits::eResumingBit,
                Self::eEnableLegacyDitheringBitExt => RenderingFlagBits::eEnableLegacyDitheringBitExt,
                RawRenderingFlagBits(b) => RenderingFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<RenderingFlagBits> {
            match self {
                RawRenderingFlagBits(0) => None,
                Self::eContentsSecondaryCommandBuffersBit => Some(RenderingFlagBits::eContentsSecondaryCommandBuffersBit),
                Self::eSuspendingBit => Some(RenderingFlagBits::eSuspendingBit),
                Self::eResumingBit => Some(RenderingFlagBits::eResumingBit),
                Self::eEnableLegacyDitheringBitExt => Some(RenderingFlagBits::eEnableLegacyDitheringBitExt),
                RawRenderingFlagBits(b) => Some(RenderingFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawRenderingFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawMemoryDecompressionMethodFlagBitsNV(pub u64);

    #[allow(non_upper_case_globals)]
    impl RawMemoryDecompressionMethodFlagBitsNV {
        pub const eGdeflate10Bit: Self = Self(0b1);

        pub fn normalise(self) -> MemoryDecompressionMethodFlagBitsNV {
            match self {
                Self::eGdeflate10Bit => MemoryDecompressionMethodFlagBitsNV::eGdeflate10Bit,
                RawMemoryDecompressionMethodFlagBitsNV(b) => MemoryDecompressionMethodFlagBitsNV::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<MemoryDecompressionMethodFlagBitsNV> {
            match self {
                RawMemoryDecompressionMethodFlagBitsNV(0) => None,
                Self::eGdeflate10Bit => Some(MemoryDecompressionMethodFlagBitsNV::eGdeflate10Bit),
                RawMemoryDecompressionMethodFlagBitsNV(b) => Some(MemoryDecompressionMethodFlagBitsNV::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawMemoryDecompressionMethodFlagBitsNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawBuildMicromapFlagBitsEXT(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawBuildMicromapFlagBitsEXT {
        pub const ePreferFastTraceBit: Self = Self(0b1);
        pub const ePreferFastBuildBit: Self = Self(0b10);
        pub const eAllowCompactionBit: Self = Self(0b100);

        pub fn normalise(self) -> BuildMicromapFlagBitsEXT {
            match self {
                Self::ePreferFastTraceBit => BuildMicromapFlagBitsEXT::ePreferFastTraceBit,
                Self::ePreferFastBuildBit => BuildMicromapFlagBitsEXT::ePreferFastBuildBit,
                Self::eAllowCompactionBit => BuildMicromapFlagBitsEXT::eAllowCompactionBit,
                RawBuildMicromapFlagBitsEXT(b) => BuildMicromapFlagBitsEXT::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<BuildMicromapFlagBitsEXT> {
            match self {
                RawBuildMicromapFlagBitsEXT(0) => None,
                Self::ePreferFastTraceBit => Some(BuildMicromapFlagBitsEXT::ePreferFastTraceBit),
                Self::ePreferFastBuildBit => Some(BuildMicromapFlagBitsEXT::ePreferFastBuildBit),
                Self::eAllowCompactionBit => Some(BuildMicromapFlagBitsEXT::eAllowCompactionBit),
                RawBuildMicromapFlagBitsEXT(b) => Some(BuildMicromapFlagBitsEXT::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawBuildMicromapFlagBitsEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawMicromapCreateFlagBitsEXT(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawMicromapCreateFlagBitsEXT {
        pub const eDeviceAddressCaptureReplayBit: Self = Self(0b1);

        pub fn normalise(self) -> MicromapCreateFlagBitsEXT {
            match self {
                Self::eDeviceAddressCaptureReplayBit => MicromapCreateFlagBitsEXT::eDeviceAddressCaptureReplayBit,
                RawMicromapCreateFlagBitsEXT(b) => MicromapCreateFlagBitsEXT::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<MicromapCreateFlagBitsEXT> {
            match self {
                RawMicromapCreateFlagBitsEXT(0) => None,
                Self::eDeviceAddressCaptureReplayBit => Some(MicromapCreateFlagBitsEXT::eDeviceAddressCaptureReplayBit),
                RawMicromapCreateFlagBitsEXT(b) => Some(MicromapCreateFlagBitsEXT::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawMicromapCreateFlagBitsEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawCompositeAlphaFlagBitsKHR(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawCompositeAlphaFlagBitsKHR {
        pub const eOpaqueBit: Self = Self(0b1);
        pub const ePreMultipliedBit: Self = Self(0b10);
        pub const ePostMultipliedBit: Self = Self(0b100);
        pub const eInheritBit: Self = Self(0b1000);

        pub fn normalise(self) -> CompositeAlphaFlagBitsKHR {
            match self {
                Self::eOpaqueBit => CompositeAlphaFlagBitsKHR::eOpaqueBit,
                Self::ePreMultipliedBit => CompositeAlphaFlagBitsKHR::ePreMultipliedBit,
                Self::ePostMultipliedBit => CompositeAlphaFlagBitsKHR::ePostMultipliedBit,
                Self::eInheritBit => CompositeAlphaFlagBitsKHR::eInheritBit,
                RawCompositeAlphaFlagBitsKHR(b) => CompositeAlphaFlagBitsKHR::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<CompositeAlphaFlagBitsKHR> {
            match self {
                RawCompositeAlphaFlagBitsKHR(0) => None,
                Self::eOpaqueBit => Some(CompositeAlphaFlagBitsKHR::eOpaqueBit),
                Self::ePreMultipliedBit => Some(CompositeAlphaFlagBitsKHR::ePreMultipliedBit),
                Self::ePostMultipliedBit => Some(CompositeAlphaFlagBitsKHR::ePostMultipliedBit),
                Self::eInheritBit => Some(CompositeAlphaFlagBitsKHR::eInheritBit),
                RawCompositeAlphaFlagBitsKHR(b) => Some(CompositeAlphaFlagBitsKHR::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawCompositeAlphaFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawDisplayPlaneAlphaFlagBitsKHR(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawDisplayPlaneAlphaFlagBitsKHR {
        pub const eOpaqueBit: Self = Self(0b1);
        pub const eGlobalBit: Self = Self(0b10);
        pub const ePerPixelBit: Self = Self(0b100);
        pub const ePerPixelPremultipliedBit: Self = Self(0b1000);

        pub fn normalise(self) -> DisplayPlaneAlphaFlagBitsKHR {
            match self {
                Self::eOpaqueBit => DisplayPlaneAlphaFlagBitsKHR::eOpaqueBit,
                Self::eGlobalBit => DisplayPlaneAlphaFlagBitsKHR::eGlobalBit,
                Self::ePerPixelBit => DisplayPlaneAlphaFlagBitsKHR::ePerPixelBit,
                Self::ePerPixelPremultipliedBit => DisplayPlaneAlphaFlagBitsKHR::ePerPixelPremultipliedBit,
                RawDisplayPlaneAlphaFlagBitsKHR(b) => DisplayPlaneAlphaFlagBitsKHR::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<DisplayPlaneAlphaFlagBitsKHR> {
            match self {
                RawDisplayPlaneAlphaFlagBitsKHR(0) => None,
                Self::eOpaqueBit => Some(DisplayPlaneAlphaFlagBitsKHR::eOpaqueBit),
                Self::eGlobalBit => Some(DisplayPlaneAlphaFlagBitsKHR::eGlobalBit),
                Self::ePerPixelBit => Some(DisplayPlaneAlphaFlagBitsKHR::ePerPixelBit),
                Self::ePerPixelPremultipliedBit => Some(DisplayPlaneAlphaFlagBitsKHR::ePerPixelPremultipliedBit),
                RawDisplayPlaneAlphaFlagBitsKHR(b) => Some(DisplayPlaneAlphaFlagBitsKHR::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawDisplayPlaneAlphaFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawSurfaceTransformFlagBitsKHR(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawSurfaceTransformFlagBitsKHR {
        pub const eIdentityBit: Self = Self(0b1);
        pub const eRotate90Bit: Self = Self(0b10);
        pub const eRotate180Bit: Self = Self(0b100);
        pub const eRotate270Bit: Self = Self(0b1000);
        pub const eHorizontalMirrorBit: Self = Self(0b10000);
        pub const eHorizontalMirrorRotate90Bit: Self = Self(0b100000);
        pub const eHorizontalMirrorRotate180Bit: Self = Self(0b1000000);
        pub const eHorizontalMirrorRotate270Bit: Self = Self(0b10000000);
        pub const eInheritBit: Self = Self(0b100000000);

        pub fn normalise(self) -> SurfaceTransformFlagBitsKHR {
            match self {
                Self::eIdentityBit => SurfaceTransformFlagBitsKHR::eIdentityBit,
                Self::eRotate90Bit => SurfaceTransformFlagBitsKHR::eRotate90Bit,
                Self::eRotate180Bit => SurfaceTransformFlagBitsKHR::eRotate180Bit,
                Self::eRotate270Bit => SurfaceTransformFlagBitsKHR::eRotate270Bit,
                Self::eHorizontalMirrorBit => SurfaceTransformFlagBitsKHR::eHorizontalMirrorBit,
                Self::eHorizontalMirrorRotate90Bit => SurfaceTransformFlagBitsKHR::eHorizontalMirrorRotate90Bit,
                Self::eHorizontalMirrorRotate180Bit => SurfaceTransformFlagBitsKHR::eHorizontalMirrorRotate180Bit,
                Self::eHorizontalMirrorRotate270Bit => SurfaceTransformFlagBitsKHR::eHorizontalMirrorRotate270Bit,
                Self::eInheritBit => SurfaceTransformFlagBitsKHR::eInheritBit,
                RawSurfaceTransformFlagBitsKHR(b) => SurfaceTransformFlagBitsKHR::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<SurfaceTransformFlagBitsKHR> {
            match self {
                RawSurfaceTransformFlagBitsKHR(0) => None,
                Self::eIdentityBit => Some(SurfaceTransformFlagBitsKHR::eIdentityBit),
                Self::eRotate90Bit => Some(SurfaceTransformFlagBitsKHR::eRotate90Bit),
                Self::eRotate180Bit => Some(SurfaceTransformFlagBitsKHR::eRotate180Bit),
                Self::eRotate270Bit => Some(SurfaceTransformFlagBitsKHR::eRotate270Bit),
                Self::eHorizontalMirrorBit => Some(SurfaceTransformFlagBitsKHR::eHorizontalMirrorBit),
                Self::eHorizontalMirrorRotate90Bit => Some(SurfaceTransformFlagBitsKHR::eHorizontalMirrorRotate90Bit),
                Self::eHorizontalMirrorRotate180Bit => Some(SurfaceTransformFlagBitsKHR::eHorizontalMirrorRotate180Bit),
                Self::eHorizontalMirrorRotate270Bit => Some(SurfaceTransformFlagBitsKHR::eHorizontalMirrorRotate270Bit),
                Self::eInheritBit => Some(SurfaceTransformFlagBitsKHR::eInheritBit),
                RawSurfaceTransformFlagBitsKHR(b) => Some(SurfaceTransformFlagBitsKHR::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawSurfaceTransformFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawSwapchainCreateFlagBitsKHR(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawSwapchainCreateFlagBitsKHR {
        pub const eSplitInstanceBindRegionsBit: Self = Self(0b1);
        pub const eProtectedBit: Self = Self(0b10);
        pub const eMutableFormatBit: Self = Self(0b100);
        pub const eDeferredMemoryAllocationBitExt: Self = Self(0b1000);

        pub fn normalise(self) -> SwapchainCreateFlagBitsKHR {
            match self {
                Self::eSplitInstanceBindRegionsBit => SwapchainCreateFlagBitsKHR::eSplitInstanceBindRegionsBit,
                Self::eProtectedBit => SwapchainCreateFlagBitsKHR::eProtectedBit,
                Self::eMutableFormatBit => SwapchainCreateFlagBitsKHR::eMutableFormatBit,
                Self::eDeferredMemoryAllocationBitExt => SwapchainCreateFlagBitsKHR::eDeferredMemoryAllocationBitExt,
                RawSwapchainCreateFlagBitsKHR(b) => SwapchainCreateFlagBitsKHR::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<SwapchainCreateFlagBitsKHR> {
            match self {
                RawSwapchainCreateFlagBitsKHR(0) => None,
                Self::eSplitInstanceBindRegionsBit => Some(SwapchainCreateFlagBitsKHR::eSplitInstanceBindRegionsBit),
                Self::eProtectedBit => Some(SwapchainCreateFlagBitsKHR::eProtectedBit),
                Self::eMutableFormatBit => Some(SwapchainCreateFlagBitsKHR::eMutableFormatBit),
                Self::eDeferredMemoryAllocationBitExt => Some(SwapchainCreateFlagBitsKHR::eDeferredMemoryAllocationBitExt),
                RawSwapchainCreateFlagBitsKHR(b) => Some(SwapchainCreateFlagBitsKHR::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawSwapchainCreateFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawPeerMemoryFeatureFlagBits(pub u32);

    pub type RawPeerMemoryFeatureFlagBitsKHR = RawPeerMemoryFeatureFlagBits;

    #[allow(non_upper_case_globals)]
    impl RawPeerMemoryFeatureFlagBits {
        pub const eCopySrcBit: Self = Self(0b1);
        pub const eCopyDstBit: Self = Self(0b10);
        pub const eGenericSrcBit: Self = Self(0b100);
        pub const eGenericDstBit: Self = Self(0b1000);
        pub const eCopySrcBitKhr: Self = Self::eCopySrcBit;
        pub const eCopyDstBitKhr: Self = Self::eCopyDstBit;
        pub const eGenericSrcBitKhr: Self = Self::eGenericSrcBit;
        pub const eGenericDstBitKhr: Self = Self::eGenericDstBit;

        pub fn normalise(self) -> PeerMemoryFeatureFlagBits {
            match self {
                Self::eCopySrcBit => PeerMemoryFeatureFlagBits::eCopySrcBit,
                Self::eCopyDstBit => PeerMemoryFeatureFlagBits::eCopyDstBit,
                Self::eGenericSrcBit => PeerMemoryFeatureFlagBits::eGenericSrcBit,
                Self::eGenericDstBit => PeerMemoryFeatureFlagBits::eGenericDstBit,
                RawPeerMemoryFeatureFlagBits(b) => PeerMemoryFeatureFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<PeerMemoryFeatureFlagBits> {
            match self {
                RawPeerMemoryFeatureFlagBits(0) => None,
                Self::eCopySrcBit => Some(PeerMemoryFeatureFlagBits::eCopySrcBit),
                Self::eCopyDstBit => Some(PeerMemoryFeatureFlagBits::eCopyDstBit),
                Self::eGenericSrcBit => Some(PeerMemoryFeatureFlagBits::eGenericSrcBit),
                Self::eGenericDstBit => Some(PeerMemoryFeatureFlagBits::eGenericDstBit),
                RawPeerMemoryFeatureFlagBits(b) => Some(PeerMemoryFeatureFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawPeerMemoryFeatureFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawMemoryAllocateFlagBits(pub u32);

    pub type RawMemoryAllocateFlagBitsKHR = RawMemoryAllocateFlagBits;

    #[allow(non_upper_case_globals)]
    impl RawMemoryAllocateFlagBits {
        pub const eDeviceMaskBit: Self = Self(0b1);
        pub const eDeviceAddressBit: Self = Self(0b10);
        pub const eDeviceAddressCaptureReplayBit: Self = Self(0b100);
        pub const eDeviceMaskBitKhr: Self = Self::eDeviceMaskBit;
        pub const eDeviceAddressBitKhr: Self = Self::eDeviceAddressBit;
        pub const eDeviceAddressCaptureReplayBitKhr: Self = Self::eDeviceAddressCaptureReplayBit;

        pub fn normalise(self) -> MemoryAllocateFlagBits {
            match self {
                Self::eDeviceMaskBit => MemoryAllocateFlagBits::eDeviceMaskBit,
                Self::eDeviceAddressBit => MemoryAllocateFlagBits::eDeviceAddressBit,
                Self::eDeviceAddressCaptureReplayBit => MemoryAllocateFlagBits::eDeviceAddressCaptureReplayBit,
                RawMemoryAllocateFlagBits(b) => MemoryAllocateFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<MemoryAllocateFlagBits> {
            match self {
                RawMemoryAllocateFlagBits(0) => None,
                Self::eDeviceMaskBit => Some(MemoryAllocateFlagBits::eDeviceMaskBit),
                Self::eDeviceAddressBit => Some(MemoryAllocateFlagBits::eDeviceAddressBit),
                Self::eDeviceAddressCaptureReplayBit => Some(MemoryAllocateFlagBits::eDeviceAddressCaptureReplayBit),
                RawMemoryAllocateFlagBits(b) => Some(MemoryAllocateFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawMemoryAllocateFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawDeviceGroupPresentModeFlagBitsKHR(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawDeviceGroupPresentModeFlagBitsKHR {
        pub const eLocalBit: Self = Self(0b1);
        pub const eRemoteBit: Self = Self(0b10);
        pub const eSumBit: Self = Self(0b100);
        pub const eLocalMultiDeviceBit: Self = Self(0b1000);

        pub fn normalise(self) -> DeviceGroupPresentModeFlagBitsKHR {
            match self {
                Self::eLocalBit => DeviceGroupPresentModeFlagBitsKHR::eLocalBit,
                Self::eRemoteBit => DeviceGroupPresentModeFlagBitsKHR::eRemoteBit,
                Self::eSumBit => DeviceGroupPresentModeFlagBitsKHR::eSumBit,
                Self::eLocalMultiDeviceBit => DeviceGroupPresentModeFlagBitsKHR::eLocalMultiDeviceBit,
                RawDeviceGroupPresentModeFlagBitsKHR(b) => DeviceGroupPresentModeFlagBitsKHR::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<DeviceGroupPresentModeFlagBitsKHR> {
            match self {
                RawDeviceGroupPresentModeFlagBitsKHR(0) => None,
                Self::eLocalBit => Some(DeviceGroupPresentModeFlagBitsKHR::eLocalBit),
                Self::eRemoteBit => Some(DeviceGroupPresentModeFlagBitsKHR::eRemoteBit),
                Self::eSumBit => Some(DeviceGroupPresentModeFlagBitsKHR::eSumBit),
                Self::eLocalMultiDeviceBit => Some(DeviceGroupPresentModeFlagBitsKHR::eLocalMultiDeviceBit),
                RawDeviceGroupPresentModeFlagBitsKHR(b) => Some(DeviceGroupPresentModeFlagBitsKHR::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawDeviceGroupPresentModeFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawDebugReportFlagBitsEXT(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawDebugReportFlagBitsEXT {
        pub const eInformationBit: Self = Self(0b1);
        pub const eWarningBit: Self = Self(0b10);
        pub const ePerformanceWarningBit: Self = Self(0b100);
        pub const eErrorBit: Self = Self(0b1000);
        pub const eDebugBit: Self = Self(0b10000);

        pub fn normalise(self) -> DebugReportFlagBitsEXT {
            match self {
                Self::eInformationBit => DebugReportFlagBitsEXT::eInformationBit,
                Self::eWarningBit => DebugReportFlagBitsEXT::eWarningBit,
                Self::ePerformanceWarningBit => DebugReportFlagBitsEXT::ePerformanceWarningBit,
                Self::eErrorBit => DebugReportFlagBitsEXT::eErrorBit,
                Self::eDebugBit => DebugReportFlagBitsEXT::eDebugBit,
                RawDebugReportFlagBitsEXT(b) => DebugReportFlagBitsEXT::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<DebugReportFlagBitsEXT> {
            match self {
                RawDebugReportFlagBitsEXT(0) => None,
                Self::eInformationBit => Some(DebugReportFlagBitsEXT::eInformationBit),
                Self::eWarningBit => Some(DebugReportFlagBitsEXT::eWarningBit),
                Self::ePerformanceWarningBit => Some(DebugReportFlagBitsEXT::ePerformanceWarningBit),
                Self::eErrorBit => Some(DebugReportFlagBitsEXT::eErrorBit),
                Self::eDebugBit => Some(DebugReportFlagBitsEXT::eDebugBit),
                RawDebugReportFlagBitsEXT(b) => Some(DebugReportFlagBitsEXT::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawDebugReportFlagBitsEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawExternalMemoryHandleTypeFlagBitsNV(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawExternalMemoryHandleTypeFlagBitsNV {
        pub const eOpaqueWin32Bit: Self = Self(0b1);
        pub const eOpaqueWin32KmtBit: Self = Self(0b10);
        pub const eD3d11ImageBit: Self = Self(0b100);
        pub const eD3d11ImageKmtBit: Self = Self(0b1000);

        pub fn normalise(self) -> ExternalMemoryHandleTypeFlagBitsNV {
            match self {
                Self::eOpaqueWin32Bit => ExternalMemoryHandleTypeFlagBitsNV::eOpaqueWin32Bit,
                Self::eOpaqueWin32KmtBit => ExternalMemoryHandleTypeFlagBitsNV::eOpaqueWin32KmtBit,
                Self::eD3d11ImageBit => ExternalMemoryHandleTypeFlagBitsNV::eD3d11ImageBit,
                Self::eD3d11ImageKmtBit => ExternalMemoryHandleTypeFlagBitsNV::eD3d11ImageKmtBit,
                RawExternalMemoryHandleTypeFlagBitsNV(b) => ExternalMemoryHandleTypeFlagBitsNV::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<ExternalMemoryHandleTypeFlagBitsNV> {
            match self {
                RawExternalMemoryHandleTypeFlagBitsNV(0) => None,
                Self::eOpaqueWin32Bit => Some(ExternalMemoryHandleTypeFlagBitsNV::eOpaqueWin32Bit),
                Self::eOpaqueWin32KmtBit => Some(ExternalMemoryHandleTypeFlagBitsNV::eOpaqueWin32KmtBit),
                Self::eD3d11ImageBit => Some(ExternalMemoryHandleTypeFlagBitsNV::eD3d11ImageBit),
                Self::eD3d11ImageKmtBit => Some(ExternalMemoryHandleTypeFlagBitsNV::eD3d11ImageKmtBit),
                RawExternalMemoryHandleTypeFlagBitsNV(b) => Some(ExternalMemoryHandleTypeFlagBitsNV::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawExternalMemoryHandleTypeFlagBitsNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawExternalMemoryFeatureFlagBitsNV(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawExternalMemoryFeatureFlagBitsNV {
        pub const eDedicatedOnlyBit: Self = Self(0b1);
        pub const eExportableBit: Self = Self(0b10);
        pub const eImportableBit: Self = Self(0b100);

        pub fn normalise(self) -> ExternalMemoryFeatureFlagBitsNV {
            match self {
                Self::eDedicatedOnlyBit => ExternalMemoryFeatureFlagBitsNV::eDedicatedOnlyBit,
                Self::eExportableBit => ExternalMemoryFeatureFlagBitsNV::eExportableBit,
                Self::eImportableBit => ExternalMemoryFeatureFlagBitsNV::eImportableBit,
                RawExternalMemoryFeatureFlagBitsNV(b) => ExternalMemoryFeatureFlagBitsNV::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<ExternalMemoryFeatureFlagBitsNV> {
            match self {
                RawExternalMemoryFeatureFlagBitsNV(0) => None,
                Self::eDedicatedOnlyBit => Some(ExternalMemoryFeatureFlagBitsNV::eDedicatedOnlyBit),
                Self::eExportableBit => Some(ExternalMemoryFeatureFlagBitsNV::eExportableBit),
                Self::eImportableBit => Some(ExternalMemoryFeatureFlagBitsNV::eImportableBit),
                RawExternalMemoryFeatureFlagBitsNV(b) => Some(ExternalMemoryFeatureFlagBitsNV::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawExternalMemoryFeatureFlagBitsNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawExternalMemoryHandleTypeFlagBits(pub u32);

    pub type RawExternalMemoryHandleTypeFlagBitsKHR = RawExternalMemoryHandleTypeFlagBits;

    #[allow(non_upper_case_globals)]
    impl RawExternalMemoryHandleTypeFlagBits {
        pub const eOpaqueFdBit: Self = Self(0b1);
        pub const eOpaqueWin32Bit: Self = Self(0b10);
        pub const eOpaqueWin32KmtBit: Self = Self(0b100);
        pub const eD3d11TextureBit: Self = Self(0b1000);
        pub const eD3d11TextureKmtBit: Self = Self(0b10000);
        pub const eD3d12HeapBit: Self = Self(0b100000);
        pub const eD3d12ResourceBit: Self = Self(0b1000000);
        pub const eHostAllocationBitExt: Self = Self(0b10000000);
        pub const eHostMappedForeignMemoryBitExt: Self = Self(0b100000000);
        pub const eDmaBufBitExt: Self = Self(0b1000000000);
        pub const eAndroidHardwareBufferBitAndroid: Self = Self(0b10000000000);
        pub const eZirconVmoBitFuchsia: Self = Self(0b100000000000);
        pub const eRdmaAddressBitNv: Self = Self(0b1000000000000);
        pub const eSciBufBitNv: Self = Self(0b10000000000000);
        pub const eOpaqueFdBitKhr: Self = Self::eOpaqueFdBit;
        pub const eOpaqueWin32BitKhr: Self = Self::eOpaqueWin32Bit;
        pub const eOpaqueWin32KmtBitKhr: Self = Self::eOpaqueWin32KmtBit;
        pub const eD3d11TextureBitKhr: Self = Self::eD3d11TextureBit;
        pub const eD3d11TextureKmtBitKhr: Self = Self::eD3d11TextureKmtBit;
        pub const eD3d12HeapBitKhr: Self = Self::eD3d12HeapBit;
        pub const eD3d12ResourceBitKhr: Self = Self::eD3d12ResourceBit;

        pub fn normalise(self) -> ExternalMemoryHandleTypeFlagBits {
            match self {
                Self::eOpaqueFdBit => ExternalMemoryHandleTypeFlagBits::eOpaqueFdBit,
                Self::eOpaqueWin32Bit => ExternalMemoryHandleTypeFlagBits::eOpaqueWin32Bit,
                Self::eOpaqueWin32KmtBit => ExternalMemoryHandleTypeFlagBits::eOpaqueWin32KmtBit,
                Self::eD3d11TextureBit => ExternalMemoryHandleTypeFlagBits::eD3d11TextureBit,
                Self::eD3d11TextureKmtBit => ExternalMemoryHandleTypeFlagBits::eD3d11TextureKmtBit,
                Self::eD3d12HeapBit => ExternalMemoryHandleTypeFlagBits::eD3d12HeapBit,
                Self::eD3d12ResourceBit => ExternalMemoryHandleTypeFlagBits::eD3d12ResourceBit,
                Self::eHostAllocationBitExt => ExternalMemoryHandleTypeFlagBits::eHostAllocationBitExt,
                Self::eHostMappedForeignMemoryBitExt => ExternalMemoryHandleTypeFlagBits::eHostMappedForeignMemoryBitExt,
                Self::eDmaBufBitExt => ExternalMemoryHandleTypeFlagBits::eDmaBufBitExt,
                Self::eAndroidHardwareBufferBitAndroid => ExternalMemoryHandleTypeFlagBits::eAndroidHardwareBufferBitAndroid,
                Self::eZirconVmoBitFuchsia => ExternalMemoryHandleTypeFlagBits::eZirconVmoBitFuchsia,
                Self::eRdmaAddressBitNv => ExternalMemoryHandleTypeFlagBits::eRdmaAddressBitNv,
                Self::eSciBufBitNv => ExternalMemoryHandleTypeFlagBits::eSciBufBitNv,
                RawExternalMemoryHandleTypeFlagBits(b) => ExternalMemoryHandleTypeFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<ExternalMemoryHandleTypeFlagBits> {
            match self {
                RawExternalMemoryHandleTypeFlagBits(0) => None,
                Self::eOpaqueFdBit => Some(ExternalMemoryHandleTypeFlagBits::eOpaqueFdBit),
                Self::eOpaqueWin32Bit => Some(ExternalMemoryHandleTypeFlagBits::eOpaqueWin32Bit),
                Self::eOpaqueWin32KmtBit => Some(ExternalMemoryHandleTypeFlagBits::eOpaqueWin32KmtBit),
                Self::eD3d11TextureBit => Some(ExternalMemoryHandleTypeFlagBits::eD3d11TextureBit),
                Self::eD3d11TextureKmtBit => Some(ExternalMemoryHandleTypeFlagBits::eD3d11TextureKmtBit),
                Self::eD3d12HeapBit => Some(ExternalMemoryHandleTypeFlagBits::eD3d12HeapBit),
                Self::eD3d12ResourceBit => Some(ExternalMemoryHandleTypeFlagBits::eD3d12ResourceBit),
                Self::eHostAllocationBitExt => Some(ExternalMemoryHandleTypeFlagBits::eHostAllocationBitExt),
                Self::eHostMappedForeignMemoryBitExt => Some(ExternalMemoryHandleTypeFlagBits::eHostMappedForeignMemoryBitExt),
                Self::eDmaBufBitExt => Some(ExternalMemoryHandleTypeFlagBits::eDmaBufBitExt),
                Self::eAndroidHardwareBufferBitAndroid => Some(ExternalMemoryHandleTypeFlagBits::eAndroidHardwareBufferBitAndroid),
                Self::eZirconVmoBitFuchsia => Some(ExternalMemoryHandleTypeFlagBits::eZirconVmoBitFuchsia),
                Self::eRdmaAddressBitNv => Some(ExternalMemoryHandleTypeFlagBits::eRdmaAddressBitNv),
                Self::eSciBufBitNv => Some(ExternalMemoryHandleTypeFlagBits::eSciBufBitNv),
                RawExternalMemoryHandleTypeFlagBits(b) => Some(ExternalMemoryHandleTypeFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawExternalMemoryHandleTypeFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawExternalMemoryFeatureFlagBits(pub u32);

    pub type RawExternalMemoryFeatureFlagBitsKHR = RawExternalMemoryFeatureFlagBits;

    #[allow(non_upper_case_globals)]
    impl RawExternalMemoryFeatureFlagBits {
        pub const eDedicatedOnlyBit: Self = Self(0b1);
        pub const eExportableBit: Self = Self(0b10);
        pub const eImportableBit: Self = Self(0b100);
        pub const eDedicatedOnlyBitKhr: Self = Self::eDedicatedOnlyBit;
        pub const eExportableBitKhr: Self = Self::eExportableBit;
        pub const eImportableBitKhr: Self = Self::eImportableBit;

        pub fn normalise(self) -> ExternalMemoryFeatureFlagBits {
            match self {
                Self::eDedicatedOnlyBit => ExternalMemoryFeatureFlagBits::eDedicatedOnlyBit,
                Self::eExportableBit => ExternalMemoryFeatureFlagBits::eExportableBit,
                Self::eImportableBit => ExternalMemoryFeatureFlagBits::eImportableBit,
                RawExternalMemoryFeatureFlagBits(b) => ExternalMemoryFeatureFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<ExternalMemoryFeatureFlagBits> {
            match self {
                RawExternalMemoryFeatureFlagBits(0) => None,
                Self::eDedicatedOnlyBit => Some(ExternalMemoryFeatureFlagBits::eDedicatedOnlyBit),
                Self::eExportableBit => Some(ExternalMemoryFeatureFlagBits::eExportableBit),
                Self::eImportableBit => Some(ExternalMemoryFeatureFlagBits::eImportableBit),
                RawExternalMemoryFeatureFlagBits(b) => Some(ExternalMemoryFeatureFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawExternalMemoryFeatureFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawExternalSemaphoreHandleTypeFlagBits(pub u32);

    pub type RawExternalSemaphoreHandleTypeFlagBitsKHR = RawExternalSemaphoreHandleTypeFlagBits;

    #[allow(non_upper_case_globals)]
    impl RawExternalSemaphoreHandleTypeFlagBits {
        pub const eOpaqueFdBit: Self = Self(0b1);
        pub const eOpaqueWin32Bit: Self = Self(0b10);
        pub const eOpaqueWin32KmtBit: Self = Self(0b100);
        pub const eD3d12FenceBit: Self = Self(0b1000);
        pub const eSyncFdBit: Self = Self(0b10000);
        pub const eSciSyncObjBitNv: Self = Self(0b100000);
        pub const eZirconEventBitFuchsia: Self = Self(0b10000000);
        pub const eOpaqueFdBitKhr: Self = Self::eOpaqueFdBit;
        pub const eOpaqueWin32BitKhr: Self = Self::eOpaqueWin32Bit;
        pub const eOpaqueWin32KmtBitKhr: Self = Self::eOpaqueWin32KmtBit;
        pub const eD3d11FenceBit: Self = Self::eD3d12FenceBit;
        pub const eD3d12FenceBitKhr: Self = Self::eD3d12FenceBit;
        pub const eSyncFdBitKhr: Self = Self::eSyncFdBit;

        pub fn normalise(self) -> ExternalSemaphoreHandleTypeFlagBits {
            match self {
                Self::eOpaqueFdBit => ExternalSemaphoreHandleTypeFlagBits::eOpaqueFdBit,
                Self::eOpaqueWin32Bit => ExternalSemaphoreHandleTypeFlagBits::eOpaqueWin32Bit,
                Self::eOpaqueWin32KmtBit => ExternalSemaphoreHandleTypeFlagBits::eOpaqueWin32KmtBit,
                Self::eD3d12FenceBit => ExternalSemaphoreHandleTypeFlagBits::eD3d12FenceBit,
                Self::eSyncFdBit => ExternalSemaphoreHandleTypeFlagBits::eSyncFdBit,
                Self::eSciSyncObjBitNv => ExternalSemaphoreHandleTypeFlagBits::eSciSyncObjBitNv,
                Self::eZirconEventBitFuchsia => ExternalSemaphoreHandleTypeFlagBits::eZirconEventBitFuchsia,
                RawExternalSemaphoreHandleTypeFlagBits(b) => ExternalSemaphoreHandleTypeFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<ExternalSemaphoreHandleTypeFlagBits> {
            match self {
                RawExternalSemaphoreHandleTypeFlagBits(0) => None,
                Self::eOpaqueFdBit => Some(ExternalSemaphoreHandleTypeFlagBits::eOpaqueFdBit),
                Self::eOpaqueWin32Bit => Some(ExternalSemaphoreHandleTypeFlagBits::eOpaqueWin32Bit),
                Self::eOpaqueWin32KmtBit => Some(ExternalSemaphoreHandleTypeFlagBits::eOpaqueWin32KmtBit),
                Self::eD3d12FenceBit => Some(ExternalSemaphoreHandleTypeFlagBits::eD3d12FenceBit),
                Self::eSyncFdBit => Some(ExternalSemaphoreHandleTypeFlagBits::eSyncFdBit),
                Self::eSciSyncObjBitNv => Some(ExternalSemaphoreHandleTypeFlagBits::eSciSyncObjBitNv),
                Self::eZirconEventBitFuchsia => Some(ExternalSemaphoreHandleTypeFlagBits::eZirconEventBitFuchsia),
                RawExternalSemaphoreHandleTypeFlagBits(b) => Some(ExternalSemaphoreHandleTypeFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawExternalSemaphoreHandleTypeFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawExternalSemaphoreFeatureFlagBits(pub u32);

    pub type RawExternalSemaphoreFeatureFlagBitsKHR = RawExternalSemaphoreFeatureFlagBits;

    #[allow(non_upper_case_globals)]
    impl RawExternalSemaphoreFeatureFlagBits {
        pub const eExportableBit: Self = Self(0b1);
        pub const eImportableBit: Self = Self(0b10);
        pub const eExportableBitKhr: Self = Self::eExportableBit;
        pub const eImportableBitKhr: Self = Self::eImportableBit;

        pub fn normalise(self) -> ExternalSemaphoreFeatureFlagBits {
            match self {
                Self::eExportableBit => ExternalSemaphoreFeatureFlagBits::eExportableBit,
                Self::eImportableBit => ExternalSemaphoreFeatureFlagBits::eImportableBit,
                RawExternalSemaphoreFeatureFlagBits(b) => ExternalSemaphoreFeatureFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<ExternalSemaphoreFeatureFlagBits> {
            match self {
                RawExternalSemaphoreFeatureFlagBits(0) => None,
                Self::eExportableBit => Some(ExternalSemaphoreFeatureFlagBits::eExportableBit),
                Self::eImportableBit => Some(ExternalSemaphoreFeatureFlagBits::eImportableBit),
                RawExternalSemaphoreFeatureFlagBits(b) => Some(ExternalSemaphoreFeatureFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawExternalSemaphoreFeatureFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawSemaphoreImportFlagBits(pub u32);

    pub type RawSemaphoreImportFlagBitsKHR = RawSemaphoreImportFlagBits;

    #[allow(non_upper_case_globals)]
    impl RawSemaphoreImportFlagBits {
        pub const eTemporaryBit: Self = Self(0b1);
        pub const eTemporaryBitKhr: Self = Self::eTemporaryBit;

        pub fn normalise(self) -> SemaphoreImportFlagBits {
            match self {
                Self::eTemporaryBit => SemaphoreImportFlagBits::eTemporaryBit,
                RawSemaphoreImportFlagBits(b) => SemaphoreImportFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<SemaphoreImportFlagBits> {
            match self {
                RawSemaphoreImportFlagBits(0) => None,
                Self::eTemporaryBit => Some(SemaphoreImportFlagBits::eTemporaryBit),
                RawSemaphoreImportFlagBits(b) => Some(SemaphoreImportFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawSemaphoreImportFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawExternalFenceHandleTypeFlagBits(pub u32);

    pub type RawExternalFenceHandleTypeFlagBitsKHR = RawExternalFenceHandleTypeFlagBits;

    #[allow(non_upper_case_globals)]
    impl RawExternalFenceHandleTypeFlagBits {
        pub const eOpaqueFdBit: Self = Self(0b1);
        pub const eOpaqueWin32Bit: Self = Self(0b10);
        pub const eOpaqueWin32KmtBit: Self = Self(0b100);
        pub const eSyncFdBit: Self = Self(0b1000);
        pub const eSciSyncObjBitNv: Self = Self(0b10000);
        pub const eSciSyncFenceBitNv: Self = Self(0b100000);
        pub const eOpaqueFdBitKhr: Self = Self::eOpaqueFdBit;
        pub const eOpaqueWin32BitKhr: Self = Self::eOpaqueWin32Bit;
        pub const eOpaqueWin32KmtBitKhr: Self = Self::eOpaqueWin32KmtBit;
        pub const eSyncFdBitKhr: Self = Self::eSyncFdBit;

        pub fn normalise(self) -> ExternalFenceHandleTypeFlagBits {
            match self {
                Self::eOpaqueFdBit => ExternalFenceHandleTypeFlagBits::eOpaqueFdBit,
                Self::eOpaqueWin32Bit => ExternalFenceHandleTypeFlagBits::eOpaqueWin32Bit,
                Self::eOpaqueWin32KmtBit => ExternalFenceHandleTypeFlagBits::eOpaqueWin32KmtBit,
                Self::eSyncFdBit => ExternalFenceHandleTypeFlagBits::eSyncFdBit,
                Self::eSciSyncObjBitNv => ExternalFenceHandleTypeFlagBits::eSciSyncObjBitNv,
                Self::eSciSyncFenceBitNv => ExternalFenceHandleTypeFlagBits::eSciSyncFenceBitNv,
                RawExternalFenceHandleTypeFlagBits(b) => ExternalFenceHandleTypeFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<ExternalFenceHandleTypeFlagBits> {
            match self {
                RawExternalFenceHandleTypeFlagBits(0) => None,
                Self::eOpaqueFdBit => Some(ExternalFenceHandleTypeFlagBits::eOpaqueFdBit),
                Self::eOpaqueWin32Bit => Some(ExternalFenceHandleTypeFlagBits::eOpaqueWin32Bit),
                Self::eOpaqueWin32KmtBit => Some(ExternalFenceHandleTypeFlagBits::eOpaqueWin32KmtBit),
                Self::eSyncFdBit => Some(ExternalFenceHandleTypeFlagBits::eSyncFdBit),
                Self::eSciSyncObjBitNv => Some(ExternalFenceHandleTypeFlagBits::eSciSyncObjBitNv),
                Self::eSciSyncFenceBitNv => Some(ExternalFenceHandleTypeFlagBits::eSciSyncFenceBitNv),
                RawExternalFenceHandleTypeFlagBits(b) => Some(ExternalFenceHandleTypeFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawExternalFenceHandleTypeFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawExternalFenceFeatureFlagBits(pub u32);

    pub type RawExternalFenceFeatureFlagBitsKHR = RawExternalFenceFeatureFlagBits;

    #[allow(non_upper_case_globals)]
    impl RawExternalFenceFeatureFlagBits {
        pub const eExportableBit: Self = Self(0b1);
        pub const eImportableBit: Self = Self(0b10);
        pub const eExportableBitKhr: Self = Self::eExportableBit;
        pub const eImportableBitKhr: Self = Self::eImportableBit;

        pub fn normalise(self) -> ExternalFenceFeatureFlagBits {
            match self {
                Self::eExportableBit => ExternalFenceFeatureFlagBits::eExportableBit,
                Self::eImportableBit => ExternalFenceFeatureFlagBits::eImportableBit,
                RawExternalFenceFeatureFlagBits(b) => ExternalFenceFeatureFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<ExternalFenceFeatureFlagBits> {
            match self {
                RawExternalFenceFeatureFlagBits(0) => None,
                Self::eExportableBit => Some(ExternalFenceFeatureFlagBits::eExportableBit),
                Self::eImportableBit => Some(ExternalFenceFeatureFlagBits::eImportableBit),
                RawExternalFenceFeatureFlagBits(b) => Some(ExternalFenceFeatureFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawExternalFenceFeatureFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawFenceImportFlagBits(pub u32);

    pub type RawFenceImportFlagBitsKHR = RawFenceImportFlagBits;

    #[allow(non_upper_case_globals)]
    impl RawFenceImportFlagBits {
        pub const eTemporaryBit: Self = Self(0b1);
        pub const eTemporaryBitKhr: Self = Self::eTemporaryBit;

        pub fn normalise(self) -> FenceImportFlagBits {
            match self {
                Self::eTemporaryBit => FenceImportFlagBits::eTemporaryBit,
                RawFenceImportFlagBits(b) => FenceImportFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<FenceImportFlagBits> {
            match self {
                RawFenceImportFlagBits(0) => None,
                Self::eTemporaryBit => Some(FenceImportFlagBits::eTemporaryBit),
                RawFenceImportFlagBits(b) => Some(FenceImportFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawFenceImportFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawSurfaceCounterFlagBitsEXT(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawSurfaceCounterFlagBitsEXT {
        pub const eVblankBit: Self = Self(0b1);
        pub const eVblank: Self = Self::eVblankBit;

        pub fn normalise(self) -> SurfaceCounterFlagBitsEXT {
            match self {
                Self::eVblankBit => SurfaceCounterFlagBitsEXT::eVblankBit,
                RawSurfaceCounterFlagBitsEXT(b) => SurfaceCounterFlagBitsEXT::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<SurfaceCounterFlagBitsEXT> {
            match self {
                RawSurfaceCounterFlagBitsEXT(0) => None,
                Self::eVblankBit => Some(SurfaceCounterFlagBitsEXT::eVblankBit),
                RawSurfaceCounterFlagBitsEXT(b) => Some(SurfaceCounterFlagBitsEXT::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawSurfaceCounterFlagBitsEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawDebugUtilsMessageSeverityFlagBitsEXT(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawDebugUtilsMessageSeverityFlagBitsEXT {
        pub const eVerboseBit: Self = Self(0b1);
        pub const eInfoBit: Self = Self(0b10000);
        pub const eWarningBit: Self = Self(0b100000000);
        pub const eErrorBit: Self = Self(0b1000000000000);

        pub fn normalise(self) -> DebugUtilsMessageSeverityFlagBitsEXT {
            match self {
                Self::eVerboseBit => DebugUtilsMessageSeverityFlagBitsEXT::eVerboseBit,
                Self::eInfoBit => DebugUtilsMessageSeverityFlagBitsEXT::eInfoBit,
                Self::eWarningBit => DebugUtilsMessageSeverityFlagBitsEXT::eWarningBit,
                Self::eErrorBit => DebugUtilsMessageSeverityFlagBitsEXT::eErrorBit,
                RawDebugUtilsMessageSeverityFlagBitsEXT(b) => DebugUtilsMessageSeverityFlagBitsEXT::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<DebugUtilsMessageSeverityFlagBitsEXT> {
            match self {
                RawDebugUtilsMessageSeverityFlagBitsEXT(0) => None,
                Self::eVerboseBit => Some(DebugUtilsMessageSeverityFlagBitsEXT::eVerboseBit),
                Self::eInfoBit => Some(DebugUtilsMessageSeverityFlagBitsEXT::eInfoBit),
                Self::eWarningBit => Some(DebugUtilsMessageSeverityFlagBitsEXT::eWarningBit),
                Self::eErrorBit => Some(DebugUtilsMessageSeverityFlagBitsEXT::eErrorBit),
                RawDebugUtilsMessageSeverityFlagBitsEXT(b) => Some(DebugUtilsMessageSeverityFlagBitsEXT::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawDebugUtilsMessageSeverityFlagBitsEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawDebugUtilsMessageTypeFlagBitsEXT(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawDebugUtilsMessageTypeFlagBitsEXT {
        pub const eGeneralBit: Self = Self(0b1);
        pub const eValidationBit: Self = Self(0b10);
        pub const ePerformanceBit: Self = Self(0b100);
        pub const eDeviceAddressBindingBit: Self = Self(0b1000);

        pub fn normalise(self) -> DebugUtilsMessageTypeFlagBitsEXT {
            match self {
                Self::eGeneralBit => DebugUtilsMessageTypeFlagBitsEXT::eGeneralBit,
                Self::eValidationBit => DebugUtilsMessageTypeFlagBitsEXT::eValidationBit,
                Self::ePerformanceBit => DebugUtilsMessageTypeFlagBitsEXT::ePerformanceBit,
                Self::eDeviceAddressBindingBit => DebugUtilsMessageTypeFlagBitsEXT::eDeviceAddressBindingBit,
                RawDebugUtilsMessageTypeFlagBitsEXT(b) => DebugUtilsMessageTypeFlagBitsEXT::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<DebugUtilsMessageTypeFlagBitsEXT> {
            match self {
                RawDebugUtilsMessageTypeFlagBitsEXT(0) => None,
                Self::eGeneralBit => Some(DebugUtilsMessageTypeFlagBitsEXT::eGeneralBit),
                Self::eValidationBit => Some(DebugUtilsMessageTypeFlagBitsEXT::eValidationBit),
                Self::ePerformanceBit => Some(DebugUtilsMessageTypeFlagBitsEXT::ePerformanceBit),
                Self::eDeviceAddressBindingBit => Some(DebugUtilsMessageTypeFlagBitsEXT::eDeviceAddressBindingBit),
                RawDebugUtilsMessageTypeFlagBitsEXT(b) => Some(DebugUtilsMessageTypeFlagBitsEXT::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawDebugUtilsMessageTypeFlagBitsEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawDescriptorBindingFlagBits(pub u32);

    pub type RawDescriptorBindingFlagBitsEXT = RawDescriptorBindingFlagBits;

    #[allow(non_upper_case_globals)]
    impl RawDescriptorBindingFlagBits {
        pub const eUpdateAfterBindBit: Self = Self(0b1);
        pub const eUpdateUnusedWhilePendingBit: Self = Self(0b10);
        pub const ePartiallyBoundBit: Self = Self(0b100);
        pub const eVariableDescriptorCountBit: Self = Self(0b1000);
        pub const eUpdateAfterBindBitExt: Self = Self::eUpdateAfterBindBit;
        pub const eUpdateUnusedWhilePendingBitExt: Self = Self::eUpdateUnusedWhilePendingBit;
        pub const ePartiallyBoundBitExt: Self = Self::ePartiallyBoundBit;
        pub const eVariableDescriptorCountBitExt: Self = Self::eVariableDescriptorCountBit;

        pub fn normalise(self) -> DescriptorBindingFlagBits {
            match self {
                Self::eUpdateAfterBindBit => DescriptorBindingFlagBits::eUpdateAfterBindBit,
                Self::eUpdateUnusedWhilePendingBit => DescriptorBindingFlagBits::eUpdateUnusedWhilePendingBit,
                Self::ePartiallyBoundBit => DescriptorBindingFlagBits::ePartiallyBoundBit,
                Self::eVariableDescriptorCountBit => DescriptorBindingFlagBits::eVariableDescriptorCountBit,
                RawDescriptorBindingFlagBits(b) => DescriptorBindingFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<DescriptorBindingFlagBits> {
            match self {
                RawDescriptorBindingFlagBits(0) => None,
                Self::eUpdateAfterBindBit => Some(DescriptorBindingFlagBits::eUpdateAfterBindBit),
                Self::eUpdateUnusedWhilePendingBit => Some(DescriptorBindingFlagBits::eUpdateUnusedWhilePendingBit),
                Self::ePartiallyBoundBit => Some(DescriptorBindingFlagBits::ePartiallyBoundBit),
                Self::eVariableDescriptorCountBit => Some(DescriptorBindingFlagBits::eVariableDescriptorCountBit),
                RawDescriptorBindingFlagBits(b) => Some(DescriptorBindingFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawDescriptorBindingFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawConditionalRenderingFlagBitsEXT(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawConditionalRenderingFlagBitsEXT {
        pub const eInvertedBit: Self = Self(0b1);

        pub fn normalise(self) -> ConditionalRenderingFlagBitsEXT {
            match self {
                Self::eInvertedBit => ConditionalRenderingFlagBitsEXT::eInvertedBit,
                RawConditionalRenderingFlagBitsEXT(b) => ConditionalRenderingFlagBitsEXT::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<ConditionalRenderingFlagBitsEXT> {
            match self {
                RawConditionalRenderingFlagBitsEXT(0) => None,
                Self::eInvertedBit => Some(ConditionalRenderingFlagBitsEXT::eInvertedBit),
                RawConditionalRenderingFlagBitsEXT(b) => Some(ConditionalRenderingFlagBitsEXT::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawConditionalRenderingFlagBitsEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawResolveModeFlagBits(pub u32);

    pub type RawResolveModeFlagBitsKHR = RawResolveModeFlagBits;

    #[allow(non_upper_case_globals)]
    impl RawResolveModeFlagBits {
        pub const eNone: Self = Self(0);
        pub const eSampleZeroBit: Self = Self(0b1);
        pub const eAverageBit: Self = Self(0b10);
        pub const eMinBit: Self = Self(0b100);
        pub const eMaxBit: Self = Self(0b1000);
        pub const eNoneKhr: Self = Self::eNone;
        pub const eSampleZeroBitKhr: Self = Self::eSampleZeroBit;
        pub const eAverageBitKhr: Self = Self::eAverageBit;
        pub const eMinBitKhr: Self = Self::eMinBit;
        pub const eMaxBitKhr: Self = Self::eMaxBit;

        pub fn normalise(self) -> ResolveModeFlagBits {
            match self {
                Self::eNone => ResolveModeFlagBits::eNone,
                Self::eSampleZeroBit => ResolveModeFlagBits::eSampleZeroBit,
                Self::eAverageBit => ResolveModeFlagBits::eAverageBit,
                Self::eMinBit => ResolveModeFlagBits::eMinBit,
                Self::eMaxBit => ResolveModeFlagBits::eMaxBit,
                RawResolveModeFlagBits(b) => ResolveModeFlagBits::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawResolveModeFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawToolPurposeFlagBits(pub u32);

    pub type RawToolPurposeFlagBitsEXT = RawToolPurposeFlagBits;

    #[allow(non_upper_case_globals)]
    impl RawToolPurposeFlagBits {
        pub const eValidationBit: Self = Self(0b1);
        pub const eProfilingBit: Self = Self(0b10);
        pub const eTracingBit: Self = Self(0b100);
        pub const eAdditionalFeaturesBit: Self = Self(0b1000);
        pub const eModifyingFeaturesBit: Self = Self(0b10000);
        pub const eDebugReportingBitExt: Self = Self(0b100000);
        pub const eDebugMarkersBitExt: Self = Self(0b1000000);
        pub const eValidationBitExt: Self = Self::eValidationBit;
        pub const eProfilingBitExt: Self = Self::eProfilingBit;
        pub const eTracingBitExt: Self = Self::eTracingBit;
        pub const eAdditionalFeaturesBitExt: Self = Self::eAdditionalFeaturesBit;
        pub const eModifyingFeaturesBitExt: Self = Self::eModifyingFeaturesBit;

        pub fn normalise(self) -> ToolPurposeFlagBits {
            match self {
                Self::eValidationBit => ToolPurposeFlagBits::eValidationBit,
                Self::eProfilingBit => ToolPurposeFlagBits::eProfilingBit,
                Self::eTracingBit => ToolPurposeFlagBits::eTracingBit,
                Self::eAdditionalFeaturesBit => ToolPurposeFlagBits::eAdditionalFeaturesBit,
                Self::eModifyingFeaturesBit => ToolPurposeFlagBits::eModifyingFeaturesBit,
                Self::eDebugReportingBitExt => ToolPurposeFlagBits::eDebugReportingBitExt,
                Self::eDebugMarkersBitExt => ToolPurposeFlagBits::eDebugMarkersBitExt,
                RawToolPurposeFlagBits(b) => ToolPurposeFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<ToolPurposeFlagBits> {
            match self {
                RawToolPurposeFlagBits(0) => None,
                Self::eValidationBit => Some(ToolPurposeFlagBits::eValidationBit),
                Self::eProfilingBit => Some(ToolPurposeFlagBits::eProfilingBit),
                Self::eTracingBit => Some(ToolPurposeFlagBits::eTracingBit),
                Self::eAdditionalFeaturesBit => Some(ToolPurposeFlagBits::eAdditionalFeaturesBit),
                Self::eModifyingFeaturesBit => Some(ToolPurposeFlagBits::eModifyingFeaturesBit),
                Self::eDebugReportingBitExt => Some(ToolPurposeFlagBits::eDebugReportingBitExt),
                Self::eDebugMarkersBitExt => Some(ToolPurposeFlagBits::eDebugMarkersBitExt),
                RawToolPurposeFlagBits(b) => Some(ToolPurposeFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawToolPurposeFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawSubmitFlagBits(pub u32);

    pub type RawSubmitFlagBitsKHR = RawSubmitFlagBits;

    #[allow(non_upper_case_globals)]
    impl RawSubmitFlagBits {
        pub const eProtectedBit: Self = Self(0b1);
        pub const eProtectedBitKhr: Self = Self::eProtectedBit;

        pub fn normalise(self) -> SubmitFlagBits {
            match self {
                Self::eProtectedBit => SubmitFlagBits::eProtectedBit,
                RawSubmitFlagBits(b) => SubmitFlagBits::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<SubmitFlagBits> {
            match self {
                RawSubmitFlagBits(0) => None,
                Self::eProtectedBit => Some(SubmitFlagBits::eProtectedBit),
                RawSubmitFlagBits(b) => Some(SubmitFlagBits::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawSubmitFlagBits {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawImageConstraintsInfoFlagBitsFUCHSIA(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawImageConstraintsInfoFlagBitsFUCHSIA {
        pub const eCpuReadRarely: Self = Self(0b1);
        pub const eCpuReadOften: Self = Self(0b10);
        pub const eCpuWriteRarely: Self = Self(0b100);
        pub const eCpuWriteOften: Self = Self(0b1000);
        pub const eProtectedOptional: Self = Self(0b10000);

        pub fn normalise(self) -> ImageConstraintsInfoFlagBitsFUCHSIA {
            match self {
                Self::eCpuReadRarely => ImageConstraintsInfoFlagBitsFUCHSIA::eCpuReadRarely,
                Self::eCpuReadOften => ImageConstraintsInfoFlagBitsFUCHSIA::eCpuReadOften,
                Self::eCpuWriteRarely => ImageConstraintsInfoFlagBitsFUCHSIA::eCpuWriteRarely,
                Self::eCpuWriteOften => ImageConstraintsInfoFlagBitsFUCHSIA::eCpuWriteOften,
                Self::eProtectedOptional => ImageConstraintsInfoFlagBitsFUCHSIA::eProtectedOptional,
                RawImageConstraintsInfoFlagBitsFUCHSIA(b) => ImageConstraintsInfoFlagBitsFUCHSIA::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<ImageConstraintsInfoFlagBitsFUCHSIA> {
            match self {
                RawImageConstraintsInfoFlagBitsFUCHSIA(0) => None,
                Self::eCpuReadRarely => Some(ImageConstraintsInfoFlagBitsFUCHSIA::eCpuReadRarely),
                Self::eCpuReadOften => Some(ImageConstraintsInfoFlagBitsFUCHSIA::eCpuReadOften),
                Self::eCpuWriteRarely => Some(ImageConstraintsInfoFlagBitsFUCHSIA::eCpuWriteRarely),
                Self::eCpuWriteOften => Some(ImageConstraintsInfoFlagBitsFUCHSIA::eCpuWriteOften),
                Self::eProtectedOptional => Some(ImageConstraintsInfoFlagBitsFUCHSIA::eProtectedOptional),
                RawImageConstraintsInfoFlagBitsFUCHSIA(b) => Some(ImageConstraintsInfoFlagBitsFUCHSIA::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawImageConstraintsInfoFlagBitsFUCHSIA {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawGraphicsPipelineLibraryFlagBitsEXT(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawGraphicsPipelineLibraryFlagBitsEXT {
        pub const eVertexInputInterfaceBit: Self = Self(0b1);
        pub const ePreRasterizationShadersBit: Self = Self(0b10);
        pub const eFragmentShaderBit: Self = Self(0b100);
        pub const eFragmentOutputInterfaceBit: Self = Self(0b1000);

        pub fn normalise(self) -> GraphicsPipelineLibraryFlagBitsEXT {
            match self {
                Self::eVertexInputInterfaceBit => GraphicsPipelineLibraryFlagBitsEXT::eVertexInputInterfaceBit,
                Self::ePreRasterizationShadersBit => GraphicsPipelineLibraryFlagBitsEXT::ePreRasterizationShadersBit,
                Self::eFragmentShaderBit => GraphicsPipelineLibraryFlagBitsEXT::eFragmentShaderBit,
                Self::eFragmentOutputInterfaceBit => GraphicsPipelineLibraryFlagBitsEXT::eFragmentOutputInterfaceBit,
                RawGraphicsPipelineLibraryFlagBitsEXT(b) => GraphicsPipelineLibraryFlagBitsEXT::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<GraphicsPipelineLibraryFlagBitsEXT> {
            match self {
                RawGraphicsPipelineLibraryFlagBitsEXT(0) => None,
                Self::eVertexInputInterfaceBit => Some(GraphicsPipelineLibraryFlagBitsEXT::eVertexInputInterfaceBit),
                Self::ePreRasterizationShadersBit => Some(GraphicsPipelineLibraryFlagBitsEXT::ePreRasterizationShadersBit),
                Self::eFragmentShaderBit => Some(GraphicsPipelineLibraryFlagBitsEXT::eFragmentShaderBit),
                Self::eFragmentOutputInterfaceBit => Some(GraphicsPipelineLibraryFlagBitsEXT::eFragmentOutputInterfaceBit),
                RawGraphicsPipelineLibraryFlagBitsEXT(b) => Some(GraphicsPipelineLibraryFlagBitsEXT::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawGraphicsPipelineLibraryFlagBitsEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawImageCompressionFlagBitsEXT(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawImageCompressionFlagBitsEXT {
        pub const eDefault: Self = Self(0);
        pub const eFixedRateDefault: Self = Self(0b1);
        pub const eFixedRateExplicit: Self = Self(0b10);
        pub const eDisabled: Self = Self(0b100);

        pub fn normalise(self) -> ImageCompressionFlagBitsEXT {
            match self {
                Self::eDefault => ImageCompressionFlagBitsEXT::eDefault,
                Self::eFixedRateDefault => ImageCompressionFlagBitsEXT::eFixedRateDefault,
                Self::eFixedRateExplicit => ImageCompressionFlagBitsEXT::eFixedRateExplicit,
                Self::eDisabled => ImageCompressionFlagBitsEXT::eDisabled,
                RawImageCompressionFlagBitsEXT(b) => ImageCompressionFlagBitsEXT::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawImageCompressionFlagBitsEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawImageCompressionFixedRateFlagBitsEXT(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawImageCompressionFixedRateFlagBitsEXT {
        pub const eNone: Self = Self(0);
        pub const e1bpcBit: Self = Self(0b1);
        pub const e2bpcBit: Self = Self(0b10);
        pub const e3bpcBit: Self = Self(0b100);
        pub const e4bpcBit: Self = Self(0b1000);
        pub const e5bpcBit: Self = Self(0b10000);
        pub const e6bpcBit: Self = Self(0b100000);
        pub const e7bpcBit: Self = Self(0b1000000);
        pub const e8bpcBit: Self = Self(0b10000000);
        pub const e9bpcBit: Self = Self(0b100000000);
        pub const e10bpcBit: Self = Self(0b1000000000);
        pub const e11bpcBit: Self = Self(0b10000000000);
        pub const e12bpcBit: Self = Self(0b100000000000);
        pub const e13bpcBit: Self = Self(0b1000000000000);
        pub const e14bpcBit: Self = Self(0b10000000000000);
        pub const e15bpcBit: Self = Self(0b100000000000000);
        pub const e16bpcBit: Self = Self(0b1000000000000000);
        pub const e17bpcBit: Self = Self(0b10000000000000000);
        pub const e18bpcBit: Self = Self(0b100000000000000000);
        pub const e19bpcBit: Self = Self(0b1000000000000000000);
        pub const e20bpcBit: Self = Self(0b10000000000000000000);
        pub const e21bpcBit: Self = Self(0b100000000000000000000);
        pub const e22bpcBit: Self = Self(0b1000000000000000000000);
        pub const e23bpcBit: Self = Self(0b10000000000000000000000);
        pub const e24bpcBit: Self = Self(0b100000000000000000000000);

        pub fn normalise(self) -> ImageCompressionFixedRateFlagBitsEXT {
            match self {
                Self::eNone => ImageCompressionFixedRateFlagBitsEXT::eNone,
                Self::e1bpcBit => ImageCompressionFixedRateFlagBitsEXT::e1bpcBit,
                Self::e2bpcBit => ImageCompressionFixedRateFlagBitsEXT::e2bpcBit,
                Self::e3bpcBit => ImageCompressionFixedRateFlagBitsEXT::e3bpcBit,
                Self::e4bpcBit => ImageCompressionFixedRateFlagBitsEXT::e4bpcBit,
                Self::e5bpcBit => ImageCompressionFixedRateFlagBitsEXT::e5bpcBit,
                Self::e6bpcBit => ImageCompressionFixedRateFlagBitsEXT::e6bpcBit,
                Self::e7bpcBit => ImageCompressionFixedRateFlagBitsEXT::e7bpcBit,
                Self::e8bpcBit => ImageCompressionFixedRateFlagBitsEXT::e8bpcBit,
                Self::e9bpcBit => ImageCompressionFixedRateFlagBitsEXT::e9bpcBit,
                Self::e10bpcBit => ImageCompressionFixedRateFlagBitsEXT::e10bpcBit,
                Self::e11bpcBit => ImageCompressionFixedRateFlagBitsEXT::e11bpcBit,
                Self::e12bpcBit => ImageCompressionFixedRateFlagBitsEXT::e12bpcBit,
                Self::e13bpcBit => ImageCompressionFixedRateFlagBitsEXT::e13bpcBit,
                Self::e14bpcBit => ImageCompressionFixedRateFlagBitsEXT::e14bpcBit,
                Self::e15bpcBit => ImageCompressionFixedRateFlagBitsEXT::e15bpcBit,
                Self::e16bpcBit => ImageCompressionFixedRateFlagBitsEXT::e16bpcBit,
                Self::e17bpcBit => ImageCompressionFixedRateFlagBitsEXT::e17bpcBit,
                Self::e18bpcBit => ImageCompressionFixedRateFlagBitsEXT::e18bpcBit,
                Self::e19bpcBit => ImageCompressionFixedRateFlagBitsEXT::e19bpcBit,
                Self::e20bpcBit => ImageCompressionFixedRateFlagBitsEXT::e20bpcBit,
                Self::e21bpcBit => ImageCompressionFixedRateFlagBitsEXT::e21bpcBit,
                Self::e22bpcBit => ImageCompressionFixedRateFlagBitsEXT::e22bpcBit,
                Self::e23bpcBit => ImageCompressionFixedRateFlagBitsEXT::e23bpcBit,
                Self::e24bpcBit => ImageCompressionFixedRateFlagBitsEXT::e24bpcBit,
                RawImageCompressionFixedRateFlagBitsEXT(b) => ImageCompressionFixedRateFlagBitsEXT::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawImageCompressionFixedRateFlagBitsEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawExportMetalObjectTypeFlagBitsEXT(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawExportMetalObjectTypeFlagBitsEXT {
        pub const eMetalDeviceBit: Self = Self(0b1);
        pub const eMetalCommandQueueBit: Self = Self(0b10);
        pub const eMetalBufferBit: Self = Self(0b100);
        pub const eMetalTextureBit: Self = Self(0b1000);
        pub const eMetalIosurfaceBit: Self = Self(0b10000);
        pub const eMetalSharedEventBit: Self = Self(0b100000);

        pub fn normalise(self) -> ExportMetalObjectTypeFlagBitsEXT {
            match self {
                Self::eMetalDeviceBit => ExportMetalObjectTypeFlagBitsEXT::eMetalDeviceBit,
                Self::eMetalCommandQueueBit => ExportMetalObjectTypeFlagBitsEXT::eMetalCommandQueueBit,
                Self::eMetalBufferBit => ExportMetalObjectTypeFlagBitsEXT::eMetalBufferBit,
                Self::eMetalTextureBit => ExportMetalObjectTypeFlagBitsEXT::eMetalTextureBit,
                Self::eMetalIosurfaceBit => ExportMetalObjectTypeFlagBitsEXT::eMetalIosurfaceBit,
                Self::eMetalSharedEventBit => ExportMetalObjectTypeFlagBitsEXT::eMetalSharedEventBit,
                RawExportMetalObjectTypeFlagBitsEXT(b) => ExportMetalObjectTypeFlagBitsEXT::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<ExportMetalObjectTypeFlagBitsEXT> {
            match self {
                RawExportMetalObjectTypeFlagBitsEXT(0) => None,
                Self::eMetalDeviceBit => Some(ExportMetalObjectTypeFlagBitsEXT::eMetalDeviceBit),
                Self::eMetalCommandQueueBit => Some(ExportMetalObjectTypeFlagBitsEXT::eMetalCommandQueueBit),
                Self::eMetalBufferBit => Some(ExportMetalObjectTypeFlagBitsEXT::eMetalBufferBit),
                Self::eMetalTextureBit => Some(ExportMetalObjectTypeFlagBitsEXT::eMetalTextureBit),
                Self::eMetalIosurfaceBit => Some(ExportMetalObjectTypeFlagBitsEXT::eMetalIosurfaceBit),
                Self::eMetalSharedEventBit => Some(ExportMetalObjectTypeFlagBitsEXT::eMetalSharedEventBit),
                RawExportMetalObjectTypeFlagBitsEXT(b) => Some(ExportMetalObjectTypeFlagBitsEXT::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawExportMetalObjectTypeFlagBitsEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawDeviceAddressBindingFlagBitsEXT(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawDeviceAddressBindingFlagBitsEXT {
        pub const eInternalObjectBit: Self = Self(0b1);

        pub fn normalise(self) -> DeviceAddressBindingFlagBitsEXT {
            match self {
                Self::eInternalObjectBit => DeviceAddressBindingFlagBitsEXT::eInternalObjectBit,
                RawDeviceAddressBindingFlagBitsEXT(b) => DeviceAddressBindingFlagBitsEXT::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<DeviceAddressBindingFlagBitsEXT> {
            match self {
                RawDeviceAddressBindingFlagBitsEXT(0) => None,
                Self::eInternalObjectBit => Some(DeviceAddressBindingFlagBitsEXT::eInternalObjectBit),
                RawDeviceAddressBindingFlagBitsEXT(b) => Some(DeviceAddressBindingFlagBitsEXT::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawDeviceAddressBindingFlagBitsEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawOpticalFlowGridSizeFlagBitsNV(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawOpticalFlowGridSizeFlagBitsNV {
        pub const eUnknown: Self = Self(0);
        pub const e1x1Bit: Self = Self(0b1);
        pub const e2x2Bit: Self = Self(0b10);
        pub const e4x4Bit: Self = Self(0b100);
        pub const e8x8Bit: Self = Self(0b1000);

        pub fn normalise(self) -> OpticalFlowGridSizeFlagBitsNV {
            match self {
                Self::eUnknown => OpticalFlowGridSizeFlagBitsNV::eUnknown,
                Self::e1x1Bit => OpticalFlowGridSizeFlagBitsNV::e1x1Bit,
                Self::e2x2Bit => OpticalFlowGridSizeFlagBitsNV::e2x2Bit,
                Self::e4x4Bit => OpticalFlowGridSizeFlagBitsNV::e4x4Bit,
                Self::e8x8Bit => OpticalFlowGridSizeFlagBitsNV::e8x8Bit,
                RawOpticalFlowGridSizeFlagBitsNV(b) => OpticalFlowGridSizeFlagBitsNV::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawOpticalFlowGridSizeFlagBitsNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawOpticalFlowUsageFlagBitsNV(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawOpticalFlowUsageFlagBitsNV {
        pub const eUnknown: Self = Self(0);
        pub const eInputBit: Self = Self(0b1);
        pub const eOutputBit: Self = Self(0b10);
        pub const eHintBit: Self = Self(0b100);
        pub const eCostBit: Self = Self(0b1000);
        pub const eGlobalFlowBit: Self = Self(0b10000);

        pub fn normalise(self) -> OpticalFlowUsageFlagBitsNV {
            match self {
                Self::eUnknown => OpticalFlowUsageFlagBitsNV::eUnknown,
                Self::eInputBit => OpticalFlowUsageFlagBitsNV::eInputBit,
                Self::eOutputBit => OpticalFlowUsageFlagBitsNV::eOutputBit,
                Self::eHintBit => OpticalFlowUsageFlagBitsNV::eHintBit,
                Self::eCostBit => OpticalFlowUsageFlagBitsNV::eCostBit,
                Self::eGlobalFlowBit => OpticalFlowUsageFlagBitsNV::eGlobalFlowBit,
                RawOpticalFlowUsageFlagBitsNV(b) => OpticalFlowUsageFlagBitsNV::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawOpticalFlowUsageFlagBitsNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawOpticalFlowSessionCreateFlagBitsNV(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawOpticalFlowSessionCreateFlagBitsNV {
        pub const eEnableHintBit: Self = Self(0b1);
        pub const eEnableCostBit: Self = Self(0b10);
        pub const eEnableGlobalFlowBit: Self = Self(0b100);
        pub const eAllowRegionsBit: Self = Self(0b1000);
        pub const eBothDirectionsBit: Self = Self(0b10000);

        pub fn normalise(self) -> OpticalFlowSessionCreateFlagBitsNV {
            match self {
                Self::eEnableHintBit => OpticalFlowSessionCreateFlagBitsNV::eEnableHintBit,
                Self::eEnableCostBit => OpticalFlowSessionCreateFlagBitsNV::eEnableCostBit,
                Self::eEnableGlobalFlowBit => OpticalFlowSessionCreateFlagBitsNV::eEnableGlobalFlowBit,
                Self::eAllowRegionsBit => OpticalFlowSessionCreateFlagBitsNV::eAllowRegionsBit,
                Self::eBothDirectionsBit => OpticalFlowSessionCreateFlagBitsNV::eBothDirectionsBit,
                RawOpticalFlowSessionCreateFlagBitsNV(b) => OpticalFlowSessionCreateFlagBitsNV::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<OpticalFlowSessionCreateFlagBitsNV> {
            match self {
                RawOpticalFlowSessionCreateFlagBitsNV(0) => None,
                Self::eEnableHintBit => Some(OpticalFlowSessionCreateFlagBitsNV::eEnableHintBit),
                Self::eEnableCostBit => Some(OpticalFlowSessionCreateFlagBitsNV::eEnableCostBit),
                Self::eEnableGlobalFlowBit => Some(OpticalFlowSessionCreateFlagBitsNV::eEnableGlobalFlowBit),
                Self::eAllowRegionsBit => Some(OpticalFlowSessionCreateFlagBitsNV::eAllowRegionsBit),
                Self::eBothDirectionsBit => Some(OpticalFlowSessionCreateFlagBitsNV::eBothDirectionsBit),
                RawOpticalFlowSessionCreateFlagBitsNV(b) => Some(OpticalFlowSessionCreateFlagBitsNV::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawOpticalFlowSessionCreateFlagBitsNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawOpticalFlowExecuteFlagBitsNV(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawOpticalFlowExecuteFlagBitsNV {
        pub const eDisableTemporalHintsBit: Self = Self(0b1);

        pub fn normalise(self) -> OpticalFlowExecuteFlagBitsNV {
            match self {
                Self::eDisableTemporalHintsBit => OpticalFlowExecuteFlagBitsNV::eDisableTemporalHintsBit,
                RawOpticalFlowExecuteFlagBitsNV(b) => OpticalFlowExecuteFlagBitsNV::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<OpticalFlowExecuteFlagBitsNV> {
            match self {
                RawOpticalFlowExecuteFlagBitsNV(0) => None,
                Self::eDisableTemporalHintsBit => Some(OpticalFlowExecuteFlagBitsNV::eDisableTemporalHintsBit),
                RawOpticalFlowExecuteFlagBitsNV(b) => Some(OpticalFlowExecuteFlagBitsNV::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawOpticalFlowExecuteFlagBitsNV {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawPresentScalingFlagBitsEXT(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawPresentScalingFlagBitsEXT {
        pub const eOneToOneBit: Self = Self(0b1);
        pub const eAspectRatioStretchBit: Self = Self(0b10);
        pub const eStretchBit: Self = Self(0b100);

        pub fn normalise(self) -> PresentScalingFlagBitsEXT {
            match self {
                Self::eOneToOneBit => PresentScalingFlagBitsEXT::eOneToOneBit,
                Self::eAspectRatioStretchBit => PresentScalingFlagBitsEXT::eAspectRatioStretchBit,
                Self::eStretchBit => PresentScalingFlagBitsEXT::eStretchBit,
                RawPresentScalingFlagBitsEXT(b) => PresentScalingFlagBitsEXT::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<PresentScalingFlagBitsEXT> {
            match self {
                RawPresentScalingFlagBitsEXT(0) => None,
                Self::eOneToOneBit => Some(PresentScalingFlagBitsEXT::eOneToOneBit),
                Self::eAspectRatioStretchBit => Some(PresentScalingFlagBitsEXT::eAspectRatioStretchBit),
                Self::eStretchBit => Some(PresentScalingFlagBitsEXT::eStretchBit),
                RawPresentScalingFlagBitsEXT(b) => Some(PresentScalingFlagBitsEXT::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawPresentScalingFlagBitsEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawPresentGravityFlagBitsEXT(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawPresentGravityFlagBitsEXT {
        pub const eMinBit: Self = Self(0b1);
        pub const eMaxBit: Self = Self(0b10);
        pub const eCenteredBit: Self = Self(0b100);

        pub fn normalise(self) -> PresentGravityFlagBitsEXT {
            match self {
                Self::eMinBit => PresentGravityFlagBitsEXT::eMinBit,
                Self::eMaxBit => PresentGravityFlagBitsEXT::eMaxBit,
                Self::eCenteredBit => PresentGravityFlagBitsEXT::eCenteredBit,
                RawPresentGravityFlagBitsEXT(b) => PresentGravityFlagBitsEXT::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<PresentGravityFlagBitsEXT> {
            match self {
                RawPresentGravityFlagBitsEXT(0) => None,
                Self::eMinBit => Some(PresentGravityFlagBitsEXT::eMinBit),
                Self::eMaxBit => Some(PresentGravityFlagBitsEXT::eMaxBit),
                Self::eCenteredBit => Some(PresentGravityFlagBitsEXT::eCenteredBit),
                RawPresentGravityFlagBitsEXT(b) => Some(PresentGravityFlagBitsEXT::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawPresentGravityFlagBitsEXT {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawVideoCodecOperationFlagBitsKHR(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawVideoCodecOperationFlagBitsKHR {
        pub const eNone: Self = Self(0);
        pub const eDecodeH264Bit: Self = Self(0b1);
        pub const eDecodeH265Bit: Self = Self(0b10);
        pub const eEncodeH264BitExt: Self = Self(0b10000000000000000);
        pub const eEncodeH265BitExt: Self = Self(0b100000000000000000);

        pub fn normalise(self) -> VideoCodecOperationFlagBitsKHR {
            match self {
                Self::eNone => VideoCodecOperationFlagBitsKHR::eNone,
                Self::eDecodeH264Bit => VideoCodecOperationFlagBitsKHR::eDecodeH264Bit,
                Self::eDecodeH265Bit => VideoCodecOperationFlagBitsKHR::eDecodeH265Bit,
                Self::eEncodeH264BitExt => VideoCodecOperationFlagBitsKHR::eEncodeH264BitExt,
                Self::eEncodeH265BitExt => VideoCodecOperationFlagBitsKHR::eEncodeH265BitExt,
                RawVideoCodecOperationFlagBitsKHR(b) => VideoCodecOperationFlagBitsKHR::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawVideoCodecOperationFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawVideoCapabilityFlagBitsKHR(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawVideoCapabilityFlagBitsKHR {
        pub const eProtectedContentBit: Self = Self(0b1);
        pub const eSeparateReferenceImagesBit: Self = Self(0b10);

        pub fn normalise(self) -> VideoCapabilityFlagBitsKHR {
            match self {
                Self::eProtectedContentBit => VideoCapabilityFlagBitsKHR::eProtectedContentBit,
                Self::eSeparateReferenceImagesBit => VideoCapabilityFlagBitsKHR::eSeparateReferenceImagesBit,
                RawVideoCapabilityFlagBitsKHR(b) => VideoCapabilityFlagBitsKHR::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<VideoCapabilityFlagBitsKHR> {
            match self {
                RawVideoCapabilityFlagBitsKHR(0) => None,
                Self::eProtectedContentBit => Some(VideoCapabilityFlagBitsKHR::eProtectedContentBit),
                Self::eSeparateReferenceImagesBit => Some(VideoCapabilityFlagBitsKHR::eSeparateReferenceImagesBit),
                RawVideoCapabilityFlagBitsKHR(b) => Some(VideoCapabilityFlagBitsKHR::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawVideoCapabilityFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawVideoSessionCreateFlagBitsKHR(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawVideoSessionCreateFlagBitsKHR {
        pub const eProtectedContentBit: Self = Self(0b1);

        pub fn normalise(self) -> VideoSessionCreateFlagBitsKHR {
            match self {
                Self::eProtectedContentBit => VideoSessionCreateFlagBitsKHR::eProtectedContentBit,
                RawVideoSessionCreateFlagBitsKHR(b) => VideoSessionCreateFlagBitsKHR::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<VideoSessionCreateFlagBitsKHR> {
            match self {
                RawVideoSessionCreateFlagBitsKHR(0) => None,
                Self::eProtectedContentBit => Some(VideoSessionCreateFlagBitsKHR::eProtectedContentBit),
                RawVideoSessionCreateFlagBitsKHR(b) => Some(VideoSessionCreateFlagBitsKHR::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawVideoSessionCreateFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawVideoCodingControlFlagBitsKHR(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawVideoCodingControlFlagBitsKHR {
        pub const eResetBit: Self = Self(0b1);
        pub const eEncodeRateControlBit: Self = Self(0b10);
        pub const eEncodeRateControlLayerBit: Self = Self(0b100);

        pub fn normalise(self) -> VideoCodingControlFlagBitsKHR {
            match self {
                Self::eResetBit => VideoCodingControlFlagBitsKHR::eResetBit,
                Self::eEncodeRateControlBit => VideoCodingControlFlagBitsKHR::eEncodeRateControlBit,
                Self::eEncodeRateControlLayerBit => VideoCodingControlFlagBitsKHR::eEncodeRateControlLayerBit,
                RawVideoCodingControlFlagBitsKHR(b) => VideoCodingControlFlagBitsKHR::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<VideoCodingControlFlagBitsKHR> {
            match self {
                RawVideoCodingControlFlagBitsKHR(0) => None,
                Self::eResetBit => Some(VideoCodingControlFlagBitsKHR::eResetBit),
                Self::eEncodeRateControlBit => Some(VideoCodingControlFlagBitsKHR::eEncodeRateControlBit),
                Self::eEncodeRateControlLayerBit => Some(VideoCodingControlFlagBitsKHR::eEncodeRateControlLayerBit),
                RawVideoCodingControlFlagBitsKHR(b) => Some(VideoCodingControlFlagBitsKHR::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawVideoCodingControlFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawVideoDecodeUsageFlagBitsKHR(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawVideoDecodeUsageFlagBitsKHR {
        pub const eDefault: Self = Self(0);
        pub const eTranscodingBit: Self = Self(0b1);
        pub const eOfflineBit: Self = Self(0b10);
        pub const eStreamingBit: Self = Self(0b100);

        pub fn normalise(self) -> VideoDecodeUsageFlagBitsKHR {
            match self {
                Self::eDefault => VideoDecodeUsageFlagBitsKHR::eDefault,
                Self::eTranscodingBit => VideoDecodeUsageFlagBitsKHR::eTranscodingBit,
                Self::eOfflineBit => VideoDecodeUsageFlagBitsKHR::eOfflineBit,
                Self::eStreamingBit => VideoDecodeUsageFlagBitsKHR::eStreamingBit,
                RawVideoDecodeUsageFlagBitsKHR(b) => VideoDecodeUsageFlagBitsKHR::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawVideoDecodeUsageFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawVideoDecodeCapabilityFlagBitsKHR(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawVideoDecodeCapabilityFlagBitsKHR {
        pub const eDpbAndOutputCoincideBit: Self = Self(0b1);
        pub const eDpbAndOutputDistinctBit: Self = Self(0b10);

        pub fn normalise(self) -> VideoDecodeCapabilityFlagBitsKHR {
            match self {
                Self::eDpbAndOutputCoincideBit => VideoDecodeCapabilityFlagBitsKHR::eDpbAndOutputCoincideBit,
                Self::eDpbAndOutputDistinctBit => VideoDecodeCapabilityFlagBitsKHR::eDpbAndOutputDistinctBit,
                RawVideoDecodeCapabilityFlagBitsKHR(b) => VideoDecodeCapabilityFlagBitsKHR::eUnknownBit(b),
            }
        }

        pub fn normalise_optional(self) -> Option<VideoDecodeCapabilityFlagBitsKHR> {
            match self {
                RawVideoDecodeCapabilityFlagBitsKHR(0) => None,
                Self::eDpbAndOutputCoincideBit => Some(VideoDecodeCapabilityFlagBitsKHR::eDpbAndOutputCoincideBit),
                Self::eDpbAndOutputDistinctBit => Some(VideoDecodeCapabilityFlagBitsKHR::eDpbAndOutputDistinctBit),
                RawVideoDecodeCapabilityFlagBitsKHR(b) => Some(VideoDecodeCapabilityFlagBitsKHR::eUnknownBit(b)),
            }
        }
    }

    impl core::fmt::Debug for RawVideoDecodeCapabilityFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawVideoDecodeH264PictureLayoutFlagBitsKHR(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawVideoDecodeH264PictureLayoutFlagBitsKHR {
        pub const eProgressive: Self = Self(0);
        pub const eInterlacedInterleavedLinesBit: Self = Self(0b1);
        pub const eInterlacedSeparatePlanesBit: Self = Self(0b10);

        pub fn normalise(self) -> VideoDecodeH264PictureLayoutFlagBitsKHR {
            match self {
                Self::eProgressive => VideoDecodeH264PictureLayoutFlagBitsKHR::eProgressive,
                Self::eInterlacedInterleavedLinesBit => VideoDecodeH264PictureLayoutFlagBitsKHR::eInterlacedInterleavedLinesBit,
                Self::eInterlacedSeparatePlanesBit => VideoDecodeH264PictureLayoutFlagBitsKHR::eInterlacedSeparatePlanesBit,
                RawVideoDecodeH264PictureLayoutFlagBitsKHR(b) => VideoDecodeH264PictureLayoutFlagBitsKHR::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawVideoDecodeH264PictureLayoutFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawVideoChromaSubsamplingFlagBitsKHR(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawVideoChromaSubsamplingFlagBitsKHR {
        pub const eInvalid: Self = Self(0);
        pub const eMonochromeBit: Self = Self(0b1);
        pub const e420Bit: Self = Self(0b10);
        pub const e422Bit: Self = Self(0b100);
        pub const e444Bit: Self = Self(0b1000);

        pub fn normalise(self) -> VideoChromaSubsamplingFlagBitsKHR {
            match self {
                Self::eInvalid => VideoChromaSubsamplingFlagBitsKHR::eInvalid,
                Self::eMonochromeBit => VideoChromaSubsamplingFlagBitsKHR::eMonochromeBit,
                Self::e420Bit => VideoChromaSubsamplingFlagBitsKHR::e420Bit,
                Self::e422Bit => VideoChromaSubsamplingFlagBitsKHR::e422Bit,
                Self::e444Bit => VideoChromaSubsamplingFlagBitsKHR::e444Bit,
                RawVideoChromaSubsamplingFlagBitsKHR(b) => VideoChromaSubsamplingFlagBitsKHR::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawVideoChromaSubsamplingFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
    #[repr(C)]
    #[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
    pub struct RawVideoComponentBitDepthFlagBitsKHR(pub u32);

    #[allow(non_upper_case_globals)]
    impl RawVideoComponentBitDepthFlagBitsKHR {
        pub const eInvalid: Self = Self(0);
        pub const e8Bit: Self = Self(0b1);
        pub const e10Bit: Self = Self(0b100);
        pub const e12Bit: Self = Self(0b10000);

        pub fn normalise(self) -> VideoComponentBitDepthFlagBitsKHR {
            match self {
                Self::eInvalid => VideoComponentBitDepthFlagBitsKHR::eInvalid,
                Self::e8Bit => VideoComponentBitDepthFlagBitsKHR::e8Bit,
                Self::e10Bit => VideoComponentBitDepthFlagBitsKHR::e10Bit,
                Self::e12Bit => VideoComponentBitDepthFlagBitsKHR::e12Bit,
                RawVideoComponentBitDepthFlagBitsKHR(b) => VideoComponentBitDepthFlagBitsKHR::eUnknownBit(b),
            }
        }
    }

    impl core::fmt::Debug for RawVideoComponentBitDepthFlagBitsKHR {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            self.normalise().fmt(f)
        }
    }
}

