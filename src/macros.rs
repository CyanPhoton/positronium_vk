macro_rules!    bit_field_enum_derives {
    ($bit_enum:ident, $bit_mask:ident, $data_type:ident) => {
        impl ::core::convert::From<$bit_enum> for $data_type {
            fn from(value: $bit_enum) -> Self {
                $bit_mask::from(value).0
            }
        }

        impl ::core::ops::BitAnd<$bit_enum> for $bit_enum {
            type Output = $bit_mask;

            fn bitand(self, rhs: $bit_enum) -> Self::Output {
                $bit_mask::from(self) & $bit_mask::from(rhs)
            }
        }

        impl ::core::ops::BitAnd<$bit_mask> for $bit_enum {
            type Output = $bit_mask;

            fn bitand(self, rhs: $bit_mask) -> Self::Output {
                $bit_mask::from(self) & rhs
            }
        }

        impl ::core::ops::BitAnd<$bit_enum> for $bit_mask {
            type Output = $bit_mask;

            fn bitand(self, rhs: $bit_enum) -> Self::Output {
                self & $bit_mask::from(rhs)
            }
        }

        impl ::core::ops::BitAndAssign<$bit_enum> for $bit_mask {
            fn bitand_assign(&mut self, rhs: $bit_enum) {
                *self &= $bit_mask::from(rhs);
            }
        }

        impl ::core::ops::BitOr<$bit_enum> for $bit_enum {
            type Output = $bit_mask;

            fn bitor(self, rhs: $bit_enum) -> Self::Output {
                $bit_mask::from(self) | $bit_mask::from(rhs)
            }
        }

        impl ::core::ops::BitOr<$bit_mask> for $bit_enum {
            type Output = $bit_mask;

            fn bitor(self, rhs: $bit_mask) -> Self::Output {
                $bit_mask::from(self) | rhs
            }
        }

        impl ::core::ops::BitOr<$bit_enum> for $bit_mask {
            type Output = $bit_mask;

            fn bitor(self, rhs: $bit_enum) -> Self::Output {
                self | $bit_mask::from(rhs)
            }
        }

        impl ::core::ops::BitOrAssign<$bit_enum> for $bit_mask {
            fn bitor_assign(&mut self, rhs: $bit_enum) {
                *self |= $bit_mask::from(rhs);
            }
        }

        impl ::core::ops::BitXor<$bit_enum> for $bit_enum {
            type Output = $bit_mask;

            fn bitxor(self, rhs: $bit_enum) -> Self::Output {
                $bit_mask::from(self) ^ $bit_mask::from(rhs)
            }
        }

        impl ::core::ops::BitXor<$bit_mask> for $bit_enum {
            type Output = $bit_mask;

            fn bitxor(self, rhs: $bit_mask) -> Self::Output {
                $bit_mask::from(self) ^ rhs
            }
        }

        impl ::core::ops::BitXor<$bit_enum> for $bit_mask {
            type Output = $bit_mask;

            fn bitxor(self, rhs: $bit_enum) -> Self::Output {
                self ^ $bit_mask::from(rhs)
            }
        }

        impl ::core::ops::BitXorAssign<$bit_enum> for $bit_mask {
            fn bitxor_assign(&mut self, rhs: $bit_enum)  {
                *self ^= $bit_mask::from(rhs);
            }
        }

        impl ::core::ops::Neg for $bit_enum {
            type Output = $bit_mask;

            fn neg(self) -> Self::Output {
                ($bit_mask::from(self)).neg()
            }
        }
    };
}
macro_rules! bit_field_mask_derives {
    ($bit_mask:ident, $data_type:ident) => {
        impl ::core::convert::From<$bit_mask> for $data_type {
            fn from(value: $bit_mask) -> Self {
                value.0
            }
        }

        impl ::core::ops::BitAnd<$bit_mask> for $bit_mask {
            type Output = $bit_mask;

            fn bitand(self, rhs: $bit_mask) -> Self::Output {
                $bit_mask(self.0 & rhs.0)
            }
        }

        impl ::core::ops::BitAndAssign<$bit_mask> for $bit_mask {
            fn bitand_assign(&mut self, rhs: $bit_mask) {
                self.0 &= rhs.0;
            }
        }

        impl ::core::ops::BitOr<$bit_mask> for $bit_mask {
            type Output = $bit_mask;

            fn bitor(self, rhs: $bit_mask) -> Self::Output {
                $bit_mask(self.0 | rhs.0)
            }
        }

        impl ::core::ops::BitOrAssign<$bit_mask> for $bit_mask {
            fn bitor_assign(&mut self, rhs: $bit_mask) {
                self.0 |= rhs.0;
            }
        }

        impl ::core::ops::BitXor<$bit_mask> for $bit_mask {
            type Output = $bit_mask;

            fn bitxor(self, rhs: $bit_mask) -> Self::Output {
                $bit_mask(self.0 ^ rhs.0)
            }
        }

        impl ::core::ops::BitXorAssign<$bit_mask> for $bit_mask {
            fn bitxor_assign(&mut self, rhs: $bit_mask)  {
                self.0 ^= rhs.0;
            }
        }

        impl ::core::ops::Neg for $bit_mask {
            type Output = $bit_mask;

            fn neg(self) -> Self::Output {
                $bit_mask(!self.0)
            }
        }
    }
}

macro_rules! bit_field_mask_with_enum_debug_derive {
    ($bit_enum:ident, $bit_mask:ident, $data_type:ident, $($o:expr),*) => {
        impl $bit_mask {
            pub fn into_flags(self) -> BitFieldIterator<$bit_enum, $bit_mask, $data_type> {
                BitFieldIterator::new(self, [$($o),*].iter(), None)
            }
        }

        impl ::core::fmt::Debug for $bit_mask {
            fn fmt(&self, f: &mut ::core::fmt::Formatter<'_>) -> core::fmt::Result {
                f.debug_list().entries(self.into_flags().map(|f| f.map_or_else(|e| format!("Unknown Bits: {:#b}", e.remainder()), |f| format!("{:?}", f)))).finish()
            }
        }
    };
    ($bit_enum:ident, $bit_mask:ident, $data_type:ident, $($o:expr),* => $d:expr) => {
        impl $bit_mask {
            pub fn into_flags(self) -> BitFieldIterator<$bit_enum, $bit_mask, $data_type> {
                BitFieldIterator::new(self, [$($o),*].iter(), Some($d))
            }
        }

        impl ::core::fmt::Debug for $bit_mask {
            fn fmt(&self, f: &mut ::core::fmt::Formatter<'_>) -> core::fmt::Result {
                f.debug_list().entries(self.into_flags().map(|f| f.map_or_else(|e| format!("Unknown Bits: {:#b}", e.remainder()), |f| format!("{:?}", f)))).finish()
            }
        }
    };
}

macro_rules! fieldless_debug_derive {
    ($struct:ty) => {
        impl ::std::fmt::Debug for $struct {
            fn fmt(&self, f: &mut ::std::fmt::Formatter<'_>) -> ::std::fmt::Result {
                f.debug_struct(stringify!($struct)).finish()
            }
        }
    };
}

macro_rules! single_field_debug_derive {
    ($struct:ty, $name:ident) => {
        impl ::std::fmt::Debug for $struct {
            fn fmt(&self, f: &mut ::std::fmt::Formatter<'_>) -> ::std::fmt::Result {
                f.debug_struct(stringify!($struct))
                    .field(stringify!($name), &self.$name)
                    .finish()
            }
        }
    };
}

macro_rules! single_field_as_ptr_debug_derive {
    ($struct:ty, $name:ident) => {
        impl ::std::fmt::Debug for $struct {
            fn fmt(&self, f: &mut ::std::fmt::Formatter<'_>) -> ::std::fmt::Result {
                write!(f, "{:p}", self.$name as *const u8)
            }
        }
    };
}

// TODO: Replace with actual const panic when available
macro_rules! const_panic {
    ($msg:literal) => {
        {
            // #[allow(unconditional_panic)]
            [1][[$msg].len()];
            loop {}
        }
    }
}