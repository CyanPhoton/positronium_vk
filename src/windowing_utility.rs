use raw_window_handle::{HasRawWindowHandle, RawWindowHandle};

use super::*;

pub fn get_platform_surface_extension_options() -> Option<Vec<Vec<UnsizedCStr<'static>>>> {
    // TODO: Test on MacOS to support this,
    #[cfg(target_os = "macos")]
        {
            None
        }

    #[cfg(any(
    target_os = "linux",
    target_os = "dragonfly",
    target_os = "freebsd",
    target_os = "netbsd",
    target_os = "openbsd"
    ))]
        {
            Some(vec![
                vec![
                    UnsizedCStr::from_str("VK_KHR_surface\0").unwrap(),
                    UnsizedCStr::from_str("VK_KHR_xlib_surface\0").unwrap(),
                ],
                vec![
                    UnsizedCStr::from_str("VK_KHR_surface\0").unwrap(),
                    UnsizedCStr::from_str("VK_KHR_xcb_surface\0").unwrap(),
                ],
                vec![
                    UnsizedCStr::from_str("VK_KHR_surface\0").unwrap(),
                    UnsizedCStr::from_str("VK_KHR_wayland_surface\0").unwrap(),
                ],
            ])
        }

    #[cfg(target_os = "windows")]
        {
            Some(vec![
                vec![
                    UnsizedCStr::from_str("VK_KHR_surface\0").unwrap(),
                    UnsizedCStr::from_str("VK_KHR_win32_surface\0").unwrap(),
                ]
            ])
        }

    // No plan to support these for now at least
    #[cfg(target_os = "ios")]
        {
            None
        }

    #[cfg(target_arch = "wasm32")]
        {
            None
        }

    #[cfg(target_os = "android")]
        {
            None
        }
}

pub fn get_required_surface_extensions<W: HasRawWindowHandle>(window: &W) -> Option<Vec<UnsizedCStr<'static>>> {
    match window.raw_window_handle() {
        // TODO: Test on MacOS to support this,
        // Possible extensions are:
        // Some(vec![
        //             UnsizedCStr::from_stb"VK_KHR_surface\0").unwrap(),
        //             UnsizedCStr::from_stb"VK_EXT_metal_surface\0").unwrap(),
        //         ])
        // or
        // Some(vec![
        //             UnsizedCStr::from_stb"VK_KHR_surface\0").unwrap(),
        //             UnsizedCStr::from_stb"VK_MVK_macos_surface\0").unwrap(),
        //         ])
        // Also possible use: https://crates.io/crates/raw-window-metal
        RawWindowHandle::UiKit(_) => None,
        RawWindowHandle::AppKit(_) => None,

        RawWindowHandle::Xlib(_) => Some(vec![
            UnsizedCStr::from_str("VK_KHR_surface\0").unwrap(),
            UnsizedCStr::from_str("VK_KHR_xlib_surface\0").unwrap(),
        ]),
        RawWindowHandle::Xcb(_) => Some(vec![
            UnsizedCStr::from_str("VK_KHR_surface\0").unwrap(),
            UnsizedCStr::from_str("VK_KHR_xcb_surface\0").unwrap(),
        ]),
        RawWindowHandle::Wayland(_) => Some(vec![
            UnsizedCStr::from_str("VK_KHR_surface\0").unwrap(),
            UnsizedCStr::from_str("VK_KHR_wayland_surface\0").unwrap(),
        ]),
        // TODO: Check out possibility?
        RawWindowHandle::Orbital(_) => None,
        RawWindowHandle::Win32(_) => Some(vec![
            UnsizedCStr::from_str("VK_KHR_surface\0").unwrap(),
            UnsizedCStr::from_str("VK_KHR_win32_surface\0").unwrap(),
        ]),
        // TODO: Check out possibility?
        RawWindowHandle::WinRt(_) => None,
        RawWindowHandle::Web(_) => None,
        RawWindowHandle::AndroidNdk(_) => None,
        _ => None,
    }
}

#[inline(always)]
pub fn flush_window_compositor() {
    #[cfg(windows)]
        {
            use winapi::um::dwmapi::DwmFlush;

            unsafe { DwmFlush(); }
        }
}

impl Instance {
    pub fn create_surface<W: HasRawWindowHandle>(&self, window: &W) -> Option<core::result::Result<(SurfaceKHR, SuccessCode), ErrorCode>> {
        self.create_surface_with_alloc(window, AllocMode::InheritParent)
    }

    pub fn create_surface_with_alloc<W: HasRawWindowHandle>(&self, window: &W, alloc_mode: AllocMode) -> Option<core::result::Result<(SurfaceKHR, SuccessCode), ErrorCode>> {
        match window.raw_window_handle() {
            // TODO: Test on MacOS to support this,
            RawWindowHandle::UiKit(_) => None,
            RawWindowHandle::AppKit(_) => None,
            RawWindowHandle::Xlib(handle) => {
                let create_info = XlibSurfaceCreateInfoKHR {
                    flags: Default::default(),
                    dpy: unsafe { Some(&mut *(handle.display as *mut Display)) },
                    window: handle.window,
                }.into_raw();

                Some(self.create_xlib_surface_khr_with_alloc(&create_info, alloc_mode))
            }
            RawWindowHandle::Xcb(handle) => {
                let create_info = XcbSurfaceCreateInfoKHR {
                    flags: Default::default(),
                    connection: unsafe { Some(&mut *(handle.connection as *mut xcb_connection_t)) },
                    window: handle.window,
                }.into_raw();

                Some(self.create_xcb_surface_khr_with_alloc(&create_info, alloc_mode))
            }
            RawWindowHandle::Wayland(handle) => {
                let create_info = WaylandSurfaceCreateInfoKHR {
                    flags: Default::default(),
                    display: unsafe { Some(&mut *(handle.display as *mut WlDisplay)) },
                    surface: unsafe { Some(&mut *(handle.surface as *mut WlSurface)) },
                }.into_raw();

                Some(self.create_wayland_surface_khr_with_alloc(&create_info, alloc_mode))
            }
            RawWindowHandle::Orbital(_) => None,
            RawWindowHandle::Win32(handle) => {
                let create_info = Win32SurfaceCreateInfoKHR {
                    flags: Default::default(),
                    hinstance: handle.hinstance,
                    hwnd: handle.hwnd,
                }.into_raw();

                Some(self.create_win32_surface_khr_with_alloc(&create_info, alloc_mode))
            }
            RawWindowHandle::WinRt(_) => None,
            RawWindowHandle::Web(_) => None,
            RawWindowHandle::AndroidNdk(_) => None,
            _ => None,
        }
    }
}