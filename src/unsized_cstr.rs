#![allow(unconditional_panic)]

use std::cmp::Ordering;
use std::ffi::{CStr, CString};
use std::fmt::{Display, Formatter, Debug};
use std::str::Utf8Error;
use std::convert::TryFrom;
use std::hash::{Hash, Hasher};
use std::marker::PhantomData;
use std::ptr::NonNull;
use crate::ConstSizeCStr;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum UnsizedCStrErrors {
    NoTerminatingNullByte,
    InteriorNullByte(usize),
    InvalidUTF8(Utf8Error),
}

impl Display for UnsizedCStrErrors {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            UnsizedCStrErrors::NoTerminatingNullByte => write!(f, "Tried to construct a UnsizedCStr without the required terminating null byte"),
            UnsizedCStrErrors::InteriorNullByte(i) => write!(f, "Tried to construct with an interior null byte as index: {}", i),
            UnsizedCStrErrors::InvalidUTF8(e) => write!(f, "Tried to interpret UnsizedCStr as a utf8 string but got error: {}", e)
        }
    }
}

///
/// A CStr like data structure, except all we hold is a reference to the first byte.
///
/// It is intended to be used with the Vulkan API where it will go both ways across the FFI barrier,
/// as is supposed to be equivalent to a '* const c_char' in Rust or 'const* char" in C, thus it
/// can not store it's owm length and it must be calculated for each call that needs it.
///
/// This does lead to a potential issue if the value received over the FFI lacks a terminating null byte,
/// as that will lead to UB. ALso if null is received it will cause UB, so if that is a potential you
/// must wrap this type in an Optional<>, which due to the `#[repr(transparent)]` will effectivity be
/// an Option<&u8> and thus have the same size and alignment as &u8, perfect for use with C over FFI.
///
/// Note: If you wish to construct using a string literal but without needing to add a null byte manually,
/// use the unsized_str!() macro
///
#[repr(transparent)]
#[derive(Copy, Clone)]
pub struct UnsizedCStr<'a> {
    pub(crate) first: NonNull<u8>,
    _phantom: PhantomData<&'a u8>
}

// Sound since this is an immutable c string
unsafe impl<'ptr> Send for UnsizedCStr<'ptr> {}
unsafe impl<'ptr> Sync for UnsizedCStr<'ptr> {}

impl<'a> UnsizedCStr<'a> {
    ///
    /// Construct an UnsizedCStr from a regular &CStr, pointing to the same memory
    ///
    /// Will never return an error as CStr makes guarantees about the presence of a null byte,
    /// this can only not be true with the use of unsafe code.
    ///
    pub fn from_cstr(cstr: &CStr) -> UnsizedCStr {
        // Always valid so long as cstr is valid
        let first = cstr.as_ref().to_bytes_with_nul().first().unwrap();
        UnsizedCStr {
            first: NonNull::from(first),
            _phantom: Default::default()
        }
    }

    ///
    /// Construct an UnsizedCStr from a regular &str, pointing to the &str
    ///
    /// Will return an error if the &str contains interior null bytes,
    /// or if it lacks a terminating null byte.
    ///
    /// Note: If you wish to construct using a string literal but without needing to add a null byte manually,
    /// use the unsized_str!() macro
    ///
    pub fn from_str(s: &str) -> Result<UnsizedCStr, UnsizedCStrErrors> {
        match Self::null_byte_location(s.as_bytes()) {
            Ok(i) => {
                if i + 1  == s.len() {
                    Ok(UnsizedCStr {
                        first: NonNull::from(s.as_bytes().first().unwrap()), // If (>= 0) + 1 == len, then len >= 1, thus first always exists
                        _phantom: Default::default()
                    })
                } else {
                    Err(UnsizedCStrErrors::InteriorNullByte(i))
                }
            }
            Err(e) => Err(e)
        }
    }

    ///
    /// Note: It is strongly recommend to only use in const setting, since it can panic at runtime otherwise,
    /// and it may also have worse runtime performance than the non const version
    ///
    /// Construct an UnsizedCStr from a regular &str, pointing to the &str.
    ///
    /// Will return an error if the &str contains interior null bytes,
    /// or if it lacks a terminating null byte.
    ///
    /// Note: If you wish to construct using a string literal but without needing to add a null byte manually,
    /// use the unsized_str!() macro
    ///
    pub const fn from_str_const(s: &str) -> UnsizedCStr {
        match Self::null_byte_location_const(s.as_bytes()) {
            Ok(i) => {
                if i + 1  == s.len() {
                    UnsizedCStr {
                        first: unsafe { NonNull::new_unchecked(&s.as_bytes()[0] as *const _ as *mut _) }, // If (>= 0) + 1 == len, then len >= 1, thus first always exists
                        _phantom: PhantomData
                    }
                } else {
                    const_panic!("Tried to construct with an interior null byte")
                }
            }
            Err(_e) => const_panic!("Tried to construct a UnsizedCStr without the required terminating null byte")
        }
    }

    ///
    /// Construct an UnsizedCStr from a byte string, pointing to it.
    ///
    /// Will return an error if the &[u8] contains interior null bytes,
    /// or if it lacks a terminating null byte.
    ///
    pub fn from_bstr(s: &[u8]) -> Result<UnsizedCStr, UnsizedCStrErrors> {
        match Self::null_byte_location(s) {
            Ok(i) => {
                if i + 1 == s.len() {
                    Ok(UnsizedCStr {
                        first: NonNull::from(&s[0]),
                        _phantom: Default::default()
                    })
                } else {
                    Err(UnsizedCStrErrors::InteriorNullByte(i))
                }
            }
            Err(e) => Err(e)
        }
    }

    ///
    /// Note: It is strongly recommend to only use in const setting, since it can panic at runtime otherwise,
    /// and it may also have worse runtime performance than the non const version
    ///
    /// Construct an UnsizedCStr from a byte string, pointing to it.
    ///
    /// Will return an error if the &[u8] contains interior null bytes,
    /// or if it lacks a terminating null byte.
    ///
    pub const fn from_bstr_const<const N: usize>(s: &[u8; N]) -> UnsizedCStr {
        match Self::null_byte_location_const(s) {
            Ok(i) => {
                if i + 1 == N {
                    UnsizedCStr {
                        first: unsafe { NonNull::new_unchecked(&s[0] as *const _ as *mut _) },
                        _phantom: PhantomData
                    }
                } else {
                    const_panic!("from_static_byte_str expects a byte string which a valid CStr, that is is null terminated also with no interior null bytes")
                }
            }
            Err(_) => const_panic!("from_static_byte_str expects a byte string which a valid CStr, that is is null terminated also with no interior null bytes")
        }
    }

    ///
    /// Construct an UnsizedCStr with the 'first' field set as provided.
    ///
    /// This is unsafe, since 'first' MUST point to a valid null terminated c style string
    ///
    pub const unsafe fn from_first_unchecked(first: &u8) -> UnsizedCStr {
        UnsizedCStr {
            first: unsafe { NonNull::new_unchecked(first as *const _ as *mut _) },
            _phantom: PhantomData
        }
    }

    fn null_byte_location(bytes: &[u8]) -> Result<usize, UnsizedCStrErrors> {
        memchr::memchr(0, bytes).ok_or(UnsizedCStrErrors::NoTerminatingNullByte)
    }

    const fn null_byte_location_const(bytes: &[u8]) -> Result<usize, UnsizedCStrErrors> {
        let mut i = 0;
        while i < bytes.len() {
            if bytes[i] == 0 {
                return Ok(i);
            }

            i += 1;
        }
        return Err(UnsizedCStrErrors::NoTerminatingNullByte);
    }

    ///
    /// Safety notice:
    ///
    /// UB if the null byte is missing, but all constructs of this type enforce it's presence, so without
    /// other unsafe code should be and remain sound
    ///
    /// Calculates the length of the UnsizedCStr by the number of bytes before the first null byte it finds.
    ///
    pub fn len(&self) -> usize {
        let ptr = self.first.as_ptr();

        let mut len = 0;
        while unsafe { *ptr.add(len) } != 0 {
            len += 1;
        }

        len
    }

    ///
    /// Calculates the length of the UnsizedCStr, including the null byte.
    ///
    pub fn len_with_null(&self) -> usize {
        self.len() + 1
    }

    ///
    /// Returns a slice of the bytes in the UnsizedCStr, excluding the null byte
    ///
    pub fn as_bytes(&self) -> &'a [u8] {
        unsafe { std::slice::from_raw_parts(self.first.as_ptr(), self.len()) }
    }

    ///
    /// Returns a slice of the bytes in the UnsizedCStr, including the null byte
    ///
    pub fn as_bytes_with_null(&self) -> &'a [u8] {
        unsafe { std::slice::from_raw_parts(self.first.as_ptr(), self.len_with_null()) }
    }

    ///
    /// Returns a CStr which points to the memory of this UnsizedCStr
    ///
    pub fn as_cstr(&self) -> &'a CStr {
        unsafe { CStr::from_bytes_with_nul_unchecked(self.as_bytes_with_null()) }
    }

    ///
    /// Returns a &str which points to the memory of this UnsizedCStr, excluding the null byte
    ///
    /// Will return an error if the UnsizedCStr is not valid utf-8
    ///
    pub fn as_str(&self) -> Result<&'a str, UnsizedCStrErrors> {
        std::str::from_utf8(self.as_bytes()).map_err(|e| UnsizedCStrErrors::InvalidUTF8(e))
    }
    ///
    /// Returns a &str which points to the memory of this UnsizedCStr, including the null byte
    ///
    /// Will return an error if the UnsizedCStr is not valid utf-8
    ///
    pub fn as_str_with_null(&self) -> Result<&'a str, UnsizedCStrErrors> {
        std::str::from_utf8(self.as_bytes_with_null()).map_err(|e| UnsizedCStrErrors::InvalidUTF8(e))
    }

    ///
    /// Returns a newly allocated CString, with a copy of this UnsizedCStr
    ///
    pub fn to_c_string(&self) -> CString {
        CString::from(self.as_cstr())
    }

    ///
    /// Returns a newly allocated String, with a copy of this UnsizedCStr, excluding the null byte
    ///
    /// Will return an error if the UnsizedCStr is not valid utf-8
    ///
    pub fn to_string(&self) -> Result<String, UnsizedCStrErrors> {
        self.as_str().map(|s| s.to_string())
    }

    ///
    /// Returns a newly allocated String, with a copy of this UnsizedCStr, including the null byte
    ///
    /// Will return an error if the UnsizedCStr is not valid utf-8
    ///
    pub fn to_string_with_null(&self) -> Result<String, UnsizedCStrErrors> {
        self.as_str_with_null().map(|s| s.to_string())
    }
}

impl<'a, 'b: 'a> From<&'b CStr> for UnsizedCStr<'a> {
    fn from(value: &'b CStr) -> Self {
        Self::from_cstr(value)
    }
}

impl<'a, 'b: 'a> TryFrom<&'b str> for UnsizedCStr<'a> {
    type Error = UnsizedCStrErrors;

    fn try_from(value: &'b str) -> Result<Self, Self::Error> {
        Self::from_str(value)
    }
}

impl<'a, 'b: 'a> TryFrom<&'b String> for UnsizedCStr<'a> {
    type Error = UnsizedCStrErrors;

    fn try_from(value: &'b String) -> Result<Self, Self::Error> {
        Self::from_str(value.as_str())
    }
}

impl<'a, 'b: 'a> TryFrom<&'b [u8]> for UnsizedCStr<'a> {
    type Error = UnsizedCStrErrors;

    fn try_from(value: &'b [u8]) -> Result<Self, Self::Error> {
        Self::from_bstr(value)
    }
}

impl<'a, 'b: 'a, const N: usize> TryFrom<&'b [u8; N]> for UnsizedCStr<'a> {
    type Error = UnsizedCStrErrors;

    fn try_from(value: &'b [u8; N]) -> Result<Self, Self::Error> {
        Self::from_bstr(&value[..])
    }
}

impl<'a, 'b> TryFrom<&'a UnsizedCStr<'b>> for &'b str {
    type Error = UnsizedCStrErrors;

    fn try_from(value: &'a UnsizedCStr<'b>) -> Result<Self, Self::Error> {
        value.as_str()
    }
}

impl<'a> TryFrom<UnsizedCStr<'a>> for &'a str {
    type Error = UnsizedCStrErrors;

    fn try_from(value: UnsizedCStr<'a>) -> Result<Self, Self::Error> {
        value.as_str()
    }
}

impl<'a> From<UnsizedCStr<'a>> for CString  {
    fn from(value: UnsizedCStr<'a>) -> Self {
        value.to_c_string()
    }
}

impl<'a, 'b> From<&'b UnsizedCStr<'a>> for CString  {
    fn from(value: &'b UnsizedCStr<'a>) -> Self {
        value.to_c_string()
    }
}

impl<'a, 'b> TryFrom<&'a UnsizedCStr<'b>> for String {
    type Error = UnsizedCStrErrors;

    fn try_from(value: &'a UnsizedCStr<'b>) -> Result<Self, Self::Error> {
        value.to_string()
    }
}

impl<'a> TryFrom<UnsizedCStr<'a>> for String {
    type Error = UnsizedCStrErrors;

    fn try_from(value: UnsizedCStr<'a>) -> Result<Self, Self::Error> {
        value.to_string()
    }
}

impl<'a> Debug for UnsizedCStr<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.as_cstr().fmt(f)
    }
}

impl Default for UnsizedCStr<'_> {
    fn default() -> Self {
        const EMPTY_BSTR: &'static [u8] = b"\0";

        UnsizedCStr::from_bstr(EMPTY_BSTR).unwrap()
    }
}

impl<'a, 'b> PartialEq<UnsizedCStr<'a>> for UnsizedCStr<'b> {
    fn eq(&self, other: &UnsizedCStr<'a>) -> bool {
        self.as_bytes().eq(other.as_bytes())
    }
}

impl<'a> PartialEq<CStr> for UnsizedCStr<'a> {
    fn eq(&self, other: &CStr) -> bool {
        self.as_bytes().eq(other.to_bytes())
    }
}

impl<'a, const N: usize> PartialEq<ConstSizeCStr<N>> for UnsizedCStr<'a> {
    fn eq(&self, other: &ConstSizeCStr<N>) -> bool {
        self.as_bytes().eq(other.as_bytes().unwrap())
    }
}

impl<'a> Eq for UnsizedCStr<'a> {}

impl<'a, 'b> PartialOrd<UnsizedCStr<'a>> for UnsizedCStr<'b> {
    fn partial_cmp(&self, other: &UnsizedCStr<'a>) -> Option<Ordering> {
        self.as_bytes().partial_cmp(other.as_bytes())
    }
}

impl<'a> PartialOrd<CStr> for UnsizedCStr<'a> {
    fn partial_cmp(&self, other: &CStr) -> Option<Ordering> {
        self.as_bytes().partial_cmp(other.to_bytes())
    }
}

impl<'a, const N: usize> PartialOrd<ConstSizeCStr<N>> for UnsizedCStr<'a> {
    fn partial_cmp(&self, other: &ConstSizeCStr<N>) -> Option<Ordering> {
        self.as_bytes().partial_cmp(other.as_bytes().unwrap())
    }
}

impl<'a> Ord for UnsizedCStr<'a> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.as_bytes().cmp(other.as_bytes())
    }
}

impl<'a> Hash for UnsizedCStr<'a> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.as_bytes().hash(state)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::ffi::CString;
    use std::convert::{TryInto, TryFrom};

    const TEST: UnsizedCStr = UnsizedCStr::from_bstr_const(b"Hello, World!\0");
    const TEST1: UnsizedCStr = UnsizedCStr::from_str_const("Hello, World!\0");

    #[test]
    fn test() {
        let c_string = CString::new("Hello, World!").unwrap();

        let s = UnsizedCStr::from_cstr(c_string.as_c_str());

        dbg!(&s);
        dbg!(&TEST);

        let mut b = String::from("Hello, World!\0");

        // let d: UnsizedCStr = b.as_str().try_into().unwrap();
        let d = UnsizedCStr::from_str(b.as_str()).unwrap();

        dbg!(&d);

        b.replace_range(7..12, "Rust");

        let d = UnsizedCStr::from_str(b.as_str()).unwrap();

        let e: &str = d.try_into().unwrap();

        dbg!((&e, &d));

        let f: String = d.try_into().unwrap();

        dbg!(&f);

        let mut g: UnsizedCStr = Default::default();

        dbg!(&g);

        g = UnsizedCStr::from_str("Test\0").unwrap();

        dbg!(&g);

        g = UnsizedCStr::from_cstr(c_string.as_c_str());

        dbg!(&g);

        assert_eq!(s, TEST);
        assert_eq!(TEST, TEST1);

    }
}