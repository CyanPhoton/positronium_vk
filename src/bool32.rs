#[repr(u32)]
#[derive(Eq, PartialEq, Copy, Clone, Ord, PartialOrd, Debug, Hash)]
pub enum Bool32 {
    False = 0,
    True = 1,
}

impl Bool32 {
    pub fn value(self) -> bool {
        self.into()
    }

    pub fn new(value: bool) -> Self {
        value.into()
    }

    pub fn is_true(self) -> bool {
        self.value()
    }

    pub fn is_false(self) -> bool {
        !self.value()
    }
}

impl From<bool> for Bool32 {
    fn from(value: bool) -> Self {
        if value {
            Bool32::True
        } else {
            Bool32::False
        }
    }
}

impl Into<bool> for Bool32 {
    fn into(self) -> bool {
        self == Bool32::True
    }
}
