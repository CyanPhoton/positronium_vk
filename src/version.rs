#[repr(transparent)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Version {
    pub value: u32,
}

impl Version {
    pub const MAX_VARIANT: u32 = 0x7;
    pub const MAX_MAJOR: u32 = 0x7F;
    pub const MAX_MINOR: u32 = 0x3FF;
    pub const MAX_PATCH: u32 = 0xFFF;

    pub const fn new(variant: u32, major: u32, minor: u32, patch: u32) -> Version {
        // assert!(major < Version::MAX_MAJOR);
        // assert!(minor < Version::MAX_MINOR);
        // assert!(patch < Version::MAX_PATCH);
        // Note: would like to use these asserts to help ensure correctness, but can't in const yet,
        //       vk.xml don't have any checks though, so whatever

        Version {
            value: (variant << 29) | (major << 22) | (minor << 12) | patch,
        }
    }

    pub const fn from_u32(value: u32) -> Version {
        Version {
            value
        }
    }

    pub fn new_checked(variant: u32, major: u32, minor: u32, patch: u32) -> Version {
        assert!(variant < Version::MAX_VARIANT);
        assert!(major < Version::MAX_MAJOR);
        assert!(minor < Version::MAX_MINOR);
        assert!(patch < Version::MAX_PATCH);

        Version::new(variant, major, minor, patch)
    }

    pub fn get_variant(self) -> u32 {
        (self.value >> 29) & Self::MAX_VARIANT
    }

    pub fn set_variant(&mut self, variant: u32) {
        assert!(variant <= Version::MAX_VARIANT);

        self.value = (self.value & !(Self::MAX_VARIANT << 29)) | (variant << 29);
    }

    pub fn get_major(self) -> u32 {
        (self.value >> 22) & Self::MAX_MAJOR
    }

    pub fn set_major(&mut self, major: u32) {
        assert!(major <= Version::MAX_MAJOR);

        self.value = (self.value & !(Self::MAX_MAJOR << 22)) | (major << 22);
    }

    pub fn get_minor(self) -> u32 {
        (self.value >> 12) & Self::MAX_MINOR
    }

    pub fn set_minor(&mut self, minor: u32) {
        assert!(minor <= Version::MAX_MINOR);

        self.value = (self.value & !(Self::MAX_MINOR << 12)) | (minor << 12);
    }

    pub fn get_patch(self) -> u32 {
        self.value & Self::MAX_PATCH
    }

    pub fn set_patch(&mut self, patch: u32) {
        assert!(patch <= Version::MAX_PATCH);

        self.value = (self.value & !Self::MAX_PATCH) | patch;
    }
}

impl From<u32> for Version {
    fn from(v: u32) -> Self {
        Version::from_u32(v)
    }
}

impl From<Version> for u32 {
    fn from(v: Version) -> Self {
        v.value
    }
}

impl std::fmt::Debug for Version {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Version")
            .field("value", &self.value)
            .field("variant", &self.get_variant())
            .field("major", &self.get_major())
            .field("minor", &self.get_minor())
            .field("patch", &self.get_patch())
            .finish()
    }
}
