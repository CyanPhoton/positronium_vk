use roxmltree::{Document, Node};

use commands::Command;
use enums::Enum;
use features_extensions::Extension;
use features_extensions::Feature;
use platforms::Platform;
use tags::Tag;
use types::Type;

pub mod platforms;
pub mod tags;
pub mod types;
pub mod enums;
pub mod commands;
pub mod features_extensions;
pub mod common;

pub fn parse(spec_xml: String, video_spec_xml: String) -> Result<Registry, roxmltree::Error> {
    let document = Document::parse(spec_xml.as_str())?;
    let video_document = Document::parse(video_spec_xml.as_str())?;

    Ok(Registry::from_node(document.root_element()).with(video_document.root_element()))
}

fn parse_children<F: Fn(Node) -> T, T>(node: Node, values: &mut Vec<T>, f: F) {
    values.extend(node.children().filter(Node::is_element).filter(|n| !n.has_tag_name("comment")).filter(check_api).map(f))
}

fn as_bool(s: &str) -> bool {
    s.trim().eq_ignore_ascii_case("true")
}

pub fn check_api(node: &Node) -> bool {
    node.attribute("api").map_or(true, |api| api.split(',').any(|api| api == "vulkan"))
}

trait FromNode {
    fn from_node(node: Node) -> Self;
}

#[derive(Debug, Default)]
pub struct Registry {
    pub platforms: Vec<Platform>,
    pub tags: Vec<Tag>,
    pub types: Vec<Type>,
    pub enums: Vec<Enum>,
    pub commands: Vec<Command>,
    pub features: Vec<Feature>,
    pub extensions: Vec<Extension>,
}

impl FromNode for Registry {
    fn from_node(node: Node) -> Self {
        Registry::default().with(node)
    }
}

impl Registry {
    fn with(mut self, node: Node) -> Self {
        assert!(node.has_tag_name("registry"));

        for child in node.children().filter(Node::is_element) {
            match child.tag_name().name() {
                "platforms" => parse_children(child, &mut self.platforms, Platform::from_node),
                "tags" => parse_children(child, &mut self.tags, Tag::from_node),
                "types" if check_api(&child) => parse_children(child, &mut self.types, Type::from_node),
                "types" => {},
                "enums" if check_api(&child) => self.enums.push(Enum::from_node(child)),
                "enums" => {},
                "commands" if check_api(&child) => parse_children(child, &mut self.commands, Command::from_node),
                "commands" => {},
                "feature" => self.features.push(Feature::from_node(child)),
                "extensions" => parse_children(child, &mut self.extensions, Extension::from_node),
                "comment" => {}
                t => println!("Ignored root tag: {}", t)
            }
        }

        self
    }
}