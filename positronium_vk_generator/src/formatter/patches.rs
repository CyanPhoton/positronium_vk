use std::collections::HashMap;
use crate::parser::commands::Command;
use crate::parser::common::{ExternSyncMode, ParameterContents};
use crate::parser::Registry;
use crate::parser::types::Type;

pub fn patch_extern_sync(registry: &mut Registry) {

    let mut struct_map: HashMap<_, _> = registry.types.iter_mut().filter_map(|t| {
        if let Type::StructUnion { name, members, .. } = t {
            Some((name.clone(), members))
        } else {
            None
        }
    }).collect();

    for cmd in &registry.commands {
        if let Command::Definition { param , ..} = cmd {
            for p in param {
                if let Some(ExternSyncMode::Fields(fields)) = &p.extern_sync {
                    for f in fields {
                        let chain = f.split("->").flat_map(|f| f.split("[].")).map(|f| f.trim_end_matches("[]")).skip(1).collect::<Vec<_>>();

                        let mut s = p.contents.iter().filter_map(|c| if let ParameterContents::Type(t) = c { Some(t.as_str()) } else { None }).collect::<String>();

                        for c in chain.iter().take(chain.len() - 1) {
                            let members = struct_map.get_mut(&s).unwrap();

                            s = members.iter_mut().find_map(|m| {
                                let n = m.contents.iter().filter_map(|c| if let ParameterContents::Name(n) = c { Some(n.as_str()) } else { None }).collect::<String>();
                                if &n == c {
                                    let ns = m.contents.iter().filter_map(|c| if let ParameterContents::Type(t) = c { Some(t.as_str()) } else { None }).collect::<String>();
                                    m.contents.retain(|c| if let ParameterContents::Code(code) = c { code.trim() != "const" } else { true });
                                    m.extern_sync = Some(ExternSyncMode::Bool(true));
                                    Some(ns)
                                } else {
                                    None
                                }
                            }).unwrap();
                        }

                        let members = struct_map.get_mut(&s).unwrap();
                        for m in members.iter_mut() {
                            let n = m.contents.iter().filter_map(|c| if let ParameterContents::Name(n) = c { Some(n.as_str()) } else { None }).collect::<String>();
                            if &n == chain.last().unwrap() {
                                m.extern_sync = Some(ExternSyncMode::Bool(true));
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}