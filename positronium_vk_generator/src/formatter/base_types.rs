use std::fmt::{Display, Formatter};

use crate::formatter::{CName, Named, RSName};
use crate::parser::Registry;
use crate::parser::types::{OtherContents, Type, TypeCategories};

use super::{c_type_to_rs_type, trim_vk_prefix};

#[derive(Debug)]
pub struct BaseType {
    pub name: (CName, RSName),
    pub t: Option<(CName, RSName)>,
    pub is_ptr: bool
}

impl Named for BaseType {
    fn get_names(&self) -> Vec<(&CName, &RSName)> {
        vec![(&self.name.0, &self.name.1)]
    }
}

pub fn format_base_types(registry: &Registry) -> Vec<BaseType> {
    registry.types.iter().filter_map(|t|
        match t {
            Type::Other { category, contents, .. } => {
                if category == &TypeCategories::BaseType {
                    let c_name = contents.iter().find_map(|c| if let OtherContents::Name(name) = &c { Some(name) } else { None }).unwrap().clone();
                    let rs_name = trim_vk_prefix(&c_name).to_string();

                    let c_type = contents.iter().find_map(|c| if let OtherContents::Type(t) = &c { Some(t) } else { None }).cloned();
                    let rs_type = c_type.as_ref().map(c_type_to_rs_type);

                    let is_ptr = contents.iter().any(|c| if let OtherContents::Code(s) = c { s.contains("*") } else { false });

                    Some(BaseType {
                        name: (c_name, rs_name),
                        t: c_type.zip(rs_type),
                        is_ptr
                    })
                } else {
                    None
                }
            }
            _ => None
        }).collect()
}

impl Display for BaseType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if let Some((_, rs_type)) = &self.t {
            match self.name.0.as_str() {
                "VkBool32" => {
                    assert_eq!(rs_type.as_str(), "u32");
                }
                _ => {
                    //TODO: Potentially add more type safety to the other base types
                    writeln!(f, "pub type {} = {};\n", self.name.1, rs_type)?;
                }
            }
        } else if self.is_ptr {
            writeln!(f, "pub type {} = *mut std::ffi::c_void;\n", self.name.1)?;
        } else {
            writeln!(f, "fieldless_debug_derive!({});", self.name.1)?;
            writeln!(f, "#[repr(C)]")?;
            writeln!(f, "pub struct {} {{ #[doc(hidden)] _private: [u8; 0] }}\n", self.name.1)?;
        }

        Ok(())
    }
}
