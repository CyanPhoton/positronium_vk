use std::fmt::{Display, Formatter};

use heck::CamelCase;

use crate::formatter::{CName, Named, RSName};
use crate::formatter::disabled_manager::DisabledManager;
use crate::parser::Registry;
use crate::parser::types::{Type, TypeCategories};

#[derive(Debug)]
pub struct ExternType {
    pub(crate) requires: Option<String>,
    pub(crate) name: (CName, RSName),
}

impl Named for ExternType {
    fn get_names(&self) -> Vec<(&CName, &RSName)> {
        vec![(&self.name.0, &self.name.1)]
    }
}

pub fn format_extern_types(registry: &Registry, disabled_manager: &mut DisabledManager) -> Vec<ExternType> {
    registry.types.iter().filter_map(|t|
        match t {
            Type::Other { category, requires, name, .. } => {
                if category == &TypeCategories::None {
                    let c_name = name.clone().unwrap();
                    let rs_name = match requires.as_ref().map(String::as_str) {
                        Some("X11/Xlib.h" | "X11/extensions/Xrandr.h" | "windows.h" | "xcb/xcb.h" | "directfb.h" | "zircon/types.h" | "ggp_c/vulkan_types.h" | "nvscisync.h" | "nvscibuf.h") => Some(c_name.clone()),
                        Some("wayland-client.h") => Some(c_name.to_camel_case()),
                        Some("screen/screen.h") => Some(c_name[1..].to_string()),
                        Some("vk_platform") | None => Some(c_name.clone()),
                        Some("stdint") => None,
                        Some(e) => {
                            if e.starts_with("vk_video/") {
                                None
                            } else {
                                panic!("Missed extern type: {}", e);
                            }
                        }
                    };

                    rs_name.map(|rs_name| ExternType {
                        requires: requires.clone(),
                        name: (c_name, rs_name),
                    })
                } else {
                    None
                }
            }
            _ => None
        }).collect()
}

impl Display for ExternType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self.requires.as_ref().map(String::as_str) {
            Some("X11/Xlib.h") => {
                match self.name.0.as_str() {
                    "Display" => {
                        writeln!(f, "fieldless_debug_derive!({});", self.name.0)?;
                        writeln!(f, "#[repr(C)]")?;
                        writeln!(f, "#[derive(Copy, Clone, Default)]")?;
                        writeln!(f, "pub struct {} {{ _private: [u8; 0] }}", self.name.0)?;
                        writeln!(f, "")?;
                    }
                    "VisualID" | "Window" => {
                        writeln!(f, "pub type {} = std::os::raw::c_ulong;", self.name.0)?;
                    }
                    s => panic!("Unknown Xlib symbol needed: {}", s)
                };
            }
            Some("X11/extensions/Xrandr.h") => {
                match self.name.0.as_str() {
                    "RROutput" => {
                        writeln!(f, "pub type {} = std::os::raw::c_ulong;", self.name.0)?;
                    }
                    s => panic!("Unknown Xrandr symbol needed: {}", s)
                };
            }
            Some("wayland-client.h") => {
                match self.name.0.as_str() {
                    "wl_display" | "wl_surface" => {
                        writeln!(f, "fieldless_debug_derive!({});", self.name.0.to_camel_case())?;
                        writeln!(f, "#[repr(C)]")?;
                        writeln!(f, "#[derive(Copy, Clone, Default)]")?;
                        writeln!(f, "pub struct {} {{ _private: [u8; 0] }}", self.name.0.to_camel_case())?;
                        writeln!(f, "")?;
                    }
                    s => panic!("Unknown Xrandr symbol needed: {}", s)
                };
            }
            Some("windows.h") => {
                match self.name.0.as_str() {
                    "SECURITY_ATTRIBUTES" => {
                        writeln!(f, "fieldless_debug_derive!({});", self.name.0)?;
                        writeln!(f, "#[repr(C)]")?;
                        writeln!(f, "#[derive(Copy, Clone, Default)]")?;
                        writeln!(f, "pub struct {} {{ _private: [u8; 0] }}", self.name.0)?;
                        writeln!(f, "")?;
                    }
                    "HINSTANCE" | "HWND" | "HMONITOR" | "HMONITOR" | "HANDLE" | "LPCWSTR" => {
                        writeln!(f, "pub type {} = *const std::os::raw::c_void;", self.name.0)?;
                    }
                    "DWORD" => {
                        writeln!(f, "pub type {} = std::os::raw::c_ulong;", self.name.0)?;
                    }
                    s => panic!("Unknown Win32 symbol needed: {}", s)
                };
            }
            Some("xcb/xcb.h") => {
                match self.name.0.as_str() {
                    "xcb_connection_t" => {
                        writeln!(f, "fieldless_debug_derive!({});", self.name.0)?;
                        writeln!(f, "#[repr(C)]")?;
                        writeln!(f, "#[derive(Copy, Clone, Default)]")?;
                        writeln!(f, "pub struct {} {{ _private: [u8; 0] }}", self.name.0)?;
                        writeln!(f, "")?;
                    }
                    "xcb_visualid_t" | "xcb_window_t" => {
                        writeln!(f, "pub type {} = u32;", self.name.0)?;
                    }
                    s => panic!("Unknown xcb symbol needed: {}", s)
                };
            }
            Some("directfb.h") => {
                writeln!(f, "fieldless_debug_derive!({});", self.name.0)?;
                writeln!(f, "#[repr(C)]")?;
                writeln!(f, "#[derive(Copy, Clone, Default)]")?;
                writeln!(f, "pub struct {} {{ _private: [u8; 0] }}", self.name.0)?;
                writeln!(f, "")?;
            }
            Some("zircon/types.h") => {
                match self.name.0.as_str() {
                    "zx_handle_t" => {
                        writeln!(f, "pub type {} = u32;", self.name.0)?;
                    }
                    s => panic!("Unknown zircon symbol needed: {}", s)
                }
            }
            Some("ggp_c/vulkan_types.h") => {
                writeln!(f, "fieldless_debug_derive!({});", self.name.0)?;
                writeln!(f, "#[repr(C)]")?;
                writeln!(f, "#[derive(Copy, Clone, Default)]")?;
                writeln!(f, "pub struct {} {{ _private: [u8; 0] }}", self.name.0)?;
                writeln!(f, "")?;
            }
            Some("screen/screen.h") => {
                writeln!(f, "fieldless_debug_derive!({});", self.name.0.trim_start_matches('_'))?;
                writeln!(f, "#[repr(C)]")?;
                writeln!(f, "#[derive(Copy, Clone, Default)]")?;
                writeln!(f, "pub struct {} {{ _private: [u8; 0] }}", self.name.0.trim_start_matches('_'))?;
                writeln!(f, "")?;
            }
            Some("nvscisync.h" | "nvscibuf.h") => {}
            Some("vk_platform") | None => {}
            Some(e) => {
                panic!("Missed extern type: {}", e);
            }
        }

        Ok(())
    }
}