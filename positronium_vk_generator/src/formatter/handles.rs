use std::collections::HashMap;
use std::fmt::{Display, Formatter};
use std::fmt::Write;

use heck::SnakeCase;

use crate::formatter::{CName, INDENT, Named, RSName, Spec, trim_vk_prefix};
use crate::formatter::common::{global_conversion, ParameterValueType};
use crate::formatter::functions::{Function, OxidisedParameterType};
use crate::parser::Registry;
use crate::parser::types::{OtherContents, Type, TypeCategories};

#[derive(Debug, Clone)]
pub struct Handle {
    pub name: (CName, RSName),
    /// bool is for if parent has dropper
    pub parent: Option<(CName, RSName, bool)>,
    /// bool is for if additional parent(s) has dropper
    pub additional_parents: Vec<(CName, RSName, bool)>,
    pub dispatchable: Option<bool>,
    pub aliases: Vec<(CName, RSName)>,
    pub functions: Vec<(Function, bool)>,
    pub lifetimes_names: Vec<String>,
    pub destructor: Option<Function>,
    pub uses_parent: bool,
    pub destructor_uses_alloc: bool,
    pub destructor_uses_slice: bool,
    pub destructor_uses_grandparent: bool,
    pub destructor_exludes_parent: bool,
    pub expose_data: bool,
    pub loads_fn_ptrs: Option<RSName>,
    pub nearest_loader_parent_dist: usize,
}

impl Named for Handle {
    fn get_names(&self) -> Vec<(&CName, &RSName)> {
        core::iter::once((&self.name.0, &self.name.1))
            .chain(self.aliases.iter().map(|(c, rs)| (c, rs)))
            .collect()
    }
}

pub fn format_handles(registry: &Registry) -> Vec<Handle> {
    let mut handles: Vec<Handle> = Vec::new();
    let mut map: HashMap<CName, usize> = HashMap::new();

    let loader_name: (CName, RSName) = ("VkLoader".to_string(), "Loader".to_string());
    let loader_as_parent: (CName, RSName, bool) = ("VkLoader".to_string(), "Loader".to_string(), false);

    handles.push(Handle {
        name: loader_name.clone(),
        parent: None,
        additional_parents: vec![("Library".to_string(), "Library".to_string(), true)],
        dispatchable: None,
        aliases: vec![],
        functions: vec![],
        lifetimes_names: vec![],
        destructor: None,
        uses_parent: false,
        destructor_uses_alloc: false,
        destructor_uses_slice: false,
        destructor_uses_grandparent: false,
        destructor_exludes_parent: false,
        expose_data: true,
        loads_fn_ptrs: Some("GetInstanceProcAddr".to_string()),
        nearest_loader_parent_dist: 0,
    });

    for t in registry.types.iter() {
        match t {
            Type::Other { category, parent, name, alias, contents, .. } => {
                if category == &TypeCategories::Handle {
                    if let Some((name, alias)) = name.as_ref().zip(alias.as_ref()) {
                        handles[map[alias]].aliases.push((name.clone(), global_conversion(trim_vk_prefix(name.as_str()))));
                    } else {
                        let c_name = contents.iter().find_map(|c| if let OtherContents::Name(name) = &c { Some(name) } else { None }).unwrap().clone();
                        let rs_name = global_conversion(trim_vk_prefix(c_name.as_str()));

                        let rs_parent = parent.as_ref().map(String::as_str).map(trim_vk_prefix).map(str::to_string);

                        let c_type = contents.iter().find_map(|c| if let OtherContents::Type(t) = &c { Some(t) } else { None }).unwrap().clone();

                        map.insert(c_name.clone(), handles.len());

                        let (mut parent, mut additional_parents) = if &c_name == "VkSwapchainKHR"  {
                            (parent.clone().zip(rs_parent).map(|p| (p.0, p.1, false)), vec![("VkSurfaceKHR".to_string(), "SurfaceKHR".to_string(), false)])
                        } else {
                            (parent.clone().zip(rs_parent).map(|p| (p.0, p.1, false)), vec![])
                        };

                        let uses_parent = if &c_name == "VkInstance" {
                            parent = Some(loader_as_parent.clone());
                            false
                        } else {
                            true
                        };

                        let (destructor_uses_grandparent, destructor_exludes_parent) = if &c_name == "VkDevice" {
                            (false, true)
                        } else if &c_name == "VkVideoSessionParametersKHR" {
                            (true, true)
                        } else {
                            (false, false)
                        };

                        let loads_fn_ptrs = match c_name.as_str() {
                            "VkInstance" => Some("GetInstanceProcAddr".to_string()),
                            "VkDevice" => Some("GetDeviceProcAddr".to_string()),
                            _ => None
                        };

                        handles.push(Handle {
                            name: (c_name, rs_name),
                            parent,
                            additional_parents,
                            dispatchable: Some(c_type == "VK_DEFINE_HANDLE"),
                            aliases: vec![],
                            functions: vec![],
                            lifetimes_names: vec![],
                            destructor: None,
                            uses_parent,
                            destructor_uses_alloc: false,
                            destructor_uses_slice: false,
                            destructor_uses_grandparent,
                            destructor_exludes_parent,
                            expose_data: true,
                            loads_fn_ptrs,
                            nearest_loader_parent_dist: 0,
                        })
                    }
                }
            }
            _ => {}
        }
    }

    let mut handle_map: HashMap<CName, _> = handles.iter().enumerate().map(|(i, h)| (h.name.0.clone(), (h.clone(), i))).collect();
    let keys: Vec<_> = handle_map.keys().cloned().collect();

    for k in keys {
        let (h, i) = handle_map.remove(&k).unwrap();

        let mut p = h.clone();
        while let Some((pp, _)) = p.parent.map(|p| handle_map.get(&p.0)).flatten() {
            if p.loads_fn_ptrs.is_some() {
                break;
            }
            handles[i].nearest_loader_parent_dist += 1;
            p = pp.clone();
        }

        handle_map.insert(k, (h, i));
    }

    fn get_lifetime_names(index: usize, handles: &mut Vec<Handle>, map: &HashMap<CName, usize>) -> Vec<String> {
        let this = &mut handles[index];

        if !this.lifetimes_names.is_empty() {
            return this.lifetimes_names.clone();
        }

        if let Some(parent) = &this.parent {
            let j = map[&parent.0];

            std::iter::once(parent.1.to_snake_case()).chain(get_lifetime_names(j, handles, map)).collect()
        } else {
            Vec::new()
        }
    }

    for i in 0..handles.len() {
        // Note: Uncomment to restore parent/grandparent lifetimes
        //handles[i].lifetimes_names = get_lifetime_names(i, &mut handles, &map);
    }

    // TODO
    // for c in registry.commands.iter() {
    //     match c {
    //         Command::Definition { queues, success_codes, error_codes, render_pass, cmd_buffer_level, pipeline, proto, param } => {
    //
    //         }
    //         Command::Alias { name, alias } => {
    //
    //         }
    //     }
    // }

    handles
}

impl Handle {
    pub fn format(&self, spec: &Spec) -> Result<String, std::fmt::Error> {
        let mut f = String::new();

        let has_dropper = (self.destructor.is_some() || self.parent.as_ref().map_or(false, |p| p.2) || self.additional_parents.iter().any(|p| p.2));

        // Handle
        if self.dispatchable.is_some() {
            writeln!(f, "single_field_debug_derive!({}, handle);", self.name.1)?;
        }
        writeln!(f, "pub struct {} {{", self.name.1)?;
        if self.dispatchable.is_some() {
            if self.expose_data {
                writeln!(f, "{}pub(crate) handle: {}Raw,", INDENT, self.name.1)?;
            } else {
                writeln!(f, "{}handle: {}Raw,", INDENT, self.name.1)?;
            }
        }
        writeln!(f, "{INDENT}pub data: {}Data,", self.name.1)?;

        writeln!(f, "}}")?;

        writeln!(f, "")?;

        // Handle Type
        if self.dispatchable.is_some() {
            writeln!(f, "single_field_debug_derive!({}Handle<'_>, handle);", self.name.1)?;
            writeln!(f, "#[repr(transparent)]")?;
            writeln!(f, "#[derive(Copy, Clone)]")?;
            writeln!(f, "pub struct {}Handle<'{}> {{", self.name.1, self.name.1.to_snake_case())?;
            if self.expose_data {
                writeln!(f, "{}pub(crate) handle: {}Raw,", INDENT, self.name.1)?;
            } else {
                writeln!(f, "{}handle: {}Raw,", INDENT, self.name.1)?;
            }
            writeln!(f, "{}_phantom: core::marker::PhantomData<&'{} [u8;0]>,", INDENT, self.name.1.to_snake_case())?;
            writeln!(f, "}}")?;

            writeln!(f, "")?;

            writeln!(f, "unsafe impl IsHandle for {0}Handle<'static> {{}}", self.name.1)?;
            writeln!(f, "unsafe impl<'handle> IsHandleRef<'handle> for {0}Handle<'handle> {{}}", self.name.1)?;
            writeln!(f, "")?;
        }

        // Mut Handle Type
        if self.dispatchable.is_some() {
            writeln!(f, "single_field_debug_derive!({}MutHandle<'_>, handle);", self.name.1)?;
            writeln!(f, "#[repr(transparent)]")?;
            writeln!(f, "#[derive(Copy, Clone)]")?;
            writeln!(f, "pub struct {}MutHandle<'{}> {{", self.name.1, self.name.1.to_snake_case())?;
            if self.expose_data {
                writeln!(f, "{}pub(crate) handle: {}MutRaw,", INDENT, self.name.1)?;
            } else {
                writeln!(f, "{}handle: {}MutRaw,", INDENT, self.name.1)?;
            }
            writeln!(f, "{}_phantom: core::marker::PhantomData<&'{} mut [u8;0]>,", INDENT, self.name.1.to_snake_case())?;
            writeln!(f, "}}")?;

            writeln!(f, "")?;

            writeln!(f, "unsafe impl IsHandle for {0}MutHandle<'static> {{}}", self.name.1)?;
            writeln!(f, "unsafe impl<'handle> IsHandleRef<'handle> for {0}MutHandle<'handle> {{}}", self.name.1)?;
            writeln!(f, "")?;

            // writeln!(f, "impl<'{0}> {1}MutHandle<'{0}> {{", self.name.1.to_snake_case(), self.name.1)?;
            // writeln!(f, "{0}pub fn mut_share<I, A: Fn(&mut I) -> &mut Self, U: FnMut(&mut [I])>(&mut self, data: &mut [I], accessor: A, mut user: U) {{", INDENT)?;
            // writeln!(f, "{0}{0}for i in 0..data.len() {{", INDENT)?;
            // writeln!(f, "{0}{0}{0}accessor(&mut data[i]).handle = self.handle;", INDENT)?;
            // writeln!(f, "{0}{0}}}", INDENT)?;
            // writeln!(f, "{0}{0}user(data);", INDENT)?;
            // writeln!(f, "{0}{0}for i in 0..data.len() {{", INDENT)?;
            // writeln!(f, "{0}{0}{0}accessor(&mut data[i]).handle = Handle::null();", INDENT)?;
            // writeln!(f, "{0}{0}}}", INDENT)?;
            // writeln!(f, "{0}}}", INDENT)?;
            // writeln!(f, "")?;
            // writeln!(f, "{0}pub fn mut_multi_share(&'{1} mut self) -> MutHandleMultiShareEmptyBase<'{1}, Self> {{", INDENT, self.name.1.to_snake_case())?;
            // writeln!(f, "{0}{0}MutHandleMultiShareEmptyBase {{ handle: self }}", INDENT)?;
            // writeln!(f, "{0}}}", INDENT)?;
            // writeln!(f, "")?;
            // writeln!(f, "{0}pub fn mut_multi_share_with<'slice, I, A: Fn(&mut I) -> &mut Self>(&'{1} mut self, slice: &'slice mut [I], accessor: A) -> MutHandleMultiShareBase<'{1}, Self, MutHandleMultiShare<'{1}, 'slice, Self, I, A>> {{", INDENT, self.name.1.to_snake_case())?;
            // writeln!(f, "{0}{0}for v in slice.iter_mut() {{", INDENT)?;
            // writeln!(f, "{0}{0}{0}accessor(v).handle = self.handle;", INDENT)?;
            // writeln!(f, "{0}{0}}}", INDENT)?;
            // writeln!(f, "{0}{0}MutHandleMultiShareBase {{ handle: self, multi_slices: MutHandleMultiShare {{", INDENT)?;
            // writeln!(f, "{0}{0}{0}_handle: Default::default(),", INDENT)?;
            // writeln!(f, "{0}{0}{0}slice,", INDENT)?;
            // writeln!(f, "{0}{0}{0}accessor,", INDENT)?;
            // writeln!(f, "{0}{0}}}}}", INDENT)?;
            // writeln!(f, "{0}}}", INDENT)?;
            // writeln!(f, "}}")?;
            //
            // writeln!(f, "")?;
        }

        // Impl Handle
        writeln!(f, "impl {} {{", self.name.1)?;

        let prefixes = ["pub(crate) fn new", "pub unsafe fn from_handle"];

        for p in prefixes {
            if &self.name.1 != "Loader" {
                if let Some(loads_fn_ptrs) = &self.loads_fn_ptrs {
                    let mut names = String::new();
                    if self.expose_data {
                        write!(f, "{}{p}(", INDENT)?;
                    } else {
                        write!(f, "{}fn new(", INDENT)?;
                    }
                    if self.dispatchable.is_some() {
                        write!(f, "handle: {}Raw, ", self.name.1)?;
                        write!(names, "handle, ")?;
                    }
                    if let Some(parent) = &self.parent {
                        write!(f, "parent_data: &{}Data, ", parent.1)?;
                        write!(names, "parent_data, ")?;
                    }
                    for add_par in &self.additional_parents {
                        write!(f, "{}_data: &{}Data, ", add_par.1.to_snake_case(), add_par.1)?;
                        write!(names, "{0}_data, ", add_par.1.to_snake_case())?;
                    }
                    if self.destructor_uses_alloc {
                        write!(f, "allocator: Option<RawAllocationCallbacks>, ")?;
                        write!(names, "allocator, ")?;
                    } else {
                        write!(names, "parent_data.allocator, ")?;
                    }
                    if self.destructor.is_some() && !self.destructor_uses_slice {
                        writeln!(f, "loader: {}, destruct_on_drop: bool) -> Self {{", loads_fn_ptrs)?;
                        write!(names, "loader, destruct_on_drop")?;
                    } else {
                        writeln!(f, "loader: {}) -> Self {{", loads_fn_ptrs)?;
                        write!(names, "loader")?;
                    }

                    if self.dispatchable.is_some() {
                        writeln!(f, "{0}{0}if handle == Handle::null() {{ panic!(\"Can not create managed handle from null raw handle. Check the documentation for this function.\"); }}", INDENT)?;
                    }
                    writeln!(f, "{0}{0}{1} {{", INDENT, self.name.1)?;
                    if self.dispatchable.is_some() {
                        writeln!(f, "{0}{0}{0}handle,", INDENT)?;
                    }
                    writeln!(f, "{0}{0}{0}data: {1}Data::new({2}),", INDENT, self.name.1, names)?;

                    writeln!(f, "{0}{0}}}", INDENT)?;

                    writeln!(f, "{}}}", INDENT)?;
                } else {
                    let mut names = String::new();
                    if self.expose_data {
                        write!(f, "{}{p}(", INDENT)?;
                    } else {
                        write!(f, "{}fn new(", INDENT)?;
                    }
                    if self.dispatchable.is_some() {
                        write!(f, "handle: {}Raw, ", self.name.1)?;
                        write!(names, "handle, ")?;
                    }
                    if let Some(parent) = &self.parent {
                        write!(f, "parent_data: &{}Data, ", parent.1)?;
                        write!(names, "parent_data, ")?;
                    }
                    for add_par in &self.additional_parents {
                        write!(f, "{}_data: &{}Data, ", add_par.1.to_snake_case(), add_par.1)?;
                        write!(names, "{0}_data, ", add_par.1.to_snake_case())?;
                    }
                    if self.destructor_uses_alloc {
                        write!(f, "allocator: Option<RawAllocationCallbacks>, ")?;
                        write!(names, "allocator, ")?;
                    } else {
                        write!(names, "parent_data.allocator, ")?;
                    }
                    f = f.trim_end_matches(", ").to_string();
                    names = names.trim_end_matches(", ").to_string();

                    if self.destructor.is_some() && !self.destructor_uses_slice {
                        writeln!(f, ", destruct_on_drop: bool) -> Self {{")?;
                        write!(names, ", destruct_on_drop")?;
                    } else {
                        writeln!(f, ") -> Self {{")?;
                    }

                    writeln!(f, "{0}{0}{1} {{", INDENT, self.name.1)?;
                    if self.dispatchable.is_some() {
                        writeln!(f, "{0}{0}{0}handle,", INDENT)?;
                    }
                    writeln!(f, "{0}{0}{0}data: {1}Data::new({2}),", INDENT, self.name.1, names)?;
                    writeln!(f, "{0}{0}}}", INDENT)?;
                    writeln!(f, "{}}}", INDENT)?;
                }

                writeln!(f, "")?;
            }
        }

        if self.dispatchable.is_some() {
            writeln!(f, "{}pub fn as_handle(&self) -> {}Handle {{", INDENT, self.name.1)?;
            writeln!(f, "{0}{0}{1}Handle {{", INDENT, self.name.1)?;
            writeln!(f, "{0}{0}{0}handle: self.handle,", INDENT)?;
            writeln!(f, "{0}{0}{0}_phantom: Default::default()", INDENT)?;
            writeln!(f, "{0}{0}}}", INDENT)?;
            writeln!(f, "{}}}\n", INDENT)?;

            writeln!(f, "{}pub fn as_mut_handle(&mut self) -> {}MutHandle {{", INDENT, self.name.1)?;
            writeln!(f, "{0}{0}{1}MutHandle {{", INDENT, self.name.1)?;
            writeln!(f, "{0}{0}{0}handle: {1}MutRaw {{ handle: self.handle.handle }},", INDENT, self.name.1)?;
            writeln!(f, "{0}{0}{0}_phantom: Default::default()", INDENT)?;
            writeln!(f, "{0}{0}}}", INDENT)?;
            writeln!(f, "{}}}\n", INDENT)?;
        }

        let mut first = true;
        for (func, ox) in &self.functions {
            if *ox {
                if let Some(o) = &func.oxidised {
                    let takes_allocator = o.parameters.iter().any(|p| &p.name == "allocator");
                    let is_destructor = self.destructor.as_ref().map_or(false, |d| &d.raw.name.1 == &o.name);
                    let mut needs_next_chain = None; // Some((rt_index, raw_index, is_array))
                    if let Some(rt) = &o.return_type {
                        for (i, p) in rt.values.iter().enumerate() {
                            match p {
                                OxidisedParameterType::Value { c_param_index } => {
                                    let mut s = o.raw_parameters[*c_param_index].full_type.1.clone();
                                    if s.starts_with("&mut ") {
                                        s = s.replacen("&mut ", "", 1);
                                    } else {
                                        eprintln!("Check: {}", s);
                                    }
                                    if s.starts_with("Raw") {
                                        if let Some(s) = spec.structs_unions.iter().find(|s| s.name.0 == o.raw_parameters[*c_param_index].type_name.0) {
                                            if !s.extended_by.is_empty() {
                                                needs_next_chain = Some((i, *c_param_index, false));
                                            }
                                        }
                                    }
                                }
                                OxidisedParameterType::Array { c_ptr_index, c_len_index } => {
                                    let mut s = o.raw_parameters[*c_ptr_index].full_type.1.clone();
                                    if s.starts_with("Option<") {
                                        s = s.replacen("Option<", "", 1);
                                        s = s.rsplit_once(">").unwrap().0.to_string();
                                    }
                                    if s.starts_with("&mut ") {
                                        s = s.replacen("&mut ", "", 1);
                                    } else {
                                        eprintln!("Check (2): {}", s);
                                    }
                                    if s.starts_with("Raw") {
                                        if let Some(s) = spec.structs_unions.iter().find(|s| s.name.0 == o.raw_parameters[*c_ptr_index].type_name.0) {
                                            if !s.extended_by.is_empty() {
                                                needs_next_chain = Some((i, *c_ptr_index, true));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if let Some(a) = &self.loads_fn_ptrs {
                        if &a.to_snake_case() != &o.name {
                            if !first {
                                writeln!(f, "")?;
                            }
                            first = false;
                            writeln!(f, "{}", o.format(self, spec, false, takes_allocator && !is_destructor, self.destructor_uses_alloc, is_destructor, false, None, false)?)?;
                            if takes_allocator && !is_destructor {
                                writeln!(f, "\n{}", o.format(self, spec, true, false, self.destructor_uses_alloc, is_destructor, false, None, false)?)?;
                            }
                            if needs_next_chain.is_some() {
                                writeln!(f, "\n{}", o.format(self, spec, false, false, self.destructor_uses_alloc, is_destructor, false, needs_next_chain, false)?)?;
                            }
                            if o.return_type.is_some() && o.return_type.as_ref().unwrap().values.iter().any(|r| match r {
                                OxidisedParameterType::Value { c_param_index } => o.raw_parameters[*c_param_index].v_type != ParameterValueType::Handle,
                                OxidisedParameterType::Array { c_ptr_index, c_len_index } => true
                            }) {
                                if !takes_allocator || is_destructor {
                                    writeln!(f, "\n{}", o.format(self, spec, false, takes_allocator && !is_destructor, self.destructor_uses_alloc, is_destructor, false, None, true)?)?;
                                } else {
                                    writeln!(f, "\n{}", o.format(self, spec, true, takes_allocator && !is_destructor, self.destructor_uses_alloc, is_destructor, false, None, true)?)?;
                                }
                            }
                            if o.name.starts_with("cmd_") {
                                writeln!(f, "\n{}", o.format(self, spec, false, false, self.destructor_uses_alloc, is_destructor, true, None, false)?)?;
                            }
                        }
                    } else {
                        if !first {
                            writeln!(f, "")?;
                        }
                        first = false;
                        writeln!(f, "{}", o.format(self, spec, false, takes_allocator && !is_destructor, self.destructor_uses_alloc, is_destructor, false, None, false)?)?;
                        if takes_allocator && !is_destructor {
                            writeln!(f, "\n{}", o.format(self, spec, true, false, self.destructor_uses_alloc, is_destructor, false, None, false)?)?;
                        }
                        if needs_next_chain.is_some() {
                            writeln!(f, "\n{}", o.format(self, spec, false, takes_allocator && !is_destructor, self.destructor_uses_alloc, is_destructor, false, needs_next_chain, false)?)?;
                        }
                        if o.return_type.is_some() && o.return_type.as_ref().unwrap().values.iter().any(|r| match r {
                            OxidisedParameterType::Value { c_param_index } => o.raw_parameters[*c_param_index].v_type != ParameterValueType::Handle,
                            OxidisedParameterType::Array { c_ptr_index, c_len_index } => true
                        }) {
                            if !takes_allocator || is_destructor {
                                writeln!(f, "\n{}", o.format(self, spec, false, takes_allocator && !is_destructor, self.destructor_uses_alloc, is_destructor, false, None, true)?)?;
                            } else {
                                writeln!(f, "\n{}", o.format(self, spec, true, takes_allocator && !is_destructor, self.destructor_uses_alloc, is_destructor, false, None, true)?)?;
                            }
                        }
                        if o.name.starts_with("cmd_") {
                            writeln!(f, "\n{}", o.format(self, spec, false, false, self.destructor_uses_alloc, is_destructor, true, None, false)?)?;
                        }
                    }
                }
            }
        }

        if self.loads_fn_ptrs.is_some() {
            //    pub fn is_function_loaded<S: AsRef<str>>(&self, name: S) -> bool {
            //         self.data.loaded_functions.contains(name.as_ref())
            //     }
            writeln!(f, "")?;
            writeln!(f, "{0}pub fn is_function_loaded<S: AsRef<str>>(&self, name: S) -> bool {{", INDENT)?;
            writeln!(f, "{0}{0}self.data.loaded_functions.contains(name.as_ref())", INDENT)?;
            writeln!(f, "{0}}}", INDENT)?;
        }

        writeln!(f, "}}")?;

        writeln!(f, "")?;

        // Handle null impl

        writeln!(f, "#[cfg(feature=\"nullable_functional_handles\")]")?;
        writeln!(f, "unsafe impl IsHandle for {} {{", self.name.1)?;
        writeln!(f, "{}fn null() -> Self {{", INDENT)?;
        writeln!(f, "{0}{0}{1} {{", INDENT, self.name.1)?;
        if self.dispatchable.is_some() {
            writeln!(f, "{0}{0}{0}handle: Handle::null(),", INDENT)?;
        }
        writeln!(f, "{0}{0}{0}data: {1}Data::null(),", INDENT, self.name.1)?;
        writeln!(f, "{0}{0}}}", INDENT)?;
        writeln!(f, "{}}}", INDENT)?;
        writeln!(f, "}}")?;
        writeln!(f, "")?;

        // Handle Data

        writeln!(f, "pub struct {}Data {{", self.name.1)?;

        if self.dispatchable.is_some() {
            if self.expose_data {
                writeln!(f, "{}pub(crate) handle: {}Raw,", INDENT, self.name.1)?;
                if let Some(parent) = &self.parent {
                    if self.uses_parent {
                        writeln!(f, "{}pub(crate) {}: {}Raw,", INDENT, parent.1.to_snake_case(), parent.1)?;
                    }
                    if self.destructor_uses_grandparent {
                        let parent_handle = spec.handles.iter().find(|h| &h.name.0 == &parent.0).unwrap();
                        let grandparent = parent_handle.parent.as_ref().unwrap();
                        writeln!(f, "{}pub(crate) {}: {}Raw,", INDENT, grandparent.1.to_snake_case(), grandparent.1)?;
                    }
                }
            } else {
                writeln!(f, "{}handle: {}Raw,", INDENT, self.name.1)?;
                if let Some(parent) = &self.parent {
                    if self.uses_parent {
                        writeln!(f, "{}{}: {}Raw,", INDENT, parent.1.to_snake_case(), parent.1)?;
                    }
                    if self.destructor_uses_grandparent {
                        let parent_handle = spec.handles.iter().find(|h| &h.name.0 == &parent.0).unwrap();
                        let grandparent = parent_handle.parent.as_ref().unwrap();
                        writeln!(f, "{}{}: {}Raw,", INDENT, grandparent.1.to_snake_case(), grandparent.1)?;
                    }
                }
            }
        }
        if !self.functions.is_empty() {
            writeln!(f, "{}// Begin function pointers", INDENT)?;
        }
        for (ft, _) in &self.functions {
            // if let Some(platform) = &ft.platform {
            //     writeln!(f, "{}#[cfg(feature = \"platform_{}\")]", INDENT, platform)?;
            // }
            if self.expose_data {
                writeln!(f, "{}pub(crate) {}: {},", INDENT, ft.raw.name.1, ft.raw.type_name)?;
            } else {
                writeln!(f, "{}{}: {},", INDENT, ft.raw.name.1, ft.raw.type_name)?;
            }
        }
        if !self.functions.is_empty() {
            writeln!(f, "{}// End function pointers", INDENT)?;
        }
        if self.loads_fn_ptrs.is_some() {
            writeln!(f, "{}loaded_functions: std::collections::HashSet<String>,", INDENT)?;
        }

        if self.name.1 == "Device" {
            writeln!(f, "{}acquired_queues: std::sync::Mutex<std::collections::HashSet<(u32, u32, DeviceQueueCreateFlags)>>,", INDENT)?;
        }

        if self.expose_data {
            if /*self.destructor_uses_alloc*/ &self.name.1 != "Loader" {
                writeln!(f, "{}pub(crate) allocator: Option<RawAllocationCallbacks>,", INDENT)?;
            }
        } else {
            if /*self.destructor_uses_alloc*/ &self.name.1 != "Loader" {
                writeln!(f, "{}allocator: Option<RawAllocationCallbacks>,", INDENT)?;
            }
        }

        if has_dropper {
            if self.expose_data {
                writeln!(f, "{0}#[cfg(feature=\"automatic_destroy_on_drop\")]", INDENT)?;
                writeln!(f, "{}pub(crate) dropper: DeferAllocArc<{}Dropper>,", INDENT, self.name.1)?;
            } else {
                writeln!(f, "{0}#[cfg(feature=\"automatic_destroy_on_drop\")]", INDENT)?;
                writeln!(f, "{}pub(crate) dropper: DeferAllocArc<{}Dropper>,", INDENT, self.name.1)?;
            }
        }
        writeln!(f, "}}")?;

        writeln!(f, "")?;

        // Dropper

        if has_dropper {
            writeln!(f, "#[cfg(feature=\"automatic_destroy_on_drop\")]")?;
            if self.expose_data {
                write!(f, "pub(crate) ")?;
            }
            writeln!(f, "struct {}Dropper {{", self.name.1)?;

            if let Some(parent) = &self.parent {
                if self.expose_data {
                    if self.uses_parent {
                        writeln!(f, "{}pub(crate) {}: {}Raw,", INDENT, parent.1.to_snake_case(), parent.1)?;
                    }
                    if self.destructor_uses_grandparent {
                        let parent_handle = spec.handles.iter().find(|h| &h.name.0 == &parent.0).unwrap();
                        let grandparent = parent_handle.parent.as_ref().unwrap();
                        writeln!(f, "{}pub(crate) {}: {}Raw,", INDENT, grandparent.1.to_snake_case(), grandparent.1)?;
                    }
                    writeln!(f, "{}pub(crate) parent_dropper: DeferAllocArc<{}Dropper>,", INDENT, parent.1)?;
                } else {
                    if self.uses_parent {
                        writeln!(f, "{}{}: {}Raw,", INDENT, parent.1.to_snake_case(), parent.1)?;
                    }
                    if self.destructor_uses_grandparent {
                        let parent_handle = spec.handles.iter().find(|h| &h.name.0 == &parent.0).unwrap();
                        let grandparent = parent_handle.parent.as_ref().unwrap();
                        writeln!(f, "{}{}: {}Raw,", INDENT, grandparent.1.to_snake_case(), grandparent.1)?;
                    }
                    writeln!(f, "{}parent_dropper: DeferAllocArc<{}Dropper>,", INDENT, parent.1)?;
                }
            }
            for add_par in &self.additional_parents {
                if self.expose_data {
                    writeln!(f, "{}pub(crate) {}_dropper: DeferAllocArc<{}Dropper>,", INDENT, add_par.1.to_snake_case(), add_par.1)?;
                } else {
                    writeln!(f, "{}{}_dropper: DeferAllocArc<{}Dropper>,", INDENT, add_par.1.to_snake_case(), add_par.1)?;
                }
            }

            if let Some(d) = &self.destructor {
                if !self.destructor_uses_slice {
                    if self.expose_data {
                        writeln!(f, "{}pub(crate) handle: {}Raw,", INDENT, self.name.1)?;
                        if self.destructor_uses_alloc {
                            writeln!(f, "{}pub(crate) allocator: Option<RawAllocationCallbacks>,", INDENT)?;
                        }
                        writeln!(f, "{}pub(crate) destructor: {},", INDENT, d.raw.type_name)?;
                    } else {
                        writeln!(f, "{}handle: {}Raw,", INDENT, self.name.1)?;
                        if self.destructor_uses_alloc {
                            writeln!(f, "{}allocator: Option<RawAllocationCallbacks>,", INDENT)?;
                        }
                        writeln!(f, "{}destructor: {},", INDENT, d.raw.type_name)?;
                    }
                }
            }

            writeln!(f, "}}")?;

            writeln!(f, "")?;

            writeln!(f, "#[cfg(all(feature=\"automatic_destroy_on_drop\", feature=\"nullable_functional_handles\"))]")?;
            writeln!(f, "impl {}Dropper {{", self.name.1)?;

            writeln!(f, "{}pub fn null() -> Self {{", INDENT)?;
            writeln!(f, "{0}{0}{1}Dropper {{", INDENT, self.name.1)?;

            if let Some(parent) = &self.parent {
                if self.uses_parent {
                    writeln!(f, "{0}{0}{0}{1}: Handle::null(),", INDENT, parent.1.to_snake_case())?;
                }
                if self.destructor_uses_grandparent {
                    let parent_handle = spec.handles.iter().find(|h| &h.name.0 == &parent.0).unwrap();
                    let grandparent = parent_handle.parent.as_ref().unwrap();
                    writeln!(f, "{0}{0}{0}{1}: Handle::null(),", INDENT, grandparent.1.to_snake_case())?;
                }
                writeln!(f, "{0}{0}{0}parent_dropper: DeferAllocArc::new({1}Dropper::null()),", INDENT, parent.1)?;
            }
            for add_par in &self.additional_parents {
                writeln!(f, "{0}{0}{0}{1}_dropper: DeferAllocArc::new({2}Dropper::null()),", INDENT, add_par.1.to_snake_case(), add_par.1)?;
            }

            if let Some(d) = &self.destructor {
                if !self.destructor_uses_slice {
                    writeln!(f, "{0}{0}{0}handle: Handle::null(),", INDENT)?;
                    if self.destructor_uses_alloc {
                        writeln!(f, "{0}{0}{0}allocator: None,", INDENT)?;
                    }
                    writeln!(f, "{0}{0}{0}destructor: no_op_destructors::{1},", INDENT, d.raw.name.1)?;
                }
            }

            writeln!(f, "{0}{0}}}", INDENT)?;
            writeln!(f, "{}}}", INDENT)?;
            writeln!(f, "}}")?;

            writeln!(f, "")?;

            if let Some(d) = &self.destructor {
                if !self.destructor_uses_slice {
                    // Drop Impl
                    writeln!(f, "#[cfg(feature=\"automatic_destroy_on_drop\")]")?;
                    if self.destructor_uses_alloc {
                        writeln!(f, "impl Drop for {}Dropper {{", self.name.1)?;
                    } else {
                        writeln!(f, "impl Drop for {}Dropper {{", self.name.1)?;
                    }
                    writeln!(f, "{}fn drop(&mut self) {{", INDENT)?;
                    write!(f, "{0}{0}(self.destructor)(", INDENT)?;
                    let mut i = 0;
                    if self.destructor_uses_grandparent {
                        i += 1;
                        write!(f, "self.{}, ", spec.handles.iter().find(|h| &h.name.0 == &self.parent.as_ref().unwrap().0).unwrap().parent.as_ref().unwrap().1.to_snake_case())?;
                    }
                    if self.uses_parent && !self.destructor_exludes_parent {
                        i += 1;
                        write!(f, "self.{}, ", self.parent.as_ref().unwrap().1.to_snake_case())?;
                    }
                    if d.raw.parameters[i].extern_sync {
                        write!(f, "self.handle.to_mut()")?;
                    } else {
                        write!(f, "self.handle")?;
                    }
                    if self.destructor_uses_alloc {
                        writeln!(f, ", self.allocator.as_ref().map(|a| a.as_general()));")?;
                    } else {
                        writeln!(f, ");")?;
                    }
                    writeln!(f, "{}}}", INDENT)?;
                    writeln!(f, "}}")?;

                    writeln!(f, "")?;
                }
            }
        }

        // Impl Data

        writeln!(f, "impl {}Data {{", self.name.1)?;
        if let Some(loads_fn_ptrs) = &self.loads_fn_ptrs {
            if self.expose_data {
                write!(f, "{}pub(crate) fn new(", INDENT)?;
            } else {
                write!(f, "{}fn new(", INDENT)?;
            }
            if self.dispatchable.is_some() {
                write!(f, "handle: {}Raw, ", self.name.1)?;
            }
            if let Some(parent) = &self.parent {
                write!(f, "parent_data: &{}Data, ", parent.1)?;
            }
            for add_par in &self.additional_parents {
                write!(f, "{}_data: &{}Data, ", add_par.1.to_snake_case(), add_par.1)?;
            }
            if /*self.destructor_uses_alloc*/ &self.name.1 != "Loader" {
                write!(f, "allocator: Option<RawAllocationCallbacks>, ")?;
            }
            if self.destructor.is_some() && !self.destructor_uses_slice {
                writeln!(f, "loader: {}, destruct_on_drop: bool) -> Self {{", loads_fn_ptrs)?;
            } else {
                writeln!(f, "loader: {}) -> Self {{", loads_fn_ptrs)?;
            }

            if self.dispatchable.is_some() {
                writeln!(f, "{0}{0}if handle == Handle::null() {{ panic!(\"Can not create managed handle from null raw handle. Check the documentation for this function.\"); }}", INDENT)?;
            }
            writeln!(f, "{0}{0}let mut loaded_functions = std::collections::HashSet::new();", INDENT)?;

            writeln!(f, "{0}{0}{1}Data {{", INDENT, self.name.1)?;

            // Fields
            if self.dispatchable.is_some() {
                writeln!(f, "{0}{0}{0}handle,", INDENT)?;
            }
            if let Some(parent) = &self.parent {
                if self.uses_parent {
                    writeln!(f, "{0}{0}{0}{1}: parent_data.handle,", INDENT, parent.1.to_snake_case())?;
                }
                if self.destructor_uses_grandparent {
                    let parent_handle = spec.handles.iter().find(|h| &h.name.0 == &parent.0).unwrap();
                    let grandparent = parent_handle.parent.as_ref().unwrap();
                    writeln!(f, "{0}{0}{0}{1}: parent_data.{1},", INDENT, grandparent.1.to_snake_case())?;
                }
            }
            for (ft, _) in &self.functions {
                // if let Some(platform) = &ft.platform {
                //     writeln!(f, "{0}{0}{0}#[cfg(feature = \"platform_{1}\")]", INDENT, platform)?;
                // }
                if &ft.raw.type_name == loads_fn_ptrs {
                    writeln!(f, "{0}{0}{0}{1}: loader,", INDENT, ft.raw.name.1)?;
                } else {
                    if self.dispatchable.is_some() {
                        writeln!(f, "{0}{0}{0}{1}: load_fn(loader, handle, UnsizedCStr::from_str_const(\"{2}\\0\")).map_or(invalid_functions::{1}, |f| {{loaded_functions.insert(\"{2}\".to_string()); f}}),", INDENT, ft.raw.name.1, ft.raw.name.0)?;
                    } else {
                        writeln!(f, "{0}{0}{0}{1}: load_fn(loader, InstanceRaw::null(), UnsizedCStr::from_str_const(\"{2}\\0\")).map_or(invalid_functions::{1}, |f| {{loaded_functions.insert(\"{2}\".to_string()); f}}),", INDENT, ft.raw.name.1, ft.raw.name.0)?;
                    }
                }
            }
            if self.loads_fn_ptrs.is_some() {
                writeln!(f, "{0}{0}{0}loaded_functions,", INDENT)?;
            }
            if self.name.1 == "Device" {
                writeln!(f, "{0}{0}{0}acquired_queues: Default::default(),", INDENT)?;
            }
            if /*self.destructor_uses_alloc*/ &self.name.1 != "Loader" {
                writeln!(f, "{0}{0}{0}allocator: allocator.clone(),", INDENT)?;
            }

            if has_dropper {
                writeln!(f, "{0}{0}{0}#[cfg(feature=\"automatic_destroy_on_drop\")]", INDENT)?;
                writeln!(f, "{0}{0}{0}dropper: DeferAllocArc::new({1}Dropper {{", INDENT, self.name.1)?;

                if let Some(parent) = &self.parent {
                    if self.uses_parent {
                        writeln!(f, "{0}{0}{0}{0}{1}: parent_data.handle,", INDENT, parent.1.to_snake_case())?;
                    }
                    if self.destructor_uses_grandparent {
                        let parent_handle = spec.handles.iter().find(|h| &h.name.0 == &parent.0).unwrap();
                        let grandparent = parent_handle.parent.as_ref().unwrap();
                        writeln!(f, "{0}{0}{0}{0}{1}: parent_data.{1},", INDENT, grandparent.1.to_snake_case())?;
                    }
                    writeln!(f, "{0}{0}{0}{0}parent_dropper: parent_data.dropper.clone(),", INDENT)?;
                }
                for add_par in &self.additional_parents {
                    writeln!(f, "{0}{0}{0}{0}{1}_dropper: {1}_data.dropper.clone(), ", INDENT, add_par.1.to_snake_case())?;
                }

                if let Some(d) = &self.destructor {
                    if !self.destructor_uses_slice {
                        writeln!(f, "{0}{0}{0}{0}handle,", INDENT)?;
                        if self.destructor_uses_alloc {
                            writeln!(f, "{0}{0}{0}{0}allocator,", INDENT)?;
                        }
                        if self.dispatchable.is_some() {
                            writeln!(f, "{0}{0}{0}{0}destructor: if destruct_on_drop {{ load_fn(loader, handle, UnsizedCStr::from_str_const(\"{2}\\0\")).unwrap_or(invalid_functions::{1}) }} else {{ no_op_destructors::{1} }},", INDENT, d.raw.name.1, d.raw.name.0)?;
                        } else {
                            writeln!(f, "{0}{0}{0}{0}destructor: if destruct_on_drop {{ load_fn(loader, Handle::null(), UnsizedCStr::from_str_const(\"{2}\\0\")).unwrap_or(invalid_functions::{1}) }} else {{ no_op_destructors::{1} }},", INDENT, d.raw.name.1, d.raw.name.0)?;
                        }
                    }
                }

                writeln!(f, "{0}{0}{0}}}),", INDENT)?;
            }

            writeln!(f, "{0}{0}}}", INDENT)?;
            writeln!(f, "{}}}", INDENT)?;
        } else {
            if self.expose_data {
                write!(f, "{}pub(crate) fn new(", INDENT)?;
            } else {
                write!(f, "{}fn new(", INDENT)?;
            }
            if self.dispatchable.is_some() {
                write!(f, "handle: {}Raw, ", self.name.1)?;
            }
            if let Some(parent) = &self.parent {
                write!(f, "parent_data: &{}Data, ", parent.1)?;
            }
            for add_par in &self.additional_parents {
                write!(f, "{}_data: &{}Data, ", add_par.1.to_snake_case(), add_par.1)?;
            }
            if /*self.destructor_uses_alloc*/ &self.name.1 != "Loader" {
                write!(f, "allocator: Option<RawAllocationCallbacks>, ")?;
            }
            f = f.trim_end_matches(", ").to_string();
            if self.destructor.is_some() && !self.destructor_uses_slice {
                writeln!(f, ", destruct_on_drop: bool) -> Self {{")?;
            } else {
                writeln!(f, ") -> Self {{")?;
            }
            if self.dispatchable.is_some() {
                writeln!(f, "{0}{0}if handle == Handle::null() {{ panic!(\"Can not create managed handle from null raw handle. Check the documentation for this function.\"); }}", INDENT)?;
            }
            writeln!(f, "{0}{0}{1}Data {{", INDENT, self.name.1)?;

            // Fields
            if self.dispatchable.is_some() {
                writeln!(f, "{0}{0}{0}handle,", INDENT)?;
            }
            if let Some(parent) = &self.parent {
                if self.uses_parent {
                    writeln!(f, "{0}{0}{0}{1}: parent_data.handle,", INDENT, parent.1.to_snake_case())?;
                }
                if self.destructor_uses_grandparent {
                    let parent_handle = spec.handles.iter().find(|h| &h.name.0 == &parent.0).unwrap();
                    let grandparent = parent_handle.parent.as_ref().unwrap();
                    writeln!(f, "{0}{0}{0}{1}: parent_data.{1},", INDENT, grandparent.1.to_snake_case())?;
                }
            }
            for (ft, _) in &self.functions {
                // if let Some(platform) = &ft.platform {
                //     writeln!(f, "{0}{0}{0}#[cfg(feature = \"platform_{1}\")]", INDENT, platform)?;
                // }
                writeln!(f, "{0}{0}{0}{1}: parent_data.{1},", INDENT, ft.raw.name.1)?;
            }
            if /*self.destructor_uses_alloc*/ &self.name.1 != "Loader" {
                writeln!(f, "{0}{0}{0}allocator: allocator.clone(),", INDENT)?;
            }

            if has_dropper {
                writeln!(f, "{0}{0}{0}#[cfg(feature=\"automatic_destroy_on_drop\")]", INDENT)?;
                writeln!(f, "{0}{0}{0}dropper: DeferAllocArc::new({1}Dropper {{", INDENT, self.name.1)?;

                if let Some(parent) = &self.parent {
                    if self.uses_parent {
                        writeln!(f, "{0}{0}{0}{0}{1}: parent_data.handle,", INDENT, parent.1.to_snake_case())?;
                    }
                    if self.destructor_uses_grandparent {
                        let parent_handle = spec.handles.iter().find(|h| &h.name.0 == &parent.0).unwrap();
                        let grandparent = parent_handle.parent.as_ref().unwrap();
                        writeln!(f, "{0}{0}{0}{0}{1}: parent_data.{1},", INDENT, grandparent.1.to_snake_case())?;
                    }
                    writeln!(f, "{0}{0}{0}{0}parent_dropper: parent_data.dropper.clone(),", INDENT)?;
                }
                for add_par in &self.additional_parents {
                    writeln!(f, "{0}{0}{0}{0}{1}_dropper: {1}_data.dropper.clone(), ", INDENT, add_par.1.to_snake_case())?;
                }

                if let Some(d) = &self.destructor {
                    if !self.destructor_uses_slice {
                        writeln!(f, "{0}{0}{0}{0}handle,", INDENT)?;
                        if self.destructor_uses_alloc {
                            writeln!(f, "{0}{0}{0}{0}allocator,", INDENT)?;
                        }
                        writeln!(f, "{0}{0}{0}{0}destructor: if destruct_on_drop {{ parent_data.{1} }} else {{ no_op_destructors::{1} }},", INDENT, d.raw.name.1)?;
                    }
                }

                writeln!(f, "{0}{0}{0}}}),", INDENT)?;
            }

            writeln!(f, "{0}{0}}}", INDENT)?;
            writeln!(f, "{}}}", INDENT)?;
        }
        writeln!(f, "")?;
        // null init
        {
            writeln!(f, "{}#[cfg(feature=\"nullable_functional_handles\")]", INDENT)?;

            if self.expose_data {
                writeln!(f, "{}pub(crate) fn null() -> Self {{", INDENT)?;
            } else {
                writeln!(f, "{}fn null() -> Self {{", INDENT)?;
            }

            writeln!(f, "{0}{0}{1}Data {{", INDENT, self.name.1)?;

            // Fields
            if self.dispatchable.is_some() {
                writeln!(f, "{0}{0}{0}handle: Handle::null(),", INDENT)?;
            }
            if let Some(parent) = &self.parent {
                if self.uses_parent {
                    writeln!(f, "{0}{0}{0}{1}: Handle::null(),", INDENT, parent.1.to_snake_case())?;
                }
                if self.destructor_uses_grandparent {
                    let parent_handle = spec.handles.iter().find(|h| &h.name.0 == &parent.0).unwrap();
                    let grandparent = parent_handle.parent.as_ref().unwrap();
                    writeln!(f, "{0}{0}{0}{1}: Handle::null(),", INDENT, grandparent.1.to_snake_case())?;
                }
            }
            for (ft, _) in &self.functions {
                // if let Some(platform) = &ft.platform {
                //     writeln!(f, "{0}{0}{0}#[cfg(feature = \"platform_{1}\")]", INDENT, platform)?;
                // }
                writeln!(f, "{0}{0}{0}{1}: null_functions::{1},", INDENT, ft.raw.name.1)?;
            }
            if self.loads_fn_ptrs.is_some() {
                writeln!(f, "{0}{0}{0}loaded_functions: Default::default(),", INDENT)?;
            }
            if self.name.1 == "Device" {
                writeln!(f, "{0}{0}{0}acquired_queues: Default::default(),", INDENT)?;
            }

            if /*self.destructor_uses_alloc*/ &self.name.1 != "Loader" {
                writeln!(f, "{0}{0}{0}allocator: None,", INDENT)?;
            }
            if has_dropper {
                writeln!(f, "{0}{0}{0}#[cfg(feature=\"automatic_destroy_on_drop\")]", INDENT)?;
                writeln!(f, "{0}{0}{0}dropper: DeferAllocArc::new({1}Dropper::null()),", INDENT, self.name.1)?;
            }

            writeln!(f, "{0}{0}}}", INDENT)?;
            writeln!(f, "{}}}", INDENT)?;
        }
        writeln!(f, "}}")?;

        writeln!(f, "")?;

        Ok((f))
    }

    pub fn format_raw(&self, spec: &Spec) -> Result<String, std::fmt::Error> {
        let mut f = String::new();

        // Handle Raw
        if let Some(dispatchable) = self.dispatchable {
            writeln!(f, "{}single_field_as_ptr_debug_derive!({}Raw, handle);", INDENT, self.name.1)?;
            writeln!(f, "{}#[repr(transparent)]", INDENT)?;
            writeln!(f, "{}#[derive(Copy, Clone, Eq, PartialEq)]", INDENT)?;
            writeln!(f, "{}pub struct {}Raw {{", INDENT, self.name.1)?;
            if dispatchable {
                writeln!(f, "{0}{0}pub(crate) handle: usize,", INDENT)?;
            } else {
                writeln!(f, "{0}{0}pub(crate) handle: u64,", INDENT)?;
            }
            writeln!(f, "{}}}", INDENT)?;

            writeln!(f, "")?;

            writeln!(f, "{0}unsafe impl IsHandle for {1}Raw {{}}", INDENT, self.name.1)?;
            writeln!(f, "")?;

            writeln!(f, "{0}impl {1}Raw {{", INDENT, self.name.1)?;
            writeln!(f, "{0}{0}pub(crate) fn to_mut(self) -> {1}MutRaw {{", INDENT, self.name.1)?;
            writeln!(f, "{0}{0}{0}{1}MutRaw {{ handle: self.handle }}", INDENT, self.name.1)?;
            writeln!(f, "{0}{0}}}", INDENT)?;
            writeln!(f, "{0}}}", INDENT)?;
            writeln!(f, "")?;

            writeln!(f, "{}single_field_as_ptr_debug_derive!({}MutRaw, handle);", INDENT, self.name.1)?;
            writeln!(f, "{}#[repr(transparent)]", INDENT)?;
            writeln!(f, "{}#[derive(Copy, Clone, Eq, PartialEq)]", INDENT)?;
            writeln!(f, "{}pub struct {}MutRaw {{", INDENT, self.name.1)?;
            if dispatchable {
                writeln!(f, "{0}{0}pub(crate) handle: usize,", INDENT)?;
            } else {
                writeln!(f, "{0}{0}pub(crate) handle: u64,", INDENT)?;
            }
            writeln!(f, "{}}}", INDENT)?;

            writeln!(f, "")?;

            writeln!(f, "{0}unsafe impl IsHandle for {1}MutRaw {{}}", INDENT, self.name.1)?;
            writeln!(f, "")?;
        }

        Ok(f)
    }
}

pub(crate) fn format_with_functions(handles: &mut Vec<Handle>, functions: &Vec<Function>) {
    // let mut  functions_clone = functions.clone();
    // let mut functions_clone2 = Vec::new();
    //
    // for h in handles.iter_mut() {
    //     h.functions = functions_clone.into_iter().filter_map(|f| {
    //         if let Some(p) = f.raw.parameters.get(0) {
    //             if &p.type_name.0 == &h.name.0 && &f.raw.type_name != "GetInstanceProcAddr" {
    //                 Some(f)
    //             } else {
    //                 functions_clone2.push(f);
    //                 None
    //             }
    //         } else {
    //             functions_clone2.push(f);
    //             None
    //         }
    //     }).collect();
    //     functions_clone = functions_clone2.clone();
    //     functions_clone2.clear();
    // }
    //
    // for f in functions_clone {
    //     // First is always Loader
    //     handles.first_mut().unwrap().functions.push(f);
    // }

    let (loader_handle, handle_map) = handles.split_first_mut().unwrap();
    let mut handle_map: HashMap<CName, _> = IntoIterator::into_iter(handle_map).map(|h| (h.name.0.clone(), h)).collect();

    let mut add_to_instance = Vec::new();
    let mut add_to_physical_device = Vec::new();
    let mut add_to_device = Vec::new();
    let mut add_to_pool: HashMap<_, Vec<(Function, bool)>> = HashMap::new();

    for f in functions {
        if let Some(p) = f.raw.parameters.get(0) {
            let mut has_device_parent = false;
            let mut has_instance_parent = false;
            if let Some(h) = handle_map.get(&p.type_name.0) {
                if &h.name.0 != "VkInstance" && &h.name.0 != "VkDevice" {
                    let mut p = Some(h.clone());
                    while let Some(ph) = p.take() {
                        if &ph.name.0 == "VkInstance" || ph.additional_parents.iter().any(|ap| &ap.0 == "VkInstance") {
                            has_instance_parent = true;
                            break;
                        } else if &ph.name.0 == "VkDevice" || ph.additional_parents.iter().any(|ap| &ap.0 == "VkDevice") {
                            has_device_parent = true;
                            break;
                        }
                        if let Some(parent) = &ph.parent {
                            if &parent.0 == "VkInstance" {
                                has_instance_parent = true;
                                break;
                            } else if &parent.0 == "VkDevice" {
                                has_device_parent = true;
                                break;
                            }

                            p = handle_map.get(&parent.0).clone()
                        }
                    }
                }
            }
            if handle_map.contains_key(&p.type_name.0) {
                match f.raw.type_name.as_str() {
                    "AllocateCommandBuffers" => {
                        add_to_pool.entry("VkCommandPool").or_default().push((f.clone(), true));
                        let h = handle_map.get_mut(&p.type_name.0).unwrap();
                        h.functions.push((f.clone(), false));
                    }
                    "AllocateDescriptorSets" => {
                        add_to_pool.entry("VkDescriptorPool").or_default().push((f.clone(), true));
                        let h = handle_map.get_mut(&p.type_name.0).unwrap();
                        h.functions.push((f.clone(), false));
                    }
                    "CreateVideoSessionParametersKhr" | "DestroyVideoSessionParametersKhr" => {
                        add_to_pool.entry("VkVideoSessionKHR").or_default().push((f.clone(), true));
                        let h = handle_map.get_mut(&p.type_name.0).unwrap();
                        h.functions.push((f.clone(), false));
                    }
                    _ => {
                        let is_destructor = f.raw.type_name.starts_with("Destroy") || f.raw.type_name.starts_with("Free") || (f.raw.type_name.starts_with("Release") && f.platform.is_none()) && f.raw.parameters.len() > 1;

                        let special_impl = &f.raw.name.1 == "free_command_buffers" || &f.raw.name.1 == "free_descriptor_sets" || &f.raw.name.1 == "get_device_fault_info_ext";

                        let h1_name = handle_map.get(&p.type_name.0).unwrap().name.clone();

                        if f.raw.parameters.get(1).map(|p| {
                            if let Some(h2) = handle_map.get_mut(&p.type_name.0) {
                                if h2.parent.is_some() && &h2.parent.as_ref().unwrap().0 == &h1_name.0 {
                                    Some(h2)
                                } else {
                                    None
                                }
                            } else {
                                None
                            }
                        }).flatten().is_some() {
                            let h = handle_map.get_mut(&f.raw.parameters.get(1).unwrap().type_name.0).unwrap();
                            h.functions.push((f.clone(), !special_impl));

                            let p2_is_optional = f.raw.parameters[1].value_optional;

                            let hp = handle_map.get_mut(&p.type_name.0).unwrap();
                            hp.functions.push((f.clone(), p2_is_optional && !is_destructor));
                        } else {
                            let h = handle_map.get_mut(&p.type_name.0).unwrap();
                            h.functions.push((f.clone(), !special_impl))
                        }
                    }
                }
                if has_device_parent {
                    add_to_device.push((f.clone(), false));
                } else if has_instance_parent {
                    add_to_instance.push((f.clone(), false));
                }
            } else {
                loader_handle.functions.push((f.clone(), true));
            }
            if &f.raw.type_name == "GetInstanceProcAddr" {
                loader_handle.functions.push((f.clone(), true));
            }
            if &f.raw.type_name == "GetDeviceProcAddr" {
                add_to_instance.push((f.clone(), false));
                add_to_physical_device.push((f.clone(), false));
            }
        } else {
            eprintln!("Warning: function with no parameters: {}", f.raw.name.0)
        }
    }

    handle_map.get_mut("VkInstance").unwrap().functions.extend(add_to_instance.into_iter());
    handle_map.get_mut("VkPhysicalDevice").unwrap().functions.extend(add_to_physical_device.into_iter());
    handle_map.get_mut("VkDevice").unwrap().functions.extend(add_to_device.into_iter());

    add_to_pool.into_iter().for_each(|(p, v)| { handle_map.get_mut(p).unwrap().functions.extend(v.into_iter()); });

    for f in functions {
        if f.raw.type_name.starts_with("Destroy") || f.raw.type_name.starts_with("Free") || f.raw.type_name.starts_with("ReleasePerformanceConfigurationINTEL") {
            let mut l = f.raw.parameters.len();
            let mut ends_in_alloc = true;
            if &f.raw.parameters[l - 1].type_name.0 != "VkAllocationCallbacks" {
                l += 1;
                ends_in_alloc = false;
            }

            match l {
                2 => {
                    let h = handle_map.get_mut(&f.raw.parameters[0].type_name.0).expect(&format!("No handle found: {:#?}", f));

                    h.destructor = Some(f.clone());
                    h.destructor_uses_alloc = ends_in_alloc;
                    // h.uses_parent = false;
                }
                3 => {
                    let h = handle_map.get_mut(&f.raw.parameters[1].type_name.0).expect(&format!("No handle found: {:#?}", f));

                    h.destructor = Some(f.clone());
                    h.destructor_uses_alloc = ends_in_alloc;
                    // h.uses_parent = true;
                }
                5 => {
                    let h = handle_map.get_mut(&f.raw.parameters[3].type_name.0).expect(&format!("No handle found: {:#?}", f));

                    h.destructor = Some(f.clone());
                    h.destructor_uses_alloc = false;
                    // h.uses_parent = true;
                    h.destructor_uses_slice = true;
                    h.destructor_uses_grandparent = true;
                }
                n => {
                    eprintln!("Unexpected number of parameters {:?} for: {:?}", n, f)
                }
            }
        }
    }

    let handle_keys = handle_map.keys().cloned().collect::<Vec<_>>();

    for handle_key in handle_keys {
        let this = handle_map.remove(&handle_key).unwrap();

        fn check_grand_parent (h: &&mut Handle, handle_map: &HashMap<CName, &mut Handle>) -> bool {
            h.parent.as_ref().map(|gp| handle_map.get(&gp.0)).flatten().map_or(false, |gp| {
                (gp.destructor.is_some() && !gp.destructor_uses_slice) || check_grand_parent(gp, handle_map)
            })
        };

        if let Some((parent, has_dropper)) = this.parent.as_mut().map(|p| handle_map.get(&p.0).map(|ph| (ph, &mut p.2))).flatten() {
            *has_dropper = *has_dropper || (parent.destructor.is_some() && !parent.destructor_uses_slice) || check_grand_parent(parent, &handle_map)
        }

        for (parent, _, has_dropper) in &mut this.additional_parents {
            let parent = handle_map.get(parent).unwrap();
            *has_dropper = *has_dropper || (parent.destructor.is_some() && !parent.destructor_uses_slice) || check_grand_parent(parent, &handle_map);
        }

        handle_map.insert(handle_key, this);
    }

    loop {
        let mut added = 0;
        let handle_keys = handle_map.keys().cloned().collect::<Vec<_>>();

        for handle_key in handle_keys {
            let this = handle_map.remove(&handle_key).unwrap();

            if this.loads_fn_ptrs.is_some() {
                continue;
            }

            if let Some(parent) = this.parent.as_mut().map(|p| handle_map.get_mut(&p.0)).flatten() {
                // if parent.loads_fn_ptrs.is_some() {
                //     continue;
                // }

                let to_add = this.functions.iter().filter(|f| !parent.functions.iter().any(|pf| &pf.0.raw.name.1 == &f.0.raw.name.1)).map(|f| (f.0.clone(), false)).collect::<Vec<_>>();
                added += to_add.len();
                parent.functions.extend(to_add);
            }

            handle_map.insert(handle_key, this);
        }

        if added == 0 {
            break;
        }
    }
}