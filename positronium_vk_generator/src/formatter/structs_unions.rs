use std::borrow::BorrowMut;
use std::cell::RefCell;
use std::collections::{BTreeMap, BTreeSet, HashMap, HashSet};
use std::fmt::{Display, Formatter, Write};
use std::mem::{ManuallyDrop, MaybeUninit};
use std::ops::{Deref, DerefMut};
use std::rc::Rc;

use heck::{CamelCase, SnakeCase};

use crate::formatter::{CName, EnumValueRef, INDENT, RSName, Spec, trim_vk_prefix};
use crate::formatter::common::{format_parameters, format_parameters_2, GenericParameter, global_conversion, Parameter, Pointer, PointerType};
use crate::formatter::disabled_manager::DisabledManager;
use crate::formatter::function_types::FunctionType;
use crate::formatter::handles::Handle;
use crate::parser::common::ParameterContents;
use crate::parser::Registry;
use crate::parser::types::{Type, TypeCategories};

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Variant {
    Struct,
    Union,
    UnionEnum { selections: Vec<CName>, values: Vec<i64>, selector_enum: String, value_variants: Vec<String>, bit_width: u8 },
}

#[derive(Debug, Clone)]
pub struct ExtendedRef {
    name: (CName, RSName),
    full_name: String,
    generics: Vec<GenericParameter>,
    platform: Option<String>,
    returned_only: bool,
}

#[derive(Debug, Clone)]
pub struct StructUnion {
    pub variant: Variant,
    pub name: (CName, RSName),
    pub members: Vec<Member>,
    pub aliases: Vec<(CName, RSName)>,
    pub platform: Option<String>,
    pub has_inline_mutable: bool,
    pub returned_only: bool,
    pub extends: Vec<(CName, RSName)>,
    pub immediate_extends: bool,
    pub extended_by: BTreeMap<CName, ExtendedRef>,
    pub s_type: Option<String>,
    pub generic_parameters: Vec<GenericParameter>,
    pub needs_ptr_debug_print: bool,
}

#[derive(Debug, Clone)]
pub struct Member {
    values: Vec<String>,
    parameter: Parameter,
    special_exclude: bool,
}

impl Deref for Member {
    type Target = Parameter;

    fn deref(&self) -> &Parameter {
        &self.parameter
    }
}

impl DerefMut for Member {
    fn deref_mut(&mut self) -> &mut Parameter {
        &mut self.parameter
    }
}

pub fn format_structs_unions(registry: &Registry, rs_name_map: &mut HashMap<CName, RSName>, handles: &Vec<Handle>, enum_map: &HashMap<CName, EnumValueRef>, disabled_manager: &DisabledManager, function_types: &Vec<FunctionType>, int_alias_types: &Vec<RSName>, enum_like: &HashSet<RSName>) -> Vec<StructUnion> {
    let handle_map: HashMap<CName, &Handle> = handles.iter().map(|h| (h.name.0.clone(), h)).collect();

    for t in &registry.types {
        match t {
            Type::StructUnion { name, alias, .. } => {
                if alias.is_some() {
                    continue;
                }

                let c_name = name.clone();

                let rs_name = {
                    let rs_name = trim_vk_prefix(&c_name).to_string();
                    if rs_name.starts_with("Raw") {
                        rs_name
                    } else {
                        "Raw".to_string() + &rs_name
                    }
                };

                rs_name_map.insert(c_name, rs_name);
            }
            _ => {}
        }
    }

    for t in &registry.types {
        match t {
            Type::StructUnion { name, alias, .. } => {
                if let Some(alias) = alias {
                    let c_name = name.clone();

                    let rs_name = {
                        let rs_name = rs_name_map.get(alias).expect(&format!("No RS Name for alias: {}", c_name)).clone();
                        if rs_name.starts_with("Raw") {
                            rs_name
                        } else {
                            "Raw".to_string() + &rs_name
                        }
                    };

                    rs_name_map.insert(c_name, rs_name);
                }
            }
            _ => {}
        }
    }

    let mut structs_unions = registry.types.iter().filter_map(|t| {
        match t {
            Type::StructUnion { name, alias, category, returned_only, struct_extends, members, .. } => {
                if alias.is_some() || ["VkBaseInStructure", "VkBaseOutStructure"].contains(&name.as_str()) || disabled_manager.is_disabled(name) {
                    None
                } else {
                    let variant = match category {
                        TypeCategories::Struct => Variant::Struct,
                        TypeCategories::Union => {
                            let selections = members.iter().filter_map(|m| m.selections.get(0).cloned()).collect::<Vec<CName>>();

                            if selections.is_empty() {
                                Variant::Union
                            } else {
                                let values: Vec<_> = selections.iter().enumerate().map(|(i, s)| enum_map.get(s).expect(s).value).collect();
                                let value_variants: Vec<_> = selections.iter().enumerate().map(|(i, s)| enum_map[s].full_variant.clone()).collect();
                                let bit_width = enum_map[&selections[0]].bit_width;
                                let selector_enum = enum_map[&selections[0]].full_variant.split("::").next().unwrap().to_string();

                                Variant::UnionEnum { selections, values, selector_enum, value_variants, bit_width }
                            }
                        }
                        _ => unreachable!()
                    };

                    let c_name = name.clone();
                    let rs_name = {
                        let rs_name = global_conversion(trim_vk_prefix(&c_name));
                        if rs_name.starts_with("Raw") {
                            rs_name
                        } else {
                            "Raw".to_string() + &rs_name
                        }
                    };


                    let has_null_member = ["RawCommandBufferAllocateInfo", "RawDescriptorSetAllocateInfo", "RawSwapchainCreateInfoKHR", "RawVideoSessionParametersCreateInfoKHR"].contains(&rs_name.as_str());

                    let members: Vec<Member> = format_parameters(rs_name.clone(), members.iter().map(|m| &m.parameter), variant != Variant::Struct, false, *returned_only, true, rs_name_map, handles, function_types, int_alias_types, enum_like)
                        .into_iter().zip(members.iter().map(|m| m.values.clone())).map(|(parameter, values)| {
                        if parameter.c_bitfield_bits.is_some() && !["RawAccelerationStructureInstanceKHR", "RawAccelerationStructureSRTMotionInstanceNV", "RawAccelerationStructureMatrixMotionInstanceNV"].contains(&rs_name.as_str()) {
                            eprintln!("WARNING: Bitfields not handled for struct: {}", rs_name);
                        }

                        let special_exclude = has_null_member && ["command_pool", "descriptor_pool", "surface", "video_session"].contains(&parameter.name.1.as_str());

                        Member {
                            values,
                            parameter,
                            special_exclude,
                        }
                    }).collect();

                    let has_inline_mutable = members.iter().any(|m| m.extern_sync || m.pointers.get(0).map_or(false, |p| p.ty == PointerType::Mut));

                    let extends = struct_extends.iter().map(|s| (s.clone(), trim_vk_prefix(s).to_string())).collect::<Vec<_>>();
                    let immediate_extends = !extends.is_empty();

                    let s_type = if let Some(m) = members.first() {
                        if m.name.0 == "sType" {
                            let value = m.values.first().expect("sType must have a value");

                            let value = enum_map.get(value).expect(&format!("The sType is not known: {t:?}"));

                            Some(value.full_variant.clone())
                        } else {
                            None
                        }
                    } else {
                        None
                    };

                    // TODO: If any generic structs where to be contained in another, then need to make sure to propagate up hierarchy, currently though I don't, so I wont.
                    //       It should give an error if ever needed.
                    let mut generic_parameters = members.iter()
                        .flat_map(|m| &m.generic_parameters)
                        .cloned()
                        .fold(BTreeMap::<RSName, HashSet<RSName>>::new(), |mut m, g| {
                            m.entry(g.name).or_default().extend(g.requirements.into_iter());
                            m
                        })
                        .into_iter().map(|(name, r)| GenericParameter { name, requirements: r.into_iter().collect(), default: None })
                        .collect::<Vec<_>>();

                    if let Some(g) = generic_parameters.iter_mut().find(|g| &g.name == "D") {
                        // TODO: Not hardcode?
                        g.default = Some("()".to_string());
                    }

                    let needs_ptr_debug_print = members.iter().any(|f| f.needs_ptr_debug_print);

                    Some((c_name.clone(), StructUnion {
                        variant,
                        name: (c_name, rs_name),
                        members,
                        aliases: Vec::new(),
                        platform: None,
                        has_inline_mutable,
                        returned_only: *returned_only,
                        extends,
                        immediate_extends,
                        extended_by: BTreeMap::new(),
                        s_type,
                        generic_parameters,
                        needs_ptr_debug_print,
                    }))
                }
            }
            _ => None
        }
    }).collect::<BTreeMap<CName, StructUnion>>();

    // Add aliases
    for t in &registry.types {
        match t {
            Type::StructUnion { name, alias, .. } => {
                if !disabled_manager.is_disabled(name)  {
                    if let Some(alias) = alias {
                        if !disabled_manager.is_disabled(alias) {
                            let rs_name = global_conversion(trim_vk_prefix(name));

                            structs_unions.get_mut(alias).unwrap().aliases.push((name.clone(), rs_name))
                        }
                    }
                }
            }
            _ => {}
        }
    }

    // Set platform
    for e in &registry.extensions {
        if !e.supported.iter().any(|s| s == "disabled") {
            for r in &e.require_removes {
                for t in &r.types {
                    if let Some(su) = structs_unions.get_mut(&t.name) {
                        su.platform = e.platform.clone();
                    }
                }
            }
        }
    }

    let keys: Vec<_> = structs_unions.keys().cloned().collect();

    // Mark structs/unions as "has_inline_mutable"
    {
        let mut mutable: HashSet<_> = structs_unions.values().filter_map(|s| if s.has_inline_mutable { Some(s.name.0.clone()) } else { None }).collect();
        let mut to_check: Vec<_> = structs_unions.values().filter(|s| !s.has_inline_mutable).collect();

        let mut previous_to_check = to_check.len();

        loop {
            to_check.retain(|s| {
                let has_inline_mutable = s.members.iter().any(|m| m.pointers.is_empty() && mutable.contains(&m.type_name.0));
                if has_inline_mutable {
                    mutable.insert(s.name.0.clone());
                }
                !has_inline_mutable
            });

            if to_check.len() == previous_to_check {
                break;
            }

            previous_to_check = to_check.len();
        }

        for m in mutable {
            structs_unions.get_mut(&m).unwrap().has_inline_mutable = true;
        }
    }


    for k in keys.clone() {
        let mut struct_union = structs_unions.remove(&k).unwrap();

        format_parameters_2(struct_union.name.1.clone(), struct_union.members.iter_mut().map(|m| &mut m.parameter), &structs_unions, &handles, true, false, struct_union.returned_only);

        // for m in struct_union.members.iter_mut() {
        for index in 0..struct_union.members.len() {
            let (pre, later) = struct_union.members.split_at_mut(index);
            let pre_len = pre.len();
            let (m, later) = later.split_at_mut(1);
            let m = &mut m[0];

            let other_members = pre.iter().chain(later.iter());

            if struct_union.generic_parameters.iter().any(|g| &g.name == "D") && &m.name.0 == "pUserData" {
                m.pointers.clear();
                m.full_type.1 = format!("Option<&'static D>");
                m.type_lifetimes.clear();
                m.handle_lifetimes.clear();
            }
        }

        structs_unions.insert(k, struct_union);
    }

    loop {
        let mut update_count = 0;

        for k in &keys {
            let (this_k, this) = structs_unions.remove_entry(k).unwrap();

            for other in &this.extends {
                let other = if let Some(other) = structs_unions.get_mut(&other.0) {
                    other
                } else {
                    structs_unions.iter_mut().find(|s| s.1.aliases.iter().any(|a| &a.0 == &other.0)).unwrap().1
                };

                let lifetimes: Vec<_> = {
                    let mut hlt: Vec<_> = this.members.iter()
                        .flat_map(|m| m.handle_lifetimes.iter())
                        .map(|l| format!("'{}", l)).collect();
                    hlt.sort();
                    hlt.dedup();

                    hlt
                }.into_iter().chain(this.members.iter()
                    .flat_map(|m|
                        m.pointers.iter()
                            .map(|f| &f.lifetime_name)
                            .flatten()
                            .chain(m.type_lifetimes.iter())
                    )
                    .map(|l| format!("'f_{}", l)))
                    .collect::<Vec<String>>();
                let lifetime_count = lifetimes.len();

                let lifetimes: String = lifetimes.join(", ");

                let full_name = if !lifetimes.is_empty() {
                    format!("{}<{}>", this.name.1, std::iter::repeat("'_").take(lifetime_count).chain(this.generic_parameters.iter().map(|g| g.name.as_str())).collect::<Vec<&str>>().join(", "))
                } else {
                    this.name.1.clone()
                };

                let starting_size = other.extended_by.len();
                other.extended_by.insert(this.name.0.clone(), ExtendedRef {
                    name: this.name.clone(),
                    full_name,
                    generics: this.generic_parameters.clone(),
                    platform: this.platform.clone(),
                    returned_only: this.returned_only,
                });
                other.extended_by.extend(this.extended_by.iter().map(|e| (e.0.clone(), e.1.clone())));

                update_count += other.extended_by.len() - starting_size;
            }

            structs_unions.insert(this_k, this);
        }

        if update_count == 0 {
            break;
        }
    }

    structs_unions.into_iter().map(|(_, v)| v).collect()
}


impl StructUnion {
    pub fn format_raw(&self, spec: &Spec) -> Result<String, std::fmt::Error> {
        let mut f = String::new();

        match self.name.1.as_str() {
            "RawAccelerationStructureInstanceKHR" => {
                writeln!(f, "{}", HARDCODED_ACCELERATION_STRUCTURE_INSTANCE_KHR)?;
                return Ok(f);
            }
            "RawAccelerationStructureSRTMotionInstanceNV" => {
                writeln!(f, "{}", HARDCODED_ACCELERATION_STRUCTURE_SRT_MOTION_INSTANCE_NV)?;
                return Ok(f);
            }
            "RawAccelerationStructureMatrixMotionInstanceNV" => {
                writeln!(f, "{}", HARDCODED_ACCELERATION_STRUCTURE_MATRIX_MOTION_INSTANCE_NV)?;
                return Ok(f);
            }
            "RawDeviceFaultCountsEXT" | "RawDeviceFaultInfoEXT" => {
                return Ok(f);
            }
            _ => {}
        }

        let variant = match self.variant {
            Variant::Struct => "struct",
            Variant::Union | Variant::UnionEnum { .. } => "union"
        };

        let lifetimes_iter: Vec<_> = {
            let mut hlt: Vec<_> = self.members.iter()
                .flat_map(|m| m.handle_lifetimes.iter())
                .map(|l| format!("'{}", l)).collect();
            hlt.sort();
            hlt.dedup();

            hlt
        }.into_iter().chain(self.members.iter()
            .flat_map(|m|
                m.pointers.iter()
                    .map(|f| &f.lifetime_name)
                    .flatten()
                    .chain(m.type_lifetimes.iter())
            )
            .map(|l| format!("'f_{}", l)))
            .collect::<Vec<String>>();
        let lifetime_count = lifetimes_iter.len();


        let to_remove = self.get_to_remove(spec).into_iter().map(|s| format!("'f_{s}")).collect::<HashSet<_>>();

        let norm_lifetimes_iter: Vec<_> = {
            let mut hlt: Vec<_> = self.members.iter()
                .filter(|m| !m.special_exclude)
                .flat_map(|m| m.handle_lifetimes.iter())
                .map(|l| format!("'{}", l)).collect();
            hlt.sort();
            hlt.dedup();

            hlt
        }.into_iter().chain(self.members.iter()
            .filter(|m| !m.special_exclude)
            .flat_map(|m|
                m.pointers.iter()
                    .map(|f| &f.lifetime_name)
                    .flatten()
                    .chain(m.type_lifetimes.iter())
            )
            .map(|l| format!("'f_{}", l)))
            .filter(|l| !to_remove.contains(l))
            .collect::<Vec<String>>();

        let lifetimes: String = lifetimes_iter.iter().map(|s| s.clone()).chain(self.generic_parameters.iter().map(|g| g.name.clone())).collect::<Vec<_>>().join(", ");
        let norm_lifetimes: String = norm_lifetimes_iter.iter().map(|s| s.clone()).chain(self.generic_parameters.iter().map(|g| g.name.clone())).collect::<Vec<_>>().join(", ");

        let lifetimes_with_default: String = lifetimes_iter.iter().map(|s| s.clone()).chain(self.generic_parameters.iter().map(|g| g.name.clone() + &g.default.clone().map_or(String::new(), |d| " = ".to_string() + &d))).collect::<Vec<_>>().join(", ");

        let blank_lifetimes = if !lifetimes.is_empty() {
            format!("<{}>", std::iter::repeat("'_").take(lifetime_count).chain(self.generic_parameters.iter().map(|g| g.name.as_str())).collect::<Vec<&str>>().join(", "))
        } else {
            String::new()
        };

        let mut has_p_next = false;
        let mut has_mut_p_next = false;

        for m in self.members.iter() {
            if &m.name.0 == "pNext" {
                has_p_next = true;
                if m.full_type.1.contains("NextMutPtr") {
                    has_mut_p_next = true;
                }
            }
        }

        // if has_p_next {
        //     if let Some(platform) = &self.platform {
        //         writeln!(f, "#[cfg(feature = \"platform_{}\")]", platform)?;
        //     }
        //     writeln!(f, "pub trait {}NextChain {{}}", self.name.1)?;
        //
        //     if let Some(platform) = &self.platform {
        //         writeln!(f, "#[cfg(feature = \"platform_{}\")]", platform)?;
        //     }
        //     writeln!(f, "impl {}NextChain for DynamicNextChain {{}}\n", self.name.1)?;
        // }

        let mut p_derive = false;
        let mut derives = Vec::new();
        if self.variant == Variant::Struct {
            if self.needs_ptr_debug_print {
                p_derive = true;
            } else {
                derives.push("Debug");
            }
        } else {
            // if let Some(platform) = &self.platform {
            //     writeln!(f, "#[cfg(feature = \"platform_{}\")]", platform)?;
            // }

            writeln!(f, "fieldless_debug_derive!({}{});", self.name.1, blank_lifetimes)?;
        }

        // if let Some(platform) = &self.platform {
        //     writeln!(f, "#[cfg(feature = \"platform_{}\")]", platform)?;
        // }

        let (_immutable_ptr_count, mutable_ptr_count) = self.members.iter().flat_map(|m| &m.pointers).map(|p| p.ty).fold((0, 0), |(immutable, mutable), p| match p {
            PointerType::Const => (immutable + 1, mutable),
            PointerType::Mut => (immutable, mutable + 1)
        });

        let mut needs_try_copy = false;

        // Enforcing Copy because:
        //      If an implementation intends to make calls through a VkAllocationCallbacks structure between
        //      the time a vkCreate* command returns and the time a corresponding vkDestroy* command begins,
        //      that implementation must save a copy of the allocator before the vkCreate* command returns.
        //      The callback functions and any data structures they rely upon must remain valid for the
        //      lifetime of the object they are associated with.
        // Thus things like AllocationCallbacks and DebugUtilsMessengerCreateInfoEXT must impl copy,
        if mutable_ptr_count == 0 && !self.has_inline_mutable && !self.members.iter().any(|m| m.extern_sync || spec.structs_unions.iter().find(|s| &s.name.1 == &m.type_name.1).map_or(false, |s| s.members.iter().any(|om| om.extern_sync))) || self.generic_parameters.iter().any(|g| &g.name == "D") {
            if !has_p_next && !self.members.iter().any(|m|
                    m.pointers.is_empty() && spec.structs_unions.iter()
                        .find(|s| &s.name.1 == &m.type_name.1)
                        .map_or(false, |s|
                            s.members.iter().any(|om| &om.name.1 == "p_next")
                        )
            ) {
                derives.extend_from_slice(&["Copy", "Clone"]);
            } else {
                needs_try_copy = true;
            }
            // TODO: Need work to get working, maybe not worth it
            // if immutable_ptr_count == 0 && self.variant != Variant::Union {
            //     derives.extend_from_slice(&["Eq", "PartialEq"])
            // }
        }

        writeln!(f, "#[repr(C)]")?;

        if !derives.is_empty() {
            writeln!(f, "#[derive({})]", derives.join(", "))?;
        }

        if !lifetimes.is_empty() {
            if self.generic_parameters.iter().map(|g| g.requirements.len()).sum::<usize>() == 0 && !self.generic_parameters.iter().any(|g| g.default.is_some()) {
                writeln!(f, "pub {} {}<{}> {{", variant, self.name.1, lifetimes)?;
            } else {
                let generic_requirements: String = self.generic_parameters.iter()
                    .filter(|g| !g.requirements.is_empty())
                    .map(|g| g.name.clone() + ": " + &g.requirements.join(" + "))
                    .collect::<Vec<_>>()
                    .join(", ");
                writeln!(f, "pub {} {}<{}> where {} {{", variant, self.name.1, lifetimes_with_default, generic_requirements)?;
            }
        } else {
            writeln!(f, "pub {} {} {{", variant, self.name.1)?;
        }

        let has_null_member = ["RawCommandBufferAllocateInfo", "RawDescriptorSetAllocateInfo", "RawSwapchainCreateInfoKHR", "RawVideoSessionParametersCreateInfoKHR"].contains(&self.name.1.as_str());
        let null_member_index = has_null_member.then(|| self.members.iter().enumerate().find(|(_, m)| ["command_pool", "descriptor_pool", "surface", "video_session"].contains(&m.name.1.as_str())).map(|(i, _)| i)).flatten();

        let mut contains_private = false;
        let mut contains_non_s_type_private = false;

        let all_member_p_next = self.variant != Variant::Struct && self.members.iter().all(|m| spec.structs_unions.iter().find(|s| &s.name.1 == &m.type_name.1).map_or(false, |s| s.members.iter().any(|om| &om.name.1 == "p_next")));

        for (index, m) in self.members.iter().enumerate() {
            let contains_array_ptr = self.members.iter().any(|om| om.is_length_for.iter().any(|(i, _, _)| i == &index));

            let publicity = if null_member_index != Some(index) && m.is_length_for.is_empty() && !contains_array_ptr && m.selector.is_none() && m.selector_for_type_and_name.is_none() && &m.name.0 != "sType" && &m.name.0 != "pNext" {
                "pub "
            } else {
                if &m.name.0 != "sType" {
                    contains_non_s_type_private = true;
                }

                contains_private = true;
                "pub(crate) "
            };

            if all_member_p_next {
                writeln!(f, "{}{}{}: core::mem::ManuallyDrop<{}>,", INDENT, publicity, m.name.1, m.full_type.1)?;
            } else {
                writeln!(f, "{}{}{}: {},", INDENT, publicity, m.name.1, m.full_type.1)?;
            }

            if m.pointers.iter().filter(|p| p.len.is_some() && p.len.as_ref().unwrap().1 != "null-terminated").count() > 1 {
                println!("Warning: {}: {}", self.name.1, m.name.1);
            }

            if m.pointers.len() >= 2 && m.pointers[0].len.is_none() && m.pointers[1].len.is_some() {
                println!("Warning 2: {}: {}", self.name.1, m.name.1);
            }
        }

        // if self.returned_only {
        //     writeln!(f, "{}_returned_only: [u8; 0],", INDENT)?;
        // }

        writeln!(f, "}}")?;
        writeln!(f, "")?;

        if p_derive {
            //impl<D> core::fmt::Debug for DebugReportCallbackCreateInfoEXT<'_, '_, D> where D: IsPtr {
            //     fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
            //         f.debug_struct("DebugReportCallbackCreateInfoEXT")
            //             .field("s_type", &self.s_type)
            //             .field("p_next", &self.p_next)
            //             .field("flags", &self.flags)
            //             .field("pfn_callback", &format!("{:p}", unsafe { core::mem::transmute::<_, VoidFunction>(self.pfn_callback) }))
            //             .field("p_user_data", &self.p_user_data)
            //             .finish()
            //     }
            // }

            let generics = self.generic_parameters.iter().map(|g| g.name.as_str()).collect::<Vec<&str>>().join(", ");
            let mut generic_requirements: String = self.generic_parameters.iter()
                .map(|g| {
                    if g.requirements.iter().any(|r| r.as_str() == "core::fmt::Debug") {
                        g.name.clone() + ": " + &g.requirements.join(" + ")
                    } else {
                        g.name.clone() + ": " + &g.requirements.iter().map(|r| r.as_str()).chain(std::iter::once("core::fmt::Debug")).collect::<Vec<&str>>().join(" + ")
                    }
                })
                .collect::<Vec<_>>()
                .join(", ");
            if !generic_requirements.is_empty() {
                generic_requirements = format!(" where {}", generic_requirements);
            }

            if self.generic_parameters.is_empty() {
                writeln!(f, "impl core::fmt::Debug for {}{} {{", self.name.1, blank_lifetimes)?;
            } else {
                writeln!(f, "impl<{}> core::fmt::Debug for {}{}{} {{", generics, self.name.1, blank_lifetimes, generic_requirements)?;
            }

            writeln!(f, "{}fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {{", INDENT)?;

            writeln!(f, "{0}{0}f.debug_struct(\"{1}\")", INDENT, self.name.1)?;

            for m in &self.members {
                if m.needs_ptr_debug_print {
                    writeln!(f, "{0}{0}{0}.field(\"{1}\", &format!(\"{{:p}}\", unsafe {{ core::mem::transmute::<_, VoidFunction>(self.{1}) }}))", INDENT, m.name.1)?;
                } else {
                    writeln!(f, "{0}{0}{0}.field(\"{1}\", &self.{1})", INDENT, m.name.1)?;
                }
            }

            writeln!(f, "{0}{0}{0}.finish()", INDENT)?;

            writeln!(f, "{}}}", INDENT)?;
            writeln!(f, "}}\n")?;
        }

        if let Some(m) = self.members.first() {
            if &m.name.1 == "s_type" {
                let s_type = self.s_type.as_ref().expect("sType must have defined enum value");

                // writeln!(f, "structure_typed_derive!({}{}, {});", self.name.1, blank_lifetimes, s_type)?;

                // if let Some(platform) = &self.platform {
                //     writeln!(f, "#[cfg(feature = \"platform_{}\")]", platform)?;
                // }

                let generics = self.generic_parameters.iter().map(|g| g.name.as_str()).collect::<Vec<&str>>().join(", ");
                let mut generic_requirements: String = self.generic_parameters.iter()
                    .filter(|g| !g.requirements.is_empty())
                    .map(|g| g.name.clone() + ": " + &g.requirements.join(" + "))
                    .collect::<Vec<_>>()
                    .join(", ");
                if !generic_requirements.is_empty() {
                    generic_requirements = format!(" where {}", generic_requirements);
                }

                if self.generic_parameters.is_empty() {
                    writeln!(f, "impl StructureTyped for {}{} {{}}", self.name.1, blank_lifetimes)?;
                } else {
                    writeln!(f, "impl<{}> StructureTyped for {}{}{} {{}}", generics, self.name.1, blank_lifetimes, generic_requirements)?;
                }
                writeln!(f, "")?;

                // if let Some(platform) = &self.platform {
                //     writeln!(f, "#[cfg(feature = \"platform_{}\")]", platform)?;
                // }

                if self.generic_parameters.is_empty() {
                    writeln!(f, "impl Default for SType<{}{}> {{", self.name.1, blank_lifetimes)?;
                } else {
                    writeln!(f, "impl<{}> Default for SType<{}{}>{} {{", generics, self.name.1, blank_lifetimes, generic_requirements)?;
                }

                writeln!(f, "{}fn default() -> Self {{", INDENT)?;
                writeln!(f, "{0}{0}SType {{", INDENT)?;
                writeln!(f, "{0}{0}{0}s_type: {1},", INDENT, s_type)?;
                writeln!(f, "{0}{0}{0}_phantom: Default::default()", INDENT)?;
                writeln!(f, "{0}{0}}}", INDENT)?;
                writeln!(f, "{}}}", INDENT)?;

                writeln!(f, "}}")?;

                writeln!(f)?;
            }
        }

        for a in &self.aliases {
            if !lifetimes.is_empty() {
                writeln!(f, "pub type {0}<{2}> = {1}<{2}>;", a.1, self.name.1, lifetimes)?;
            } else {
                writeln!(f, "pub type {} = {};", a.1, self.name.1)?;
            }
        }
        if !self.aliases.is_empty() {
            writeln!(f, "")?;
        }

        {
            // if let Some(platform) = &self.platform {
            //     writeln!(f, "#[cfg(feature = \"platform_{}\")]", platform)?;
            // }

            let gen = if !self.generic_parameters.is_empty() {
                format!("<{}>", self.generic_parameters.iter().map(|g| g.name.as_str()).collect::<Vec<&str>>().join(", "))
            } else {
                String::new()
            };

            let wh = if self.generic_parameters.iter().map(|g| g.requirements.len()).sum::<usize>() != 0 {
                let generic_requirements: String = self.generic_parameters.iter()
                    .filter(|g| !g.requirements.is_empty())
                    .map(|g| g.name.clone() + ": " + &g.requirements.join(" + "))
                    .collect::<Vec<_>>()
                    .join(", ");
                format!(" where {}", generic_requirements)
            } else {
                String::new()
            };

            if has_p_next {
                if has_mut_p_next || self.returned_only {
                    writeln!(f, "impl{} OutStructure for {}{}{} {{}}\n", gen, self.name.1, blank_lifetimes, wh)?;
                } else {
                    writeln!(f, "impl{} InStructure for {}{}{} {{}}\n", gen, self.name.1, blank_lifetimes, wh)?;
                }
            } else {
                writeln!(f, "impl{} PlainStructure for {}{}{} {{}}\n", gen, self.name.1, blank_lifetimes, wh)?;
            }

            /*if self.returned_only {
                // if let Some(platform) = &self.platform {
                //     writeln!(f, "#[cfg(feature = \"platform_{}\")]", platform)?;
                // }

                writeln!(f, "impl{} {}{}{} {{", gen, self.name.1, blank_lifetimes, wh)?;

                writeln!(f, "{}pub fn new() -> UninitOutStruct<Self> {{", INDENT)?;

                writeln!(f, "{0}{0}UninitOutStruct::<Self>::new()", INDENT)?;

                writeln!(f, "{}}}", INDENT)?;

                writeln!(f, "}}\n")?;
            }*/
        }

        let contains_union = self.members.iter().any(|m| spec.structs_unions.iter().find(|s| s.name.0 == m.type_name.0).map_or(false, |s| s.variant == Variant::Union));
        let needs_constructor = /* && contains_private*/  self.variant == Variant::Struct;
        let needs_setters_getters = contains_non_s_type_private || has_p_next;
        let needs_impl = needs_constructor || needs_setters_getters || (variant != "union" && !contains_union);

        if needs_impl {
            let mut something_printed = false;

            // if let Some(platform) = &self.platform {
            //     writeln!(f, "#[cfg(feature = \"platform_{}\")]", platform)?;
            // }

            if !lifetimes.is_empty() {
                // writeln!(f, "impl<{0}> {1}<{0}> {{", lifetimes, self.name.1)?;

                if self.generic_parameters.iter().map(|g| g.requirements.len()).sum::<usize>() == 0 {
                    // writeln!(f, "pub {} {}<{}> {{", variant, self.name.1, lifetimes)?;
                    writeln!(f, "impl<{0}> {1}<{0}> {{", lifetimes, self.name.1)?;
                } else {
                    let generic_requirements: String = self.generic_parameters.iter()
                        .filter(|g| !g.requirements.is_empty())
                        .map(|g| g.name.clone() + ": " + &g.requirements.join(" + "))
                        .collect::<Vec<_>>()
                        .join(", ");
                    // writeln!(f, "pub {} {}<{}> where {} {{", variant, self.name.1, lifetimes, generic_requirements)?;
                    writeln!(f, "impl<{0}> {1}<{0}> where {2} {{", lifetimes, self.name.1, generic_requirements)?;
                }
            } else {
                writeln!(f, "impl {} {{", self.name.1)?;
            }

            if needs_constructor {
                let mut extra_generic_requirements = vec![];

                let (n, n_pad) = if self.has_inline_p_next(spec) {

                    let mut generics = vec![];
                    let mut i = 0;
                    if has_p_next {
                        generics.insert(i, GenericParameter {
                            name: "N".to_string(),
                            requirements: vec![
                                format!("IntoIterator<Item=&'f_p_next mut dyn {}{}>", self.name.1.trim_start_matches("Raw"), if has_mut_p_next { "MutNext" } else { "Next" }),
                                // "DoubleEndedIterator".to_string(),
                            ],
                            // default: Some(format!("[&'static mut dyn {}{}; 0]", self.name.1.trim_start_matches("Raw"), if has_mut_p_next { "MutNext" } else { "Next" })),
                            default: None,
                        });
                        i += 1;
                        extra_generic_requirements.push(format!("<N as IntoIterator>::IntoIter: DoubleEndedIterator"));
                    }
                    for (j, (j_n, j_n_n)) in self.inline_p_next_names(spec).into_iter().skip(if has_p_next { 1 } else { 0 }).enumerate() {

                        let mut has_mut_p_next = false;
                        for m in spec.structs_unions.iter().find(|os| os.name.1 == j_n_n).unwrap().members.iter() {
                            if &m.name.0 == "pNext" {
                                if m.full_type.1.contains("NextMutPtr") {
                                    has_mut_p_next = true;
                                }
                            }
                        }

                        generics.insert(i, GenericParameter {
                            name: format!("N{j}"),
                            requirements: vec![
                                format!("IntoIterator<Item=&'f_{j_n} mut dyn {}{}>", j_n_n.trim_start_matches("Raw"), if has_mut_p_next { "MutNext" } else { "Next" }),
                                // "DoubleEndedIterator".to_string(),
                            ],
                            // default: Some(format!("[&'static mut dyn {}{}; 0]", j_n_n.trim_start_matches("Raw"), if has_mut_p_next { "MutNext" } else { "Next" })),
                            default: None,
                        });
                        i += 1;
                        extra_generic_requirements.push(format!("<N{j} as IntoIterator>::IntoIter: DoubleEndedIterator"));
                    }

                    let g = generics.into_iter().map(|g| format!("{}: {}", g.name, g.requirements.join(" + "))).collect::<Vec<_>>().join(", ");

                    (format!("pub fn new<{g}>("), format!("{INDENT}{INDENT}"))
                } else {
                    (format!("pub fn new("), " ".repeat("pub fn new(".len()))
                };

                write!(f, "{}{n}", INDENT)?;

                let mut nk = 0;
                let mut first = true;
                for (i, m) in self.members.iter().enumerate() {
                    if (!m.is_length_for.is_empty() && !((self.name.1 == "RawPipelineMultisampleStateCreateInfo" && m.name.1 == "rasterization_samples") || (&self.name.1 == "RawDescriptorSetLayoutBinding" && &m.name.1 == "descriptor_count"))) || &m.name.0 == "sType" || m.selector_for_type_and_name.is_some() {
                        continue;
                    }
                    if null_member_index == Some(i) {
                        continue;
                    }

                    if !first || has_p_next || self.has_inline_p_next(spec) {
                        write!(f, "\n{}{}", INDENT, n_pad)?;
                    } else {
                        first = false;
                    }

                    if &m.name.0 == "pNext" {
                        let l = m.full_type.1.trim_start_matches("Option<").trim_start_matches("NextMutPtr<").trim_start_matches("NextPtr<").trim_end_matches(">");
                        if has_mut_p_next {
                            write!(f, "{0}: N,", m.name.1)?;
                        } else {
                            write!(f, "{0}: N,", m.name.1)?;
                        }

                        continue;
                    }


                    let contains_array_ptr = self.members.iter().any(|om| om.is_length_for.iter().any(|(j, _, _)| j == &i));

                    if contains_array_ptr {
                        if m.slice_type.starts_with("OptionalSliceMut") {
                            write!(f, "mut {}: {},", m.name.1, m.slice_type)?;
                        } else {
                            write!(f, "{}: {},", m.name.1, m.slice_type)?;
                        }
                    } else if let Some(selector) = &m.selector {
                        let lifetimes: String = m.handle_lifetimes.iter().map(|l| format!("'{}", l)).chain(m.type_lifetimes.iter().map(|l| format!("'f_{}", l))).collect::<Vec<_>>()
                            .join(", ");

                        if lifetimes.is_empty() {
                            write!(f, "{}_variant: {},", m.name.1, m.type_name.1.trim_start_matches("Raw"))?;
                        } else {
                            write!(f, "{}_variant: {}<{}>,", m.name.1, m.type_name.1.trim_start_matches("Raw"), lifetimes)?;
                        }
                    } else {
                        let s = m.full_type.1.trim_start_matches("Raw");
                        if s.starts_with("[Raw") {
                            write!(f, "{}: {},", m.name.1, s.replacen("[Raw", "[", 1))?;
                        } else {
                            let mut alt_full_type = s.to_string();
                            if let Some(b) = spec.bit_fields.iter().find(|b| b.enum_name.as_ref().map(|n| n.1.trim_start_matches("Raw")) == Some(s)) {
                                if !b.variants.contains_key(&0) && m.value_optional {
                                    alt_full_type = format!("Option<{}>", alt_full_type);
                                }
                            }

                            let name_suffix = "WithNext";

                            let mut no_lifetime_type = m.full_type.1.as_str();
                            if let Some(i) = no_lifetime_type.find('<') {
                                no_lifetime_type = &no_lifetime_type[..i];
                            }

                            if let Some(s) = spec.structs_unions.iter().find(|s| s.name.1 == no_lifetime_type) {
                                if s.has_inline_p_next(spec) {
                                    if let Some(i) = alt_full_type.find('<') {
                                        alt_full_type.insert_str(i, name_suffix);

                                        for _ in s.inline_p_next_names(spec) {
                                            let j = alt_full_type.find('>').unwrap();
                                            alt_full_type.insert_str(j, &format!(", N{nk}"));
                                            nk += 1;
                                        }
                                    } else {
                                        alt_full_type += name_suffix;
                                    }
                                }
                            }

                            if &alt_full_type == "Bool32" {
                                alt_full_type = "bool".to_string();
                            }

                            write!(f, "{}: {},", m.name.1, alt_full_type)?;
                        }
                    }
                }

                if extra_generic_requirements.is_empty() {
                    writeln!(f, ") -> Self {{")?;
                } else {
                    writeln!(f, ") -> Self where {} {{", extra_generic_requirements.join(", "))?;
                }


                if self.name.1 == "RawAccelerationStructureBuildGeometryInfoKHR" {
                    writeln!(f, "{0}{0}assert!(p_geometries.is_empty() || pp_geometries.is_empty(), \"p_geometries and pp_geometries are mutually exclusive.\");", INDENT)?;
                    writeln!(f, "")?;
                } else if self.name.1 == "RawAccelerationStructureTrianglesOpacityMicromapEXT" || self.name.1 == "RawMicromapBuildInfoEXT" {
                    writeln!(f, "{0}{0}assert!(p_usage_counts.is_empty() || pp_usage_counts.is_empty(), \"p_usage_counts and pp_usage_counts are mutually exclusive.\");", INDENT)?;
                    writeln!(f, "")?;
                } else {
                    for m in self.members.iter() {
                        if m.is_length_for.len() > 1 {
                            let m0_opt = self.members[m.is_length_for[0].0].pointers[0].optional;
                            let i_range = if m0_opt { m.is_length_for.len() } else { 1 };

                            for i in 0..i_range {
                                let ith = m.is_length_for[i].clone();
                                let i_opt = self.members[ith.0].pointers[0].optional;
                                let ith = &self.members[ith.0].name.1;

                                for j in i + 1..m.is_length_for.len() {
                                    let nth = m.is_length_for[j].clone();
                                    let n_opt = self.members[nth.0].pointers[0].optional;
                                    let nth = &self.members[nth.0].name.1;

                                    match (i_opt, n_opt) {
                                        (true, true) => {
                                            writeln!(f, "{0}{0}assert!({1}.is_empty() || {2}.is_empty() || ({1}.len() == {2}.len()), \"{1} and {2} must have the same length, or either be empty.\");", INDENT, ith, nth)?;
                                        }
                                        (true, false) => {
                                            writeln!(f, "{0}{0}assert!({1}.is_empty() || ({1}.len() == {2}.len()), \"{1} and {2} must have the same length, or {1} be empty.\");", INDENT, ith, nth)?;
                                        }
                                        (false, true) => {
                                            writeln!(f, "{0}{0}assert!({2}.is_empty() || ({1}.len() == {2}.len()), \"{1} and {2} must have the same length, or {2} be empty.\");", INDENT, ith, nth)?;
                                        }
                                        (false, false) => {
                                            writeln!(f, "{0}{0}assert_eq!({1}.len(), {2}.len(), \"{1} and {2} must have the same length.\");", INDENT, ith, nth)?;
                                        }
                                    }
                                }
                            }
                            writeln!(f, "")?;
                        }
                    }
                }

                if self.name.1 == "RawPipelineMultisampleStateCreateInfo" {
                    writeln!(f, "{0}{0}assert!(sample_mask.is_empty() || sample_mask.len() == ((u32::from(rasterization_samples) + 31) / 32) as usize, \"The length of sample_mask must be 0 or equal to ceil(rasterization_samples/32), however {{}} != {{}}\", sample_mask.len(), (u32::from(rasterization_samples) + 31) / 32);", INDENT)?;
                    writeln!(f)?;
                }

                if self.name.1 == "RawDescriptorSetLayoutBinding" {
                    writeln!(f, "{0}{0}assert!(immutable_samplers.is_empty() || immutable_samplers.len() == descriptor_count as usize, \"If specifying immutable_samplers, descriptor_count must match it's length. Otherwise it's the normal meaning.\");", INDENT)?;
                    writeln!(f)?;
                }

                if has_p_next {
                    writeln!(f, "{0}{0}let mut result = {1} {{", INDENT, self.name.1)?;
                } else {
                    writeln!(f, "{0}{0}{1} {{", INDENT, self.name.1)?;
                }


                for (i, m) in self.members.iter().enumerate() {
                    let contains_array_ptr = self.members.iter().any(|om| om.is_length_for.iter().any(|(j, _, _)| j == &i));

                    if m.name.0 == "sType" {
                        writeln!(f, "{0}{0}{0}{1}: Default::default(),", INDENT, m.name.1)?;
                    } else if m.name.0 == "pNext" {
                        writeln!(f, "{0}{0}{0}{1}: None,", INDENT, m.name.1)?;
                    } else if !m.is_length_for.is_empty() {
                        let other = &self.members[m.is_length_for[0].0].name.1;

                        match (self.name.0.as_str(), m.name.0.as_str()) {
                            ("VkShaderModuleCreateInfo", "codeSize") => {
                                writeln!(f, "{0}{0}{0}{1}: {2}.len() * 4,", INDENT, m.name.1, other)?;
                            }
                            ("VkPipelineMultisampleStateCreateInfo", "rasterizationSamples") => {
                                writeln!(f, "{0}{0}{0}{1}: {1}.into_raw(),", INDENT, m.name.1)?;
                            }
                            ("VkDescriptorSetLayoutBinding", "descriptorCount") => {
                                writeln!(f, "{0}{0}{0}{1},", INDENT, m.name.1)?;
                            }
                            _ => {
                                if m.is_length_for.len() == 1 {
                                    if &m.full_type.1 != "usize" {
                                        writeln!(f, "{0}{0}{0}{1}: {2}.len() as {3},", INDENT, m.name.1, other, m.full_type.1)?;
                                    } else {
                                        writeln!(f, "{0}{0}{0}{1}: {2}.len(),", INDENT, m.name.1, other)?;
                                    }
                                } else {
                                    let len: String = m.is_length_for.iter().map(|l| format!("{}.len()", self.members[l.0].name.1)).collect::<Vec<String>>().join(", ");

                                    if m.full_type.1.as_str() != "usize" {
                                        writeln!(f, "{0}{0}{0}{1}: IntoIterator::into_iter([{2}]).max().unwrap() as {3},", INDENT, m.name.1, len, m.full_type.1)?;
                                    } else {
                                        writeln!(f, "{0}{0}{0}{1}: IntoIterator::into_iter([{2}]).max().unwrap(),", INDENT, m.name.1, len)?;
                                    }
                                }
                            }
                        }
                    } else if contains_array_ptr {
                        if m.pointers[0].ty == PointerType::Mut || m.full_type.1.contains(" mut ") || m.full_type.1.contains("OptionalSliceMut") {
                            write!(f, "{0}{0}{0}{1}: {1}.first_mut()", INDENT, m.name.1)?;
                        } else {
                            write!(f, "{0}{0}{0}{1}: {1}.first()", INDENT, m.name.1)?;
                        }

                        if m.pointers[0].optional {
                            writeln!(f, ",")?;
                        } else {
                            writeln!(f, ".expect(\"Length of `{}` must be > 0\"),", m.name.1)?;
                        }
                    } else if let Some((_, n)) = &m.selector_for_type_and_name {
                        writeln!(f, "{0}{0}{0}{1}: {2}_variant.get_selector(),", INDENT, m.name.1, n)?;
                    } else if m.selector.is_some() {
                        writeln!(f, "{0}{0}{0}{1}: {1}_variant.get_value(),", INDENT, m.name.1)?;
                    } else if &m.full_type.1 == "Bool32" {
                        writeln!(f, "{0}{0}{0}{1}: {1}.into(),", INDENT, m.name.1)?;
                    } else {
                        if null_member_index == Some(i) {
                            writeln!(f, "{0}{0}{0}{1}: Handle::null(),", INDENT, m.name.1)?;
                        } else {
                            if m.full_type.1.starts_with("Raw") {
                                let mut opt = false;
                                if let Some(b) = spec.bit_fields.iter().find(|b| b.enum_name.as_ref().map(|n| &n.1) == Some(&m.full_type.1)) {
                                    if !b.variants.contains_key(&0) && m.value_optional {
                                        opt = true;
                                    }
                                }

                                if opt {
                                    writeln!(f, "{0}{0}{0}{1}: {1}.map_or({2}(0), |v| v.into_raw()),", INDENT, m.name.1, m.full_type.1)?;
                                } else {
                                    writeln!(f, "{0}{0}{0}{1}: {1}.into_raw(),", INDENT, m.name.1)?;
                                }
                            } else if m.full_type.1.starts_with("[Raw") {
                                writeln!(f, "{0}{0}{0}{1}: {1}.map(|v| v.into_raw()),", INDENT, m.name.1)?;
                            } else {
                                writeln!(f, "{0}{0}{0}{1},", INDENT, m.name.1)?;
                            }
                        }
                    }
                }


                if has_p_next {
                    writeln!(f, "{0}{0}}};", INDENT)?;
                    writeln!(f)?;
                    writeln!(f, "{INDENT}{INDENT}for n in p_next.into_iter().rev() {{")?;
                    writeln!(f, "{INDENT}{INDENT}{INDENT}result.push_next(n.as_base_in());")?;
                    writeln!(f, "{INDENT}{INDENT}}}")?;
                    writeln!(f)?;
                    writeln!(f, "{INDENT}{INDENT}result")?;
                } else {
                    writeln!(f, "{0}{0}}}", INDENT)?;
                }

                writeln!(f, "{}}}", INDENT)?;
                something_printed = true;
            }

            if variant != "union" && !contains_union {
                if something_printed {
                    writeln!(f, "")?;
                }

                let mut needs_mut = "";

                if has_p_next {
                    needs_mut = "mut ";
                } else {
                    for (i, m) in self.members.iter().enumerate() {
                        if (!m.is_length_for.is_empty() && !((self.name.1 == "RawPipelineMultisampleStateCreateInfo" && m.name.1 == "rasterization_samples") || (&self.name.1 == "RawDescriptorSetLayoutBinding" && &m.name.1 == "descriptor_count"))) || &m.name.0 == "sType" || m.selector_for_type_and_name.is_some() {
                            continue;
                        }
                        if null_member_index == Some(i) {
                            continue;
                        }

                        let contains_array_ptr = self.members.iter().any(|om| om.is_length_for.iter().any(|(j, _, _)| j == &i));

                        if contains_array_ptr {
                            if m.slice_type.contains(" mut ") || m.slice_type.contains("OptionalSliceMut") {
                                needs_mut = "mut ";
                                break;
                            }
                        }
                    }
                }

                if norm_lifetimes.is_empty() {
                    writeln!(f, "{0}pub fn normalise({2}self) -> {1} {{", INDENT, self.name.1.trim_start_matches("Raw"), needs_mut)?;
                } else {
                    writeln!(f, "{0}pub fn normalise({3}self) -> {1}<{2}> {{", INDENT, self.name.1.trim_start_matches("Raw"), norm_lifetimes, needs_mut)?;
                }

                for (i, m) in self.members.iter().enumerate() {
                    if (!m.is_length_for.is_empty() && !((self.name.1 == "RawPipelineMultisampleStateCreateInfo" && m.name.1 == "rasterization_samples") || (&self.name.1 == "RawDescriptorSetLayoutBinding" && &m.name.1 == "descriptor_count"))) || &m.name.0 == "sType" || m.selector_for_type_and_name.is_some() {
                        continue;
                    }
                    if null_member_index == Some(i) {
                        continue;
                    }

                    let contains_array_ptr = self.members.iter().any(|om| om.is_length_for.iter().any(|(j, _, _)| j == &i));

                    if contains_array_ptr {
                        if m.slice_type.contains(" mut ") || m.slice_type.contains("OptionalSliceMut") {
                            writeln!(f, "{0}{0}let {1} = self.get_{1}_mut();", INDENT, m.name.1)?;
                        } else {
                            writeln!(f, "{0}{0}let {1} = self.get_{1}();", INDENT, m.name.1)?;
                        }
                    } else if let Some(_) = &m.selector {
                        if m.name.1 == "geometry" {
                            writeln!(f, "{0}{0}let {1}_variant = unsafe {{ core::mem::transmute(self.get_{1}_variant()) }};", INDENT, m.name.1)?;
                        } else {
                            writeln!(f, "{0}{0}let {1}_variant = self.get_{1}_variant();", INDENT, m.name.1)?;
                        }
                    } else {
                        if m.full_type.1.starts_with("Raw") {
                            // writeln!(f, "{0}{0}{0}{1}: self.{1}.normalise(),", INDENT, m.name.1)?;
                        } else {
                            // writeln!(f, "{0}{0}{0}{1}: self.{1},", INDENT, m.name.1)?;
                        }
                    }
                }


                if has_p_next {
                    writeln!(f, "{INDENT}{INDENT}self.recursive_clear_next();")?;
                }

                writeln!(f, "{0}{0}{1} {{", INDENT, self.name.1.trim_start_matches("Raw"))?;

                for (i, m) in self.members.iter().enumerate() {
                    if (!m.is_length_for.is_empty() && !((self.name.1 == "RawPipelineMultisampleStateCreateInfo" && m.name.1 == "rasterization_samples") || (&self.name.1 == "RawDescriptorSetLayoutBinding" && &m.name.1 == "descriptor_count"))) || &m.name.0 == "sType" || m.selector_for_type_and_name.is_some() {
                        continue;
                    }
                    if null_member_index == Some(i) {
                        continue;
                    }

                    if &m.name.0 == "pNext" {
                        continue;
                    }

                    let contains_array_ptr = self.members.iter().any(|om| om.is_length_for.iter().any(|(j, _, _)| j == &i));

                    if contains_array_ptr {
                        if m.slice_type.contains(" mut ") || m.slice_type.contains("OptionalSliceMut") {
                            writeln!(f, "{0}{0}{0}{1},", INDENT, m.name.1)?;
                        } else {
                            writeln!(f, "{0}{0}{0}{1},", INDENT, m.name.1)?;
                        }
                    } else if let Some(_) = &m.selector {
                        writeln!(f, "{0}{0}{0}{1}_variant,", INDENT, m.name.1)?;
                    } else {
                        if m.full_type.1.starts_with("Raw") {
                            let mut opt = false;
                            if let Some(b) = spec.bit_fields.iter().find(|b| b.enum_name.as_ref().map(|n| &n.1) == Some(&m.full_type.1)) {
                                if !b.variants.contains_key(&0) && m.value_optional {
                                    opt = true;
                                }
                            }

                            if opt {
                                writeln!(f, "{0}{0}{0}{1}: self.{1}.normalise_optional(),", INDENT, m.name.1)?;
                            } else {
                                writeln!(f, "{0}{0}{0}{1}: self.{1}.normalise(),", INDENT, m.name.1)?;
                            }
                        } else if m.full_type.1.starts_with("[Raw") {
                            writeln!(f, "{0}{0}{0}{1}: self.{1}.map(|v| v.normalise()),", INDENT, m.name.1)?;
                        } else if m.full_type.1 == "Bool32" {
                            writeln!(f, "{0}{0}{0}{1}: self.{1}.into(),", INDENT, m.name.1)?;
                        } else {
                            writeln!(f, "{0}{0}{0}{1}: self.{1},", INDENT, m.name.1)?;
                        }
                    }
                }

                writeln!(f, "{0}{0}}}", INDENT)?;
                writeln!(f, "{0}}}", INDENT)?;

                something_printed = true;
            }

            // /// Acquire a copy if p_next is null
            //         pub fn try_copy(&self) -> Option<Self> {
            //             if self.p_next.is_some() {
            //                 return None;
            //             }
            //
            //             Some(Self {
            //                 s_type: self.s_type,
            //                 p_next: None,
            //                 application_name: self.application_name,
            //                 application_version: self.application_version,
            //                 engine_name: self.engine_name,
            //                 engine_version: self.engine_version,
            //                 api_version: self.api_version,
            //             })
            //         }
            if needs_try_copy && &self.name.1 != "RawAccelerationStructureGeometryKHR" {
                if something_printed {
                    writeln!(f, "")?;
                }


                writeln!(f, "{INDENT}/// Acquire a copy if p_next is null")?;
                writeln!(f, "{INDENT}pub fn try_copy(&self) -> Option<Self> {{")?;
                if has_p_next {
                    writeln!(f, "{INDENT}{INDENT}if self.p_next.is_some() {{")?;
                    writeln!(f, "{INDENT}{INDENT}{INDENT}return None;")?;
                    writeln!(f, "{INDENT}{INDENT}}}")?;
                    writeln!(f, "")?;
                }
                writeln!(f, "{INDENT}{INDENT}Some(Self {{")?;
                for (i, m) in self.members.iter().enumerate() {
                    // if (!m.is_length_for.is_empty() && !((self.name.1 == "RawPipelineMultisampleStateCreateInfo" && m.name.1 == "rasterization_samples") || (&self.name.1 == "RawDescriptorSetLayoutBinding" && &m.name.1 == "descriptor_count"))) || &m.name.0 == "sType" || m.selector_for_type_and_name.is_some() {
                    //     continue;
                    // }
                    // if null_member_index == Some(i) {
                    //     continue;
                    // }

                    if spec.structs_unions.iter().find(|os| os.name.0 == m.full_type.0).map_or(false, |os| {
                        let mut has_p_next = false;
                        let mut has_mut_p_next = false;

                        for m in os.members.iter() {
                            if &m.name.0 == "pNext" {
                                has_p_next = true;
                                if m.full_type.1.contains("NextMutPtr") {
                                    has_mut_p_next = true;
                                }
                            }
                        }

                        let (_immutable_ptr_count, mutable_ptr_count) = os.members.iter().flat_map(|m| &m.pointers).map(|p| p.ty).fold((0, 0), |(immutable, mutable), p| match p {
                            PointerType::Const => (immutable + 1, mutable),
                            PointerType::Mut => (immutable, mutable + 1)
                        });

                        let mut needs_try_copy = false;

                        if mutable_ptr_count == 0 && !os.has_inline_mutable && !os.members.iter().any(|m| m.extern_sync || spec.structs_unions.iter().find(|s| &s.name.1 == &m.type_name.1).map_or(false, |s| s.members.iter().any(|om| om.extern_sync))) || os.generic_parameters.iter().any(|g| &g.name == "D") {
                            if has_p_next || os.members.iter().any(|m| spec.structs_unions.iter().find(|s| &s.name.1 == &m.type_name.1).map_or(false, |s| s.members.iter().any(|om| &om.name.1 == "p_next"))) {
                                needs_try_copy = true;
                            }
                        }

                        needs_try_copy && os.variant == Variant::Struct
                    }) {
                        writeln!(f, "{INDENT}{INDENT}{INDENT}{0}: self.{0}.try_copy()?,", m.name.1)?;
                    } else if &m.name.1 == "p_next" {
                        writeln!(f, "{INDENT}{INDENT}{INDENT}{0}: None,", m.name.1)?;
                    } else {
                        writeln!(f, "{INDENT}{INDENT}{INDENT}{0}: self.{0},", m.name.1)?;
                    }
                }
                writeln!(f, "{INDENT}{INDENT}}})")?;
                writeln!(f, "{INDENT}}}")?;

                something_printed = true;
            }

            if needs_setters_getters {
                let mut shared_length_arrays = HashSet::<CName>::new();
                let mut array_length_type = HashMap::<CName, RSName>::new();

                for (i, m) in self.members.iter().enumerate() {
                    for l in &m.is_length_for {
                        array_length_type.insert(self.members[l.0].name.0.clone(), m.full_type.1.clone());
                    }

                    if m.is_length_for.len() > 1 {
                        for l in &m.is_length_for {
                            shared_length_arrays.insert(self.members[l.0].name.0.clone());
                        }
                    }
                }

                if has_p_next {
                    if something_printed {
                        writeln!(f, "")?;
                    }
                    if has_mut_p_next {
                        writeln!(f, "{0}pub unsafe fn set_p_next(&mut self, p_next: Option<NextMutPtr<'f_p_next>>) {{", INDENT)?;
                    } else {
                        writeln!(f, "{0}pub unsafe fn set_p_next(&mut self, p_next: Option<NextPtr<'f_p_next>>) {{", INDENT)?;
                    }
                    writeln!(f, "{0}{0}self.p_next = p_next;", INDENT)?;
                    writeln!(f, "{0}}}", INDENT)?;

                    writeln!(f, "")?;

                    if has_mut_p_next {
                        writeln!(f, "{0}pub fn take_p_next(&mut self) -> Option<NextMutPtr<'f_p_next>> {{", INDENT)?;
                    } else {
                        writeln!(f, "{0}pub fn take_p_next(&mut self) -> Option<NextPtr<'f_p_next>> {{", INDENT)?;
                    }
                    writeln!(f, "{0}{0}self.p_next.take()", INDENT)?;
                    writeln!(f, "{0}}}", INDENT)?;

                    something_printed = true;
                }

                for (i, m) in self.members.iter().enumerate() {
                    let contains_array_ptr = self.members.iter().any(|om| om.is_length_for.iter().any(|(j, _, _)| j == &i));

                    if m.name.0 == "sType" {
                        continue;
                    } else if !m.is_length_for.is_empty() {
                        continue;
                    } else if contains_array_ptr {
                        if something_printed {
                            writeln!(f, "")?;
                        }

                        // let

                        // Setter

                        if !shared_length_arrays.contains(&m.name.0) {
                            match (self.name.0.as_str(), m.name.0.as_str()) {
                                ("VkPipelineMultisampleStateCreateInfo", "pSampleMask") => {
                                    writeln!(f, "{0}pub fn set_{1}(&mut self, {1}: {2}, rasterization_samples: SampleCountFlagBits) {{", INDENT, m.name.1, m.slice_type)?;

                                    writeln!(f, "{0}{0}assert!(sample_mask.is_empty() || sample_mask.len() == ((u32::from(rasterization_samples) + 31) / 32) as usize, \"The length of sample_mask must be 0 or equal to ceil(rasterization_samples/32), however {{}} != {{}}\", sample_mask.len(), (u32::from(rasterization_samples) + 31) / 32);", INDENT)?;
                                    writeln!(f)?;
                                }
                                _ => if m.slice_type.starts_with("OptionalSliceMut") {
                                    writeln!(f, "{0}pub fn set_{1}(&mut self, mut {1}: {2}) {{", INDENT, m.name.1, m.slice_type)?
                                } else {
                                    writeln!(f, "{0}pub fn set_{1}(&mut self, {1}: {2}) {{", INDENT, m.name.1, m.slice_type)?
                                }
                            }

                            match (self.name.0.as_str(), m.name.0.as_str()) {
                                ("VkShaderModuleCreateInfo", "pCode") => {
                                    let stripped_len = m.pointers[0].len.as_ref().unwrap().1.trim_end_matches("/ 4").trim();

                                    writeln!(f, "{0}{0}self.{1} = {2}.len() * 4;", INDENT, stripped_len, &m.name.1)?;
                                }
                                ("VkPipelineMultisampleStateCreateInfo", "pSampleMask") => {
                                    writeln!(f, "{0}{0}self.rasterization_samples = rasterization_samples.into_raw();", INDENT)?;
                                }
                                _ => {
                                    if array_length_type[&m.name.0].as_str() != "usize" {
                                        writeln!(f, "{0}{0}self.{1} = {2}.len() as {3};", INDENT, m.pointers[0].len.as_ref().unwrap().1, &m.name.1, array_length_type[&m.name.0])?;
                                    } else {
                                        writeln!(f, "{0}{0}self.{1} = {2}.len();", INDENT, m.pointers[0].len.as_ref().unwrap().1, &m.name.1)?;
                                    }
                                }
                            }


                            if m.pointers[0].ty == PointerType::Mut || m.full_type.1.contains(" mut ") || m.full_type.1.contains("OptionalSliceMut") {
                                write!(f, "{0}{0}self.{1} = {1}.first_mut()", INDENT, m.name.1)?;
                            } else {
                                write!(f, "{0}{0}self.{1} = {1}.first()", INDENT, m.name.1)?;
                            }

                            if m.pointers[0].optional {
                                writeln!(f, ";")?;
                            } else {
                                writeln!(f, ".expect(\"Length of `{0}` must be > 0\");", m.name.1)?;
                            }

                            writeln!(f, "{}}}", INDENT)?;
                            writeln!(f, "")?;
                        }
                        //


                        // Getter

                        if m.pointers[0].ty == PointerType::Mut || m.full_type.1.contains(" mut ") || m.full_type.1.contains("OptionalSliceMut") {
                            writeln!(f, "{0}pub fn get_{1}(&self) -> {2} {{", INDENT, m.name.1, m.slice_type.replace(" mut", "").replacen("OptionalSliceMut", "OptionalSlice", 1))?;
                        } else {
                            writeln!(f, "{0}pub fn get_{1}(&self) -> {2} {{", INDENT, m.name.1, m.slice_type)?;
                        }

                        let cast = |input: String, name: String, len: String| {
                            if m.pointers[0].optional {
                                if self.returned_only {
                                    if m.pointers[0].ty == PointerType::Mut || m.full_type.1.contains(" mut ") || m.full_type.1.contains("OptionalSliceMut") {
                                        format!("self.{0}.as_ref().map(|p| *p as *const _).map_or(OptionalSlice::from_len({len}), |{0}| OptionalSlice::Slice({1}) )", name, input)
                                    } else {
                                        format!("self.{0}.map(|p| p as *const _).map_or(OptionalSlice::from_len({len}), |{0}| OptionalSlice::Slice({1}) )", name, input)
                                    }
                                } else {
                                    if m.pointers[0].ty == PointerType::Mut || m.full_type.1.contains(" mut ") {
                                        format!("self.{0}.as_ref().map(|p| *p as *const _).map_or(&[][..], |{0}| {1} )", name, input)
                                    } else {
                                        format!("self.{0}.map(|p| p as *const _).map_or(&[][..], |{0}| {1} )", name, input)
                                    }
                                }
                            } else {
                                format!("{{ let {0} = self.{0} as *const _; {1} }}", name, input)
                            }
                        };

                        // let cast = if m.pointers[0].optional {
                        //     if m.pointers[0].ty == PointerType::Mut {
                        //         ".as_ref().map_or(std::ptr::null(), |p| *p as *const _)"
                        //     } else {
                        //         ".map_or(std::ptr::null(), |p| p as *const _)"
                        //     }
                        // } else {
                        //     " as *const _"
                        // };

                        if array_length_type[&m.name.0].as_str() != "usize" {
                            match (self.name.0.as_str(), m.name.0.as_str()) {
                                // ("VkPipelineMultisampleStateCreateInfo", "pSampleMask") => writeln!(f, "{0}{0}unsafe {{ std::slice::from_raw_parts(self.{1}{2}, (self.rasterization_samples as usize + 31) / 32) }}", INDENT, m.name.1, cast)?,
                                ("VkPipelineMultisampleStateCreateInfo", "pSampleMask") => writeln!(f, "{0}{0}{1}", INDENT, cast(format!("unsafe {{ std::slice::from_raw_parts({0}, (self.rasterization_samples.0 as usize + 31) / 32) }}", m.name.1), m.name.1.clone(), "(self.rasterization_samples.0 as usize + 31) / 32".to_string()))?,
                                // _ => writeln!(f, "{0}{0}unsafe {{ std::slice::from_raw_parts(self.{1}{3}, self.{2} as usize) }}", INDENT, m.name.1, m.pointers[0].len.as_ref().unwrap().1, cast)?
                                _ => writeln!(f, "{0}{0}{1}", INDENT, cast(format!("unsafe {{ std::slice::from_raw_parts({0}, self.{1} as usize) }}", m.name.1, m.pointers[0].len.as_ref().unwrap().1), m.name.1.clone(), format!("self.{} as usize", m.pointers[0].len.as_ref().unwrap().1)))?,
                            }
                        } else {
                            // writeln!(f, "{0}{0}unsafe {{ std::slice::from_raw_parts(self.{1}{3}, self.{2}) }}", INDENT, m.name.1, m.pointers[0].len.as_ref().unwrap().1, cast)?;
                            writeln!(f, "{0}{0}{1}", INDENT, cast(format!("unsafe {{ std::slice::from_raw_parts({0}, self.{1}) }}", m.name.1, m.pointers[0].len.as_ref().unwrap().1), m.name.1.clone(), format!("self.{}", m.pointers[0].len.as_ref().unwrap().1)))?;
                        }

                        writeln!(f, "{}}}", INDENT)?;

                        if m.pointers[0].ty == PointerType::Mut || m.full_type.1.contains(" mut ") || m.full_type.1.contains("OptionalSliceMut") {
                            writeln!(f, "")?;

                            writeln!(f, "{0}pub fn get_{1}_mut(&mut self) -> {2} {{", INDENT, m.name.1, m.slice_type)?;

                            let cast = |input: String, name: String, len: String| {
                                if self.returned_only {
                                    if m.pointers[0].optional {
                                        format!("self.{0}.as_mut().map(|p| *p as *mut _).map_or(OptionalSliceMut::from_len({len}), |{0}| OptionalSliceMut::Slice({1}) )", name, input)
                                    } else {
                                        format!("{{ let {0} = self.{0} as *mut _; {1} }}", name, input)
                                    }
                                } else {
                                    if m.pointers[0].optional {
                                        format!("self.{0}.as_mut().map(|p| *p as *mut _).map_or(&mut [][..], |{0}| {1} )", name, input)
                                    } else {
                                        format!("{{ let {0} = self.{0} as *mut _; {1} }}", name, input)
                                    }
                                }
                            };

                            // let cast = if m.pointers[0].optional {
                            //     ".as_mut().map_or(std::ptr::null_mut(), |p| *p as *mut _)"
                            // } else {
                            //     " as *mut _"
                            // };

                            if array_length_type[&m.name.0].as_str() != "usize" {
                                // writeln!(f, "{0}{0}unsafe {{ std::slice::from_raw_parts_mut(self.{1}{3}, self.{2} as usize) }}", INDENT, m.name.1, m.pointers[0].len.as_ref().unwrap().1, cast)?;
                                writeln!(f, "{0}{0}{1}", INDENT, cast(format!("unsafe {{ std::slice::from_raw_parts_mut({0}, self.{1} as usize) }}", m.name.1, m.pointers[0].len.as_ref().unwrap().1), m.name.1.clone(), format!("self.{} as usize", m.pointers[0].len.as_ref().unwrap().1)))?;
                            } else {
                                // writeln!(f, "{0}{0}unsafe {{ std::slice::from_raw_parts_mut(self.{1}{3}, self.{2}) }}", INDENT, m.name.1, m.pointers[0].len.as_ref().unwrap().1, cast)?;
                                writeln!(f, "{0}{0}{1}", INDENT, cast(format!("unsafe {{ std::slice::from_raw_parts_mut({0}, self.{1}) }}", m.name.1, m.pointers[0].len.as_ref().unwrap().1), m.name.1.clone(), format!("self.{}", m.pointers[0].len.as_ref().unwrap().1)))?;
                            }

                            writeln!(f, "{}}}", INDENT)?;
                        }

                        // if m.pointers[0].ty == PointerType::Mut {
                        //     write!(f, "{0}{0}{0}{1}: {1}.first_mut()", INDENT, m.name.1)?;
                        // } else {
                        //     write!(f, "{0}{0}{0}{1}: {1}.first()", INDENT, m.name.1)?;
                        // }
                        //
                        // if m.pointers[0].optional {
                        //     writeln!(f, ",")?;
                        // } else {
                        //     writeln!(f, ".expect(\"Length of `{}` must be > 0\"),", m.name.1)?;
                        // }

                        something_printed = true;
                    } else if m.is_length_for.len() > 0 {
                        // if i != 0 || !needs_constructor {
                        //     writeln!(f, "")?;
                        // }

                        // Length getter

                        if m.is_length_for.len() > 1 {
                            // Group Setter
                        }
                    } else if let Some((_, n)) = &m.selector_for_type_and_name {
                        continue;
                    } else if let Some(selector) = &m.selector {
                        if something_printed {
                            writeln!(f, "")?;
                        }

                        let lifetimes: String = m.handle_lifetimes.iter().map(|l| format!("'{}", l)).chain(m.type_lifetimes.iter().map(|l| format!("'f_{}", l))).collect::<Vec<_>>()
                            .join(", ");

                        let formatted_selector_name = {
                            let formatted_selector_name = selector.to_snake_case();
                            // let formatted_selector_name = formatted_selector_name
                            // .trim_start_matches("s_")
                            // .trim_start_matches("p_")
                            // .trim_start_matches("pp_");

                            if formatted_selector_name == "type" {
                                "e_type".to_string() // TODO: Is this best?
                            } else {
                                formatted_selector_name
                            }
                        };

                        // Setter
                        if lifetimes.is_empty() {
                            writeln!(f, "{0}pub fn set_{1}_variant(&mut self, {1}_variant: {2}) {{", INDENT, m.name.1, m.type_name.1.trim_start_matches("Raw"))?;
                        } else {
                            writeln!(f, "{0}pub fn set_{1}_variant(&mut self, {1}_variant: {2}<{3}>) {{", INDENT, m.name.1, m.type_name.1.trim_start_matches("Raw"), lifetimes)?;
                        }
                        writeln!(f, "{0}{0}self.{2} = {1}_variant.get_selector();", INDENT, m.name.1, formatted_selector_name)?;
                        writeln!(f, "{0}{0}self.{1} = {1}_variant.get_value();", INDENT, m.name.1)?;
                        writeln!(f, "{}}}", INDENT)?;
                        //

                        writeln!(f, "")?;

                        // Getter
                        if m.name.1 == "geometry" {
                            writeln!(f, "{0}pub unsafe fn get_{1}_variant(&self) -> {2} {{", INDENT, m.name.1, m.type_name.1.trim_start_matches("Raw"))?;
                        } else {
                            if lifetimes.is_empty() {
                                writeln!(f, "{0}pub fn get_{1}_variant(&self) -> {2} {{", INDENT, m.name.1, m.type_name.1.trim_start_matches("Raw"))?;
                            } else {
                                writeln!(f, "{0}pub fn get_{1}_variant(&self) -> {2}<{3}> {{", INDENT, m.name.1, m.type_name.1.trim_start_matches("Raw"), lifetimes)?;
                            }
                        }
                        if m.name.1 == "geometry" {
                            writeln!(f, "{0}{0}{1}::new(self.{2}, unsafe {{ core::mem::transmute_copy(&self.{3}) }})", INDENT, m.type_name.1.trim_start_matches("Raw"), formatted_selector_name, m.name.1)?;
                        } else {
                            writeln!(f, "{0}{0}{1}::new(self.{2}, self.{3})", INDENT, m.type_name.1.trim_start_matches("Raw"), formatted_selector_name, m.name.1)?;
                        }
                        writeln!(f, "{0}}}", INDENT)?;

                        something_printed = true;
                    } else {
                        continue;
                    }
                }

                // Multi setter

                for m in self.members.iter() {
                    if m.is_length_for.len() > 1 {
                        let name: String = m.is_length_for.iter().map(|l| self.members[l.0].name.1.as_str()).collect::<Vec<&str>>().join("_and_");
                        let in_var: String = m.is_length_for.iter().map(|l| if self.members[l.0].slice_type.starts_with("OptionalSliceMut") {
                            format!("mut {}: {}", self.members[l.0].name.1, self.members[l.0].slice_type)
                        } else {
                            format!("{}: {}", self.members[l.0].name.1, self.members[l.0].slice_type)
                        }).collect::<Vec<String>>().join(", ");

                        writeln!(f, "")?;
                        writeln!(f, "{0}pub fn set_{1}(&mut self, {2}) {{", INDENT, name, in_var)?;

                        if self.name.1 == "RawAccelerationStructureBuildGeometryInfoKHR" {
                            writeln!(f, "{0}{0}assert!(p_geometries.is_empty() || pp_geometries.is_empty(), \"p_geometries and pp_geometries are mutually exclusive.\");", INDENT)?;
                        } else if self.name.1 == "RawAccelerationStructureTrianglesOpacityMicromapEXT" || self.name.1 == "RawMicromapBuildInfoEXT" {
                            writeln!(f, "{0}{0}assert!(p_usage_counts.is_empty() || pp_usage_counts.is_empty(), \"p_usage_counts and pp_usage_counts are mutually exclusive.\");", INDENT)?;
                            writeln!(f, "")?;
                        } else {
                            let m0_opt = self.members[m.is_length_for[0].0].pointers[0].optional;
                            let i_range = if m0_opt { m.is_length_for.len() } else { 1 };

                            for i in 0..i_range {
                                let ith = m.is_length_for[i].clone();
                                let i_opt = self.members[ith.0].pointers[0].optional;
                                let ith = &self.members[ith.0].name.1;

                                for j in i + 1..m.is_length_for.len() {
                                    let nth = m.is_length_for[j].clone();
                                    let n_opt = self.members[nth.0].pointers[0].optional;
                                    let nth = &self.members[nth.0].name.1;

                                    match (i_opt, n_opt) {
                                        (true, true) => {
                                            writeln!(f, "{0}{0}assert!({1}.is_empty() || {2}.is_empty() || ({1}.len() == {2}.len()), \"{1} and {2} must have the same length, or either be empty.\");", INDENT, ith, nth)?;
                                        }
                                        (true, false) => {
                                            writeln!(f, "{0}{0}assert!({1}.is_empty() || ({1}.len() == {2}.len()), \"{1} and {2} must have the same length, or {1} be empty.\");", INDENT, ith, nth)?;
                                        }
                                        (false, true) => {
                                            writeln!(f, "{0}{0}assert!({2}.is_empty() || ({1}.len() == {2}.len()), \"{1} and {2} must have the same length, or {2} be empty.\");", INDENT, ith, nth)?;
                                        }
                                        (false, false) => {
                                            writeln!(f, "{0}{0}assert_eq!({1}.len(), {2}.len(), \"{1} and {2} must have the same length.\");", INDENT, ith, nth)?;
                                        }
                                    }
                                }
                            }
                        }

                        if m.value_optional {
                            if let Some(i) = m.is_length_for.iter().find(|l| !self.members[l.0].pointers[0].optional) {
                                writeln!(f, "{0}{0}assert!({1}.len() > 0, \"{2} length must be > 0.\");", INDENT, self.members[i.0].name.1, m.name.1)?;
                            }
                        }

                        writeln!(f, "")?;

                        let len: String = m.is_length_for.iter().map(|l| format!("{}.len()", self.members[l.0].name.1)).collect::<Vec<String>>().join(", ");

                        if m.full_type.1.as_str() != "usize" {
                            writeln!(f, "{0}{0}self.{1} = IntoIterator::into_iter([{2}]).max().unwrap() as {3};", INDENT, m.name.1, len, m.full_type.1)?;
                        } else {
                            writeln!(f, "{0}{0}self.{1} = IntoIterator::into_iter([{2}]).max().unwrap();", INDENT, m.name.1, len)?;
                        }

                        writeln!(f, "")?;

                        for a in m.is_length_for.iter().map(|l| &self.members[l.0]) {
                            if a.pointers[0].ty == PointerType::Mut || a.full_type.1.contains(" mut ") {
                                write!(f, "{0}{0}self.{1} = {1}.first_mut()", INDENT, a.name.1)?;
                            } else {
                                write!(f, "{0}{0}self.{1} = {1}.first()", INDENT, a.name.1)?;
                            }

                            if a.pointers[0].optional {
                                writeln!(f, ";")?;
                            } else {
                                writeln!(f, ".expect(\"Length of `{0}` must be > 0\");", a.name.1)?;
                            }
                        }


                        writeln!(f, "{0}}}", INDENT)?;
                    }
                }
            }


            writeln!(f, "}}\n")?;
        }

        /*if has_p_next || all_member_p_next {
            if !lifetimes.is_empty() {
                // writeln!(f, "impl<{0}> {1}<{0}> {{", lifetimes, self.name.1)?;

                if self.generic_parameters.iter().map(|g| g.requirements.len()).sum::<usize>() == 0 {
                    // writeln!(f, "pub {} {}<{}> {{", variant, self.name.1, lifetimes)?;
                    writeln!(f, "impl<{0}> Drop for {1}<{0}> {{", lifetimes, self.name.1)?;
                } else {
                    let generic_requirements: String = self.generic_parameters.iter()
                        .filter(|g| !g.requirements.is_empty())
                        .map(|g| g.name.clone() + ": " + &g.requirements.join(" + "))
                        .collect::<Vec<_>>()
                        .join(", ");
                    // writeln!(f, "pub {} {}<{}> where {} {{", variant, self.name.1, lifetimes, generic_requirements)?;
                    writeln!(f, "impl<{0}> Drop for {1}<{0}> where {2} {{", lifetimes, self.name.1, generic_requirements)?;
                }
            } else {
                writeln!(f, "impl Drop for {} {{", self.name.1)?;
            }

            writeln!(f, "{INDENT}fn drop(&mut self) {{")?;
            if all_member_p_next {
                writeln!(f, "{INDENT}{INDENT}unsafe {{ &mut *(self as *mut _ as *mut VkBaseInStructure) }}.recursive_clear_next();")?;
            } else {
                writeln!(f, "{INDENT}{INDENT}self.recursive_clear_next();")?;
            }
            writeln!(f, "{INDENT}}}")?;
            writeln!(f, "}}\n")?;
        }*/

        if self.generic_parameters.len() == 1 {
            if self.generic_parameters[0].requirements.len() == 1 && &self.generic_parameters[0].requirements[0] == "'static" && &self.generic_parameters[0].name == "D" {
                //impl<P: IsPtrValid> From<&AllocationCallbacks<P>> for &AllocationCallbacks {
                //     fn from(a: &AllocationCallbacks<P>) -> Self {
                //         unsafe { core::mem::transmute(a) }
                //     }
                // }


                // if let Some(platform) = &self.platform {
                //     writeln!(f, "#[cfg(feature = \"platform_{}\")]", platform)?;
                // }

                if lifetimes_iter.is_empty() {
                    writeln!(f, "impl<D> {0}<D> {{", self.name.1)?;
                    writeln!(f, "{}pub(crate) fn as_general(&self) -> &{} {{", INDENT, self.name.1)?;
                    writeln!(f, "{0}{0}unsafe {{ core::mem::transmute(self) }}", INDENT)?;
                    writeln!(f, "{}}}", INDENT)?;
                    writeln!(f, "}}")?;
                    writeln!(f)?;
                } else {
                    writeln!(f, "impl<'a, D> {0}<'a, D> {{", self.name.1)?;
                    writeln!(f, "{}pub(crate) fn as_general(&self) -> &{}<'a> {{", INDENT, self.name.1)?;
                    writeln!(f, "{0}{0}unsafe {{ core::mem::transmute(self) }}", INDENT)?;
                    writeln!(f, "{}}}", INDENT)?;
                    writeln!(f, "}}")?;
                    writeln!(f)?;
                }
            }
        }

        Ok(f)
    }

    pub fn has_inline_p_next(&self, spec: &Spec) -> bool {
        self.members.iter()
            .any(|m| &m.name.1 == "p_next" ||
                spec.structs_unions.iter()
                    .find(|os| os.name.0 == m.full_type.0)
                    .map_or(false, |os|
                        Self::has_inline_p_next(os, spec),
                    )
            )
    }

    pub fn inline_p_next_names(&self, spec: &Spec) -> Vec<(String, String)> {
        let mut result = if self.members.iter().any(|m| &m.name.1 == "p_next") {
            vec![("p_next".to_string(), self.name.1.clone())]
        } else {
            vec![]
        };

        for e in self.members.iter().map(|m| spec.structs_unions.iter()
            .find(|os| os.name.0 == m.full_type.0 && os.variant == Variant::Struct)
            .map_or(vec![], |os|
                Self::inline_p_next_names(os, spec).into_iter().map(|(n, nn)| (format!("{}_{n}", m.name.1.trim_start_matches("Raw").to_snake_case()), nn)).collect::<Vec<_>>(),
            )
        ) {
            result.extend(e);
        }
        result
    }

    fn get_to_remove(&self, spec: &Spec) -> Vec<String> {
        let mut to_remove = vec!["p_next".to_string()];
        for m in &self.members {
            let mut no_lifetime_type = m.full_type.1.as_str();
            if let Some(i) = no_lifetime_type.find('<') {
                no_lifetime_type = &no_lifetime_type[..i];
            }

            if let Some(s) = spec.structs_unions.iter().find(|s| s.name.1 == no_lifetime_type && s.variant == Variant::Struct) {
                to_remove.extend(s.get_to_remove(spec).into_iter().map(|s| format!("{}_{s}", m.name.1)))
            }
        }
        to_remove
    }

    pub fn format(&self, spec: &Spec, with_p_next: bool) -> Result<String, std::fmt::Error> {
        let mut f = String::new();

        if &self.name.1 == "RawDeviceFaultInfoEXT" || &self.name.1 == "RawDeviceFaultCountsEXT" {
            return Ok(f);
        }

        let has_null_member = ["RawCommandBufferAllocateInfo", "RawDescriptorSetAllocateInfo", "RawSwapchainCreateInfoKHR", "RawVideoSessionParametersCreateInfoKHR"].contains(&self.name.1.as_str());
        let null_member_index = has_null_member.then(|| self.members.iter().enumerate().find(|(_, m)| ["command_pool", "descriptor_pool", "surface", "video_session"].contains(&m.name.1.as_str())).map(|(i, _)| i)).flatten();

        let mut has_p_next = false;
        let mut has_mut_p_next = false;

        let mut would_have_p_next = false;
        let mut would_have_mut_p_next = false;

        if with_p_next {
            for m in self.members.iter() {
                if &m.name.0 == "pNext" {
                    has_p_next = true;
                    if m.full_type.1.contains("NextMutPtr") {
                        has_mut_p_next = true;
                    }
                }
            }
        } else {
            for m in self.members.iter() {
                if &m.name.0 == "pNext" {
                    would_have_p_next = true;
                    if m.full_type.1.contains("NextMutPtr") {
                        would_have_mut_p_next = true;
                    }
                }
            }
        }

        let mut generics = self.generic_parameters.clone();
        let mut with_p_next_generics = self.generic_parameters.clone();
        let mut extra_generic_requirements = vec![];
        if with_p_next {
            let mut i = 0;
            if has_p_next {
                generics.insert(i, GenericParameter {
                    name: "N".to_string(),
                    requirements: vec![
                        format!("IntoIterator<Item=&'f_p_next mut dyn {}{}>", self.name.1.trim_start_matches("Raw"), if has_mut_p_next { "MutNext" } else { "Next" }),
                        // "DoubleEndedIterator".to_string(),
                    ],
                    // default: Some(format!("[&'static mut dyn {}{}; 0]", self.name.1.trim_start_matches("Raw"), if has_mut_p_next { "MutNext" } else { "Next" })),
                    default: None,
                });
                i += 1;
                extra_generic_requirements.push(format!("<N as IntoIterator>::IntoIter: DoubleEndedIterator"));
            }
            for (j, (j_n, j_n_n)) in self.inline_p_next_names(spec).into_iter().skip(if has_p_next { 1 } else { 0 }).enumerate() {

                let mut has_mut_p_next = false;
                for m in spec.structs_unions.iter().find(|os| os.name.1 == j_n_n).unwrap().members.iter() {
                    if &m.name.0 == "pNext" {
                        if m.full_type.1.contains("NextMutPtr") {
                            has_mut_p_next = true;
                        }
                    }
                }

                generics.insert(i, GenericParameter {
                    name: format!("N{j}"),
                    requirements: vec![
                        format!("IntoIterator<Item=&'f_{j_n} mut dyn {}{}>", j_n_n.trim_start_matches("Raw"), if has_mut_p_next { "MutNext" } else { "Next" }),
                        // "DoubleEndedIterator".to_string(),
                    ],
                    // default: Some(format!("[&'static mut dyn {}{}; 0]", j_n_n.trim_start_matches("Raw"), if has_mut_p_next { "MutNext" } else { "Next" })),
                    default: None,
                });
                i += 1;
                extra_generic_requirements.push(format!("<N{j} as IntoIterator>::IntoIter: DoubleEndedIterator"));
            }
        } else {
            let mut i = 0;
            if has_p_next {
                with_p_next_generics.insert(i, GenericParameter {
                    name: format!("[&'static mut dyn {}{}; 0]", self.name.1.trim_start_matches("Raw"), if has_mut_p_next { "MutNext" } else { "Next" }),
                    requirements: vec![
                        // format!("IntoIterator<Item=&'f_p_next mut dyn {}{}>", self.name.1.trim_start_matches("Raw"), if has_mut_p_next { "MutNext" } else { "Next" }),
                        // "DoubleEndedIterator".to_string(),
                    ],
                    default: None,
                });
                i += 1;
            }
            for (j, (j_n, j_n_n)) in self.inline_p_next_names(spec).into_iter().skip(if has_p_next { 1 } else { 0 }).enumerate() {

                let mut has_mut_p_next = false;
                for m in spec.structs_unions.iter().find(|os| os.name.1 == j_n_n).unwrap().members.iter() {
                    if &m.name.0 == "pNext" {
                        if m.full_type.1.contains("NextMutPtr") {
                            has_mut_p_next = true;
                        }
                    }
                }

                with_p_next_generics.insert(i, GenericParameter {
                    name: format!("[&'static mut dyn {}{}; 0]", j_n_n.trim_start_matches("Raw"), if has_mut_p_next { "MutNext" } else { "Next" }),
                    requirements: vec![
                        // format!("IntoIterator<Item=&'f_{j_n} mut dyn {}{}>", j_n_n.trim_start_matches("Raw"), if has_mut_p_next { "MutNext" } else { "Next" }),
                        // "DoubleEndedIterator".to_string(),
                    ],
                    default: None,
                });
                i += 1;
            }
        }

        let to_remove = self.get_to_remove(spec).into_iter().map(|s| format!("'f_{s}")).collect::<HashSet<_>>();

        let lifetimes_iter: Vec<_> = {
            let mut hlt: Vec<_> = self.members.iter()
                .filter(|m| !m.special_exclude)
                .flat_map(|m| m.handle_lifetimes.iter())
                .map(|l| format!("'{}", l)).collect();
            hlt.sort();
            hlt.dedup();

            hlt
        }.into_iter().chain(self.members.iter()
            .filter(|m| !m.special_exclude)
            .flat_map(|m|
                m.pointers.iter()
                    .map(|f| &f.lifetime_name)
                    .flatten()
                    .chain(m.type_lifetimes.iter())
            )
            .map(|l| format!("'f_{}", l)))
            .filter(|l| with_p_next || !to_remove.contains(l))
            .collect::<Vec<String>>();

        let full_lifetimes_iter: Vec<_> = {
            let mut hlt: Vec<_> = self.members.iter()
                .filter(|m| !m.special_exclude)
                .flat_map(|m| m.handle_lifetimes.iter())
                .map(|l| format!("'{}", l)).collect();
            hlt.sort();
            hlt.dedup();

            hlt
        }.into_iter().chain(self.members.iter()
            .filter(|m| !m.special_exclude)
            .flat_map(|m|
                m.pointers.iter()
                    .map(|f| &f.lifetime_name)
                    .flatten()
                    .chain(m.type_lifetimes.iter())
            )
            .map(|l| format!("'f_{}", l)))
            .collect::<Vec<String>>();

        let raw_lifetimes_iter: Vec<_> = {
            let to_be_static = Rc::new(RefCell::new(HashSet::new()));

            let to_be_static_clone = to_be_static.clone();

            let mut hlt: Vec<_> = self.members.iter()
                .flat_map(|m|
                    m.handle_lifetimes.iter().map(|l| {
                        if m.special_exclude {
                            RefCell::borrow_mut(&to_be_static_clone).insert(format!("'{}", l));
                        }
                        l
                    })
                )
                .map(|l| format!("'{}", l)).collect();
            hlt.sort();
            hlt.dedup();

            for i in 0..hlt.len() {
                if RefCell::borrow(&to_be_static).contains(&hlt[i]) {
                    hlt[i] = "'static".to_string();
                }
            }

            hlt
        }.into_iter().chain(self.members.iter()
            .filter(|m| !m.special_exclude)
            .flat_map(|m|
                m.pointers.iter()
                    .map(|f| &f.lifetime_name)
                    .flatten()
                    .chain(m.type_lifetimes.iter())
            )
            .map(|l| if !with_p_next && to_remove.contains(format!("'f_{}", l).as_str()) {
                "'static".to_string()
            } else {
                format!("'f_{}", l)
            }))
            .collect::<Vec<String>>();

        let into_with_next_lifetimes_iter: Vec<_> = {
            let to_be_static = Rc::new(RefCell::new(HashSet::new()));

            let to_be_static_clone = to_be_static.clone();

            let mut hlt: Vec<_> = self.members.iter().enumerate()
                .filter(|(i, m)| Some(*i) != null_member_index)
                .flat_map(|(i, m)|
                    m.handle_lifetimes.iter().map(|l| {
                        if m.special_exclude {
                            RefCell::borrow_mut(&to_be_static_clone).insert(format!("'{}", l));
                        }
                        l
                    })
                )
                .map(|l| format!("'{}", l)).collect();
            hlt.sort();
            hlt.dedup();

            for i in 0..hlt.len() {
                if RefCell::borrow(&to_be_static).contains(&hlt[i]) {
                    hlt[i] = "'static".to_string();
                }
            }

            hlt
        }.into_iter().chain(self.members.iter()
            .filter(|m| !m.special_exclude)
            .flat_map(|m|
                m.pointers.iter()
                    .map(|f| &f.lifetime_name)
                    .flatten()
                    .chain(m.type_lifetimes.iter())
            )
            .map(|l| if !with_p_next && to_remove.contains(format!("'f_{}", l).as_str()) {
                "'static".to_string()
            } else {
                format!("'f_{}", l)
            }))
            .collect::<Vec<String>>();

        let lifetimes: String = lifetimes_iter.iter().map(|s| s.clone()).chain(generics.iter().map(|g| g.name.clone())).collect::<Vec<_>>().join(", ");
        let full_lifetimes: String = full_lifetimes_iter.iter().map(|s| s.clone()).chain(generics.iter().map(|g| g.name.clone())).collect::<Vec<_>>().join(", ");
        let raw_lifetimes: String = raw_lifetimes_iter.iter().map(|s| s.clone()).chain(self.generic_parameters.iter().map(|g| g.name.clone())).collect::<Vec<_>>().join(", ");
        let into_with_next_lifetimes: String = into_with_next_lifetimes_iter.iter().map(|s| s.clone()).chain(with_p_next_generics.iter().map(|g| g.name.clone())).collect::<Vec<_>>().join(", ");
        let lifetimes_with_default: String = lifetimes_iter.iter().map(|s| s.clone()).chain(generics.iter().map(|g| g.name.clone() + &g.default.clone().map_or(String::new(), |d| " = ".to_string() + &d))).collect::<Vec<_>>().join(", ");

        let all_member_p_next = self.variant != Variant::Struct && self.members.iter().all(|m| spec.structs_unions.iter().find(|s| &s.name.1 == &m.type_name.1).map_or(false, |s| s.members.iter().any(|om| &om.name.1 == "p_next")));

        let mut derives = Vec::new();
        if self.variant == Variant::Struct {
            if self.needs_ptr_debug_print {
                // p_derive = true;
            } else {
                derives.push("Debug");
            }
        }

        // if let Some(platform) = &self.platform {
        //     writeln!(f, "#[cfg(feature = \"platform_{}\")]", platform)?;
        // }

        let mut contains_private = false;
        let mut contains_non_s_type_private = false;

        for (index, m) in self.members.iter().enumerate() {
            let contains_array_ptr = self.members.iter().any(|om| om.is_length_for.iter().any(|(i, _, _)| i == &index));

            if null_member_index == Some(index) || !m.is_length_for.is_empty() || contains_array_ptr || !m.selector.is_none() || !m.selector_for_type_and_name.is_none() || &m.name.0 == "sType" {
                if &m.name.0 != "sType" {
                    contains_non_s_type_private = true;
                }

                contains_private = true;

                if contains_private && contains_non_s_type_private {
                    break;
                }
            };
        }

        let mut has_non_bool = false;
        let mut contiguous_bools: Option<(usize, usize)> = None;
        for (i, m) in self.members.iter().enumerate() {
            if &m.name.0 == "sType" || &m.name.0 == "pNext" {
                continue;
            }

            if &m.full_type.1 == "Bool32" {
                if let Some((_, count)) = &mut contiguous_bools {
                    *count += 1;
                } else {
                    contiguous_bools = Some((i, 1));
                }
            } else {
                has_non_bool = true;
            }
        }

        if let Variant::UnionEnum { selections, values, selector_enum, value_variants, bit_width } = &self.variant {
            // Write out enum of each union variant

            // writeln!(f, "#[repr(i{})]", bit_width)?;

            derives.insert(0, "Debug");
            // derives.push("Copy");
            // derives.push("Clone");
            if !self.members.iter().any(|m| spec.structs_unions.iter().find(|s| &s.name.1 == &m.type_name.1).map_or(false, |s| s.members.iter().any(|om| &om.name.1 == "p_next"))) {
                derives.extend_from_slice(&["Copy", "Clone"]);
            }

            if !derives.is_empty() {
                writeln!(f, "#[derive({})]", derives.join(", "))?;
            }

            if !full_lifetimes.is_empty() {
                writeln!(f, "pub enum {}<{}> {{", self.name.1.trim_start_matches("Raw"), full_lifetimes)?;
            } else {
                writeln!(f, "pub enum {} {{", self.name.1.trim_start_matches("Raw"))?;
            }

            for m in &self.members {
                let enum_variant_name = m.name.1.to_camel_case();

                writeln!(f, "{}{}({}),", INDENT, enum_variant_name, m.full_type.1)?;
            }

            if !full_lifetimes.is_empty() {
                writeln!(f, "{}UnknownVariant(i{}, {}<{}>)", INDENT, bit_width, self.name.1, full_lifetimes)?;
            } else {
                writeln!(f, "{}UnknownVariant(i{}, {})", INDENT, bit_width, self.name.1)?;
            }

            writeln!(f, "}}\n")?;

            // Implement the enum

            if !full_lifetimes.is_empty() {
                writeln!(f, "impl<{1}> {0}<{1}> {{", self.name.1.trim_start_matches("Raw"), full_lifetimes)?;
            } else {
                writeln!(f, "impl {} {{", self.name.1.trim_start_matches("Raw"))?;
            }

            // Function to construct

            if !full_lifetimes.is_empty() {
                writeln!(f, "{}pub fn new(selector: {}, value: {}<{}>) -> Self {{", INDENT, selector_enum, self.name.1, full_lifetimes)?;
            } else {
                writeln!(f, "{}pub fn new(selector: {}, value: {}) -> Self {{", INDENT, selector_enum, self.name.1)?;
            }

            if all_member_p_next {
                writeln!(f, "{INDENT}{INDENT}let value = core::mem::ManuallyDrop::new(value);")?;
            }

            writeln!(f, "{0}{0}match selector {{", INDENT)?;

            for (i, m) in self.members.iter().enumerate() {
                let enum_variant_name = m.name.1.to_camel_case();
                if all_member_p_next {
                    writeln!(f, "{0}{0}{0}{1} => Self::{2}(unsafe {{ core::mem::transmute_copy(&value.{3}) }}),", INDENT, value_variants[i], enum_variant_name, m.name.1)?;
                } else {
                    writeln!(f, "{0}{0}{0}{1} => Self::{2}(unsafe {{ value.{3} }}),", INDENT, value_variants[i], enum_variant_name, m.name.1)?;
                }
            }
            if all_member_p_next {
                writeln!(f, "{0}{0}{0}{1}(s) => Self::UnknownVariant(s, core::mem::ManuallyDrop::into_inner(value)),", INDENT, selector_enum)?;
            } else {
                writeln!(f, "{0}{0}{0}{1}(s) => Self::UnknownVariant(s, value),", INDENT, selector_enum)?;
            }

            writeln!(f, "{0}{0}}}", INDENT)?;

            writeln!(f, "{}}}", INDENT)?;

            writeln!(f, "")?;

            // Function to get the selector value

            writeln!(f, "{}pub fn get_selector(&self) -> {} {{", INDENT, selector_enum)?;

            writeln!(f, "{0}{0}match self {{", INDENT)?;

            for (i, m) in self.members.iter().enumerate() {
                let enum_variant_name = m.name.1.to_camel_case();

                writeln!(f, "{0}{0}{0}Self::{1}(_) => {2},", INDENT, enum_variant_name, value_variants[i])?;
            }
            writeln!(f, "{0}{0}{0}Self::UnknownVariant(s, _) => {1}(*s),", INDENT, selector_enum)?;

            writeln!(f, "{0}{0}}}", INDENT)?;

            writeln!(f, "{}}}", INDENT)?;

            writeln!(f, "")?;

            // Function to get union from enum

            if !full_lifetimes.is_empty() {
                writeln!(f, "{}pub fn get_value(self) -> {}<{}> {{", INDENT, self.name.1, full_lifetimes)?;
            } else {
                writeln!(f, "{}pub fn get_value(self) -> {} {{", INDENT, self.name.1)?;
            }

            // writeln!(f, "{}pub fn get_value(self) -> {} {{", INDENT, self.name.1)?;

            writeln!(f, "{0}{0}match self {{", INDENT)?;

            for (i, m) in self.members.iter().enumerate() {
                let enum_variant_name = m.name.1.to_camel_case();

                if all_member_p_next {
                    writeln!(f, "{0}{0}{0}Self::{1}({2}) => {3} {{ {2}: core::mem::ManuallyDrop::new({2}) }},", INDENT, enum_variant_name, m.name.1, self.name.1)?;
                } else {
                    writeln!(f, "{0}{0}{0}Self::{1}({2}) => {3} {{ {2} }},", INDENT, enum_variant_name, m.name.1, self.name.1)?;
                }
            }
            writeln!(f, "{0}{0}{0}Self::UnknownVariant(_, value) => value,", INDENT)?;

            writeln!(f, "{0}{0}}}", INDENT)?;

            writeln!(f, "{}}}", INDENT)?;


            // End Implement the enum

            writeln!(f, "}}\n")?;

            return Ok(f);
        }

        if let Variant::Union = &self.variant {
            writeln!(f, "#[derive(Debug, Copy, Clone)]")?;

            if !full_lifetimes.is_empty() {
                writeln!(f, "pub enum {}<{}> {{", self.name.1.trim_start_matches("Raw"), full_lifetimes)?;
            } else {
                writeln!(f, "pub enum {} {{", self.name.1.trim_start_matches("Raw"))?;
            }

            for m in &self.members {
                let enum_variant_name = m.name.1.to_camel_case();

                writeln!(f, "{}{}({}),", INDENT, enum_variant_name, m.full_type.1)?;
            }

            writeln!(f, "}}\n")?;

            // Implement the enum

            if !full_lifetimes.is_empty() {
                writeln!(f, "impl<{1}> {0}<{1}> {{", self.name.1.trim_start_matches("Raw"), full_lifetimes)?;
            } else {
                writeln!(f, "impl {} {{", self.name.1.trim_start_matches("Raw"))?;
            }

            // Function to turn into union

            if !full_lifetimes.is_empty() {
                writeln!(f, "{}pub fn into_raw(self) -> {}<{}> {{", INDENT, self.name.1, full_lifetimes)?;
            } else {
                writeln!(f, "{}pub fn into_raw(self) -> {} {{", INDENT, self.name.1)?;
            }

            writeln!(f, "{0}{0}match self {{", INDENT)?;

            for (i, m) in self.members.iter().enumerate() {
                let enum_variant_name = m.name.1.to_camel_case();

                writeln!(f, "{0}{0}{0}Self::{1}(v) => {2} {{ {3}: v }},", INDENT, enum_variant_name, self.name.1, m.name.1)?;
            }

            writeln!(f, "{0}{0}}}", INDENT)?;

            writeln!(f, "{}}}", INDENT)?;

            // End Implement the enum

            writeln!(f, "}}\n")?;

            return Ok(f);
        }

        if has_p_next {
            let trait_suffix = if has_mut_p_next {
                "MutNext"
            } else {
                "Next"
            };

            if has_mut_p_next {
                writeln!(f, "pub trait {}{}: OutStructure + core::fmt::Debug {{}}", self.name.1.trim_start_matches("Raw"), trait_suffix)?;
            } else {
                writeln!(f, "pub trait {}{}: StructureTyped + core::fmt::Debug {{}}", self.name.1.trim_start_matches("Raw"), trait_suffix)?;
            }

            writeln!(f)?;
        }

        if !self.extended_by.is_empty() && with_p_next {
            let trait_suffix = if has_mut_p_next {
                "MutNext"
            } else {
                "Next"
            };
            for e in &self.extended_by {
                if has_mut_p_next {
                    if e.1.generics.is_empty() {
                        writeln!(f, "impl {0}{1} for TrackedUninitTypedStruct<{2}> {{}}", self.name.1.trim_start_matches("Raw"), trait_suffix, e.1.full_name)?;
                    } else {
                        writeln!(f, "impl<{3}: core::fmt::Debug> {0}{1} for TrackedUninitTypedStruct<{2}> {{}}", self.name.1.trim_start_matches("Raw"), trait_suffix, e.1.full_name, e.1.generics.iter().map(|g| g.name.clone()).collect::<Vec<String>>().join(", "))?;
                    }
                }
                // if !e.1.returned_only {
                if e.1.generics.is_empty() {
                    writeln!(f, "impl {0}{1} for {2} {{}}", self.name.1.trim_start_matches("Raw"), trait_suffix, e.1.full_name)?;
                } else {
                    writeln!(f, "impl<{3}: core::fmt::Debug> {0}{1} for {2} {{}}", self.name.1.trim_start_matches("Raw"), trait_suffix, e.1.full_name, e.1.generics.iter().map(|g| g.name.clone()).collect::<Vec<String>>().join(", "))?;
                }
                // }
            }
            writeln!(f)?;
        }

        if let Some((_, count)) = &contiguous_bools {
            if *count > 1 {
                writeln!(f, "#[repr(C)]")?;
            }
        }

        let (_immutable_ptr_count, mutable_ptr_count) = self.members.iter().filter(|m| &m.name.1 != "p_next").flat_map(|m| &m.pointers).map(|p| p.ty).fold((0, 0), |(immutable, mutable), p| match p {
            PointerType::Const => (immutable + 1, mutable),
            PointerType::Mut => (immutable, mutable + 1)
        });

        if !with_p_next {
            if mutable_ptr_count == 0 && !self.has_inline_mutable && !self.members.iter().any(|m| m.extern_sync || spec.structs_unions.iter().find(|s| &s.name.1 == &m.type_name.1).map_or(false, |s| s.members.iter().any(|om| om.extern_sync))) /*|| generics.iter().any(|g| &g.name == "D")*/ {
                if &self.name.1 != "RawAccelerationStructureGeometryKHR" && &self.name.1 != "RawDescriptorGetInfoEXT" {
                    derives.extend_from_slice(&["Copy", "Clone"]);
                }
                // TODO: Need work to get working, maybe not worth it
                // if immutable_ptr_count == 0 && self.variant != Variant::Union {
                //     derives.extend_from_slice(&["Eq", "PartialEq"])
                // }
            }
        }

        if !derives.is_empty() {
            writeln!(f, "#[derive({})]", derives.join(", "))?;
        }

        let name_suffix = if with_p_next {
            "WithNext"
        } else {
            ""
        };

        if !lifetimes.is_empty() {
            if generics.iter().map(|g| g.requirements.len()).sum::<usize>() == 0 {
                writeln!(f, "pub struct {}{name_suffix}<{}> {{", self.name.1.trim_start_matches("Raw"), lifetimes)?;
            } else {
                let generic_requirements: String = generics.iter()
                    .filter(|g| !g.requirements.is_empty())
                    .map(|g| g.name.clone() + ": " + &g.requirements.join(" + "))
                    .chain(extra_generic_requirements.clone())
                    .collect::<Vec<_>>()
                    .join(", ");
                writeln!(f, "pub struct {}{name_suffix}<{}> where {} {{", self.name.1.trim_start_matches("Raw"), lifetimes_with_default, generic_requirements)?;
            }
        } else {
            writeln!(f, "pub struct {}{name_suffix} {{", self.name.1.trim_start_matches("Raw"))?;
        }

        let mut nk = 0;

        for (i, m) in self.members.iter().enumerate() {
            if (!m.is_length_for.is_empty() && !((self.name.1 == "RawPipelineMultisampleStateCreateInfo" && m.name.1 == "rasterization_samples") || (&self.name.1 == "RawDescriptorSetLayoutBinding" && &m.name.1 == "descriptor_count"))) || &m.name.0 == "sType" || m.selector_for_type_and_name.is_some() {
                continue;
            }
            if null_member_index == Some(i) {
                continue;
            }

            if &m.name.0 == "pNext" {
                let l = m.full_type.1.trim_start_matches("Option<").trim_start_matches("NextMutPtr<").trim_start_matches("NextPtr<").trim_end_matches(">");
                if with_p_next {
                    if has_mut_p_next {
                        writeln!(f, "{INDENT}pub {0}: N,", m.name.1)?;
                    } else {
                        writeln!(f, "{INDENT}pub {0}: N,", m.name.1)?;
                    }
                }

                continue;
            }

            if &m.full_type.1 == "Bool32" {
                continue;
            }

            let contains_array_ptr = self.members.iter().any(|om| om.is_length_for.iter().any(|(j, _, _)| j == &i));

            let mut alt_full_type = m.full_type.1.trim_start_matches("Raw").replace("Self", &if !lifetimes.is_empty() {
                format!("{}<{}>", self.name.1, lifetimes)
            } else {
                self.name.1.clone()
            });

            if alt_full_type.starts_with("[Raw") {
                alt_full_type = alt_full_type.replacen("[Raw", "[", 1).to_string();
            }

            if let Some(b) = spec.bit_fields.iter().find(|b| b.enum_name.as_ref().map(|n| n.1.trim_start_matches("Raw")) == Some(&alt_full_type)) {
                if !b.variants.contains_key(&0) && m.value_optional {
                    alt_full_type = format!("Option<{}>", alt_full_type);
                }
            }

            if with_p_next {
                let mut no_lifetime_type = m.full_type.1.as_str();
                if let Some(i) = no_lifetime_type.find('<') {
                    no_lifetime_type = &no_lifetime_type[..i];
                }

                if let Some(s) = spec.structs_unions.iter().find(|s| s.name.1 == no_lifetime_type) {
                    if s.has_inline_p_next(spec) {
                        if let Some(i) = alt_full_type.find('<') {
                            alt_full_type.insert_str(i, name_suffix);

                            for _ in s.inline_p_next_names(spec) {
                                let j = alt_full_type.find('>').unwrap();
                                alt_full_type.insert_str(j, &format!(", N{nk}"));
                                nk += 1;
                            }
                        } else {
                            alt_full_type += name_suffix;
                        }
                    }
                }
            } else {
                let mut no_lifetime_type = m.full_type.1.as_str();
                if let Some(i) = no_lifetime_type.find('<') {
                    no_lifetime_type = &no_lifetime_type[..i];
                }

                if let Some(s) = spec.structs_unions.iter().find(|s| s.name.1 == no_lifetime_type) {
                    let mut only_lifetimes = alt_full_type.as_str();
                    let mut range = None;
                    let mut k = 0;
                    while let Some(i) = only_lifetimes.find('<') {
                        let j = only_lifetimes.rfind('>').unwrap();
                        only_lifetimes = &only_lifetimes[i + 1..j];
                        range = Some(i + 1 + k..j + k);
                        k += i + 1
                    }

                    if let Some(range) = range {
                        let mut lifetimes = only_lifetimes.split(", ").collect::<Vec<_>>();

                        let to_remove = s.get_to_remove(spec).into_iter().map(|s| format!("'f_{}_{s}", m.name.1)).collect::<HashSet<_>>();

                        lifetimes.retain(|l| !to_remove.contains(*l));

                        alt_full_type.replace_range(range, lifetimes.join(", ").as_str());
                    }
                }
            }

            if contains_array_ptr {
                writeln!(f, "{}pub {}: {},", INDENT, m.name.1, m.slice_type)?;
            } else if let Some(_) = &m.selector {
                let lifetimes: String = m.handle_lifetimes.iter().map(|l| format!("'{}", l)).chain(m.type_lifetimes.iter().map(|l| format!("'f_{}", l))).collect::<Vec<_>>()
                    .join(", ");

                if lifetimes.is_empty() {
                    writeln!(f, "{}pub {}_variant: {},", INDENT, m.name.1, m.type_name.1.trim_start_matches("Raw"))?;
                } else {
                    writeln!(f, "{}pub {}_variant: {}<{}>,", INDENT, m.name.1, m.type_name.1.trim_start_matches("Raw"), lifetimes)?;
                }
            } else {
                writeln!(f, "{}pub {}: {},", INDENT, m.name.1, alt_full_type)?;
            }
        }

        for (i, m) in self.members.iter().enumerate() {
            if &m.full_type.1 != "Bool32" {
                continue;
            }

            writeln!(f, "{}pub {}: bool,", INDENT, m.name.1)?;
        }

        writeln!(f, "}}")?;
        writeln!(f)?;

        for a in &self.aliases {
            if !lifetimes.is_empty() {
                writeln!(f, "pub type {0}{name_suffix}<{2}> = {1}{name_suffix}<{2}>;", a.1.trim_start_matches("Raw"), self.name.1.trim_start_matches("Raw"), lifetimes)?;
            } else {
                writeln!(f, "pub type {}{name_suffix} = {}{name_suffix};", a.1.trim_start_matches("Raw"), self.name.1.trim_start_matches("Raw"))?;
            }
        }

        if !self.aliases.is_empty() {
            writeln!(f)?;
        }

        // if let Some(platform) = &self.platform {
        //     writeln!(f, "#[cfg(feature = \"platform_{}\")]", platform)?;
        // }

        if !lifetimes.is_empty() {
            // writeln!(f, "impl<{0}> {1}<{0}> {{", lifetimes, self.name.1)?;

            if generics.iter().map(|g| g.requirements.len()).sum::<usize>() == 0 {
                // writeln!(f, "pub {} {}<{}> {{", variant, self.name.1, lifetimes)?;
                writeln!(f, "impl<{0}> {1}{name_suffix}<{0}> {{", lifetimes, self.name.1.trim_start_matches("Raw"))?;
            } else {
                let generic_requirements: String = generics.iter()
                    .filter(|g| !g.requirements.is_empty())
                    .map(|g| g.name.clone() + ": " + &g.requirements.join(" + "))
                    .chain(extra_generic_requirements.clone())

                    .collect::<Vec<_>>()
                    .join(", ");
                // writeln!(f, "pub {} {}<{}> where {} {{", variant, self.name.1, lifetimes, generic_requirements)?;
                writeln!(f, "impl<{0}> {1}{name_suffix}<{0}> where {2} {{", lifetimes, self.name.1.trim_start_matches("Raw"), generic_requirements)?;
            }
        } else {
            writeln!(f, "impl {}{name_suffix} {{", self.name.1.trim_start_matches("Raw"))?;
        }

        if /* !self.returned_only */ true {
            write!(f, "{}pub fn into_raw(self) -> ", INDENT)?;
            if !raw_lifetimes.is_empty() {
                writeln!(f, "{}<{}> {{", self.name.1, raw_lifetimes)?;
            } else {
                writeln!(f, "{} {{", self.name.1)?;
            }

            writeln!(f, "{0}{0}{1}::new(", INDENT, self.name.1)?;

            for (i, m) in self.members.iter().enumerate() {
                if (!m.is_length_for.is_empty() && !((self.name.1 == "RawPipelineMultisampleStateCreateInfo" && m.name.1 == "rasterization_samples") || (&self.name.1 == "RawDescriptorSetLayoutBinding" && &m.name.1 == "descriptor_count"))) || &m.name.0 == "sType" || m.selector_for_type_and_name.is_some() {
                    continue;
                }
                if null_member_index == Some(i) {
                    continue;
                }
                if !with_p_next && &m.name.1 == "p_next" {
                    writeln!(f, "{0}{0}{0}[],", INDENT)?;
                    continue;
                }

                if let Some(_) = &m.selector {
                    writeln!(f, "{0}{0}{0}self.{1}_variant,", INDENT, m.name.1)?;
                } else {
                    if !with_p_next && spec.structs_unions.iter().find(|os| os.name.0 == m.full_type.0).map_or(false, |os| Self::has_inline_p_next(os, spec)) {
                        writeln!(f, "{0}{0}{0}self.{1}.into_with_next(),", INDENT, m.name.1)?;
                    } else {
                        writeln!(f, "{0}{0}{0}self.{1},", INDENT, m.name.1)?;
                    }
                }
            }

            writeln!(f, "{0}{0})", INDENT)?;

            writeln!(f, "{}}}", INDENT)?;

            if generics.iter().any(|g| &g.name == "D") {
                writeln!(f, "}}")?;
                writeln!(f)?;
                return Ok(f);
            }

            writeln!(f)?;
        }

        if !with_p_next && self.has_inline_p_next(spec) {
            let t = if would_have_mut_p_next {
                format!("{0}MutNext", self.name.1.trim_start_matches("Raw"))
            } else {
                format!("{0}Next", self.name.1.trim_start_matches("Raw"))
            };

            writeln!(f, "{INDENT}pub fn into_with_next(self) -> {}WithNext<{}> {{", self.name.1.trim_start_matches("Raw"), into_with_next_lifetimes)?;
            writeln!(f, "{INDENT}{INDENT}{}WithNext {{", self.name.1.trim_start_matches("Raw"))?;

            for (i, m) in self.members.iter().enumerate() {
                if (!m.is_length_for.is_empty() && !((self.name.1 == "RawPipelineMultisampleStateCreateInfo" && m.name.1 == "rasterization_samples") || (&self.name.1 == "RawDescriptorSetLayoutBinding" && &m.name.1 == "descriptor_count"))) || &m.name.0 == "sType" || m.selector_for_type_and_name.is_some() {
                    continue;
                }
                if null_member_index == Some(i) {
                    continue;
                }

                if &m.name.1 == "p_next" {
                    writeln!(f, "{INDENT}{INDENT}{INDENT}{}: [],", m.name.1)?;
                } else if let Some(_) = &m.selector {
                    writeln!(f, "{INDENT}{INDENT}{INDENT}{0}_variant: self.{0}_variant,", m.name.1)?;
                } else {
                    if spec.structs_unions.iter().find(|os| os.name.0 == m.full_type.0).map_or(false, |os| Self::has_inline_p_next(os, spec)) {
                        writeln!(f, "{INDENT}{INDENT}{INDENT}{0}: self.{0}.into_with_next(),", m.name.1)?;
                    } else {
                        writeln!(f, "{INDENT}{INDENT}{INDENT}{0}: self.{0},", m.name.1)?;
                    }
                }
            }

            writeln!(f, "{INDENT}{INDENT}}}")?;
            writeln!(f, "{INDENT}}}")?;
            writeln!(f)?;
        }

        if has_p_next || would_have_p_next {
            if raw_lifetimes.is_empty() {
                writeln!(f, "{INDENT}pub fn tracked_uninit() -> TrackedUninitTypedStruct<{}> {{", self.name.1)?;
            } else {
                writeln!(f, "{INDENT}pub fn tracked_uninit() -> TrackedUninitTypedStruct<{}<{raw_lifetimes}>> {{", self.name.1)?;
            }
            writeln!(f, "{INDENT}{INDENT}TrackedUninitTypedStruct::<{}>::new()", self.name.1)?;
        } else {
            if raw_lifetimes.is_empty() {
                writeln!(f, "{INDENT}pub fn tracked_uninit() -> TrackedUninitPlainStruct<{}> {{", self.name.1)?;
            } else {
                writeln!(f, "{INDENT}pub fn tracked_uninit() -> TrackedUninitPlainStruct<{}<{raw_lifetimes}>> {{", self.name.1)?;
            }
            writeln!(f, "{INDENT}{INDENT}TrackedUninitPlainStruct::<{}>::new()", self.name.1)?;
        }
        writeln!(f, "{INDENT}}}")?;

        writeln!(f)?;
        if raw_lifetimes.is_empty() {
            writeln!(f, "{INDENT}pub fn uninit() -> core::mem::MaybeUninit<{}> {{", self.name.1)?;
        } else {
            writeln!(f, "{INDENT}pub fn uninit() -> core::mem::MaybeUninit<{}<{raw_lifetimes}>> {{", self.name.1)?;
        }
        if has_p_next || would_have_p_next {
            writeln!(f, "{INDENT}{INDENT}TrackedUninitTypedStruct::<{}>::new().s", self.name.1)?;
        } else {
            writeln!(f, "{INDENT}{INDENT}TrackedUninitPlainStruct::<{}>::new().s", self.name.1)?;
        }
        writeln!(f, "{INDENT}}}")?;

        if !with_p_next { // TODO: Support with_p_next?
            if let Some((start, count)) = contiguous_bools {
                if count > 1 {
                    writeln!(f)?;
                    writeln!(f, "{INDENT}pub const NUMBER_OF_BOOLS: usize = {count};")?;

                    if !has_non_bool {
                        writeln!(f)?;
                        writeln!(f, "{INDENT}pub fn all_false() -> Self {{")?;
                        writeln!(f, "{INDENT}{INDENT}Self::from_array([false; Self::NUMBER_OF_BOOLS])")?;
                        writeln!(f, "{INDENT}}}")?;
                        writeln!(f)?;
                        writeln!(f, "{INDENT}pub fn all_true() -> Self {{")?;
                        writeln!(f, "{INDENT}{INDENT}Self::from_array([true; Self::NUMBER_OF_BOOLS])")?;
                        writeln!(f, "{INDENT}}}")?;
                        writeln!(f)?;
                        // let mut result = unsafe { core::mem::MaybeUninit::<Self>::zeroed() };
                        //         let p_next: *mut _ = unsafe { addr_of_mut!((*result.as_mut_ptr()).p_next) };
                        //         unsafe { p_next.write([]]); }
                        //         let mut result = unsafe { result.assume_init() };
                        //         result.bool_slice_mut().copy_from_slice(values);
                        //         result
                        writeln!(f, "{INDENT}pub fn from_slice(values: &[bool]) -> Self {{")?;
                        if has_p_next {
                            writeln!(f, "{INDENT}{INDENT}let mut result = core::mem::MaybeUninit::<Self>::zeroed();")?;
                            writeln!(f, "{INDENT}{INDENT}let p_next: *mut _ = unsafe {{ core::ptr::addr_of_mut!((*result.as_mut_ptr()).p_next) }};")?;
                            writeln!(f, "{INDENT}{INDENT}unsafe {{ p_next.write([]); }}")?;
                            writeln!(f, "{INDENT}{INDENT}let mut result = unsafe {{ result.assume_init() }};")?;
                        } else {
                            writeln!(f, "{INDENT}{INDENT}let mut result = unsafe {{ core::mem::MaybeUninit::<Self>::zeroed().assume_init() }};")?;
                        }
                        writeln!(f, "{INDENT}{INDENT}result.bool_slice_mut().copy_from_slice(values);")?;
                        writeln!(f, "{INDENT}{INDENT}result")?;
                        writeln!(f, "{INDENT}}}")?;
                        writeln!(f)?;
                        writeln!(f, "{INDENT}pub fn from_array(values: [bool; {count}]) -> Self {{")?;
                        writeln!(f, "{INDENT}{INDENT}Self::from_slice(values.as_slice())")?;
                        writeln!(f, "{INDENT}}}")?;
                    }

                    writeln!(f)?;
                    writeln!(f, "{INDENT}pub fn bool_slice(&self) -> &[bool] {{")?;
                    writeln!(f, "{INDENT}{INDENT}unsafe {{ std::slice::from_raw_parts(&self.{} as *const _, Self::NUMBER_OF_BOOLS) }}", self.members[start].name.1)?;
                    writeln!(f, "{INDENT}}}")?;
                    writeln!(f)?;
                    writeln!(f, "{INDENT}pub fn bool_slice_mut(&mut self) -> &mut [bool] {{")?;
                    writeln!(f, "{INDENT}{INDENT}unsafe {{ std::slice::from_raw_parts_mut(&mut self.{} as *mut _, Self::NUMBER_OF_BOOLS) }}", self.members[start].name.1)?;
                    writeln!(f, "{INDENT}}}")?;
                    writeln!(f)?;
                    writeln!(f, "{INDENT}pub fn into_bools(self) -> [bool; {count}] {{")?;
                    writeln!(f, "{INDENT}{INDENT}unsafe {{ std::mem::transmute_copy(&self.{}) }}", self.members[start].name.1)?;
                    writeln!(f, "{INDENT}}}")?;
                }
            }
        }

        writeln!(f, "}}")?;
        writeln!(f)?;

        Ok(f)
    }
}

const HARDCODED_ACCELERATION_STRUCTURE_INSTANCE_KHR: &str = r#"use c2rust_bitfields::BitfieldStruct;

#[repr(C)]
#[derive(BitfieldStruct)]
#[derive(Debug, Copy, Clone)]
pub struct RawAccelerationStructureInstanceKHR {
    pub transform: RawTransformMatrixKHR,
    // pub instance_custom_index: u32,
    // pub mask: u32,
    #[bitfield(name = "instance_custom_index", ty = "u32", bits = "0..=24")]
    #[bitfield(name = "mask", ty = "u32", bits = "0..=8")]
    instance_custom_index_mask: [u8; 4],
    // pub instance_shader_binding_table_record_offset: u32,
    // pub flags: GeometryInstanceFlagsKHR,
    #[bitfield(name = "instance_shader_binding_table_record_offset", ty = "u32", bits = "0..=24")]
    #[bitfield(name = "flags", ty = "GeometryInstanceFlagsKHR", bits = "0..=8")]
    instance_shader_binding_table_record_offset_flags: [u8; 4],
    pub acceleration_structure_reference: u64,
}

pub type RawAccelerationStructureInstanceNV = RawAccelerationStructureInstanceKHR;

impl PlainStructure for RawAccelerationStructureInstanceKHR {}

impl RawAccelerationStructureInstanceKHR {
    pub fn new(transform: TransformMatrixKHR,
               instance_custom_index: u32,
               mask: u32,
               instance_shader_binding_table_record_offset: u32,
               flags: GeometryInstanceFlagsKHR,
               acceleration_structure_reference: u64,) -> Self {
        let mut result = RawAccelerationStructureInstanceKHR {
            transform: transform.into_raw(),
            instance_custom_index_mask: [0; 4],
            instance_shader_binding_table_record_offset_flags: [0; 4],
            acceleration_structure_reference,
        };

        result.set_instance_custom_index(instance_custom_index);
        result.set_mask(mask);
        result.set_instance_shader_binding_table_record_offset(instance_shader_binding_table_record_offset);
        result.set_flags(flags);

        result
    }
}"#;

const HARDCODED_ACCELERATION_STRUCTURE_SRT_MOTION_INSTANCE_NV: &str = r#"
#[repr(C)]
#[derive(BitfieldStruct)]
#[derive(Debug, Copy, Clone)]
pub struct RawAccelerationStructureSRTMotionInstanceNV {
    pub transform_t0: RawSRTDataNV,
    pub transform_t1: RawSRTDataNV,
    // pub instance_custom_index: u32,
    // pub mask: u32,
    #[bitfield(name = "instance_custom_index", ty = "u32", bits = "0..=24")]
    #[bitfield(name = "mask", ty = "u32", bits = "0..=8")]
    instance_custom_index_mask: [u8; 4],
    // pub instance_shader_binding_table_record_offset: u32,
    // pub flags: GeometryInstanceFlagsKHR,
    #[bitfield(name = "instance_shader_binding_table_record_offset", ty = "u32", bits = "0..=24")]
    #[bitfield(name = "flags", ty = "GeometryInstanceFlagsKHR", bits = "0..=8")]
    instance_shader_binding_table_record_offset_flags: [u8; 4],
    pub acceleration_structure_reference: u64,
}

impl PlainStructure for RawAccelerationStructureSRTMotionInstanceNV {}

impl RawAccelerationStructureSRTMotionInstanceNV {
    pub fn new(transform_t0: SRTDataNV,
               transform_t1: SRTDataNV,
               instance_custom_index: u32,
               mask: u32,
               instance_shader_binding_table_record_offset: u32,
               flags: GeometryInstanceFlagsKHR,
               acceleration_structure_reference: u64,) -> Self {
        let mut result = RawAccelerationStructureSRTMotionInstanceNV {
            transform_t0: transform_t0.into_raw(),
            transform_t1: transform_t1.into_raw(),
            instance_custom_index_mask: [0; 4],
            instance_shader_binding_table_record_offset_flags: [0; 4],
            acceleration_structure_reference,
        };

        result.set_instance_custom_index(instance_custom_index);
        result.set_mask(mask);
        result.set_instance_shader_binding_table_record_offset(instance_shader_binding_table_record_offset);
        result.set_flags(flags);

        result
    }
}
"#;

const HARDCODED_ACCELERATION_STRUCTURE_MATRIX_MOTION_INSTANCE_NV: &str = r#"
#[repr(C)]
#[derive(BitfieldStruct)]
#[derive(Debug, Copy, Clone)]
pub struct RawAccelerationStructureMatrixMotionInstanceNV {
    pub transform_t0: RawTransformMatrixKHR,
    pub transform_t1: RawTransformMatrixKHR,
    // pub instance_custom_index: u32,
    // pub mask: u32,
    #[bitfield(name = "instance_custom_index", ty = "u32", bits = "0..=24")]
    #[bitfield(name = "mask", ty = "u32", bits = "0..=8")]
    instance_custom_index_mask: [u8; 4],
    // pub instance_shader_binding_table_record_offset: u32,
    // pub flags: GeometryInstanceFlagsKHR,
    #[bitfield(name = "instance_shader_binding_table_record_offset", ty = "u32", bits = "0..=24")]
    #[bitfield(name = "flags", ty = "GeometryInstanceFlagsKHR", bits = "0..=8")]
    instance_shader_binding_table_record_offset_flags: [u8; 4],
    pub acceleration_structure_reference: u64,
}

impl PlainStructure for RawAccelerationStructureMatrixMotionInstanceNV {}

impl RawAccelerationStructureMatrixMotionInstanceNV {
    pub fn new(transform_t0: TransformMatrixKHR,
               transform_t1: TransformMatrixKHR,
               instance_custom_index: u32,
               mask: u32,
               instance_shader_binding_table_record_offset: u32,
               flags: GeometryInstanceFlagsKHR,
               acceleration_structure_reference: u64,) -> Self {
        let mut result = RawAccelerationStructureMatrixMotionInstanceNV {
            transform_t0: transform_t0.into_raw(),
            transform_t1: transform_t1.into_raw(),
            instance_custom_index_mask: [0; 4],
            instance_shader_binding_table_record_offset_flags: [0; 4],
            acceleration_structure_reference,
        };

        result.set_instance_custom_index(instance_custom_index);
        result.set_mask(mask);
        result.set_instance_shader_binding_table_record_offset(instance_shader_binding_table_record_offset);
        result.set_flags(flags);

        result
    }
}
"#;