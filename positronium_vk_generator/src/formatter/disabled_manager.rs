use std::collections::HashSet;

use crate::formatter::CName;
use crate::parser::features_extensions::RequireRemoveType;
use crate::parser::Registry;

pub struct DisabledManager {
    disabled_names: HashSet<CName>,
}

impl DisabledManager {
    pub fn new(registry: &Registry) -> DisabledManager {
        let disabled_names = registry.extensions.iter()
            .filter(|e| !e.supported.contains(&"vulkan".to_string()) || e.provisional)
            .flat_map(|e| &e.require_removes)
            .chain({
                registry.features.iter()
                    .filter(|f| !f.api.contains(&"vulkan".to_string()))
                    .flat_map(|f| &f.require_removes)
            })
            .chain({
                registry.extensions.iter()
                    .filter(|e| e.supported.contains(&"vulkan".to_string()))
                    .flat_map(|e| &e.require_removes)
                    .filter(|rr| !rr.api.is_empty() && !rr.api.contains(&"vulkan".to_string()))
            })
            .filter(|rr| rr.rr_type == RequireRemoveType::Required)
            .flat_map(|rr|
                rr.types.iter().map(|t| &t.name)
                    .chain(rr.commands.iter().map(|c| &c.name))
                    .chain(rr.enums.iter().map(|e| &e.name))
            ).cloned().collect();

        dbg!(&disabled_names);

        DisabledManager {
            disabled_names
        }
    }

    pub fn is_disabled(&self, c_name: &CName) -> bool {
        self.disabled_names.contains(c_name)
    }
}
