use std::collections::{BTreeMap, HashMap, HashSet};
use std::fmt::Display;
use std::mem::MaybeUninit;

use heck::SnakeCase;

use crate::formatter::{CName, RSName};
use crate::formatter::function_types::FunctionType;
use crate::formatter::handles::Handle;
use crate::formatter::structs_unions::{Member, StructUnion};
use crate::parser;
use crate::parser::common::{ExternSyncMode, ParameterContents};

#[derive(Debug, Clone)]
pub struct GenericParameter {
    pub name: RSName,
    pub requirements: Vec<RSName>,
    pub default: Option<RSName>,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum ParameterValueType {
    Handle,
    Integer,
    Bool32,
    Version,
    Pointer,
    EnumLike,
    Other,
}

#[derive(Debug, Clone)]
pub struct Parameter {
    pub name: (CName, RSName),
    pub type_name: (CName, RSName),
    pub full_type: (CName, RSName),
    pub full_ptr_type: RSName,
    pub pointers: Vec<Pointer>,
    pub array_sizes: Vec<(CName, RSName)>,
    pub value_optional: bool,
    pub type_lifetimes: Vec<String>,
    pub handle_lifetimes: Vec<String>,
    pub is_length_for: Vec<(usize, usize, Option<String>)>,
    pub selector: Option<CName>,
    pub selector_for_type_and_name: Option<(CName, RSName)>,
    pub slice_type: RSName,
    pub generic_parameters: Vec<GenericParameter>,
    pub needs_ptr_debug_print: bool,
    pub c_bitfield_bits: Option<u8>,
    pub v_type: ParameterValueType,
    pub extern_sync: bool,
}

impl Parameter {
    pub fn uninit(&self, in_array: bool) -> String {
        match &self.v_type {
            ParameterValueType::Handle { .. } => "Handle::null()".to_string(),
            ParameterValueType::Integer => "0".to_string(),
            ParameterValueType::Bool32 => "Bool32::False".to_string(),
            ParameterValueType::Version => "Version::new(0,0,0,0)".to_string(),
            ParameterValueType::Pointer => {
                if self.pointers[1].optional {
                    "None"
                } else {
                    if self.pointers[1].ty == PointerType::Mut {
                        "core::ptr::null_mut()"
                    } else {
                        "core::ptr::null()"
                    }
                }.to_string()
            }
            ParameterValueType::EnumLike => format!("{}(0)", self.type_name.1),
            ParameterValueType::Other => if in_array {
                format!("{}::uninit()", self.type_name.1.trim_start_matches("Raw"))
            } else {
                format!("{}::tracked_uninit()", self.type_name.1.trim_start_matches("Raw"))
            },
        }
    }
}

#[derive(Debug, Clone)]
pub struct Pointer {
    pub ty: PointerType,
    pub len: Option<(CName, RSName, Option<String>)>,
    pub optional: bool,
    pub lifetime_name: Option<String>,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum PointerType {
    Const,
    Mut,
}

impl Pointer {
    pub fn without_lt(&self) -> &'static str {
        if self.lifetime_name.is_some() {
            match &self.ty {
                PointerType::Const => "&",
                PointerType::Mut => "&mut ",
            }
        } else {
            match &self.ty {
                PointerType::Const => "*const ",
                PointerType::Mut => "*mut ",
            }
        }
    }
}

impl Display for Pointer {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(lifetime_name) = &self.lifetime_name {
            match &self.ty {
                PointerType::Const => write!(f, "&'f_{} ", lifetime_name),
                PointerType::Mut => write!(f, "&'f_{} mut ", lifetime_name)
            }
        } else {
            match &self.ty {
                PointerType::Const => write!(f, "*const "),
                PointerType::Mut => write!(f, "*mut ")
            }
        }
    }
}

pub fn format_parameters<'a, I: Iterator<Item=&'a parser::common::Parameter> + Clone>(owner_name: CName, parameters: I, union: bool, function: bool, returned_only: bool, use_handle_ref: bool, rs_name_map: &mut HashMap<CName, RSName>, handles: &Vec<Handle>, function_types: &Vec<FunctionType>, int_alias_types: &Vec<RSName>, enum_like: &HashSet<RSName>) -> Vec<Parameter> {
    let handle_map: HashMap<CName, &Handle> = handles.iter().map(|h| (h.name.0.clone(), h)).collect();

    let member_c_names: HashSet<_> = parameters.clone().map(|m| {
        assert_eq!(1, m.contents.iter().filter(|c| if let ParameterContents::Name(_) = &c { true } else { false }).count());
        assert_eq!(1, m.contents.iter().filter(|c| if let ParameterContents::Type(_) = &c { true } else { false }).count());

        m.contents.iter().find_map(|c| if let ParameterContents::Name(s) = c { Some(s) } else { None }).unwrap().clone()
    }).collect();

    let mut parameters: Vec<Parameter> = parameters.clone().map(|m| {
        let (c_mem_name, name_index) = m.contents.iter().enumerate().find_map(|(i, c)| if let ParameterContents::Name(s) = &c { Some((s.clone(), i)) } else { None }).unwrap();
        let rs_mem_name = {
            let mut rs_mem_name = c_mem_name.to_snake_case();

            if &owner_name != "RawAccelerationStructureBuildGeometryInfoKHR"
                && &owner_name != "RawAccelerationStructureTrianglesOpacityMicromapEXT"
                && &owner_name != "RawMicromapBuildInfoEXT"
                && &rs_mem_name != "p_next" {
                rs_mem_name = rs_mem_name
                    // .trim_start_matches("s_")
                    .trim_start_matches("p_")
                    .trim_start_matches("pp_")
                    .to_string();
            }

            if rs_mem_name == "type" {
                "e_type".to_string() // TODO: Is this best?
            } else {
                rs_mem_name
            }
        };

        let rs_mem_name = global_conversion(&rs_mem_name);

        let (c_type_name, _type_name_index) = m.contents.iter().enumerate().find_map(|(i, c)| if let ParameterContents::Type(s) = &c {
            Some((match m.valid_structs.as_slice() {
                [] => s.clone(),
                [v] => v.clone(),
                [v, ..] => {
                    // TODO: Need to handle this case, not sure how best though
                    s.clone()
                }
            }, i))
        } else { None }).unwrap();

        let c_pre_type_extra = m.contents[0..name_index].iter().filter_map(|c| match c {
            ParameterContents::Code(s) => Some(s.as_str()),
            _ => None
        }).collect::<String>();

        let mut pointers: Vec<_> = c_pre_type_extra.replace(" ", "").replace("struct", "").replace("const*", "#").replace("const", "").chars().enumerate().map(|(i, c)| {
            let ty = match c {
                '*' => PointerType::Mut,
                '#' => {
                    if &owner_name == "RawSurfaceProtectedCapabilitiesKHR" {
                        PointerType::Mut
                    } else {
                        PointerType::Const
                    }
                },
                _ => unreachable!()
            };

            let len = m.alt_len.get(i).or_else(|| m.len.get(i)).map(|c_len| {
                if c_len == "1" {
                    None
                } else {
                    let (rs_len, parameter) = if member_c_names.contains(c_len) {
                        (global_conversion(c_len.to_snake_case()), None)
                    } else {
                        let mut rs_len = c_len.clone();

                        for s in member_c_names.iter().filter(|s| c_len.contains(s.as_str())) {
                            rs_len = rs_len.replace(s, global_conversion(s.to_snake_case()).as_str());
                        }

                        for (c_name, rs_name) in rs_name_map.iter() {
                            if let Some(start) = rs_len.find(c_name) {
                                if rs_len.chars().nth(start - 1).map_or(true, |c| !c.is_alphabetic()) && rs_len.chars().nth(start + c_name.len()).map_or(true, |c| !c.is_alphabetic()) {
                                    rs_len.replace_range(start..start + c_name.len(), rs_name);
                                }
                            }
                        }

                        rs_len.clone().split_once("->").map_or((rs_len, None), |(a, b)| (a.to_string(), Some(b.to_string().to_snake_case())))

                        // rs_len
                    };

                    Some((c_len.clone(), rs_len, parameter))
                }
            }).flatten();

            let optional = m.optional.get(i).map_or(i == 0 && returned_only, |b| *b) ||
                parameters.clone().any(|m| {
                    let c_mem_name = m.contents.iter().find_map(|c| if let ParameterContents::Name(s) = &c { Some(s.clone()) } else { None }).unwrap();

                    len.iter().any(|l| l.0.contains(&c_mem_name) && m.optional.contains(&true))
                });

            Pointer {
                ty,
                len,
                // Note: returned_only -> top level are optional
                optional,
                lifetime_name: None,
            }
        }).collect();

        if owner_name.starts_with("RawExportMetal") && rs_mem_name == "p_next" {
            if pointers[0].ty == PointerType::Mut {
                dbg!("Can remove this hack now");
            }
            pointers[0].ty = PointerType::Mut;
        }

        // Note: returned_only -> top level are optional
        let value_optional = m.optional.get(pointers.len()).map_or(false /*pointers.is_empty()*//* && returned_only*/, |b| *b);

        let c_array_sizes: Vec<_> = if let Some((array_start, s)) = m.contents[name_index + 1..].iter().enumerate().find_map(|(i, c)| if let ParameterContents::Code(s) = c { if s.starts_with('[') { Some((name_index + 1 + i, s)) } else { None } } else { None }) {
            if s.ends_with(']') {
                s.split_terminator(']').map(|s| s.trim_start_matches('[').to_string()).collect()
            } else {
                let array_end = m.contents[array_start + 1..].iter().enumerate().find_map(|(i, c)| if let ParameterContents::Code(s) = c { if s == "]" { Some(array_start + 1 + i) } else { None } } else { None }).expect("No ']'");

                m.contents[array_start + 1..array_end].iter().map(|c| match c {
                    ParameterContents::Code(s) | ParameterContents::Type(s) | ParameterContents::Name(s) | ParameterContents::Enum(s) => s.as_str()
                }).collect::<String>().split("][").map(|s| s.to_string()).collect()
            }
        } else {
            Vec::new()
        };

        let mut rs_array_sizes: Vec<_> = c_array_sizes.iter().rev().map(|s| {
            if s.parse::<usize>().is_ok() {
                s.clone()
            } else {
                rs_name_map.get(s).map(|s| s.clone()/* + " as usize"*/).expect(&format!("Array size is neither usize or an enum: {c_array_sizes:?}"))
            }
        }).collect();

        // Fixed size arrays passed into functions in c are actually just pointers for C ABI simplicity or something
        // but just he first one, a pointer to a fixed size array remains the same
        // Assuming arrays are always top level
        // Assuming arrays are const, since the only ones that uses this are
        if function && !rs_array_sizes.is_empty() {
            let s = rs_array_sizes.remove(0);

            pointers.insert(0, Pointer {
                ty: PointerType::Const,
                len: Some((c_array_sizes[0].clone(), s, None)),
                optional: false,
                lifetime_name: None,
            });
        }

        // if variant != Variant::Union {
        let ptr_count = pointers.len();
        for (i, p) in pointers.iter_mut().enumerate() {
            p.lifetime_name = match ptr_count {
                1 => {
                    if union && p.ty == PointerType::Mut {
                        // TODO: Would be nice to be able to mut reference here, rather than forcing mut ptr, but oh well
                        None
                    } else {
                        Some(format!("{}", rs_mem_name))
                    }
                }
                _ => Some(format!("{}_{}", rs_mem_name, i))
            }
        }
        // }


        let /*mut*/ rs_type_name = if let Some(handle) = handle_map.get(&c_type_name) {
            if use_handle_ref {
                if m.extern_sync.as_ref().map_or(false, |e| if let ExternSyncMode::Bool(v) = e { *v } else { false }) {
                    format!("{}MutHandle", handle.name.1)
                } else {
                    format!("{}Handle", handle.name.1)
                }
            } else {
                if m.extern_sync.as_ref().map_or(false, |e| if let ExternSyncMode::Bool(v) = e { *v } else { false }) {
                    format!("{}MutRaw", handle.name.1)
                } else {
                    format!("{}Raw", handle.name.1)
                }
            }
        } else if rs_mem_name.contains("version") && if let Some(n) = rs_name_map.get(&c_type_name) { n == "u32" } else { false } {
            "Version".to_string()
        } else {
            rs_name_map.get(&c_type_name).expect(&format!("No recorded RS Name for: {}", c_type_name)).clone()
        };

        let rs_type_name = global_conversion(rs_type_name);

        // dbg!((&c_name, &c_type_name, &rs_type_name, &c_pre_type_extra, &pointers, &value_optional, &m.len, &m.alt_len, &c_array_sizes, &rs_array_sizes));

        let c_type = m.contents.iter().filter_map(|c| match c {
            ParameterContents::Code(s) /* | ParameterContents::Name(s) */ | ParameterContents::Enum(s) => Some(s.as_str()),
            ParameterContents::Type(s) => {
                match m.valid_structs.as_slice() {
                    [] => Some(s.as_str()),
                    [v] => Some(v.as_str()),
                    [v, ..] => {
                        // TODO: Need to handle this case, not sure how best though
                        Some(s.as_str())
                    }
                }
            }
            _ => None,
        }).collect::<String>();

        // let rs_type = {
        //     let mut rs_type = rs_type_name.clone();
        //
        //     for (level, ptr) in pointers.iter().rev().enumerate() {
        //         rs_type = ptr.ty.format(field,level) + &rs_type;
        //         if ptr.optional {
        //             rs_type = "Option<".to_string() + &rs_type + ">";
        //         }
        //     }
        //
        //     for sz in &rs_array_sizes {
        //         rs_type = "[".to_string() + &rs_type + "; " + sz + "]";
        //     }
        //
        //     rs_type
        // };

        // if *returned_only && !pointers.is_empty() && c_mem_name != "pNext" {
        //     dbg!(&c_name);
        // }

        let needs_ptr_debug_print = function_types.iter().any(|f| &f.name.0 == &c_type_name);

        let mut generic_parameters = function_types.iter().find(|f| &f.name.0 == &c_type_name).map_or(Vec::new(), |f| f.generic_parameters.clone());
        match c_type_name.as_str() {
            "VkDebugReportCallbackCreateInfoEXT" | "VkDebugUtilsMessengerCreateInfoEXT" => generic_parameters.push(GenericParameter {
                name: "D".to_string(),
                requirements: vec!["'static".to_string()],
                default: Some("()".to_string()),
            }),
            _ => {}
        }

        let extra_code = m.contents.iter().filter_map(|c| if let ParameterContents::Code(s) = c { Some(s.as_str()) } else { None }).collect::<String>();

        let c_bitfield_bits = extra_code.split_once(':').map_or(None, |(l, r)| r.parse().ok());

        let v_type = if pointers.len() >= 2 {
            ParameterValueType::Pointer
        } else if let Some(h) = handle_map.get(&c_type_name) {
            ParameterValueType::Handle
        } else if &rs_type_name == "Version" {
            ParameterValueType::Version
        } else if rs_type_name.starts_with('u')
            || rs_type_name.starts_with('i')
            || rs_type_name.ends_with("c_int")
            || rs_type_name.ends_with("c_char")
            || int_alias_types.contains(&rs_type_name) {
            ParameterValueType::Integer
        } else if &rs_type_name == "Bool32" {
            ParameterValueType::Bool32
        } else if enum_like.contains(&rs_type_name) {
            ParameterValueType::EnumLike
        } else {
            ParameterValueType::Other
        };

        Parameter {
            name: (c_mem_name, rs_mem_name),
            type_name: (c_type_name, rs_type_name),
            full_type: (c_type, "".to_string()),
            full_ptr_type: "".to_string(),
            pointers,
            array_sizes: c_array_sizes.into_iter().zip(rs_array_sizes.into_iter()).collect(),
            value_optional,
            type_lifetimes: Vec::new(),
            handle_lifetimes: Vec::new(),
            is_length_for: Vec::new(),
            selector: m.selector.clone(),
            selector_for_type_and_name: None,
            slice_type: String::new(),
            generic_parameters,
            needs_ptr_debug_print,
            c_bitfield_bits,
            v_type,
            extern_sync: m.extern_sync.as_ref().map_or(false, |e| if let ExternSyncMode::Bool(v) = e { *v } else { false })
        }
    }).collect();

    for index in 0..parameters.len() {
        let (pre, later) = parameters.split_at_mut(index);
        let pre_len = pre.len();
        let (m, later) = later.split_at_mut(1);
        let m = &mut m[0];

        let other_members = pre.iter().chain(later.iter());

        m.is_length_for = other_members.clone().enumerate().flat_map(|(i, om)| om.pointers.iter().enumerate().filter_map(|(j, p)| p.len.as_ref().map(|l| (j, l))).filter_map(|(j, p)| if /*&p.0 == &m.name.0*/ p.0.contains(&m.name.0) { Some((j, p.2.clone())) } else { None }).map(move |(j, p)|
            if i >= pre_len {
                (i + 1, j, p)
            } else {
                (i, j, p)
            }
        )).collect();

        m.selector_for_type_and_name = other_members.clone().find_map(|om| if om.selector.as_ref().map_or(false, |om_s| om_s == &m.name.0) { Some((om.type_name.0.clone(), om.name.1.clone())) } else { None });
    }

    parameters
}

pub fn format_parameters_2<'a, I: Iterator<Item=&'a mut Parameter>>(owner_name: CName, parameters: I, structs_unions: &BTreeMap<CName, StructUnion>, handles: &Vec<Handle>, include_lifetimes: bool, is_function: bool, returned_only: bool) {
    let mut parameters: Vec<_> = parameters.collect();

    let handle_map: HashMap<CName, &Handle> = handles.iter().map(|h| (h.name.0.clone(), h)).collect();

    fn get_member_type_lifetimes(m: &Parameter, others: &BTreeMap<CName, StructUnion>) -> Vec<String> {
        if let Some(other) = others.get(&m.type_name.0) {
            other.members.iter()
                .flat_map(|om|
                              om.pointers.iter().map(|p| p.lifetime_name.as_ref()).flatten().map(move |l| format!("{}_{}", m.name.1, l))
                                  .chain(get_member_type_lifetimes(om, others).into_iter().map(move |l| format!("{}_{}", m.name.1, l)))
                          //.chain(handle_map.get(&om.type_name.0).iter().map(|h| std::iter::once( h.name.1.to_snake_case().as_str()).chain(h.lifetimes_names.iter().map(|l| l.as_str()))) )
                ).collect()
        } else {
            Vec::new()
        }
    }

    fn get_member_handle_lifetimes(m: &Parameter, others: &BTreeMap<CName, StructUnion>, handle_map: &HashMap<CName, &Handle>, returned_only: bool) -> Vec<String> {
        handle_map.get(&m.type_name.0).into_iter().flat_map(|h| std::iter::once(h.name.1.to_snake_case()).filter(|_| !returned_only).chain(h.lifetimes_names.iter().map(|l| l.clone())))
            .chain(others.get(&m.type_name.0).into_iter().flat_map(|other| other.members.iter().flat_map(|om| get_member_handle_lifetimes(om, others, handle_map, other.returned_only))))
            .collect()
    }

    for index in 0..parameters.len() {
        let (pre, later) = parameters.split_at_mut(index);
        let pre_len = pre.len();
        let (m, later) = later.split_at_mut(1);
        let m = &mut m[0];

        let other_members = pre.iter().chain(later.iter());

        m.type_lifetimes = get_member_type_lifetimes(m, &structs_unions);

        m.handle_lifetimes = get_member_handle_lifetimes(m, &structs_unions, &handle_map, returned_only);
        m.handle_lifetimes.sort_unstable();
        m.handle_lifetimes.dedup();

        let (rs_type, rs_slice_type, ptr_type) = {
            let mut rs_type = match m.name.1.as_str() {
                "p_next" if m.pointers[0].ty == PointerType::Const => "NextPtr".to_string(),
                "p_next" if m.pointers[0].ty == PointerType::Mut => "NextMutPtr".to_string(),
                "s_type" => "SType<Self>".to_string(),
                _ => m.type_name.1.clone()
            };

            if returned_only && rs_type.ends_with("Handle") {
                rs_type = rs_type.replace("Handle", "Raw");
            }

            let mut rs_slice_type = rs_type.clone();
            let mut ptr_type = rs_type.clone();

            if (!m.type_lifetimes.is_empty() || !m.handle_lifetimes.is_empty() || !m.generic_parameters.is_empty()) && include_lifetimes {
                let l = m.handle_lifetimes.iter().map(|l| format!("'{}", l)).chain(m.type_lifetimes.iter().map(|l| format!("'f_{}", l))).chain(m.generic_parameters.iter().map(|g| g.name.clone())).collect::<Vec<String>>().join(", ");
                rs_type = format!("{}<{}>", rs_type, l);
                rs_slice_type = format!("{}<{}>", rs_slice_type, l);
                ptr_type = format!("{}<{}>", ptr_type, l);
            }

            for ptr in m.pointers.iter().rev() {
                if let Some((_c_len, rs_len, _)) = &ptr.len {
                    if rs_len == "1" {
                        // Single ptr

                        if &m.name.0 == "pNext" {
                            if include_lifetimes {
                                rs_type = format!("Option<{}<'f_{}>>", rs_type, ptr.lifetime_name.as_ref().unwrap());
                                rs_slice_type = format!("Option<{}<'f_{}>>", rs_slice_type, ptr.lifetime_name.as_ref().unwrap());
                            } else {
                                rs_type = format!("Option<{}>", rs_type);
                                rs_slice_type = format!("Option<{}>", rs_slice_type);
                            }
                        } else {
                            if include_lifetimes {
                                rs_type = format!("{}{}", ptr, rs_type);
                            } else {
                                rs_type = format!("{}{}", ptr.without_lt(), rs_type);
                            }
                            if ptr.optional {
                                rs_type = format!("Option<{}>", rs_type);
                            }

                            if include_lifetimes {
                                rs_slice_type = format!("{}{}", ptr, rs_slice_type);
                            } else {
                                rs_slice_type = format!("{}{}", ptr.without_lt(), rs_slice_type);
                            }
                            if ptr.optional {
                                rs_slice_type = format!("Option<{}>", rs_slice_type);
                            }

                            if ptr.ty == PointerType::Mut {
                                ptr_type = format!("core::ptr::NonNull<{}>", ptr_type);
                                if ptr.optional {
                                    ptr_type = format!("Option<{}>", ptr_type);
                                }
                            } else {
                                if include_lifetimes {
                                    ptr_type = format!("{}{}", ptr, ptr_type);
                                } else {
                                    ptr_type = format!("{}{}", ptr.without_lt(), ptr_type);
                                }
                                if ptr.optional {
                                    ptr_type = format!("Option<{}>", ptr_type);
                                }
                            }
                        }
                    } else if rs_len == "null-terminated" {
                        // Ptr to c string

                        if include_lifetimes {
                            rs_type = format!("UnsizedCStr<{}>", ptr.lifetime_name.as_ref().map(/*"'static".to_string(),*/ |s| "'f_".to_string() + s).unwrap());
                        } else {
                            rs_type = "UnsizedCStr".to_string();
                        }
                        if ptr.optional {
                            rs_type = format!("Option<{}>", rs_type);
                        }

                        if include_lifetimes {
                            rs_slice_type = format!("UnsizedCStr<{}>", ptr.lifetime_name.as_ref().map(/*"'static".to_string(),*/ |s| "'f_".to_string() + s).unwrap());
                        } else {
                            rs_slice_type = "UnsizedCStr".to_string();
                        }
                        if ptr.optional {
                            rs_slice_type = format!("Option<{}>", rs_slice_type);
                        }

                        if ptr.ty == PointerType::Mut {
                            ptr_type = format!("core::ptr::NonNull<{}>", ptr_type);
                            if ptr.optional {
                                ptr_type = format!("Option<{}>", ptr_type);
                            }
                        } else {
                            if include_lifetimes {
                                ptr_type = format!("UnsizedCStr<{}>", ptr.lifetime_name.as_ref().map(/*"'static".to_string(),*/ |s| "'f_".to_string() + s).unwrap());
                            } else {
                                ptr_type = "UnsizedCStr".to_string();
                            }
                            if ptr.optional {
                                ptr_type = format!("Option<{}>", ptr_type);
                            }
                        }
                    } else if other_members.clone().any(|om| rs_len.contains(&om.name.1)) {
                        // Ptr to array with length coming from another member (or expression related to other member)
                        //TODO:

                        if rs_len.chars().any(|c| c.is_numeric() || (!c.is_alphabetic() && c != '_')) {
                            // dbg!(&rs_len);
                        }

                        if include_lifetimes {
                            rs_type = format!("{}{}", ptr, rs_type);
                        } else {
                            rs_type = format!("{}{}", ptr.without_lt(), rs_type);
                        }
                        if ptr.optional {
                            rs_type = format!("Option<{}>", rs_type);
                        }

                        if include_lifetimes {
                            if ptr.optional && !is_function && returned_only {
                                if ptr.ty == PointerType::Mut {
                                    rs_slice_type = format!("OptionalSliceMut<'f_{}, {}>", ptr.lifetime_name.as_ref().unwrap(), rs_slice_type);
                                } else {
                                    rs_slice_type = format!("OptionalSlice<'f_{}, {}>", ptr.lifetime_name.as_ref().unwrap(), rs_slice_type);
                                }
                            } else {
                                rs_slice_type = format!("{}[{}]", ptr, rs_slice_type);
                            }
                        } else {
                            if ptr.optional && !is_function && returned_only {
                                if ptr.ty == PointerType::Mut {
                                    rs_slice_type = format!("OptionalSliceMut<{}>", rs_slice_type);
                                } else {
                                    rs_slice_type = format!("OptionalSlice<{}>", rs_slice_type);
                                }
                            } else {
                                rs_slice_type = format!("{}[{}]", ptr.without_lt().trim(), rs_slice_type);
                            }
                        }
                        // if ptr.optional { // Can just use 0 size slice? `&[]`
                        //     rs_slice_type = format!("Option<{}>", rs_slice_type);
                        // }


                        if ptr.ty == PointerType::Mut {
                            ptr_type = format!("core::ptr::NonNull<{}>", ptr_type);
                            if ptr.optional {
                                ptr_type = format!("Option<{}>", ptr_type);
                            }
                        } else {
                            if include_lifetimes {
                                ptr_type = format!("{}{}", ptr, ptr_type);
                            } else {
                                ptr_type = format!("{}{}", ptr.without_lt(), ptr_type);
                            }
                            if ptr.optional {
                                ptr_type = format!("Option<{}>", ptr_type);
                            }
                        }
                    } else {
                        // Otherwise: Length must come from a constant

                        let const_sized = match &ptr.ty {
                            PointerType::Const => "ConstSizePtr",
                            PointerType::Mut => "ConstSizeMutPtr"
                        };

                        if rs_len.parse::<usize>().is_err() {
                            if include_lifetimes {
                                rs_type = format!("{}<'f_{}, {}, {{ {} }}>", const_sized, ptr.lifetime_name.as_ref().unwrap(), rs_type, rs_len);
                            } else {
                                rs_type = format!("{}<{}, {{ {} }}>", const_sized, rs_type, rs_len);
                            }
                            if ptr.optional {
                                rs_type = format!("Option<{}>", rs_type);
                            }

                            if include_lifetimes {
                                rs_slice_type = format!("{}<'f_{}, {}, {{ {} }}>", const_sized, ptr.lifetime_name.as_ref().unwrap(), rs_slice_type, rs_len);
                            } else {
                                rs_slice_type = format!("{}<{}, {{ {} }}>", const_sized, rs_slice_type, rs_len);
                            }
                            if ptr.optional {
                                rs_slice_type = format!("Option<{}>", rs_slice_type);
                            }

                            if include_lifetimes {
                                ptr_type = format!("{}<'f_{}, {}, {{ {} }}>", const_sized, ptr.lifetime_name.as_ref().unwrap(), ptr_type, rs_len);
                            } else {
                                ptr_type = format!("{}<{}, {{ {} }}>", const_sized, ptr_type, rs_len);
                            }
                            if ptr.optional {
                                ptr_type = format!("Option<{}>", ptr_type);
                            }
                        } else {
                            if include_lifetimes {
                                rs_type = format!("{}<'f_{}, {}, {}>", const_sized, ptr.lifetime_name.as_ref().unwrap(), rs_type, rs_len);
                            } else {
                                rs_type = format!("{}<{}, {}>", const_sized, rs_type, rs_len);
                            }
                            if ptr.optional {
                                rs_type = format!("Option<{}>", rs_type);
                            }

                            if include_lifetimes {
                                rs_slice_type = format!("{}<'f_{}, {}, {}>", const_sized, ptr.lifetime_name.as_ref().unwrap(), rs_slice_type, rs_len);
                            } else {
                                rs_slice_type = format!("{}<{}, {}>", const_sized, rs_slice_type, rs_len);
                            }
                            if ptr.optional {
                                rs_slice_type = format!("Option<{}>", rs_slice_type);
                            }

                            if include_lifetimes {
                                ptr_type = format!("{}<'f_{}, {}, {}>", const_sized, ptr.lifetime_name.as_ref().unwrap(), ptr_type, rs_len);
                            } else {
                                ptr_type = format!("{}<{}, {}>", const_sized, ptr_type, rs_len);
                            }
                            if ptr.optional {
                                ptr_type = format!("Option<{}>", ptr_type);
                            }
                        }
                    }
                } else {
                    // No len -> Single ptr


                    if &m.name.0 == "pNext" {
                        if include_lifetimes {
                            rs_type = format!("Option<{}<'f_{}>>", rs_type, ptr.lifetime_name.as_ref().unwrap());
                            rs_slice_type = format!("Option<{}<'f_{}>>", rs_slice_type, ptr.lifetime_name.as_ref().unwrap());
                        } else {
                            rs_type = format!("Option<{}>", rs_type);
                            rs_slice_type = format!("Option<{}>", rs_slice_type);
                        }
                    } else {
                        if include_lifetimes {
                            rs_type = format!("{}{}", ptr, rs_type);
                        } else {
                            rs_type = format!("{}{}", ptr.without_lt(), rs_type);
                        }
                        if ptr.optional {
                            rs_type = format!("Option<{}>", rs_type);
                        }

                        if include_lifetimes {
                            rs_slice_type = format!("{}{}", ptr, rs_slice_type);
                        } else {
                            rs_slice_type = format!("{}{}", ptr.without_lt(), rs_slice_type);
                        }
                        if ptr.optional {
                            rs_slice_type = format!("Option<{}>", rs_slice_type);
                        }

                        if ptr.ty == PointerType::Mut {
                            ptr_type = format!("core::ptr::NonNull<{}>", ptr_type);
                            if ptr.optional {
                                ptr_type = format!("Option<{}>", ptr_type);
                            }
                        } else {
                            if include_lifetimes {
                                ptr_type = format!("{}{}", ptr, ptr_type);
                            } else {
                                ptr_type = format!("{}{}", ptr.without_lt(), ptr_type);
                            }
                            if ptr.optional {
                                ptr_type = format!("Option<{}>", ptr_type);
                            }
                        }
                    }
                }
            }

            for sz in &m.array_sizes {
                if &rs_type == "::std::os::raw::c_char" {
                    rs_type = format!("ConstSizeCStr<{}>", sz.1);
                } else {
                    rs_type = format!("[{}; {}]", rs_type, sz.1);
                }

                if &rs_slice_type == "::std::os::raw::c_char" {
                    rs_slice_type = format!("ConstSizeCStr<{}>", sz.1);
                } else {
                    rs_slice_type = format!("[{}; {}]", rs_slice_type, sz.1);
                }

                if &ptr_type == "::std::os::raw::c_char" {
                    ptr_type = format!("ConstSizeCStr<{}>", sz.1);
                } else {
                    ptr_type = format!("[{}; {}]", ptr_type, sz.1);
                }
            }

            if m.pointers.len() == 1 {
                if m.extern_sync || (structs_unions.contains_key(&m.type_name.0) && structs_unions.get(&m.type_name.0).unwrap().members.iter().any(|m| m.extern_sync)) {
                    if !include_lifetimes {
                        rs_type = rs_type.replace("&", "&mut ");
                        if rs_slice_type.starts_with("OptionalSlice") {
                            rs_slice_type = rs_slice_type.replace("OptionalSlice", "OptionalSliceMut");
                        } else {
                            rs_slice_type = rs_slice_type.replace("&", "&mut ");
                        }
                        ptr_type = ptr_type.replace("&", "");
                        if ptr_type.starts_with("Option<") {
                            ptr_type = format!("Option<core::ptr::NonNull<{}>", ptr_type.trim_start_matches("Option<"));
                        } else {
                            ptr_type = format!("core::ptr::NonNull<{}>", ptr_type);
                        }
                    } else if rs_type.contains("&") && !rs_type.contains(" mut ") {
                        rs_type = rs_type.replacen(' ', " mut ", 1);

                        if rs_slice_type.starts_with("OptionalSlice") {
                            rs_slice_type = rs_slice_type.replace("OptionalSlice", "OptionalSliceMut");
                        } else {
                            rs_slice_type = rs_slice_type.replacen(' ', " mut ", 1);
                        }
                        // ptr_type = ptr_type.replacen(' ', " mut ", 1);
                    } else {
                        rs_slice_type = rs_slice_type.replace("OptionalSlice", "OptionalSliceMut");
                    }
                }
            }

            rs_type = rs_type.replacen("&mut &mut", "&mut *mut", 1);
            if ptr_type.starts_with("core::ptr::NonNull<core::ptr::NonNull<") {
                ptr_type = ptr_type.replacen("core::ptr::NonNull<", "", 2);
                ptr_type = ptr_type.replacen(">", "", 2);
                ptr_type = format!("core::ptr::NonNull<*mut {}>", ptr_type);
            }

            (rs_type, rs_slice_type, ptr_type)
        };

        m.full_type.1 = rs_type;
        m.full_ptr_type = ptr_type;
        m.slice_type = rs_slice_type;
    }
}

pub fn global_conversion<S: AsRef<str>>(s: S) -> String {
    let s = s.as_ref();
    #[cfg(feature = "non_american_english")]
        let s = non_american_english(s);
    s.to_owned()
}

#[cfg(feature = "non_american_english")]
fn non_american_english<S: AsRef<str>>(s: S) -> String {
    s.as_ref()
        .replace("color", "colour")
        .replace("Color", "Colour")
        .replace("COLOR", "COLOUR")
        .replace("normalize", "normalise")
}