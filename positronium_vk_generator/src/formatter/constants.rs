use std::collections::{BTreeMap, HashMap};
use std::fmt::{Display, Formatter};

use heck::ShoutySnakeCase;

use crate::formatter::{CName, Named, RSName, trim_unneeded_parenthesis, trim_vk_prefix};
use crate::formatter::common::global_conversion;
use crate::parser::enums::{EnumTypes, EnumVariantType};
use crate::parser::features_extensions::{EnumExtensionType, EnumType, RequireRemoveType};
use crate::parser::Registry;

#[derive(Debug)]
enum ConstantType {
    String,
    UnsizedCStr,
    Float,
    UInteger,
    ULongInteger,
    ISize,
    USize,
}

impl Display for ConstantType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ConstantType::String => write!(f, "&'static str"),
            ConstantType::UnsizedCStr => write!(f, "UnsizedCStr"),
            ConstantType::Float => write!(f, "f32"),
            ConstantType::UInteger => write!(f, "u32"),
            ConstantType::ULongInteger => write!(f, "u64"),
            ConstantType::ISize => write!(f, "isize"),
            ConstantType::USize => write!(f, "usize"),
        }
    }
}

#[derive(Debug)]
pub struct Constant {
    name: (CName, RSName),
    // macro_call: RSName,
    value: String,
    value_type: ConstantType,
    aliases: Vec<(CName, RSName)>,
    platform: Option<String>,
}

impl Named for Constant {
    fn get_names(&self) -> Vec<(&CName, &RSName)> {
        core::iter::once((&self.name.0, &self.name.1))
            // core::iter::once((&self.name.0, &self.macro_call))
            .chain(self.aliases.iter().map(|(c, rs)| (c, rs)))
            .collect()
    }
}

pub fn format_constants(registry: &Registry) -> Vec<Constant> {
    //TODO: Remove special constants and handle specially? e.g. True/False and WholeSize

    let mut constants: BTreeMap<_, _> = registry.enums.iter().filter(|e| e.enum_type == EnumTypes::Constant).flat_map(|e| {
        let mut variant_aliases = HashMap::new();

        let mut variants: BTreeMap<CName, Constant> = e.variants.iter().filter_map(|v| {
            let c_name = v.name.clone();
            let rs_name = format_constant(&c_name);

            // let macro_call = format!("{}!()", rs_name);

            match &v.variant_type {
                EnumVariantType::Value { value, value_type: _ } => {
                    Some(parse_value_type(value))
                }
                EnumVariantType::Bitpos(bitpos) => {
                    let bitpos = bitpos.parse::<u32>().expect(&format!("Bitpos not integer: {}", bitpos));

                    if bitpos >= 32 {
                        Some((format!("1u64 << {}", bitpos), ConstantType::ULongInteger))
                    } else {
                        Some((format!("1u32 << {}", bitpos), ConstantType::UInteger))
                    }
                }
                EnumVariantType::Alias(alias) => {
                    let a_c_name = alias.clone();
                    let a_rs_name = format_constant(&a_c_name);

                    variant_aliases.entry((a_c_name, a_rs_name)).or_insert(Vec::new()).push((c_name.clone(), rs_name.clone()));

                    None
                }
            }.map(|(value, value_type)| (c_name.clone(), Constant {
                name: (c_name, rs_name),
                // macro_call,
                value,
                value_type,
                aliases: vec![],
                platform: None,
            }))
        }).collect();

        for ((c_name, _), aliases) in variant_aliases {
            variants.get_mut(&c_name).unwrap().aliases = aliases;
        }

        variants
    }).collect();

    registry.features.iter().map(|f| (&f.require_removes, None, None)).chain(registry.extensions.iter().filter(|e| !e.supported.iter().any(|s| s == "disabled")).map(|r| (&r.require_removes, r.number, r.platform.as_ref()))).for_each(|(r, _number, platform)| {
        r.iter().filter(|r| r.rr_type == RequireRemoveType::Required).for_each(|r| {
            r.enums.iter().for_each(|e| {
                match &e.enum_type {
                    EnumType::Reference => {}
                    EnumType::Extension(eex) => {
                        match &eex {
                            EnumExtensionType::NumericValue { value, value_type: _, extends } => {
                                if let None = extends { // Only constants

                                    let (value, value_type) = parse_value_type(value);

                                    let c_name = e.name.clone();
                                    let rs_name = format_constant(&c_name);

                                    // let macro_call = format!("{}!()", rs_name);

                                    constants.insert(c_name.clone(), Constant {
                                        name: (c_name, rs_name),
                                        // macro_call,
                                        value,
                                        value_type,
                                        aliases: vec![],
                                        platform: platform.cloned(),
                                    });
                                }
                            }
                            EnumExtensionType::Alias { alias, extends } => {
                                if let None = extends {
                                    let c_name = e.name.clone();
                                    let rs_name = format_constant(&c_name);

                                    let a_c_name = alias.clone();

                                    constants.get_mut(&a_c_name).unwrap().aliases.push((c_name, rs_name));
                                }
                            }
                            EnumExtensionType::ValueAdded { .. } => {
                                // Ignored
                            }
                            EnumExtensionType::BitmaskValue { extends, .. } => {
                                // Ignored?
                                if extends.is_none() {
                                    panic!("Expected no global bitmask values, but got one: {}", &e.name);
                                }
                            }
                        }
                    }
                }
            })
        })
    });

    let extra: Vec<_> = constants.iter().filter_map(|(_, c)| {
        if let ConstantType::String = c.value_type {
            Some((c.name.0.clone() + "_UNSIZED_CSTR", Constant {
                name: (c.name.0.clone() + "_UNSIZED_CSTR", c.name.1.clone() + "_UNSIZED_CSTR"),
                value: format!("unsized_cstr_int!({})", c.value),
                value_type: ConstantType::UnsizedCStr,
                aliases: c.aliases.iter().map(|(a, b)| (a.clone() + "_UNSIZED_CSTR", b.clone() + "_UNSIZED_CSTR")).collect(),
                platform: c.platform.clone(),
            }))
        } else {
            None
        }
    }).collect();

    constants.extend(extra);

    constants.into_iter().map(|(_, v)| v).collect()
}

fn format_constant(s: &str) -> String {
    global_conversion(trim_vk_prefix(s).to_shouty_snake_case())
}

fn parse_value_type(value: &str) -> (String, ConstantType) {
    if value.starts_with('"') {
        (value.to_string(), ConstantType::String)
    } else {
        let value = trim_unneeded_parenthesis(value);

        if value.ends_with('f') || value.ends_with('F') {
            let value = value.trim_end_matches('f').trim_end_matches('F');
            let value = if !value.contains('.') {
                value.to_string() + ".0"
            } else {
                value.to_string()
            };
            (value, ConstantType::Float)
        } else if value.contains("ULL") {
            let value = value.replace("ULL", "u64").replace('~', "!");
            (value, ConstantType::ULongInteger)
        } else if value.contains('U') {
            let value = value.replace("U", "u32").replace('~', "!");
            (value, ConstantType::UInteger)
        } else if value.contains('-') {
            (value.to_string(), ConstantType::ISize)
        } else {
            (value.to_string(), ConstantType::USize)
        }
    }
}

impl Display for Constant {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        // if let Some(platform) = &self.platform {
        //     writeln!(f, "#[cfg(feature = \"platform_{}\")]", platform)?;
        // }
        writeln!(f, "pub const {}: {} = {};", self.name.1, self.value_type, self.value)
        // writeln!(f, "#[macro_export] macro_rules! {} {{ () => {{ {} }}; }}", self.name.1, self.value)
    }
}