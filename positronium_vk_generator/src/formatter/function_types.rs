use heck::CamelCase;

use crate::formatter::{CName, Named, RSName, trim_vk_prefix};
use crate::formatter::common::GenericParameter;
use crate::formatter::disabled_manager::DisabledManager;
use crate::parser::Registry;
use crate::parser::types::{OtherContents, Type, TypeCategories};

#[derive(Debug)]
pub struct FunctionType {
    pub name: (CName, RSName),
    pub formatted: RSName,
    pub generic_parameters: Vec<GenericParameter>,
}

impl Named for FunctionType {
    fn get_names(&self) -> Vec<(&CName, &RSName)> {
        vec![(&self.name.0, &self.name.1)]
    }
}

pub fn format_function_types(registry: &Registry, disabled_manager: &DisabledManager) -> Vec<FunctionType> {
    registry.types.iter().filter_map(|t| match t {
        Type::Other { category, contents, .. } => {
            match category {
                TypeCategories::FuncPointer => {
                    let (c_name, name_index) = contents.iter().enumerate().find_map(|(i, c)| {
                        match c {
                            OtherContents::Name(name) => Some((name.clone(), i)),
                            _ => None
                        }
                    }).unwrap();

                    if disabled_manager.is_disabled(&c_name) {
                        return None;
                    }

                    let rs_name = trim_vk_prefix(c_name.trim_start_matches("PFN_")).to_camel_case();

                    let (formatted, const_parameters) = match rs_name.as_str() {
                        "InternalAllocationNotification" => (
                            "extern \"C\" fn(user_data: Option<&'static D>, size: usize, allocation_type: RawInternalAllocationType, allocation_scope: RawSystemAllocationScope)".to_string(),
                            vec![GenericParameter { name: "D".to_string(), requirements: vec!["'static".to_string()], default: Some("()".to_string()) }]
                        ),
                        "InternalFreeNotification" => (
                            "extern \"C\" fn(user_data: Option<&'static D>, size: usize, allocation_type: RawInternalAllocationType, allocation_scope: RawSystemAllocationScope)".to_string(),
                            vec![GenericParameter { name: "D".to_string(), requirements: vec!["'static".to_string()], default: Some("()".to_string()) }]
                        ),
                        "ReallocationFunction" => (
                            "extern \"C\" fn(user_data: Option<&'static D>, original: *mut [u8; 0], size: usize, alignment: usize, allocation_scope: RawSystemAllocationScope) -> *mut [u8; 0]".to_string(),
                            vec![GenericParameter { name: "D".to_string(), requirements: vec!["'static".to_string()], default: Some("()".to_string()) }]
                        ),
                        "AllocationFunction" => (
                            "extern \"C\" fn(user_data: Option<&'static D>, size: usize, alignment: usize, allocation_scope: RawSystemAllocationScope) -> *mut [u8; 0]".to_string(),
                            vec![GenericParameter { name: "D".to_string(), requirements: vec!["'static".to_string()], default: Some("()".to_string()) }]
                        ),
                        "FreeFunction" => (
                            "extern \"C\" fn(user_data: Option<&'static D>, memory: *mut [u8; 0])".to_string(),
                            vec![GenericParameter { name: "D".to_string(), requirements: vec!["'static".to_string()], default: Some("()".to_string()) }]
                        ),
                        "VoidFunction" => (
                            "extern \"C\" fn()".to_string(),
                            vec![]
                        ),
                        "DebugReportCallbackExt" => (
                            "extern \"C\" fn(flags: DebugReportFlagsEXT, object_type: RawDebugReportObjectTypeEXT, object: u64, location: usize, message_code: i32, layer_prefix: Option<UnsizedCStr>, message: Option<UnsizedCStr>, user_data: Option<&'static D>) -> Bool32".to_string(),
                            vec![GenericParameter { name: "D".to_string(), requirements: vec!["'static".to_string()], default: Some("()".to_string()) }]
                        ),
                        "DebugUtilsMessengerCallbackExt" => (
                            "extern \"C\" fn(message_severity: RawDebugUtilsMessageSeverityFlagBitsEXT, message_types: DebugUtilsMessageTypeFlagsEXT, callback_data: Option<&RawDebugUtilsMessengerCallbackDataEXT>, user_data: Option<&'static D>) -> Bool32".to_string(),
                            vec![GenericParameter { name: "D".to_string(), requirements: vec!["'static".to_string()], default: Some("()".to_string()) }]
                        ),
                        "DeviceMemoryReportCallbackExt" => (
                            "extern \"C\" fn(callback_data: Option<&RawDeviceMemoryReportCallbackDataEXT>, user_data: Option<&'static D>)".to_string(),
                            vec![GenericParameter { name: "D".to_string(), requirements: vec!["'static".to_string()], default: Some("()".to_string()) }]
                        ),
                        "GetInstanceProcAddrLunarg" => (
                            "extern \"C\" fn(instance: InstanceRaw, name: UnsizedCStr) -> VoidFunction".to_string(),
                            vec![]
                        ),
                        _ => {
                            eprintln!("ERROR: No pre-defined rule for function pointer {}/{}, defaulting to void function.", c_name, rs_name);
                            (
                                "extern \"C\" fn()".to_string(),
                                vec![]
                            )
                        }
                    };

                    Some(FunctionType {
                        name: (c_name, rs_name),
                        formatted,
                        generic_parameters: const_parameters,
                    })
                }
                _ => None
            }
        }
        _ => None
    }).collect()
}

impl core::fmt::Display for FunctionType {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        if self.generic_parameters.is_empty() {
            writeln!(f, "pub type {} = {};", self.name.1, self.formatted)
        } else {
            let generics = self.generic_parameters.iter().map(|c| &c.name).map(|r| r.as_str()).collect::<Vec<&str>>().join(", ");
            writeln!(f, "pub type {}<{}> = {};", self.name.1, generics, self.formatted)
        }
    }
}
