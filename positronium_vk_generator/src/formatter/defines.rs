use std::collections::HashMap;
use std::fmt::{Display, Formatter};

use crate::formatter::{CName, RSName, trim_vk_prefix};
use crate::formatter::common::global_conversion;
use crate::formatter::disabled_manager::DisabledManager;
use crate::parser::features_extensions::RequireRemoveType;
use crate::parser::Registry;
use crate::parser::types::{OtherContents, Type, TypeCategories};

#[derive(Debug)]
pub struct Define {
    name: Option<(CName, RSName)>,
    last_code: String,
    aliases: Vec<RSName>
}

pub fn format_defines(registry: &Registry, disabled_manager: &DisabledManager) -> Vec<Define> {
    let mut aliases = HashMap::<_, Vec<_>>::new();

    for e in &registry.extensions {
        for r in &e.require_removes {
            if r.rr_type == RequireRemoveType::Required {
                for a in &r.define_aliases {
                    aliases.entry(&a.value).or_default().push(&a.name);
                }
            }
        }
    }

    registry.types.iter().filter_map(|t|
        match t {
            Type::Other { category, contents, .. } => {
                if category == &TypeCategories::Define {
                    let c_name = contents.iter().find_map(|c| if let OtherContents::Name(name) = &c { Some(name) } else { None }).cloned();
                    let rs_name = c_name.as_ref().map(String::as_str).map(trim_vk_prefix).map(global_conversion);

                    if let Some(c_name) = &c_name {
                        if disabled_manager.is_disabled(c_name) {
                            return None;
                        }
                    }

                    let last_code = contents.iter().rfind(|c| if let OtherContents::Code(_) = &c { true } else { false }).
                        map(|c| if let OtherContents::Code(code) = &c { code } else { unreachable!() }).unwrap().clone().split("//").next().unwrap()
                        .replace("VK_", "");

                    Some(Define {
                        aliases: c_name.as_ref().map(|c| aliases.get(c).map(|v| v.iter().map(|s| global_conversion(trim_vk_prefix(s))).collect::<Vec<_>>())).flatten().unwrap_or_default(),
                        name: c_name.zip(rs_name),
                        last_code,
                    })
                } else {
                    None
                }
            }
            _ => None
        }).collect()
}

impl Display for Define {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self.name.as_ref().map(|(c, rs)| (c.as_str(), rs.as_str())) {
            Some(("VK_HEADER_VERSION", rs)) => {
                writeln!(f, "pub const {}: u32 = {};", rs, self.last_code)?;
                for a in &self.aliases {
                    writeln!(f, "pub const {}: u32 = {};", a, rs)?;
                }
                Ok(())
            },
            Some((c, rs)) => {
                if (c.starts_with("VK_API_VERSION_") && c.chars().last().unwrap().is_numeric()) || c == "VK_HEADER_VERSION_COMPLETE" {
                    writeln!(f, "pub const {}: Version = Version::new{};", rs, self.last_code)?;
                    for a in &self.aliases {
                        writeln!(f, "pub const {}: Version = {};", a, rs)?;
                    }
                    Ok(())
                } else if c.starts_with("VK_STD_VULKAN_VIDEO") && c.contains("_VERSION_") {
                    writeln!(f, "pub const {}: Version = Version::new(0, {});", rs, self.last_code.trim_start_matches('(').trim_end_matches(')'))?;
                    for a in &self.aliases {
                        writeln!(f, "pub const {}: Version = {};", a, rs)?;
                    }
                    Ok(())
                } else {
                    Ok(())
                }
            }
            None => Ok(())
        }
    }
}