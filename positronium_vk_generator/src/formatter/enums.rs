use std::collections::{BTreeMap, BTreeSet, HashMap};
use std::fmt::{Display, Formatter, Write};

use heck::CamelCase;

use crate::formatter::{CName, INDENT, Named, RSName, trim_tag_suffix, trim_vk_prefix};
use crate::formatter::common::global_conversion;
use crate::formatter::disabled_manager::DisabledManager;
use crate::parser::enums::{EnumTypes, EnumVariantType};
use crate::parser::features_extensions::{EnumExtensionType, EnumType, RequireRemoveType};
use crate::parser::Registry;
use crate::parser::types::Type;

#[derive(Debug)]
pub struct Enum {
    pub name: (CName, RSName),
    pub bit_width: u8,
    pub aliases: Vec<(CName, RSName)>,
    pub variants: BTreeMap<i64, Variant>,
    pub variant_aliases: BTreeMap<(CName, RSName), BTreeSet<(CName, RSName)>>,
}

impl Named for Enum {
    fn get_names(&self) -> Vec<(&CName, &RSName)> {
        core::iter::once((&self.name.0, &self.name.1))
            .chain(self.aliases.iter().map(|(c, rs)| (c, rs)))
            .collect()
    }
}

#[derive(Debug, Clone)]
pub struct Variant {
    pub name: (CName, RSName),
    pub value: i64,
}

pub fn format_enums(registry: &Registry, disabled_manager: &DisabledManager) -> Vec<Enum> {
    let mut enums: Vec<_> = registry.enums.iter().filter(|e| e.enum_type == EnumTypes::Enum && !disabled_manager.is_disabled(e.name.as_ref().unwrap())).map(|e| {
        let c_name = e.name.clone().unwrap();
        let rs_name = "Raw".to_string() + &global_conversion(trim_vk_prefix(c_name.as_str()));

        let aliases: Vec<(CName, RSName)> = registry.types.iter().filter_map(|t| {
            match t {
                Type::StructUnion { .. } => None,
                Type::Other { .. } => None,
                Type::Enum { name, alias, .. } => {
                    if disabled_manager.is_disabled(name) {
                        return None;
                    }

                    if let Some(alias) = alias {
                        if alias == &c_name {
                            Some(name.clone())
                        } else {
                            None
                        }
                    } else {
                        None
                    }
                }
            }.map(|c_name| {
                let rs_name = "Raw".to_string() + &global_conversion(trim_vk_prefix(c_name.as_str()));
                (c_name, rs_name)
            })
        }).collect();

        let mut variant_aliases = BTreeMap::new();

        let variants = e.variants.iter().filter_map(|v| {
            let v_c_name = v.name.clone();

            let v_rs_name = format_enum_variant_name(&rs_name, &v_c_name, registry);

            match &v.variant_type {
                EnumVariantType::Value { value, value_type: _ } => {
                    Some(if let Some(value) = value.strip_prefix("0x") {
                        i64::from_str_radix(value, 16).expect(&format!("Value not hex: {}", value))
                    } else {
                        value.parse::<i64>().expect(&format!("Value not int: {}", value))
                    })
                }
                EnumVariantType::Bitpos(bitpos) => Some(1i64 << bitpos.parse::<u32>().unwrap()),
                EnumVariantType::Alias(alias) => {
                    if disabled_manager.is_disabled(alias) || disabled_manager.is_disabled(&v_c_name) {
                        return None;
                    }

                    let a_c_name = alias.clone();
                    let a_rs_name = format_enum_variant_name(&rs_name, &a_c_name, registry);

                    if a_rs_name.contains("ErrorValidationFailed") {
                        dbg!();
                    }

                    variant_aliases.entry((a_c_name, a_rs_name)).or_insert(BTreeSet::new()).insert((v_c_name.clone(), v_rs_name.clone()));

                    None
                }
            }.map(|value| (value, Variant {
                name: (v_c_name, v_rs_name),
                value,
            }))
        }).collect();

        Enum {
            name: (c_name, rs_name),
            bit_width: e.bit_width,
            aliases,
            variants,
            variant_aliases,
        }
    }).collect();

    let mut enum_lookup: HashMap<CName, &mut Enum> = enums.iter_mut().map(|e| (e.name.0.clone(), e)).collect();


    registry.features.iter().map(|f| (&f.require_removes, None)).chain(registry.extensions.iter().filter(|e| !e.supported.iter().any(|s| s == "disabled")).map(|r| (&r.require_removes, r.number))).for_each(|(r, number)| {
        r.iter().filter(|r| r.rr_type == RequireRemoveType::Required).for_each(|r| {
            r.enums.iter().for_each(|e| {
                match &e.enum_type {
                    EnumType::Reference => {}
                    EnumType::Extension(eex) => {
                        match &eex {
                            EnumExtensionType::NumericValue { value, value_type: _, extends } => {
                                if let Some(extends) = extends { // Ignore constants
                                    if let Some(extends) = enum_lookup.get_mut(extends) { // If no there, then most likely a bit mask thing

                                        let value = if let Some(value) = value.strip_prefix("0x") {
                                            i64::from_str_radix(value, 16).expect(&format!("Value not hex: {}", value))
                                        } else {
                                            value.parse::<i64>().expect(&format!("Value not int: {}", value))
                                        };

                                        let c_name = e.name.clone();
                                        let rs_name = format_enum_variant_name(extends.name.1.as_str(), c_name.as_str(), registry);

                                        extends.variants.insert(value, Variant {
                                            name: (c_name, rs_name),
                                            value,
                                        });
                                    }
                                }
                            }
                            EnumExtensionType::Alias { alias, extends } => {
                                if let Some(extends) = extends {
                                    if let Some(extends) = enum_lookup.get_mut(extends) { // If no there, then most likely a bit mask thing
                                        let c_name = e.name.clone();
                                        let rs_name = format_enum_variant_name(extends.name.1.as_str(), c_name.as_str(), registry);

                                        let a_c_name = alias.clone();
                                        let a_rs_name = format_enum_variant_name(extends.name.1.as_str(), a_c_name.as_str(), registry);

                                        if !disabled_manager.is_disabled(&a_c_name) && !disabled_manager.is_disabled(&c_name) {
                                            extends.variant_aliases.entry((a_c_name, a_rs_name)).or_default()
                                                .insert((c_name, rs_name));
                                        }
                                    }
                                }
                            }
                            EnumExtensionType::ValueAdded { offset, ext_number, negative_dir, extends } => {
                                let ext_number = ext_number.or(number).expect(&format!("Require Enum with no extension number specified in either place: {}", e.name)) as i64;

                                let value = if *negative_dir {
                                    -(1_000_000_000 + (ext_number - 1) * 1_000 + (*offset as i64))
                                } else {
                                    1_000_000_000 + (ext_number - 1) * 1_000 + (*offset as i64)
                                };

                                let extends = enum_lookup.get_mut(extends).expect(&format!("Require enum extends non existent enum: {}", extends));

                                let c_name = e.name.clone();
                                let rs_name = format_enum_variant_name(extends.name.1.as_str(), c_name.as_str(), registry);


                                extends.variants.insert(value, Variant {
                                    name: (c_name, rs_name),
                                    value,
                                });
                            }
                            EnumExtensionType::BitmaskValue { .. } => {
                                // Ignored
                            }
                        }
                    }
                }
            })
        })
    });

    let result_enum = enum_lookup.get("VkResult").unwrap();
    let success_code_enum = Enum {
        name: ("SuccessCode".to_string(), "RawSuccessCode".to_string()),
        bit_width: result_enum.bit_width,
        aliases: vec![],
        variants: result_enum.variants.iter().filter_map(|(i, v)| (*i >= 0).then(|| (*i, v.clone()))).collect(),
        variant_aliases: Default::default()
    };
    let error_code_enum = Enum {
        name: ("ErrorCode".to_string(), "RawErrorCode".to_string()),
        bit_width: result_enum.bit_width,
        aliases: vec![],
        variants: result_enum.variants.iter().filter_map(|(i, v)| (*i < 0).then(|| (*i, v.clone()))).collect(),
        variant_aliases: Default::default()
    };

    enums.push(success_code_enum);
    enums.push(error_code_enum);

    enums
}

fn format_enum_variant_name(e_rs_name: &str, v_c_name: &str, registry: &Registry) -> String {
    let e_rs_name = e_rs_name.trim_start_matches("Raw");
    let (e_rs_name, e_rs_name_suffix) = {
        let trimmed = trim_tag_suffix(e_rs_name, registry.tags.iter());
        let suffix = &e_rs_name[trimmed.len()..];
        (trimmed, suffix)
    };

    let v_c_name = global_conversion(v_c_name);
    let v_c_name = if let Some(v_c_name) = v_c_name.strip_suffix(e_rs_name_suffix) {
        v_c_name
    } else {
        v_c_name.as_str()
    };

    let mut name = trim_vk_prefix(v_c_name).to_camel_case();

    if let Some(s_name) = name.strip_prefix(e_rs_name) {
        name = s_name.to_string();
    }

    for i in 1..name.len() - 1 {
        let mut cs = name.chars();
        let c0 = cs.nth(i - 1).unwrap();
        let c1 = cs.next().unwrap();
        let c2 = cs.next().unwrap();

        if c0.is_numeric() && c1.is_alphabetic() && c2.is_numeric() {
            (&mut name[i..=i]).make_ascii_uppercase();
        }
    }

    "e".to_string() + &name
}

impl Enum {
    pub fn format(&self) -> Result<String, std::fmt::Error> {
        let mut f = String::new();

        // writeln!(f, "#[repr(i{})]", self.bit_width)?;
        writeln!(f, "#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]")?;
        writeln!(f, "#[allow(non_camel_case_types)]")?;
        writeln!(f, "#[non_exhaustive]")?;
        writeln!(f, "pub enum {} {{", self.name.1.trim_start_matches("Raw"))?;

        for variant in &self.variants {
            writeln!(f, "{}{},", INDENT, variant.1.name.1)?;
        }
        writeln!(f, "{}eUnknownVariant(i{})", INDENT, self.bit_width)?;

        writeln!(f, "}}\n")?;

        for (_, rs_alias) in &self.aliases {
            writeln!(f, "pub type {} = {};\n", rs_alias.trim_start_matches("Raw"), self.name.1.trim_start_matches("Raw"))?;
        }

        writeln!(f, "#[allow(non_upper_case_globals)]")?;
        writeln!(f, "impl {} {{", self.name.1.trim_start_matches("Raw"))?;
        for ((_, rs_name), aliases) in &self.variant_aliases {
            if rs_name.contains("eErrorValidationFailed") {
                dbg!(rs_name);
                dbg!();
            }

            for (_, rs_alias) in aliases {
                writeln!(f, "{}pub const {}: Self = Self::{};", INDENT, rs_alias, rs_name)?;
            }
        }

        if !self.variant_aliases.is_empty() {
            writeln!(f)?;
        }

        // writeln!(f, "impl {0} {{", self.name.1.trim_start_matches("Raw"))?;
        writeln!(f, "{0}pub fn into_raw(self) -> {1} {{", INDENT, self.name.1)?;
        writeln!(f, "{0}{0}match self {{", INDENT)?;
        for variant in &self.variants {
            writeln!(f, "{0}{0}{0}Self::{1} => {2}::{1},", INDENT, variant.1.name.1, self.name.1)?;
        }
        writeln!(f, "{0}{0}{0}Self::eUnknownVariant(v) => {1}(v)", INDENT, self.name.1)?;
        writeln!(f, "{0}{0}}}", INDENT)?;
        writeln!(f, "{0}}}", INDENT)?;
        writeln!(f, "}}")?;
        writeln!(f, "")?;

        Ok(f)
    }

    pub fn format_raw(&self) -> Result<String, std::fmt::Error> {
        let mut f = String::new();

        writeln!(f, "#[repr(C)]")?;
        writeln!(f, "#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]")?;
        writeln!(f, "pub struct {}(pub i{});", self.name.1, self.bit_width)?;

        writeln!(f)?;

        for (_, rs_alias) in &self.aliases {
            writeln!(f, "pub type {} = {};\n", rs_alias, self.name.1)?;
        }

        if !self.aliases.is_empty() {
            writeln!(f)?;
        }

        writeln!(f, "#[allow(non_upper_case_globals)]")?;
        writeln!(f, "impl {} {{", self.name.1)?;
        for variant in &self.variants {
            writeln!(f, "{0}pub const {1}: Self = {2}({3});", INDENT, variant.1.name.1, self.name.1, variant.1.value)?;
        }
        for ((_, rs_name), aliases) in &self.variant_aliases {
            for (_, rs_alias) in aliases {
                if self.variants.iter().any(|v| &v.1.name.1 == rs_alias) {
                    continue;
                }
                writeln!(f, "{}pub const {}: Self = Self::{};", INDENT, rs_alias, rs_name)?;
            }
        }

        writeln!(f)?;

        writeln!(f, "{0}pub fn normalise(self) -> {1} {{", INDENT, self.name.1.trim_start_matches("Raw"))?;
        writeln!(f, "{0}{0}match self {{", INDENT)?;
        for variant in &self.variants {
            writeln!(f, "{0}{0}{0}Self::{1} => {2}::{1},", INDENT, variant.1.name.1, self.name.1.trim_start_matches("Raw"))?;
        }
        writeln!(f, "{0}{0}{0}v => {1}::eUnknownVariant(v.0),", INDENT, self.name.1.trim_start_matches("Raw"))?;
        writeln!(f, "{0}{0}}}", INDENT)?;
        writeln!(f, "{0}}}", INDENT)?;

        writeln!(f, "}}\n")?;

        writeln!(f, "impl core::fmt::Debug for {} {{", self.name.1)?;
        {
            writeln!(f, "{}fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {{", INDENT)?;
            {
                writeln!(f, "{0}{0}self.normalise().fmt(f)", INDENT)?;
            }
            writeln!(f, "{}}}", INDENT)?;
        }
        writeln!(f, "}}\n")?;

        Ok(f)
    }
}

impl Display for Variant {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{}{} = {},", INDENT, self.name.1, self.value)
    }
}
