use std::collections::{BTreeMap, HashMap, HashSet};
use std::convert::identity;
use std::fmt::{Display, Formatter, Write};

use heck::{CamelCase, ShoutySnakeCase, SnakeCase};

use crate::formatter::{CName, INDENT, RSName, Spec, trim_vk_prefix};
use crate::formatter::common::{format_parameters, format_parameters_2, global_conversion, Parameter, ParameterValueType, PointerType};
use crate::formatter::disabled_manager::DisabledManager;
use crate::formatter::extern_types::ExternType;
use crate::formatter::function_types::FunctionType;
use crate::formatter::handles::Handle;
use crate::formatter::structs_unions::StructUnion;
use crate::parser::commands::Command;
use crate::parser::common::ParameterContents;
use crate::parser::Registry;

#[derive(Debug, Clone)]
pub struct Function {
    pub parent: Option<(CName, RSName)>,
    pub raw: RawFunction,
    pub oxidised: Option<OxidisedFunction>,
    pub platform: Option<String>,
}

// #[derive(Debug)]
// pub struct RawParameter {
//     name: (CName, RSName),
//     ty: (CName, RSName)
// }

#[derive(Debug, Clone)]
pub struct RawFunction {
    pub name: (CName, RSName),
    pub type_name: RSName,
    pub parameters: Vec<Parameter>,
    pub return_ty: Option<(CName, RSName)>,
    pub platform: Option<String>,
}

#[derive(Debug, Clone)]
pub struct OxidisedFunction {
    pub name: String,
    pub parameters: Vec<OxidisedParameter>,
    pub reverse_parameters: Vec<OxidisedReversedParameterType>,
    pub return_type: Option<OxidisedReturn>,
    pub raw_parameters: Vec<Parameter>,
    pub platform: Option<String>,
    pub first_param_is_handle: bool,
}

#[derive(Debug, Clone)]
pub struct OxidisedParameter {
    pub name: String,
    pub p_type: OxidisedParameterType,
    pub not_return: bool,
    pub extern_sync: bool
}

#[derive(Debug, Clone)]
pub struct OxidisedReturn {
    pub has_result: bool,
    pub other_return: Option<String>,
    pub success_codes: Vec<String>,
    pub error_codes: Vec<String>,
    pub values: Vec<OxidisedParameterType>,
}

#[derive(Debug, Clone)]
pub enum OxidisedParameterType {
    Value { c_param_index: usize },
    Array { c_ptr_index: usize, c_len_index: usize },
}

#[derive(Debug, Clone)]
pub enum OxidisedReversedParameterType {
    Value { original_index: Option<usize> },
    ArrayPtr { ptr_from_index: Option<usize> },
    ArrayLen { len_from_index: Option<Vec<(usize, bool)>> },
}

const NOT_RETURNED: &[(&str, &str)] = &[("vkAcquireXlibDisplayEXT", "dpy"), ("vkGetMemoryAndroidHardwareBufferANDROID", "pBuffer")];

pub fn format_functions(registry: &Registry, disabled: &DisabledManager, rs_name_map: &mut HashMap<CName, RSName>, handles: &Vec<Handle>, function_types: &Vec<FunctionType>, structs_unions: &BTreeMap<CName, StructUnion>, int_alias_types: &Vec<RSName>, extern_types: &Vec<ExternType>, enum_like: &HashSet<RSName>) -> Vec<Function> {
    let mut functions: BTreeMap<_, _> = registry.commands.iter()
        .filter_map(|c| match c {
            Command::Alias { name, alias } => {
                // dbg!((name, alias)); //TODO handle aliases for final oxidised functions
                None
            }
            Command::Definition { queues, success_codes, error_codes, render_pass, cmd_buffer_level, pipeline, proto, param } => {
                let c_name = proto.iter().filter_map(|p| if let ParameterContents::Name(s) = p { Some(s.as_str()) } else { None }).collect::<String>();

                if disabled.is_disabled(&c_name) {
                    None
                } else {
                    let rs_name = global_conversion(trim_vk_prefix(&c_name).to_snake_case());
                    let type_name = global_conversion(trim_vk_prefix(&c_name).to_camel_case());

                    let c_return_ty = proto.iter().filter_map(|p| match p {
                        ParameterContents::Code(s) => Some(s.as_str()),
                        ParameterContents::Type(s) => Some(s.as_str()),
                        ParameterContents::Name(_) => None,
                        ParameterContents::Enum(_) => None,
                    }).collect::<String>().trim().to_string();

                    let return_ty = if &c_return_ty != "void" {
                        let rs_return_ty = rs_name_map.get(&c_return_ty).map_or(c_return_ty.clone(), |s| s.clone());

                        Some((c_return_ty, rs_return_ty))
                    } else {
                        None
                    };

                    let mut parameters = format_parameters(rs_name.clone(), param.iter(), false, true, false, false, rs_name_map, handles, function_types, int_alias_types, enum_like);
                    format_parameters_2(rs_name.clone(), parameters.iter_mut(), structs_unions, handles, false, true, false);

                    // if &type_name == "EnumerateInstanceVersion" {
                    //     parameters[0].type_name.1 = "Version".to_string();
                    //     parameters[0].full_type.1 = "&mut Version".to_string();
                    // }

                    let raw = RawFunction {
                        name: (c_name, rs_name),
                        type_name,
                        parameters,
                        return_ty,
                        platform: None,
                    };

                    let oxidised = {
                        let name = raw.name.1.clone();


                        let mut return_type = {
                            if let Some(rt) = &raw.return_ty {
                                if &rt.1 == "RawResult" {
                                    OxidisedReturn {
                                        has_result: true,
                                        other_return: None,
                                        success_codes: success_codes.iter().map(|s| trim_vk_prefix(s.as_str()).to_string()).collect(),
                                        error_codes: error_codes.iter().map(|s| trim_vk_prefix(s.as_str()).to_string()).collect(),
                                        values: vec![],
                                    }
                                } else {
                                    OxidisedReturn {
                                        has_result: false,
                                        other_return: Some(rt.1.clone()),
                                        success_codes: vec![],
                                        error_codes: vec![],
                                        values: vec![],
                                    }
                                }
                            } else {
                                OxidisedReturn {
                                    has_result: false,
                                    other_return: None,
                                    success_codes: vec![],
                                    error_codes: vec![],
                                    values: vec![],
                                }
                            }
                        };

                        let mut reverse_parameters = vec![];
                        let mut current_index = 0;
                        let mut first_param_is_handle = false;
                        let parameters: Vec<_> = raw.parameters.iter().enumerate().filter_map(|(i, p)| {
                            if i == 0 && !raw.parameters.is_empty() && handles.iter().any(|h| &h.name.0 == &raw.parameters[0].type_name.0) {
                                first_param_is_handle = true;
                                reverse_parameters.push(OxidisedReversedParameterType::Value { original_index: Some(0) });
                                current_index += 1;
                                Some(OxidisedParameter { name: p.name.1.clone(), p_type: OxidisedParameterType::Value { c_param_index: 0 }, not_return: false, extern_sync: p.extern_sync })
                            } else if p.is_length_for.is_empty() {
                                let p_name = p.name.1.clone();
                                let (p_type, r) = if p.pointers.is_empty() {
                                    (OxidisedParameterType::Value { c_param_index: i }, OxidisedReversedParameterType::Value { original_index: Some(current_index) })
                                } else {
                                    let ptr = &p.pointers[0];

                                    if let Some((_c_len, rs_len, _)) = &ptr.len {
                                        if rs_len == "1" {
                                            // Single ptr

                                            (OxidisedParameterType::Value { c_param_index: i }, OxidisedReversedParameterType::Value { original_index: Some(current_index) })
                                        } else if rs_len == "null-terminated" {
                                            // Ptr to c string

                                            (OxidisedParameterType::Value { c_param_index: i }, OxidisedReversedParameterType::Value { original_index: Some(current_index) })
                                        } else if let Some(j) = raw.parameters.iter().find_map(|p| p.is_length_for.iter().find_map(|(m, p, _)| if *m == i && *p == 0 { Some(*m) } else { None })) {
                                            (OxidisedParameterType::Array { c_ptr_index: i, c_len_index: j }, OxidisedReversedParameterType::ArrayPtr { ptr_from_index: Some(current_index) })
                                        } else if let Some(j) = raw.parameters.iter().enumerate().filter_map(|(j, p)| if i != j { Some((j, p)) } else { None }).clone().find_map(|(j, om)| if rs_len.contains(&om.name.1) { Some(j) } else { None }) {
                                            (OxidisedParameterType::Array { c_ptr_index: i, c_len_index: j }, OxidisedReversedParameterType::ArrayPtr { ptr_from_index: Some(current_index) })
                                        } else {
                                            // Otherwise: Length must come from a constant
                                            (OxidisedParameterType::Value { c_param_index: i }, OxidisedReversedParameterType::Value { original_index: Some(current_index) })
                                        }
                                    } else {
                                        // No len -> Single ptr
                                        (OxidisedParameterType::Value { c_param_index: i }, OxidisedReversedParameterType::Value { original_index: Some(current_index) })
                                    }
                                };

                                if let Some(ptr) = p.pointers.first() {
                                    match ptr.ty {
                                        PointerType::Const => {
                                            current_index += 1;
                                            reverse_parameters.push(r);
                                            Some(OxidisedParameter {
                                                name: p_name,
                                                p_type,
                                                not_return: false,
                                                extern_sync: p.extern_sync
                                            })
                                        }
                                        PointerType::Mut => {
                                            if NOT_RETURNED.contains(&(raw.name.0.as_str(), p.name.0.as_str())) || extern_types.iter().any(|e| e.name.1 == p.type_name.1) {
                                                reverse_parameters.push(r);
                                                current_index += 1;
                                                Some(OxidisedParameter {
                                                    name: p_name,
                                                    p_type,
                                                    not_return: true,
                                                    extern_sync: p.extern_sync
                                                })
                                            } else {
                                                let r = match r {
                                                    OxidisedReversedParameterType::Value { .. } => OxidisedReversedParameterType::Value { original_index: None },
                                                    OxidisedReversedParameterType::ArrayPtr { .. } => OxidisedReversedParameterType::ArrayPtr { ptr_from_index: None },
                                                    OxidisedReversedParameterType::ArrayLen { .. } => OxidisedReversedParameterType::ArrayLen { len_from_index: None },
                                                };
                                                reverse_parameters.push(r);
                                                return_type.values.push(p_type);
                                                None
                                            }
                                        }
                                    }
                                } else {
                                    reverse_parameters.push(r);
                                    current_index += 1;
                                    Some(OxidisedParameter {
                                        name: p_name,
                                        p_type,
                                        not_return: false,
                                        extern_sync: p.extern_sync
                                    })
                                }
                            } else {
                                for l in &p.is_length_for {
                                    if l.1 != 0 {
                                        eprintln!("Warning:");
                                    }
                                }
                                if (p.pointers.is_empty() || p.pointers[0].ty == PointerType::Const) && p.is_length_for.iter().any(|l| !raw.parameters[l.0].pointers.is_empty() && raw.parameters[l.0].pointers[0].ty == PointerType::Const) {
                                    reverse_parameters.push(OxidisedReversedParameterType::ArrayLen { len_from_index: Some(p.is_length_for.iter().map(|l| (l.0, false)).collect()) });
                                    if p.is_length_for.iter().any(|l| l.2.is_some()) {
                                        current_index += 1;
                                        Some(OxidisedParameter {
                                            name: p.name.1.clone(),
                                            p_type: OxidisedParameterType::Value { c_param_index: i },
                                            not_return: false,
                                            extern_sync: p.extern_sync
                                        })
                                    } else {
                                        None
                                    }
                                } else if (p.pointers.is_empty() || p.pointers[0].ty == PointerType::Const) && p.is_length_for.iter().any(|l| !raw.parameters[l.0].pointers.is_empty() && raw.parameters[l.0].pointers[0].ty == PointerType::Mut) {
                                    reverse_parameters.push(OxidisedReversedParameterType::ArrayLen { len_from_index: Some(p.is_length_for.iter().map(|l| (l.0, false)).collect()) });
                                    if true || p.is_length_for.iter().any(|l| l.2.is_some()) {
                                        current_index += 1;
                                        Some(OxidisedParameter {
                                            name: p.name.1.clone(),
                                            p_type: OxidisedParameterType::Value { c_param_index: i },
                                            not_return: false,
                                            extern_sync: p.extern_sync
                                        })
                                    } else {
                                        None
                                    }
                                } else {
                                    reverse_parameters.push(OxidisedReversedParameterType::ArrayLen { len_from_index: None });
                                    None
                                }
                            }
                        }).collect();

                        for i in 0..reverse_parameters.len() {
                            let mut r = OxidisedReversedParameterType::ArrayLen { len_from_index: None };
                            std::mem::swap(&mut r, &mut reverse_parameters[i]);

                            r = if let OxidisedReversedParameterType::ArrayLen { len_from_index } = r {
                                if let Some(len_from_index) = len_from_index {
                                    OxidisedReversedParameterType::ArrayLen {
                                        len_from_index: Some(len_from_index.into_iter()
                                            .filter_map(|l| {
                                                match &reverse_parameters[l.0] {
                                                    OxidisedReversedParameterType::Value { original_index } => unreachable!("Can't get len from value"),
                                                    OxidisedReversedParameterType::ArrayPtr { ptr_from_index } => {
                                                        ptr_from_index.map(|ptr_from_index| (ptr_from_index, raw.parameters[l.0].pointers[0].ty == PointerType::Mut))
                                                    }
                                                    OxidisedReversedParameterType::ArrayLen { len_from_index } => unreachable!("Can't get len of len")
                                                }
                                            })
                                            .collect())
                                    }
                                } else {
                                    OxidisedReversedParameterType::ArrayLen { len_from_index: None }
                                }
                            } else {
                                r
                            };

                            std::mem::swap(&mut r, &mut reverse_parameters[i]);
                        }

                        Some(
                            OxidisedFunction {
                                name,
                                parameters,
                                reverse_parameters,
                                return_type: if return_type.has_result || return_type.other_return.is_some() || !return_type.values.is_empty() { Some(return_type) } else { None },
                                raw_parameters: raw.parameters.clone(),
                                platform: None,
                                first_param_is_handle,
                            }
                        )
                    };

                    Some((raw.name.0.clone(), Function {
                        parent: None,
                        raw,
                        oxidised,
                        platform: None,
                    }))
                }
            }
        })
        .collect();

    for e in &registry.extensions {
        if !e.supported.iter().any(|s| s == "disabled") {
            for r in &e.require_removes {
                for c in &r.commands {
                    if let Some(f) = functions.get_mut(&c.name) {
                        f.platform = e.platform.clone();
                        f.raw.platform = e.platform.clone();
                        if let Some(o) = &mut f.oxidised {
                            o.platform = e.platform.clone();
                        }
                    }
                }
            }
        }
    }

    functions.into_iter().map(|(_, v)| v).collect()
}

impl RawFunction {
    pub fn invalid_function(&self) -> std::result::Result<String, std::fmt::Error> {
        let mut f = String::new();

        // if let Some(platform) = &self.platform {
        //     writeln!(f, "{}#[cfg(feature = \"platform_{}\")]", INDENT, platform)?;
        // }

        write!(f, "{}pub extern \"C\" fn {} (", INDENT, self.name.1)?;

        for (i, p) in self.parameters.iter().enumerate() {
            write!(f, "_: {}", p.full_ptr_type)?;
            if i != self.parameters.len() - 1 {
                write!(f, ", ")?;
            }
        }

        if let Some(r) = &self.return_ty {
            write!(f, ") -> {} ", r.1)?;
        } else {
            write!(f, ") ")?;
        }

        write!(f, "{{ panic!(\"{} was not loaded successfully\") }}", self.name.0)?;

        Ok(f)
    }

    pub fn null_function(&self) -> std::result::Result<String, std::fmt::Error> {
        let mut f = String::new();

        // if let Some(platform) = &self.platform {
        //     writeln!(f, "{}#[cfg(feature = \"platform_{}\")]", INDENT, platform)?;
        // }

        write!(f, "{}pub extern \"C\" fn {} (", INDENT, self.name.1)?;

        for (i, p) in self.parameters.iter().enumerate() {
            write!(f, "_: {}", p.full_ptr_type)?;
            if i != self.parameters.len() - 1 {
                write!(f, ", ")?;
            }
        }

        if let Some(r) = &self.return_ty {
            write!(f, ") -> {} ", r.1)?;
        } else {
            write!(f, ") ")?;
        }

        write!(f, "{{ panic!(\"Associated handle has not been initialised\") }}")?;

        Ok(f)
    }

    pub fn no_op_function(&self) -> std::result::Result<String, std::fmt::Error> {
        let mut f = String::new();

        // if let Some(platform) = &self.platform {
        //     writeln!(f, "{}#[cfg(feature = \"platform_{}\")]", INDENT, platform)?;
        // }

        write!(f, "{}pub extern \"C\" fn {} (", INDENT, self.name.1)?;

        for (i, p) in self.parameters.iter().enumerate() {
            write!(f, "_: {}", p.full_ptr_type)?;
            if i != self.parameters.len() - 1 {
                write!(f, ", ")?;
            }
        }

        if let Some(r) = &self.return_ty {
            write!(f, ") -> {} {{ /* No Op */ RawResult::eSuccess }}", r.1)?;
        } else {
            write!(f, ") {{ /* No Op */ }}")?;
        }

        Ok(f)
    }
}

impl core::fmt::Display for RawFunction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // if let Some(platform) = &self.platform {
        //     writeln!(f, "#[cfg(feature = \"platform_{}\")]", platform)?;
        // }

        write!(f, "pub(crate) type {} = extern \"C\" fn(", self.type_name)?;

        for (i, p) in self.parameters.iter().enumerate() {
            write!(f, "{}: {}", p.name.1, p.full_ptr_type)?;
            if i != self.parameters.len() - 1 {
                write!(f, ", ")?;
            }
        }

        if let Some(r) = &self.return_ty {
            writeln!(f, ") -> {};", r.1)
        } else {
            writeln!(f, ");")
        }
    }
}

impl OxidisedFunction {
    pub fn format(&self, self_handle: &Handle, spec: &Spec, with_alloc: bool, with_alloc_exists: bool, destructor_uses_alloc: bool, is_destructor: bool, unchecked_external_sync: bool, needs_next_chain: Option<(usize, usize, bool)>, with_output: bool) -> Result<String, core::fmt::Error> {
        let mut f = String::new();

        let handle_map = spec.handles.iter().map(|h| (h.name.0.clone(), h)).collect::<HashMap<_, _>>();

        let enumerate = if let Some(rt) = &self.return_type {
            rt.success_codes.contains(&"INCOMPLETE".to_string()) && self.raw_parameters.iter().filter(|p| !p.is_length_for.is_empty()).count() != 0 && &self.name != "get_device_subpass_shading_max_workgroup_size_huawei"
        } else {
            false
        } | self.raw_parameters.iter().any(|p| p.v_type == ParameterValueType::Integer && !p.is_length_for.is_empty() && p.pointers.len() == 1 && p.pointers[0].ty == PointerType::Mut && !p.pointers[0].optional && p.value_optional);

        // if enumerate {
        //     writeln!(f, "/*name:\n{:#?}\n*/", self.name)?;
        //     writeln!(f, "/*return_type:\n{:#?}\n*/", self.return_type)?;
        //     writeln!(f, "/*parameters:\n{:#?}\n*/", self.parameters)?;
        //     writeln!(f, "/*rev:\n{:#?}\n*/", self.reverse_parameters)?;
        //     writeln!(f, "/*raw:\n{:#?}\n*/", self.raw_parameters)?;
        // }

        // if let Some(platform) = &self.platform {
        //     writeln!(f, "{0}#[cfg(feature = \"platform_{1}\")]", INDENT, platform)?;
        // }

        let mut generics = Vec::new();
        let mut generic_requirements = Vec::new();

        if &self.name == "create_shared_swapchains_khr" {
            generics.push("'surfaces".to_string());
            generics.push("S: IntoIterator<Item=&'surfaces mut SurfaceKHR>".to_string());
        }
        if let Some(n) = needs_next_chain {
            let s = spec.structs_unions.iter().find(|s| s.name.1 == self.raw_parameters[n.1].type_name.1).unwrap();
            let has_mut_p_next = s.members.iter().find(|m| &m.name.0 == "pNext").unwrap().pointers[0].ty == PointerType::Mut;

            generics.push("'item".to_string());
            if has_mut_p_next {
                generics.push(format!("I: IntoIterator<Item=&'item mut dyn {}MutNext>", s.name.1.trim_start_matches("Raw")));
            } else {
                generics.push(format!("I: IntoIterator<Item=&'item mut dyn {}Next>", s.name.1.trim_start_matches("Raw")));
            }

            generic_requirements.push(format!("<I as IntoIterator>::IntoIter: DoubleEndedIterator"));

            if n.2 {
                generics.push("N: IntoIterator<Item = I>".to_string());
                generics.push("F: FnOnce(usize) -> N".to_string());
            }
        }

        let returns_vec = if let Some(rt) = &self.return_type {
            rt.values.iter().filter_map(|v| {
                match v {
                    OxidisedParameterType::Value { .. } => None,
                    OxidisedParameterType::Array { c_ptr_index, ..} => {
                        let mut s = self.raw_parameters[*c_ptr_index].full_type.1.clone();
                        if s.starts_with("Option<") {
                            s = s.replacen("Option<", "", 1);
                            s = s.rsplit_once(">").unwrap().0.to_string();
                        }
                        if s.starts_with("&mut ") {
                            s = s.replacen("&mut ", "", 1);
                        } else {
                            eprintln!("Check (2): {}", s);
                        }
                        let mut t = if spec.handles.iter().any(|h| h.name.0 == self.raw_parameters[*c_ptr_index].type_name.0) {
                            s.replace("Raw", "")
                        } else {
                            s.trim_start_matches("Raw").to_string()
                        };
                        if ["DisplayProperties2KHR", "DisplayPropertiesKHR", "PipelineExecutableInternalRepresentationKHR", "CheckpointData2NV", "CheckpointDataNV"].contains(&t.as_str()) {
                            generics.insert(0, "'a".to_string());
                            t = format!("{t}<'a>");
                        }
                        Some(t)
                    },
                }
            }).collect::<Vec<_>>()
        } else { vec![] };

        let needs_vec = !returns_vec.is_empty() || ["build_acceleration_structures_khr", "cmd_build_acceleration_structures_khr", "cmd_build_acceleration_structures_indirect_khr"].contains(&self.name.as_str());

        if needs_vec && !with_output {
            if returns_vec.is_empty() {
                generics.push("V: GenericVecGen".to_string());
            }
            for (i, v) in returns_vec.iter().enumerate() {
                generics.push(format!("V{i}: GenericVec<{v}>"));
            }
        }

        for g in self.raw_parameters.iter().map(|p| p.generic_parameters.iter()).flatten() {
            generics.push(format!("{}", g.name));
        }

        if is_destructor {
            writeln!(f, "{}#[cfg(not(feature=\"automatic_destroy_on_drop\"))]", INDENT)?;
        }

        let self_mut = if (self.first_param_is_handle && self.raw_parameters[0].extern_sync) ||
            (self.raw_parameters.len() > 1 && &self_handle.name.0 == &self.raw_parameters[1].type_name.0 && self.raw_parameters[1].extern_sync) ||
            ["allocate_command_buffers", "allocate_descriptor_sets", "device_wait_idle"].contains(&self.name.as_str()) {
            "mut self"
        } else {
            "self"
        };

        if &self.name == "reset_descriptor_pool" {
            writeln!(f, "{0}/// Marked unsafe, since has the implicit external synchronization requirement that can not be enforced:", INDENT)?;
            writeln!(f, "{0}/// \"Any VkDescriptorSet objects allocated from descriptorPool in vkResetDescriptorPool\"", INDENT)?;
        }

        if let Some(rt) = &self.return_type {
            if !rt.success_codes.is_empty() {
                writeln!(f, "{0}/// SUCCESS_CODES = [{1}];", INDENT, rt.success_codes.iter().map(|s| format!("[SuccessCode::e{}]", s.to_camel_case())).collect::<Vec<_>>().join(", "))?;
            }
            if !rt.success_codes.is_empty() && !rt.error_codes.is_empty() {
                writeln!(f, "{}///", INDENT)?;
            }
            if !rt.error_codes.is_empty() {
                writeln!(f, "{0}/// ERROR_CODES = [{1}];", INDENT, rt.error_codes.iter().map(|s| format!("[ErrorCode::e{}]", s.to_camel_case())).collect::<Vec<_>>().join(", "))?;
            }
        }

        if unchecked_external_sync {
            if !generics.is_empty() {
                write!(f, "{}pub(crate) unsafe fn {}_unchecked<{}>(&{}", INDENT, self.name, generics.join(", "), self_mut)?;
            } else {
                write!(f, "{}pub(crate) unsafe fn {}_unchecked(&{}", INDENT, self.name, self_mut)?;
            }
        } else if !generics.is_empty() {
            if with_alloc && with_output {
                write!(f, "{}pub fn {}_with_output_and_alloc<{}>(&{}", INDENT, self.name, generics.join(", "), self_mut)?;
            } else if with_alloc {
                write!(f, "{}pub fn {}_with_alloc<{}>(&{}", INDENT, self.name, generics.join(", "), self_mut)?;
            } else if needs_next_chain.is_some() {
                write!(f, "{}pub fn {}_with_next<{}>(&{}", INDENT, self.name, generics.join(", "), self_mut)?;
            } else if with_output {
                write!(f, "{}pub fn {}_with_output<{}>(&{}", INDENT, self.name, generics.join(", "), self_mut)?;
            } else {
                write!(f, "{}pub fn {}<{}>(&{}", INDENT, self.name, generics.join(", "), self_mut)?;
            }
        } else if self.name == "map_memory" {
            if with_output {
                write!(f, "{}pub fn {}_with_output(&{}", INDENT, self.name, self_mut)?;
            } else {
                write!(f, "{}pub fn {}(&{}", INDENT, self.name, self_mut)?;
            }
        } else {
            if with_alloc && with_output {
                write!(f, "{}pub fn {}_with_output_and_alloc(&{}", INDENT, self.name, self_mut)?;
            } else if is_destructor {
                write!(f, "{}pub fn {}({}", INDENT, self.name, self_mut)?;
            } else if with_alloc {
                write!(f, "{}pub fn {}_with_alloc(&{}", INDENT, self.name, self_mut)?;
            } else if with_output {
                write!(f, "{}pub fn {}_with_output(&{}", INDENT, self.name, self_mut)?;
            } else {
                write!(f, "{}pub fn {}(&{}", INDENT, self.name, self_mut)?;
            }
        }

        if &self.name == "reset_descriptor_pool" {
            f = f.replace(" fn ", " unsafe fn ");
        }

        for (i, rp) in self.reverse_parameters.iter().enumerate() {
            match rp {
                OxidisedReversedParameterType::Value { original_index } => {
                    if let Some(p) = self.raw_parameters[i].pointers.first() {
                        if PointerType::Mut == p.ty {
                            match &self.raw_parameters[i].v_type {
                                ParameterValueType::Handle => {
                                    let handle = handle_map.get(&self.raw_parameters[i].type_name.0).unwrap();
                                    for add_par in &handle.additional_parents {
                                        if self.raw_parameters[i].extern_sync {
                                            write!(f, "/* TODO */")?;
                                        }
                                        write!(f, ", {}: &mut {}", add_par.1.to_snake_case(), add_par.1.trim_end_matches("Data"))?;
                                    }
                                }
                                _ => {}
                            }
                        }
                    }
                }
                _ => {}
            }
        }

        if &self_handle.name.1 == "CommandBuffer" && !unchecked_external_sync {
            write!(f, ", command_pool: &mut CommandPool")?;
        }

        if &self.name == "unmap_memory" {
            write!(f, ", mapped_memory: MappedMemory")?;
        }

        for p in self.parameters.iter().skip(if self.first_param_is_handle {
            if self.raw_parameters.len() > 1 && &self_handle.name.0 == &self.raw_parameters[1].type_name.0 {
                2
            } else {
                1
            }
        } else { 0 }) {
            if p.name == "allocator" {
                if with_alloc {
                    write!(f, ", alloc_mode: AllocMode")?;
                }
                continue;
            }
            let mut optional_handle = false;
            let ty = match &p.p_type {
                OxidisedParameterType::Value { c_param_index } => {
                    optional_handle = self.raw_parameters[*c_param_index].value_optional;

                    if spec.handles.iter().any(|h| h.name.0 == self.raw_parameters[*c_param_index].type_name.0) {
                        self.raw_parameters[*c_param_index].full_type.1.clone().replace("MutRaw", "MutHandle").replace("Raw", "Handle")
                    } else {
                        self.raw_parameters[*c_param_index].full_type.1.clone()
                    }
                }
                OxidisedParameterType::Array { c_ptr_index, c_len_index } => {
                    if spec.handles.iter().any(|h| h.name.0 == self.raw_parameters[*c_ptr_index].type_name.0) {
                        self.raw_parameters[*c_ptr_index].slice_type.clone().replace("MutRaw", "MutHandle").replace("Raw", "Handle")
                    } else {
                        self.raw_parameters[*c_ptr_index].slice_type.clone()
                    }
                }
            };
            match (self.name.as_str(), p.name.as_str()) {
                ("build_acceleration_structures_khr", "build_range_infos") | ("cmd_build_acceleration_structures_khr", "build_range_infos") | ("cmd_build_acceleration_structures_indirect_khr", "max_primitive_counts") => {
                    write!(f, ", {}: {}", p.name, ty.replace("&[&", "&[&[").replace("]", "]]"))?;
                    continue;
                }
                ("cmd_set_sample_mask_ext", "sample_mask") => {
                    write!(f, ", samples: SampleCountFlagBits, {}: {}", p.name, ty.trim_start_matches("Raw"))?;
                    continue;
                }
                ("create_video_session_parameters_khr", "create_info") => {
                    write!(f, ", {}: {}", p.name, ty.trim_start_matches("Raw").replacen("&", "&mut ", 1))?;
                    continue;
                }
                _ => {}
            }
            if ty.ends_with("Handle") {
                if p.extern_sync {
                    if optional_handle {
                        write!(f, ", {}: Option<&mut {}>", p.name, ty.trim_end_matches("MutHandle").trim_end_matches("Handle"))?;
                    } else {
                        write!(f, ", {}: &mut {}", p.name, ty.trim_end_matches("MutHandle").trim_end_matches("Handle"))?;
                    }
                } else {
                    if optional_handle {
                        write!(f, ", {}: Option<&{}>", p.name, ty.trim_end_matches("MutHandle").trim_end_matches("Handle"))?;
                    } else {
                        write!(f, ", {}: &{}", p.name, ty.trim_end_matches("MutHandle").trim_end_matches("Handle"))?;
                    }
                }
            } else if ty.contains("Handle]") {
                if p.extern_sync {
                    write!(f, ", {}: {}", p.name, ty.replace("&[", "&mut ["))?;
                } else {
                    write!(f, ", {}: {}", p.name, ty)?;
                }
            } else {
                write!(f, ", {}: {}", p.name, ty.trim_start_matches("Raw"))?;
            }
            // if &self.name == "map_memory" && &p.name == "memory" {
            //     write!(f, "<'device_memory>")?;
            // }
            if let OxidisedParameterType::Value { c_param_index } = &p.p_type {
                if !self.raw_parameters[*c_param_index].generic_parameters.is_empty() {
                    write!(f, "<{}>", self.raw_parameters[*c_param_index].generic_parameters.iter().map(|g| g.name.as_str()).collect::<Vec<_>>().join(", "))?;
                }
            }
        }

        if &self.name == "create_shared_swapchains_khr" {
            write!(f, ", surfaces: S")?;
        }

        if let Some(n) = &needs_next_chain {
            if n.2 {
                write!(f, ", next_chain_provider: F")?;
            } else {
                write!(f, ", next_chain: I")?;
            }
        }

        if with_output {
            // Add extra param
            if let Some(rt) = &self.return_type {
                let mut t: String = rt.values.iter().enumerate().map(|(i, p)| {
                    match p {
                        OxidisedParameterType::Value { c_param_index } => {
                            let p = &self.raw_parameters[*c_param_index];

                            if &self.name == "map_memory" && &p.name.1 == "data" {
                                return "data: &mut Option<core::ptr::NonNull<u8>>".to_string();
                            }

                            let mut s = p.full_type.1.clone();
                            if spec.handles.iter().any(|h| h.name.0 == p.type_name.0) {
                                format!("{}: {}", p.name.1, s)
                            } else {
                                format!("{}: {}", p.name.1, s)
                            }
                        }
                        OxidisedParameterType::Array { c_ptr_index, c_len_index } => {
                            let p = &self.raw_parameters[*c_ptr_index];

                            if &self.name == "map_memory" && &p.name.1 == "data" {
                                return "data: &mut Option<core::ptr::NonNull<u8>>".to_string();
                            }
                            // let l = &self.raw_parameters[*c_len_index];

                            let l = self.raw_parameters.iter().find(|p| p.is_length_for.iter().any(|l| l.0 == *c_ptr_index)).unwrap();

                            if enumerate {
                                if rt.values.len() == 2 {
                                    if i == 0 {
                                        let other_i = match &rt.values[1] {
                                            OxidisedParameterType::Value { .. } => unreachable!(),
                                            OxidisedParameterType::Array { c_len_index, .. } => *c_len_index,
                                        };
                                        let other_p = &self.raw_parameters[other_i];

                                        let s1 = p.full_type.1.replacen("&mut ", "", 1).trim_start_matches("Option<").trim_end_matches(">").to_string();
                                        let s2 = other_p.full_type.1.replacen("&mut ", "", 1).trim_start_matches("Option<").trim_end_matches(">").to_string();

                                        format!("mut {}: DoubleEnumerateOptions<{}, {}, {}>", p.name.1, l.type_name.1, s1, s2)

                                    } else {
                                        "".to_string()
                                    }
                                } else {
                                    let s = p.full_type.1.replacen("&mut ", "", 1).trim_start_matches("Option<").trim_end_matches(">").to_string();

                                    format!("mut {}: EnumerateOptions<{}, {}>", p.name.1, l.type_name.1, s)
                                }
                            } else {
                                let s = p.full_type.1.replacen("&mut ", "&mut [", 1) + "]";
                                if spec.handles.iter().any(|h| h.name.0 == p.type_name.0) {
                                    format!("{}: {}", p.name.1, s)
                                } else {
                                    format!("{}: {}", p.name.1, s)
                                }
                            }
                        }
                    }
                }).filter(|s| !s.is_empty()).collect::<Vec<String>>().join(", ");

                // if &self.name == "map_memory" {
                //     t = "MappedMemory".to_string();
                // }

                if !t.is_empty() {
                    write!(f, ", {}", t)?;
                }
            }
        }

        let mut input_param = HashSet::<usize>::new();
        for x in &self.parameters {
            match &x.p_type {
                OxidisedParameterType::Value { c_param_index } => {
                    input_param.insert(*c_param_index);
                }
                OxidisedParameterType::Array { c_ptr_index, c_len_index } => {
                    input_param.insert(*c_ptr_index);
                    input_param.insert(*c_len_index);
                }
            }
        }

        let mut return_needs_norm = vec![false; self.return_type.as_ref().map_or(0, |r| r.values.len())];
        let mut return_is_handle = vec![false; self.return_type.as_ref().map_or(0, |r| r.values.len())];
        write!(f, ") ")?;

        if let Some(rt) = &self.return_type {
            let mut t: String = rt.values.iter().enumerate().map(|(i, p)| {
                match p {
                    OxidisedParameterType::Value { c_param_index } => {
                        let mut s = self.raw_parameters[*c_param_index].full_type.1.clone();
                        if s.starts_with("&mut ") {
                            s = s.replacen("&mut ", "", 1);
                        } else {
                            eprintln!("Check: {}", s);
                        }
                        if spec.handles.iter().any(|h| h.name.0 == self.raw_parameters[*c_param_index].type_name.0) {
                            return_is_handle[i] = true;
                            s.replace("Raw", "")
                        } else {
                            if s.starts_with("Raw") {
                                return_needs_norm[i] = true;
                            }
                            s.trim_start_matches("Raw").to_string()
                        }
                    }
                    OxidisedParameterType::Array { c_ptr_index, c_len_index } => {
                        let mut s = self.raw_parameters[*c_ptr_index].full_type.1.clone();
                        if s.starts_with("Option<") {
                            s = s.replacen("Option<", "", 1);
                            s = s.rsplit_once(">").unwrap().0.to_string();
                        }
                        if s.starts_with("&mut ") {
                            s = s.replacen("&mut ", "", 1);
                        } else {
                            eprintln!("Check (2): {}", s);
                        }
                        if spec.handles.iter().any(|h| h.name.0 == self.raw_parameters[*c_ptr_index].type_name.0) {
                        //     format!("V::V<{}>", s.replace("Raw", ""))
                            return_is_handle[i] = true;
                        } else {
                            if s.starts_with("Raw") {
                                return_needs_norm[i] = true;
                            }
                        //     format!("V::V<{}>", s.trim_start_matches("Raw"))
                        }
                        format!("V{i}")
                    }
                }
            }).collect::<Vec<String>>().join(", ");

            if &self.name == "map_memory" {
                t = "MappedMemory".to_string();
            }

            if with_output {
                t.clear();
            }

            if rt.has_result {
                if !t.is_empty() {
                    t = format!("core::result::Result<({}, SuccessCode), ErrorCode>", t);
                } else {
                    t = format!("core::result::Result<SuccessCode, ErrorCode>");
                }
            } else if let Some(o) = &rt.other_return {
                if t.is_empty() {
                    t = o.clone();
                } else {
                    t = format!("({}, {})", t, o);
                }
            }

            if !t.is_empty() {
                write!(f, "-> {} ", t)?;
            }
        }

        if &self.name == "create_shared_swapchains_khr" {
           write!(f, "where <S as IntoIterator>::IntoIter: Clone ")?;
        } else
        if needs_next_chain.is_some() {
            write!(f, "where {} ", generic_requirements.join(", "))?;
        }

        let mut uses_parent = false;
        let p_str = if &self_handle.name.0 == &self.raw_parameters[0].type_name.0
            || &self.name == "allocate_command_buffers" || &self.name == "allocate_descriptor_sets" || self.name == "create_video_session_parameters_khr" {
            "&self.data"
        } else {
            uses_parent = true;
            "&parent.data"
        };

        let mut added_parent = false;
        for (i, rp) in self.reverse_parameters.iter().enumerate() {
            match rp {
                OxidisedReversedParameterType::ArrayPtr { ptr_from_index } => {
                    let p = &self.raw_parameters[i].pointers[0];
                    if PointerType::Mut == p.ty {
                        if ParameterValueType::Handle == self.raw_parameters[i].v_type {
                            let handle = handle_map.get(&self.raw_parameters[i].type_name.0).unwrap();

                            if &self.name != "create_shared_swapchains_khr" {
                            // {
                                if let Some(parent) = &handle.parent {
                                    if uses_parent {
                                        if !added_parent {
                                            f = f.replacen("&self", &format!("&self, parent: &{}", self_handle.parent.as_ref().unwrap().1), 1);
                                            added_parent = true;
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
                _ => {}
            }
        }

        writeln!(f, "{{")?;

        if !with_alloc && with_alloc_exists && !with_output {
            let g = generics.iter().filter_map(|g| g.split_once(':').map(|g| g.0)).collect::<Vec<&str>>().join(", ");
            if !g.is_empty() {
                write!(f, "{INDENT}{INDENT}self.{}_with_alloc::<{g}>(", self.name)?;
            } else {
                write!(f, "{INDENT}{INDENT}self.{}_with_alloc(", self.name)?;
            }

            if added_parent {
                write!(f, "parent, ")?;
            }

            for (i, rp) in self.reverse_parameters.iter().enumerate() {
                match rp {
                    OxidisedReversedParameterType::Value { original_index } => {
                        if let Some(p) = self.raw_parameters[i].pointers.first() {
                            if PointerType::Mut == p.ty {
                                match &self.raw_parameters[i].v_type {
                                    ParameterValueType::Handle => {
                                        let handle = handle_map.get(&self.raw_parameters[i].type_name.0).unwrap();
                                        for add_par in &handle.additional_parents {
                                            if self.raw_parameters[i].extern_sync {
                                                write!(f, "/* TODO */")?;
                                            }
                                            write!(f, "{}, ", add_par.1.to_snake_case())?;
                                        }
                                    }
                                    _ => {}
                                }
                            }
                        }
                    }
                    _ => {}
                }
            }

            if &self_handle.name.1 == "CommandBuffer" && !unchecked_external_sync {
                write!(f, "command_pool, ")?;
            }

            if &self.name == "unmap_memory" {
                write!(f, "mapped_memory, ")?;
            }

            for p in self.parameters.iter().skip(if self.first_param_is_handle {
                if self.raw_parameters.len() > 1 && &self_handle.name.0 == &self.raw_parameters[1].type_name.0 {
                    2
                } else {
                    1
                }
            } else { 0 }) {
                if p.name == "allocator" {
                    write!(f, "AllocMode::InheritParent, ")?;
                } else {
                    write!(f, "{}, ", p.name)?;
                }
            }

            if &self.name == "create_shared_swapchains_khr" {
                write!(f, "surfaces, ")?;
            }

            f = f.trim_end_matches(", ").to_string();
            writeln!(f, ")")?;

            write!(f, "{INDENT}}}")?;

            return Ok(f);
        }

        if with_output {
            if enumerate {
                assert_eq!(1, self.raw_parameters.iter().filter(|p| !p.is_length_for.is_empty()).count());
                let length_param_index = self.raw_parameters.iter().enumerate().find_map(|(i, p)| (!p.is_length_for.is_empty()).then(|| i)).unwrap();

                let array_param_indices = self.raw_parameters[length_param_index].is_length_for.iter().map(|v| v.0).collect::<Vec<_>>();

                if self.raw_parameters[length_param_index].type_name.1 != "usize" {
                    writeln!(f, "{0}{0}let mut {1} = {2}.get_slice_len() as {3};", INDENT, self.raw_parameters[length_param_index].name.1, self.raw_parameters[array_param_indices[0]].name.1, self.raw_parameters[length_param_index].type_name.1)?;
                } else {
                    writeln!(f, "{0}{0}let mut {1} = {2}.get_slice_len();", INDENT, self.raw_parameters[length_param_index].name.1, self.raw_parameters[array_param_indices[0]].name.1)?;
                }

                {
                    if self.return_type.as_ref().unwrap().success_codes.len() < 2 {
                        write!(f, "{0}{0}(self.data.{1})(", INDENT, self.name)?;
                    } else {
                        write!(f, "{0}{0}let result = (self.data.{1})(", INDENT, self.name)?;
                    }
                    for (i, rp) in self.reverse_parameters.iter().enumerate() {
                        if i != 0 {
                            write!(f, ", ")?;
                        }
                        if i == 0 && self.first_param_is_handle {
                            write!(f, "self")?;
                            if &self_handle.name.0 != &self.raw_parameters[0].type_name.0 {
                                write!(f, ".data.{}", self.raw_parameters[0].name.1)?;
                            } else {
                                if self.raw_parameters[0].extern_sync {
                                    write!(f, ".as_mut_handle().handle")?;
                                } else {
                                    write!(f, ".handle")?;
                                }
                            }
                        } else if i == length_param_index {
                            write!(f, "core::ptr::NonNull::from(&mut {})", self.raw_parameters[i].name.1)?;
                        } else if array_param_indices.contains(&i) {
                            if array_param_indices.len() == 2 {
                                if array_param_indices[0] == i {
                                    write!(f, "{}.first_mut_1().map(|p| core::ptr::NonNull::from(p))", self.raw_parameters[array_param_indices[0]].name.1)?;
                                } else {
                                    write!(f, "{}.first_mut_2().map(|p| core::ptr::NonNull::from(p))", self.raw_parameters[array_param_indices[0]].name.1)?;
                                }
                            } else {
                                write!(f, "{}.first_mut().map(|p| core::ptr::NonNull::from(p))", self.raw_parameters[i].name.1)?;
                            }
                        } else {
                            match rp {
                                OxidisedReversedParameterType::Value { original_index } => {
                                    if let Some(p) = self.raw_parameters[i].pointers.first() {
                                        if original_index.is_some() && !self.raw_parameters[i].generic_parameters.is_empty() {
                                            write!(f, "unsafe {{ core::mem::transmute(")?;
                                        }
                                        if PointerType::Mut == p.ty {
                                            if original_index.is_some() && self.parameters[i].not_return {
                                                write!(f, "{}", self.raw_parameters[i].name.1)?;
                                            } else {
                                                write!(f, "core::ptr::NonNull::from(&mut {})", self.raw_parameters[i].name.1)?;
                                            }
                                        } else {
                                            if &self.raw_parameters[i].name.0 == "pAllocator" {
                                                write!(f, "None")?;
                                            } else {
                                                if p.optional {
                                                    // write!(f, "Some(")?;
                                                }
                                                write!(f, "{}", self.raw_parameters[i].name.1)?;
                                                if p.optional {
                                                    // write!(f, ")")?;
                                                }
                                            }
                                        }
                                        if original_index.is_some() && !self.raw_parameters[i].generic_parameters.is_empty() {
                                            write!(f, ") }}", )?;
                                        }
                                    } else {
                                        let mut is_self = false;
                                        if i == 1 && &self_handle.name.0 == &self.raw_parameters[1].type_name.0 {
                                            write!(f, "self")?;
                                            is_self = true;
                                        } else {
                                            write!(f, "{}", self.raw_parameters[i].name.1)?;
                                        }
                                        if self.raw_parameters[i].full_type.1.starts_with("Raw") {
                                            write!(f, ".into_raw()")?;
                                        }
                                        match self.raw_parameters[i].v_type {
                                            ParameterValueType::Handle => {
                                                if self.raw_parameters[i].extern_sync {
                                                    if self.raw_parameters[i].value_optional && !is_self {
                                                        write!(f, ".map_or(Handle::null(), |handle| handle.as_mut_handle().handle)")?;
                                                    } else {
                                                        write!(f, ".as_mut_handle().handle")?
                                                    }
                                                } else {
                                                    if self.raw_parameters[i].value_optional && !is_self {
                                                        write!(f, ".map_or(Handle::null(), |handle| handle.handle)")?;
                                                    } else {
                                                        write!(f, ".handle")?
                                                    }
                                                }
                                            }
                                            _ => {}
                                        };
                                    }
                                }
                                OxidisedReversedParameterType::ArrayPtr { ptr_from_index } => {}
                                OxidisedReversedParameterType::ArrayLen { len_from_index } => {}
                            }
                        }
                    }
                    writeln!(f, ");")?;
                }
                writeln!(f, "{INDENT}{INDENT}{}.set_len({});", self.raw_parameters[array_param_indices[0]].name.1, self.raw_parameters[length_param_index].name.1)?;

                if self.return_type.as_ref().unwrap().success_codes.len() >= 2 {
                    writeln!(f, "{0}{0}if result.0 >= 0  {{", INDENT)?;
                    writeln!(f, "{0}{0}{0}Ok(result.into_success().normalise())", INDENT)?;
                    writeln!(f, "{0}{0}}} else {{", INDENT)?;
                    writeln!(f, "{0}{0}{0}Err(result.into_error().normalise())", INDENT)?;
                    writeln!(f, "{0}{0}}}", INDENT)?;
                }
            } else {
                match self.name.as_str() {
                    "allocate_command_buffers" => {
                        writeln!(f, "{0}{0}let mut allocate_info = core::ptr::NonNull::from(allocate_info);", INDENT)?;
                        writeln!(f, "{0}{0}unsafe {{ allocate_info.as_mut() }}.command_pool = CommandPoolMutHandle {{ handle: self.handle.to_mut(), _phantom: Default::default() }};", INDENT)?;
                    }
                    "allocate_descriptor_sets" => {
                        writeln!(f, "{0}{0}let mut allocate_info = core::ptr::NonNull::from(allocate_info);", INDENT)?;
                        writeln!(f, "{0}{0}unsafe {{ allocate_info.as_mut() }}.descriptor_pool = DescriptorPoolMutHandle {{ handle: self.handle.to_mut(), _phantom: Default::default() }};", INDENT)?;
                    }
                    "create_video_session_parameters_khr" => {
                        // writeln!(f, "{0}{0}create_info.video_session = self.as_handle();", INDENT)?;
                        writeln!(f, "{0}{0}let mut create_info = core::ptr::NonNull::from(create_info);", INDENT)?;
                        writeln!(f, "{0}{0}unsafe {{ create_info.as_mut() }}.video_session = VideoSessionKHRHandle {{ handle: self.handle, _phantom: Default::default() }};", INDENT)?;
                    }
                    "create_swapchain_khr" => {
                        writeln!(f, "{0}{0}let mut create_info = core::ptr::NonNull::from(create_info);", INDENT)?;
                        writeln!(f, "{0}{0}unsafe {{ create_info.as_mut() }}.surface = surface_khr.as_mut_handle();", INDENT)?;
                    }
                    "create_shared_swapchains_khr" => {
                        writeln!(f, "{0}{0}let create_infos: &mut [RawSwapchainCreateInfoKHR] = unsafe {{ std::mem::transmute(create_infos) }}; // Hack the lifetimes", INDENT)?;
                        writeln!(f, "{0}{0}let iter = surfaces.into_iter();", INDENT)?;
                        writeln!(f, "{0}{0}for (i, s) in iter.clone().enumerate() {{", INDENT)?;
                        writeln!(f, "{0}{0}{0}create_infos[i].surface = s.as_mut_handle();", INDENT)?;
                        writeln!(f, "{0}{0}}}", INDENT)?;
                    }
                    "get_device_queue" => {
                        writeln!(f, "{0}{0}assert!(self.data.acquired_queues.lock().unwrap().insert((queue_family_index, queue_index, DeviceQueueCreateFlags::eNone)), \"Must only get a queue once, if you want multiple handles then use something like Arc<Queue>\");", INDENT)?;
                    }
                    "get_device_queue2" => {
                        writeln!(f, "{0}{0}assert!(self.data.acquired_queues.lock().unwrap().insert((queue_info.queue_family_index, queue_info.queue_index, queue_info.flags)), \"Must only get a queue once, if you want multiple handles then use something like Arc<Queue>\");", INDENT)?;
                    }
                    "cmd_set_sample_mask_ext" => {
                        writeln!(f, "{0}{0}assert_eq!(sample_mask.len(), ((u32::from(samples) + 31) / 32) as usize, \"The length of sample_mask must be equal to ceil(rasterization_samples/32), however {{}} != {{}}\", sample_mask.len(), (u32::from(samples) + 31) / 32);", INDENT)?;
                        writeln!(f, "{0}{0}let samples = samples.into_raw();", INDENT)?;
                    }
                    _ => {}
                }

                let mut optional = vec![false; self.parameters.len()];

                for i in 0..self.raw_parameters.len() {
                    if let OxidisedReversedParameterType::ArrayPtr { ptr_from_index } = self.reverse_parameters[i] {
                        if let Some(j) = ptr_from_index {
                            if self.raw_parameters[i].pointers[0].optional {
                                optional[j] = true;
                            }
                        }
                    }
                }

                for (ri, r) in self.reverse_parameters.iter().enumerate() {
                    if let OxidisedReversedParameterType::ArrayLen { len_from_index } = r {
                        if let Some(len_from_index) = len_from_index {
                            let in_len: Vec<_> = len_from_index.iter().filter(|l| !l.1).collect();

                            if in_len.len() > 1 {
                                let p0_optional = optional[in_len[0].0];
                                let i_range = if p0_optional { in_len.len() } else { 1 };

                                for i in 0..i_range {
                                    let pi_optional = optional[in_len[i].0];

                                    for j in i + 1..in_len.len() {
                                        let pj_optional = optional[in_len[j].0];

                                        match (pi_optional, pj_optional) {
                                            (true, true) => {
                                                writeln!(f, "{0}{0}assert!({1}.len() == 0 || {2}.len() == 0 || ({1}.len() == {2}.len()), \"{1} and {2} must have the same length, or either be empty.\");", INDENT, self.parameters[in_len[i].0].name, self.parameters[in_len[j].0].name)?;
                                            }
                                            (true, false) => {
                                                writeln!(f, "{0}{0}assert!({1}.len() == 0 || ({1}.len() == {2}.len()), \"{1} and {2} must have the same length, or {1} be empty.\");", INDENT, self.parameters[in_len[i].0].name, self.parameters[in_len[j].0].name)?;
                                            }
                                            (false, true) => {
                                                writeln!(f, "{0}{0}assert!({2}.len() == 0 || ({1}.len() == {2}.len()), \"{1} and {2} must have the same length, or {2} be empty.\");", INDENT, self.parameters[in_len[i].0].name, self.parameters[in_len[j].0].name)?;
                                            }
                                            (false, false) => {
                                                writeln!(f, "{0}{0}assert_eq!({1}.len(), {2}.len(), \"{1} and {2} must have the same length.\");", INDENT, self.parameters[in_len[i].0].name, self.parameters[in_len[j].0].name)?;
                                            }
                                        }
                                    }
                                }
                            }

                            if !self.raw_parameters[ri].value_optional {
                                if let Some(i) = in_len.iter().find(|l| !optional[l.0]) {
                                    writeln!(f, "{0}{0}assert!({1}.len() > 0, \"{2} length must be > 0.\");", INDENT, self.parameters[i.0].name, self.raw_parameters[ri].name.1)?;
                                }
                            }
                        } else {
                            eprintln!("Warn: {:?}", self);
                        }
                    }
                }

                for p in &self.raw_parameters {
                    for l in &p.is_length_for {
                        if let Some(v) = &l.2 {
                            if self.raw_parameters[l.0].pointers[0].ty == PointerType::Const {
                                writeln!(f, "{0}{0}assert_eq!({1}.{2} as usize, {3}.len(), \"{1}.{2} and {3} length must match.\");", INDENT, p.name.1, v, self.raw_parameters[l.0].name.1)?;
                            }
                        }
                    }
                }

                if &self.name == "map_memory" {
                    writeln!(f, "{0}{0}assert_ne!(size, WHOLE_SIZE, \"For memory safety purposes the use of vk::WHOLE_SIZE is not supported here.\");", INDENT)?;
                }

                for p in &self.parameters {
                    match (self.name.as_str(), p.name.as_str()) {
                        ("build_acceleration_structures_khr", "build_range_infos") | ("cmd_build_acceleration_structures_khr", "build_range_infos") | ("cmd_build_acceleration_structures_indirect_khr", "max_primitive_counts") => {
                            writeln!(f, "{0}{0}assert!(infos.iter().zip({1}.iter()).all(|(a, b)| a.geometry_count as usize == b.len()), \"The length of each sub slice must match that provide in the corresponding infos[i] struct\");", INDENT, p.name)?;
                            writeln!(f, "{0}{0}let {1} = {1}.iter().map(|infos| infos.first().expect(\"Each sub slice must have a length > 0\")).collect::<V::V<_>>();\n", INDENT, p.name)?;
                        }
                        _ => {}
                    }
                }

                if &self_handle.name.1 == "CommandBuffer" && !unchecked_external_sync {
                    writeln!(f, "{0}{0}assert_eq!(self.data.command_pool, command_pool.handle, \"Must provide a ref to the command pool this command buffer was allocated from, this is to enforce thread safety requirements (external sync)\");", INDENT)?;
                }

                if with_alloc {
                    let v = if destructor_uses_alloc {
                        "self.data.allocator"
                    } else {
                        if &self.name == "create_instance" {
                            "None"
                        } else if &self_handle.name.0 == &self.raw_parameters[0].type_name.0 {
                            "self.data.allocator"
                        } else {
                            "self.data.allocator"
                        }
                    };

                    writeln!(f, "{0}{0}let allocator = alloc_mode.apply({1});", INDENT, v)?;
                }

                write!(f, "{0}{0}", INDENT)?;
                if let Some(rt) = &self.return_type {
                    if rt.has_result || rt.other_return.is_some() {
                        write!(f, "let result = ")?;
                    }
                }
                write!(f, "(self.data.{})(", self.name)?;
                for (i, rp) in self.reverse_parameters.iter().enumerate() {
                    if i != 0 {
                        write!(f, ", ")?;
                    }
                    if i == 0 && self.first_param_is_handle {
                        write!(f, "self")?;
                        if &self_handle.name.0 != &self.raw_parameters[0].type_name.0 {
                            write!(f, ".data.{}", self.raw_parameters[0].name.1)?;
                        } else {
                            if self.raw_parameters[0].extern_sync {
                                write!(f, ".as_mut_handle().handle")?;
                            } else {
                                write!(f, ".handle")?;
                            }
                        }
                    } else {
                        match rp {
                            OxidisedReversedParameterType::Value { original_index } => {
                                if let Some(p) = self.raw_parameters[i].pointers.first() {
                                    if original_index.is_some() && !self.raw_parameters[i].generic_parameters.is_empty() {
                                        write!(f, "unsafe {{ core::mem::transmute(")?;
                                    }
                                    if PointerType::Mut == p.ty {
                                        if original_index.is_some() && self.parameters[i].not_return {
                                            // write!(f, "core::ptr::NonNull::from({})", self.raw_parameters[i].name.1)?;
                                            match self.name.as_str() {
                                                "allocate_command_buffers" => write!(f, "{}", self.raw_parameters[i].name.1)?,
                                                "allocate_descriptor_sets" => write!(f, "{}", self.raw_parameters[i].name.1)?,
                                                "create_video_session_parameters_khr" => write!(f, "{}", self.raw_parameters[i].name.1)?,
                                                _ => write!(f, "core::ptr::NonNull::from({})", self.raw_parameters[i].name.1)?,
                                            };
                                        } else {
                                            write!(f, "core::ptr::NonNull::from({})", self.raw_parameters[i].name.1)?;
                                        }
                                    } else {
                                        if &self.raw_parameters[i].name.0 == "pAllocator" {
                                            if is_destructor {
                                                write!(f, "self.data.allocator.as_ref().map(|a| a.as_general())")?;
                                            } else if with_alloc {
                                                write!(f, "allocator.as_ref()")?;
                                            } else {
                                                write!(f, "None")?;
                                            }
                                        } else {
                                            if p.optional {
                                                // write!(f, "Some(")?;
                                            }
                                            if self.raw_parameters[i].full_type.1.starts_with("&mut") {
                                                match self.name.as_str() {
                                                    "allocate_command_buffers" => write!(f, "{}", self.raw_parameters[i].name.1)?,
                                                    "allocate_descriptor_sets" => write!(f, "{}", self.raw_parameters[i].name.1)?,
                                                    _ => write!(f, "core::ptr::NonNull::from({})", self.raw_parameters[i].name.1)?,
                                                };
                                                // write!(f, "core::ptr::NonNull::from({})", self.raw_parameters[i].name.1)?;
                                            } else {
                                                write!(f, "{}", self.raw_parameters[i].name.1)?;
                                            }
                                            if p.optional {
                                                // write!(f, ")")?;
                                            }
                                        }
                                    }
                                    if original_index.is_some() && !self.raw_parameters[i].generic_parameters.is_empty() {
                                        write!(f, ") }}", )?;
                                    }
                                } else {
                                    let mut is_self = false;
                                    if i == 1 && &self_handle.name.0 == &self.raw_parameters[1].type_name.0 {
                                        write!(f, "self")?;
                                        is_self = true;
                                    } else {
                                        write!(f, "{}", self.raw_parameters[i].name.1)?;
                                    }
                                    if self.raw_parameters[i].full_type.1.starts_with("Raw") {
                                        write!(f, ".into_raw()")?;
                                    }
                                    match self.raw_parameters[i].v_type {
                                        ParameterValueType::Handle => {
                                            if self.raw_parameters[i].extern_sync {
                                                if self.raw_parameters[i].value_optional && !is_self {
                                                    write!(f, ".map_or(Handle::null(), |handle| handle.as_mut_handle().handle)")?;
                                                } else {
                                                    write!(f, ".as_mut_handle().handle")?
                                                }
                                            } else {
                                                if self.raw_parameters[i].value_optional && !is_self {
                                                    write!(f, ".map_or(Handle::null(), |handle| handle.handle)")?;
                                                } else {
                                                    write!(f, ".handle")?
                                                }
                                            }
                                        },
                                        _ => {}
                                    };
                                }
                            }
                            OxidisedReversedParameterType::ArrayPtr { ptr_from_index } => {
                                // if self.raw_parameters[i].pointers[0].optional {
                                //     write!(f, "Some(")?;
                                // }

                                let needs_handle_to_raw = self.raw_parameters[i].v_type == ParameterValueType::Handle && !(self.raw_parameters[i].pointers[0].ty == PointerType::Mut && (ptr_from_index.is_none() || !self.parameters[ptr_from_index.unwrap()].not_return));

                                if !self.raw_parameters[i].pointers[0].optional {
                                    if needs_handle_to_raw {
                                        if self.raw_parameters[i].full_type.1.contains("&mut") {
                                            // write!(f, "&mut ")?;
                                        } else {
                                            write!(f, "&")?;
                                        }
                                    }
                                }
                                let mut handled_handle = false;
                                if self.raw_parameters[i].full_type.1.contains("&mut") {
                                    if needs_handle_to_raw {
                                        write!(f, "{}.first_mut().map(|p| core::ptr::NonNull::from(&mut p.handle))", self.raw_parameters[i].name.1)?;
                                        handled_handle = true;
                                    } else {
                                        write!(f, "{}.first_mut().map(|p| core::ptr::NonNull::from(p))", self.raw_parameters[i].name.1)?;
                                    }
                                } else {
                                    match self.raw_parameters[i].pointers[0].ty {
                                        PointerType::Const => write!(f, "{}.first()", self.raw_parameters[i].name.1)?,
                                        PointerType::Mut => write!(f, "{}.first_mut()", self.raw_parameters[i].name.1)?
                                    }
                                }
                                if !self.raw_parameters[i].pointers[0].optional {
                                    write!(f, ".unwrap()")?;
                                    if needs_handle_to_raw && !handled_handle {
                                        write!(f, ".handle")?;
                                    }
                                } else if needs_handle_to_raw && !handled_handle {
                                    write!(f, ".map(|v| &v.handle)")?;
                                }
                                // if self.raw_parameters[i].pointers[0].optional {
                                //     write!(f, ")")?;
                                // }
                            }
                            OxidisedReversedParameterType::ArrayLen { len_from_index } => {
                                if self.raw_parameters[i].v_type == ParameterValueType::Integer {
                                    if let Some(pick) = len_from_index.as_ref().map(|l| l.iter().find(|l| !l.1)).flatten() {
                                        if &self.raw_parameters[i].type_name.1 != "usize" {
                                            write!(f, "{}.len() as {}", self.parameters[pick.0].name, self.raw_parameters[i].type_name.1)?;
                                        } else {
                                            write!(f, "{}.len()", self.parameters[pick.0].name)?;
                                        }
                                    } else {
                                        // write!(f, "oops")?;
                                        write!(f, "{}", self.raw_parameters[i].name.1)?;
                                    }
                                } else {
                                    if self.raw_parameters[i].full_type.1.starts_with("&mut") {
                                        // write!(f, "core::ptr::NonNull::from({})", self.raw_parameters[i].name.1)?;
                                        match self.name.as_str() {
                                            "allocate_command_buffers" => write!(f, "{}", self.raw_parameters[i].name.1)?,
                                            "allocate_descriptor_sets" => write!(f, "{}", self.raw_parameters[i].name.1)?,
                                            "create_video_session_parameters_khr" => write!(f, "{}", self.raw_parameters[i].name.1)?,
                                            _ => write!(f, "core::ptr::NonNull::from({})", self.raw_parameters[i].name.1)?,
                                        };
                                    } else {
                                        write!(f, "{}", self.raw_parameters[i].name.1)?;
                                    }
                                }
                            }
                        }
                    }
                }
                writeln!(f, ");")?;

                match self.name.as_str() {
                    "allocate_command_buffers" => {
                        writeln!(f, "{0}{0}unsafe {{ allocate_info.as_mut() }}.command_pool = Handle::null();", INDENT)?;
                    }
                    "allocate_descriptor_sets" => {
                        writeln!(f, "{0}{0}unsafe {{ allocate_info.as_mut() }}.descriptor_pool = Handle::null();", INDENT)?;
                    }
                    "create_video_session_parameters_khr" => {
                        // writeln!(f, "{0}{0}create_info.video_session = Handle::null();", INDENT)?;
                        writeln!(f, "{0}{0}unsafe {{ create_info.as_mut() }}.video_session = Handle::null();", INDENT)?;
                    }
                    "create_swapchain_khr" => {
                        writeln!(f, "{0}{0}unsafe {{ create_info.as_mut() }}.surface = Handle::null();", INDENT)?;
                    }
                    _ => {}
                }


                let mut extra_indent = "".to_string();

                if let Some(rt) = &self.return_type {
                    if rt.has_result {
                        writeln!(f, "{1}{0}{0}if result.0 >= 0 {{", INDENT, extra_indent)?;
                        extra_indent += INDENT;
                    }
                }

                // let mut uses_parent = false;
                // let p_str = if &self_handle.name.0 == &self.raw_parameters[0].type_name.0
                //     || &self.name == "allocate_command_buffers" || &self.name == "allocate_descriptor_sets" {
                //     "&self.data"
                // } else {
                //     uses_parent = true;
                //     "&parent.data"
                // };

                // for (i, rp) in self.reverse_parameters.iter().enumerate() {
                //     match rp {
                //         OxidisedReversedParameterType::Value { original_index } => {
                //             if let Some(p) = self.raw_parameters[i].pointers.first() {
                //                 if PointerType::Mut == p.ty {
                //                     match &self.raw_parameters[i].v_type {
                //                         ParameterValueType::Handle => {
                //                             let handle = handle_map.get(&self.raw_parameters[i].type_name.0).unwrap();
                //
                //                             write!(f, "{0}{0}{3}let {1} = {2}::new({1}, ", INDENT, self.raw_parameters[i].name.1, self.raw_parameters[i].type_name.1.replace("Raw", ""), extra_indent)?;
                //                             if let Some(loads_fn_ptrs) = &handle.loads_fn_ptrs {
                //                                 if let Some(parent) = &handle.parent {
                //                                     write!(f, "&self.data, ")?;
                //                                 }
                //                                 for add_par in &handle.additional_parents {
                //                                     write!(f, "&{}.data, ", add_par.1.to_snake_case())?;
                //                                 }
                //                                 if handle.destructor_uses_alloc {
                //                                     if with_alloc {
                //                                         write!(f, "allocator, ")?;
                //                                     } else {
                //                                         write!(f, "None, ")?;
                //                                     }
                //                                 }
                //                                 if handle.destructor.is_some() && !handle.destructor_uses_slice {
                //                                     writeln!(f, "self.data.{}, true);", loads_fn_ptrs.to_snake_case())?;
                //                                 } else {
                //                                     writeln!(f, "self.data.{});", loads_fn_ptrs.to_snake_case())?;
                //                                 }
                //                             } else {
                //                                 if let Some(parent) = &handle.parent {
                //                                     write!(f, "&self.data, ")?;
                //                                 }
                //                                 for add_par in &handle.additional_parents {
                //                                     write!(f, "&{}.data, ", add_par.1.to_snake_case())?;
                //                                 }
                //                                 if handle.destructor_uses_alloc {
                //                                     if with_alloc {
                //                                         write!(f, "allocator")?;
                //                                     } else {
                //                                         write!(f, "None")?;
                //                                     }
                //                                 }
                //                                 f = f.trim_end_matches(", ").to_string();
                //                                 if handle.destructor.is_some() && !handle.destructor_uses_slice {
                //                                     writeln!(f, ", true);")?;
                //                                 } else {
                //                                     writeln!(f, ");")?;
                //                                 }
                //                             }
                //                         }
                //                         _ => {}
                //                     }
                //                 }
                //             }
                //             if let Some(n) = &needs_next_chain {
                //                 if i == n.1 {
                //                     writeln!(f, "{INDENT}{INDENT}{extra_indent}{}.recursive_clear_next_mark_init(uninit_count);", self.raw_parameters[i].name.1)?;
                //                 }
                //             }
                //         }
                //         OxidisedReversedParameterType::ArrayPtr { ptr_from_index } => {
                //             let p = &self.raw_parameters[i].pointers[0];
                //             if PointerType::Mut == p.ty {
                //                 if ParameterValueType::Handle == self.raw_parameters[i].v_type {
                //                     let handle = handle_map.get(&self.raw_parameters[i].type_name.0).unwrap();
                //
                //                     if &self.name == "create_shared_swapchains_khr" {
                //                         writeln!(f,
                //                                  "\
                //                      {0}{0}{1}let mut surfaces = iter;\n\
                //                      {0}{0}{1}let swapchains = swapchains.into_iter().enumerate().map(|(i, handle)| {{\n\
                //                      {0}{0}{0}{1}let surface = surfaces.next().expect(\"There must be a surface corresponding to each create info\");\n\
                //                      {0}{0}{0}{1}create_infos[i].surface = Handle::null();\n\
                //                      {0}{0}{0}{1}SwapchainKHR::new(handle, &self.data, &surface.data, None, true)\n\
                //                      {0}{0}{1}}}).collect();", INDENT, extra_indent
                //                         )?;
                //                     } else {
                //                         write!(f, "{0}{0}{3}let {1} = {1}.into_iter().map(|handle| {2}::new(handle, ", INDENT, self.raw_parameters[i].name.1, self.raw_parameters[i].type_name.1.replace("Raw", ""), extra_indent)?;
                //                         if let Some(loads_fn_ptrs) = &handle.loads_fn_ptrs {
                //                             if let Some(parent) = &handle.parent {
                //                                 if uses_parent {
                //                                     // f = f.replacen("&self", &format!("&self, parent: &{}", self_handle.parent.as_ref().unwrap().1), 1);
                //                                 }
                //                                 write!(f, "{}, ", p_str)?;
                //                             }
                //                             for add_par in &handle.additional_parents {
                //                                 write!(f, "&{}.data, ", add_par.1.to_snake_case())?;
                //                             }
                //                             if handle.destructor_uses_alloc {
                //                                 write!(f, "None, ")?;
                //                             }
                //                             write!(f, "self.data.{}", loads_fn_ptrs.to_snake_case())?;
                //                         } else {
                //                             if let Some(parent) = &handle.parent {
                //                                 if uses_parent {
                //                                     // f = f.replacen("&self", &format!("&self, parent: &{}", self_handle.parent.as_ref().unwrap().1), 1);
                //                                 }
                //                                 write!(f, "{}, ", p_str)?;
                //                             }
                //                             for add_par in &handle.additional_parents {
                //                                 write!(f, "&{}.data, ", add_par.1.to_snake_case())?;
                //                             }
                //                             if handle.destructor_uses_alloc {
                //                                 write!(f, "None")?;
                //                             }
                //                             f = f.trim_end_matches(", ").to_string();
                //                             // writeln!(f, ");")?;
                //                         }
                //                         if handle.destructor.is_some() && !handle.destructor_uses_slice {
                //                             writeln!(f, ", true)).collect();")?;
                //                         } else {
                //                             writeln!(f, ")).collect();")?;
                //                         }
                //                     }
                //                 }
                //             }
                //         }
                //         OxidisedReversedParameterType::ArrayLen { len_from_index } => {}
                //     }
                // }

                // if &self.name == "map_memory" {
                //     //        let pp_data = if let Some(pp_data) = pp_data {
                //     //             unsafe { core::slice::from_raw_parts_mut(pp_data as *mut u8, size as usize) }
                //     //         } else {
                //     //             &mut [][..]
                //     //         };
                //     writeln!(f, "{0}{0}{1}let data = if let Some(data) = data {{", INDENT, extra_indent)?;
                //     writeln!(f, "{0}{0}{0}{1}unsafe {{ core::slice::from_raw_parts_mut(data.as_ptr(), size as usize) }}", INDENT, extra_indent)?;
                //     writeln!(f, "{0}{0}{1}}} else {{", INDENT, extra_indent)?;
                //     writeln!(f, "{0}{0}{0}{1}&mut [][..]", INDENT, extra_indent)?;
                //     writeln!(f, "{0}{0}{1}}};", INDENT, extra_indent)?;
                //     writeln!(f, "{0}{0}{1}let data = MappedMemory::new(data, self);", INDENT, extra_indent)?;
                // }

                if let Some(rt) = &self.return_type {


                    if rt.has_result {
                        writeln!(f, "{1}{0}{0}Ok(result.into_success().normalise())", INDENT, extra_indent)?;
                        writeln!(f, "{0}{1}}} else {{", INDENT, extra_indent)?;

                        writeln!(f, "{0}{0}{1}Err(result.into_error().normalise())", INDENT, extra_indent)?;
                        writeln!(f, "{0}{1}}}", INDENT, extra_indent)?;
                    } else if let Some(o) = &rt.other_return {
                        writeln!(f, "{0}{0}result", INDENT)?;
                    }
                }
            }


            write!(f, "{INDENT}}}")?;
            return Ok(f);
        }

        if &self.name == "unmap_memory" {
            writeln!(f, "{0}{0}#[cfg(feature=\"automatic_destroy_on_drop\")]", INDENT)?;
            writeln!(f, "{0}{0}core::mem::drop(mapped_memory);", INDENT)?;
            writeln!(f, "{0}{0}#[cfg(not(feature=\"automatic_destroy_on_drop\"))]", INDENT)?;
        }

        if enumerate {
            if self.return_type.as_ref().unwrap().success_codes.len() < 2 {
                //         let mut p_queue_family_properties = Vec::new();
                //         let mut p_queue_family_property_count = 0;
                //         (self.data.get_physical_device_queue_family_properties)(physical_device, &mut p_queue_family_property_count, None);
                //
                //         p_queue_family_properties.resize(p_queue_family_property_count as usize, unsafe { core::mem::MaybeUninit::zeroed().assume_init() });
                //         (self.data.get_physical_device_queue_family_properties)(physical_device, &mut p_queue_family_property_count, p_queue_family_properties.first_mut());
                //         p_queue_family_properties.resize(p_queue_family_property_count as usize, unsafe { core::mem::MaybeUninit::zeroed().assume_init() });

                //         p_queue_family_properties

                assert_eq!(1, self.raw_parameters.iter().filter(|p| !p.is_length_for.is_empty()).count());
                let length_param_index = self.raw_parameters.iter().enumerate().find_map(|(i, p)| (!p.is_length_for.is_empty()).then(|| i)).unwrap();

                let array_param_indices = self.raw_parameters[length_param_index].is_length_for.iter().map(|v| v.0).collect::<Vec<_>>();

                for (k, i) in array_param_indices.iter().enumerate() {
                    if return_needs_norm[k] || return_is_handle[k] {
                        writeln!(f, "{0}{0}let mut {1} = V{k}::new_with();", INDENT, self.raw_parameters[*i].name.1)?;
                    } else {
                        writeln!(f, "{0}{0}let mut {1} = V{k}::new();", INDENT, self.raw_parameters[*i].name.1)?;
                    }
                }

                writeln!(f, "{0}{0}let mut {1} = 0;", INDENT, self.raw_parameters[length_param_index].name.1)?;
                {
                    write!(f, "{0}{0}(self.data.{1})(", INDENT, self.name)?;
                    for (i, rp) in self.reverse_parameters.iter().enumerate() {
                        if i != 0 {
                            write!(f, ", ")?;
                        }
                        if i == 0 && self.first_param_is_handle {
                            write!(f, "self")?;
                            if &self_handle.name.0 != &self.raw_parameters[0].type_name.0 {
                                write!(f, ".data.{}", self.raw_parameters[0].name.1)?;
                            } else {
                                if self.raw_parameters[0].extern_sync {
                                    write!(f, ".as_mut_handle().handle")?;
                                } else {
                                    write!(f, ".handle")?;
                                }
                            }
                        } else if i == length_param_index {
                            write!(f, "core::ptr::NonNull::from(&mut {})", self.raw_parameters[i].name.1)?;
                        } else if array_param_indices.contains(&i) {
                            write!(f, "None")?;
                        } else {
                            match rp {
                                OxidisedReversedParameterType::Value { original_index } => {
                                    if let Some(p) = self.raw_parameters[i].pointers.first() {
                                        if original_index.is_some() && !self.raw_parameters[i].generic_parameters.is_empty() {
                                            write!(f, "unsafe {{ core::mem::transmute(")?;
                                        }
                                        if PointerType::Mut == p.ty {
                                            if original_index.is_some() && self.parameters[i].not_return {
                                                write!(f, "{}", self.raw_parameters[i].name.1)?;
                                            } else {
                                                write!(f, "core::ptr::NonNull::from(&mut {})", self.raw_parameters[i].name.1)?;
                                            }
                                        } else {
                                            if &self.raw_parameters[i].name.0 == "pAllocator" {
                                                write!(f, "None")?;
                                            } else {
                                                if p.optional {
                                                    // write!(f, "Some(")?;
                                                }
                                                write!(f, "{}", self.raw_parameters[i].name.1)?;
                                                if p.optional {
                                                    // write!(f, ")")?;
                                                }
                                            }
                                        }
                                        if original_index.is_some() && !self.raw_parameters[i].generic_parameters.is_empty() {
                                            write!(f, ") }}", )?;
                                        }
                                    } else {
                                        let mut is_self = false;
                                        if i == 1 && &self_handle.name.0 == &self.raw_parameters[1].type_name.0 {
                                            write!(f, "self")?;
                                            is_self = true;
                                        } else {
                                            write!(f, "{}", self.raw_parameters[i].name.1)?;
                                        }
                                        if self.raw_parameters[i].full_type.1.starts_with("Raw") {
                                            write!(f, ".into_raw()")?;
                                        }
                                        match self.raw_parameters[i].v_type {
                                            ParameterValueType::Handle => {
                                                if self.raw_parameters[i].extern_sync {
                                                    if self.raw_parameters[i].value_optional && !is_self {
                                                        write!(f, ".map_or(Handle::null(), |handle| handle.as_mut_handle().handle)")?;
                                                    } else {
                                                        write!(f, ".as_mut_handle().handle")?
                                                    }
                                                } else {
                                                    if self.raw_parameters[i].value_optional && !is_self {
                                                        write!(f, ".map_or(Handle::null(), |handle| handle.handle)")?;
                                                    } else {
                                                        write!(f, ".handle")?
                                                    }
                                                }
                                            },
                                            _ => {}
                                        };
                                    }
                                }
                                OxidisedReversedParameterType::ArrayPtr { ptr_from_index } => {}
                                OxidisedReversedParameterType::ArrayLen { len_from_index } => {}
                            }
                        }
                    }
                    writeln!(f, ");")?;
                }

                writeln!(f, "")?;

                for i in &array_param_indices {

                    let cast = if &self.raw_parameters[length_param_index].type_name.1 != "usize" {
                        " as usize"
                    } else {
                        ""
                    };

                    if let Some(n) = needs_next_chain {
                        writeln!(f, "{0}{0}let mut next_chains = next_chain_provider({1}{2}).into_iter();", INDENT, self.raw_parameters[length_param_index].name.1, cast)?;
                        writeln!(f, "{0}{0}{1}.resize_with({2}{3}, || {{", INDENT, self.raw_parameters[*i].name.1, self.raw_parameters[length_param_index].name.1, cast)?;
                        writeln!(f, "{0}{0}{0}let mut value = {1};", INDENT, self.raw_parameters[*i].uninit(true))?;
                        writeln!(f, "{0}{0}{0}for next in next_chains.next().unwrap().into_iter().rev() {{", INDENT)?;
                        assert!(self.return_type.as_ref().map_or(true, |rt| !rt.has_result), "Need to handle this case");
                        writeln!(f, "{0}{0}{0}{0}next.mark_initialised();", INDENT)?; // NOTE: HACK that assumes above
                        writeln!(f, "{0}{0}{0}{0}value.push_next(next.as_base_in());", INDENT)?;
                        writeln!(f, "{0}{0}{0}}}", INDENT)?;
                        writeln!(f, "{0}{0}{0}value", INDENT)?;
                        writeln!(f, "{0}{0}}});", INDENT)?;
                    } else {
                        writeln!(f, "{0}{0}{1}.resize_with({2}{4}, || {3});", INDENT, self.raw_parameters[*i].name.1, self.raw_parameters[length_param_index].name.1, self.raw_parameters[*i].uninit(true), cast)?;
                    }
                }
                {
                    write!(f, "{0}{0}(self.data.{1})(", INDENT, self.name)?;
                    for (i, rp) in self.reverse_parameters.iter().enumerate() {
                        if i != 0 {
                            write!(f, ", ")?;
                        }
                        if i == 0 && self.first_param_is_handle {
                            write!(f, "self")?;
                            if &self_handle.name.0 != &self.raw_parameters[0].type_name.0 {
                                write!(f, ".data.{}", self.raw_parameters[0].name.1)?;
                            } else {
                                if self.raw_parameters[0].extern_sync {
                                    write!(f, ".as_mut_handle().handle")?;
                                } else {
                                    write!(f, ".handle")?;
                                }
                            }
                        } else if i == length_param_index {
                            write!(f, "core::ptr::NonNull::from(&mut {})", self.raw_parameters[i].name.1)?;
                        } else if array_param_indices.contains(&i) {
                            if self.raw_parameters[i].v_type != ParameterValueType::Other || input_param.contains(&i) {
                                write!(f, "{}.first_mut().map(|p| core::ptr::NonNull::from(p))", self.raw_parameters[i].name.1)?;
                            } else {
                                write!(f, "{}.first_mut().map(|p| nonnull_maybe_uninit(p))", self.raw_parameters[i].name.1)?;
                            }
                        } else {
                            match rp {
                                OxidisedReversedParameterType::Value { original_index } => {
                                    if let Some(p) = self.raw_parameters[i].pointers.first() {
                                        if original_index.is_some() && !self.raw_parameters[i].generic_parameters.is_empty() {
                                            write!(f, "unsafe {{ core::mem::transmute(")?;
                                        }
                                        if PointerType::Mut == p.ty {
                                            if original_index.is_some() && self.parameters[i].not_return {
                                                write!(f, "{}", self.raw_parameters[i].name.1)?;
                                            } else {
                                                write!(f, "core::ptr::NonNull::from(&mut {})", self.raw_parameters[i].name.1)?;
                                            }
                                        } else {
                                            if &self.raw_parameters[i].name.0 == "pAllocator" {
                                                write!(f, "None")?;
                                            } else {
                                                if p.optional {
                                                    // write!(f, "Some(")?;
                                                }
                                                write!(f, "{}", self.raw_parameters[i].name.1)?;
                                                if p.optional {
                                                    // write!(f, ")")?;
                                                }
                                            }
                                        }
                                        if original_index.is_some() && !self.raw_parameters[i].generic_parameters.is_empty() {
                                            write!(f, ") }}", )?;
                                        }
                                    } else {
                                        let mut is_self = false;
                                        if i == 1 && &self_handle.name.0 == &self.raw_parameters[1].type_name.0 {
                                            write!(f, "self")?;
                                            is_self = true;
                                        } else {
                                            write!(f, "{}", self.raw_parameters[i].name.1)?;
                                        }
                                        if self.raw_parameters[i].full_type.1.starts_with("Raw") {
                                            write!(f, ".into_raw()")?;
                                        }
                                        match self.raw_parameters[i].v_type {
                                            ParameterValueType::Handle => {
                                                if self.raw_parameters[i].extern_sync {
                                                    if self.raw_parameters[i].value_optional && !is_self {
                                                        write!(f, ".map_or(Handle::null(), |handle| handle.as_mut_handle().handle)")?;
                                                    } else {
                                                        write!(f, ".as_mut_handle().handle")?
                                                    }
                                                } else {
                                                    if self.raw_parameters[i].value_optional && !is_self {
                                                        write!(f, ".map_or(Handle::null(), |handle| handle.handle)")?;
                                                    } else {
                                                        write!(f, ".handle")?
                                                    }
                                                }
                                            },
                                            _ => {}
                                        };
                                    }
                                }
                                OxidisedReversedParameterType::ArrayPtr { ptr_from_index } => {}
                                OxidisedReversedParameterType::ArrayLen { len_from_index } => {}
                            }
                        }
                    }
                    writeln!(f, ");")?;
                }

                writeln!(f, "")?;

                for i in &array_param_indices {
                    if self.raw_parameters[*i].v_type == ParameterValueType::Handle {
                        if &self.raw_parameters[length_param_index].type_name.1 != "usize" {
                            writeln!(f, "{0}{0}{1}.truncate({2} as usize);", INDENT, self.raw_parameters[*i].name.1, self.raw_parameters[length_param_index].name.1)?;
                        } else {
                            writeln!(f, "{0}{0}{1}.truncate({2});", INDENT, self.raw_parameters[*i].name.1, self.raw_parameters[length_param_index].name.1)?;
                        }
                    } else {
                        if &self.raw_parameters[length_param_index].type_name.1 != "usize" {
                            writeln!(f, "{0}{0}{1}.truncate({2} as usize);", INDENT, self.raw_parameters[*i].name.1, self.raw_parameters[length_param_index].name.1)?;
                        } else {
                            writeln!(f, "{0}{0}{1}.truncate({2});", INDENT, self.raw_parameters[*i].name.1, self.raw_parameters[length_param_index].name.1)?;
                        }
                    }
                }
                writeln!(f, "")?;
                if self.raw_parameters[array_param_indices[0]].v_type == ParameterValueType::Other {
                    if array_param_indices.len() == 1 {

                        let collect = if return_needs_norm[0] {
                            ".normalise()).collect()"
                        } else {
                            ").collect()"
                        };

                        if let Some(n) = needs_next_chain {
                            writeln!(f, "{0}{0}{1}.into_iter().map(|mut v| {{", INDENT, self.raw_parameters[array_param_indices[0]].name.1)?;
                            writeln!(f, "{0}{0}{0}v.recursive_clear_next();", INDENT)?;
                            writeln!(f, "{0}{0}{0}unsafe {{ v.assume_init() }}{1}", INDENT, collect.replace(").", &format!("\n{0}{0}}}).", INDENT)))?;
                        } else {
                            writeln!(f, "{0}{0}{1}.into_iter().map(|v| unsafe {{ v.assume_init() }}{2}", INDENT, self.raw_parameters[array_param_indices[0]].name.1, collect)?;
                        }
                    } else {
                        writeln!(f, "{0}{0}({1})", INDENT, array_param_indices.iter().enumerate().map(|(ri, i)| {
                            if return_needs_norm[ri] {
                                format!("{}.into_iter().map(|v| unsafe {{ v.assume_init() }}.normalise()).collect()", self.raw_parameters[*i].name.1)
                            } else {
                                format!("{}.into_iter().map(|v| unsafe {{ v.assume_init() }}).collect()", self.raw_parameters[*i].name.1)
                            }
                        }).collect::<Vec<String>>().join(", "))?;
                    }
                } else {
                    if array_param_indices.len() == 1 {
                        if return_needs_norm[0] {
                            writeln!(f, "{0}{0}{1}.into_iter().map(|v| v.normalise()).collect()", INDENT, self.raw_parameters[array_param_indices[0]].name.1)?;
                        } else {
                            writeln!(f, "{0}{0}{1}", INDENT, self.raw_parameters[array_param_indices[0]].name.1)?;
                        }
                    } else {
                        writeln!(f, "{0}{0}({1})", INDENT, array_param_indices.iter().enumerate().map(|(ri, i)| {
                            if return_needs_norm[ri] {
                                format!("{}.into_iter().map(|v| v.normalise()).collect()", self.raw_parameters[*i].name.1)
                            } else {
                                format!("{}", self.raw_parameters[*i].name.1)
                            }
                        }).collect::<Vec<String>>().join(", "))?;
                    }
                }
            } else {
                //    pub fn enumerate_instance_extension_properties(&self, p_layer_name: UnsizedCStr) -> core::result::Result<(Vec<ExtensionProperties>, Result), Result> {
                //         let mut p_properties = Vec::new();
                //         loop {
                //             let mut p_properties_count = 0;
                //             let result = (self.data.enumerate_instance_extension_properties)(p_layer_name, &mut p_properties_count, None);
                //             if result != Result::eSuccess {
                //                 return Err(result)
                //             }
                //
                //             p_properties.resize(p_properties_count as usize, unsafe { core::mem::MaybeUninit::zeroed().assume_init() });
                //             let result = (self.data.enumerate_instance_extension_properties)(p_layer_name, &mut p_properties_count, p_properties.first_mut());
                //
                //             if result == Result::eSuccess {
                //                 p_properties.resize(p_properties_count as usize, unsafe { core::mem::MaybeUninit::zeroed().assume_init() });
                //
                //                 return Ok((p_properties, result))
                //             } else if result != Result::eSuccess && result != Result::eIncomplete {
                //                 return Err(result)
                //             }
                //         }
                //     }

                assert_eq!(1, self.raw_parameters.iter().filter(|p| !p.is_length_for.is_empty()).count(), "{self:#?}\n\n{f}");
                let length_param_index = self.raw_parameters.iter().enumerate().find_map(|(i, p)| (!p.is_length_for.is_empty()).then(|| i)).unwrap();

                let array_param_indices = self.raw_parameters[length_param_index].is_length_for.iter().map(|v| v.0).collect::<Vec<_>>();

                for (k, i) in array_param_indices.iter().enumerate() {
                    if return_needs_norm[k] || return_is_handle[k] {
                        writeln!(f, "{0}{0}let mut {1} = V{k}::new_with();", INDENT, self.raw_parameters[*i].name.1)?;
                    } else {
                        writeln!(f, "{0}{0}let mut {1} = V{k}::new();", INDENT, self.raw_parameters[*i].name.1)?;
                    }
                }

                writeln!(f, "{0}{0}loop {{", INDENT)?;

                writeln!(f, "{0}{0}{0}let mut {1} = 0;", INDENT, self.raw_parameters[length_param_index].name.1)?;
                {
                    write!(f, "{0}{0}{0}let result = (self.data.{1})(", INDENT, self.name)?;
                    for (i, rp) in self.reverse_parameters.iter().enumerate() {
                        if i != 0 {
                            write!(f, ", ")?;
                        }
                        if i == 0 && self.first_param_is_handle {
                            write!(f, "self")?;
                            if &self_handle.name.0 != &self.raw_parameters[0].type_name.0 {
                                write!(f, ".data.{}", self.raw_parameters[0].name.1)?;
                            } else {
                                if self.raw_parameters[0].extern_sync {
                                    write!(f, ".as_mut_handle().handle")?;
                                } else {
                                    write!(f, ".handle")?;
                                }
                            }
                        } else if i == length_param_index {
                            write!(f, "core::ptr::NonNull::from(&mut {})", self.raw_parameters[i].name.1)?;
                        } else if array_param_indices.contains(&i) {
                            write!(f, "None")?;
                        } else {
                            match rp {
                                OxidisedReversedParameterType::Value { original_index } => {
                                    if let Some(p) = self.raw_parameters[i].pointers.first() {
                                        if original_index.is_some() && !self.raw_parameters[i].generic_parameters.is_empty() {
                                            write!(f, "unsafe {{ core::mem::transmute(")?;
                                        }
                                        if PointerType::Mut == p.ty {
                                            if original_index.is_some() && self.parameters[i].not_return {
                                                write!(f, "{}", self.raw_parameters[i].name.1)?;
                                            } else {
                                                write!(f, "core::ptr::NonNull::from(&mut {})", self.raw_parameters[i].name.1)?;
                                            }
                                        } else {
                                            if &self.raw_parameters[i].name.0 == "pAllocator" {
                                                write!(f, "None")?;
                                            } else {
                                                if p.optional {
                                                    // write!(f, "Some(")?;
                                                }
                                                write!(f, "{}", self.raw_parameters[i].name.1)?;
                                                if p.optional {
                                                    // write!(f, ")")?;
                                                }
                                            }
                                        }
                                        if original_index.is_some() && !self.raw_parameters[i].generic_parameters.is_empty() {
                                            write!(f, ") }}", )?;
                                        }
                                    } else {
                                        let mut is_self = false;
                                        if i == 1 && &self_handle.name.0 == &self.raw_parameters[1].type_name.0 {
                                            write!(f, "self")?;
                                            is_self = true;
                                        } else {
                                            write!(f, "{}", self.raw_parameters[i].name.1)?;
                                        }
                                        if self.raw_parameters[i].full_type.1.starts_with("Raw") {
                                            write!(f, ".into_raw()")?;
                                        }
                                        match self.raw_parameters[i].v_type {
                                            ParameterValueType::Handle => {
                                                if self.raw_parameters[i].extern_sync {
                                                    if self.raw_parameters[i].value_optional && !is_self {
                                                        write!(f, ".map_or(Handle::null(), |handle| handle.as_mut_handle().handle)")?;
                                                    } else {
                                                        write!(f, ".as_mut_handle().handle")?
                                                    }
                                                } else {
                                                    if self.raw_parameters[i].value_optional && !is_self {
                                                        write!(f, ".map_or(Handle::null(), |handle| handle.handle)")?;
                                                    } else {
                                                        write!(f, ".handle")?
                                                    }
                                                }
                                            },
                                            _ => {}
                                        };
                                    }
                                }
                                OxidisedReversedParameterType::ArrayPtr { ptr_from_index } => {}
                                OxidisedReversedParameterType::ArrayLen { len_from_index } => {}
                            }
                        }
                    }
                    writeln!(f, ");")?;
                }
                writeln!(f, "{0}{0}{0}if result.0 < 0 {{", INDENT)?;
                writeln!(f, "{0}{0}{0}{0}return Err(result.into_error().normalise())", INDENT)?;
                writeln!(f, "{0}{0}{0}}}", INDENT)?;

                writeln!(f, "")?;

                for i in &array_param_indices {
                    if &self.raw_parameters[length_param_index].type_name.1 != "usize" {
                        writeln!(f, "{0}{0}{0}{1}.resize_with({2} as usize, || {3});", INDENT, self.raw_parameters[*i].name.1, self.raw_parameters[length_param_index].name.1, self.raw_parameters[*i].uninit(true))?;
                    } else {
                        writeln!(f, "{0}{0}{0}{1}.resize_with({2}, || {3});", INDENT, self.raw_parameters[*i].name.1, self.raw_parameters[length_param_index].name.1, self.raw_parameters[*i].uninit(true))?;
                    }
                }
                {
                    write!(f, "{0}{0}{0}let result = (self.data.{1})(", INDENT, self.name)?;
                    for (i, rp) in self.reverse_parameters.iter().enumerate() {
                        if i != 0 {
                            write!(f, ", ")?;
                        }
                        if i == 0 && self.first_param_is_handle {
                            write!(f, "self")?;
                            if &self_handle.name.0 != &self.raw_parameters[0].type_name.0 {
                                write!(f, ".data.{}", self.raw_parameters[0].name.1)?;
                            } else {
                                if self.raw_parameters[0].extern_sync {
                                    write!(f, ".as_mut_handle().handle")?;
                                } else {
                                    write!(f, ".handle")?;
                                }
                            }
                        } else if i == length_param_index {
                            write!(f, "core::ptr::NonNull::from(&mut {})", self.raw_parameters[i].name.1)?;
                        } else if array_param_indices.contains(&i) {
                            if self.raw_parameters[i].v_type != ParameterValueType::Other || input_param.contains(&i) {
                                write!(f, "{}.first_mut().map(|p| core::ptr::NonNull::from(p))", self.raw_parameters[i].name.1)?;
                            } else {
                                write!(f, "{}.first_mut().map(|p| nonnull_maybe_uninit(p))", self.raw_parameters[i].name.1)?;
                            }
                        } else {
                            match rp {
                                OxidisedReversedParameterType::Value { original_index } => {
                                    if let Some(p) = self.raw_parameters[i].pointers.first() {
                                        if original_index.is_some() && !self.raw_parameters[i].generic_parameters.is_empty() {
                                            write!(f, "unsafe {{ core::mem::transmute(")?;
                                        }
                                        if PointerType::Mut == p.ty {
                                            if original_index.is_some() && self.parameters[i].not_return {
                                                write!(f, "{}", self.raw_parameters[i].name.1)?;
                                            } else {
                                                write!(f, "core::ptr::NonNull::from(&mut {})", self.raw_parameters[i].name.1)?;
                                            }
                                        } else {
                                            if &self.raw_parameters[i].name.0 == "pAllocator" {
                                                write!(f, "None")?;
                                            } else {
                                                if p.optional {
                                                    // write!(f, "Some(")?;
                                                }
                                                write!(f, "{}", self.raw_parameters[i].name.1)?;
                                                if p.optional {
                                                    // write!(f, ")")?;
                                                }
                                            }
                                        }
                                        if original_index.is_some() && !self.raw_parameters[i].generic_parameters.is_empty() {
                                            write!(f, ") }}", )?;
                                        }
                                    } else {
                                        let mut is_self = false;
                                        if i == 1 && &self_handle.name.0 == &self.raw_parameters[1].type_name.0 {
                                            write!(f, "self")?;
                                            is_self = true;
                                        } else {
                                            write!(f, "{}", self.raw_parameters[i].name.1)?;
                                        }
                                        if self.raw_parameters[i].full_type.1.starts_with("Raw") {
                                            write!(f, ".into_raw()")?;
                                        }
                                        match self.raw_parameters[i].v_type {
                                            ParameterValueType::Handle => {
                                                if self.raw_parameters[i].extern_sync {
                                                    if self.raw_parameters[i].value_optional && !is_self {
                                                        write!(f, ".map_or(Handle::null(), |handle| handle.as_mut_handle().handle)")?;
                                                    } else {
                                                        write!(f, ".as_mut_handle().handle")?
                                                    }
                                                } else {
                                                    if self.raw_parameters[i].value_optional && !is_self {
                                                        write!(f, ".map_or(Handle::null(), |handle| handle.handle)")?;
                                                    } else {
                                                        write!(f, ".handle")?
                                                    }
                                                }
                                            }
                                            _ => {}
                                        };
                                    }
                                }
                                OxidisedReversedParameterType::ArrayPtr { ptr_from_index } => {}
                                OxidisedReversedParameterType::ArrayLen { len_from_index } => {}
                            }
                        }
                    }
                    writeln!(f, ");")?;
                }

                writeln!(f, "")?;

                writeln!(f, "{0}{0}{0}if result.0 >= 0 && result != RawResult::eIncomplete {{", INDENT)?;
                for i in &array_param_indices {
                    if self.raw_parameters[*i].v_type == ParameterValueType::Handle {
                        if &self.raw_parameters[length_param_index].type_name.1 != "usize" {
                            writeln!(f, "{0}{0}{0}{0}{1}.truncate({2} as usize);", INDENT, self.raw_parameters[*i].name.1, self.raw_parameters[length_param_index].name.1)?;
                        } else {
                            writeln!(f, "{0}{0}{0}{0}{1}.truncate({2});", INDENT, self.raw_parameters[*i].name.1, self.raw_parameters[length_param_index].name.1)?;
                        }
                    } else {
                        if &self.raw_parameters[length_param_index].type_name.1 != "usize" {
                            writeln!(f, "{0}{0}{0}{0}{1}.truncate({2} as usize);", INDENT, self.raw_parameters[*i].name.1, self.raw_parameters[length_param_index].name.1)?;
                        } else {
                            writeln!(f, "{0}{0}{0}{0}{1}.truncate({2});", INDENT, self.raw_parameters[*i].name.1, self.raw_parameters[length_param_index].name.1)?;
                        }
                    }
                }
                writeln!(f, "")?;

                let mut any_conversion = false;
                for (k, i) in array_param_indices.iter().enumerate() {
                    if self.raw_parameters[*i].type_name.1.ends_with("Raw") {
                        let handle = &self.raw_parameters[*i].type_name.0;
                        let handle = *handle_map.get(handle).unwrap();

                        let p_str = if &self_handle.name.0 == &self.raw_parameters[0].type_name.0 {
                            "&self.data"
                        } else {
                            // f = f.replacen("&self", &format!("&self, parent: &{}", self_handle.parent.as_ref().unwrap().1), 1);

                            "&parent.data"
                        };

                        if handle.destructor_uses_alloc {
                            writeln!(f, "{0}{0}{0}{0}let {1} = {1}.into_iter().map(|handle| {2}::new(handle, {3}, None, false)).collect::<V{k}>();", INDENT, self.raw_parameters[*i].name.1, handle.name.1, p_str)?;
                            // Because these images should only be destroyed via destroying swapchain
                        } else {
                            if handle.destructor.is_some() && !handle.destructor_uses_slice {
                                writeln!(f, "{0}{0}{0}{0}let {1} = {1}.into_iter().map(|handle| {2}::new(handle, {3}, true)).collect();", INDENT, self.raw_parameters[*i].name.1, handle.name.1, p_str)?;
                            } else {
                                writeln!(f, "{0}{0}{0}{0}let {1} = {1}.into_iter().map(|handle| {2}::new(handle, {3})).collect();", INDENT, self.raw_parameters[*i].name.1, handle.name.1, p_str)?;
                            }
                        }
                        any_conversion = true;
                    } else if self.raw_parameters[*i].type_name.1.starts_with("Raw") {
                        if self.raw_parameters[*i].v_type == ParameterValueType::Other {
                            writeln!(f, "{0}{0}{0}{0}let {1} = {1}.into_iter().map(|v| unsafe {{ v.assume_init() }}.normalise()).collect();", INDENT, self.raw_parameters[*i].name.1)?;
                        } else {
                            writeln!(f, "{0}{0}{0}{0}let {1} = {1}.into_iter().map(|v| v.normalise()).collect();", INDENT, self.raw_parameters[*i].name.1)?;
                        }
                    } else {
                        if self.raw_parameters[*i].v_type == ParameterValueType::Other {
                            writeln!(f, "{0}{0}{0}{0}let {1} = {1}.into_iter().map(|v| unsafe {{ v.assume_init() }}).collect();", INDENT, self.raw_parameters[*i].name.1)?;
                        }
                    }
                }
                if any_conversion {
                    writeln!(f, "")?;
                }

                writeln!(f, "{0}{0}{0}{0}return Ok(({1}, result.into_success().normalise()))", INDENT, array_param_indices.iter().map(|i| self.raw_parameters[*i].name.1.as_str()).collect::<Vec<&str>>().join(", "))?;
                // writeln!(f, "{0}{0}{0}}} else {{", INDENT)?;
                writeln!(f, "{0}{0}{0}}} else if result.0 < 0 {{", INDENT)?;
                writeln!(f, "{0}{0}{0}{0}return Err(result.into_error().normalise())", INDENT)?;
                writeln!(f, "{0}{0}{0}}}", INDENT)?;
                writeln!(f, "{0}{0}}}", INDENT)?;
            }
        } else {
            match self.name.as_str() {
                "allocate_command_buffers" => {
                    writeln!(f, "{0}{0}let mut allocate_info = core::ptr::NonNull::from(allocate_info);", INDENT)?;
                    writeln!(f, "{0}{0}unsafe {{ allocate_info.as_mut() }}.command_pool = CommandPoolMutHandle {{ handle: self.handle.to_mut(), _phantom: Default::default() }};", INDENT)?;
                }
                "allocate_descriptor_sets" => {
                    writeln!(f, "{0}{0}let mut allocate_info = core::ptr::NonNull::from(allocate_info);", INDENT)?;
                    writeln!(f, "{0}{0}unsafe {{ allocate_info.as_mut() }}.descriptor_pool = DescriptorPoolMutHandle {{ handle: self.handle.to_mut(), _phantom: Default::default() }};", INDENT)?;
                }
                "create_video_session_parameters_khr" => {
                    // writeln!(f, "{0}{0}create_info.video_session = self.as_handle();", INDENT)?;
                    writeln!(f, "{0}{0}let mut create_info = core::ptr::NonNull::from(create_info);", INDENT)?;
                    writeln!(f, "{0}{0}unsafe {{ create_info.as_mut() }}.video_session = VideoSessionKHRHandle {{ handle: self.handle, _phantom: Default::default() }};", INDENT)?;
                }
                "create_swapchain_khr" => {
                    writeln!(f, "{0}{0}let mut create_info = core::ptr::NonNull::from(create_info);", INDENT)?;
                    writeln!(f, "{0}{0}unsafe {{ create_info.as_mut() }}.surface = surface_khr.as_mut_handle();", INDENT)?;
                }
                "create_shared_swapchains_khr" => {
                    writeln!(f, "{0}{0}let create_infos: &mut [RawSwapchainCreateInfoKHR] = unsafe {{ std::mem::transmute(create_infos) }}; // Hack the lifetimes", INDENT)?;
                    writeln!(f, "{0}{0}let iter = surfaces.into_iter();", INDENT)?;
                    writeln!(f, "{0}{0}for (i, s) in iter.clone().enumerate() {{", INDENT)?;
                    writeln!(f, "{0}{0}{0}create_infos[i].surface = s.as_mut_handle();", INDENT)?;
                    writeln!(f, "{0}{0}}}", INDENT)?;
                }
                "get_device_queue" => {
                    writeln!(f, "{0}{0}assert!(self.data.acquired_queues.lock().unwrap().insert((queue_family_index, queue_index, DeviceQueueCreateFlags::eNone)), \"Must only get a queue once, if you want multiple handles then use something like Arc<Queue>\");", INDENT)?;
                }
                "get_device_queue2" => {
                    writeln!(f, "{0}{0}assert!(self.data.acquired_queues.lock().unwrap().insert((queue_info.queue_family_index, queue_info.queue_index, queue_info.flags)), \"Must only get a queue once, if you want multiple handles then use something like Arc<Queue>\");", INDENT)?;
                }
                "cmd_set_sample_mask_ext" => {
                    writeln!(f, "{0}{0}assert_eq!(sample_mask.len(), ((u32::from(samples) + 31) / 32) as usize, \"The length of sample_mask must be equal to ceil(rasterization_samples/32), however {{}} != {{}}\", sample_mask.len(), (u32::from(samples) + 31) / 32);", INDENT)?;
                    writeln!(f, "{0}{0}let samples = samples.into_raw();", INDENT)?;
                }
                _ => {}
            }

            let mut optional = vec![false; self.parameters.len()];

            for i in 0..self.raw_parameters.len() {
                if let OxidisedReversedParameterType::ArrayPtr { ptr_from_index } = self.reverse_parameters[i] {
                    if let Some(j) = ptr_from_index {
                        if self.raw_parameters[i].pointers[0].optional {
                            optional[j] = true;
                        }
                    }
                }
            }

            for (ri, r) in self.reverse_parameters.iter().enumerate() {
                if let OxidisedReversedParameterType::ArrayLen { len_from_index } = r {
                    if let Some(len_from_index) = len_from_index {
                        let in_len: Vec<_> = len_from_index.iter().filter(|l| !l.1).collect();

                        if in_len.len() > 1 {
                            let p0_optional = optional[in_len[0].0];
                            let i_range = if p0_optional { in_len.len() } else { 1 };

                            for i in 0..i_range {
                                let pi_optional = optional[in_len[i].0];

                                for j in i + 1..in_len.len() {
                                    let pj_optional = optional[in_len[j].0];

                                    match (pi_optional, pj_optional) {
                                        (true, true) => {
                                            writeln!(f, "{0}{0}assert!({1}.len() == 0 || {2}.len() == 0 || ({1}.len() == {2}.len()), \"{1} and {2} must have the same length, or either be empty.\");", INDENT, self.parameters[in_len[i].0].name, self.parameters[in_len[j].0].name)?;
                                        }
                                        (true, false) => {
                                            writeln!(f, "{0}{0}assert!({1}.len() == 0 || ({1}.len() == {2}.len()), \"{1} and {2} must have the same length, or {1} be empty.\");", INDENT, self.parameters[in_len[i].0].name, self.parameters[in_len[j].0].name)?;
                                        }
                                        (false, true) => {
                                            writeln!(f, "{0}{0}assert!({2}.len() == 0 || ({1}.len() == {2}.len()), \"{1} and {2} must have the same length, or {2} be empty.\");", INDENT, self.parameters[in_len[i].0].name, self.parameters[in_len[j].0].name)?;
                                        }
                                        (false, false) => {
                                            writeln!(f, "{0}{0}assert_eq!({1}.len(), {2}.len(), \"{1} and {2} must have the same length.\");", INDENT, self.parameters[in_len[i].0].name, self.parameters[in_len[j].0].name)?;
                                        }
                                    }
                                }
                            }
                        }

                        if !self.raw_parameters[ri].value_optional {
                            if let Some(i) = in_len.iter().find(|l| !optional[l.0]) {
                                writeln!(f, "{0}{0}assert!({1}.len() > 0, \"{2} length must be > 0.\");", INDENT, self.parameters[i.0].name, self.raw_parameters[ri].name.1)?;
                            }
                        }
                    } else {
                        eprintln!("Warn: {:?}", self);
                    }
                }
            }

            for p in &self.raw_parameters {
                for l in &p.is_length_for {
                    if let Some(v) = &l.2 {
                        if self.raw_parameters[l.0].pointers[0].ty == PointerType::Const {
                            writeln!(f, "{0}{0}assert_eq!({1}.{2} as usize, {3}.len(), \"{1}.{2} and {3} length must match.\");", INDENT, p.name.1, v, self.raw_parameters[l.0].name.1)?;
                        }
                    }
                }
            }

            if &self.name == "map_memory" {
                writeln!(f, "{0}{0}assert_ne!(size, WHOLE_SIZE, \"For memory safety purposes the use of vk::WHOLE_SIZE is not supported here.\");", INDENT)?;
            }

            for p in &self.parameters {
                match (self.name.as_str(), p.name.as_str()) {
                    ("build_acceleration_structures_khr", "build_range_infos") | ("cmd_build_acceleration_structures_khr", "build_range_infos") | ("cmd_build_acceleration_structures_indirect_khr", "max_primitive_counts") => {
                        writeln!(f, "{0}{0}assert!(infos.iter().zip({1}.iter()).all(|(a, b)| a.geometry_count as usize == b.len()), \"The length of each sub slice must match that provide in the corresponding infos[i] struct\");", INDENT, p.name)?;
                        writeln!(f, "{0}{0}let {1} = {1}.iter().map(|infos| infos.first().expect(\"Each sub slice must have a length > 0\")).collect::<V::V<_>>();\n", INDENT, p.name)?;
                    }
                    _ => {}
                }
            }

            if &self_handle.name.1 == "CommandBuffer" && !unchecked_external_sync {
                writeln!(f, "{0}{0}assert_eq!(self.data.command_pool, command_pool.handle, \"Must provide a ref to the command pool this command buffer was allocated from, this is to enforce thread safety requirements (external sync)\");", INDENT)?;
            }

            if with_alloc {
                let v = if destructor_uses_alloc {
                    "self.data.allocator"
                } else {
                    if &self.name == "create_instance" {
                        "None"
                    } else if &self_handle.name.0 == &self.raw_parameters[0].type_name.0 {
                        "self.data.allocator"
                    } else {
                        "self.data.allocator"
                    }
                };

                writeln!(f, "{0}{0}let allocator = alloc_mode.apply({1});", INDENT, v)?;
            }

            let mut k = 0;

            for (i, rp) in self.reverse_parameters.iter().enumerate() {
                match rp {
                    OxidisedReversedParameterType::Value { original_index } => {
                        if let Some(p) = self.raw_parameters[i].pointers.first() {
                            if PointerType::Mut == p.ty {
                                if original_index.is_none() || !self.parameters[original_index.unwrap()].not_return {
                                    writeln!(f, "{0}{0}let mut {1} = {2};", INDENT, self.raw_parameters[i].name.1, self.raw_parameters[i].uninit(false))?;

                                    if let Some(n) = &needs_next_chain {
                                        if i == n.1 {
                                            writeln!(f, "{INDENT}{INDENT}let mut last_uninit = None;")?;
                                            writeln!(f, "{INDENT}{INDENT}let mut uninit_count = 0;")?;
                                            writeln!(f, "{INDENT}{INDENT}for next in next_chain.into_iter().rev() {{")?;
                                            writeln!(f, "{INDENT}{INDENT}{INDENT}if next.is_tracked_uninitialized() {{")?;
                                            writeln!(f, "{INDENT}{INDENT}{INDENT}{INDENT}uninit_count += 1;")?;
                                            writeln!(f, "{INDENT}{INDENT}{INDENT}{INDENT}{}.push_next(next.as_base_in());", self.raw_parameters[i].name.1)?;
                                            writeln!(f, "{INDENT}{INDENT}{INDENT}{INDENT}if last_uninit.is_none() {{")?;
                                            writeln!(f, "{INDENT}{INDENT}{INDENT}{INDENT}{INDENT}last_uninit = Some(next);")?;
                                            writeln!(f, "{INDENT}{INDENT}{INDENT}{INDENT}}}")?;
                                            writeln!(f, "{INDENT}{INDENT}{INDENT}}} else {{")?;
                                            writeln!(f, "{INDENT}{INDENT}{INDENT}{INDENT}if let Some(last_uninit) = &mut last_uninit {{")?;
                                            writeln!(f, "{INDENT}{INDENT}{INDENT}{INDENT}{INDENT}last_uninit.push_next(next.as_base_in());")?;
                                            writeln!(f, "{INDENT}{INDENT}{INDENT}{INDENT}}} else {{")?;
                                            writeln!(f, "{INDENT}{INDENT}{INDENT}{INDENT}{INDENT}{}.push_next(next.as_base_in());", self.raw_parameters[i].name.1)?;
                                            writeln!(f, "{INDENT}{INDENT}{INDENT}{INDENT}}}")?;
                                            writeln!(f, "{INDENT}{INDENT}{INDENT}}}")?;
                                            writeln!(f, "{INDENT}{INDENT}}}")?;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    OxidisedReversedParameterType::ArrayPtr { ptr_from_index } => {
                        match self.raw_parameters[i].pointers[0].ty {
                            PointerType::Const => {}
                            PointerType::Mut => {
                                if ptr_from_index.is_none() || !self.parameters[ptr_from_index.unwrap()].not_return {
                                    let in_slice_same_len = self.raw_parameters.iter().find_map(|rp| if rp.is_length_for.iter().any(|(i1, i2, s)| *i1 == i) {
                                        rp.is_length_for.iter().enumerate().filter_map(|(j, l)| (j != i).then(|| l)).find_map(|(i1, i2, s)| {
                                            match self.reverse_parameters[*i1] {
                                                OxidisedReversedParameterType::Value { .. } => None,
                                                OxidisedReversedParameterType::ArrayPtr { ptr_from_index } => ptr_from_index.map(|j| self.parameters.get(j)).flatten(),
                                                OxidisedReversedParameterType::ArrayLen { .. } => None,
                                            }
                                        })
                                    } else { None });

                                    let init = if return_needs_norm[k] || return_is_handle[k] {
                                        "init_with"
                                    } else {
                                        "init"
                                    };

                                    let result = writeln!(f, "{0}{0}let mut {1} = V{k}::{init}({2}, {3});", INDENT, self.raw_parameters[i].name.1, self.raw_parameters[i].uninit(true), {
                                                 if let Some(o) = in_slice_same_len {
                                                     format!("{}.len()", o.name)
                                                 } else if let Some(l) = self.raw_parameters.iter().find(|p| (p.pointers.is_empty() || p.pointers[0].ty == PointerType::Const) && p.is_length_for.iter().any(|l| !self.raw_parameters[l.0].pointers.is_empty() && self.raw_parameters[l.0].pointers[0].ty == PointerType::Mut)) {
                                                     if let Some((_, _, Some(p))) = l.is_length_for.iter().find(|p| p.0 == i) {
                                                         match self.name.as_str() {
                                                             "allocate_command_buffers" => format!("unsafe {{ {}.as_mut() }}.{} as usize", l.name.1, p),
                                                             "allocate_descriptor_sets" => format!("unsafe {{ {}.as_mut() }}.{} as usize", l.name.1, p),
                                                             _ => format!("{}.{} as usize", l.name.1, p),
                                                         }
                                                     } else {
                                                         if &l.full_type.1 == "usize" {
                                                             format!("{}", l.name.1)
                                                         } else {
                                                             format!("{} as usize", l.name.1)
                                                         }
                                                     }
                                                 } else {
                                                     format!("{}.len()", &self.parameters[ptr_from_index.unwrap()].name)
                                                 }
                                             })?;

                                    k += 1;

                                    result
                                }
                            }
                        }
                    }
                    OxidisedReversedParameterType::ArrayLen { len_from_index } => {}
                }
            }

            write!(f, "{0}{0}", INDENT)?;
            if let Some(rt) = &self.return_type {
                if rt.has_result || rt.other_return.is_some() {
                    write!(f, "let result = ")?;
                }
            }
            write!(f, "(self.data.{})(", self.name)?;

            for (i, rp) in self.reverse_parameters.iter().enumerate() {
                if i != 0 {
                    write!(f, ", ")?;
                }
                if i == 0 && self.first_param_is_handle {
                    write!(f, "self")?;
                    if &self_handle.name.0 != &self.raw_parameters[0].type_name.0 {
                        write!(f, ".data.{}", self.raw_parameters[0].name.1)?;
                    } else {
                        if self.raw_parameters[0].extern_sync {
                            write!(f, ".as_mut_handle().handle")?;
                        } else {
                            write!(f, ".handle")?;
                        }
                    }
                } else {
                    match rp {
                        OxidisedReversedParameterType::Value { original_index } => {
                            if let Some(p) = self.raw_parameters[i].pointers.first() {
                                if original_index.is_some() && !self.raw_parameters[i].generic_parameters.is_empty() {
                                    write!(f, "unsafe {{ core::mem::transmute(")?;
                                }
                                if PointerType::Mut == p.ty {
                                    if original_index.is_some() && self.parameters[i].not_return {
                                        // write!(f, "core::ptr::NonNull::from({})", self.raw_parameters[i].name.1)?;
                                        match self.name.as_str() {
                                            "allocate_command_buffers" => write!(f, "{}", self.raw_parameters[i].name.1)?,
                                            "allocate_descriptor_sets" => write!(f, "{}", self.raw_parameters[i].name.1)?,
                                            "create_video_session_parameters_khr" => write!(f, "{}", self.raw_parameters[i].name.1)?,
                                            _ => write!(f, "core::ptr::NonNull::from({})", self.raw_parameters[i].name.1)?,
                                        };
                                    } else {
                                        if self.raw_parameters[i].v_type == ParameterValueType::Other {
                                            write!(f, "nonnull_maybe_uninit(&mut {})", self.raw_parameters[i].name.1)?;
                                        } else {
                                            write!(f, "core::ptr::NonNull::from(&mut {})", self.raw_parameters[i].name.1)?;
                                        }
                                    }
                                } else {
                                    if &self.raw_parameters[i].name.0 == "pAllocator" {
                                        if is_destructor {
                                            write!(f, "self.data.allocator.as_ref().map(|a| a.as_general())")?;
                                        } else if with_alloc {
                                            write!(f, "allocator.as_ref()")?;
                                        } else {
                                            write!(f, "None")?;
                                        }
                                    } else {
                                        if p.optional {
                                            // write!(f, "Some(")?;
                                        }
                                        if self.raw_parameters[i].full_type.1.starts_with("&mut") {
                                            match self.name.as_str() {
                                                "allocate_command_buffers" => write!(f, "{}", self.raw_parameters[i].name.1)?,
                                                "allocate_descriptor_sets" => write!(f, "{}", self.raw_parameters[i].name.1)?,
                                                "create_video_session_parameters_khr" => write!(f, "{}", self.raw_parameters[i].name.1)?,
                                                _ => write!(f, "core::ptr::NonNull::from({})", self.raw_parameters[i].name.1)?,
                                            };
                                            // write!(f, "core::ptr::NonNull::from({})", self.raw_parameters[i].name.1)?;
                                        } else {
                                            match self.name.as_str() {
                                                "create_video_session_parameters_khr" => write!(f, "unsafe {{ {}.as_mut() }}", self.raw_parameters[i].name.1)?,
                                                _ => write!(f, "{}", self.raw_parameters[i].name.1)?
                                            };
                                        }
                                        if p.optional {
                                            // write!(f, ")")?;
                                        }
                                    }
                                }
                                if original_index.is_some() && !self.raw_parameters[i].generic_parameters.is_empty() {
                                    write!(f, ") }}", )?;
                                }
                            } else {
                                let mut is_self = false;
                                if i == 1 && &self_handle.name.0 == &self.raw_parameters[1].type_name.0 {
                                    write!(f, "self")?;
                                    is_self = true;
                                } else {
                                    write!(f, "{}", self.raw_parameters[i].name.1)?;
                                }
                                if self.raw_parameters[i].full_type.1.starts_with("Raw") {
                                    write!(f, ".into_raw()")?;
                                }
                                match self.raw_parameters[i].v_type {
                                    ParameterValueType::Handle => {
                                        if self.raw_parameters[i].extern_sync {
                                            if self.raw_parameters[i].value_optional && !is_self {
                                                write!(f, ".map_or(Handle::null(), |handle| handle.as_mut_handle().handle)")?;
                                            } else {
                                                write!(f, ".as_mut_handle().handle")?
                                            }
                                        } else {
                                            if self.raw_parameters[i].value_optional && !is_self {
                                                write!(f, ".map_or(Handle::null(), |handle| handle.handle)")?;
                                            } else {
                                                write!(f, ".handle")?
                                            }
                                        }
                                    },
                                    _ => {}
                                };
                            }
                        }
                        OxidisedReversedParameterType::ArrayPtr { ptr_from_index } => {
                            // if self.raw_parameters[i].pointers[0].optional {
                            //     write!(f, "Some(")?;
                            // }

                            let needs_handle_to_raw = self.raw_parameters[i].v_type == ParameterValueType::Handle && !(self.raw_parameters[i].pointers[0].ty == PointerType::Mut && (ptr_from_index.is_none() || !self.parameters[ptr_from_index.unwrap()].not_return));

                            if !self.raw_parameters[i].pointers[0].optional {
                                if needs_handle_to_raw {
                                    if self.raw_parameters[i].full_type.1.contains("&mut") {
                                        // write!(f, "&mut ")?;
                                    } else {
                                        write!(f, "&")?;
                                    }
                                }
                            }
                            let mut handled_handle = false;
                            if self.raw_parameters[i].full_type.1.contains("&mut") {
                                if self.raw_parameters[i].v_type != ParameterValueType::Other || input_param.contains(&i) {
                                    if needs_handle_to_raw {
                                        write!(f, "{}.first_mut().map(|p| core::ptr::NonNull::from(&mut p.handle))", self.raw_parameters[i].name.1)?;
                                        handled_handle = true;
                                    } else {
                                        write!(f, "{}.first_mut().map(|p| core::ptr::NonNull::from(p))", self.raw_parameters[i].name.1)?;
                                    }
                                } else {
                                    write!(f, "{}.first_mut().map(|p| nonnull_maybe_uninit(p))", self.raw_parameters[i].name.1)?;
                                }
                            } else {
                                match self.raw_parameters[i].pointers[0].ty {
                                    PointerType::Const => write!(f, "{}.first()", self.raw_parameters[i].name.1)?,
                                    PointerType::Mut => write!(f, "{}.first_mut()", self.raw_parameters[i].name.1)?
                                }
                            }
                            if !self.raw_parameters[i].pointers[0].optional {
                                write!(f, ".unwrap()")?;
                                if needs_handle_to_raw && !handled_handle {
                                    write!(f, ".handle")?;
                                }
                            } else if needs_handle_to_raw && !handled_handle {
                                write!(f, ".map(|v| &v.handle)")?;
                            }
                            // if self.raw_parameters[i].pointers[0].optional {
                            //     write!(f, ")")?;
                            // }
                        }
                        OxidisedReversedParameterType::ArrayLen { len_from_index } => {
                            if self.raw_parameters[i].v_type == ParameterValueType::Integer {
                                if let Some(pick) = len_from_index.as_ref().map(|l| l.iter().find(|l| !l.1)).flatten() {
                                    if &self.raw_parameters[i].type_name.1 != "usize" {
                                        write!(f, "{}.len() as {}", self.parameters[pick.0].name, self.raw_parameters[i].type_name.1)?;
                                    } else {
                                        write!(f, "{}.len()", self.parameters[pick.0].name)?;
                                    }
                                } else {
                                    // write!(f, "oops")?;
                                    write!(f, "{}", self.raw_parameters[i].name.1)?;
                                }
                            } else {
                                if self.raw_parameters[i].full_type.1.starts_with("&mut") {
                                    // write!(f, "core::ptr::NonNull::from({})", self.raw_parameters[i].name.1)?;
                                    match self.name.as_str() {
                                        "allocate_command_buffers" => write!(f, "{}", self.raw_parameters[i].name.1)?,
                                        "allocate_descriptor_sets" => write!(f, "{}", self.raw_parameters[i].name.1)?,
                                        "create_video_session_parameters_khr" => write!(f, "{}", self.raw_parameters[i].name.1)?,
                                        _ => write!(f, "core::ptr::NonNull::from({})", self.raw_parameters[i].name.1)?,
                                    };
                                } else {
                                    write!(f, "{}", self.raw_parameters[i].name.1)?;
                                }
                            }
                        }
                    }
                }
            }
            writeln!(f, ");")?;

            match self.name.as_str() {
                "allocate_command_buffers" => {
                    writeln!(f, "{0}{0}unsafe {{ allocate_info.as_mut() }}.command_pool = Handle::null();", INDENT)?;
                }
                "allocate_descriptor_sets" => {
                    writeln!(f, "{0}{0}unsafe {{ allocate_info.as_mut() }}.descriptor_pool = Handle::null();", INDENT)?;
                }
                "create_video_session_parameters_khr" => {
                    // writeln!(f, "{0}{0}create_info.video_session = Handle::null();", INDENT)?;
                    writeln!(f, "{0}{0}unsafe {{ create_info.as_mut() }}.video_session = Handle::null();", INDENT)?;
                }
                "create_swapchain_khr" => {
                    writeln!(f, "{0}{0}unsafe {{ create_info.as_mut() }}.surface = Handle::null();", INDENT)?;
                }
                _ => {}
            }


            let mut extra_indent = "".to_string();

            if let Some(rt) = &self.return_type {
                if rt.has_result {
                    writeln!(f, "{1}{0}{0}if result.0 >= 0 {{", INDENT, extra_indent)?;
                    extra_indent += INDENT;
                }
            }

            let mut uses_parent = false;
            let p_str = if &self_handle.name.0 == &self.raw_parameters[0].type_name.0
                || &self.name == "allocate_command_buffers" || &self.name == "allocate_descriptor_sets" {
                "&self.data"
            } else {
                uses_parent = true;
                "&parent.data"
            };

            for (i, rp) in self.reverse_parameters.iter().enumerate() {
                match rp {
                    OxidisedReversedParameterType::Value { original_index } => {
                        if let Some(p) = self.raw_parameters[i].pointers.first() {
                            if PointerType::Mut == p.ty {
                                match &self.raw_parameters[i].v_type {
                                    ParameterValueType::Handle => {
                                        let handle = handle_map.get(&self.raw_parameters[i].type_name.0).unwrap();

                                        write!(f, "{0}{0}{3}let {1} = {2}::new({1}, ", INDENT, self.raw_parameters[i].name.1, self.raw_parameters[i].type_name.1.replace("Raw", ""), extra_indent)?;
                                        if let Some(loads_fn_ptrs) = &handle.loads_fn_ptrs {
                                            if let Some(parent) = &handle.parent {
                                                write!(f, "&self.data, ")?;
                                            }
                                            for add_par in &handle.additional_parents {
                                                write!(f, "&{}.data, ", add_par.1.to_snake_case())?;
                                            }
                                            if handle.destructor_uses_alloc {
                                                if with_alloc {
                                                    write!(f, "allocator, ")?;
                                                } else {
                                                    write!(f, "None, ")?;
                                                }
                                            }
                                            if handle.destructor.is_some() && !handle.destructor_uses_slice {
                                                writeln!(f, "self.data.{}, true);", loads_fn_ptrs.to_snake_case())?;
                                            } else {
                                                writeln!(f, "self.data.{});", loads_fn_ptrs.to_snake_case())?;
                                            }
                                        } else {
                                            if let Some(parent) = &handle.parent {
                                                write!(f, "&self.data, ")?;
                                            }
                                            for add_par in &handle.additional_parents {
                                                write!(f, "&{}.data, ", add_par.1.to_snake_case())?;
                                            }
                                            if handle.destructor_uses_alloc {
                                                if with_alloc {
                                                    write!(f, "allocator")?;
                                                } else {
                                                    write!(f, "None")?;
                                                }
                                            }
                                            f = f.trim_end_matches(", ").to_string();
                                            if handle.destructor.is_some() && !handle.destructor_uses_slice {
                                                writeln!(f, ", true);")?;
                                            } else {
                                                writeln!(f, ");")?;
                                            }
                                        }
                                    }
                                    _ => {}
                                }
                            }
                        }
                        if let Some(n) = &needs_next_chain {
                            if i == n.1 {
                                writeln!(f, "{INDENT}{INDENT}{extra_indent}{}.recursive_clear_next_mark_init(uninit_count);", self.raw_parameters[i].name.1)?;
                            }
                        }
                    }
                    OxidisedReversedParameterType::ArrayPtr { ptr_from_index } => {
                        let p = &self.raw_parameters[i].pointers[0];
                        if PointerType::Mut == p.ty {
                            if ParameterValueType::Handle == self.raw_parameters[i].v_type {
                                let handle = handle_map.get(&self.raw_parameters[i].type_name.0).unwrap();

                                if &self.name == "create_shared_swapchains_khr" {
                                    writeln!(f,
                                             "\
                                     {0}{0}{1}let mut surfaces = iter;\n\
                                     {0}{0}{1}let swapchains = swapchains.into_iter().enumerate().map(|(i, handle)| {{\n\
                                     {0}{0}{0}{1}let surface = surfaces.next().expect(\"There must be a surface corresponding to each create info\");\n\
                                     {0}{0}{0}{1}create_infos[i].surface = Handle::null();\n\
                                     {0}{0}{0}{1}SwapchainKHR::new(handle, &self.data, &surface.data, None, true)\n\
                                     {0}{0}{1}}}).collect();", INDENT, extra_indent
                                    )?;
                                } else
                                {
                                    write!(f, "{0}{0}{3}let {1} = {1}.into_iter().map(|handle| {2}::new(handle, ", INDENT, self.raw_parameters[i].name.1, self.raw_parameters[i].type_name.1.replace("Raw", ""), extra_indent)?;
                                    if let Some(loads_fn_ptrs) = &handle.loads_fn_ptrs {
                                        if let Some(parent) = &handle.parent {
                                            if uses_parent {
                                                // f = f.replacen("&self", &format!("&self, parent: &{}", self_handle.parent.as_ref().unwrap().1), 1);
                                            }
                                            write!(f, "{}, ", p_str)?;
                                        }
                                        for add_par in &handle.additional_parents {
                                            write!(f, "&{}.data, ", add_par.1.to_snake_case())?;
                                        }
                                        if handle.destructor_uses_alloc {
                                            write!(f, "None, ")?;
                                        }
                                        write!(f, "self.data.{}", loads_fn_ptrs.to_snake_case())?;
                                    } else {
                                        if let Some(parent) = &handle.parent {
                                            if uses_parent {
                                                // f = f.replacen("&self", &format!("&self, parent: &{}", self_handle.parent.as_ref().unwrap().1), 1);
                                            }
                                            write!(f, "{}, ", p_str)?;
                                        }
                                        for add_par in &handle.additional_parents {
                                            write!(f, "&{}.data, ", add_par.1.to_snake_case())?;
                                        }
                                        if handle.destructor_uses_alloc {
                                            write!(f, "None")?;
                                        }
                                        f = f.trim_end_matches(", ").to_string();
                                        // writeln!(f, ");")?;
                                    }
                                    if handle.destructor.is_some() && !handle.destructor_uses_slice {
                                        writeln!(f, ", true)).collect();")?;
                                    } else {
                                        writeln!(f, ")).collect();")?;
                                    }
                                }
                            }
                        }
                    }
                    OxidisedReversedParameterType::ArrayLen { len_from_index } => {}
                }
            }

            if &self.name == "map_memory" {
                //        let pp_data = if let Some(pp_data) = pp_data {
                //             unsafe { core::slice::from_raw_parts_mut(pp_data as *mut u8, size as usize) }
                //         } else {
                //             &mut [][..]
                //         };
                writeln!(f, "{0}{0}{1}let data = if let Some(data) = data {{", INDENT, extra_indent)?;
                writeln!(f, "{0}{0}{0}{1}unsafe {{ core::slice::from_raw_parts_mut(data.as_ptr(), size as usize) }}", INDENT, extra_indent)?;
                writeln!(f, "{0}{0}{1}}} else {{", INDENT, extra_indent)?;
                writeln!(f, "{0}{0}{0}{1}&mut [][..]", INDENT, extra_indent)?;
                writeln!(f, "{0}{0}{1}}};", INDENT, extra_indent)?;
                writeln!(f, "{0}{0}{1}let data = MappedMemory::new(data, self);", INDENT, extra_indent)?;
            }

            if let Some(rt) = &self.return_type {
                let mut t = rt.values.iter().enumerate().map(|(i, p)| {
                    match p {
                        OxidisedParameterType::Value { c_param_index } => {
                            if return_needs_norm[i] {
                                if self.raw_parameters[*c_param_index].v_type == ParameterValueType::Other {
                                    format!("unsafe {{ {}.into_value_unchecked() }}.normalise()", self.raw_parameters[*c_param_index].name.1)
                                } else {
                                    format!("{}.normalise()", self.raw_parameters[*c_param_index].name.1)
                                }
                            } else {
                                if self.raw_parameters[*c_param_index].v_type == ParameterValueType::Other {
                                    format!("unsafe {{ {}.into_value_unchecked() }}", self.raw_parameters[*c_param_index].name.1)
                                } else {
                                    self.raw_parameters[*c_param_index].name.1.clone()
                                }
                            }
                        }
                        OxidisedParameterType::Array { c_ptr_index, c_len_index } => {
                            if return_needs_norm[i] {
                                format!("{}.into_iter().map(|v| v.normalise()).collect()", self.raw_parameters[*c_ptr_index].name.1)
                            } else {
                                self.raw_parameters[*c_ptr_index].name.1.clone()
                            }
                        }
                    }
                }).collect::<Vec<String>>();
                let t_l = t.len();
                let t: String = t.join(", ");

                if rt.has_result {
                    if !t.is_empty() {
                        writeln!(f, "{2}{0}{0}Ok(({1}, result.into_success().normalise()))", INDENT, t, extra_indent)?;
                    } else {
                        writeln!(f, "{1}{0}{0}Ok(result.into_success().normalise())", INDENT, extra_indent)?;
                    }
                    writeln!(f, "{0}{1}}} else {{", INDENT, extra_indent)?;

                    for (i, rp) in self.reverse_parameters.iter().enumerate() {
                        match rp {
                            OxidisedReversedParameterType::Value { original_index } => {
                                if let Some(n) = &needs_next_chain {
                                    if i == n.1 {
                                        writeln!(f, "{INDENT}{INDENT}{extra_indent}{}.recursive_clear_next();", self.raw_parameters[i].name.1)?;
                                    }
                                }
                            }
                            _ => {}
                        }
                    }

                    writeln!(f, "{0}{0}{1}Err(result.into_error().normalise())", INDENT, extra_indent)?;
                    writeln!(f, "{0}{1}}}", INDENT, extra_indent)?;
                } else if let Some(o) = &rt.other_return {
                    if t.is_empty() {
                        writeln!(f, "{0}{0}result", INDENT)?;
                    } else {
                        writeln!(f, "{0}{0}({1}, result)", INDENT, t)?;
                    }
                } else if !t.is_empty() {
                    if t_l == 1 {
                        writeln!(f, "{0}{0}{1}", INDENT, t)?;
                    } else {
                        writeln!(f, "{0}{0}({1})", INDENT, t)?;
                    }
                }
            }
        }

        write!(f, "{}}}", INDENT)?;

        Ok(f)
    }
}