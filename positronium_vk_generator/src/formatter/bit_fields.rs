use std::collections::{BTreeMap, HashMap};
use std::fmt::{Display, Formatter, Write};

use heck::CamelCase;

use crate::formatter::{CName, INDENT, Named, RSName, trim_tag_suffix, trim_vk_prefix};
use crate::formatter::common::global_conversion;
use crate::formatter::disabled_manager::DisabledManager;
use crate::parser::enums::{EnumTypes, EnumVariantType};
use crate::parser::features_extensions::{EnumExtensionType, EnumType, RequireRemoveType};
use crate::parser::Registry;
use crate::parser::types::{OtherContents, Type, TypeCategories};

#[derive(Debug)]
pub struct BitField {
    pub mask_name: (CName, RSName),
    pub mask_aliases: Vec<(CName, RSName)>,
    pub enum_name: Option<(CName, RSName)>,
    pub enum_aliases: Vec<(CName, RSName)>,
    pub bit_width: u8,
    pub variants: BTreeMap<u64, Variant>,
}

impl Named for BitField {
    fn get_names(&self) -> Vec<(&CName, &RSName)> {
        if let Some((c, rs)) = &self.enum_name {
            core::iter::once((&self.mask_name.0, &self.mask_name.1))
                .chain(self.mask_aliases.iter().map(|(c, rs)| (c, rs)))
                .chain(core::iter::once((c, rs)))
                .chain(self.enum_aliases.iter().map(|(c, rs)| (c, rs)))
                .collect()
        } else {
            core::iter::once((&self.mask_name.0, &self.mask_name.1))
                .chain(self.mask_aliases.iter().map(|(c, rs)| (c, rs)))
                .collect()
        }
    }
}

#[derive(Debug)]
pub struct Variant {
    pub name: (CName, RSName),
    pub aliases: Vec<(CName, RSName)>,
    pub value: String,
}

pub fn format_bit_fields(registry: &Registry, disabled_manager: &DisabledManager) -> Vec<BitField> {
    //TODO: more derives

    let mut mask_aliases = HashMap::<CName, Vec<(CName, RSName)>>::new();
    let mut enum_aliases = HashMap::<CName, Vec<(CName, RSName)>>::new();

    let mut bit_fields: Vec<_> = registry.types.iter().filter_map(|t| {
        match t {
            Type::Other { requires, name, alias, category, contents, bit_values, .. } => {
                if category == &TypeCategories::BitMask && !name.as_ref().map(|n| disabled_manager.is_disabled(n)).unwrap_or(false) {
                    if let Some(alias) = alias {
                        let mask_c_alias = alias.clone();

                        let mask_c_name = name.clone().unwrap();
                        let mask_rs_name = global_conversion(trim_vk_prefix(&mask_c_name));

                        mask_aliases.entry(mask_c_alias).or_default().push((mask_c_name, mask_rs_name));

                        None
                    } else {
                        let mask_c_name = contents.iter().find_map(|c| if let OtherContents::Name(name) = c { Some(name.clone()) } else { None }).expect("Bitmask with no name");

                        if disabled_manager.is_disabled(&mask_c_name) {
                            None
                        } else {
                            let mask_rs_name = global_conversion(trim_vk_prefix(&mask_c_name));

                            let enum_c_name = requires.clone().or(bit_values.clone());
                            let enum_rs_name = enum_c_name.as_ref().map(|e| global_conversion(trim_vk_prefix(e))).map(|s| "Raw".to_string() + &s);

                            Some(BitField {
                                mask_name: (mask_c_name, mask_rs_name),
                                mask_aliases: vec![],
                                enum_name: enum_c_name.zip(enum_rs_name),
                                enum_aliases: vec![],
                                bit_width: 32,
                                variants: BTreeMap::new(),
                            })
                        }
                    }
                } else {
                    None
                }
            }
            Type::Enum { name, alias, .. } => {
                if let Some(alias) = alias {
                    let c_name = name.clone();
                    let rs_name = "Raw".to_string() + &global_conversion(trim_vk_prefix(&c_name));

                    let alias_c_name = alias.clone();

                    enum_aliases.entry(alias_c_name).or_default().push((c_name, rs_name));
                };
                None
            }
            _ => None,
        }
    }).collect();

    // let bit_field_mask_lookup: HashMap<CName, usize> = bit_fields.iter().enumerate().map(|(i, b)| (b.mask_name.0.clone(), i)).collect();
    let bit_field_enum_lookup: HashMap<CName, usize> = bit_fields.iter().enumerate().filter_map(|(i, b)| b.enum_name.clone().map(|(e, _)| (e, i))).collect();

    for b in &mut bit_fields {
        if let Some(aliases) = mask_aliases.remove(&b.mask_name.0) {
            b.mask_aliases = aliases;
        }
        if let Some(enum_name) = &b.enum_name {
            if let Some(aliases) = enum_aliases.remove(&enum_name.0) {
                b.enum_aliases = aliases;
            }
        }
    }

    for e in registry.enums.iter().filter(|e| e.enum_type == EnumTypes::BitMask && !disabled_manager.is_disabled(e.name.as_ref().unwrap())) {
        let enum_c_name = e.name.clone().unwrap();
        let enum_rs_name = "Raw".to_string() + &global_conversion(trim_vk_prefix(&enum_c_name));

        if let Some(i) = bit_field_enum_lookup.get(&enum_c_name) {
            let mut bit_field = &mut bit_fields[*i];

            bit_field.bit_width = e.bit_width;

            let mut variant_alias = HashMap::<CName, Vec<(CName, RSName)>>::new();

            bit_field.variants = e.variants.iter().filter_map(|v| {
                let c_name = v.name.clone();
                let rs_name = format_bit_field_variant_name(&enum_rs_name, &c_name, registry);

                match &v.variant_type {
                    EnumVariantType::Value { value, value_type: _ } => {
                        let value = if let Some(value) = value.strip_prefix("0x") {
                            u64::from_str_radix(value, 16).expect(&format!("Value not hex: {}", value))
                        } else {
                            value.parse::<u64>().expect(&format!("Value not int: {}", value))
                        };

                        Some(value)
                    }
                    EnumVariantType::Bitpos(bitpos) => {
                        Some(1u64 << bitpos.parse::<u32>().unwrap())
                    }
                    EnumVariantType::Alias(alias) => {
                        variant_alias.entry(alias.clone()).or_default().push((c_name.clone(), rs_name.clone()));

                        None
                    }
                }.map(|value| {
                    let value_str = format_bit_field_value(value);

                    (value, Variant {
                        name: (c_name, rs_name),
                        aliases: vec![],
                        value: value_str,
                    })
                })
            }).collect();

            for v in bit_field.variants.values_mut() {
                if let Some(aliases) = variant_alias.remove(&v.name.0) {
                    v.aliases = aliases;
                }
            }
        } else {
            // eprintln!("No bitfield associated with enum: {}", enum_c_name);
        }
    }

    registry.features.iter().map(|f| (&f.require_removes, None)).chain(registry.extensions.iter().filter(|e| !e.supported.iter().any(|s| s == "disabled")).map(|r| (&r.require_removes, r.number))).for_each(|(r, _number)| {
        r.iter().filter(|r| r.rr_type == RequireRemoveType::Required).for_each(|r| {
            r.enums.iter().for_each(|e| {
                match &e.enum_type {
                    EnumType::Reference => {}
                    EnumType::Extension(eex) => {
                        match eex {
                            EnumExtensionType::NumericValue { value, value_type: _, extends } => {
                                if let Some(extends) = extends {
                                    if let Some(i) = bit_field_enum_lookup.get(extends) {
                                        let bit_field = &mut bit_fields[*i];

                                        let c_name = e.name.clone();
                                        let rs_name = format_bit_field_variant_name(&bit_field.enum_name.as_ref().unwrap().1, &c_name, registry);

                                        let value = if let Some(value) = value.strip_prefix("0x") {
                                            u64::from_str_radix(value, 16).expect(&format!("Value not hex: {}", value))
                                        } else {
                                            value.parse::<u64>().expect(&format!("Value not int: {}", value))
                                        };

                                        let value_str = format_bit_field_value(value);

                                        bit_field.variants.insert(value, Variant {
                                            name: (c_name, rs_name),
                                            aliases: vec![],
                                            value: value_str,
                                        });
                                    }
                                }
                            }
                            EnumExtensionType::BitmaskValue { bitpos, extends } => {
                                if let Some(extends) = extends {
                                    if let Some(i) = bit_field_enum_lookup.get(extends) {
                                        let bit_field = &mut bit_fields[*i];

                                        let c_name = e.name.clone();
                                        let rs_name = format_bit_field_variant_name(&bit_field.enum_name.as_ref().unwrap().1, &c_name, registry);

                                        let value = 1u64 << *bitpos;
                                        let value_str = format_bit_field_value(value);

                                        bit_field.variants.insert(value, Variant {
                                            name: (c_name, rs_name),
                                            aliases: vec![],
                                            value: value_str,
                                        });
                                    }
                                }
                            }
                            EnumExtensionType::Alias { alias, extends } => {
                                if let Some(extends) = extends {
                                    if let Some(i) = bit_field_enum_lookup.get(extends) {
                                        let bit_field = &mut bit_fields[*i];

                                        let c_name = e.name.clone();
                                        let rs_name = format_bit_field_variant_name(&bit_field.enum_name.as_ref().unwrap().1, &c_name, registry);

                                        if let Some(v) = bit_field.variants.values_mut().find(|v| &v.name.0 == alias) {
                                            v.aliases.push((c_name, rs_name));
                                        }
                                    }
                                }
                            }
                            EnumExtensionType::ValueAdded { .. }  => {}
                        }
                    }
                }
            })
        });
    });

    //
    for b in &mut bit_fields {
        if /* !b.variants.contains_key(&0) */ b.variants.is_empty() {
            // if let Some(e) = b.variants.iter().find(|v| &v.1.name.1 == "eNone") {
            //     panic!("0 already defined but with different name: {:?}", e);
            // }

            b.variants.insert(0, Variant {
                name: ("".to_string(), "eNone".to_string()),
                // name: ("None".to_string(), "None".to_string()),
                aliases: vec![],
                value: "0".to_string(),
            });
        }
    }

    bit_fields
}

fn format_bit_field_value(value: u64) -> String {
    // TODO: Consider desired format for bitfield values, bit shifts or binary literals, hex literals, octal, a mix?
    // if value.is_power_of_two() && value != 0 {
    //     format!("1 << {}", value.trailing_zeros())
    // } else
    if value == 0 {
        "0".to_string()
    } else {
        format!("{:#b}", value)
    }
}

fn format_bit_field_variant_name(e_rs_name: &str, v_c_name: &str, registry: &Registry) -> String {
    let e_rs_name = e_rs_name.trim_start_matches("Raw");

    let (e_rs_name, e_rs_name_suffix) = {
        let trimmed = trim_tag_suffix(e_rs_name, registry.tags.iter());
        let suffix = &e_rs_name[trimmed.len()..];
        (trimmed, suffix)
    };

    let e_rs_name = if let Some((a, b)) = e_rs_name.split_once(char::is_numeric) {
        a.trim_end_matches("FlagBits").to_string() + &e_rs_name[a.len()..]
    } else {
        e_rs_name.to_string()
    };

    let v_c_name = global_conversion(v_c_name);
    let v_c_name = if let Some(v_c_name) = v_c_name.strip_suffix(&e_rs_name_suffix) {
        v_c_name
    } else {
        v_c_name.as_str()
    };

    let mut name = trim_vk_prefix(v_c_name).to_camel_case();

    if let Some(s_name) = name.strip_prefix(&e_rs_name) {
        name = s_name.to_string();
    }

    let mut i = 0;
    for (j, (a, b)) in name.chars().zip(e_rs_name.chars()).enumerate() {
        if j == 0 && (a != b || !a.is_uppercase()) {
            break;
        } else if j == 0 {
            continue;
        }

        if (a.is_uppercase() && b.is_uppercase()) || (a.is_uppercase() && b.is_numeric()) || (a.is_numeric() && b.is_uppercase()) {
            i = j;
        }
        if a != b {
            // if a.is_uppercase() && b.is_uppercase() {
            //     i = j;
            // }

            break;
        }
    }

    "e".to_string() + &name[i..]
    // name
}

impl BitField {
    pub fn format(&self) -> Result<String, std::fmt::Error> {
        let mut f = String::new();

        if let Some(enum_name) = &self.enum_name {
            // writeln!(f, "#[repr(u{})]", self.bit_width)?;
            writeln!(f, "#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]")?;
            writeln!(f, "#[allow(non_camel_case_types)]")?;
            writeln!(f, "#[non_exhaustive]")?;
            writeln!(f, "pub enum {} {{", enum_name.1.trim_start_matches("Raw"))?;
            for v in self.variants.values().filter(|v| !v.name.0.is_empty()) {
                writeln!(f, "{}{},", INDENT, v.name.1)?;
            }
            writeln!(f, "{}eUnknownBit(u{}),", INDENT, self.bit_width)?;
            writeln!(f, "}}")?;

            writeln!(f, "")?;

            for a in &self.enum_aliases {
                writeln!(f, "pub type {} = {};", a.1.trim_start_matches("Raw"), enum_name.1.trim_start_matches("Raw"))?;
            }

            if !self.enum_aliases.is_empty() {
                writeln!(f, "")?;
            }

            writeln!(f, "#[allow(non_upper_case_globals)]")?;
            writeln!(f, "impl {} {{", enum_name.1.trim_start_matches("Raw"))?;

            for v in self.variants.values().filter(|v| !v.name.0.is_empty()) {
                for a in &v.aliases {
                    writeln!(f, "{}pub const {}: Self = Self::{};", INDENT, a.1, v.name.1)?;
                }
            }

            if self.variants.values().filter(|v| !v.name.0.is_empty()).map(|v| v.aliases.len()).sum::<usize>() != 0 {
                writeln!(f, "")?;
            }

            writeln!(f, "{0}pub fn into_raw(self) -> {1} {{", INDENT, enum_name.1)?;
            {
                writeln!(f, "{0}{0}match self {{", INDENT)?;
                {
                    for v in self.variants.values().filter(|v| !v.name.0.is_empty()) {
                        writeln!(f, "{0}{0}{0}Self::{1} => {2}::{1},", INDENT, v.name.1, enum_name.1)?;
                    }
                    writeln!(f, "{0}{0}{0}Self::eUnknownBit(b) => {1}(b),", INDENT, enum_name.1)?;
                }
                writeln!(f, "{0}{0}}}", INDENT)?;
            }
            writeln!(f, "{0}}}", INDENT)?;

            writeln!(f, "}}")?;
            writeln!(f, "")?;

            writeln!(f, "impl core::convert::From<{0}> for {1} {{", enum_name.1.trim_start_matches("Raw"), self.mask_name.1)?;
            {
                writeln!(f, "{0}fn from(value: {1}) -> Self {{", INDENT, enum_name.1.trim_start_matches("Raw"))?;
                {
                    writeln!(f, "{0}{0}match value {{", INDENT)?;
                    {
                        for v in self.variants.values().filter(|v| !v.name.0.is_empty()) {
                            writeln!(f, "{0}{0}{0}{2}::{1} => {3}::{1},", INDENT, v.name.1, enum_name.1.trim_start_matches("Raw"), self.mask_name.1)?;
                        }
                        writeln!(f, "{0}{0}{0}{1}::eUnknownBit(b) => {2}(b),", INDENT, enum_name.1.trim_start_matches("Raw"), self.mask_name.1)?;
                    }
                    writeln!(f, "{0}{0}}}", INDENT)?;
                }
                writeln!(f, "{0}}}", INDENT)?;
            }
            writeln!(f, "}}")?;
            writeln!(f, "")?;

            writeln!(f, "bit_field_enum_derives!({0}, {1}, u{2});", enum_name.1.trim_start_matches("Raw"), self.mask_name.1, self.bit_width)?;

            writeln!(f, "")?;
        }

        writeln!(f, "#[repr(C)]")?;
        writeln!(f, "#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]")?;
        writeln!(f, "pub struct {}(pub(crate) u{});", self.mask_name.1, self.bit_width)?;

        if self.mask_name.1 == "GeometryInstanceFlagsKHR" {
            writeln!(f, "")?;
            writeln!(f, "{}", HARDCODED_GEOMETRY_INSTANCE_FLAGS_KHR_FIELD_TYPE)?;
        }

        writeln!(f, "")?;

        for a in &self.mask_aliases {
            writeln!(f, "pub type {} = {};", a.1, self.mask_name.1)?;
        }

        if !self.mask_aliases.is_empty() {
            writeln!(f, "")?;
        }

        if !self.variants.is_empty() {
            writeln!(f, "#[allow(non_upper_case_globals)]")?;
            writeln!(f, "impl {} {{", self.mask_name.1)?;
            if !self.variants.contains_key(&0) {
                writeln!(f, "{}pub const eNone: Self = Self(0);", INDENT)?;
            }
            for v in self.variants.values() {
                writeln!(f, "{}pub const {}: Self = Self({});", INDENT, v.name.1, v.value)?;
            }
            for v in self.variants.values() {
                for a in v.aliases.iter().filter(|a| &a.1 != &v.name.1) {
                    writeln!(f, "{}pub const {}: Self = Self::{};", INDENT, a.1, v.name.1)?;
                }
            }
            writeln!(f, "}}")?;

            writeln!(f, "")?;
        }

        writeln!(f, "bit_field_mask_derives!({}, u{});", self.mask_name.1, self.bit_width)?;

        writeln!(f, "")?;

        if let Some(enum_name) = &self.enum_name {
            if let Some(d) = self.variants.get(&0) {
                if !(d.name.0.is_empty()) {
                    writeln!(f, "bit_field_mask_with_enum_debug_derive!({}, {}, u{}, {} => {});", enum_name.1.trim_start_matches("Raw"), self.mask_name.1, self.bit_width,
                             self.variants.iter().filter(|v| *v.0 != 0).map(|v| format!("{}::{}", enum_name.1.trim_start_matches("Raw"), v.1.name.1)).collect::<Vec<String>>().join(", "),
                             format!("{}::{}", enum_name.1.trim_start_matches("Raw"), d.name.1))?;
                } else {
                    writeln!(f, "bit_field_mask_with_enum_debug_derive!({}, {}, u{}, {});", enum_name.1.trim_start_matches("Raw"), self.mask_name.1, self.bit_width,
                             self.variants.iter().filter(|v| *v.0 != 0).map(|v| format!("{}::{}", enum_name.1.trim_start_matches("Raw"), v.1.name.1)).collect::<Vec<String>>().join(", "))?;
                }
            } else {
                writeln!(f, "bit_field_mask_with_enum_debug_derive!({}, {}, u{}, {});", enum_name.1.trim_start_matches("Raw"), self.mask_name.1, self.bit_width,
                         self.variants.iter().filter(|v| *v.0 != 0).map(|v| format!("{}::{}", enum_name.1.trim_start_matches("Raw"), v.1.name.1)).collect::<Vec<String>>().join(", "))?;
            }
        } else {
            writeln!(f, "fieldless_debug_derive!({});", self.mask_name.1)?;
        }

        writeln!(f, "")?;

        Ok(f)
    }

    pub fn format_raw(&self) -> Result<String, std::fmt::Error> {
        let mut f = String::new();

        if let Some(enum_name) = &self.enum_name {
            writeln!(f, "#[repr(C)]")?;
            writeln!(f, "#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]")?;
            writeln!(f, "pub struct {}(pub u{});", enum_name.1, self.bit_width)?;
            writeln!(f)?;

            for a in &self.enum_aliases {
                writeln!(f, "pub type {} = {};", a.1, enum_name.1)?;
            }

            if !self.enum_aliases.is_empty() {
                writeln!(f, "")?;
            }

            writeln!(f, "#[allow(non_upper_case_globals)]")?;
            writeln!(f, "impl {} {{", enum_name.1)?;
            {
                for v in self.variants.values().filter(|v| !v.name.0.is_empty()) {
                    writeln!(f, "{}pub const {}: Self = Self({});", INDENT, v.name.1, v.value)?;
                }
                for v in self.variants.values().filter(|v| !v.name.0.is_empty()) {
                    for a in v.aliases.iter().filter(|a| &a.1 != &v.name.1) {
                        writeln!(f, "{}pub const {}: Self = Self::{};", INDENT, a.1, v.name.1)?;
                    }
                }
                if self.variants.values().filter(|v| !v.name.0.is_empty()).next().is_some() {
                    writeln!(f)?;
                }

                writeln!(f, "{0}pub fn normalise(self) -> {1} {{", INDENT, enum_name.1.trim_start_matches("Raw"))?;
                {
                    writeln!(f, "{0}{0}match self {{", INDENT)?;
                    for v in self.variants.values().filter(|v| !v.name.0.is_empty()) {
                        writeln!(f, "{0}{0}{0}Self::{1} => {2}::{1},", INDENT, v.name.1, enum_name.1.trim_start_matches("Raw"))?;
                    }
                    writeln!(f, "{0}{0}{0}{1}(b) => {2}::eUnknownBit(b),", INDENT, enum_name.1, enum_name.1.trim_start_matches("Raw"))?;
                    writeln!(f, "{0}{0}}}", INDENT)?;
                }
                writeln!(f, "{0}}}", INDENT)?;

                if !self.variants.contains_key(&0) {
                    writeln!(f)?;
                    writeln!(f, "{0}pub fn normalise_optional(self) -> Option<{1}> {{", INDENT, enum_name.1.trim_start_matches("Raw"))?;
                    {
                        writeln!(f, "{0}{0}match self {{", INDENT)?;
                        writeln!(f, "{0}{0}{0}{1}(0) => None,", INDENT, enum_name.1)?;
                        for v in self.variants.values() {
                            writeln!(f, "{0}{0}{0}Self::{1} => Some({2}::{1}),", INDENT, v.name.1, enum_name.1.trim_start_matches("Raw"))?;
                        }
                        writeln!(f, "{0}{0}{0}{1}(b) => Some({2}::eUnknownBit(b)),", INDENT, enum_name.1, enum_name.1.trim_start_matches("Raw"))?;
                        writeln!(f, "{0}{0}}}", INDENT)?;
                    }
                    writeln!(f, "{0}}}", INDENT)?;
                }
            }
            writeln!(f, "}}")?;
            writeln!(f)?;

            writeln!(f, "impl core::fmt::Debug for {} {{", enum_name.1)?;
            {
                writeln!(f, "{}fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {{", INDENT)?;
                {
                    writeln!(f, "{0}{0}self.normalise().fmt(f)", INDENT)?;
                }
                writeln!(f, "{}}}", INDENT)?;
            }
            writeln!(f, "}}")?;
        }

        Ok(f)
    }
}

const HARDCODED_GEOMETRY_INSTANCE_FLAGS_KHR_FIELD_TYPE: &str = r#"use c2rust_bitfields::FieldType;

impl FieldType for GeometryInstanceFlagsKHR {
    const IS_SIGNED: bool = false;

    fn get_bit(&self, bit: usize) -> bool {
        self.0.get_bit(bit)
    }

    fn set_field(&self, field: &mut [u8], bit_range: (usize, usize)) {
        self.0.set_field(field, bit_range)
    }

    fn get_field(field: &[u8], bit_range: (usize, usize)) -> Self {
        GeometryInstanceFlagsKHR(u32::get_field(field, bit_range))
    }
}"#;