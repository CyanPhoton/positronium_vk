use std::fmt::Write;
use heck::CamelCase;

use crate::formatter::{INDENT, Spec};
use crate::formatter::functions::OxidisedParameterType;

pub fn format(spec: &Spec) -> Result<String, std::fmt::Error> {
    let mut f = String::new();

    let cmd_handle = spec.handles.iter().find(|h| h.name.1 == "CommandBuffer")
        .expect("Should be a CommandBuffer handle");

    writeln!(f, "{}", START)?;

    let mut printed = false;

    for (func, do_ox) in &cmd_handle.functions {
        if *do_ox {
            if let Some(o) = &func.oxidised {
                if o.name.starts_with("cmd_") {
                    if printed {
                        writeln!(f, "")?;
                    }
                    printed = true;

                    if let Some(rt) = &o.return_type {
                        if !rt.success_codes.is_empty() {
                            writeln!(f, "{0}/// SUCCESS_CODES = [{1}];", INDENT, rt.success_codes.iter().map(|s| format!("[SuccessCode::e{}]", s.to_camel_case())).collect::<Vec<_>>().join(", "))?;
                        }
                        if !rt.success_codes.is_empty() && !rt.error_codes.is_empty() {
                            writeln!(f, "{}///", INDENT)?;
                        }
                        if !rt.error_codes.is_empty() {
                            writeln!(f, "{0}/// ERROR_CODES = [{1}];", INDENT, rt.error_codes.iter().map(|s| format!("[ErrorCode::e{}]", s.to_camel_case())).collect::<Vec<_>>().join(", "))?;
                        }
                    }

                    let needs_vec = if let Some(rt) = &o.return_type {
                        rt.values.iter().any(|v| {
                            match v {
                                OxidisedParameterType::Value { .. } => false,
                                OxidisedParameterType::Array { .. } => true,
                            }
                        })
                    } else { false } || ["build_acceleration_structures_khr", "cmd_build_acceleration_structures_khr", "cmd_build_acceleration_structures_indirect_khr"].contains(&o.name.as_str());

                    if needs_vec {
                        write!(f, "{0}pub fn {1}<V: GenericVecGen>(&mut self", INDENT, o.name.trim_start_matches("cmd_"))?;
                    } else {
                        write!(f, "{0}pub fn {1}(&mut self", INDENT, o.name.trim_start_matches("cmd_"))?;
                    }


                    for p in o.parameters.iter().skip(1) {
                        let mut optional_handle = false;
                        let ty = match &p.p_type {
                            OxidisedParameterType::Value { c_param_index } => {
                                optional_handle = o.raw_parameters[*c_param_index].value_optional;
                                if spec.handles.iter().any(|h| h.name.0 == o.raw_parameters[*c_param_index].type_name.0) {
                                    o.raw_parameters[*c_param_index].full_type.1.clone().replace("Raw", "Handle")
                                } else {
                                    o.raw_parameters[*c_param_index].full_type.1.clone()
                                }
                            }
                            OxidisedParameterType::Array { c_ptr_index, c_len_index } => {
                                if spec.handles.iter().any(|h| h.name.0 == o.raw_parameters[*c_ptr_index].type_name.0) {
                                    o.raw_parameters[*c_ptr_index].slice_type.clone().replace("Raw", "Handle")
                                } else {
                                    o.raw_parameters[*c_ptr_index].slice_type.clone()
                                }
                            }
                        };
                        match (o.name.as_str(), p.name.as_str()) {
                            ("cmd_build_acceleration_structures_khr", "build_range_infos") | ("cmd_build_acceleration_structures_indirect_khr", "max_primitive_counts") => {
                                write!(f, ", {}: {}", p.name, ty.replace("&[&", "&[&[").replace("]", "]]"))?;
                                continue;
                            }
                            ("cmd_set_sample_mask_ext", "sample_mask") => {
                                write!(f, ", samples: SampleCountFlagBits, {}: {}", p.name, ty.trim_start_matches("Raw"))?;
                                continue;
                            }
                            _ => {}
                        }
                        if ty.ends_with("Handle") {
                            if p.extern_sync {
                                if optional_handle {
                                    write!(f, ", {}: Option<&mut {}>", p.name, ty.replace("Handle", ""))?;
                                } else {
                                    write!(f, ", {}: &mut {}", p.name, ty.replace("Handle", ""))?;
                                }
                            } else {
                                if optional_handle {
                                    write!(f, ", {}: Option<&{}>", p.name, ty.replace("Handle", ""))?;
                                } else {
                                    write!(f, ", {}: &{}", p.name, ty.replace("Handle", ""))?;
                                }
                            }
                        } else {
                            write!(f, ", {}: {}", p.name, ty.trim_start_matches("Raw"))?;
                        }
                        if let OxidisedParameterType::Value { c_param_index } = &p.p_type {
                            if !o.raw_parameters[*c_param_index].generic_parameters.is_empty() {
                                write!(f, "<{}>", o.raw_parameters[*c_param_index].generic_parameters.iter().map(|g| g.name.as_str()).collect::<Vec<_>>().join(", "))?;
                            }
                        }
                    }

                    write!(f, ") ")?;

                    if let Some(rt) = &o.return_type {
                        let mut t: String = rt.values.iter().map(|p| {
                            match p {
                                OxidisedParameterType::Value { c_param_index } => {
                                    let mut s = o.raw_parameters[*c_param_index].full_type.1.clone();
                                    if s.starts_with("&mut ") {
                                        s = s.replacen("&mut ", "", 1);
                                    } else {
                                        eprintln!("Check: {}", s);
                                    }
                                    s.replace("Raw", "")
                                }
                                OxidisedParameterType::Array { c_ptr_index, c_len_index } => {
                                    let mut s = o.raw_parameters[*c_ptr_index].full_type.1.clone();
                                    if s.starts_with("Option<") {
                                        s = s.replacen("Option<", "", 1);
                                        s = s.rsplit_once(">").unwrap().0.to_string();
                                    }
                                    if s.starts_with("&mut ") {
                                        s = s.replacen("&mut ", "", 1);
                                    } else {
                                        eprintln!("Check (2): {}", s);
                                    }
                                    format!("Vec<{}>", s.replace("Raw", ""))
                                }
                            }
                        }).collect::<Vec<String>>().join(", ");

                        if &o.name == "map_memory" {
                            t = "&'device_memory mut [u8]".to_string();
                        }

                        if rt.has_result {
                            if !t.is_empty() {
                                t = format!("core::result::Result<({}, SuccessCode), ErrorCode>", t);
                            } else {
                                t = format!("core::result::Result<SuccessCode, ErrorCode>");
                            }
                        } else if let Some(o) = &rt.other_return {
                            if t.is_empty() {
                                t = o.clone();
                            } else {
                                t = format!("({}, {})", t, o);
                            }
                        }

                        if !t.is_empty() {
                            write!(f, "-> {} ", t)?;
                        }
                    }
                    writeln!(f, "{{")?;

                    if needs_vec {
                        write!(f, "{0}{0}unsafe {{ self.command_buffer.{1}_unchecked::<V>(", INDENT, o.name)?;
                    } else {
                        write!(f, "{0}{0}unsafe {{ self.command_buffer.{1}_unchecked(", INDENT, o.name)?;
                    }

                    for (i, p) in o.parameters.iter().skip(1).enumerate() {
                        if i == 0 {
                            if let ("cmd_set_sample_mask_ext", "sample_mask") = (o.name.as_str(), p.name.as_str()) { write!(f, "samples, ")?; }
                            write!(f, "{}", p.name)?;
                        } else {
                            if let ("cmd_set_sample_mask_ext", "sample_mask") = (o.name.as_str(), p.name.as_str()) { write!(f, ", samples")?; }
                            write!(f, ", {}", p.name)?;
                        }
                    }

                    writeln!(f, ") }}")?;

                    writeln!(f, "{0}}}", INDENT)?;
                }
            }
        }
    }

    write!(f, "{}", END)?;

    Ok(f)
}

const START: &str = "pub struct RecordingCommandBuffer<'cmd> {
    command_buffer: &'cmd mut CommandBuffer,
}

impl CommandBuffer {
    pub fn record_with<F: FnOnce(RecordingCommandBuffer) -> core::result::Result<(), ErrorCode>>(&mut self, command_pool: &mut CommandPool, begin_info: &RawCommandBufferBeginInfo, record: F) -> core::result::Result<(SuccessCode, SuccessCode), ErrorCode> {
        let begin_result = self.begin_command_buffer(command_pool, begin_info)?;

        let recording = RecordingCommandBuffer {
            command_buffer: self,
        };

        record(recording)?;

        let end_result = self.end_command_buffer(command_pool)?;

        Ok((begin_result, end_result))
    }
}

impl<'cmd> RecordingCommandBuffer<'cmd> {";

const END: &str = "}";