use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::io::ErrorKind;

use base_types::BaseType;
use bit_fields::BitField;
use defines::Define;
use enums::Enum;
use extern_types::ExternType;
use function_types::FunctionType;
use functions::Function;
use handles::Handle;

use crate::formatter::common::ParameterValueType;
use crate::formatter::constants::Constant;
use crate::formatter::structs_unions::{StructUnion, Variant};
use crate::parser::Registry;
use crate::parser::tags::Tag;

pub mod base_types;
pub mod extern_types;
pub mod defines;
pub mod handles;
pub mod function_types;
pub mod enums;
pub mod constants;
pub mod bit_fields;
pub mod structs_unions;
pub mod functions;
pub mod disabled_manager;
pub mod common;
mod command_utility;
mod patches;

pub type CName = String;
pub type RSName = String;

const INDENT: &'static str = "    ";

trait Named {
    fn get_names(&self) -> Vec<(&CName, &RSName)>;
}

pub struct EnumValueRef {
    pub value: i64,
    pub bit_width: u8,
    pub full_variant: String,
}

pub fn format_spec(mut registry: Registry) -> Spec {
    patches::patch_extern_sync(&mut registry);

    let mut disabled_manager = disabled_manager::DisabledManager::new(&registry);

    let defines = defines::format_defines(&registry, &disabled_manager);

    let extern_types = extern_types::format_extern_types(&registry, &mut disabled_manager);
    let base_types = base_types::format_base_types(&registry);
    let enums = enums::format_enums(&registry, &disabled_manager);
    let constants = constants::format_constants(&registry);
    let bit_fields = bit_fields::format_bit_fields(&registry, &disabled_manager);
    let function_types = function_types::format_function_types(&registry, &disabled_manager);
    let mut handles = handles::format_handles(&registry);

    let base_c_types: Vec<_> = ["uint8_t", "int8_t", "uint16_t", "int16_t", "uint32_t", "int32_t", "uint64_t", "int64_t", "size_t", "float", "double", "int", "char", "void"].iter().map(|s| s.to_string()).collect();
    let base_rs_types: Vec<_> = base_c_types.iter().map(String::as_str).map(c_type_to_rs_type).collect();

    let int_alias_types = base_types.iter()
        .filter(|b| &b.name.1 != "Bool32")
        .filter_map(|b| b.t.as_ref().map(|t| (&b.name, t)))
        .filter_map(|(n, t)| (
            t.1.starts_with('u') || t.1.starts_with('i')
        ).then(|| n.1.clone()))
        .collect::<Vec<_>>();

    let mut rs_name_map: HashMap<_, _> = base_types.iter().flat_map(Named::get_names)
        .chain(extern_types.iter().flat_map(Named::get_names))
        .chain(enums.iter().flat_map(Named::get_names))
        .chain(constants.iter().flat_map(Named::get_names))
        .chain(bit_fields.iter().flat_map(Named::get_names))
        .chain(function_types.iter().flat_map(Named::get_names))
        .chain(handles.iter().flat_map(Named::get_names))
        .chain(base_c_types.iter().zip(base_rs_types.iter()))
        .map(|(c, rs)| (c.clone(), rs.clone()))
        .collect();

    let enum_map: HashMap<CName, EnumValueRef> = enums.iter().flat_map(|e| e.variants.iter().map(move |v| (v.1.name.0.clone(), EnumValueRef {
        value: *v.0,
        bit_width: e.bit_width,
        full_variant: e.name.1.clone() + "::" + &v.1.name.1,
    }))).collect();

    let enum_like = bit_fields.iter().map(|b| b.mask_name.1.clone())
        .chain(bit_fields.iter().filter_map(|b| b.enum_name.as_ref().map(|e| e.1.clone())))
        .chain(enums.iter().map(|e| e.name.1.clone()))
        .collect::<HashSet<_>>();

    let structs_unions = structs_unions::format_structs_unions(&registry, &mut rs_name_map, &handles, &enum_map, &disabled_manager, &function_types, &int_alias_types, &enum_like);
    let functions = {
        let structs_unions = structs_unions.clone().into_iter().map(|s| (s.name.0.clone(), s)).collect();

        functions::format_functions(&registry, &disabled_manager, &mut rs_name_map, &handles, &function_types, &structs_unions, &int_alias_types, &extern_types, &enum_like)
    };

    handles::format_with_functions(&mut handles, &functions);

    Spec {
        base_types,
        extern_types,
        defines,
        handles,
        function_types,
        enums,
        constants,
        bit_fields,
        structs_unions,
        functions,
    }
}

#[derive(Debug)]
pub struct Spec {
    base_types: Vec<BaseType>,
    extern_types: Vec<ExternType>,
    defines: Vec<Define>,
    handles: Vec<Handle>,
    function_types: Vec<FunctionType>,
    enums: Vec<Enum>,
    constants: Vec<Constant>,
    bit_fields: Vec<BitField>,
    structs_unions: Vec<StructUnion>,
    functions: Vec<Function>,
}

impl Spec {
    pub fn write_multiple_files<G, W>(&self, output_gen: G) -> std::io::Result<()> where G: Fn(&str) -> W, W: std::io::Write {
        self.write_to_file("lib.rs", &output_gen, |f| {
            Self::lib_prelude(f)?;

            writeln!(f, "pub mod base_types;")?;
            writeln!(f, "pub mod extern_types;")?;
            writeln!(f, "pub mod defines;")?;
            writeln!(f, "pub mod constants;")?;
            writeln!(f, "pub mod function_types;")?;
            writeln!(f, "pub mod enums;")?;
            writeln!(f, "pub mod bit_fields;")?;
            writeln!(f, "pub mod structs_unions;")?;
            writeln!(f, "pub mod handles;")?;
            writeln!(f, "pub mod command_utility;")?;
            writeln!(f, "pub mod enum_utility;")?;
            writeln!(f, "pub mod vector_utility;")?;

            writeln!(f, "")?;

            writeln!(f, "pub use base_types::*;")?;
            writeln!(f, "pub use extern_types::*;")?;
            writeln!(f, "pub use defines::*;")?;
            writeln!(f, "pub use constants::*;")?;
            writeln!(f, "pub use function_types::*;")?;
            writeln!(f, "pub use enums::*;")?;
            writeln!(f, "pub use bit_fields::*;")?;
            writeln!(f, "pub use structs_unions::*;")?;
            writeln!(f, "pub use handles::*;")?;
            writeln!(f, "pub use command_utility::*;")?;
            writeln!(f, "pub use enum_utility::*;")?;
            writeln!(f, "pub use vector_utility::*;")?;

            Ok(())
        })?;

        self.write_to_file("base_types.rs", &output_gen, |f| {
            writeln!(f, "pub use super::*;")?;
            self.write_base_types(f)
        })?;

        self.write_to_file("extern_types.rs", &output_gen, |f| {
            writeln!(f, "pub use super::*;")?;
            self.write_extern_types(f)
        })?;

        self.write_to_file("defines.rs", &output_gen, |f| {
            writeln!(f, "pub use super::*;")?;
            self.write_defines(f)
        })?;

        self.write_to_file("constants.rs", &output_gen, |f| {
            writeln!(f, "pub use super::*;")?;
            writeln!(f, "use unsized_cstr_derive::unsized_cstr_int;")?;
            self.write_constants(f)
        })?;

        self.write_to_file("function_types.rs", &output_gen, |f| {
            writeln!(f, "pub use super::*;")?;
            self.write_function_types(f)
        })?;

        self.write_to_file("enums.rs", &output_gen, |f| {
            writeln!(f, "pub use super::*;")?;
            self.write_enums(f)
        })?;

        self.write_to_file("bit_fields.rs", &output_gen, |f| {
            writeln!(f, "pub use super::*;")?;
            self.write_bit_fields(f)
        })?;

        self.write_to_file("structs_unions.rs", &output_gen, |f| {
            writeln!(f, "pub use super::*;")?;
            self.write_structs_unions(f)
        })?;

        self.write_to_file("handles.rs", &output_gen, |f| {
            writeln!(f, "pub use super::*;")?;
            self.write_handles(f)
        })?;

        self.write_to_file("command_utility.rs", &output_gen, |f| {
            writeln!(f, "pub use super::*;")?;
            self.write_command_utility(f)
        })?;

        Ok(())
    }

    pub fn write_single_file<W>(&self, f: &mut W) -> std::io::Result<()> where W: std::io::Write {
        Self::lib_prelude(f)?;
        self.write_base_types(f)?;
        self.write_extern_types(f)?;
        self.write_defines(f)?;
        self.write_constants(f)?;
        self.write_function_types(f)?;
        self.write_enums(f)?;
        self.write_bit_fields(f)?;
        self.write_structs_unions(f)?;
        self.write_handles(f)?;
        self.write_command_utility(f)?;

        Ok(())
    }

    fn write_to_file<G, W, O>(&self, file: &str, output_gen: &G, writer: O) -> std::io::Result<()> where G: Fn(&str) -> W, W: std::io::Write, O: Fn(&mut W) -> std::io::Result<()> {
        let mut output = output_gen(file);
        writer(&mut output)
    }

    fn lib_prelude<W>(f: &mut W) -> std::io::Result<()> where W: std::io::Write {
        writeln!(f, "#[macro_use]")?;
        writeln!(f, "mod macros;")?;
        writeln!(f, "mod version;")?;
        writeln!(f, "mod bool32;")?;
        writeln!(f, "mod bit_field_iter;")?;
        writeln!(f, "mod const_size_ptr;")?;
        writeln!(f, "mod const_size_cstr;")?;
        writeln!(f, "mod unsized_cstr;")?;
        writeln!(f, "mod struct_utility;")?;
        writeln!(f, "mod handle_utility;")?;
        writeln!(f, "#[cfg(feature = \"windowing_utility\")]")?;
        writeln!(f, "mod windowing_utility;")?;
        writeln!(f, "")?;

        writeln!(f, "pub use version::*;")?;
        writeln!(f, "pub use bool32::*;")?;
        writeln!(f, "pub use bit_field_iter::*;")?;
        writeln!(f, "pub use const_size_ptr::*;")?;
        writeln!(f, "pub use const_size_cstr::*;")?;
        writeln!(f, "pub use unsized_cstr::*;")?;
        writeln!(f, "pub use struct_utility::*;")?;
        writeln!(f, "pub use handle_utility::*;")?;
        writeln!(f, "#[cfg(feature = \"windowing_utility\")]")?;
        writeln!(f, "pub use windowing_utility::*;")?;
        writeln!(f, "use defer_alloc::DeferAllocArc;")?;
        writeln!(f, "")?;

        writeln!(f, "pub use unsized_cstr_derive::unsized_cstr;")?;
        writeln!(f, "")?;

        Ok(())
    }

    fn write_base_types<W>(&self, f: &mut W) -> std::io::Result<()> where W: std::io::Write {
        writeln!(f, "// Base types:")?;
        for b in &self.base_types {
            write!(f, "{}", b)?;
        }

        Ok(())
    }

    fn write_extern_types<W>(&self, f: &mut W) -> std::io::Result<()> where W: std::io::Write {
        writeln!(f, "// Extern types:")?;
        for b in &self.extern_types {
            write!(f, "{}", b)?;
        }
        Ok(())
    }

    fn write_defines<W>(&self, f: &mut W) -> std::io::Result<()> where W: std::io::Write {
        writeln!(f, "// Defines:")?;
        for d in &self.defines {
            write!(f, "{}", d)?;
        }
        writeln!(f, "")?;
        Ok(())
    }

    fn write_constants<W>(&self, f: &mut W) -> std::io::Result<()> where W: std::io::Write {
        writeln!(f, "// Constants:")?;
        for c in &self.constants {
            write!(f, "{}", c)?;
        }
        writeln!(f, "")?;
        Ok(())
    }

    fn write_function_types<W>(&self, f: &mut W) -> std::io::Result<()> where W: std::io::Write {
        writeln!(f, "// Function types/pointers:")?;
        for ft in &self.function_types {
            write!(f, "{}", ft)?;
        }
        writeln!(f, "")?;
        for ft in &self.functions {
            write!(f, "{}", ft.raw)?;
        }
        writeln!(f, "")?;
        writeln!(f, "pub(crate) mod invalid_functions {{")?;
        writeln!(f, "{}use super::*;", INDENT)?;
        for ft in self.functions.iter() {
            writeln!(f, "{}", ft.raw.invalid_function().unwrap())?;
        }
        writeln!(f, "}}")?;
        writeln!(f, "")?;
        writeln!(f, "pub(crate) mod null_functions {{")?;
        writeln!(f, "{}use super::*;", INDENT)?;
        for ft in self.functions.iter() {
            writeln!(f, "{}", ft.raw.null_function().unwrap())?;
        }
        writeln!(f, "}}")?;
        writeln!(f, "")?;
        writeln!(f, "pub(crate) mod no_op_destructors {{")?;
        writeln!(f, "{}use super::*;", INDENT)?;
        for ft in self.functions.iter() {
            if self.handles.iter().any(|h| h.destructor.as_ref().map_or(false, |d| &d.raw.name.1 == &ft.raw.name.1)) {
                writeln!(f, "{}", ft.raw.no_op_function().unwrap())?;
            }
        }
        writeln!(f, "}}")?;
        writeln!(f, "")?;
        Ok(())
    }

    fn write_enums<W>(&self, f: &mut W) -> std::io::Result<()> where W: std::io::Write {
        writeln!(f, "// Enums:")?;
        for e in &self.enums {
            if &e.name.0 == "VkResult" {
                writeln!(f, "pub(crate) use result::*;")?;
                writeln!(f, "pub mod result {{")?;
                writeln!(f, "{}use super::*;", INDENT)?;
                let s = e.format().unwrap();
                if s.is_empty() {
                    continue;
                }

                for l in s.lines() {
                    if l.is_empty() {
                        writeln!(f)?;
                    } else {
                        writeln!(f, "{}{}", INDENT, l)?;
                    }
                }
                writeln!(f, "}}")?;
            } else {
                write!(f, "{}", e.format().unwrap())?;
            }
        }
        writeln!(f, "")?;
        writeln!(f, "pub(crate) use raw_enums::*;")?;
        writeln!(f, "pub mod raw_enums {{")?;
        writeln!(f, "{}use super::*;", INDENT)?;
        for e in &self.enums {
            let s = e.format_raw().unwrap();
            if s.is_empty() {
                continue;
            }

            for l in s.lines() {
                if l.is_empty() {
                    writeln!(f)?;
                } else {
                    writeln!(f, "{}{}", INDENT, l)?;
                }
            }
        }
        writeln!(f, "}}")?;
        writeln!(f, "")?;
        Ok(())
    }

    fn write_bit_fields<W>(&self, f: &mut W) -> std::io::Result<()> where W: std::io::Write {
        writeln!(f, "// Bit fields:")?;
        for b in &self.bit_fields {
            write!(f, "{}", b.format().unwrap())?;
        }
        writeln!(f, "")?;
        writeln!(f, "pub(crate) use raw_bit_fields::*;")?;
        writeln!(f, "pub mod raw_bit_fields {{")?;
        writeln!(f, "{}use super::*;", INDENT)?;
        for b in &self.bit_fields {
            let s = b.format_raw().unwrap();
            if s.is_empty() {
                continue;
            }

            for l in s.lines() {
                if l.is_empty() {
                    writeln!(f)?;
                } else {
                    writeln!(f, "{}{}", INDENT, l)?;
                }
            }
        }
        writeln!(f, "}}")?;
        writeln!(f, "")?;
        Ok(())
    }

    fn write_structs_unions<W>(&self, f: &mut W) -> std::io::Result<()> where W: std::io::Write {
        writeln!(f, "// Structs Unions")?;
        for su in &self.structs_unions {
            write!(f, "{}", su.format(self, false).unwrap())?;
            if su.has_inline_p_next(self) && su.variant == Variant::Struct {
                write!(f, "{}", su.format(self, true).unwrap())?;
            }
        }
        writeln!(f, "")?;
        writeln!(f, "pub(crate) use raw_structs::*;")?;
        writeln!(f, "pub mod raw_structs {{")?;
        writeln!(f, "{}use super::*;", INDENT)?;
        for su in &self.structs_unions {
            let s = su.format_raw(self).unwrap();
            if s.is_empty() {
                continue;
            }

            for l in s.lines() {
                if l.is_empty() {
                    writeln!(f)?;
                } else {
                    writeln!(f, "{}{}", INDENT, l)?;
                }
            }
        }
        writeln!(f, "}}")?;
        writeln!(f, "")?;
        Ok(())
    }

    fn write_handles<W>(&self, f: &mut W) -> std::io::Result<()> where W: std::io::Write {
        writeln!(f, "// Handles:")?;
        for h in &self.handles {
            write!(f, "{}", h.format(&self).unwrap())?;
        }
        writeln!(f, "pub(crate) use raw_handles::*;")?;
        writeln!(f, "pub mod raw_handles {{")?;
        writeln!(f, "{}use super::*;", INDENT)?;
        for h in &self.handles {
            write!(f, "{}", h.format_raw(&self).unwrap())?;
        }
        writeln!(f, "}}")?;
        writeln!(f, "")?;
        Ok(())
    }

    fn write_command_utility<W>(&self, f: &mut W) -> std::io::Result<()> where W: std::io::Write {
        writeln!(f, "// Command Utility")?;
        write!(f, "{}", command_utility::format(self).unwrap())?;
        writeln!(f, "")?;
        Ok(())
    }
}

fn trim_vk_prefix(s: &str) -> &str {
    match (s, s.split_at(2), s.split_at(3)) {
        (_, ("Vk", s), _) => s,
        (_, ("vk", s), _) => s,
        (_, _, ("VK_", s)) => s,
        (s, ..) => s
    }
}

fn c_type_to_rs_type<S: AsRef<str>>(s: S) -> String {
    match s.as_ref() {
        "uint8_t" => String::from("u8"),
        "int8_t" => String::from("i8"),
        "uint16_t" => String::from("u16"),
        "int16_t" => String::from("i16"),
        "uint32_t" => String::from("u32"),
        "int32_t" => String::from("i32"),
        "uint64_t" => String::from("u64"),
        "int64_t" => String::from("i64"),
        "size_t" => String::from("usize"),
        "float" => String::from("f32"),
        "double" => String::from("f64"),
        "int" => String::from("::std::os::raw::c_int"),
        "char" => String::from("::std::os::raw::c_char"),
        "void" => String::from("u8"),
        s => panic!("Unexpected c_type: {}", s)
    }
}

fn trim_tag_suffix<'a, 'b, I: Iterator<Item=&'a Tag>>(s: &'b str, tags: I) -> &'b str {
    for tag in tags {
        if s.ends_with(&tag.name) {
            return s.strip_suffix(&tag.name).unwrap();
        }
    }
    s
}

fn trim_unneeded_parenthesis(mut s: &str) -> &str {
    s = s.trim();

    if s.starts_with('(') && s.ends_with(')') {
        let mut level = 0;

        for (i, c) in s.chars().enumerate() {
            match c {
                '(' => {
                    level += 1;
                }
                ')' => {
                    level -= 1;
                    if level == 0 && i != s.len() - 1 {
                        return s;
                    }
                }
                _ => {}
            }
        }

        if level == 0 {
            return trim_unneeded_parenthesis(&s[1..s.len() - 1]);
        }
    }

    s
}