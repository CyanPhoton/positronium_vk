use std::borrow::Cow;

mod parser;
mod formatter;

pub fn main() {
    println!("Building...");

    let version = select_version();

    println!("Using version: {}", version);

    std::fs::create_dir("out").ok();

    let xml_spec = download_vulkan_spec_file(&version);
    std::fs::write("out/vk.xml", &xml_spec).unwrap();

    let xml_video_spec = download_vulkan_video_spec_file(&version);
    std::fs::write("out/video.xml", &xml_video_spec).unwrap();

    let spec = parser::parse(xml_spec, xml_video_spec);

    // dbg!(&spec);

    std::fs::write("out/vk.struct", format!("{:#?}", spec)).unwrap();

    let formatted = formatter::format_spec(spec.unwrap());

    // dbg!(&formatted);

    formatted.write_multiple_files::<_, _>(|path| {
        let file = std::fs::OpenOptions::new()
            .write(true)
            .append(false)
            .truncate(true)
            .create(true)
            .read(true)
            .open(format!("../src/{}", path))
            .expect("Failed to open file for output");

        std::io::BufWriter::new(file)
    }).expect("Failed to write out to file");

    // formatted.write_single_file( &mut {
    //     let file = std::fs::OpenOptions::new()
    //         .write(true)
    //         .append(false)
    //         .truncate(true)
    //         .create(true)
    //         .read(true)
    //         .open("../src/lib.rs")
    //         .expect("Failed to open file for output");
    //
    //     std::io::BufWriter::new(file)
    // }).expect("Failed to write out to file");

    // std::fs::write("../src/lib.rs", formatted.to_string()).unwrap();
}

fn select_version() -> Cow<'static, str> {
    std::env::var("VK_GEN_VERSION")
        .map_or_else(|_| Cow::from("main"), |s| Cow::from(s))
}

fn download_vulkan_spec_file(version: &Cow<str>) -> String {
    let fetch_url = format!("https://raw.githubusercontent.com/KhronosGroup/Vulkan-Docs/{}/xml/vk.xml", version);

    let response = ureq::get(&fetch_url).set("User-Agent", "vk_positron")
        .call();

    match response {
        Ok(r) => {
            r.into_string().unwrap()
        }
        Err(e) => {
            dbg!(&e);
            panic!("Expected valid response\n{:?}", e)
        }
    }
}

fn download_vulkan_video_spec_file(version: &Cow<str>) -> String {
    let fetch_url = format!("https://raw.githubusercontent.com/KhronosGroup/Vulkan-Docs/{}/xml/video.xml", version);

    let response = ureq::get(&fetch_url).set("User-Agent", "vk_positron")
        .call();

    match response {
        Ok(r) => {
            r.into_string().unwrap()
        }
        Err(e) => {
            dbg!(&e);
            panic!("Expected valid response\n{:?}", e)
        }
    }
}
