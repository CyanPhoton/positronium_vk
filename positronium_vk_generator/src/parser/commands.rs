use std::str::FromStr;

use roxmltree::{Node, NodeType};
use crate::parser::check_api;

use crate::parser::common::{ExternSyncMode, Parameter, ParameterContents};

use super::{as_bool, FromNode};

#[derive(Debug)]
pub enum CommandQueuesPipelines {
    Compute,
    Transfer,
    Graphics,
    SparseBinding,
    Decode,
    Encode,
    OpticalFlow,
}

impl FromStr for CommandQueuesPipelines {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "compute" => Ok(CommandQueuesPipelines::Compute),
            "transfer" => Ok(CommandQueuesPipelines::Transfer),
            "graphics" => Ok(CommandQueuesPipelines::Graphics),
            "sparse_binding" => Ok(CommandQueuesPipelines::SparseBinding),
            "decode" => Ok(CommandQueuesPipelines::Decode),
            "encode" => Ok(CommandQueuesPipelines::Encode),
            "opticalflow" => Ok(CommandQueuesPipelines::OpticalFlow),
            e => Err(format!("'{}' is not a command queue", e))
        }
    }
}

#[derive(Debug)]
pub enum CommandRenderPass {
    Inside,
    Outside,
    Both,
}

impl FromStr for CommandRenderPass {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "inside" => Ok(CommandRenderPass::Inside),
            "outside" => Ok(CommandRenderPass::Outside),
            "both" => Ok(CommandRenderPass::Both),
            e => Err(format!("'{}' is not a command render pass", e))
        }
    }
}

#[derive(Debug)]
pub enum CommandBufferLevel {
    Primary,
    Secondary,
}

impl FromStr for CommandBufferLevel {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "primary" => Ok(CommandBufferLevel::Primary),
            "secondary" => Ok(CommandBufferLevel::Secondary),
            e => Err(format!("'{}' is not a command buffer level", e))
        }
    }
}


// impl FromNode for CommandContents {
//     fn from_node(node: Node) -> Self {
//         match node.node_type() {
//             NodeType::Text => CommandContents::Code(node.text().unwrap_or("").trim().to_string()),
//             NodeType::Element => match node.tag_name().name() {
//                 "type" => CommandContents::Type(node.text().unwrap_or("").to_string()),
//                 "name" => CommandContents::Name(node.text().unwrap_or("").to_string()),
//                 e => panic!("Invalid type member sub tag: '{}'", e)
//             }
//             e => panic!("Invalid node type: '{:?}'", e)
//         }
//     }
// }

impl FromNode for Parameter {
    fn from_node(node: Node) -> Self {
        assert!(node.has_tag_name("param"));

        Parameter {
            len: node.attribute("len").map_or(Vec::new(), |s| s.split(',').map(str::to_string).collect()),
            alt_len: node.attribute("altlen").map_or(Vec::new(), |s| s.split(',').map(str::to_string).collect()),
            optional: node.attribute("optional").map_or((!node.has_attribute("validstructs")).then(|| node.attribute("noautovalidity")).flatten().map(as_bool).map_or(Vec::new(), |v| vec![v]), |s| s.split(',').map(as_bool).collect()),
            selector: node.attribute("selector").map(str::to_string),
            extern_sync: node.attribute("externsync").map(|e| {
                if e.trim().eq_ignore_ascii_case("true") || e.trim().eq_ignore_ascii_case("false") {
                    ExternSyncMode::Bool(as_bool(e))
                } else {
                    ExternSyncMode::Fields(e.split(',').map(|e| e.to_string()).collect())
                }
            }),
            valid_structs: node.attribute("validstructs").map_or(Vec::new(), |s| s.split(',').map(str::to_string).collect()),
            contents: node.children().filter(|n| n.is_text() || n.is_element()).map(ParameterContents::from_node).collect(),
        }
    }
}

#[derive(Debug)]
pub enum Command {
    Alias {
        name: String,
        alias: String,
    },
    Definition {
        queues: Vec<CommandQueuesPipelines>,
        success_codes: Vec<String>,
        error_codes: Vec<String>,
        render_pass: Option<CommandRenderPass>,
        cmd_buffer_level: Vec<CommandBufferLevel>,
        pipeline: Option<CommandQueuesPipelines>,
        proto: Vec<ParameterContents>,
        param: Vec<Parameter>,
    },
}

impl FromNode for Command {
    fn from_node(node: Node) -> Self {
        assert!(node.has_tag_name("command"));

        if let Some(alias) = node.attribute("alias") {
            return Command::Alias {
                name: node.attribute("name").unwrap().to_string(),
                alias: alias.to_string(),
            };
        }

        Command::Definition {
            queues: node.attribute("queues").map_or(Vec::new(), |s| s.split(',').map(CommandQueuesPipelines::from_str).map(Result::unwrap).collect()),
            success_codes: node.attribute("successcodes").map_or(Vec::new(), |s| s.split(',').map(str::to_string).collect()),
            error_codes: node.attribute("errorcodes").map_or(Vec::new(), |s| s.split(',').map(str::to_string).collect()),
            render_pass: node.attribute("renderpass").map(CommandRenderPass::from_str).map(Result::unwrap),
            cmd_buffer_level: node.attribute("cmdbufferlevel").map_or(Vec::new(), |s| s.split(',').map(CommandBufferLevel::from_str).map(Result::unwrap).collect()),
            pipeline: node.attribute("pipeline").map(CommandQueuesPipelines::from_str).map(Result::unwrap),
            proto: node.first_element_child().unwrap().children().filter(|n| n.is_element() || n.is_text()).map(ParameterContents::from_node).collect(),
            param: node.children().filter(Node::is_element).filter(|n| n.has_tag_name("param")).filter(check_api).map(Parameter::from_node).collect(),
        }
    }
}
