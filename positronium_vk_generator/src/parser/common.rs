#[derive(Debug)]
pub struct Parameter {
    pub len: Vec<String>,
    pub alt_len: Vec<String>,
    pub optional: Vec<bool>,
    pub selector: Option<String>,
    pub extern_sync: Option<ExternSyncMode>,
    pub valid_structs: Vec<String>,
    pub contents: Vec<ParameterContents>,
}

#[derive(Debug)]
pub enum ExternSyncMode {
    Bool(bool),
    Fields(Vec<String>)
}

#[derive(Debug)]
pub enum ParameterContents {
    Code(String),
    Type(String),
    Name(String),
    Enum(String),
}