use roxmltree::Node;

use super::FromNode;

#[derive(Debug)]
pub struct Platform {
    pub name: String,
    pub protect: String,
}

impl FromNode for Platform {
    fn from_node(node: Node) -> Self {
        assert!(node.has_tag_name("platform"));

        Platform {
            name: node.attribute("name").unwrap().to_string(),
            protect: node.attribute("protect").unwrap().to_string(),
        }
    }
}
