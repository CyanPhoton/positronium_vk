use std::str::FromStr;

use roxmltree::Node;

use super::FromNode;

#[derive(Debug, Eq, PartialEq)]
pub enum EnumTypes {
    Constant,
    Enum,
    BitMask,
}

impl FromStr for EnumTypes {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "enum" => Ok(EnumTypes::Enum),
            "bitmask" => Ok(EnumTypes::BitMask),
            "" => Ok(EnumTypes::BitMask),
            e => Err(format!("'{}' is not a valid enum type", e))
        }
    }
}

#[derive(Debug)]
pub enum EnumVariantType {
    Value { value: String, value_type: Option<String> },
    Bitpos(String),
    Alias(String),
}

#[derive(Debug)]
pub struct EnumVariant {
    pub variant_type: EnumVariantType,
    pub name: String,
    pub api: Option<String>,
}

impl FromNode for EnumVariant {
    fn from_node(node: Node) -> Self {
        assert!(node.has_tag_name("enum"));

        let variant_type = if let Some(value) = node.attribute("value") {
            assert!(!node.has_attribute("bitpos"));
            assert!(!node.has_attribute("alias"));

            EnumVariantType::Value {
                value: value.to_string(),
                value_type: node.attribute("type").map(str::to_string),
            }
        } else if let Some(bitpos) = node.attribute("bitpos") {
            assert!(!node.has_attribute("alias"));

            EnumVariantType::Bitpos(bitpos.to_string())
        } else if let Some(alias) = node.attribute("alias") {
            EnumVariantType::Alias(alias.to_string())
        } else {
            panic!("Invalid enum variant")
        };

        EnumVariant {
            variant_type,
            name: node.attribute("name").unwrap().to_string(),
            api: node.attribute("api").map(str::to_string),
        }
    }
}

#[derive(Debug)]
pub struct Enum {
    pub name: Option<String>,
    pub enum_type: EnumTypes,
    pub start: Option<String>,
    pub end: Option<String>,
    pub vendor: Option<String>,
    pub bit_width: u8,
    pub variants: Vec<EnumVariant>,
}

impl FromNode for Enum {
    fn from_node(node: Node) -> Self {
        assert!(node.has_tag_name("enums"));

        Enum {
            name: node.attribute("name").map(str::to_string),
            enum_type: node.attribute("type").map(EnumTypes::from_str).map(Result::unwrap).unwrap_or(EnumTypes::Constant),
            start: node.attribute("start").map(str::to_string),
            end: node.attribute("end").map(str::to_string),
            vendor: node.attribute("vendor").map(str::to_string),
            bit_width: node.attribute("bitwidth").map(u8::from_str).map(Result::unwrap).unwrap_or(32),
            variants: node.children().filter(|n| n.has_tag_name("enum")).map(EnumVariant::from_node).collect(),
        }
    }
}
