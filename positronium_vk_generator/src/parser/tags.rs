use roxmltree::Node;

use super::FromNode;

#[derive(Debug)]
pub struct Tag {
    pub name: String,
    pub author: String,
    pub contact: String,
}

impl FromNode for Tag {
    fn from_node(node: Node) -> Self {
        assert!(node.has_tag_name("tag"));

        Tag {
            name: node.attribute("name").unwrap().to_string(),
            author: node.attribute("author").unwrap().to_string(),
            contact: node.attribute("contact").unwrap().to_string(),
        }
    }
}
