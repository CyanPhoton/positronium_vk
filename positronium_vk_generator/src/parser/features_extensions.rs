use std::str::FromStr;

use roxmltree::Node;

use crate::parser::{as_bool, check_api};

use super::FromNode;

#[derive(Debug)]
pub struct Feature {
    pub api: Vec<String>,
    pub name: String,
    pub number: String,
    pub sort_order: i32,
    pub protect: Option<String>,
    pub require_removes: Vec<RequireRemove>,
}

impl FromNode for Feature {
    fn from_node(node: Node) -> Self {
        assert!(node.has_tag_name("feature"));

        Feature {
            api: node.attribute("api").unwrap().split(',').map(str::to_string).collect(),
            name: node.attribute("name").unwrap().to_string(),
            number: node.attribute("number").unwrap().to_string(),
            sort_order: node.attribute("sortorder").map(i32::from_str).map(Result::unwrap).unwrap_or(0),
            protect: node.attribute("protect").map(str::to_string),
            require_removes: node.children().filter(Node::is_element).filter(|n| n.has_tag_name("require") || n.has_tag_name("remove")).map(RequireRemove::from_node).collect(),
        }
    }
}

#[derive(Debug)]
pub enum ExtensionType {
    Instance,
    Device,
    Disabled,
}

impl FromStr for ExtensionType {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "instance" => Ok(ExtensionType::Instance),
            "device" => Ok(ExtensionType::Device),
            "" => Ok(ExtensionType::Disabled),
            e => Err(format!("'{}' is not a valid extension type", e))
        }
    }
}

#[derive(Debug)]
pub struct Extension {
    pub name: String,
    pub number: Option<u32>,
    pub sort_order: i32,
    pub author: Option<String>,
    pub extension_type: ExtensionType,
    pub requires: Vec<String>,
    pub requires_core: Option<String>,
    pub depends: Option<String>,
    pub protect: Option<String>,
    pub platform: Option<String>,
    pub supported: Vec<String>,
    pub promoted_to: Option<String>,
    pub deprecated_by: Option<String>,
    pub obsoleted_by: Option<String>,
    pub provisional: bool,
    pub special_use: Option<String>,
    pub require_removes: Vec<RequireRemove>,
}

impl FromNode for Extension {
    fn from_node(node: Node) -> Self {
        assert!(node.has_tag_name("extension"));

        Extension {
            name: node.attribute("name").unwrap().to_string(),
            number: node.attribute("number").map(|n| n.parse().unwrap()),
            sort_order: node.attribute("sortorder").map(|s| s.parse().unwrap()).unwrap_or(0),
            author: node.attribute("author").map(str::to_string),
            extension_type: node.attribute("type").map(ExtensionType::from_str).map(Result::unwrap).unwrap_or(ExtensionType::Disabled),
            requires: node.attribute("requires").map_or(Vec::new(), |s| s.split(',').map(str::to_string).collect()),
            requires_core: node.attribute("requiresCore").map(str::to_string),
            depends: node.attribute("depends").map(str::to_string),
            protect: node.attribute("protect").map(str::to_string),
            platform: node.attribute("platform").map(str::to_string),
            supported: node.attribute("supported").unwrap().split(',').map(str::to_string).collect(),
            promoted_to: node.attribute("promotedto").map(str::to_string),
            deprecated_by: node.attribute("deprecatedby").map(str::to_string),
            obsoleted_by: node.attribute("obsoletedby").map(str::to_string),
            provisional: node.attribute("provisional").map_or(false, as_bool),
            special_use: node.attribute("specialuse").map(str::to_string),
            require_removes: node.children().filter(Node::is_element).filter(|n| n.has_tag_name("require") || n.has_tag_name("remove")).map(RequireRemove::from_node).collect(),
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum RequireRemoveType {
    Required,
    Removed,
}

impl FromStr for RequireRemoveType {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "require" => Ok(RequireRemoveType::Required),
            "remove" => Ok(RequireRemoveType::Removed),
            e => Err(format!("'{}' is not either 'require' or 'remove'", e))
        }
    }
}

#[derive(Debug)]
pub struct Command {
    pub name: String,
}

impl FromNode for Command {
    fn from_node(node: Node) -> Self {
        assert!(node.has_tag_name("command"));

        Command {
            name: node.attribute("name").unwrap().to_string()
        }
    }
}

#[derive(Debug)]
pub enum EnumType {
    Reference,
    Extension(EnumExtensionType),
}

#[derive(Debug)]
pub enum EnumExtensionType {
    NumericValue {
        value: String,
        value_type: Option<String>,
        extends: Option<String>,
    },
    BitmaskValue {
        bitpos: u32,
        extends: Option<String>,
    },
    Alias {
        alias: String,
        extends: Option<String>,
    },
    ValueAdded {
        offset: u32,
        ext_number: Option<u32>,
        negative_dir: bool,
        extends: String,
    },
}

#[derive(Debug)]
pub struct Enum {
    pub name: String,
    pub api: Vec<String>,
    pub enum_type: EnumType,
}

impl FromNode for Enum {
    fn from_node(node: Node) -> Self {
        assert!(node.has_tag_name("enum"));

        let value = node.attribute("value").map(str::to_string);
        let bitpos = node.attribute("bitpos").map(u32::from_str).map(Result::unwrap);
        let alias = node.attribute("alias").map(str::to_string);
        let offset = node.attribute("offset").map(u32::from_str).map(Result::unwrap);

        let enum_type = if let Some(value) = value {
            EnumType::Extension(EnumExtensionType::NumericValue {
                value,
                value_type: node.attribute("type").map(str::to_string),
                extends: node.attribute("extends").map(str::to_string),
            })
        } else if let Some(bitpos) = bitpos {
            EnumType::Extension(EnumExtensionType::BitmaskValue {
                bitpos,
                extends: node.attribute("extends").map(str::to_string),
            })
        } else if let Some(alias) = alias {
            EnumType::Extension(EnumExtensionType::Alias {
                alias,
                extends: node.attribute("extends").map(str::to_string),
            })
        } else if let Some(offset) = offset {
            EnumType::Extension(EnumExtensionType::ValueAdded {
                offset,
                ext_number: node.attribute("extnumber").map(u32::from_str).map(Result::unwrap),
                negative_dir: node.attribute("dir").map_or(false, |s| s == "-"),
                extends: node.attribute("extends").unwrap().to_string(),
            })
        } else {
            EnumType::Reference
        };

        Enum {
            name: node.attribute("name").unwrap().to_string(),
            api: node.attribute("api").map_or(Vec::new(), |s| s.split(',').map(str::to_string).collect()),
            enum_type,
        }
    }
}

#[derive(Debug)]
pub struct Type {
    pub name: String,
}

impl FromNode for Type {
    fn from_node(node: Node) -> Self {
        assert!(node.has_tag_name("type"));

        Type {
            name: node.attribute("name").unwrap().to_string()
        }
    }
}

#[derive(Debug)]
pub struct DefineAlias {
    pub name: String,
    pub value: String,
}

impl FromNode for DefineAlias {
    fn from_node(node: Node) -> Self {
        assert!(node.has_tag_name("enum"));

        DefineAlias {
            name: node.attribute("name").unwrap().to_string(),
            value: node.attribute("value").unwrap().to_string(),
        }
    }
}

#[derive(Debug)]
pub struct RequireRemove {
    pub rr_type: RequireRemoveType,
    pub profile: Option<String>,
    pub api: Vec<String>,
    pub extension: Option<String>,
    pub feature: Option<String>,
    pub commands: Vec<Command>,
    pub enums: Vec<Enum>,
    pub types: Vec<Type>,
    pub define_aliases: Vec<DefineAlias>,
}

impl FromNode for RequireRemove {
    fn from_node(node: Node) -> Self {
        assert!(node.has_tag_name("require") || node.has_tag_name("remove"));

        RequireRemove {
            rr_type: RequireRemoveType::from_str(node.tag_name().name()).unwrap(),
            profile: node.attribute("profile").map(str::to_string),
            api: node.attribute("api").map_or(Vec::new(), |s| s.split(',').map(str::to_string).collect()),
            extension: node.attribute("extension").map(str::to_string),
            feature: node.attribute("features").map(str::to_string),
            commands: node.children().filter(Node::is_element).filter(|n| n.has_tag_name("command")).filter(check_api).map(Command::from_node).collect(),
            enums: node.children().filter(Node::is_element).filter(|n| n.has_tag_name("enum") && n.attribute("value").map_or(true, |v| !v.starts_with("VK_"))).map(Enum::from_node).collect(),
            types: node.children().filter(Node::is_element).filter(|n| n.has_tag_name("type")).map(Type::from_node).collect(),
            define_aliases: node.children().filter(Node::is_element).filter(|n| n.has_tag_name("enum") && n.attribute("value").map_or(false, |v| v.starts_with("VK_"))).map(DefineAlias::from_node).collect(),
        }
    }
}