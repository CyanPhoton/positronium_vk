use std::ops::{Deref, DerefMut};
use std::str::FromStr;

use roxmltree::{Node, NodeType};
use crate::parser::check_api;

use crate::parser::common::{ExternSyncMode, Parameter, ParameterContents};

use super::{as_bool, FromNode};

#[derive(Debug, Eq, PartialEq)]
pub enum TypeCategories {
    BaseType,
    BitMask,
    Define,
    FuncPointer,
    Group,
    Handle,
    Include,
    Struct,
    Union,
    Enum,
    None,
}

impl FromStr for TypeCategories {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "basetype" => Ok(TypeCategories::BaseType),
            "bitmask" => Ok(TypeCategories::BitMask),
            "define" => Ok(TypeCategories::Define),
            "enum" => Ok(TypeCategories::Enum),
            "funcpointer" => Ok(TypeCategories::FuncPointer),
            "group" => Ok(TypeCategories::Group),
            "handle" => Ok(TypeCategories::Handle),
            "include" => Ok(TypeCategories::Include),
            "struct" => Ok(TypeCategories::Struct),
            "union" => Ok(TypeCategories::Union),
            "" => Ok(TypeCategories::None),
            e => Err(format!("'{}' is not a type category", e))
        }
    }
}

#[derive(Debug)]
pub enum OtherContents {
    Code(String),
    Type(String),
    ApiEntry(String),
    Name(String),
    BitValues(String),
}

impl OtherContents {
    pub fn inner(&self) -> &String {
        match self {
            OtherContents::Code(s) | OtherContents::Type(s) | OtherContents::ApiEntry(s) | OtherContents::Name(s) | OtherContents::BitValues(s) => s
        }
    }
}

impl FromNode for OtherContents {
    fn from_node(node: Node) -> Self {
        match node.node_type() {
            NodeType::Text => OtherContents::Code(node.text().unwrap_or("").trim().to_string()),
            NodeType::Element => match node.tag_name().name() {
                "type" => OtherContents::Type(node.text().unwrap_or("").to_string()),
                "apientry/" => OtherContents::ApiEntry(node.text().unwrap_or("").to_string()),
                "name" => OtherContents::Name(node.text().unwrap_or("").to_string()),
                "bitvalues" => OtherContents::BitValues(node.text().unwrap_or("").to_string()),
                e => panic!("Invalid other sub tag: '{}'", e)
            }
            e => panic!("Invalid node type: '{:?}'", e)
        }
    }
}

impl FromNode for ParameterContents {
    fn from_node(node: Node) -> Self {
        match node.node_type() {
            NodeType::Text => ParameterContents::Code(node.text().unwrap_or("").trim().to_string()),
            NodeType::Element => match node.tag_name().name() {
                "type" => ParameterContents::Type(node.text().unwrap_or("").to_string()),
                "name" => ParameterContents::Name(node.text().unwrap_or("").to_string()),
                "enum" => ParameterContents::Enum(node.text().unwrap_or("").to_string()),
                e => panic!("Invalid type member sub tag: '{}'", e)
            }
            e => panic!("Invalid node type: '{:?}'", e)
        }
    }
}

#[derive(Debug)]
pub struct TypeMember {
    pub values: Vec<String>,
    pub selections: Vec<String>,
    pub parameter: Parameter,
}

impl Deref for TypeMember {
    type Target = Parameter;

    fn deref(&self) -> &Parameter {
        &self.parameter
    }
}

impl DerefMut for TypeMember {
    fn deref_mut(&mut self) -> &mut Parameter {
        &mut self.parameter
    }
}

impl FromNode for TypeMember {
    fn from_node(node: Node) -> Self {
        assert!(node.has_tag_name("member"));

        TypeMember {
            values: node.attribute("values").map_or(Vec::new(), |s| s.split(',').map(str::to_string).collect()),
            selections: node.attribute("selection").map(|s| s.split(',').map(str::to_string).collect::<Vec<_>>()).unwrap_or_default(),
            parameter: Parameter {
                len: node.attribute("len").map_or(Vec::new(), |s| s.split(',').map(str::to_string).collect()),
                alt_len: node.attribute("altlen").map_or(Vec::new(), |s| s.split(',').map(str::to_string).collect()),
                extern_sync: node.attribute("externsync").map(|e| {
                    if e.trim().eq_ignore_ascii_case("true") || e.trim().eq_ignore_ascii_case("false") {
                        ExternSyncMode::Bool(as_bool(e))
                    } else {
                        ExternSyncMode::Fields(e.split(',').map(|e| e.to_string()).collect())
                    }
                }),
                optional: node.attribute("optional").map_or_else(|| node.attribute("noautovalidity").map(as_bool).into_iter().collect(), |s| s.split(',').map(as_bool).collect()),
                selector: node.attribute("selector").map(str::to_string),
                contents: node.children().filter(|n| n.is_element() || n.is_text()).filter(|n| !n.has_tag_name("comment")).map(ParameterContents::from_node).collect(),
                valid_structs: vec![]
            },
        }
    }
}

#[derive(Debug)]
pub enum Type {
    StructUnion {
        requires: Option<String>,
        name: String,
        alias: Option<String>,
        api: Option<String>,
        category: TypeCategories,
        returned_only: bool,
        struct_extends: Vec<String>,
        allow_duplicate: bool,
        members: Vec<TypeMember>,
    },
    Enum {
        requires: Option<String>,
        name: String,
        alias: Option<String>,
        api: Option<String>,
    },
    Other {
        requires: Option<String>,
        name: Option<String>,
        alias: Option<String>,
        api: Option<String>,
        category: TypeCategories,
        parent: Option<String>,
        contents: Vec<OtherContents>,
        obj_type_enum: Option<String>,
        bit_values: Option<String>,
    },
}

impl FromNode for Type {
    fn from_node(node: Node) -> Self {
        assert!(node.has_tag_name("type"));

        let category = node.attribute("category").map(TypeCategories::from_str).map(Result::unwrap).unwrap_or(TypeCategories::None);

        match category {
            c @ TypeCategories::Struct | c @ TypeCategories::Union => Type::StructUnion {
                requires: node.attribute("requires").map(str::to_string),
                name: node.attribute("name").unwrap().to_string(),
                alias: node.attribute("alias").map(str::to_string),
                api: node.attribute("api").map(str::to_string),
                category: c,
                returned_only: node.attribute("returnedonly").map_or(false, as_bool),
                struct_extends: node.attribute("structextends").map_or(Vec::new(), |s| s.split(',').map(str::to_string).collect()),
                allow_duplicate: node.attribute("allowduplicate").map_or(false, as_bool),
                members: node.children().filter(Node::is_element).filter(|n| !n.has_tag_name("comment")).filter(check_api).map(TypeMember::from_node).collect(),
            },
            TypeCategories::Enum => Type::Enum {
                requires: node.attribute("requires").map(str::to_string),
                name: node.attribute("name").unwrap().to_string(),
                alias: node.attribute("alias").map(str::to_string),
                api: node.attribute("api").map(str::to_string),
            },
            c => Type::Other {
                requires: node.attribute("requires").map(str::to_string),
                name: node.attribute("name").map(str::to_string),
                alias: node.attribute("alias").map(str::to_string),
                api: node.attribute("api").map(str::to_string),
                category: c,
                parent: node.attribute("parent").map(str::to_string),
                contents: node.children().filter(|n| n.is_element() || n.is_text()).filter(|n| !n.has_tag_name("comment")).filter(check_api).map(OtherContents::from_node).collect(),
                obj_type_enum: node.attribute("objtypeenum").map(str::to_string),
                bit_values: node.attribute("bitvalues").map(str::to_string),
            }
        }
    }
}
