extern crate proc_macro;

use proc_macro::TokenStream;

use proc_macro2::Span;
use quote::quote;
use syn::{Ident, parse_macro_input};

#[proc_macro]
pub fn unsized_cstr_int(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as syn::LitStr);

    let mut bytes = input.value().into_bytes();

    let error = if let Some(i) = bytes.iter().position(|b| *b == 0) {
        if i != bytes.len() - 1 {
            // panic!("Can not construct UnsizedCStr from string literal that contains interior null bytes")
            true
        } else {
            false
        }
    } else {
        bytes.push(0);
        false
    };

    TokenStream::from(
        if error {
            syn::Error::new_spanned(input, "The string contains interior nul byte(s).").to_compile_error()
        } else {
            quote! {
                unsafe { crate::UnsizedCStr::from_first_unchecked(&[#(#bytes,)*][0]) }
            }
        },
    )
}

#[proc_macro]
pub fn unsized_cstr(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as syn::LitStr);

    let mut bytes = input.value().into_bytes();

    let error = if let Some(i) = bytes.iter().position(|b| *b == 0) {
        if i != bytes.len() - 1 {
            // panic!("Can not construct UnsizedCStr from string literal that contains interior null bytes")
            true
        } else {
            false
        }
    } else {
        bytes.push(0);
        false
    };

    let krate = Ident::new(&get_vk_crate_name(), Span::call_site());

    TokenStream::from(
        if error {
            syn::Error::new_spanned(input, "The string contains interior nul byte(s).").to_compile_error()
        } else {
            quote! {
                unsafe { ::#krate::UnsizedCStr::from_first_unchecked(&[#(#bytes,)*][0]) }
            }
        },
    )
}

fn get_vk_crate_name() -> String {
    ::proc_macro_crate::crate_name("positronium_vk").unwrap_or_else(|err| {
        eprintln!("Warning: {}\n    => defaulting to `positronium_vk`", err, );
        String::from("positronium_vk")
    })
}
